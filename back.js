var fs = require('fs');
var spawn = require('child_process').spawn;
var argv = process.argv;
var mongoHost = process.argv[2];
var baseDirName = process.argv[3];
var database = process.argv[4];

var interval = 3 * 24 * 60 * 60 * 1000;
 
function getDateString(date) {
  if (typeof date === 'number') {
    date = new Date(date);
  }
  return String(date.getFullYear()) + (date.getMonth() + 1) + date.getDate();
}
 
function handleDump(now) {
  var collectionIndex = 5;
  doDump(collectionIndex, now); 
}
 
function doDump(index, now) {
  var saveDir = getDateString(now) + '_' + baseDirName;
  var dumpArgvs = ['-h', mongoHost, '-o', saveDir];
  if (database) {
    dumpArgvs = dumpArgvs.concat(['-d', database]);
    var collection = process.argv[index];
    if (collection) {
      dumpArgvs = dumpArgvs.concat(['-c', collection]);
    }
  }
  spawn('mongodump', dumpArgvs).on('exit', function (code) {
    if (process.argv[++index]) {
      doDump(index, now);
    }
  });
}
 
setInterval(function () {
  var now = new Date();
  handleDump(now);
}, interval);
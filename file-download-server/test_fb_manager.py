__author__ = 'bone-lee'

from fb_manager import FBCoinManager
from resource_manager import ResourceStoreManager

import tornado.gen
import tornado.testing
from time import time
import motor

def mock_file_hash(file_name):
    return abs(hash(file_name))

class DownloadTestCase(tornado.testing.AsyncTestCase):
  def test_download(self):
      @tornado.gen.engine
      def func(callback):
            uid=1234
            file_hash=hash("test.text")

            db=motor.MotorClient('localhost', 27017, io_loop=self.io_loop).fbt_test
            FBCoinManager.set_db(db)
            yield FBCoinManager.clear_db()

            ret0=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret0, 0)

            yield FBCoinManager.register_ok(uid)
            ret1=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret1, FBCoinManager._coin_rules["register"])

            yield FBCoinManager.invite_a_user(uid)
            ret2=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret2, ret1+FBCoinManager._coin_rules["invite_a_user"])

            now=long(time())
            yield FBCoinManager.user_online_ok(uid,now,now+3600)
            ret3=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret3,ret2+FBCoinManager._coin_rules["online_an_hour"]*0)

            yield FBCoinManager.plus_fb(uid,170)
            ret4=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret4,ret3+170)

            yield FBCoinManager.user_online_ok(uid,now,now+3600)
            ret5=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret5, ret4+FBCoinManager._coin_rules["online_an_hour"])

            yield FBCoinManager.user_online_ok(uid,now+3600,now+7200)
            ret6=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret6, ret5+(1*FBCoinManager._coin_rules["online_an_hour"]))

            yield FBCoinManager.focus_us_ok(uid)
            ret7=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret7, ret6+FBCoinManager._coin_rules["focus_us"])

            yield FBCoinManager.focus_us_ok(uid)
            ret8=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret8, ret7)

            yield FBCoinManager.sns_share_ok(uid)
            ret9=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret9, ret8+FBCoinManager._coin_rules["sns_share_first_time"])

            yield FBCoinManager.sns_share_ok(uid)
            ret10=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret10, ret9+FBCoinManager._coin_rules["sns_share_second_time"])

            yield FBCoinManager.sns_share_ok(uid)
            ret2=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret2, ret10+FBCoinManager._coin_rules["sns_share_second_time"])

            yield FBCoinManager.add_to_public_download_queue(uid,mock_file_hash("test2.txt"))
            yield FBCoinManager.add_to_public_download_queue(uid,mock_file_hash("test3.txt"))
            yield FBCoinManager.add_to_public_download_queue(uid,mock_file_hash("test4.txt"))
            ret3=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret3, ret2)

            (his_uid,her_uid)=(4321,9876)
            yield FBCoinManager.register_ok(his_uid)
            # yield FBCoinManager.register_ok(her_uid)
            his_fb=yield FBCoinManager.get_user_total_fb_coin(his_uid)
            her_fb=yield FBCoinManager.get_user_total_fb_coin(her_uid)
            self.assertEqual(his_fb, FBCoinManager._coin_rules["register"])
            self.assertEqual(her_fb, 0)

            users_downloaded_from=[his_uid,her_uid]
            yield FBCoinManager.public_resource_download_ok(uid,mock_file_hash("test2.txt"),users_downloaded_from)
            ret2=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret2, ret3+FBCoinManager._coin_rules["download_a_public_resource"])
            his_fb2=yield FBCoinManager.get_user_total_fb_coin(his_uid)
            self.assertEqual(his_fb2, his_fb+FBCoinManager._coin_rules["resource_download_by_other"])
            her_fb=yield FBCoinManager.get_user_total_fb_coin(her_uid)
            self.assertEqual(her_fb, 0)

            yield FBCoinManager.public_resource_download_ok(uid,mock_file_hash("test3.txt"),users_downloaded_from)
            ret3=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret3, ret2+FBCoinManager._coin_rules["download_a_public_resource"])
            his_fb3=yield FBCoinManager.get_user_total_fb_coin(his_uid)
            self.assertEqual(his_fb3, his_fb2+FBCoinManager._coin_rules["resource_download_by_other"])
            her_fb=yield FBCoinManager.get_user_total_fb_coin(her_uid)
            self.assertEqual(her_fb, 0)

            #test private download
            yield FBCoinManager.public_resource_download_ok(uid,mock_file_hash("not-exist-or-private-resource.txt"),users_downloaded_from)
            ret4=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret4, ret3)

            file_hashes=set()
            for i in range(100):
                file_name="more_file"+str(i)+".txt"
                file_hash=mock_file_hash(file_name)
                assert file_hash not in file_hashes
                file_hashes.add(file_hash)
                yield FBCoinManager.add_to_public_download_queue(uid,file_hash)

            for i in range(100):
                file_name="more_file"+str(i)+".txt"
                yield FBCoinManager.public_resource_download_ok(uid,mock_file_hash(file_name),users_downloaded_from)

            his_fb4=yield FBCoinManager.get_user_total_fb_coin(his_uid)
            self.assertEqual(his_fb4, his_fb3+100*FBCoinManager._coin_rules["resource_download_by_other"])
            her_fb=yield FBCoinManager.get_user_total_fb_coin(her_uid)
            self.assertEqual(her_fb, 0)

            ret2=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.assertEqual(ret2, 0)

            # test hot resource download 100 times
            file_name="test_file.txt"
            FBCoinManager.register_ok(her_uid)
            ResourceStoreManager.set_db(db)
            ResourceStoreManager.clear_db()
            yield ResourceStoreManager.user_upload_resource(her_uid,"her",mock_file_hash(file_name),file_name,123,True,["test"],0,0,2,"comments")
            for i in range(100):
                yield ResourceStoreManager.increase_download_num(mock_file_hash(file_name))
            her_fb=yield FBCoinManager.get_user_total_fb_coin(her_uid)
            self.assertEqual(her_fb, FBCoinManager._coin_rules["register"]+FBCoinManager._coin_rules["hot100_resource"])

            callback()

      func(callback=self.stop)
      self.wait()

if __name__ == '__main__':
    import unittest
    unittest.main()
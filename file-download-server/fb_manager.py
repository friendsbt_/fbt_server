#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'bone-lee'

import logging
from tornado import gen
import motor
from fb_rank_manager import FBRankManager

class FBCoinManager(object):
    '''
FBT积分规则：
（1）注册成功，第一次登陆FBT送30个F币
（2）上传和下载私有圈资源（仅好友可见），无需F币
（3）下载一个公共资源扣5个F币
（4）积分大于100的用户，每在线1小时，送5个F币
（5）上传资源被他人成功下载一次，送5个F币
（6）上传非法（色情、反动等）公共资源扣除50个F币，情节严重者封号处理
（7）成功举报非法公共资源，送50个F币
（8）上传的资源标题和内容不符，扣除50个F币
（9）成功举报标题和内容不符的公共资源，送50个F币
（10）通过社交平台分享fbt链接成功第一次送50个F币，以后每次送10个F币，100封顶
（11）关注我们微博，送50个F币
    '''
    public_resource_download_coin = 5
    _ASCENDING = 1
    _DESCENDING = -1

    _coin_rules={"register": 30,
                 "online_an_hour": 5,
                 "invite_a_user": 50,
                 "hot100_resource": 100,
                 "online_plus_fb_line": 100,
                 "resource_download_by_other": 5,
                 "download_a_public_resource": -public_resource_download_coin,
                 "sns_share_first_time": 50,
                 "sns_share_second_time": 10,
                 "tip_off": 50,
                 "focus_us": 50}

    _db = motor.MotorClient('localhost', 27017).fbt
    @classmethod
    def set_db(cls,db):
        cls._db=db

    @classmethod
    @gen.coroutine
    def clear_db(cls):
        yield cls._db.coins_of_user.remove()

    @classmethod
    @gen.coroutine
    def register_ok(cls,uid):
        uid = long(uid)
        assert (uid > 0)
        coin = yield cls._db.coins_of_user.find_one({'uid': uid})
        if not coin:
            yield cls._db.coins_of_user.insert({'uid' : uid, 'total_coins' : cls._coin_rules["register"]})

    @classmethod
    @gen.coroutine
    def user_online_ok(cls,uid,online_at,offline_at):
        assert online_at>0 and offline_at>0 and offline_at>online_at
        uid = long(uid)
        assert uid > 0
        online_hour=(offline_at-online_at)/3600.0 #long(time()) return seconds
        coin = yield cls._db.coins_of_user.find_one({'uid': uid})
        if coin:
            if "total_coins" in coin and coin["total_coins"]>=cls._coin_rules["online_plus_fb_line"]:
                cls.update_coin(coin,uid,online_hour*cls._coin_rules["online_an_hour"])
                # coin["total_coins"]+=online_hour*cls._coin_rules["online_an_hour"]
                # print "coins:"+str(coin["total_coins"])
                yield cls._db.coins_of_user.save(coin)
        else:
            logging.info("Warning: uid not in coin db. uid:"+str(uid))

    @classmethod
    @gen.coroutine
    def focus_us_ok(cls,uid):
        uid = long(uid)
        assert uid > 0
        coin = yield cls._db.coins_of_user.find_one({'uid': uid})
        if coin:
            if "focus_us" in coin:
                if not coin["focus_us"]:
                    coin["focus_us"]=True
                    cls.update_coin(coin,uid,cls._coin_rules["focus_us"])
                    # coin["total_coins"]+=cls._coin_rules["focus_us"]
                else:
                    logging.info("duplicated focus us uid:"+str(uid))
            else:
                coin["focus_us"]=True
                cls.update_coin(coin,uid,cls._coin_rules["focus_us"])
                # coin["total_coins"]+=cls._coin_rules["focus_us"]
            yield cls._db.coins_of_user.save(coin)
        else:
            logging.info("Warning: uid not in coin db. uid:"+str(uid))

    @classmethod
    @gen.coroutine
    def add_to_public_download_queue(cls,uid,file_hash):
        uid = long(uid)
        assert uid > 0
        assert file_hash > 0
        yield cls._db.coins_of_user.update({"uid": uid}, {"$addToSet": {"public_download_queue": file_hash}}, True)

    @classmethod
    @gen.coroutine
    def public_resource_download_ok(cls,uid,file_hash, users_downloaded_from):
        uid = long(uid)
        assert uid > 0
        assert len(users_downloaded_from)>0
        coin = yield cls._db.coins_of_user.find_one({'uid': uid})
        if coin and "public_download_queue" in coin:
            try:
                coin["public_download_queue"].remove(file_hash)
                cls.update_coin(coin,uid,cls._coin_rules["download_a_public_resource"])
                # coin["total_coins"]+=cls._coin_rules["download_a_public_resource"]
                # if coin["total_coins"]<0:
                #     coin["total_coins"]=0
                yield cls._db.coins_of_user.save(coin)
                for user in users_downloaded_from:
                    assert user>0
                    yield cls.plus_fb(user,cls._coin_rules["resource_download_by_other"])
            except:
                logging.info("Warning: logic err. file_hash not in download queue of coin db.")
        else:
            logging.info("Warning: logic err. uid or public_download_queue not in coin db.")

    @classmethod
    @gen.coroutine
    def sns_share_ok(cls,uid_of_shared_person):
        uid_of_shared_person = long(uid_of_shared_person)
        assert uid_of_shared_person > 0
        coin = yield cls._db.coins_of_user.find_one({'uid': uid_of_shared_person})
        if coin:
            if "sns_share_times" in coin:
                MAX_SNS_SHARE_CNT=11
                if coin["sns_share_times"] < MAX_SNS_SHARE_CNT:
                    coin["sns_share_times"]+=1
                    cls.update_coin(coin,uid_of_shared_person,cls._coin_rules["sns_share_second_time"])
                    #coin["total_coins"]+=cls._coin_rules["sns_share_second_time"]
                #else:
                #    logging.info("Sns share for "+str(shared_uid)+" is exceed 10.")
            else:
                coin["sns_share_times"]=1
                cls.update_coin(coin,uid_of_shared_person,cls._coin_rules["sns_share_first_time"])
                #coin["total_coins"]+=cls._coin_rules["sns_share_first_time"]
            yield cls._db.coins_of_user.save(coin)
        else:
            logging.info("Warning: logic err. uid not in coin db.")


    @classmethod
    @gen.coroutine
    def get_user_total_fb_coin(cls,uid):
        uid = long(uid)
        assert uid > 0
        coin = yield cls._db.coins_of_user.find_one({'uid': uid})
        if coin and "total_coins" in coin:
            raise gen.Return(int(round(coin["total_coins"])))
        else:
            raise gen.Return(0)

    @classmethod
    @gen.coroutine
    def plus_fb(cls,uid,how_many):
        uid = long(uid)
        assert uid > 0
        coin = yield cls._db.coins_of_user.find_one({'uid': uid})
        if coin and "total_coins" in coin:
            #coin["total_coins"]+=how_many
            #if coin["total_coins"]<0:
            #    coin["total_coins"]=0
            cls.update_coin(coin,uid,how_many)
            yield cls._db.coins_of_user.save(coin)

    @classmethod
    @gen.coroutine
    def get_total_rank(cls):
        cursor = cls._db.coins_of_user.find({}).sort([('total_coins', cls._DESCENDING),]).limit(50)
        ans=[]
        while (yield cursor.fetch_next):
                coin = cursor.next_object()
                ans.append((coin['uid'],int(coin['total_coins'])))
        raise gen.Return(ans)

   
    @classmethod
    @gen.coroutine
    def invite_a_user(cls,uid):
        uid = long(uid)
        assert uid > 0
        yield cls.plus_fb(uid,cls._coin_rules["invite_a_user"])

    @classmethod
    @gen.coroutine
    def hot100_resource(cls,uid):
        uid = long(uid)
        assert uid > 0
        yield cls.plus_fb(uid,cls._coin_rules["hot100_resource"])

    @classmethod
    def update_coin(cls,coin,uid,delta_fb):
        assert "total_coins" in coin
        coin["total_coins"]+=delta_fb
        if coin["total_coins"]<0:
            coin["total_coins"]=0
        FBRankManager.update_fb(uid,int(delta_fb))


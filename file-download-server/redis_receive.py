# coding: utf-8
__author__ = 'bone-lee'

import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.gen
import tornadoredis

msg_list=list()

class RedisClient(object):
    def __init__(self,channel):
        self.sub_client = tornadoredis.Client()
        self.channel = channel
        self.subscribe()

    @tornado.gen.engine
    def subscribe(self):
        self.sub_client.connect()
        yield tornado.gen.Task(self.sub_client.subscribe, self.channel)
        self.sub_client.listen(self.on_redis_message)

    def on_redis_message(self, message):
        kind, body = message.kind, message.body
        if kind == 'message':
            msg_list.append(body)
        if kind == 'disconnect':
            self.close()

    def close(self):
        if self.sub_client.subscribed:
            self.sub_client.unsubscribe(self.channel)
            self.sub_client.disconnect()

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("message list:"+str(msg_list))

channel="tornado"
redis_sub_client = RedisClient(channel)

class WebApp(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/get', MainHandler),
        ]
        tornado.web.Application.__init__( self, handlers )

if __name__ == '__main__':
    application = WebApp()
    httpServer = tornado.httpserver.HTTPServer(application)
    httpServer.listen(8890)
    try:
        tornado.ioloop.IOLoop.instance().start()
    except:
        redis_sub_client.close()
        print "OK. Exit..."

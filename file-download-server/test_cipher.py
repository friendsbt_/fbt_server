# -*- coding: utf-8 -*-

__author__ = 'bone-lee'

#from cipher import  AESCipher
from cipher import  XorCipher

def test_cipher():
    def test_xor(text):
        enc=XorCipher.encrypt(text)
        dec=XorCipher.decrypt(enc)
        assert  dec == text
    test_xor('')
    test_xor(u'你好')
    test_xor(u'hello，你好！')
    test_xor(u'hello')
    test_xor('hello world')


    def test_AES(text):
        return
        c= AESCipher()
        #enc=c.encrypt(u'你好') #not work
        enc=c.encrypt(text)
        #enc=c.encrypt('hi')
        #print enc
        dec=c.decrypt(enc)
        #print dec
        assert  dec == text

    test_AES('')
    test_AES(u'你好')
    test_AES(u'hello，你好！')
    test_AES('hello')

    #test long data
    test_AES(u'''
    滚滚长江东逝水，浪花淘尽英雄。是非成败转头空。
    　　青山依旧在，几度夕阳红。　　白发渔樵江渚上，惯
    　　看秋月春风。一壶浊酒喜相逢。古今多少事，都付
    　　笑谈中。
    　　——调寄《临江仙》

    话说天下大势，分久必合，合久必分。周末七国分争，并入于秦。及秦灭之后，楚、汉
    分争，又并入于汉。汉朝自高祖斩白蛇而起义，一统天下，后来光武中兴，传至献帝，遂分
    为三国。推其致乱之由，殆始于桓、灵二帝。桓帝禁锢善类，崇信宦官。及桓帝崩，灵帝即
    位，大将军窦武、太傅陈蕃共相辅佐。时有宦官曹节等弄权，窦武、陈蕃谋诛之，机事不
    密，反为所害，中涓自此愈横。

    　　建宁二年四月望日，帝御温德殿。方升座，殿角狂风骤起。只见一条大青蛇，从梁上飞
    将下来，蟠于椅上。帝惊倒，左右急救入宫，百官俱奔避。须臾，蛇不见了。忽然大雷大
    雨，加以冰雹，落到半夜方止，坏却房屋无数。建宁四年二月，洛阳地震；又海水泛溢，沿
    海居民，尽被大浪卷入海中。光和元年，雌鸡化雄。六月朔，黑气十余丈，飞入温德殿中。
    秋七月，有虹现于玉堂；五原山岸，尽皆崩裂。种种不祥，非止一端。帝下诏问群臣以灾异
    之由，议郎蔡邕上疏，以为*堕鸡化，乃妇寺干政之所致，言颇切直。帝览奏叹息，因起更
    衣。曹节在后窃视，悉宣告左右；遂以他事陷邕于罪，放归田里。后张让、赵忠、封谞、段
    珪、曹节、侯览、蹇硕、程旷、夏恽、郭胜十人朋比为奸，号为“十常侍”。帝尊信张让，
    呼为“阿父”。朝政日非，以致天下人心思乱，盗贼蜂起。
    ''')

    #from node js client
#print AESCipher().decrypt(u'''14058273741831401OrWORjbOsSDWWcs7HTvg+9n8dX9cF1vYHALS+x1N3V5ljtBGXVsd2xgyXUydc3dn9pe2sgZNJOeIiwrks+a/sba3ZTEtoqQFU/FOtQAxa/yp2mR5w6dJ+Y6UiUKifPfRZO0Cul9jtUpqeiw9eTUDxAsJsP/aK5JzKkMw4lqa2R4vCSKBQsz6VZJ2U1oGkEknCa6WMR6aMglYJBmclmdqFGoo+CZO5uPWiroOpmbjfEOik1ekK55oU3Xi0v4woCrUklIMh2DHn2UYeMfaCxquIkbdPAWwFDSQEERPIjdp36xgjt8l4UcRcLZAvC1+CHtXNKzYVRvN4LaH13E67VaCk2xfbunIrXPr8Z36/LRLuxj+Naax9nFxrldfnhc2MH/9IWuBI5hXErfcU9gg1zb/MwHgjj8M3851AscR9mJv0aKCohZz5tAu9PUDIAqgtg/7Qw+wPnfDlbeS2dfrbmWrHvh3D9R86UVJ/NgPU0tOywkSPmjQh7oStMWsJAWMQuc0mUugPew7rRkD0DxchfJ/35Ur+IKzI93GSlQaDNZF+zYsk1lYqvk8yaUjHJkYL0xNv1t3BkG2f3k0ROKguPuvt0coHMkjSuD5nfnLx7vN/UH0oF9SqP9CpqY05n4/PuKILhKaZBHaS9qNCnBBwQ94oxhnasN0P4h9zfXA6rldOyJfMpPxfHzlS/+/yJ34J0ed9d8C3j1/ZhhCmV34afNBgdBI9coFg737Lm2jweILtZsOiwu+Q3TYVNnT6bSFdwIrZ388n5PGkZ5K1FGCxZfyz/a1fXAKoLN0BWtoRozgyu0hRrOopp7PR31pvtYKX2W6hANZzfMCVlOC39wHR3DL6rgkj0TuHGW68imo4X8K5hrHAo13gb5WQ0jgwEK76I+I9h7xjUUnR8ewJY2ReDW/KMo+Rdu0JFX3kkPN8Zgc+lW+qsNo1MonksMHR8GW9pB5hftkgQrbNDGgr99FEulbgrDMxAeik4TWFPI/ybZLkX92RdwArYRej9LIkALpD988PkDlVf4S33PqfirV97DXayaB0bu11Cp0m7zAg1M/9xDzMZTbSLD9YFuTHyUj/Lf4Lfj7hVSsZdPSWmaM8vl1wAwyQ4+vi6I/gkdhf8wex+ULQrftjUGUlww9KJEUmKwZjxjlMuvP3WkZGmjhhnwQf9qd8wK2swDe4wg2zljmJeK3t56JlTvQDagQtbDaZiUFpsG2ePFjfbqsmuv7Q6wYKdjBCSKPRavBoH/jlmj9wgJrsE3KSbStslGa/AFVDJla1y5jbbeTNw7bx/qf5ruU5UTUDhFx9Gm2lzuNhTAyV9tR6VADQU/MnxvulzgQE07qDkAzkHqzfVXg4oxHpEsji+Dl3EBoSLwPXz4psqlDcNziCxJzpl294GBn/Ak56fh4SIYyDQjJQ/wNaBO8JXi1dXVxSgeWN4PwTKk93lotLT1lYpFHnMp+F68Y29Xgonbrc/9F8WOuIk9g6G/wvBzXcqQWOFkST4vankYAcZurTqKIc/IyRhMM9Jxnrcq4GZNRoHhnV1Sh0NbyGss2M9cf/pTdV1RYpNKWqYF0CjMV7zgrLC/rKDRwlZTYZslhf+80q6WE0sDQ1SVQI3Hr4BQV+QYmjlltiw6nwAH/8YWfotYPnXw+uC/4X0D+gTJPftTALAa4X+vCAR6zzg9x6XYdk2tsbsi9z9Z7VZWVYrsClK8GvqOob77FcsCS85H5kAZ/BWnJI40GjVaPZYQYh5ZzzUSjX7SP2QYXP2bI2zrSuRcUapJLXdzDRx90QgnHERc6qpNz6Cb+2zXIuSsbyCC/X1jHy35V4Wb9evd7to3c8r9pID6IJenluoKJBZivjx8Ij4TTDg8bsQ4CZFyOTofBncCKeRC9ZDHOUJBmFSR/V++rpzbwtxnvsrJY0tf7oaj1ApRxYIDw/gOVUNmAF5f6g2cixRKvSfuxS1z85mHHUm1Bm9FNHkwBziBo6jZEnN2xY/7zT8nHqD/iTgg2BONVajy+N2UikiftXHY6nsk3WsyVS+ZwODj0Gdt9X8v2lm4r4BTgEVsV3ok5VVpkFc1MXxiXg5gNXpl8wRecAxhcYkt0HmWkdLdGRjpc0H4dYzH+CiWCZ4Y1EkAaR25qwqqnYzZSW9wn0b7FRrIafAuoQM5UplzbtarW+SkhZX8oBRWy2aA4qhHKZ/Nfhizw2RdZH4spedWpEuPdaORj0gvfDQ8Z/F5ZJWlQHWqxNdW7TY9VXdCk49Uj5I22sntLDK7SZMSZfrXAEXMWzl6TR+KLQsfvP9UbAI98u3U8n/IDT/pw4akR7e3FnGWsB9khZzb+/wjFgQjKO82F5kBl9MdM1JmHy7bMmYCrowH12VcWrXUHFQZKMeWOebQGt3pt8IDjAwK4y/r7+bS9WaAMaJLU7xBkeFEtdII47OM5P1NTy3A''')
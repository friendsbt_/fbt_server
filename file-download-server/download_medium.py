__author__ = 'bone-lee'

import ipaddress as IP
from user_ip_cache import UserIPCache
from resource_manager import ResourceStoreManager
from http_server_info_cache import HttpServerInfoCache

# import motor
from tornado import gen


class DownloadMedium(object):
    download_type = {"None": 0, "V4_LAN": 1, "V4_NAT": 2, "V6": 3}

    @classmethod
    def get_matched_online_owners(cls, my_uid, res_users, need_romove_self=False):
        assert my_uid > 0
        online_owners = filter(UserIPCache.user_online, res_users)
        if need_romove_self:
            if online_owners.count(my_uid):  # remove myself
                online_owners.remove(my_uid)
        if len(online_owners):
            if (UserIPCache.user_online(my_uid)):
                my_ipv6_addrs = HttpServerInfoCache.get_user_ipv6(my_uid)
                if isinstance(my_ipv6_addrs, set):
                    assert len(my_ipv6_addrs) > 0
                    v6_owners = []
                    for user in online_owners:
                        v6_addrs = HttpServerInfoCache.get_user_ipv6(user)
                        if isinstance(v6_addrs, set):
                            for addr in v6_addrs:
                                assert IP.is_valid_ipv6_address(addr)
                                v6_owners.append({"uid": user, "host": addr, "port": 8886})
                    if len(v6_owners):
                        return {"owners": v6_owners, "download_type": cls.download_type["V6"]}
                    else:
                        return {"owners": [], "download_type": cls.download_type["V6"]}
                else:
                    # if my_ipv6_addrs is None:
                    my_ipv4 = UserIPCache.get_user_ip(my_uid)
                    assert IP.is_valid_ipv4_address(my_ipv4)
                    same_ip_users = filter(lambda user: UserIPCache.get_user_ip(user) == my_ipv4, online_owners)
                    if len(same_ip_users):  #LAN USER
                        v4_owners = [user for user in same_ip_users if
                                     IP.is_valid_ipv4_address(HttpServerInfoCache.get_user_ipv4(user))]
                        grep_owners = [{"uid": user, "host": HttpServerInfoCache.get_user_ipv4(user), "port": 8884} for
                                       user in
                                       v4_owners]
                        return {"owners": grep_owners, "download_type": cls.download_type["V4_LAN"]}
                    else:  #NAT USER #V4 user cant download v6's resource
                        #return {"owners": [], "download_type": cls.download_type["None"]}
                        valid_v4_owners = [user for user in online_owners if
                                            IP.is_valid_ipv4_address(UserIPCache.get_user_ip(user))] #and not isinstance(HttpServerInfoCache.get_user_ipv6(user),set)]
                        grep_owners = [{"uid": user, "host": UserIPCache.get_user_ip(user), "port": 8884} for user in
                                        valid_v4_owners]  #CAUTION: if user has a external IP, he cant open local LAN server. SO there must use get_user_ip, but not get_user_ipv4
                        return {"owners": grep_owners, "download_type": cls.download_type["V4_NAT"]}
            else:
                return {"owners": [], "download_type": cls.download_type["None"]}
        else:
            return {"owners": [], "download_type": cls.download_type["None"]}

    @classmethod
    @gen.coroutine
    def get_online_file_owner(cls, my_uid, file_hash):
        my_uid = long(my_uid)
        file_hash = long(file_hash)
        assert file_hash > 0
        assert my_uid >= 0
        res_users = yield ResourceStoreManager.get_resource_owners(file_hash)
        raise gen.Return(cls.get_matched_online_owners(my_uid, res_users,True))


__author__ = 'bone-lee'

from resource_manager import ResourceStoreManager
import tornado.gen
import tornado.testing
import motor

class ResourceOwnerAddTestCase(tornado.testing.AsyncTestCase):
  def test_add_owner(self):
      @tornado.gen.engine
      def func(callback):
          ResourceStoreManager.set_db(motor.MotorClient('localhost', 27017, io_loop=self.io_loop).fbt_test)
          yield ResourceStoreManager.clear_db()

          # user 2 uploaded the resource
          yield ResourceStoreManager.user_upload_resource(2,"user2",hash("file_hash2"),"test2.txt",1023,1,"test",0,1,3,"user2 uploaded file")
          yield ResourceStoreManager.add_owner_when_download_over('file_hash2',2) #the same user

          ret1 = yield ResourceStoreManager.get_resource_owners(hash("file_hash2"))
          self.assertEqual(ret1,[2])
          yield ResourceStoreManager.add_owner_when_download_over(hash('file_hash2'),20) #the same user
          ret2 =yield ResourceStoreManager.get_resource_owners(hash("file_hash2"))
          self.assertEqual(ret2,[2,20])

          yield ResourceStoreManager.increase_download_num(hash("file_hash2"))
          res_list = yield ResourceStoreManager.get_resources_list()
          res =filter(lambda x: x['file_hash']==hash("file_hash2"),res_list)
          self.assertEqual(res[0]["download_num"],1)

          yield ResourceStoreManager.increase_download_num(hash("file_hash2"))
          res_list2= yield ResourceStoreManager.get_resources_list()
          res2 =filter(lambda x: x['file_hash']==hash("file_hash2"),res_list2)
          self.assertEqual(res2[0]["download_num"],2)
          callback()

      func(callback=self.stop)
      self.wait()

if __name__ == '__main__':
    import unittest
    unittest.main()
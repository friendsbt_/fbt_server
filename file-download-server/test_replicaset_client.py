__author__ = 'bone-lee'

from motor import MotorReplicaSetClient

if __name__ == "__main__":
    import tornado.ioloop
    from datetime import datetime
    date = datetime.now()

    ioloop = tornado.ioloop.IOLoop()
    rsc = MotorReplicaSetClient(hosts_or_uri="127.0.0.1:27018",io_loop=ioloop, replicaSet='replset')
    print rsc.database_names
    print rsc.test_col.find_one()
    try:
        ioloop.start()
    except KeyboardInterrupt:
        print "I will exit..."

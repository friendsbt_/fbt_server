__author__ = 'bone-lee'

from user_ip_cache import UserIPCache
from resource_manager import ResourceStoreManager
from http_server_info_cache import HttpServerInfoCache
from download_medium import DownloadMedium

import tornado.gen
import tornado.testing
import motor

def mock_hash(file_name):
    return abs(hash(file_name))

class DownloadTestCase(tornado.testing.AsyncTestCase):
  def test_download(self):
      @tornado.gen.engine
      def func(callback):
            UserIPCache.update_my_ip(1,"1.1.1.1")

            UserIPCache.update_my_ip(2,"2.2.2.2")
            UserIPCache.update_my_ip(3,"2.2.2.2")
            UserIPCache.update_my_ip(4,"2.2.2.2") #2,3,4 are the same IP
            UserIPCache.update_my_ip(10,"2.2.2.3")

            UserIPCache.update_my_ip(5,"2.2.2.4")
            UserIPCache.update_my_ip(6,"2.2.2.5")

            UserIPCache.update_my_ip(7,"5.2.2.1")
            UserIPCache.update_my_ip(8,"5.4.2.3")

            UserIPCache.update_my_ip(9,"2.2.2.9")
            UserIPCache.update_my_ip(11,"2.2.2.11")

            HttpServerInfoCache.update_ipv4_address(1,'192.168.1.101')

            HttpServerInfoCache.update_ipv4_address(2,'192.168.1.103')
            HttpServerInfoCache.update_ipv4_address(3,'192.168.1.104') #has no LAN IP
            HttpServerInfoCache.update_ipv4_address(4,'192.168.1.105')

            HttpServerInfoCache.update_ipv6_address(5,'2:2:2::2')
            HttpServerInfoCache.update_ipv6_address(6,'2:2:2::3')
            HttpServerInfoCache.update_ipv6_address(7,"5:2:2::3")
            HttpServerInfoCache.update_ipv6_address(8,"5:4:2::3")

            #mutiple ipv6 addr
            HttpServerInfoCache.update_ipv6_address(9,"5:4:2::9")
            HttpServerInfoCache.update_ipv6_address(9,"5:4:2::10")

            HttpServerInfoCache.update_ipv6_address(11,"5:4:2::11")
            HttpServerInfoCache.update_ipv6_address(11,"5:4:2::12")

            ResourceStoreManager.set_db(motor.MotorClient('localhost', 27017, io_loop=self.io_loop).fbt_test)
            yield ResourceStoreManager.clear_db()

            # user 2,3,4 uploaded the same file
            yield ResourceStoreManager.user_upload_resource(2,"user2",mock_hash("file_hash2"),"test2.txt",1023,1,["tag1","tag2"],0,1,3,"user2 uploaded file")
            yield ResourceStoreManager.user_upload_resource(3,"user3",mock_hash("file_hash2"),"test2.txt",1023,1,["tag1","tag2"],0,1,3,"user3 uploaded file")
            yield ResourceStoreManager.user_upload_resource(4,"user4",mock_hash("file_hash2"),"test2.txt",1023,1,["tag1","tag2"],0,1,3,"user4 uploaded file")
            yield ResourceStoreManager.user_upload_resource(10,"user10",mock_hash("file_hash2"),"test2.txt",1023,1,["tag1","tag2"],0,1,3,"user10 uploaded file")

            # user 3,4 uploaded the same file
            yield ResourceStoreManager.user_upload_resource(3,"user3",mock_hash("file_hash3"),"test4.txt",1023,1,["tag1","tag2"],0,1,3,"user3 uploaded file")
            yield ResourceStoreManager.user_upload_resource(4,"user4",mock_hash("file_hash3"),"test4.txt",1023,1,["tag1","tag2"],0,1,3,"user4 uploaded file")

            # user 5,6,7 uploaded the same file
            yield ResourceStoreManager.user_upload_resource(5,"user5",mock_hash("file_hash5"),"test5.txt",10123,1,["tag1","tag2"],0,1,3,"user5 uploaded file")
            yield ResourceStoreManager.user_upload_resource(6,"user6",mock_hash("file_hash5"),"test5.txt",10123,1,["tag1","tag2"],0,1,3,"user6 uploaded file")
            yield ResourceStoreManager.user_upload_resource(7,"user7",mock_hash("file_hash5"),"test5.txt",10123,1,["tag1","tag2"],0,1,3,"user7 uploaded file")

            yield ResourceStoreManager.user_upload_resource(11,"user11",mock_hash("file_hash6"),"test6.txt",1023,1,["tag1","tag2"],0,1,3,"user10 uploaded file")
            # res_users = yield ResourcesCache.get_resource_owners(mock_hash("file_hash2"))
            # print "res_users:",res_users

            ret1=yield DownloadMedium.get_online_file_owner(1,mock_hash("file_hash2"))
            self.assertEqual(ret1, {'download_type': DownloadMedium.download_type["V4_NAT"],
                                                                            'owners': [{'host': '2.2.2.2', 'port': 8884, 'uid': 2},
                                                                                       {'host': '2.2.2.2', 'port': 8884, 'uid': 3},
                                                                                        {'host': '2.2.2.2', 'port': 8884, 'uid': 4},
                                                                                        {'host': '2.2.2.3', 'port': 8884, 'uid': 10}]})
            #v4 user can't download v6 user's resource
            ret1=yield DownloadMedium.get_online_file_owner(1,mock_hash("file_hash6"))
            self.assertEqual(ret1, {'download_type': DownloadMedium.download_type["V4_NAT"],
                                                                            'owners': []})

            #test remove an resource owner
            yield ResourceStoreManager.remove_owner(mock_hash("file_hash2"),10)
            #yield ResourceStoreManager.remove_owner(mock_hash("file_hash2"),3)
            ret1=yield DownloadMedium.get_online_file_owner(1,mock_hash("file_hash2"))
            self.assertEqual(ret1, {'download_type': DownloadMedium.download_type["V4_NAT"],
                                                                            'owners': [{'host': '2.2.2.2', 'port': 8884, 'uid': 2},
                                                                                       {'host': '2.2.2.2', 'port': 8884, 'uid': 3},
                                                                                        {'host': '2.2.2.2', 'port': 8884, 'uid': 4}]})

            # ipv4 user download the ipv6 user resource
            ret2=yield DownloadMedium.get_online_file_owner(1,mock_hash("file_hash5"))
            self.assertEqual(ret2, {'download_type': DownloadMedium.download_type["V4_NAT"], 'owners':  []})

            # test download file not exist
            ret3=yield DownloadMedium.get_online_file_owner(1,mock_hash("file_hash_not_exist"))
            self.assertEqual(ret3, {'download_type': DownloadMedium.download_type["None"], 'owners': []})

            ret4=yield DownloadMedium.get_online_file_owner(8,mock_hash("file_hash_not_exist"))
            self.assertEqual(ret4, {'download_type': DownloadMedium.download_type["None"], 'owners': []})

            user_off_line=11111
            ret30=yield DownloadMedium.get_online_file_owner(user_off_line,mock_hash("file_hash2"))
            self.assertEqual(ret30, {'download_type': DownloadMedium.download_type["None"], 'owners': []})

            # ipv6 user download ivp4 user resource
            ret5=yield DownloadMedium.get_online_file_owner(8,mock_hash("file_hash2"))
            self.assertEqual(ret5, {'owners': [], 'download_type': DownloadMedium.download_type["V6"]})

            #test LAN download
            ret6=yield DownloadMedium.get_online_file_owner(2,mock_hash("file_hash3"))
            self.assertEqual(ret6, {'download_type': DownloadMedium.download_type["V4_LAN"], "owners": [{"uid": 3, "host": '192.168.1.104', "port":8884},{"uid": 4, "host": '192.168.1.105', "port":8884}]})

            #test V6 download
            ret7=yield DownloadMedium.get_online_file_owner(8,mock_hash("file_hash5"))
            self.assertEqual(ret7, {'download_type': DownloadMedium.download_type["V6"], "owners":[{"uid": 5, "host": '2:2:2::2', "port":8886},{"uid": 6, "host": '2:2:2::3', "port":8886},{"uid": 7, "host": "5:2:2::3", "port":8886}]})

            #test mutiple v6 ip
            ret8=yield DownloadMedium.get_online_file_owner(9,mock_hash("file_hash6"))
            self.assertEqual(ret8, {'download_type': DownloadMedium.download_type["V6"], "owners": [{'host': '5:4:2::11', 'uid': 11, 'port': 8886}, {'host': '5:4:2::12', 'uid': 11, 'port': 8886}]})
            callback()

      func(callback=self.stop)
      self.wait()

if __name__ == '__main__':
    import unittest
    unittest.main()
__author__ = 'bone-lee'

from fb_rank_timer import  FBRankTimer
from fb_rank_manager import FBRankManager
from random import randint,uniform

def my_cmp(x,y):
    if x[1]==y[1]: return cmp(y[0],x[0])
    else: return cmp(y[1],x[1])
if __name__ == "__main__":
    import tornado.ioloop
    from datetime import datetime
    date = datetime.now()

    ioloop = tornado.ioloop.IOLoop()
    FBRankManager.initialize()
    FBRankTimer.set_test_data(20,date.hour,date.day,date.isoweekday())
    FBRankTimer.set_io_loop(ioloop)
    FBRankTimer.run()

    gold_ans=[]
    assert FBRankManager.get_weekly_top()==gold_ans
    assert FBRankManager.get_monthly_top()==gold_ans

    for i in range(1,50000):
        uid=randint(1,1000)
        delta_fb=uniform(-10,300) # a float in range -100,3000

        found=False
        delta_fb=int(delta_fb)
        for i,data in enumerate(gold_ans):
            if data[0]==uid:
                assert data[1]>=0
                if data[1]+delta_fb>0:
                    gold_ans[i]=(uid,data[1]+delta_fb)
                else:
                   gold_ans[i]=(uid,0)
                found=True
                break
        if not found:
            if delta_fb>=0:
                gold_ans.append((uid,delta_fb))

        FBRankManager.update_fb(uid,delta_fb)
        # gold_ans.sort(my_cmp)
        # ans2=list(FBRankManager.get_weekly_top())
        # ans2.sort(my_cmp)
        # if ans2!=gold_ans[:20]:
        #     print "error"
        #     print "got:",ans2
        #     print "want:",gold_ans[:20]
        #     raw_input("next")

    gold_ans.sort(my_cmp)
    # print(gold_ans[:20])
    # print FBRankManager.get_weekly_top()
    # print FBRankManager.get_monthly_top()
    ans2=list(FBRankManager.get_weekly_top())
    ans2.sort(my_cmp)
    assert FBRankManager.get_weekly_top()==FBRankManager.get_monthly_top()
    if ans2!=gold_ans[:100]:
        print "error"
        print "got:",ans2
        print "want:",gold_ans[:100]
    else:
        print "pass test"
    try:
        ioloop.start()
    except KeyboardInterrupt:
        FBRankManager.save_info_to_file()
        print "I will exit..."
    #TODO FIXME write unit test

# coding: utf-8
__author__ = 'bone-lee'

import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.gen
import tornadoredis

pub_client = tornadoredis.Client()
channel="tornado"

class PostHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self):
        pub_client.publish(channel, self.get_argument("msg", "hello"))
        self.write("send OK. go to localhost:8890/get to see message list.")
        self.finish()

class WebApp(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/post', PostHandler),
        ]
        tornado.web.Application.__init__( self, handlers )

if __name__ == '__main__':
    application = WebApp()
    httpServer = tornado.httpserver.HTTPServer(application)
    httpServer.listen(8889)
    try:
        tornado.ioloop.IOLoop.instance().start()
    except:
        print "OK. Exit..."

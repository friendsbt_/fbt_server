__author__ = 'bone-lee'

from time import time
from datetime import datetime
from fb_rank_manager import FBRankManager

class FBRankTimer(object):
    _io_loop = None
    _TWO_HOUR= 2*3600
    _HOUR_AT_2 = 2
    _FIRST_DAY_OF_MONTH = 1
    _MONDAY = 1

    @classmethod
    def set_test_data(cls,two_hour_time,hour_at_two,first_day_of_month,monday):
        assert hour_at_two>0 and first_day_of_month>0 and two_hour_time>0 and monday>0
        cls._MONDAY=monday
        cls._TWO_HOUR=two_hour_time
        cls._HOUR_AT_2=hour_at_two
        cls._FIRST_DAY_OF_MONTH=first_day_of_month

    @classmethod
    def set_io_loop(cls, ioloop):
        cls._io_loop = ioloop

    @classmethod
    def run(cls):
        assert (cls._io_loop is not None)  # must set io loop
        #print "run...."
        cls._io_loop.add_timeout(time() + cls._TWO_HOUR, lambda: cls._reset_fb_rank())

    @classmethod
    def _reset_fb_rank(cls):
        #print "time up...."
        if cls._is_monday():
            #print "weekly time up...."
            FBRankManager.get_weekly_ranker().reset()
        if cls._is_first_day_of_month():
            #print "monthly time up...."
            FBRankManager.get_monthly_ranker().reset()
        # next tick
        cls._io_loop.add_timeout(time() + cls._TWO_HOUR, lambda: cls._reset_fb_rank())

    @classmethod
    def _is_monday(cls):
        '''
        if it is time to monday 2:00
        '''
        date = datetime.now()
        # check if weekday is monday
        #print date
        #print "weekday:",str(date.isoweekday())," hour:",str(date.hour)
        if date.isoweekday() == cls._MONDAY and date.hour == cls._HOUR_AT_2:
            return True
        else:
            return False

    @classmethod
    def _is_first_day_of_month(cls):
        '''
        if it is time to first day of month at 2:00
        '''
        date = datetime.now()
        #print date
        #print "day:",str(date.day)," hour:",str(date.hour)
        if date.day == cls._FIRST_DAY_OF_MONTH and date.hour == cls._HOUR_AT_2:
            return True
        else:
            return False
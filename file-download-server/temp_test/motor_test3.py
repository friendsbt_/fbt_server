#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'bone-lee'

import logging

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.gen

import motor

from tornado.options import define, options
define("port", default=8002, help="run on the given port", type=int)

class MainHandler(tornado.web.RequestHandler):
    client = motor.MotorClient('localhost', 27017)

    @tornado.gen.coroutine
    def outer(self):
        logging.info('outer starts')
        yield self.inner()
        yield self.inner()
        logging.info('outer ends')
        collection = MainHandler.client.test.test_collection
        #yield motor.Op(collection.remove)
        yield collection.remove()
        yield motor.Op(collection.insert, {'_id': 1, 'x': 17})
        #doc = yield motor.Op(collection.find_one, {'_id': 1}) #work
        #doc=yield motor.Op(self.inner_db_operation) #work
        doc=yield self.inner_db_operation() #work too
        #self.assertEqual(17, doc['x'])
        raise tornado.gen.Return('hello mongo:'+str(doc['x']))

    @tornado.gen.coroutine
    def inner(self):
        logging.info('inner runs')

    @tornado.gen.coroutine
    def inner_db_operation(self):
        collection = MainHandler.client.test.test_collection
        doc=yield motor.Op(collection.find_one, {'_id': 1})
        #logging.info('inner runs')
        raise tornado.gen.Return(doc)

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        res = yield self.outer()
        self.write(res)

if __name__ == "__main__":
    tornado.options.parse_command_line()
    app = tornado.web.Application(handlers=[(r"/", MainHandler)])
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
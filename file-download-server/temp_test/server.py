import json

from tornado.ioloop import IOLoop
from tornado.web import RequestHandler, Application, asynchronous
from tornado.httpclient import AsyncHTTPClient, HTTPError
from tornado import options, gen

import motor

###class FBHandler222(RequestHandler):
###
###    FB_GRAPH_ME_URL = 'https://www.gfsoso.com?q=1'
###
###    @asynchronous
###    def get(self):
###        self.fb_id = self.get_argument('fb_id',None)
###        fb_token = self.get_argument('fb_token',None)
###        url = self.FB_GRAPH_ME_URL.format(fb_token=fb_token)
###        http_client = AsyncHTTPClient()
###        try:
###            response = http_client.fetch(
###                request=url, method='GET', callback=self.on_fetch)
###        except HTTPError:
###            self.write('Fail')
###
###    def on_fetch(self, response):
###        '''
###        if json.loads(response.body).get('id') == self.fb_id:
###            self.write('Ok')
###        else:
###        '''
###        self.write('Fail')
###        self.finish()
###
###class FBHandler333(RequestHandler):
###
###    FB_GRAPH_ME_URL = 'https://graph.facebook.com/me?fields=id&access_token={fb_token}'
###
###    @gen.coroutine
###    def get(self):
###        fb_id = self.get_argument('fb_id')
###        fb_token = self.get_argument('fb_token')
###        url = self.FB_GRAPH_ME_URL.format(fb_token=fb_token)
###        http_client = AsyncHTTPClient()
###        try:
###            response = yield http_client.fetch(request=url, method='GET')
###            if json.loads(response.body).get('id') == fb_id:
###                self.write('Ok')
###                return
###            self.write('Fail')
###        except HTTPError:
###            pass
###        self.write('Fail')

class FBHandler(RequestHandler):

    @gen.coroutine
    def get(self):
        try:
            fb_id = self.get_argument('fb_id',None)
            fb_token = self.get_argument('fb_token',None)
            response = yield self.validate(fb_id=fb_id, fb_token=fb_token)
            self.write(response)
            #self.write('Ok')
        except Exception:
            self.write('Fail')

    @gen.coroutine
    def validate(self, fb_id, fb_token):
        FB_GRAPH_ME_URL = 'http://www.gfsoso.com/?q=1'

        url = FB_GRAPH_ME_URL.format(fb_token=fb_token)
        http_client = AsyncHTTPClient()
        response = yield http_client.fetch(request=url, method='GET')
        ###assert json.loads(response.body).get('id') == fb_id
        raise gen.Return(True)

def test_thing(self):
client = motor.MotorClient('localhost', 27017, io_loop=self.io_loop)
client.open_sync()
  
@gen.engine
def f(callback):
collection = client.test.test_collection
yield motor.Op(collection.remove)
yield motor.Op(collection.insert, {'_id': 1, 'x': 17})
doc = yield motor.Op(collection.find_one, {'_id': 1})
self.assertEqual(17, doc['x'])
callback()
  
f(callback=self.stop)
self.wait()

if __name__ == '__main__':
    options.parse_command_line()
    application = Application(
        handlers=[
            (r'/', FBHandler),
        ],
        debug=True,
    )
    application.listen(8000)
    IOLoop.instance().start()

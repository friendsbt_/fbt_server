from tornado import gen
#import copy
import motor
from tornado.ioloop import IOLoop

class ResourcesCache(object):
    @classmethod
    def hi(cls):
      print "hello"

    db = motor.MotorClient().fbt

    @classmethod
    @gen.coroutine
    def test_motor(cls):
        #cursor = cls.db.all_resources.find()
        #return [res for res in (yield cursor.to_list())]
        cx = cls.db #motor.MotorClient(test.env.uri, io_loop=self.io_loop)
        collection = cx.motor_test
        future0 = collection.insert({'foo': 'bar'})
        future1 = collection.insert({'foo': 'bar'})
        yield [future0, future1]
        if (2 == (yield collection.find({'foo': 'bar'}).count())):
            print "pass test"
        else:
            print "failed test"


db = motor.MotorClient().test

from tornado import gen
@gen.coroutine
def insert_db():
    yield db.users.insert({'name': 'Randall',"age":23})
    yield db.users.insert({'name': 'Jesse',"age":34})
    doc = yield db.users.find_one()
    print "doc:",doc
    print "doc count:",len(doc)


@gen.coroutine
def do_insert():
    for i in range(20):
        future = db.test_collection.insert({'i': i})
        result = yield future

@gen.coroutine
def do_find_one():
     document = yield db.test_collection.find_one({'i': {'$lt': 2}})
     print document

IOLoop.current().run_sync(insert_db)
IOLoop.current().run_sync(ResourcesCache.test_motor)

IOLoop.current().run_sync(do_insert)
IOLoop.current().run_sync(do_find_one)

#loop = IOLoop()

def got_user(user, error):
    if error:
        print 'error getting user!', error
    else:
        print "got user:",user

IOLoop.current().run_sync(lambda :db.users.find_one({'name': 'Jesse'}, callback=got_user))

def got_users(users, error):
    if error:
        print 'error getting users!', error
    else:
        for user in users:
            print "????",user

IOLoop.current().run_sync(lambda: db.users.find().to_list(length=10, callback=got_users))

import time
time.sleep(2)
print "sleep over"

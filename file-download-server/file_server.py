#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path

import json
# import uuid
from  time import  time
import ipaddress as IP
from user_ip_cache import UserIPCache
from resource_manager import ResourceStoreManager
from http_server_info_cache import HttpServerInfoCache
from download_medium import DownloadMedium
from resource_type import ResourceType
#from cipher import XorCipher
from cipher import WaveCipher as Cipher
from fb_manager import FBCoinManager
from  users_manager import  FbtUserStoreManager
from fb_rank_timer import FBRankTimer
from fb_rank_manager import FBRankManager

from tornado.options import define, options
import tornado.gen
import motor

define("port", default=8888, help="run on the given port", type=int)
db = motor.MotorClient().fbt#motor.MotorClient().open_sync().fbt

class Application(tornado.web.Application):
    def __init__(self):
        ResourceStoreManager.set_db(db)
        FBCoinManager.set_db(db)
        FbtUserStoreManager.set_db(db)
        handlers = [
            (r"/", HomeHandler),
            (r"/sns_share", SnsShareHandler),
            (r"/home", HomeHandler),
            (r"/registration", RegistrationHandler),
            (r"/login", LoginHandler),
            (r"/logout", LogoutHandler),
            (r"/request_fb_coin", RequestMyFBCoinHandler),

            (r"/fb_rank", FBCoinRankHandler),
            (r"/total_fb_rank", FBCoinTotalRankHandler),

            (r"/upload_resource", ResourceUploaHandler),
            (r"/view_resource", ResourceViewHandler),
            (r"/view_resource_download", ResourceDownloadViewHandler),
            (r"/comment_resource", CommentResourceHandler),
            (r"/score_resource", ScoreResourceHandler),
            (r"/tip_off_resource", TipOffResourceHandler),
            (r"/download_resource", ResourceDownloadHandler),
            (r"/download_over", ResourceDownloadOverHandler),
            (r"/remove_upload", ResourceRemoveHandler), #TODO add to fbt client
            (r"/resource404", Resource404Handler),
            (r"/search_resource", ResourceSearchHandler),
            (r"/view_user_resource", UserResourceViewHandler),
            (r"/view_friends_resources", FriendsResourceViewHandler),

            #(r"/request_ip", GetPublicIPHandler),
            (r"/report_http_server_info", HttpServerInfoHandler),

            (r"/websocket", FbtWebSocketHandler),

            #TODO FIXME remove
            (r"/debug/users", UsersListHandler),
            (r"/debug/http_server_info", ViewHttpServerInfoHandler),
            (r"/debug/view_my_resource", UserResourceViewHandler),
            (r"/debug/view_resource", ResourceViewDebugHandler),
        ]
        settings = dict(
            cookie_secret="123",
            # "__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
        )
        tornado.web.Application.__init__(self, handlers, **settings)

class SnsShareHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        # set cookie just for mock
        uid = self.get_argument("uid", None)  # mock user since node client has no cookie
        if uid:
            try:
                print "sns share uid:",uid
                uid = long(uid)
                yield FBCoinManager.sns_share_ok(uid)
                msg = {"err": 0}
            except:
                msg = {"err": 1, "what": "uid error"}
        self.write(json.dumps(msg)) 

class RegistrationHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        # set cookie just for mock
        uid = self.get_argument("user", None)  # mock user since node client has no cookie
        try:
            uid =  long(uid) #TODO FIXME
            yield FBCoinManager.register_ok(uid)
            self.write(json.dumps({"err": 0}))
        except:
            self.write(json.dumps({"err": 1, "what": "no user argument"}))

class RequestMyFBCoinHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        # set cookie just for mock
        uid = self.get_argument("user", None)  # mock user since node client has no cookie
        try:
            uid =  long(uid)
            yield FBCoinManager.register_ok(uid) #TODO pass me just mock user register
            fb_coin=yield FBCoinManager.get_user_total_fb_coin(uid)
            self.write(json.dumps({"err": 0, "how_many": fb_coin}))
        except:
            self.write(json.dumps({"err": 1, "what": "no user argument"}))

class FBCoinTotalRankHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        rank=yield FBCoinManager.get_total_rank()
        self.write(json.dumps({"err": 0, "rank": rank}))

class FBCoinRankHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        is_rank_by_week = self.get_argument("by_week", 1)
        try:
            is_rank_by_week=int(is_rank_by_week)
            assert is_rank_by_week==0 or is_rank_by_week==1
        except:
            self.write(json.dumps({"err": 1, "what": "invalid argument"}))
        if is_rank_by_week:
            ans=FBRankManager.get_weekly_top()
        else:
            ans=FBRankManager.get_monthly_top()
        self.write(json.dumps({"err": 0, "rank": ans}))

class HomeHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        # set cookie just for mock
        uid = self.get_argument("uid", None)  # mock user since node client has no cookie
        if uid:
            try:
                uid = long(uid)
                yield FBCoinManager.sns_share_ok(uid)
            except:
                pass #pass error
        self.render("home.html")



#class GetPublicIPHandler(tornado.web.RequestHandler):
#    def get(self):
#        self.write(json.dumps({"err": 0, "ip": self.request.remote_ip}))


# ---------------------------------------------------------
# just a mock of login and logout
# !!!pass me!!! if you are merge the code
class LoginHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_argument("user", None)
        if user:
            self.set_secure_cookie("fbt_user", user)
            # set cookie just for mock

            #!!! record the user IP
            #!!!-----------------------------------
            #UserIPCache.update_my_ip(user,self.request.remote_ip)

            #return the page to open websockeGt
            uid=1234 #TODO FIXME
            fb_coin=yield FBCoinManager.get_user_total_fb_coin(uid)
            #TODO render fb_coin in user page

            self.render("open_websocket.html", fbt_user=user)

            #-----------------------------------!!!

            #self.write(json.dumps({"err":0}))
        else:
            self.write(json.dumps({"err": 1, "what": "no user argument"}))


class LogoutHandler(tornado.web.RequestHandler):
    def get(self):
        #!!!-----------------------------------
        #user=get_current_user(self)
        #UserIPCache.delete_my_ip(user)
        #-----------------------------------!!!

        self.clear_cookie("fbt_user")
        self.write("You are now logged out")


#---------------------------------------------------------


def get_current_user(handler):
    user_json = handler.get_secure_cookie("fbt_user")
    if not user_json:
        return None
    else:
        return user_json


#class OpenSocketHandler(tornado.web.RequestHandler):
#    # socket open and client http server are in the page
#    def get(self):
#        self.render("open_websocket.html")

class ResourceUploaHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        #user is uid
        user = get_current_user(self)
        if not user:
            user = self.get_argument("user", None)  # mock user since node client has no cookie
        if user:
            try:
                uid = long(user)
            except ValueError:
                err = json.dumps({"err": 4, "what": "uid invalid"})
                self.write(err)
                return
            #TODO
            user_name = "TODO FIXME"  # get the real user name from DB

            file_name = self.get_argument("file_name", None)
            file_hash = self.get_argument("file_hash", None)
            tags = self.get_argument("tags", None)
            try:
                main_type = int(self.get_argument("main_type", 0))
                sub_type = int(self.get_argument("sub_type", 0))
                res_grade = float(self.get_argument("res_grade", 0))
                file_size = int(self.get_argument("file_size", None))
            except ValueError:
                err = json.dumps({"err": 3, "what": "main_type sub_type or res_grade invalid"})
                self.write(err)
                return
            if (not ResourceType.is_valid_main_type(main_type)) or (not ResourceType.is_valid_sub_type(sub_type)):
                err = json.dumps({"err": 4, "what": "main_type sub_type invalid range"})
                self.write(err)
                return
            comment = self.get_argument("comment", None)
            try:
                is_public = int(self.get_argument("is_public", 1))  #default is public
                file_hash = int(file_hash)
            except ValueError:
                err = json.dumps({"err": 5, "what": "is_public or file_hash invalid"})
                self.write(err)
                return
            #decrypt
            file_name=Cipher.decrypt(file_name).strip()
            tags=Cipher.decrypt(tags).strip()
            comment=Cipher.decrypt(comment).strip()

            if file_name and file_hash and file_size and tags and \
                    comment:
                tags_list =[tag.strip() for tag in tags.split(" ") if len(tag.strip())>0]
                if (len(tags_list) > 0):
                    yield  ResourceStoreManager.user_upload_resource(uid, user_name,
                                                        file_hash,
                                                        file_name, file_size,
                                                        is_public,
                                                        tags_list,
                                                        main_type, sub_type,
                                                        res_grade,
                                                        comment)
                    # yield FBCoinManager.upload_public_resource_ok(uid,file_hash)
                    ok = json.dumps({"err": 0})
                    self.write(ok)
                else:
                    err = json.dumps({"err": 2, "what": "tags or blocks hash invalid"})
                    self.write(err)
            else:
                err = json.dumps({"err": 1, "what": "argument missing error"})
                self.write(err)


class ResourceDownloadHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        #user is uid
        #TODO download the resource if he can view it
        user = get_current_user(self)
        if not user:
            user = self.get_argument("user", None)  # mock user since node client has no cookie
        is_private = self.get_argument("private", 0)  # private download
        try:
            uid = long(user)
            is_private = int(is_private)
        except:
            err = json.dumps({"err": 4, "what": "uid invalid"})
            self.write(err)
            return
        file_hash = int(self.get_argument("file_hash", None))  #default is public
        if not file_hash:
            err = json.dumps({"err": 5, "what": "file hash err"})
            self.write(err)
            return
        if not is_private: #public download need fb coin
            user_fb_coins=yield FBCoinManager.get_user_total_fb_coin(uid)
            if user_fb_coins < FBCoinManager.public_resource_download_coin:
                ok = json.dumps({"err": 1, "what": "fb coin not enough"})
                self.write(ok)
                return
            yield FBCoinManager.add_to_public_download_queue(uid,file_hash)

        res_header = yield ResourceStoreManager.get_resource_header(file_hash)
        online_owners=yield DownloadMedium.get_online_file_owner(uid,file_hash)
        download_type= online_owners["download_type"]
        if download_type == DownloadMedium.download_type["V4_NAT"]:
            nat_users=[owner["uid"] for owner in online_owners["owners"]]
            if len(nat_users)>0:
                FbtWebSocketHandler.notify_open_file_server(nat_users, uid, file_hash)
        ok = json.dumps({"err": 0, "file_info": res_header, "owners": online_owners["owners"], "download_type": download_type})
        self.write(ok)

class ResourceDownloadOverHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = get_current_user(self)
        if not user:
            user = self.get_argument("user", None)  # mock user since node client has no cookie
        try:
            uid = long(user)
            file_hash = int(self.get_argument("file_hash", None))  #default is public
            assert file_hash>0
            users_downloaded_from=[int(u) for u in self.get_argument("from","").split(',')]
            assert len(users_downloaded_from)>0
            for u in users_downloaded_from:
                assert u>0
        except:
            err = json.dumps({"err": 4, "what": "argument invalid"})
            self.write(err)
            return
        yield ResourceStoreManager.add_owner_when_download_over(file_hash,uid)
        yield ResourceStoreManager.increase_download_num(file_hash)
        yield FBCoinManager.public_resource_download_ok(uid,file_hash,users_downloaded_from)
        ok = json.dumps({"err": 0})
        self.write(ok)

class ResourceRemoveHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        #user is uid
        user = get_current_user(self)
        if not user:
            user = self.get_argument("user", None)  # mock user since node client has no cookie
        try:
            uid = long(user)
            file_hash = long(self.get_argument("file_hash", None))
        except:
            err = json.dumps({"err": 1, "what": "uid or file_hash invalid"})
            self.write(err)
            return
        yield ResourceStoreManager.remove_owner(file_hash,uid)
        # yield FBCoinManager.delete_public_resource_ok(uid,file_hash)
        ok = json.dumps({"err": 0})
        self.write(ok)

class Resource404Handler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_argument("user", None)  # mock user since node client has no cookie
        try:
            uid = long(user)
            file_id = self.get_argument("file_id", "")
            assert len(file_id)>0
        except:
            err = json.dumps({"err": 1, "what": "uid or file_id invalid"})
            self.write(err)
            return
        yield ResourceStoreManager.remove_owner(file_id,uid)
        ok = json.dumps({"err": 0})
        self.write(ok)

class CommentResourceHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        #user is uid
        user = get_current_user(self)
        if not user:
            user = self.get_argument("user", None)  # mock user since node client has no cookie
        try:
            uid = long(user)
        except:
            err = json.dumps({"err": 4, "what": "uid invalid"})
            self.write(err)
            return
        user_name = "TODO FIXME"  # get the real user name from DB
        file_hash = int(self.get_argument("file_hash", None))  #default is public
        if not file_hash:
            err = json.dumps({"err": 5, "what": "file hash err"})
            self.write(err)
            return
        comment = self.get_argument("comment", None)  #default is public
        comment=Cipher.decrypt(comment)
        if not comment:
            err = json.dumps({"err": 6, "what": "comment err"})
            self.write(err)
            return
        yield ResourceStoreManager.add_comment(file_hash,uid,user_name,comment)
        ok = json.dumps({"err": 0})
        self.write(ok)

class ScoreResourceHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        #user is uid
        user = get_current_user(self)
        if not user:
            user = self.get_argument("user", None)  # mock user since node client has no cookie
        try:
            uid = long(user)
        except:
            err = json.dumps({"err": 4, "what": "uid invalid"})
            self.write(err)
            return
        file_hash = int(self.get_argument("file_hash", None))  #default is public
        if not file_hash:
            err = json.dumps({"err": 5, "what": "file hash err"})
            self.write(err)
            return
        score = self.get_argument("score", None)  #default is public
        try:
            score=float(score)
            if score < 0 or score > 10:
                err = json.dumps({"err": 7, "what": "score format err"})
                self.write(err)
                return
        except:
            err = json.dumps({"err": 6, "what": "score format err"})
            self.write(err)
            return
        yield ResourceStoreManager.add_score(file_hash,uid, score)
        ok = json.dumps({"err": 0})
        self.write(ok)

class TipOffResourceHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = get_current_user(self) #TODO FIXME
        if not user:
            user = self.get_argument("user", None)  # mock user since node client has no cookie
        file_hash = self.get_argument("file_hash", None)  #default is public
        try:
            uid = long(user)
            file_hash = long(file_hash)
            assert file_hash>0
            assert uid>0
        except:
            err = json.dumps({"err": 4, "what": "argument invalid"}) #TODO FIXME
            self.write(err)
            return
        yield ResourceStoreManager.tip_off(file_hash,uid)
        ok = json.dumps({"err": 0}) #TODO FIXME
        self.write(ok)

def cipher_resource_list(resource_list):
    #a comment: {"who": user_name, "uid": uid, "content": comment, "ctime": long(time())})
    #all comments: ret[file_hash]['comments'] = cls._all_resources[k]['comments']
    #comments is a list
    for resource in resource_list:
        for comment in resource['comments']:
            comment['content']=Cipher.encrypt(comment['content'])
        resource['tags']=Cipher.encrypt(resource['tags'])
        resource['file_name']=Cipher.encrypt(resource['file_name'])

def add_download_info_to_resource_list(uid,resource_list):
   assert uid>0
   for resource in resource_list:
       res_download_cnt = len(set([owner['uid'] for owner in DownloadMedium.get_matched_online_owners(uid,resource['owners'])["owners"]]))
       resource["online_owners_num"] = res_download_cnt

class ResourceViewHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        """ by type{by hot,by latest},by page """
        user = get_current_user(self)
        if not user:
            user = self.get_argument("user", None)
        page = self.get_argument("page", 1)
        sort_by = self.get_argument("sort_by", ResourceStoreManager.res_sort_by["download_num"])
        res_type = self.get_argument("res_type", ResourceType.get_main_index_by_type("电影"))
        try:
            user=long(user)
            page = int(page)
            assert page>=0
            sort_by = int(sort_by)
            assert sort_by==ResourceStoreManager.res_sort_by["time"]or sort_by==ResourceStoreManager.res_sort_by["download_num"]
            res_type = int(res_type)
            assert ResourceType.is_valid_main_type(res_type)
        except:
            err = json.dumps({"err": 1, "what": "argument type error."})
            self.write(err)
            return
        RES_CNT_IN_A_PAGE=20
        resource_list = yield ResourceStoreManager.get_resources_overview(res_type,page,sort_by,RES_CNT_IN_A_PAGE)
        cipher_resource_list(resource_list)
        add_download_info_to_resource_list(user,resource_list)
        resource_data = json.dumps({"err": 0, "resource_list": resource_list})
        self.write(resource_data)

class ResourceDownloadViewHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        """ by file hashes"""
        user = get_current_user(self)
        if not user:
            user = self.get_argument("user", None)
        hashes = self.get_argument("file_hashes", "")
        try:
            user=long(user)
            file_hash_list=[long(file_hash) for file_hash in hashes.split()]
            assert len(file_hash_list)>0
        except:
            err = json.dumps({"err": 1, "what": "argument type error."})
            self.write(err)
            return
        resource_list = yield ResourceStoreManager.get_resources_by_hashes(file_hash_list)
        cipher_resource_list(resource_list)
        add_download_info_to_resource_list(user,resource_list)
        resource_data = json.dumps({"err": 0, "resource_list": resource_list})
        self.write(resource_data)

class SetEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, set):
                return list(obj)
            #if isinstance(obj, Something):
            #   return 'CustomSomethingRepresentation'
            return json.JSONEncoder.default(self, obj)

class UserResourceViewHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        #user is uid
        page = self.get_argument("page", 1)
        friend_uid = self.get_argument("friend", None)
        sort_by = self.get_argument("sort_by", ResourceStoreManager.res_sort_by["download_num"])
        user = get_current_user(self)
        if not user:
            user = self.get_argument("user", None)
        try:
            user = long(user)
            friend_uid= long(friend_uid)
            page = int(page)
            assert page>=0
            sort_by = int(sort_by)
            assert sort_by==ResourceStoreManager.res_sort_by["time"]or sort_by==ResourceStoreManager.res_sort_by["download_num"]
        except:
            err = json.dumps({"err": 1, "what": "argument type error."})
            self.write(err)
            return
        RES_CNT_IN_A_PAGE=20 #maybe this should be large
        resource_list = yield ResourceStoreManager.get_my_resource(friend_uid,page,sort_by,RES_CNT_IN_A_PAGE)
        cipher_resource_list(resource_list)
        add_download_info_to_resource_list(user,resource_list)
        resource_data = json.dumps({"err": 0, "resource_list": resource_list})
        self.write(resource_data)

class FriendsResourceViewHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        page = self.get_argument("page", 1)
        user = get_current_user(self)
        if not user:
            user = self.get_argument("user", None)
        try:
            user = long(user)
            page = int(page)
            assert page>=1
        except:
            err = json.dumps({"err": 1, "what": "argument type error."})
            self.write(err)
            return
        RES_CNT_IN_A_PAGE=20
        friends_list = yield FbtUserStoreManager.get_friend_list(user)
        resource_list = yield ResourceStoreManager.get_private_resources(friends_list,page,RES_CNT_IN_A_PAGE)
        cipher_resource_list(resource_list)
        add_download_info_to_resource_list(user,resource_list)
        resource_data = json.dumps({"err": 0, "resource_list": resource_list})
        self.write(resource_data)

class ResourceViewDebugHandler(tornado.web.RequestHandler):
    def get(self):
        resource_list = yield ResourceStoreManager.get_resources_list()
        resource_data = json.dumps({"err": 0, "resource_list": resource_list}, cls=SetEncoder)
        self.write(resource_data)


class HttpServerInfoHandler(tornado.web.RequestHandler):
    def get(self):
        user = get_current_user(self)
        if not user:
            user = self.get_argument("user", None)  # mock user since node client has no cookie
            user = Cipher.decrypt(user)
        if user:
            try:
                user=long(user)
            except ValueError:
                err = json.dumps({"err": 5, "what": "invalid uid"})
                self.write(err)
                return
            ip = self.get_argument("ip", None)
            port = self.get_argument("port", None)
            if ip and port:
                ip = Cipher.decrypt(ip)
                port = Cipher.decrypt(port)
                if port == '8884' and IP.is_valid_ipv4_address(ip):
                    HttpServerInfoCache.update_ipv4_address(user, ip)
                    ok = json.dumps({"err": 0})
                    self.write(ok)
                elif port == '8886' and IP.is_valid_ipv6_address(ip):
                    HttpServerInfoCache.update_ipv6_address(user, ip)
                    ok = json.dumps({"err": 0})
                    self.write(ok)
                else:
                    err = json.dumps({"err": 2, "what": "port number err or ip address format err. ip:"+ip+" port:"+port})
                    self.write(err)
            else:
                err = json.dumps({"err": 3, "what": "no ip or port"})
                self.write(err)
        else:
            err = json.dumps({"err": 1, "what": "login first"})
            self.write(err)


class ResourceSearchHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        """ by type{by hot,by latest},by page """
        user = get_current_user(self)
        if not user:
            user = self.get_argument("user", None)
        page = self.get_argument("page", 1)
        sort_by = self.get_argument("sort_by", ResourceStoreManager.res_sort_by["time"])
        key_word = self.get_argument("key_word", "")
        try:
            user=long(user)
            page = int(page)
            assert page>0
            sort_by = int(sort_by)
            assert sort_by==ResourceStoreManager.res_sort_by["time"]or sort_by==ResourceStoreManager.res_sort_by["download_num"]
            key_word=Cipher.decrypt(key_word).strip()
            assert len(key_word)>0
        except:
            err = json.dumps({"err": 1, "what": "argument type error."})
            self.write(err)
            return
        RES_CNT_IN_A_PAGE=20
        resource_list = yield ResourceStoreManager.search_resources(key_word,page,sort_by,RES_CNT_IN_A_PAGE)
        cipher_resource_list(resource_list)
        add_download_info_to_resource_list(user,resource_list)
        resource_data = json.dumps({"err": 0, "resource_list": resource_list})
        self.write(resource_data)

class UsersListHandler(tornado.web.RequestHandler):
    def get(self):
        user_ip_list = UserIPCache.get_user_ip_list()
        self.render("user_ip_list.html", title="user ip list", items=user_ip_list)


class ViewHttpServerInfoHandler(tornado.web.RequestHandler):
    '''
    this will see how many user are online.
    '''

    def get(self):
        http_server_info = HttpServerInfoCache.get_server_info()
        self.render("http_server_info.html", server_info=http_server_info)


#-------------------------------------------------------------------------------
# when user login, open web socket to interact with server.
# what if the same ip behind different LAN network???
#-------------------------------------------------------------------------------
class FbtWebSocketHandler(tornado.websocket.WebSocketHandler):
    '''
    Just used for user online detection. I keep it as simple as possible.
    '''
    waiters = dict()  #all online users are here
    user_online_at = dict()

    def open(self):
        logging.info("websocket address:" + self.request.remote_ip)

    @tornado.gen.coroutine
    def on_close(self):
        user_to_del = None
        for user, connection in FbtWebSocketHandler.waiters.iteritems():
            if connection == self:
                user_to_del = user
                logging.info("detect websocket close:" + str(user_to_del))
                break
        if user_to_del:
            del FbtWebSocketHandler.waiters[user_to_del]
            UserIPCache.delete_my_ip(user_to_del)
            yield FBCoinManager.user_online_ok(user_to_del,FbtWebSocketHandler.user_online_at[user_to_del],long(time()))
            del FbtWebSocketHandler.user_online_at[user_to_del]

    def on_message(self, message):
        logging.info("got message %r", message)
        json_data = json.loads(message)
        if "uid" in json_data:
            user = json_data["uid"]
            logging.info("got message from uid:%r", user)
            try:
                user=long(user)
                FbtWebSocketHandler.user_online_at[user]=long(time())
                UserIPCache.update_my_ip(user, self.request.remote_ip)  #!!! record the user IP
                FbtWebSocketHandler.waiters[user] = self
                self.write_message(json.dumps({"err": 0, "ip": self.request.remote_ip}))
            except ValueError:
                self.write_message(json.dumps({"err": 2, "what": "uid is not number"}))
        else:
            self.write_message(json.dumps({"err": 1, "what": "json format error"}))

    @classmethod
    def notify_open_file_server(cls, user_ids, for_who, file_hash):
        assert len(user_ids)>0
        assert len(file_hash)>0
        assert for_who>=0
        OPEN_UDP_SERVER=1
        for uid in user_ids:
            if uid in cls.waiters:
                try:
                    cls.waiters[uid].write_message(json.dumps({"message_type": OPEN_UDP_SERVER, "file_hash": file_hash, "what": "open udp server","for": for_who}))
                except:
                    logging.debug("Warning: writing open udp server message failed. the user has left."+uid)
                    # del cls.waiters[uid]
                    # UserIPCache.delete_my_ip(uid)
#-------------------------------------------------------------------------------

def main():
    tornado.options.parse_command_line()
    app = Application()
    app.listen(options.port)
    ioloop=tornado.ioloop.IOLoop.instance()
    from datetime import datetime
    date = datetime.now()
    FBRankTimer.set_test_data(20,date.hour,date.day,date.isoweekday())
    FBRankTimer.set_io_loop(ioloop)
    FBRankTimer.run()
    print "running..."
    ioloop.start()

if __name__ == "__main__":
    main()

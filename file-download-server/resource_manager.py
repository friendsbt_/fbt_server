__author__ = 'bone-lee'

from time import time
from tornado import gen
# import copy
from resource_type import ResourceType
import motor
import operator
from fb_manager import FBCoinManager

def gen_file_id(file_hash,file_size):
    assert file_size>0
    return str(file_hash)+"_"+str(file_size)

class ResourceStoreManager(object):
    '''
    memory resources
    # a resource in all_resources DB such as
    #   {
    #   file_hash: file_hash1,
    #   file_name: xxx,
    #   main_type: 0~5, #see ResourceType
    #   sub_type: 0~3,
    #   file_size: 1234,
    #   mtime: 2014-3-4,
    #   tags: [tag1,tag2,...]
    #   owners: [{uid:uid1, is_public: 1},...]
    #   grades: [{uid:uid1, score: grade1},...]
    #   comments: [{who: user_name1, uid: uid1, content: the content of comment, ctime: 2014-4-3},...]
    #   download_num: 123
    #   },

    # a resource in resources_of_user DB such as
    #such as {uid : uid1, file_hashes : [file_hash1,file_hash2]}

    # a tag in tags DB such as
    #{tag : tag1, file_hashes : [file_hash1,file_hash2,...]} , ...}
    '''
    _db = motor.MotorClient('localhost', 27017).fbt
    res_sort_by = {"time": 0, "download_num": 1}

    _ASCENDING = 1
    _DESCENDING = -1

    @classmethod
    def set_db(cls, db):
        cls._db = db

    @classmethod
    @gen.coroutine
    def clear_db(cls):
        #motor.Op is deprecated
        yield cls._db.all_resources.remove()
        yield cls._db.resources_of_user.remove()
        yield cls._db.tags.remove()

    @classmethod
    @gen.coroutine
    def get_resources_list(cls):
        cursor = cls._db.all_resources.find({}, {"_id": 0})
        all_resources = yield cursor.to_list(None)
        res_list = [res for res in all_resources]
        raise gen.Return(res_list)

    @classmethod
    @gen.coroutine
    def get_my_resource(cls, uid, page, sort_by, max_resources_cnt_in_page):
        assert uid > 0
        assert page >= 1
        assert sort_by == cls.res_sort_by["time"] or sort_by == cls.res_sort_by["download_num"]
        assert max_resources_cnt_in_page > 0
        my_resource = yield cls._db.resources_of_user.find_one({'uid': uid})
        (start_index, end_index) = ((page - 1) * max_resources_cnt_in_page, page * max_resources_cnt_in_page)
        if my_resource and "file_hashes" in my_resource:
            ret = list()
            index = start_index
            to_remove=[] #resource DB delete some resource
            for file_hash in my_resource["file_hashes"][start_index:]:
                    res = yield cls._db.all_resources.find_one({'file_hash': file_hash})
                    if res:
                            one_resource = cls.extract_resource_from_db(res, True)
                            ret.append(one_resource)
                            index += 1
                            if index==end_index:
                                break
                    else:
                            to_remove.append(file_hash)
            if len(to_remove)>0: #remove unwanted file_hash
                    for file_hash in to_remove:
                        my_resource["file_hashes"].remove(file_hash)
                    yield cls._db.resources_of_user.save(my_resource)

            # TODO FIXME if equal, sort by mtime
            if sort_by == cls.res_sort_by["download_num"]:
                sort_cmp = lambda res1, res2: cmp(res2["download_num"], res1["download_num"])
            else:
                sort_cmp = lambda res1, res2: cmp(res2["mtime"], res1["mtime"])
            if len(ret):
                ret.sort(sort_cmp)
            raise gen.Return(ret)
        else:
            raise gen.Return([])


    @classmethod
    def extract_resource_from_db(cls, res, need_private_owner=False):
        one_resource = dict()
        one_resource['file_hash'] = res['file_hash']
        one_resource['file_name'] = res['file_name']
        one_resource['main_type'] = res['main_type']
        one_resource['sub_type'] = res['sub_type']
        one_resource['file_size'] = res['file_size']
        one_resource['mtime'] = res['mtime']
        one_resource['tags'] = " ".join(tag for tag in res['tags'])
        one_resource['grades'] = dict()
        map(lambda score1: operator.setitem(one_resource['grades'], score1['uid'], score1["score"]), res['scores'])
        total_score = \
            reduce(lambda score1, score2: {"score": score1['score'] + score2['score'], "uid": 0}, res['scores'])[
                "score"]
        one_resource['avg_grade'] = (total_score + 0.0) / len(res['scores'])
        one_resource['comments'] = res[
            'comments']  # copy.deepcopy(res['comments']) #need deep copy #make a copy is safe to prevent change the value externally
        if "download_num" not in res:
            res["download_num"] = 0
        one_resource['download_num'] = res['download_num']
        if need_private_owner:
            owners = [owner['uid'] for owner in res['owners']]  # yield cls.get_resource_owners(k)
        else:
            owners = [owner['uid'] for owner in res['owners'] if owner['is_public']]  # yield cls.get_resource_owners(k)
        one_resource['owners'] = owners
        one_resource['total_owners_num'] = len(owners)
        return one_resource

    @classmethod
    @gen.coroutine
    def get_resources_by_tag(cls, tag, page, max_resources_cnt_in_page):
        assert len(tag)>0
        assert page >0
        assert max_resources_cnt_in_page>0
        ret = list()
        (start_index, end_index) = ((page - 1) * max_resources_cnt_in_page, page * max_resources_cnt_in_page)
        index = start_index
        resource_hashes = yield cls._db.tags.find_one({"tag": tag})
        if resource_hashes and "file_hashes" in resource_hashes:
            to_remove=[]
            for file_hash in resource_hashes["file_hashes"][start_index:]:
                    assert file_hash > 0
                    # can't see the private resource and tip_off resource
                    # print "file_hash:",str(file_hash)
                    res = yield cls._db.all_resources.find_one({"hidden": {"$ne": 1}, "public": 1,'file_hash': file_hash})
                    if res:
                        one_resource = cls.extract_resource_from_db(res)
                        ret.append(one_resource)
                        index += 1
                        if index == end_index:
                            break
                    else:
                        to_remove.append(file_hash)
            if len(to_remove):
                # print "to_remove:",to_remove
                for file_hash in to_remove:
                    resource_hashes["file_hashes"].remove(file_hash)
                yield cls._db.tags.save(resource_hashes)
        raise gen.Return(ret)

    @classmethod
    @gen.coroutine
    def search_resources(cls, key_word, page, sort_by, max_resources_cnt_in_page):
        assert len(key_word) > 0
        assert page >= 1
        assert sort_by == cls.res_sort_by["time"] or sort_by == cls.res_sort_by["download_num"]
        assert max_resources_cnt_in_page > 0
        if ResourceType.is_valid_tag(key_word):
            ret=yield cls.get_resources_by_tag(key_word, page, max_resources_cnt_in_page)
            raise gen.Return(ret)
        else:
            ret = list()
            cursor = cls._db.all_resources.find(
                {"hidden": {"$ne": 1}, "public": 1, "file_name": {"$regex": key_word, '$options': '-i'}}).sort(
                [('mtime', cls._DESCENDING), ('download_num', cls._DESCENDING)])
            if sort_by == cls.res_sort_by["download_num"]:
                cursor = cls._db.all_resources.find(
                    {"hidden": {"$ne": 1}, "public": 1, "file_name": {"$regex": key_word, '$options': '-i'}}).sort(
                    [('download_num', cls._DESCENDING), ('mtime', cls._DESCENDING)])
            cursor = cursor.limit(max_resources_cnt_in_page).skip((page - 1) * max_resources_cnt_in_page)  #skip page-1
            #docs = yield cursor.to_list(length=RES_CNT_IN_A_PAGE)
            while (yield cursor.fetch_next):
                res = cursor.next_object()
                one_resource = cls.extract_resource_from_db(res)
                ret.append(one_resource)
            raise gen.Return(ret)

    @classmethod
    @gen.coroutine
    def get_private_resources(cls, friend_uid_list, page, max_resources_cnt_in_page):
        assert page >= 1
        # TODO FIXME
        # assert sort_by==cls.res_sort_by["time"] or sort_by==cls.res_sort_by["download_num"]
        assert max_resources_cnt_in_page > 0
        ret = list()
        (start_index, end_index) = ((page - 1) * max_resources_cnt_in_page, page * max_resources_cnt_in_page)
        index = 0
        for uid in friend_uid_list:
            assert uid > 0
            friend_resource = yield cls._db.resources_of_user.find_one({'uid': uid})
            if friend_resource and "file_hashes" in friend_resource:
                to_remove=[]
                for file_hash in friend_resource["file_hashes"]:
                    res = yield cls._db.all_resources.find_one({'file_hash': file_hash})
                    # print "file_hash:",str(file_hash)
                    if res: #is a valid resource
                        if index >= start_index:
                                one_resource = cls.extract_resource_from_db(res, True)
                                ret.append(one_resource)
                        index += 1
                        if index == end_index:
                            break
                    else:
                        to_remove.append(file_hash)
                if len(to_remove)>0: #remove unwanted file_hash that is not in resource DB
                    # print "to remove:",to_remove
                    for file_hash in to_remove:
                        friend_resource["file_hashes"].remove(file_hash)
                    yield cls._db.resources_of_user.save(friend_resource)
            if index == end_index:
                break
        raise gen.Return(ret)

    @classmethod
    @gen.coroutine
    def get_resources_overview(cls, res_type, page, sort_by, max_resources_cnt_in_page):
        assert ResourceType.is_valid_main_type(res_type)
        assert page >= 1
        assert sort_by == cls.res_sort_by["time"] or sort_by == cls.res_sort_by["download_num"]
        assert max_resources_cnt_in_page > 0
        ret = list()
        one_week_ago=long(time())-7*24*3600
        cursor = cls._db.all_resources.find({"hidden": {"$ne": 1}, "public": 1, "main_type": res_type, "mtime": {"$gt":one_week_ago}}).sort(
                    [('mtime', cls._DESCENDING), ('download_num', cls._DESCENDING)])
        #cursor = cls._db.all_resources.find({"hidden": {"$ne": 1}, "public": 1, "main_type": res_type}).sort(
        #    [('mtime', cls._DESCENDING), ('download_num', cls._DESCENDING)])
        if sort_by == cls.res_sort_by["download_num"]:
            cursor = cls._db.all_resources.find({"hidden": {"$ne": 1}, "public": 1, "main_type": res_type, "mtime": {"$gt":one_week_ago}}).sort(
                [('download_num', cls._DESCENDING), ('mtime', cls._DESCENDING)])
        cursor = cursor.limit(max_resources_cnt_in_page).skip((page - 1) * max_resources_cnt_in_page)  #skip page-1
        #docs = yield cursor.to_list(length=RES_CNT_IN_A_PAGE)
        while (yield cursor.fetch_next):
            res = cursor.next_object()
            one_resource = cls.extract_resource_from_db(res)
            ret.append(one_resource)
        raise gen.Return(ret)

    @classmethod
    @gen.coroutine
    def get_resources_by_hashes(cls, hashes):
        assert len(hashes) > 0
        ret = list()
        for file_hash in hashes:
            assert file_hash > 0
            res = yield cls._db.all_resources.find_one({'file_hash': file_hash})
            if res:
                one_resource = cls.extract_resource_from_db(res)
                ret.append(one_resource)
        raise gen.Return(ret)

    @classmethod
    @gen.coroutine
    def get_resource_owners(cls, file_hash):
        #owners: {uid1: is_public,uid2: is_private,...}
        assert file_hash > 0
        #res=yield motor.Op(cls.db.all_resources.find_one, {'file_hash': file_hash})
        res = yield cls._db.all_resources.find_one({'file_hash': file_hash})
        if res and "owners" in res:
            owners = [owner['uid'] for owner in res['owners']]  # if owner['is_public']] #
            raise gen.Return(owners)
        else:
            raise gen.Return([])

    @classmethod
    @gen.coroutine
    def get_resource_header(cls, file_hash):
        assert file_hash > 0
        res = yield cls._db.all_resources.find_one({'file_hash': file_hash})
        if res:
            header_info = dict()
            header_info['file_size'] = res['file_size']
            header_info['file_name'] = res['file_name']
            header_info['file_hash'] = file_hash
            raise gen.Return(header_info)
        else:
            raise gen.Return({})

    @classmethod
    @gen.coroutine
    def user_upload_resource(cls, uid,  #unique user id
                             user_name,  #user name registered
                             file_hash,  #file hash
                             #blocks_hash,#file blocks(4KB for each block) hash
                             file_name,  #file name
                             file_size,  #file size
                             is_public,  #is public share
                             tags,  #tags for resources
                             main_type,  #resource main type
                             sub_type,  #resource sub type
                             res_grade,  #resource grade by user
                             comment):  #user's comment
        assert file_hash > 0
        file_name = file_name.strip()
        assert len(file_name) > 0
        assert uid > 0
        assert main_type >= 0
        assert sub_type >= 0
        assert file_size > 0
        assert len(user_name) > 0
        assert is_public == 0 or is_public == 1
        assert res_grade >= 0 and res_grade <= 10
        comment = comment.strip()
        assert len(comment) > 0

        tags = [tag.strip() for tag in tags if len(tag.strip()) > 0]
        assert len(tags) > 0

        file_id=str(file_hash)+"_"+str(file_size)
        resource_header = {'file_hash': file_hash, 'file_name': file_name,
                           'file_id': file_id,
                           'main_type': main_type, 'sub_type': sub_type,
                           'file_size': file_size, 'mtime': long(time())}
        res = yield cls._db.all_resources.find_one({'file_id': file_id})
        if not res: #resource not in DB
            res_to_save=resource_header
            resource_comment = {"who": user_name, "uid": uid, "content": comment, "ctime": long(time())}
            res_to_save["comments"]=[resource_comment]

            score_doc = {"uid": uid, "score": res_grade}
            res_to_save['scores']=[score_doc]

            owner_doc = {"uid": uid, "is_public": is_public}
            res_to_save['owners'] = [owner_doc]
            res_to_save['public'] = is_public

            res_to_save["tags"]=[]
            for tag in tags:
                if len(tag) and (tag not in res_to_save["tags"]):
                    res_to_save["tags"].append(tag)
            yield cls._db.all_resources.insert(res_to_save)
        else:
            yield cls._update_resource_header(file_hash, resource_header)

            resource_comment = {"who": user_name, "uid": uid, "content": comment, "ctime": long(time())}
            yield cls._add_resource_comment(file_hash, resource_comment)

            yield cls._add_resource_score(file_hash, uid, res_grade)

            yield cls._add_resource_owner(file_hash, uid, is_public)

            yield cls._add_tags_for_resource(file_hash, tags)

        yield cls._add_resource_for_tag(file_hash, tags)
        yield cls._add_to_my_resource_list(uid, file_hash)

    @classmethod
    @gen.coroutine
    def add_owner_when_download_over(cls, file_hash, uid):
        assert (uid >= 0)
        assert (file_hash > 0)
        yield cls._add_to_my_resource_list(uid, file_hash)
        yield cls._add_resource_owner(file_hash, uid, None)

    @classmethod
    @gen.coroutine
    def remove_owner(cls, file_hash_or_id, uid):
        assert (uid >= 0)
        if isinstance(file_hash_or_id,unicode):
            file_id=file_hash_or_id.strip()
            assert len(file_id)>0
            yield cls._remove_resource_owner(file_id, uid)
        else:
            assert (file_hash_or_id > 0)
            file_hash=file_hash_or_id
            yield cls._remove_from_my_resource_list(uid, file_hash)
            yield cls._remove_resource_owner(file_hash, uid)

    @classmethod
    @gen.coroutine
    def add_comment(cls, file_hash, uid, user_name, comment):
        assert uid >= 0
        assert len(user_name) > 0
        comment = comment.strip()
        assert len(comment) > 0
        assert file_hash > 0
        resource_comment = {"who": user_name, "uid": uid, "content": comment, "ctime": long(time())}
        yield cls._add_resource_comment(file_hash, resource_comment)

    @classmethod
    @gen.coroutine
    def tip_off(cls, file_hash, uid):
        assert uid > 0
        assert file_hash > 0
        res = yield cls._db.all_resources.find_one({'file_hash': file_hash})
        if res:
            if 'tip_off_users' not in res:
                res['tip_off_users'] = [uid]
            else:
                am_i_in = filter(lambda id: uid == id, res["tip_off_users"])
                if len(am_i_in) > 0:
                    assert len(am_i_in) == 1
                else:
                    res['tip_off_users'].append(uid)
                MAX_TIP_OFF_CNT = 10
                if len(res['tip_off_users']) == MAX_TIP_OFF_CNT:
                    assert "owners" in res
                    for owner in res["owners"]:
                        if owner["is_public"]:
                            yield FBCoinManager.plus_fb(owner["uid"], -FBCoinManager._coin_rules["tip_off"])
                    for owner in res["tip_off_users"]:
                        yield FBCoinManager.plus_fb(owner, FBCoinManager._coin_rules["tip_off"])
                    res["hidden"] = 1
            yield cls._db.all_resources.save(res)

    @classmethod
    @gen.coroutine
    def add_score(cls, file_hash, uid, score):
        assert uid >= 0
        assert score >= 0 and score <= 10
        assert file_hash > 0
        yield cls._add_resource_score(file_hash, uid, score)

    @classmethod
    @gen.coroutine
    def increase_download_num(cls, file_hash,file_size=None):
        assert (file_hash > 0)
        # yield cls._db.all_resources.update({'file_hash': file_hash}, {'$inc': {"hot.0.download_num_day":1,'download_num': 1}}, True)
        res=None
        if file_size:
            file_id=gen_file_id(file_hash,file_size)
            res=yield cls._db.all_resources.find_one({'file_id': file_id})
        else:
            res=yield cls._db.all_resources.find_one({'file_hash': file_hash})
        if res:
            if "hot" not in res:
                res["hot"]=[{"download_num_day":0, "hot_day":0}]
            if "download_num" not in res:
                res["download_num"]=0
            res["hot"][0]["download_num_day"]+=1
            res["download_num"]+=1
            if res["download_num"]==100:
                yield FBCoinManager.hot100_resource(res["owners"][0]["uid"])
            yield cls._db.all_resources.save(res)

    @classmethod
    @gen.coroutine
    def _add_to_my_resource_list(cls, uid, file_hash):
        assert (uid >= 0)
        assert (file_hash > 0)  #file hash is an int
        yield cls._db.resources_of_user.update({"uid": uid}, {"$addToSet": {"file_hashes": file_hash}}, True)

    @classmethod
    @gen.coroutine
    def _remove_from_my_resource_list(cls, uid, file_hash):
        assert (uid >= 0)
        assert (file_hash > 0)  #file hash is an int
        yield cls._db.resources_of_user.update({"uid": uid}, {"$pull": {"file_hashes": file_hash}}, True)

    @classmethod
    @gen.coroutine
    def _add_resource_owner(cls, file_hash, uid, is_public):
        '''
        resource owners in all_resources db is like, owners: [{"uid":uid1, "is_public":1}, {"uid": uid2, is_public: 0},...]
        '''
        assert (file_hash > 0)
        assert (uid >= 0)
        unknown = None
        assert (is_public == 0 or is_public == 1 or is_public == unknown)
        res = yield cls._db.all_resources.find_one({'file_hash': file_hash})
        if res:
            if is_public == unknown:  #check history public attr and inherit it
                if "public" in res:
                    is_public = res["public"]
                else:
                    is_public = 1
            owner_doc = {"uid": uid, "is_public": is_public}
            if 'owners' not in res:
                res['owners'] = [owner_doc]
                res['public'] = is_public
            else:
                am_i_in = filter(lambda owner: uid == owner['uid'], res["owners"])
                if len(am_i_in) > 0:
                    assert len(am_i_in) == 1
                    am_i_in[0]["is_public"] = is_public
                else:
                    res['owners'].append(owner_doc)
                public_owners = filter(lambda owner: owner['is_public'] == 1, res["owners"])
                if len(public_owners) > 0:  #the resource is public and every one can see it
                    res["public"] = 1
                else:
                    res["public"] = 0
            yield cls._db.all_resources.save(res)
        else:
            #print "warning: file not found. but the user want to use it! He is a hacker..."
            #pass
            owner_doc = {"uid": uid, "is_public": 1}
            yield cls._db.all_resources.insert({'file_hash': file_hash, 'owners': [owner_doc], "public": is_public})

    @classmethod
    @gen.coroutine
    def _remove_resource_owner(cls, file_hash_or_id, uid):
        '''
        resource owners in all_resources db is like, owners: [{"uid":uid1, "is_public":1}, {"uid": uid2, is_public: 0},...]
        '''
        assert (uid >= 0)
        if isinstance(file_hash_or_id,unicode):
            file_id=file_hash_or_id
            assert len(file_id)>0
            res = yield cls._db.all_resources.find_one({'file_id': file_id})
        else:
            file_hash=file_hash_or_id
            assert (file_hash > 0)
            res = yield cls._db.all_resources.find_one({'file_hash': file_hash})
        if res and 'owners' in res:
            am_i_in = filter(lambda owner: uid == owner['uid'], res["owners"])
            if len(am_i_in) > 0:
                assert len(am_i_in) == 1
                res["owners"].remove(am_i_in[0])
            public_owners = filter(lambda owner: owner['is_public'] == 1, res["owners"])
            if len(public_owners) > 0:  #the resource is public and every one can see it
                res["public"] = 1
            else:
                res["public"] = 0
            yield cls._db.all_resources.save(res)

    @classmethod
    @gen.coroutine
    def _update_resource_header(cls, file_hash, resource_header):
        assert file_hash > 0
        yield cls._db.all_resources.update({"file_hash": file_hash}, {"$set": resource_header}, True)

    @classmethod
    @gen.coroutine
    def _add_resource_comment(cls, file_hash, resource_comment):
        '''
        comments in all_resources db is like, comments: [{who: user_name1, uid: uid1, content: the content of comment, ctime: 2014-4-3},...]
        '''
        assert file_hash > 0
        yield cls._db.all_resources.update({"file_hash": file_hash}, {"$push": {"comments": resource_comment}}, True)

    @classmethod
    @gen.coroutine
    def _add_resource_score(cls, file_hash, uid, resource_score):
        '''
        grades in all_resources db is like, scores: [{uid: uid1, "score": grade1}, {uid: uid2, score:grade2},...]
        '''
        assert file_hash > 0
        assert uid > 0
        assert resource_score >= 0 and resource_score <= 10
        score_doc = {"uid": uid, "score": resource_score}
        res = yield cls._db.all_resources.find_one({'file_hash': file_hash})
        if res:
            if 'scores' not in res:
                res['scores'] = [score_doc]
            else:
                am_i_in = filter(lambda owner: uid == owner['uid'], res["scores"])
                if len(am_i_in) > 0:
                    assert len(am_i_in) == 1
                    am_i_in[0]["score"] = resource_score
                else:
                    res['scores'].append(score_doc)
            yield cls._db.all_resources.save(res)
        else:
            #print "warning: file not found. but the user want to score it! He is a hacker..."
            #pass
            yield cls._db.all_resources.insert({'file_hash': file_hash, 'scores': [score_doc]})

    @classmethod
    @gen.coroutine
    def _add_resource_for_tag(cls, file_hash, tags_list):
        '''
        tags db is like doc1 {tag: tag1, file_hashes: [file_hash1,file_hash2,...]}, ....
        '''
        assert (file_hash > 0)
        assert (len(tags_list) > 0)
        for tag in tags_list:
            assert len(tag) > 0
            yield cls._db.tags.update({"tag": tag}, {"$addToSet": {"file_hashes": file_hash}}, True)

    @classmethod
    @gen.coroutine
    def _add_tags_for_resource(cls, file_hash, tags_list):
        '''
        tags for all_resources is like {tags: [tag1,tag2,...]}
        '''
        assert file_hash > 0
        assert len(tags_list) > 0
        yield cls._db.all_resources.update({"file_hash": file_hash}, {"$addToSet": {"tags": {"$each": tags_list}}},
                                           True)

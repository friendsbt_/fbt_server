__author__ = 'bone-lee'

import pymongo
import tornado.gen
import tornado.testing

class DownloadTestCase(tornado.testing.AsyncTestCase):
  def test_download(self):
      @tornado.gen.engine
      def func(callback):
            from motor import MotorReplicaSetClient
            # db=motor.MotorClient('localhost', 27017, io_loop=self.io_loop).fbt_test

            # master to write
            rsc = MotorReplicaSetClient(hosts_or_uri="127.0.0.1:27017",io_loop=self.io_loop, replicaSet='fbt_repl')
            # print rsc.database_names
            # rsc.open()
            # NOT allow to write
            yield rsc.fbt_repl.test_col.remove()
            yield rsc.fbt_repl.test_col.insert({"name":"bone","age":11})
            yield rsc.fbt_repl.test_col.insert({"name":"xixi","age":1})
            print rsc.secondaries #set([(u'bone-lee-laptop', 27019), (u'bone-lee-laptop', 27018)])
            print rsc.primary #(u'bone-lee-laptop', 27017)
            print rsc.arbiters #frozenset([(u'bone-lee-laptop', 27020)])

            # test read client
            # rsc = MotorReplicaSetClient(hosts_or_uri="127.0.0.1:27018",io_loop=self.io_loop, replicaSet='fbt_repl')
            # rsc.read_preference = pymongo.ReadPreference.NEAREST
            rsc.read_preference = pymongo.ReadPreference.SECONDARY_ONLY
            # print rsc.secondaries
            # print rsc.test_database
            ff=yield rsc.fbt_repl.test_col.find_one()
            print ff
            cursor=rsc.fbt_repl.test_col.find()
            while (yield cursor.fetch_next):
                res = cursor.next_object()
                print res

            # test for fbt
            rsc.fbt.all_resources.insert({"public":3,"file_name":"kaka","file_hash":1,"mtime":12321312})

            cursor=rsc.fbt.all_resources.find({"public":1}).limit(3)
            while (yield cursor.fetch_next):
                res = cursor.next_object()
                print res
            callback()

      func(callback=self.stop)
      self.wait()

if __name__ == '__main__':
    import unittest
    unittest.main()
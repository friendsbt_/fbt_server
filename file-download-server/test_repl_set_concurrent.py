__author__ = 'bone-lee'

import pymongo
import tornado.gen
import tornado.testing
from motor import MotorReplicaSetClient
from motor import MotorClient

class ReplicaSetTest(tornado.testing.AsyncTestCase):
  def test_replica(self):
      @tornado.gen.engine
      def func(callback):
            # master to write
            rsc = MotorReplicaSetClient(hosts_or_uri="fbt:27017",io_loop=self.io_loop, replicaSet='fbt_repl') 
            #rsc  = MotorClient()
			#*************************************************************
			# MUST set the following clause			
            #rsc.read_preference = pymongo.ReadPreference.SECONDARY
            #rsc.read_preference = pymongo.ReadPreference.SECONDARY_ONLY
            yield rsc.open()
            #*************************************************************
            # NOT allow to write
            print rsc.secondaries #set([(u'bone-lee-laptop', 27019), (u'bone-lee-laptop', 27018)])
            print rsc.primary #(u'bone-lee-laptop', 27017)
            print rsc.arbiters #frozenset([(u'bone-lee-laptop', 27020)])
            # print rsc.wirte_concern
            #rsc.write_concern.doc.put("w",0)
            #rsc.wirte_concern['w'] = 0
            # print rsc.wirte_concern
            
            #how_many=yield rsc.fbt_repl.test_col.find().count()
            #print "how_many="+str(how_many)

            #yield rsc.fbt_repl.test_col.remove()
            insert_cnt=100
            for i in range(insert_cnt):
                yield rsc.fbt_repl.test_col.insert({"name":"bone","age":11})
                yield rsc.fbt_repl.test_col.insert({"name":"xixi","age":1})

            print "******************************************read**************************************"
            # rsc = MotorReplicaSetClient(hosts_or_uri="fbt:27018",io_loop=self.io_loop, replicaSet='fbt_repl')
            rsc.read_preference = pymongo.ReadPreference.SECONDARY_ONLY
            yield rsc.open()
            #*************************************************************
            # NOT allow to write
            print rsc.secondaries #set([(u'bone-lee-laptop', 27019), (u'bone-lee-laptop', 27018)])
            print rsc.primary #(u'bone-lee-laptop', 27017)
            print rsc.arbiters #frozenset([(u'bone-lee-laptop', 27020)])
            cursor=rsc.fbt_repl.test_col.find(read_preference=pymongo.ReadPreference.SECONDARY_ONLY).limit(300)
            ans=[]
            i=0
            while (yield cursor.fetch_next):
                if i==200: break
                res = cursor.next_object()
                ans.append(res)
                i+=1
            print "count=%d" % len(ans)
            assert len(ans)>=200
            callback()

      func(callback=self.stop)
      self.wait()

if __name__ == '__main__':
    import unittest
    unittest.main()

__author__ = 'bone-lee'

from tornado import gen
import motor

class FbtUserStoreManager(object):
    _db = motor.MotorClient('localhost', 27017).fbt

    @classmethod
    def set_db(cls, db):
        cls._db = db

    @classmethod
    @gen.coroutine
    def get_friend_list(cls,uid):
        assert uid>0
        my = yield cls._db.users.find_one({"uid":uid})
        if my and "friends" in my:
             raise gen.Return([friend['uid'] for friend in my["friends"]])
        raise gen.Return([])
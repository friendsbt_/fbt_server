__author__ = 'bone-lee'

import cPickle as pickle
import os

from fb_ranker import FBRanker

class FBRankManager(object):
    _weekly_ranker=FBRanker(5000,'weekly-rank')
    _monthly_ranker=FBRanker(10000,'monthly-rank')
    _weekly_ranker_file="weekly.pkl"
    _monthly_ranker_file="monthly.pkl"

    @classmethod
    def initialize(cls):
        if os.path.isfile(cls._weekly_ranker_file):
            with open(cls._weekly_ranker_file,'r+') as infile:
                cls._weekly_ranker.restore_from_backup_info(pickle.load(infile))
        if os.path.isfile(cls._monthly_ranker_file):
            with open(cls._monthly_ranker_file,'r+') as infile:
                cls._monthly_ranker.restore_from_backup_info(pickle.load(infile))

    @classmethod
    def save_info_to_file(cls):
        with open(cls._weekly_ranker_file,'wb') as infile:
            pickle.dump(cls._weekly_ranker.get_backup_info(),infile)
        with open(cls._monthly_ranker_file,'wb') as infile:
            pickle.dump(cls._monthly_ranker.get_backup_info(),infile)

    @classmethod
    def get_weekly_ranker(cls):
        return cls._weekly_ranker

    @classmethod
    def get_monthly_ranker(cls):
        return cls._monthly_ranker

    @classmethod
    def update_fb(cls,uid,delta_fb):
        assert uid>0
        cls._weekly_ranker.update_fb(uid,delta_fb)
        cls._monthly_ranker.update_fb(uid,delta_fb)

    @classmethod
    def get_monthly_top(cls):
        return cls._monthly_ranker.get_top(100)

    @classmethod
    def get_weekly_top(cls):
        return cls._weekly_ranker.get_top(100)

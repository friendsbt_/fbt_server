function executeScript(html)
{
    var reg = /<script[^>]*>([^\x00]+)$/i;
    //对整段HTML片段按<\/script>拆分
    var htmlBlock = html.split("<\/script>");
    for (var i in htmlBlock)
    {
        var blocks;//匹配正则表达式的内容数组，blocks[1]就是真正的一段脚本内容，因为前面reg定义我们用了括号进行了捕获分组
        if( (blocks = htmlBlock[i].match(reg)) )
        {
            //清除可能存在的注释标记，对于注释结尾-->可以忽略处理，eval一样能正常工作
            var code = blocks[1].replace(/<!--/, '');
            try
            {
                eval(code); //执行脚本
            }
            catch (e)
            {
            }
        }
    }
}
function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
jQuery.postJSON = function(url, args, callback) {
    //args._xsrf = getCookie("_xsrf");
    //console.log(args);
    $.ajax({url: url, data: $.param(args), dataType: "text", type: "POST",
            success: function(response) {
                if(response == "nul"){
                    showErrorToast("目前网络状况不佳，请检查网络或者重试");
                    return;
                }
        if (callback) callback(response);
    }, error: function(response) {
        console.log("ERROR:", response);
    }});
};
jQuery.post = function(url, args, callback) {
    //args._xsrf = getCookie("_xsrf");
    $.ajax({url: url, data: $.param(args), type: "POST",
            success: function(response) {
                if(response == "nul"){
                    showErrorToast("目前网络状况不佳，请检查网络或者重试");
                    return;
                }
        if (callback) callback(response);
    }, error: function(response) {
        console.log("ERROR:", response);
    }});
};
jQuery.get = function(url, callback, flag) {
    if(flag)
    {
        $.ajax({url: url, type: "GET", success: function(response) {
            if(response == "nul"){
                    showErrorToast("目前网络状况不佳，请检查网络或者重试");
                    return;
                }
            if(callback) callback(response);
        }, error: function(response) {
            console.log("ERROR:", response);
        }});
        return;
    }
    //url = url + "?_xsrf=" + getCookie("_xsrf");
    $.ajax({url: url, type: "GET", success: function(response) {
        if(response == "nul"){
                    showErrorToast("目前网络状况不佳，请检查网络或者重试");
                    return;
                }
        if(callback) callback(response);
        $('a').unbind(".plugin"); 
        $("a").bind("click.plugin",function(e){
        if($(this).attr("href") && $(this).attr("href").charAt(0) == '/'){
        e.preventDefault();
        var addr = $(this).attr("href");
        //console.log(addr+2);
        if(addr == "/index"){
            var that = $(".show");
            $(that).removeClass("show").addClass("hide");
            $(".index").removeClass("hide").addClass("show");
            $("body").css("padding-top","0px");
        }
        else if(addr == "/mySpace"){
            var space = $(".mySpace");
            if(space.length > 0){
                var that = $(".show");
                $(that).removeClass("show").addClass("hide");
                $(".mySpace").removeClass("hide").addClass("show");
                $("body").css("padding-top","50px");
                $("#msgHint").hide();
            }
            else{
                var t = this;
                $(t).disable();
                $.get(addr, function(data) {
                    $(t).enable();
                    if($(".mySpace").length > 0)
                        return;
                    var that = $(".show");
                    $(that).removeClass("show").addClass("hide");
                    var div = "<div class='mySpace show'></div>";
                    $("body").append(div);
                    $(".mySpace").html(data);
                    $("body").css("padding-top","50px");
                }, false);
            }
        }
        else{
            var t = this;
            $(t).disable();
            $.get(addr, function(data) {
                $(t).enable();
                var that = $(".show");
                $(that).removeClass("show").addClass("hide");
                $("body > div.wrap_all").html(data);
                $("body > div.wrap_all").removeClass("hide").addClass("show");
                $("body").css("padding-top","0px");
            }, false);
        }
        return false;
        }
        else{
        return true;
        }
        });
    }, error: function(response) {
        console.log("ERROR:", response);
    }});
};

jQuery.fn.formToDict = function() {
    var fields = this.serializeArray();
    var json = {};
    for (var i = 0; i < fields.length; i++) {
        json[fields[i].name] = fields[i].value;
    }
    if (json.next) delete json.next;
    return json;
};

jQuery.fn.disable = function() {
    //this.enable(false);
    $(this).addClass('disabled');
    return this;
};

jQuery.fn.enable = function(opt_enable) {
    /*if (arguments.length && !opt_enable) {
        this.attr("disabled", "disabled");
    } else {
        this.removeAttr("disabled");
    }*/
    $(this).removeClass('disabled');
    return this;
};
$(function(){
    $('a').unbind(".plugin"); 
    $("a").bind("click.plugin",function(e){
        if($(this).attr("href") && $(this).attr("href").charAt(0) == '/'){
        e.preventDefault();
        var addr = $(this).attr("href");
        //console.log(addr+"1");
        if(addr == "/index"){
            var that = $(".show");
            $(that).removeClass("show").addClass("hide");
            $(".index").removeClass("hide").addClass("show");
            $("body").css("padding-top","0px");
        }
        else if(addr == "/mySpace"){
            var space = $(".mySpace");
            if(space.length > 0){
                var that = $(".show");
                $(that).removeClass("show").addClass("hide");
                $(".mySpace").removeClass("hide").addClass("show");
                $("body").css("padding-top","50px");
                $("#msgHint").hide();
            }
            else{
                var t = this;
                $(t).disable();
                $.get(addr, function(data) {
                    $(t).enable();
                    if($(".mySpace").length > 0)
                        return;
                    var that = $(".show");
                    $(that).removeClass("show").addClass("hide");
                    var div = "<div class='mySpace show'></div>";
                    $("body").append(div);
                    $(".mySpace").html(data);
                    $("body").css("padding-top","50px");
                }, false);
            }
        }
        else{
            var t = this;
            $(t).disable();
            $.get(addr, function(data) {
                $(t).enable();
                var that = $(".show");
                $(that).removeClass("show").addClass("hide");
                $("body > div.wrap_all").html(data);
                $("body > div.wrap_all").removeClass("hide").addClass("show");
                $("body").css("padding-top","0px");
            }, false);
        }
        return false;
        }
        else{
        return true;
        }
    });
});
function quit(){
    $.get('/logout', function(data) {
        /*optional stuff to do after success */
        //console.log(data);
        //data = JSON.parse(data);
        if(data && data["type"] == 1){
            //$("html").remove();
            window.location.href = "/login";
        }
    }, true);
}
function getJsonLength(jsonData){

var jsonLength = 0;

for(var item in jsonData){

jsonLength++;

}

return jsonLength;

}
function appendGroupChat(data){
    var div = '<div class="row comment"><div class="col-lg-12"><h5 style="font-size: 16px;margin: 0"><span class="flatColor">'
            +data["name"]
            +'</span>&emsp;说：<span class="time">'
            +data["time"]
            +'</span></h5></div><div class="col-lg-12" style="word-break: break-all;"><p style="margin: 0 0 5px 0;font-size: 14px;">&emsp;'
            +data["msg"]
            +'</p></div></div>';
    if(!window.group_chat_count)
    {
        window.group_chat_count = 0;
        window.group_chat_index = 10;
    }
    window.group_chat_count++;
    $("#group_chat").prepend(div);
    if(window.group_chat_count > 10){
        $("#moreGroupChat").css("visibility","visible");
        $("#moreGroupChat").click(function(){
            //console.log("message");
            $("#loadingText").text("正在获取更多...");
            $("#loadingGif").css({"visibility":"visible"});
            var index = window.group_chat_index;
            if((window.group_chat_index+=10) > window.group_chat_count)
              window.group_chat_index = window.group_chat_count;
            $.each($("#group_chat").children(), function(i, value){
              if(i == window.group_chat_index)
                return false;
              if(i < index)
                return true;
              $(value).fadeIn('fast',function(){}); 
            });
            $("#loadingText").text("查看更多");
            $("#loadingGif").css({"visibility":"hidden"});
        });
        $("#group_chat > div:nth-child("+window.group_chat_index+")").fadeOut('fast', function() {                        
            });
    }
}
//check and change online state
function onlineState(state, uid){
    var myFriends = $("#myFriends");
    var t = ["gray", "#1abc9c"];
    if(myFriends.length > 0){
        var ret_state;
        myFriends.find('a.'+uid).each(function(index, el) {
            s = window.parseInt(state);
            if(s == -1){
                if($(el).attr("data") == "0") 
                    ret_state = false;
                else
                    ret_state = true;
            }
            else if(s == 0 || s == 1){
                $(el).attr("data", state);
                $(el).css("color",t[s]);
            }
            return false;
        });
        return ret_state;
    }
}
function addFriendCount(isNew,isOnline){
    var count_h = $("#group_count").html();
    //console.log(count_h);
    if(count_h){
        var first = count_h.indexOf("(");
        var next = count_h.indexOf("/");
        var end = count_h.indexOf(")");
        var online = window.parseInt(count_h.substring(first+1,next));
        var all = window.parseInt(count_h.substring(next+1,end));
        if(isOnline)
            online++;
        if(isNew)
            all++;
        $("#group_count").html("我的好友("+online+"/"+all+")");
    }
}
function initFriendState(){
    var count_h = $("#group_count").html();
    //console.log(count_h);
    if(count_h){
        var next = count_h.indexOf("/");
        var end = count_h.indexOf(")");
        var all = window.parseInt(count_h.substring(next+1,end));
        $("#group_count").html("我的好友(0/"+all+")");
        $("#myFriends").find("a[data='1']").each(function(index, el) {
            $(el).attr("data", 0);
            $(el).css("color","gray");
        });
    }
}
function handleDelHis(html,hash){
  var flag;
  var l1 = html.length;
  var l2 = hash.length;
  if(l1 == l2)
    flag = "";
  else{
    flag = html.charAt(l1-1);
  }
  handleComment(hash,3,flag);
  $("#download_file_hash"+html).attr("data", '0');
  $("#download_file_hash"+html).attr("download", '0').removeAttr('download');
  $("#download_file_hash"+html).attr('error','1').removeAttr('error');
  if($("#container_download_resources").children("#item"+html).length > 0){
      var tmp = $("#item"+html);
      var item_download = tmp.clone(true,true);
      tmp.hide();
      tmp.remove();
      //$("#container_resources").prepend(item_download);
      if($("#container_download_resources").children().length == 0)
      {
        $("#text_his_page_tips").show();
        $("#text_his_page_tips").html("<center><h3>暂时没有下载资源！</h3></center>");
      }
  }
}
function update(num){
    $("#updateModal").modal('hide');
    if(num == 1){
        window.socket.emit("update");
    }
}
function getFriendRes(addr){
    $.get(addr, function(data) {
        var that = $(".show");
        $(that).removeClass("show").addClass("hide");
        $("body > div.wrap_all").html(data);
        $("body > div.wrap_all").removeClass("hide").addClass("show");
        $("body").css("padding-top","0px");
    }, false);
}
function createLocalSock(){
    if(!window.socket){
        console.log("create Sock");
    window.socket = io.connect('http://localhost/', { port: 12345 });
    /*socket.on("test", onGetData);
    socket.emit("test", "client sock");
    function onGetData(data){
        console.log(data);
    }*/
    window.socket.on('connect', function(){
      console.log("Sock send");
      window.socket.emit("nick", window.nick_name);
      window.socket.on('inform', function(data){
        showNoticeToast(data);
      });
      window.socket.on('download', function(data){
        //console.log("Sock recv");
        //console.log(data);
        var tmp_data;
        try{
            tmp_data = JSON.parse(data);
        }
        catch(e){
            showNoticeToast("网络通信出现了一点小问题，请重试");
            return;
        }
        data = tmp_data;
        if(data["type"] == 1){
          $('#download_progress' + data["html"]).html(data["error"]);
          var tmp = $("#download_file_hash"+data["html"]);
          tmp.attr("data", '0');
          tmp.attr("error", '1');
          tmp.text("|重新下载");
          //$("#download_file_hash"+data["html"]).removeAttr('download');
        }
        else if(data["type"] == 2){
          var obj = data["html"];
          var act = data["actions"];
          var value = data["value"];
          if(act.length == 1){
            action(obj, act[0], value);
          }
          else{
            for(var i = 0; i < act.length; i++){
              action(obj, act[i], value[i]);
            }
          }
        }
        else if(data["type"] == 6){
          handleDelHis(data["html"],data["hash"]);
          if (data["error"] == 0) {
              var tmp_html = $('#download_progress' + data["html"]).html();
              var index_s = 0;
              for(var i =0; i< tmp_html.length;i++){
                var unit = tmp_html.charAt(i);
                if(unit == 'B' || unit == 'K' || unit == 'M' || unit == 'G')
                {
                    index_s = i;
                    break;
                }
              }
              var index_e = tmp_html.lastIndexOf('B')+1;
              $('#download_progress' + data["html"]).html("0"+tmp_html.substring(index_s,index_e)+"&emsp;0%");
              var progressVal = 0;
              $('#download_progress_bar' + data["html"]).css('width', progressVal + '%').attr('aria-valuenow', progressVal);
              //handleComment(fileHash,3,flag);
          } else {
              $('#download_progress' + data["html"]).html("删除失败");
          }
        }
      });
      window.socket.on("download_start",function(data){
        var tmp = $("#item"+data);
        if(tmp.parent("#container_download_resources").length <= 0)
        {
            /*var item_download = tmp.clone(true,true);
            tmp.remove();
            $("#container_download_resources").prepend(item_download);*/
            tmp.detach().prependTo('#container_download_resources');
        }        
        $("#text_his_page_tips").hide();
        $("#download_file_hash"+data).attr("download",true);
        downloadGrumble();
        $('#download_progress' + data).html("排队中");
      });
      window.socket.on("download_over",function(data){
        data = JSON.parse(data);
        $("#download_file_hash"+data["html"]).attr("data", '0');
        if("name" in data){
            $("#cancel"+data["html"]).hide();
            $("#open"+data["html"]).show();
            showSuccessToast(data["name"]+"下载完成");
            $("#audio").trigger('play');
            var tmp = $("#item"+data["html"]);
            tmp.detach().appendTo('#container_download_resources');
        }
      });
      window.socket.on("friend",function(data){
        console.log("add friend finish");
        data = JSON.parse(data);
        if(data["online"] == 1)
        {
            $("#myFriends").prepend(data["html"]);
            addFriendCount(true,true);
        }
        else
        {
            $("#myFriends").append(data["html"]);
            addFriendCount(true,false);
        }
      });
      window.socket.on("net",function(){
        showErrorToast("目前网络状况不佳，有操作未能完成，请检查网络或者重试");
        var login = $("#login");
        if(login.length > 0){
            login.removeAttr("disabled");
        }
        return;
      });
      window.socket.on("update",function(){
        $("#updateModal").modal('show');
      });
      window.socket.on('close', function(){
        console.log("Sock end");
        delete window.socket;
      });
      window.socket.on('his_del', function(data){
        var tmp_data;
        try{
            tmp_data = JSON.parse(data);
        }
        catch(e){
            tmp_data = data;
        }
        data = tmp_data;
        var html = data["html"];
        if($("#container_download_resources").children("#item"+html).length > 0){
          var tmp = $("#item"+html);
          tmp.hide();
          tmp.remove();
          if($("#container_download_resources").children().length == 0)
          {
            $("#text_his_page_tips").show();
            $("#text_his_page_tips").html("<center><h3>暂时没有下载资源！</h3></center>");
          }
      }
      });
      window.socket.on('start_sock',function(data){
        var tmp_data;
        try{
            tmp_data = JSON.parse(data);
        }
        catch(e){
            tmp_data = data;
        }
        data = tmp_data;
        window.fbtUID = window.parseInt(data["uid"]);
        window.user = data["user"];
        createSock();
      });
      window.socket.on('checkOnline', function(data){
        try{
            window.ws.send(JSON.stringify({"uid":window.fbtUID,"token":window.token,"type":10,"uid_list":data}));
            console.log("check offline");
        }
        catch(e){
            console.log("sock offline");
        }
      });
      window.socket.on('s_upload', function(data){
        //console.log(data);
        try{
            window.ws.send(JSON.stringify({"uid":window.fbtUID,"token":window.token,"data":data,"type":11}));
        }
        catch(e){
            if(!window.uploadData){
                window.uploadData = [];
            }
            window.uploadData.push(data);
        }
      });
      window.socket.on('upload', function(data){
        var tmp_data;
        try{
            tmp_data = JSON.parse(data);
        }
        catch(e){
            tmp_data = data;
        }
        data = tmp_data;
        if(data != ""){
            if(data["type"] == 2){
                window.uploadSize[data["id"]] = data["size"];
                var li = '<li id="l'+data["id"]+'"><a style="font-size: 18px;padding: 5px;">';
                li = li + '<span style="min-width: 260px;max-width: 260px; white-space:nowrap; overflow:hidden; text-overflow: ellipsis;display: inline-block;position: relative;top: 4px;">';
                li = li + data["name"] + '</span><span style="position: relative;right: -4px;"><span id="u'+data["id"]+'">0</span>/'+data["size"];
                li = li + '</span></a></li>';
                var panel = $("#upload_control_panel");
                if(panel.children().length > 0){
                    var divide = '<li class="divider"></li>';
                    $("#upload_control_panel").prepend(divide);
                }
                panel.prepend(li);
                $("#upload_control_total").text(window.upload_count);
            }
            else if(data["type"] == 1){
                if("msg" in data)
                    showSuccessToast(data["msg"]);
                else
                {
                    showErrorToast(data["error"]);
                    var li = $("li#l"+data["id"]);
                    if(li.length > 0)
                        li.children('a').children('span').css("color","red");
                }
                if(window.upload_count > 0)
                    window.upload_count--;
                $("#upload_control_total").text(window.upload_count);
                if(window.upload_count == 0)
                  $("a#tab3 > span.uploadHint").hide();
            }
            else if(data["type"] == 0)
            {
                showErrorToast(data["error"]);
                var li = $("li#l"+data["id"]);
                if(li.length > 0)
                    li.children('a').children('span').css("color","red");
            }
            else if(data["type"] == 3){
                var span = $("span#u"+data["id"]);
                var size = window.parseInt(span.text());
                if(span.length > 0 && size < window.parseInt(window.uploadSize[data["id"]])){
                    span.text(size+1);
                }
            }              
        }
      });
    }); 
  }
}
//205.147.105.205
function sendHeart(){
    //console.log("send heart");
    window.socket.emit('tick');
    try{
        window.ws.send(JSON.stringify({type: 9}));
    }
    catch(e){
        console.log(e.stack);
    }
}
function sendUpload(){
    if(!window.uploadData || window.uploadData.length == 0){
        clearInterval(window.uploadInterval);
        delete window.uploadInterval;
    }
    else{
        var data = window.uploadData.shift();
        window.ws.send(JSON.stringify({"uid":window.fbtUID,"token":window.token,"data":data,"type":11}));
    }
}
function createSock(){
    if(!window.ws){
      window.ws = new WebSocket('ws://www.friendsbt.com:8888/socket');
      window.ws.onopen = function(){
          //ws.send( "0"+$("b.user").html());
          if(window.sock_restart){
            showNoticeToast("系统已经自动重连");
            console.log("sock restart");
            $("#userName").css("color","#1abc9c");
            window.ws.send(JSON.stringify({connect:1,token:window.token,type: 0, uid: window.fbtUID, user: window.user}));
            window.uploadInterval = setInterval(sendUpload, 10000);
            clearInterval(window.sock_restart);
            delete window.sock_restart;
          }
          else{
            console.log("sock open");
            window.ws.send(JSON.stringify({connect:0,token:window.token,type: 0, uid: window.fbtUID, user: window.user}));
            //$.get("/startServer/local", function(data){console.log("local server start.");},true);
          }
          window.heart = setInterval(sendHeart,180000);
      };
      window.ws.onmessage = function(event) {
        //console.log(event.data);
        var data;
        try{
            data = JSON.parse(event.data);
        }
        catch(e){
            showNoticeToast("网络通信出现了一点小问题，请重试");
            return;
        }
        //type 0 for sys, 1 for left, 2 for single chat, 3 fro group chat, 4 for coin, 5 for login repeat
        //6 for remove friend, 7 for sys info, 8 for update,9 for heart
        if(!("type" in data))
        {
            showNoticeToast("网络通信出现了一点小问题，请重试");
            return;
        }
        if(data["type"] == 0){
            if("ip" in data){
                $.get("/startServer/local?ip="+data["ip"], function(data){console.log("local server start.");},true);
            }
            else if("id" in data){
                if("nick" in data){
                    showNoticeToast(data["nick"]+"想加你为好友");
                }
                var li = '<li><a class="unRead msg'+data["id"]+'" onclick="handle(&apos;'+data["nick"]+'&apos;, &apos;'+data["id"]+'&apos;, &apos;'+data["sender"]+'&apos;)">'+data["content"]+'&nbsp;'+data["time"]+'</a></li>';
                $("li.dropdown > ul.msg").each(function(index, el) {
                    $(el).prepend(li);
                });
                var msg_count = $("li.dropdown > a.dropdown-toggle > span.msg_count");
                var count = window.parseInt(msg_count.html()) + 1;
                msg_count.each(function(index, el) {
                  $(el).html(count);
                });
              }
            else if("message_type" in data){
                var MESSAGE={"OPEN_UDP_SERVER": 1};//more....
                if(data["message_type"]==MESSAGE["OPEN_UDP_SERVER"]){
                    //json.dumps({"message_type": OPEN_UDP_SERVER, "file_hash": file_hash,"what": "open udp server","for": for_who})
                    $.get("/startServer/nat?for="+data["for"]+"&hash="+data["file_hash"], function(data){console.log("nat server start.");},true);
                }//TODO FIXME more mesaage processing here
            }
            else if("user" in data){
                if($("#accordion"+data["uid"]).length > 0)
                {
                    console.log("friend exist");
                    return;
                }
                //console.log("add friend");
                var myFriends = $("#myFriends");
                if(myFriends.length > 0){
                    console.log("my friend panel exist");
                    data["count"] = myFriends.children().length;
                    window.socket.emit('friend', JSON.stringify(data));
                }
            }
            else if("sys" in data){
                onlineState("1", data["sys"]);
                addFriendCount(false,true);
                showNoticeToast(data["msg"]);
                var div = $("#accordion"+data["sys"]);
                div.detach().prependTo('#myFriends');
            }
            else if("err" in data){
                showErrorToast(data["err"]);
            }
            else{
                showNoticeToast(data["msg"]);
            }
        }
        else if(data["type"] == 1){
            onlineState("0", data["user"]);
            var count_h = $("#group_count").html();
            if(count_h){
                var first = count_h.indexOf("(");
                var next = count_h.indexOf("/");
                var end = count_h.indexOf(")");
                var online = window.parseInt(count_h.substring(first+1,next));
                var all = window.parseInt(count_h.substring(next+1,end));
                if(online != 0)
                    online--;
                $("#group_count").html("我的好友("+online+"/"+all+")");
            }
            showNoticeToast(data["msg"]);
            var div = $("#accordion"+data["user"]);
            div.detach().appendTo('#myFriends');
        }
        else if(data["type"] == 3){
            //{"type":3,"recv":"","sender":who,"msg":content,"time":time}                        
            appendGroupChat(data);
            var msgHint = $("#msgHint");
            if(msgHint.length > 0 && $(".mySpace").is(":hidden"))
                $("#msgHint").show();
        }
        else if(data["type"] == 2){
            //showNoticeToast()
            if(!window.singleChat[data["sender"]])
                window.singleChat[data["sender"]] = [];
            window.singleChat[data["sender"]].push(data);
            var p_div = $(".chatBox").find("#c_"+data["sender"]);
            if(p_div.length > 0){
                var p = "<p class='msgNameTime msgLeft'>"+data["nick_name"]+"&emsp;"
                    +data["time"]
                    +"<br><span class='msgContent'>"
                    +data["msg"]
                    +"</span></p>";
                p_div.append(p);
                p_div.scrollTop(p_div[0].scrollHeight); 
            }
            else{
                var date = (new Date()).getTime();
                var li = '<li><a class="unRead msg2'+date+'" onclick="handle(0,2'+date+',1,true)">'+data["nick_name"]+'给您发了一条私信.'+'</a></li>';
                $("li.dropdown > ul.msg").each(function(index, el) {
                    $(el).prepend(li);
                });
                var msg_count = $("li.dropdown > a.dropdown-toggle > span.msg_count");
                var count = window.parseInt(msg_count.html()) + 1;
                msg_count.each(function(index, el) {
                   $(el).html(count);
                });
                var msgHint = $("#msgHint");
                if(msgHint.length > 0 && ($(".mySpace").is(":hidden")||$(".mySpace").length==0))
                    $("#msgHint").show();
            }            
        }
        else if(data["type"] == 4){
            var fb_coin = $("#fb_coin");
            if(fb_coin.length > 0){
                if(data["add"] == 1)
                    fb_coin.text(window.parseInt(fb_coin.text()) + window.parseInt(data["coin"]));
                else
                    fb_coin.text(window.parseInt(data["coin"]));
            }
        }
        else if(data["type"] == 5){
            showWarningToast("您的账号在其他地方登录了，如果不是本人操作，请重新登录，并修改自己的密码。");
            clearCookie();
            window.location.href = "/login";
        }
        else if(data["type"] == 6){
            var count_h = $("#group_count").html();
            if(count_h){
                var first = count_h.indexOf("(");
                var next = count_h.indexOf("/");
                var end = count_h.indexOf(")");
                var online = window.parseInt(count_h.substring(first+1,next));
                var all = window.parseInt(count_h.substring(next+1,end));
                if(online != 0)
                  online--;
                all--;
                $("#group_count").html("我的好友("+online+"/"+all+")");
            }
            $("#accordion"+data["uid"]).remove();
        }
        else if(data["type"] == 7){
            var date = (new Date()).getTime();
            var li = '<li><a class="unRead msg7'+date+'" onclick="handle(0,7'+date+',1,true)">系统消息:'+data["content"]+'</a></li>';
            $("li.dropdown > ul.msg").each(function(index, el) {
                $(el).prepend(li);
            });
            var msg_count = $("li.dropdown > a.dropdown-toggle > span.msg_count");
            var count = window.parseInt(msg_count.html()) + 1;
            msg_count.each(function(index, el) {
               $(el).html(count);
            });
            if("show" in data){
                showNoticeToast(data["content"]);
            }
        }
        else if(data["type"] == 8){
            //re-connect friend restore
            var all_uid = data["uid"];
            var l = all_uid.length;
            for(var i=0; i < l; i++){
                onlineState("1", all_uid[i]);
                addFriendCount(false,true);
                var div = $("#accordion"+all_uid[i]);
                div.detach().prependTo('#myFriends');
            }
        }
        else if(data["type"] == 9){
            window.socket.emit("offline", data["uid"]);
        }
        else if(data["type"] == 11){
            window.socket.emit("s_upload", event.data);
        }
      };
      window.ws.onclose = function(event){
        console.log(event);
        delete window.ws;
        if(window.heart)
        {
            clearInterval(window.heart);
            delete window.heart;
        }
        if(window.fbtUID && !window.sock_restart){
            initFriendState();
            $("#userName").css("color","gray");
            window.sock_restart = setInterval(createSock, 2000);
        }           
      };
    }
}
function openDir(fileHash,flag){
    window.socket.emit("open_dir",JSON.stringify({"file_hash":fileHash,"flag":flag}));
}
function clearCookie(){
    var keys=document.cookie.match(/[^ =;]+(?=\=)/g);
    if (keys) {
        for (var i =  keys.length; i--;)
            document.cookie=keys[i]+'=0;expires=' + new Date( 0).toUTCString()
    }    
}
Date.prototype.Format = function(fmt)   
{ //author: meizz   
  var o = {   
    "M+" : this.getMonth()+1,                 //月份   
    "d+" : this.getDate(),                    //日   
    "h+" : this.getHours(),                   //小时   
    "m+" : this.getMinutes(),                 //分   
    "s+" : this.getSeconds(),                 //秒   
    "q+" : Math.floor((this.getMonth()+3)/3), //季度   
    "S"  : this.getMilliseconds()             //毫秒   
  };   
  if(/(y+)/.test(fmt))   
    fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
  for(var k in o)   
    if(new RegExp("("+ k +")").test(fmt))   
  fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
  return fmt;  
}
$.fn.localResizeIMG = function (obj) {
    this.on('change', function () {
        var file = this.files[0];
        var URL = URL || webkitURL;
        var blob = URL.createObjectURL(file);
        // 执行前函数
        if($.isFunction(obj.before)) { obj.before(this, blob, file) };

        _create(blob, file);
        this.value = '';   // 清空临时数据
    });

    function _create(blob) {
        var img = new Image();
        img.src = blob;

        img.onload = function () {
            var that = this;
            //生成比例
            w = obj.width || w;
            h = w;

            //生成canvas
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            $(canvas).attr({width : w, height : h});
            ctx.drawImage(that, 0, 0, w, h);
            var base64 = canvas.toDataURL('image/jpeg', obj.quality || 0.8 );
            obj.success(base64);
        };
    }
};
//toast
/*
只要使用这个函数就可以调用toast效果！
showSuccessToast('要点碧莲好嘛！');
*/
function showSuccessToast() {
    $().toastmessage('showSuccessToast', "Success Dialog which is fading away ...");
}
function showSuccessToast(words) {
    $().toastmessage('showSuccessToast', words);
}
function showStickySuccessToast() {
    $().toastmessage('showToast', {
        text     : 'Success Dialog which is sticky',
        sticky   : true,
        position : 'top-right',
        type     : 'success',
        closeText: '',
        close    : function () {
            console.log("toast is closed ...");
        }
    });
}
function showNoticeToast(data) {
    $().toastmessage('showNoticeToast', data);
}
function showStickyNoticeToast() {
    $().toastmessage('showToast', {
         text     : 'Notice Dialog which is sticky',
         sticky   : true,
         position : 'top-right',
         type     : 'notice',
         closeText: '',
         close    : function () {console.log("toast is closed ...");}
    });
}
function showWarningToast(data) {
    $().toastmessage('showWarningToast', data);
}
function showStickyWarningToast() {
    $().toastmessage('showToast', {
        text     : 'Warning Dialog which is sticky',
        sticky   : true,
        position : 'top-right',
        type     : 'warning',
        closeText: '',
        close    : function () {
            console.log("toast is closed ...");
        }
    });
}
function showErrorToast(data) {
    $().toastmessage('showErrorToast', data);
}
function showStickyErrorToast() {
    $().toastmessage('showToast', {
        text     : 'Error Dialog which is sticky',
        sticky   : true,
        position : 'top-right',
        type     : 'error',
        closeText: '',
        close    : function () {
            console.log("toast is closed ...");
        }
    });
}
/*var w;

function startWorker()
{
if(typeof(Worker)!=="undefined")
{
  if(typeof(w)=="undefined")
    {
    w=new Worker("demo_workers.js");
    }
  w.onmessage = function (event) {
    document.getElementById("result").innerHTML=event.data;
  };
}
else
{
document.getElementById("result").innerHTML="Sorry, your browser
 does not support Web Workers...";
}
}

function stopWorker()
{
w.terminate();
}*/

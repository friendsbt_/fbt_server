$(function(){
	$(".pinned").pin({containerSelector: ".container"});
});
function press(e){
	if (e.keyCode == 13) {
	showSearchResult();
	return false;
	}
}
function show(){
	$("#searchModal").modal('show');
}
$(".close").click(function(event) {
/* Act on the event */
	$("#searchModal").modal('hide');
});
function showSearchResult(){
	var search = $("#nameOfSearch").val();
	var param = {}
	param["op"] = 0;
	param["search"] = search;
	$.post('/myFriend', param, function(data, textStatus, xhr) {
	/*optional stuff to do after success */
	//data =  JSON.parse(data);
	if(data["type"] && data["type"] == 1){
	data = JSON.parse(data["result"]);
	console.log(data);
	$(".friendDiv").empty();
	var len = getJsonLength(data)
	if(len == 0)
	{
	$(".friendDiv").html("抱歉，未能搜索到符合条件的结果。");
	}
	else{
	var i = 0;
	for (;i < len; i++) {
	item = data[i];
	console.log(item);
	if(!item["icon"])
	  break;
	var div = '<div class="col-xs-4 divInSearch noPaddingLR"><div class="col-xs-4 noPaddingLR"><img src="' + item["icon"];
	div = div + '" style="width:70px; height:70px;"></div><div class="col-xs-8 textInSearch">' + item["nick_name"] +'</div>';
	div = div + '<div class="col-xs-8 textInSearch user">'+item["user"]+'</div>';
	div = div + '<div class="col-xs-12 noPaddingLR addFriendDiv"><button class="btn btn-primary btn-xs" ' + 'onclick="addFriend(&apos;'+item["user"]+'&apos;)">+好友</button></div></div>';
	$(".friendDiv").append($(div));
	}
	}
	}
	else{
	$(".friendDiv").html("抱歉，未能搜索到符合条件的结果。");
	}
	});
}
function addFriend(user){
	$(this).disable();
	var param = {}
	param["op"] = 1;
	param["user"] = user;
	$.post('/myFriend', param, function(data, textStatus, xhr) {
	/*optional stuff to do after success */
	console.log(data);
	//data = $.parseJSON(data)
	if(data["type"] && data["type"] == 1)
	{
	/*data = $.parseJSON(data["result"])*/
	showSuccessToast(data["result"]["result"]);
	}
	else{
	showErrorToast(data["error"])
	}
	});
}
function delFriend(user, star, index){
	var param = {}
	param["op"] = 3;
	param["user"] = user;
	param["star"] = star;
	var that = this;
	$.post('/myFriend', param, function(data, textStatus, xhr) {
	/*optional stuff to do after success */
	console.log(data);
	//data = $.parseJSON(data)
	if(data["type"] && data["type"] == 1)
	{
	var count_h = $("#group_count").html();
	var first = count_h.indexOf("(");
	var next = count_h.indexOf("/");
	var online = window.parseInt(count_h.substr(first+1,1));
	var all = window.parseInt(count_h.substr(next+1,1));
	if(online != 0)
	  online--;
	all--;
	$("#group_count").html("第一分组("+online+"/"+all+")");
	$("#accordion"+index).remove();
	/*data = $.parseJSON(data["result"])*/
	showSuccessToast(data["result"]["result"]);
	}
	else{
	showErrorToast(data["error"])
	}
	});
}
function starFriend(user){
	var param = {}
	param["op"] = 4;
	param["user"] = user;
	$.post('/myFriend', param, function(data, textStatus, xhr) {
	/*optional stuff to do after success */
	console.log(data);
	//data = $.parseJSON(data)
	if(data["type"] && data["type"] == 1)
	{
	var t = $(".friendStar").parents(".friendOp")
	$(".friendStar").remove();
	var b = '<b class="textOfUnfriend friendUnStar" onclick="unStarFriend(&apos;'+user;
	b = b + '&apos;)"><span class="glyphicon glyphicon-star"></span>取消星标好友</b>';
	t.append($(b))
	/*data = $.parseJSON(data["result"])*/
	showSuccessToast(data["result"]["result"]);
	}
	else{
	showErrorToast(data["error"])
	}
	});
}
function unStarFriend(user){
	var param = {}
	param["op"] = 5;
	param["user"] = user;
	$.post('/myFriend', param, function(data, textStatus, xhr) {
	/*optional stuff to do after success */
	console.log(data);
	//data = $.parseJSON(data)
	if(data["type"] && data["type"] == 1)
	{
	var t = $(".friendUnStar").parents(".friendOp");
	$(".friendUnStar").remove();
	var b = '<b class="textOfUnfriend friendStar" onclick="starFriend(&apos;'+user;
	b = b + '&apos;)"><span class="glyphicon glyphicon-star"></span>星标好友</b>';
	t.append($(b))
	/*data = $.parseJSON(data["result"])*/
	showSuccessToast(data["result"]["result"]);
	}
	else{
	showErrorToast(data["error"])
	}
	});
}
function publish(){
	var content = $(".myTextArea").val();
	param = {}
	param["op"] = 0
	param["param"] = content
	$.post('/myShuo', param, function(data, textStatus, xhr) {
	/*optional stuff to do after success */
	console.log(data);
	//data = $.parseJSON(data)
	if(data["type"] && data["type"] == 1)
	{
	/*data = $.parseJSON(data["result"])*/
	$(".shuoShuo").html(content);
	$(".myTextArea").val("");
	showSuccessToast(data["result"]["result"]);
	}
	else{
	showErrorToast(data["error"])
	}
	});
}
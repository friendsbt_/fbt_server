//download part
//type 0 is download, 1 is error, 2 is action, 3 is pause, 4 is resume, 5 is cancel
//6 is cancel callback
//action 0 is html, 1 is inc, 2 is append, 3 is css, 4 is attr
function action(obj, act, val){
  if(act == 0){
    $(obj).html(val);
  }
  else if(act == 1){
    $(obj).text(window.parseInt($(obj).text())+1);
  }
  else if(act == 2){
    $(obj).append(val);
  }
  else if(act == 3){
    $(obj).css(val);
  } 
  else if(act == 4){
    $(obj).attr(val);
  } 
}
function downloadFile(flag,fileHash,isPrivateDownload,isContinue,progress) {
  var whichHtmlElement = fileHash+$.trim(flag);
  if(!$("#download_file_hash"+whichHtmlElement)[0].hasAttribute('error') || $("#collapseMyDownload"+whichHtmlElement).is(':hidden'))
    handleComment(fileHash,3,flag);
  if($("#download_file_hash"+whichHtmlElement).attr("data") == "1")
    return;
  if(!isContinue && !$("#download_file_hash"+whichHtmlElement).attr("download") && $("#container_download_resources").children("#item"+fileHash).length > 0)
  {
    showWarningToast("此资源不需要重复下载。");
    $('#download_progress' + whichHtmlElement).html("无需重复下载");
    return;
  }
/*  if(window.isFirst != 1)
    
  else
    window.isFirst = 1;*/
  $("#download_file_hash"+whichHtmlElement).attr("data", "1");
  $("#download_file_hash"+whichHtmlElement).attr('error','1').removeAttr('error');
  $("#paused"+whichHtmlElement).text(0);
  var btn_pause = $("#button_pause_download"+whichHtmlElement);
  if(btn_pause.hasClass('glyphicon-play'))
    btn_pause.removeClass('glyphicon-play').addClass('glyphicon-pause');
  /*if(!window.state_html[fileHash+""])
  {
    window.state_html[fileHash+""] = $("#collapseMyDownload"+whichHtmlElement).html();
  }
  else{
    $("#collapseMyDownload"+whichHtmlElement).html(window.state_html[fileHash+""]);
  }*/
  console.log("Sock have created , send");
  if(isContinue)
    window.socket.emit('download', JSON.stringify({"progress":progress,"type":0,"private": isPrivateDownload,"hash":fileHash, "html":whichHtmlElement}));
  else if($("#download_file_hash"+whichHtmlElement).attr("download"))
    window.socket.emit('download', JSON.stringify({"progress":0,"type":0,"private": isPrivateDownload,"hash":fileHash, "html":whichHtmlElement}));
  else
    window.socket.emit('download', JSON.stringify({"type":0,"private": isPrivateDownload,"hash":fileHash, "html":whichHtmlElement}));
}
function download(which){
  $("#download_file_hash"+which).click();
}
function pauseFileDownload(fileHash) {
  console.log("pauseFileDownload");
  window.socket.emit('pauseFileDownload', JSON.stringify({"type":3, "hash":fileHash}));
}

function resumeFileDownload(fileHash,flag) {
  console.log("resumeFileDownload");
  window.socket.emit('resumeFileDownload', JSON.stringify({"type":4, "hash":fileHash}));
}

function cancelFileDownload(fileHash,flag) {
  var whichHtmlElement = fileHash+$.trim(flag);
  console.log("cancelFileDownload");
  window.socket.emit('cancelFileDownload', JSON.stringify({"type":5, "html":whichHtmlElement ,"hash":fileHash}));
}
function pause(th){
  if(th.className == 'glyphicon glyphicon-play'){
  th.className = 'glyphicon glyphicon-pause';
  }else{
  th.className = 'glyphicon glyphicon-play';
  }
}
function handleComment(fileHash,flag,f){
  //console.log(fileHash);
  if(flag == 4){
    $.get('/inform?uid='+window.fbtUID+"&file_hash="+fileHash, function(data) {
      /*optional stuff to do after success */
      var tmp;
      try{
        tmp = JSON.parse(data);
      }
      catch(e){
        tmp = data;
      }
      if(tmp["type"] == 0)
        showErrorToast("举报失败，请重试");
      else
        showSuccessToast("您的举报信息已提交，我们将及时处理。");
    },true);
    return;
  }
  fileHash = fileHash + $.trim(f);
  //console.log(fileHash);
  var ids = ["#collapseAllComment", "#collapseMyComment", "#collapseMyDownload"];
  var comment = $("#comment"+fileHash);
  if(comment.is(":hidden"))
    comment.show();
  else
    comment.hide();
  var data = comment.attr("collapse");
  var tmp = comment.attr("show");
  if(data)
  {
    $(ids[window.parseInt(data)-1]+fileHash).hide();
    $(ids[window.parseInt(data)-1]+fileHash).slideUp();
    if(flag == window.parseInt(data) && tmp && window.parseInt(tmp) == 1){
      comment.attr("show",0);
      return;
    }
  }  
  comment.attr("show",1);
  comment.attr("collapse",flag);
  $(ids[flag-1]+fileHash).slideDown();
  $(ids[flag-1]+fileHash).show();
  if(flag == 3){
    $("#collapseMyDownload"+fileHash).css("overflow","inherit");
  }
  comment.hide();
}
function longer(x){
  document.getElementById(x).style.width="100%";
}
function shorter(x){
  document.getElementById(x).style.width="60%";
}
//resource part
function getResListCallback(len,flag,res,container,page_tips,resource_type) {
    var type = res["type"];
    if (type == 1) {
        var html = res["html"];
        var resourceList = res["resource_list"];
        //console.log(resourceList);
        if($(container).children().length == 0 || resourceList){
          var resourceContainer = $(container);
          resourceContainer.append(html);
          resourceContainer.find('.container_score').slice(-resourceList.length).each(function (index, element) {
              var myScore = 0;
              var fileHash = resourceList[index]['file_hash'];
              var grade = resourceList[index]['grades'];
              if(window.fbtUID in grade){
                  myScore=resourceList[index]['grades'][window.fbtUID]/2;
              }            
              $(this).raty({
                  half: true,
                  //hints: ['1', '2', '3', '4', '5'],
                  path: "images",
                  starOff: 'star-off.png',
                  starOn: 'star-on.png',
                  size: 30,
                  score: myScore, //parseFloat($(this).find('.text_score_value').text()),
                  // target: '#result',
                  targetKeep: true,
                  click: function (score, evt) {
                      //这里就能获取用户的分数
                      //alert('u selected '+score);
                      //var grades=[0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10];
                      score=Math.round(score*2);
                      //var score = $(this).find('input[name="score"]').val();
                      $.post("/res", {"op":6,"fileHash":fileHash,"score": score}, function(item, textStatus, xhr){
                          //console.log(item);
                          var tmp;
                          try{
                            tmp = JSON.parse(item);
                          }
                          catch(e){
                            tmp = item;
                          }
                          if(tmp["type"] == 0)
                            showErrorToast("评分失败，请重试");
                          else
                            showSuccessToast("评分成功");
                        });
                  }
              });
              var span = '<span class="pst">('+getJsonLength(grade)+'人)</span>';
              $(this).append(span);
          });
          resourceContainer.find("a.new_win").each(function(index, el) {
            $(el).click(function(event) {
              /* Act on the event */
              event.preventDefault();
              window.open($(this).attr("href"), "_blank");
            });
          });
        }
        var MAX_RES_CNT_PER_PAGE = 20; //the server is set to 20
        if (len < MAX_RES_CNT_PER_PAGE) {
          hideGetMoreResourceButton(flag); // no more resources
        } else {
          resetGetMoreResourceButton(flag);
        }
        if(resource_type)
          $(resource_type).show();
        $(page_tips).hide();
        if(searchMode){
          var searchTime = ((new Date()).getTime() - curTime.getTime())/1000;
          var c = "搜索到"+resourceList.length+"个资源，用时"+searchTime+"s";
          $("#search_time").text(c);
          inSearch();
        }
        else{
          notInSearch(flag);
        }
    } else {
      $(page_tips).show();
      $(page_tips).html("<center><h3>资源列表请求失败！</h3></center>");
    }
}
function getHisListCallback(res) {
    var type = res["type"];
    if (type == 1) {
        var html = res["html"];
        var resourceList = res["resource_list"];
        if(!resourceList || (resourceList.length == 0)){
          $("#text_his_page_tips").show();
          $("#text_his_page_tips").html("<center><h3>下载列表为空！</h3></center>");
          return;
        }
        //console.log(html);
        var resourceContainer = $("#container_download_resources");
        resourceContainer.append(html);
        resourceContainer.find('.container_score').slice(-resourceList.length).each(function (index, element) {
            var myScore = 0;
            var fileHash = resourceList[index]['file_hash'];
            var grade = resourceList[index]['grades'];
            if(window.fbtUID in grade){
                myScore=resourceList[index]['grades'][window.fbtUID]/2;
            }            
            $(this).raty({
                half: true,
                //hints: ['1', '2', '3', '4', '5'],
                path: "images",
                starOff: 'star-off.png',
                starOn: 'star-on.png',
                size: 30,
                score: myScore, //parseFloat($(this).find('.text_score_value').text()),
                // target: '#result',
                targetKeep: true,
                click: function (score, evt) {
                    //这里就能获取用户的分数
                    //alert('u selected '+score);
                    //var grades=[0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10];
                    score=Math.round(score*2);
                    //var score = $(this).find('input[name="score"]').val();
                    $.post("/res", {"op":6,"fileHash":fileHash,"score": score}, function(item, textStatus, xhr){
                        //console.log(item);
                        //console.log(item);
                        var tmp;
                        try{
                          tmp = JSON.parse(item);
                        }
                        catch(e){
                          tmp = item;
                        }
                        if(tmp["type"] == 0)
                          showErrorToast("评分失败，请重试");
                        else
                          showSuccessToast("评分成功");
                      });
                }
            });
            var span = '<span class="pst">('+getJsonLength(grade)+'人)</span>';
            $(this).append(span);
        });
        resourceContainer.find("a.new_win").each(function(index, el) {
          $(el).click(function(event) {
            /* Act on the event */
            event.preventDefault();
            window.open($(this).attr("href"), "_blank");
          });
        });
        $("#text_his_page_tips").hide();
    } else {
      $("#text_his_page_tips").show();
      $("#text_his_page_tips").html("<center><h3>资源列表请求失败！</h3></center>");
    }
}
function inSearch(){
  $("#search_time").show();
  $("#select_resource_type").hide();
}
function notInSearch(flag){
  $("#search_time").hide();
  if(flag == 0)
    $("#select_resource_type").show();
}
function hideGetMoreResourceButton(num) {
  $('#btn_get_more_resources'+num).hide();
}
function displayGetMoreResourceButton(num) {
  $('#btn_get_more_resources'+num).show();
}
function resetGetMoreResourceButton(num) {
  $('#btn_get_more_resources'+num).show();
  $("#btn_get_more_resources"+num).attr("disabled", false);
  $("#loadingText"+num).text("查看更多");
  $("#loadingGif"+num).hide();
}
function resetResourcePage() {
    currentPage = 1;
    $('#text_home_page_tips').show();
    $('#text_home_page_tips').html("<center><h3>正在请求资源列表...</h3></center>");
    $('#select_resource_type').hide();
    $('#container_resources').html("");
    hideGetMoreResourceButton(0);
    if(!searchMode){
      $('#input_search_resource').val("");
    }
}
function validKeyWord(keyWord){
    return keyWord.length > 0 && keyWord.length<=20;
}
function refreshResourceView() {
  $("#search_time").hide();
  var sortType = $('#select_resource_type').find("input:checked").val();
  if(searchMode){
    var keyWord=$('#input_search_resource').val().trim();
    if(validKeyWord(keyWord)){
      $.post("/res", {"op":7,"key_word":keyWord, "page": currentPage,  "sort_by": sortType,"user":window.fbtUID}, function(item, textStatus, xhr){
        var tmp;
        try{
          tmp = JSON.parse(item);
        }
        catch(e){
          tmp = item;
        }
        var data = {};
        data["type"] = tmp["type"];
        data["resource_list"] = tmp["resource_list"];
        data["html"] = tmp["html"];
        var len = tmp["resource_list"].length;
        if("len" in tmp)
          len = tmp["len"]
        getResListCallback(len,0,data,'#container_resources','#text_home_page_tips','#select_resource_type');
      });
    }
  }else{
    $.post("/res", {"op":4,"res_type": currentResourceType, "page": currentPage,  "sort_by": sortType,"user":window.fbtUID}, function(item, textStatus, xhr){
      var tmp;
      try{
        tmp = JSON.parse(item);
      }
      catch(e){
        tmp = item;
      }
      var data = {};
      data["type"] = tmp["type"];
      data["resource_list"] = tmp["resource_list"];
      data["html"] = tmp["html"];
      var len = tmp["resource_list"].length;
      if("len" in tmp)
        len = tmp["len"]
      getResListCallback(len,0,data,'#container_resources','#text_home_page_tips','#select_resource_type');
    });
  }
}
function getCurrentCommentCnt(commentText) {
  return parseInt(commentText.replace(/[^0-9]/g, ''));
  }
function commentPress(event,flag,whichHtmlElement, fileHash){
    keynum = event.keyCode || event.which;
    if(event.ctrlKey && keynum == 13 || keynum == 10){
      commentResource(flag,whichHtmlElement, fileHash);
    }
  }
function commentResource(flag,whichHtmlElement, fileHash) {
  whichHtmlElement = whichHtmlElement+$.trim(flag);
  var rawComment = $('#editbox_comment_content' + whichHtmlElement).val().trim();
  if (rawComment.length == 0 || rawComment.length > 200) {
  //TODO FIXME ERROR TIPS
  showNoticeToast("评论过长，请保持在200以内");
  return;
  }
  $.post("/res", {"op":5, "fileHash":fileHash, "comment": rawComment}, function(item, textStatus, xhr){
    $('#editbox_comment_content' + whichHtmlElement).val("");
    if(item["type"] == 0)
        showErrorToast("评论失败，请重试");
    else
        showSuccessToast("评论成功");
  });
  var all_comments = $('#text_all_comments' + whichHtmlElement).html();
  $('#text_all_comments' + whichHtmlElement).html(all_comments + "<strong>我</strong>(刚刚)：" + rawComment + "<br/>");
  var commentsCntText = $('#text_view_comments' + whichHtmlElement).text();
  $('#text_view_comments' + whichHtmlElement).text("查看" + (getCurrentCommentCnt(commentsCntText) + 1) + "人评论");
  $("#collapseMyComment" + whichHtmlElement).slideUp();
  $("#collapseMyComment" + whichHtmlElement).hide();
  $("#comment"+whichHtmlElement).show();
}
function typeChange(){
  var all_tag = {
    "剧集": ["喜剧", "古装", "伦理", "武侠", "纪录片", "玄幻", "冒险", "警匪", "军事", "神话", "科幻", "搞笑", "偶像", "悬疑", "历史", "儿童", "都市", "家庭", "言情"],
    "电影": ["喜剧", "古装", "伦理", "恐怖", "纪录片", "爱情", "动作", "科幻", "武侠", "战争", "犯罪", "惊悚", "剧情", "玄幻", "冒险", "动画"],
    "音乐": ["流行","摇滚","舞曲","电子","HIP-HOP","乡村","民族","古典","音乐剧","轻音乐"],
    "动漫": ["热血","恋爱","搞笑","LOLI","神魔","科幻","真人","美少女","运动","亲子","励志","剧情","校园","历史"],
    "游戏": ["动作","冒险","模拟","角色扮演","休闲","其他"],
    "综艺": ["晚会","生活","访谈","音乐","游戏","旅游","真人秀","美食","益智","搞笑","纪实","汽车"],
    "体育": ["篮球","足球","台球","羽毛球","乒乓球","田径","水上项目","体操","其他"],
    "软件": ["系统","应用","管理","行业","安全防护","多媒体","游戏"],
    "其他": ["其他"],
    "学习": ["公开课","名人名嘴","艺术","伦理社会","理工农医","文史哲法","经管","课件","计算机","职业培训","家庭教育","其他"]
  };
  //console.log($("#resourceMainType option:selected").text());
  var list = all_tag[$.trim($("#resourceMainType option:selected").text())];
  $("#resourceLabel").empty();
  for(var i = 0; i < list.length; i++){
    var item = list[i];
    var tmp = "<option value='"+item+"'>" + item + "</option>";
    $("#resourceLabel").append(tmp);
  }
}
function fileChange(){
  var path = "";
  if(window.fileSel == "1"){
    path=$('#resourceFile').val();
  }
  else if(window.fileSel == "2"){
    path=$('#resourceDir').val();
  }  
  $("#resourceName").val(path);
}
function fileSelChange(t){
  var val = $(t).attr("data");
  if(val == "1"){
    $("#resourceFile").trigger('click');
  }
  else{
    $("#resourceDir").trigger('click');
  }
  window.fileSel = val;
  $(t).parent('li').parent('ul').siblings('button').children('strong').html($(t).html());
}
function fileUpload(){
  $('#err_tips').html("");
  var filePath;
  var val = window.fileSel;
  if(val == "1"){
    filePath=$('#resourceFile').val();
  }
  else{
    filePath=$('#resourceDir').val();
  }
  if (!filePath) {
  $('#err_tips').html("请选择分享的文件");
  return;
  }
  $('#resourceFile').val("");
  $('#resourceDir').val("");
  var mainType = $('#resourceMainType').val();
  var subType = $('#resourceSubType').val();
  var fileName = $('#resourceName').val();
  if (!fileName) { 
  fileName=filePath;
  }
  var label = $('#resourceLabel').val();
  var isPublic = 1;
  if ($("#checkbox_is_public").is(':checked')) {
  isPublic = 0;
  }
  var comment = $.trim($('#resourceComment').val());
  if (!comment) {
    comment = "我很懒，什么都没填。";
  }
  if(comment.length > 200){
    showNoticeToast("评论过长，请保持在200以内");
    return;
  }
  //var grade = $("input:radio[name ='radio_grade']:checked").val();
  var grade = $('#text_my_socre').text();
  if (!grade) {
    grade = 5;
  }
  else
    grade = parseFloat(grade).toFixed(1);//10分制
  var param = {};
  param["op"] = 0;
  param["path"] = filePath;
  param["name"] = fileName;
  param["mainType"] = mainType;
  param["subType"] = subType;
  param["label"] = label;
  param["desc"] = comment;
  param["grade"] = grade;
  param["isPublic"] = isPublic;
  param["nick_name"] = window.nick_name;
  if(val == '3'){
    param["isDir"] = 1;
  }
  else{
    param["isDir"] = 0;
  }
  /*var time = (new Date()).getTime();
  param["time"] = time;*/
  $.ajax({
    url:"/res",
    type:"POST",
    data:param,
    timeout:600000,
    dataType:"json",
    success:function(item){
      //console.log(item);
      
    },
    error:function(){
      console.log("upload error");
      showErrorToast("上传失败，请检查文件是否存在或者重试");
      window.upload_count--;
      if(window.upload_count == 0)
        $("a#tab3 > span.uploadHint").hide();
    }
  });
  //hide the file upload dialog
  window.upload_count++;
  $("a#tab3 > span.uploadHint").show();
  showNoticeToast("正在上传，请稍后");
  $('#uploadModal').modal("hide");            
}
function show_to_top(){
  var bt = $("div.show").find('.topcontrol');
    if(bt.length > 0){
      var st = $(window).scrollTop(); 
     if(st > 30){ 
             bt.show(); 
     }else{ 
             bt.hide(); 
     }
  } 
}
function firstGetRes(){
  if(!window.ws)
  {
    return;
  }
  clearInterval(window.getRes);
  delete window.getRes;
  $.post("/res", {"op":10,"user":window.fbtUID}, function(item, textStatus, xhr){
    var tmp;
    try{
      tmp = JSON.parse(item);
    }
    catch(e){
      tmp = item;
    }
    //render history
    var his = {};
    his["resource_list"] = tmp["his_list"];
    his["type"] = tmp["type"];
    his["html"] = tmp["his_html"];
    getHisListCallback(his);
  });
  $.post("/res", {"op":4,"user":window.fbtUID}, function(item, textStatus, xhr){
    var tmp;
    try{
      tmp = JSON.parse(item);
    }
    catch(e){
      tmp = item;
    }
    var data = {};
    data["type"] = tmp["type"];
    data["resource_list"] = tmp["resource_list"];
    data["html"] = tmp["html"];
    var len = tmp["resource_list"].length;
    if("len" in tmp)
      len = tmp["len"]
    getResListCallback(len,0,data,'#container_resources','#text_home_page_tips','#select_resource_type');
    if(("first" in window.setting) && window.setting["first"] != 0){
      $("a#set").trigger('click');
      window.setting["first"] = 0;
      window.socket.emit("first");
    }
  });
}
function index_init(){
  console.log("index init");
  window.fbtUID = window.parseInt(getCookie("fbt_user_id"));
  window.nick_name = $.trim($("#nick_name").text());
  console.log(window.nick_name);
  resourceMainType = {0: "电影", 1: "剧集", 2: "音乐", 3: "动漫", 4: "游戏", 5: "综艺", 6: "体育", 7: "软件", 8: "其他"};
  resourceTypes = Object.keys(resourceMainType);
  window.state_html = {};
  window.singleChat = {};
  window.setting = {};
  window.uploadSize = {};
  window.upload_count = 0; 
  $(window).scroll(function() {
      show_to_top();     
  });
  $.get("/html/modal_index.html", function(data){
    $("body").append(data);
    //$("#menu").addClass("dropdown-menu");
    $.get("/setting/init", function(data){
      window.setting = JSON.parse(data);
      if (setting["downloadSaveDir"]) {
        $('#text_download_path').text(setting["downloadSaveDir"]);
      } else {
          $('#text_download_path').text("尚未设置下载目录");
      }
      var tray = $("#settingModal").find("label#exit_tray");
      if(setting["tray"] == 0){
        tray.trigger('click');
        tray.trigger('click');
      }
      else{
        tray.trigger('click');
      }
      var auto = $("#settingModal").find("label#auto_login");
      if(setting["boot"] == 0){
        auto.trigger('click');
        auto.trigger('click');
      }
      else{
        auto.trigger('click');
      }
    },true);
    $("#button_download_path").click(function(){
      $("#editbox_download_path").trigger('click');
    });
    $('#star').raty({
      half: true,
      //hints: ['1', '2', '3', '4', '5'],
      path: "images",
      starOff: 'star-off.png',
      starOn: 'star-on.png',
      size: 30,
      // target: '#result',
      targetKeep: true,
      click: function (score, evt) {
          //这里就能获取用户的分数
          //alert('u selected '+score);
          score=Math.round(score*2);
          $('#text_my_socre').text(score);
          //var score = $(this).find('input[name="score"]').val();
      }
    });
    var label = $("#uploadModal").find("div.modal-footer").find("label.checkbox");
    label.trigger('click');
    label.trigger('click');
    $("#resourceComment").val("");         
  });
  window.getRes = setInterval(firstGetRes,1000);                    
  $('.home_nav > li > a.clickable').click(function () {
    $(".home_nav > li > a.clickable").css({"border-bottom": "3px solid #ecf0f1"});
    $(this).css({"border-bottom": "3px solid #1abc9c"});
  });
  $("#tab4").click(function() {
    /* Act on the event */
    if($(this).attr("data") == 1 || $(this).attr("data") == "1")
      return;
    showNoticeToast("这里是好友资源的集中营，下载无需F币");
    $(this).attr("data","1");
    var that = this;
    if($("#text_all_friend_tips").is(":hidden")){
      $("#text_all_friend_tips").html("<center><h3>正在努力请求资源列表...</h3></center>");
      $("#container_allFriend_resources").empty();
      $("#btn_get_more_resources2").hide();
      $("#text_all_friend_tips").show();
    }   
    $.post("/res", {"op":9,"user":window.fbtUID}, function(item, textStatus, xhr){
      var tmp;
      try{
        tmp = JSON.parse(item);
      }
      catch(e){
        tmp = item;
      }
      var data = {};
      data["type"] = tmp["type"];
      data["resource_list"] = tmp["resource_list"];
      data["html"] = tmp["html"];
      var len = tmp["resource_list"].length;
      if("len" in tmp)
        len = tmp["len"]
      getResListCallback(len,2,data,'#container_allFriend_resources','#text_all_friend_tips');
      $(that).attr("data","0");
    });
  });

  currentFPage = 1;
  currentPage = 1;
  currentResourceType = 0; //movie
  searchMode=false;
  curTime = 0;
  $("#btn_get_more_resources0").click(function () {
      $(this).attr("disabled", true);
      $("#loadingText0").text("正在获取更多...");
      $("#loadingGif0").show();
      curTime = new Date();
      currentPage += 1;
      refreshResourceView();
  });
  $("#btn_get_more_resources2").click(function () {
      $(this).attr("disabled", true);
      $("#loadingText2").text("正在获取更多...");
      $("#loadingGif2").show();
      currentFPage += 1;
      $.post("/res", {"op":9, "user":window.fbtUID,"page": currentFPage}, function(item, textStatus, xhr){
        var tmp;
        try{
          tmp = JSON.parse(item);
        }
        catch(e){
          tmp = item;
        }
        var data = {};
        data["type"] = tmp["type"];
        data["resource_list"] = tmp["resource_list"];
        data["html"] = tmp["html"];
        var len = tmp["resource_list"].length;
        if("len" in tmp)
          len = tmp["len"]
        getResListCallback(len,2,data,'#container_allFriend_resources','#text_all_friend_tips');
      });
  });
  $('#list_resource_view a').each(function (index) {
    $(this).on("click", function () {
      $("#list_resource_view").find(".sel").removeClass('sel').addClass('not_sel');
      $(this).addClass('sel').removeClass('not_sel');
      if($(this).attr("data")){
        if(window.platform == 'darwin')
        {
          showWarningToast("很抱歉，您的操作系统暂时不支持该功能");
          return;
        }
        if($("#container_resources").children('iframe').length > 0)
          return;
        var frame = $("<iframe></iframe>");
        frame.attr("src","http://127.0.0.1:12345/ipv6");
        frame.attr("width", $(document).width());
        frame.attr("height", $(document).height());
        var left = $("#container_resources").css("margin-left");
        var p_left = $("#container_resources").css("padding-left");
        left = window.parseInt(left.replace("px","")) + window.parseInt(p_left.replace("px",""));
        frame.css("margin-left",-left+"px");
        $("#container_resources").html(frame);
        $("#btn_get_more_resources0").hide();
        return;
      }
      if(!$("#select_resource_type").find('input:radio[name="radio"]').first().is(':checked')){
        $("#list_resource_view").attr("data","1");
        $("#select_resource_type").find('input:radio[name="radio"]').first().click();
      }        
      searchMode=false;
      currentResourceType = index;
      resetResourcePage();
      refreshResourceView();
    });
  });
  $("#select_resource_type").find('input:radio[name="radio"]').change(
    function(){
        if ($(this).is(':checked')) {
          if($("#list_resource_view").attr("data") == "1")
          {
            $("#list_resource_view").attr("data","0");
            return;
          }
          resetResourcePage();
          refreshResourceView();
        }
  });
  $('#input_search_resource').keyup(function(e){
      if(e.keyCode == 13)//enter key
      {
        searchMode=true;
        curTime = new Date();
        var keyWord=$('#input_search_resource').val().trim();
        if(validKeyWord(keyWord)){
            if(!$("#resources").hasClass('active')){
              $("#tab1").click();
            }
            resetResourcePage();
            refreshResourceView();
        }
      }
  });
  $("#tab3").click(function(event) {
    /* Act on the event */
    $("#resourceName").val("");
    $("#resourceComment").val("");
  });
  //openDir("","");//TODO FIXME
}

function downloadGrumble(){
  showNoticeToast("已经添加到我的下载");
  $('#tab2').find(".downloadHint").fadeIn('400', function() {
    setTimeout(out, 2000);
  });
  function out(){
    $('#tab2').find(".downloadHint").fadeOut('400', function() {
      
    });
  }
}
function copyMsg(dest){
  var tmp = $(".index li.dropdown");
  var a = tmp.find("a.dropdown-toggle");
  var ul = tmp.find("ul.menu");
  var a_clone = a.clone(true,true);
  var ul_clone = ul.clone(true,true);
  var d = $(dest).find("li.dropdown");
  d.append(a_clone);
  d.append(ul_clone);
}
function settingChange() {
  var value = $("#editbox_download_path").val();
  if(value)
    $('#text_download_path').text(value);
}


function saveSettings(){
  var path = $('#text_download_path').text();
  var tray;
  var boot;
  if($("#auto_login").hasClass('checked'))
    boot = 1;
  else
    boot = 0;
  if($("#exit_tray").hasClass('checked'))
    tray = 1;
  else
    tray = 0;
  $.get("/setting/save?path="+path+"&tray="+tray+"&boot="+boot, function(data){
    data = JSON.parse(data);
    if (data["type"] == 1) {
      //点击保存设置后 隐藏对话框
      $('#settingModal').modal('hide');
      window.setting["downloadSaveDir"] = $("#editbox_download_path").val();
      showSuccessToast("保存成功");
    } else {
      $('#text_download_path').text('无效下载路径');
      showErrorToast("保存失败");
    }
  },true); 
}
function resetSettings(){
  window.setting["downloadSaveDir"] = window.setting["defaultDownloadDir"];
  $('#text_download_path').text(window.setting["downloadSaveDir"]);
  var path = window.setting["downloadSaveDir"];
  var tray = 1;
  var boot = 0;
  $.get("/setting/save?path="+path+"&tray="+tray+"&boot="+boot, function(data){
    if (data == 0) {
      showSuccessToast("恢复默认成功");
    } else {
      $('#text_download_path').text('无效下载路径');
    }
  },true); 
}
function handle(nick,id, sender, flag){
  //console.log($(".msg"+id));
  if(!$(".msg"+id).hasClass('unRead'))
  {
    showNoticeToast("消息已处理");
    return;
  }
  if(flag)
  {
    $(".msg"+id).each(function(index, el) {
      $(el).removeClass('unRead').addClass('read');
    });    
    var msg_count = $("li.dropdown > a.dropdown-toggle > span.msg_count");
    var count = window.parseInt(msg_count.html());
    if(count != 0)
      count --;
    msg_count.each(function(index, el) {
      $(el).html(count);
    });
    return;
  }
  var add = $("#addModal");
  add.find("#addModalNick").text(nick);
  add.find("#addModalOk").attr("mid",id);
  add.find("#addModalOk").attr("send",sender);
  add.modal("show");
}
function handleFriend(flag){
  var id = $("#addModal").find("#addModalOk").attr("mid");
  var sender = $("#addModal").find("#addModalOk").attr("send");
  var param = {};
  param["op"] = 2;
  param["user"] = sender;
  param["id"] = id;
  param["flag"] = flag;
  $("#addModal").modal("hide");
  $.post('/myFriend', param, function(data, textStatus, xhr) {
  /*optional stuff to do after success */
  //console.log(data);
  //data = $.parseJSON(data)
  if(data["type"] && data["type"] == 1)
  {
  $(".msg"+id).each(function(index, el) {
      $(el).removeClass('unRead').addClass('read');
    });
  /*data = $.parseJSON(data["result"])*/
  showSuccessToast(data["result"]["result"]);
  var msg_count = $("li.dropdown > a.dropdown-toggle > span.msg_count");
  var count = window.parseInt(msg_count.html());
  if(count != 0)
    count --;
  msg_count.each(function(index, el) {
    $(el).html(count);
  });
  }
  else{
  showErrorToast(data["error"]);
  }
  });
}

// Http Adapter
function getFromHttp(listname) {
  var data = [];
  switch(listname) {
    case 'week-list':
      var topn = 20;
      // 伪造Ajax数据
      for(var i=1; i<=topn; i++) {
        var tmp = [];
        var img = 'avatar.gif';
        var rank = i;
        var user = 'Alex' + i;
        var total_credit = (topn-i+1)*1000;
        var week_credit = (total_credit)/10;
        tmp.push('<img class="avatar" src="'+img+'"> ' + rank);
        tmp.push(user);
        tmp.push(total_credit);
        tmp.push(week_credit);
        data.push(tmp);
      }
      break;
    case 'month-list':
      var topn = 20;
      // 伪造Ajax数据
      for(var i=1; i<=topn; i++) {
        var tmp = [];
        var img = 'avatar.gif';
        var rank = i;
        var user = 'Bob' + i;
        var total_credit = (topn-i+1)*1000;
        var month_credit = (total_credit)/10;
        tmp.push('<img class="avatar" src="'+img+'"> ' + rank);
        tmp.push(user);
        tmp.push(total_credit);
        tmp.push(month_credit);
        data.push(tmp);
      }
      break;
    case 'F-list':
      var topn = 50;
      // 伪造Ajax数据
      for(var i=1; i<=topn; i++) {
        var tmp = [];
        var img = 'avatar.gif';
        var rank = i;
        var user = 'Master' + i;
        var total_credit = (topn-i+1)*1000;
        tmp.push('<img class="avatar" src="'+img+'"> ' + rank);
        tmp.push(user);
        tmp.push(total_credit);
        data.push(tmp);
      }
      break;
    default:
      break;
  }
  return data;
}

// 拉取榜单
function pullList(listname) {
  var data = getFromHttp(listname);
  for(var i=0; i<data.length; i++) {
    var tr = '<tr><td>'+data[i].join('</td><td>')+'</td></tr>';
    $('.'+listname).append(tr);
  }
}

// Main entrance
$(document).ready(function() {
  // 选择榜单
  $('.title').click(function() {
    $('.title').removeClass('active');
    $(this).addClass('active');
    $('table').hide();
    var listnames = {
      0:'week-list',
      1:'month-list',
      2:'F-list'
    };
    var listindex = $(this).index();
    $('.'+listnames[listindex]+' tbody').empty();
    $('.'+listnames[listindex]).show();
    pullList(listnames[listindex]);
  });

  // 默认显示周排行榜
  $('.title')[0].click();
});
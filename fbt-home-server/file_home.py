#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path
from tornado.options import define, options

from tornado.httpclient import  AsyncHTTPClient
#import json
import tornado.gen

define("port", default=80, help="run on the given port", type=int)

# fbt_url="localhost"
# fbt_port="8888"
download_url = "http://pan.baidu.com/s/1IJi70"
fbt_version = "2.0.7"
fbt_url="www.friendsbt.com"
fbt_port="8888"
class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", NewHomeHandler),
            (r"/w.html", NewWHomeHandler),
            (r"/index.html", NewHomeHandler),
            (r"/software.html", NewSoftwareHandler),
            (r"/fmall.html", NewFmallHandler),
            (r"/fmall", NewFmallHandler),
            (r"/campus.html", NewCampusHandler),
            (r"/about.html", NewAboutHandler),
            (r"/guifan.html", NewGuifanHandler),
            (r"/tv", TVHandler),
            (r"/agreement.htm", AgreementHandler),
            (r"/mianze", MianzeHandler),
            (r"/xuexi.html", XuexiHandler),
        ]
        settings = dict(
            cookie_secret="123",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
        )
        tornado.web.Application.__init__(self, handlers, **settings)
		
class HomeHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self):
        self.render("TODO.html")
        uid = self.get_argument("uid", None)
        if uid:
            try:
                uid = long(uid)
                http_client = AsyncHTTPClient()
                http_client.fetch("http://"+fbt_url+":"+fbt_port+"/sns_share?uid="+str(uid),callback=self.sns_share_callback)
            except:
                pass #pass error

    def sns_share_callback(self, response):
        pass
        # if response.error:
        #     print "Error:", response.error
        # else:
        #     print "Sns share ok:"+response.body

class NewHomeHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self):
        if "study" in self.request.host:
            self.redirect("http://www.校园星空.com")
            return
        uid = self.get_argument("uid", None)
        ip = self.request.remote_ip
        if uid and ip:
            try:
                http_client = AsyncHTTPClient()
                http_client.fetch("http://"+fbt_url+":"+fbt_port+"/sns_share?uid="+str(uid)+"&ip="+str(ip), callback=self.sns_share_callback)
            except:
                pass #pass error
        ua = self.request.headers['User-Agent']  
        isMobile = True if ua.lower().find("mobile") != -1 else False

        force = self.get_argument('force', None)
        if isMobile and not force:
            self.redirect('/w.html')
        else:
            self.render("index.html", download_url = download_url)

    def sns_share_callback(self, response):
        pass

class NewWHomeHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self):
        self.render("w.html", download_url = download_url)

class NewSoftwareHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self):
        self.render("software.html", download_url = download_url, fbt_version = fbt_version)

class NewFmallHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self):
        self.render("fmall.html", download_url = download_url)

class NewCampusHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self):
        self.render("campus.html", download_url = download_url)

class NewAboutHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self):
        self.render("about.html", download_url = download_url)

class NewGuifanHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self):
        self.render("guifan.html", download_url = download_url)

class TVHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self):
        self.render("tv.html")

class AgreementHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self):
        self.render("agreement.html", download_url = download_url)

class MianzeHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self):
        self.render("mianze.htm", download_url = download_url)

class XuexiHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self):
        self.render("xuexi.html", download_url = download_url)

def main():
    tornado.options.parse_command_line()
    app = Application()
    app.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()

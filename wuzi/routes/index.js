var express = require('express');
var router = express.Router();
var base = require('../util/base');

router.get('/', function (req, res) {
    var sid = req.query.sid;  //获取地址栏的sid后面的值，
    //如果sid不为空，
    if (sid) {
        base.getUserInfo(sid, function (err, replies) {
            if (!err && replies) {
                console.log("get user info:"+replies+" sid:"+sid);
                res.render("home", {
                    title: 'Home',
                    userData: replies
                });
            } else {
                console.log("get user info2:"+err+" sid:"+sid);
                res.render("home", {
                    title: 'Home',
                });
            }
        });
    } else {
        res.render("home", {
            title: 'Home'
        });
    }
})
module.exports = router;

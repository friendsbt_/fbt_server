﻿var module = require("./five");
var app = new module.FiveChess();
app.SetConfig({
	"ListenPort" : 3005,
	"RoomTotal" : 100,
	"MaxClientNum" : 300
});
app.Startup();

/**
 * Created by yulei on 2015/8/13.
 */

var redis = require('redis');
var mongojs = require('mongojs');

//create redis
var DEBUG = false;
var LOCAL_RUN = false;
var client = null;
var HOST = null;
var PORT = 6382;

var databaseUrl = "fbt"; // "username:password@example.com/mydb"
var collections = ["coins_of_user"];
var db = mongojs(databaseUrl, collections);
var HOW_MUCH = 100;

if(!LOCAL_RUN){
    var PASSWORD = '123-fbt-session-cache-!@#';
    if(DEBUG){
        HOST = '192.168.208.82';
        var databaseUrl = "192.168.207.145/fbt";
        db = mongojs(databaseUrl, collections);
    } else {
        HOST = '192.168.207.72';
        var databaseUrl = "192.168.195.32/fbt";
        db = mongojs(databaseUrl, collections);
    }
    client = redis.createClient(PORT, HOST);//如果没写默认是game.friendsbt.com;
    client.on('error', function (err) {
        console.log('Error' + err);
    });
    client.auth(PASSWORD, function (err){
        if(!err) console.log("auth OK.");
    });
} else{
    client = redis.createClient();
    var databaseUrl = "fbt"; // "username:password@example.com/mydb"
    db = mongojs(databaseUrl, collections);
}

function subDBCoin(uid, callback){
    db.coins_of_user.findOne({uid: uid}, function(err, coin) {
        if( err || !coin) {
            console.log(err);
            console.log("No coin found");
        } else {
            console.log("coin:"+JSON.stringify(coin));
            coin["total_coins"] -= HOW_MUCH;
            if(coin["total_coins"] < 0) {
                coin["total_coins"] = 0;
            }
            if("coins_by_study" in coin && coin["coins_by_study"] > coin["total_coins"]) {
                coin["coins_by_study"] = coin["total_coins"];
            }
            db.coins_of_user.save(coin, function(err, saved) {
                callback(err);
                console.log("update coin Ok:"+ err);
            });
        }
    });
}

function addDBCoin(uid, callback){
    db.coins_of_user.update({uid: uid}, {"$inc": {"total_coins": HOW_MUCH}}, function(err,  updated) {
        if(err) {
            callback("add coin failed");
            console.log(err);
            console.log("addCoin failed");
        } else {
            if(updated["ok"] == 1){
                callback(null);
            }else{
                callback("add coin failed");
            }
            console.log("coin add updated:"+ JSON.stringify(updated));
        }
    });
}

exports.plusCoin = function(sid, howMuch, callback){
    client.get('session:' + sid, function (err, replies) {
        if(!err && replies){
            var userInfo = JSON.parse(replies);
            userInfo.total_coins += howMuch;
            console.log("coin add OK:"+userInfo["uid"]+" nick:"+userInfo["nick_name"])
            client.setex('session:' + sid, 24*3600,JSON.stringify(userInfo), callback);
        }
    });
}

exports.getUserInfo = function (sid, callback){
    client.get('session:' + sid, function (err, replies) {
        callback(err, replies);
    });
}

exports.addDBCoin = addDBCoin;
exports.subDBCoin = subDBCoin;

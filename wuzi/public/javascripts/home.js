﻿/**
 * Created by yulei on 2015/8/4.
 */
$(function () {
    
    var timeFlag; //用于终止计时
    var timeFlag2;
    var MSG_ALL = 0;//发送到所有用户
    var MSG_TO = 1;//发送指定用户
    var MSG_ROOM = 2;//向指定桌发送消息

    var STAT_NORMAL = 0;//无状态
    var STAT_READY = 1;//准备
    var STAT_START = 2;//游戏中

    var COLOR_BLACK = 1;//黑色
    var COLOR_WHITE = 2;//白色

    var g_Connected = false;
    var g_Host = "game.friendsbt.com";
    var g_Port = 3005;
    var g_Info = {
        "id": 0,
        "university": userData.university,    //用户所在大学
        "icon": userData.icon,   //用户积分
        "status": 0,
        "roomIdx": -1,
        "posIdx": -1,
        "gameID": -1,
    };
    var app = new FiveChess(g_Host, g_Port);
    app.on("init", function (data) {//登陆返回
        if (data.ret == 1) {
            g_Info.id = data.info.id;
            g_Info.nickname = data.info.nickname;
            g_Info.status = data.info.status;
            initRoomList(data.room);
            initUserList(data.list);
        } else {
            showMessage("登陆失败");
        }
    })
        .on("close", function (data) {//退出程序
            $("#user-" + data.id).remove();

            //本房间有人退出
            if (data.roomIdx == g_Info.roomIdx) {
                removeRoom(data.posIdx);
                if (g_Info.status == STAT_START) {
                    g_Info.status = STAT_NORMAL;
                    updateRoom(g_Info.posIdx, g_Info);
                    timeFlag = 0;
                    timeFlag2 = 0;
                    showMessage("由于对方逃跑，现判定为你获胜，你获得了100F。","gameNotOver");
                    $("#room-" + data.roomIdx).removeClass("room_item_start");
                    //把用户的信息发送到服务器，进行加分运算；
                    app.addCoins(g_Info);

                }
            }

            //大厅有人退出
            if (data.roomIdx != -1) {
                var name = $('#room-' + data.roomIdx + '-name-' + data.posIdx);
                var icon = $('#room-' + data.roomIdx + '-icon-' + data.posIdx);
                name.html('');
                icon.removeClass('yes').addClass('no');
                icon.css("background-image", "url(/images/player_no.gif)");
                icon.removeClass('imgSize');
            }
        }).on("join", function (data) {//新用户加入大厅
            if (g_Info.id != data.id) {
                $("#list-box").append(makeHtmlUserList(data));
            }
        }).on("joinRoom", function (data) {//用户加入房间

            var name = $('#room-' + data.roomIdx + '-name-' + data.posIdx);
            var icon = $('#room-' + data.roomIdx + '-icon-' + data.posIdx);
            name.html(data.nickname);
            icon.removeClass('no').addClass('yes');
            showUserInformation(data.roomIdx, data.posIdx, data);
            //大厅显示用户头像信息
            icon.css("background-image", 'url(http://static.friendsbt.com' + data.icon + ')');
            icon.addClass('imgSize');

            //自己
            if (data.id == g_Info.id) {
                g_Info.roomIdx = data.roomIdx;
                g_Info.posIdx = data.posIdx;
                g_Info.status = STAT_NORMAL;
            } else if (data.roomIdx == g_Info.roomIdx) {
                //有人加入本房间
                data.status = STAT_NORMAL;
                updateRoom(data.posIdx, data);
            }
        }).on("ready", function (data) {//准备
            //看看积分是否够100，不够100实现跳转
            if (data.total_coins < 100) {
                //判断是在房间里发送消息
                if (g_Info.id == data.id) {
                    showMessage("对不起，你的积分不够100F，暂时不能进入游戏。请单击“确定”按钮返回", "url");
                    return;
                }
            } else {
                //本房间有人准备
                if (data.roomIdx == g_Info.roomIdx) {
                    updateRoom(data.posIdx, data);
                    //提示对方准备进入游戏；
                    if ($("#game_ready").val() == "准备") {
                        showMessage("请未准备的玩家单击'准备'按钮准备进入游戏");
                    }
                }
                //提示大厅有人准备
                var stat = (data.status == STAT_NORMAL ?
                    "无状态" : (data.status == STAT_READY ? "已准备" : "游戏中"));
                $("#user-" + data.id + " span").html(stat);
                //提示用户准备
            }
        }).on("roomInfo", function (data) {//获取房间信息
            initRoom(data[0], data[1]);
        }).on("start", function (data) {//开始游戏
            g_Info.status = STAT_START;
            g_Info.color = data.color;
            g_Info.allowDraw = data.allowDraw;
            g_Info.gameID = data.gameID;
            if (g_Info.allowDraw) {
                $("div.room_chess").css("cursor", "pointer");
            } else {
                $("div.room_chess").css("cursor", "no-drop");
            }
            //开始游戏后显示时间
            $("div.room_chess div").remove();//清除棋子
            $("#game_ready").val("游戏中...");
            showMessage("游戏开始啦......100F一局哦，加油！！！");
            //游戏开始显示游戏中
            $("#room-p1-status").html("游戏中");
            $("#room-p2-status").html("游戏中");
            //updateRoom(data.posIdx, data);
            if (g_Info.allowDraw) {
                showTime(data.color, "firstTime");
            } else {
                timeFlag = false;
                showTime2();
            }
        }).on("startInfo", function (data) {//有游戏开始了
            $("#room-" + data.roomIdx).addClass("room_item_start");
            $("#user-" + data.player1 + " span").html("游戏中");
            $("#user-" + data.player2 + " span").html("游戏中");
        }).on("overInfo", function (data) {//游戏结束了
            $("#room-" + data.roomIdx).removeClass("room_item_start");
            $("#user-" + data.player1 + " span").html("无状态");
            $("#user-" + data.player2 + " span").html("无状态");
            if (data.roomIdx == g_Info.roomIdx) {
                //更新房间另一个成员的状态
                var p = (data.player1 == g_Info.id ? 2 : 1);
                $("#room-p" + p + "-status").html("未准备");
            }
        }).on("leaveRoom", function (data) {//离开房间
            var name = $('#room-' + data.roomIdx + '-name-' + data.posIdx);
            var icon = $('#room-' + data.roomIdx + '-icon-' + data.posIdx);
            name.html('');
            icon.removeClass('yes').addClass('no');
            //离开房间不显示用户头像
            icon.css("background-image", "url(/images/player_no.gif)");
            icon.removeClass('imgSize');
            if (data.id == g_Info.id) {//更新自己的信息
                g_Info.roomIdx = -1;
                g_Info.posIdx = -1;
                changeTag("room_list");
            } else if (data.roomIdx == g_Info.roomIdx) {//本房间有人退出
                removeRoom(data.posIdx);
            }
        }).on("joinRoomError", function (data) {//加入房间失败
            showMessage("加入房间失败");
        }).on("message", function (data) {//接受消息
            if (data.type == MSG_ALL) {
                $("#msg-content").append("<span>" + data.nickname + ": " + data.body + "</br>" + "</span>");
                $("#msg-content").scrollTop($("#msg-content")[0].scrollHeight);
            } else if (data.type == MSG_TO) {
                $("#msg-content").append("<span style=\"color:#339933\">" + data.nickname + ": " + data.body + "</br>" + "</span>");
                $("#msg-content").scrollTop($("#msg-content")[0].scrollHeight);
            } else if (data.type == MSG_ROOM) {
                $("#room-msg-content").append("<span>" + data.nickname + ": " + data.body + "</br>" + "</span>");
                $("#room-msg-content").scrollTop($("#room-msg-content")[0].scrollHeight);
            }
        }).on("drawChess", function (data) {//落子
            var left = data.x * 35 + 5;
            var top = data.y * 35 + 5;
            var css = (data.color == COLOR_BLACK ? "black" : "white");
            var html = '<div id="chess-' + data.x + '-' + data.y + '" style="left:' + left + 'px;top:' + top + 'px" class="' + css + '"></div>';
            $("div.room_chess").append(html);
            if ($("div.room_chess .cur").length == 0) {
                $("div.room_chess").append('<div class="cur"></div>');
            }
            $("div.room_chess .cur").css({
                left: left,
                top: top
            });
            if (data.id == g_Info.id) {
                g_Info.allowDraw = false;
                $("div.room_chess").css("cursor", "no-drop");
                timeFlag = false;
                showTime2();
            } else {
                g_Info.allowDraw = true;
                $("div.room_chess").css("cursor", "pointer");
                timeFlag2 = false;
                showTime(data.color);
            }
        }).on("winner", function (data) {//胜利
            g_Info.status = STAT_NORMAL;
            g_Info.allowDraw = false;
            g_Info.total_coins = data.total_coins;
            updateRoom(g_Info.posIdx, g_Info);
            if (data.timeOut == 1) {
                showMessage("由于对方时间到了，现判定为你获胜,你获得了100F！！！", true);
            } else {
                showMessage("恭喜你获胜了，你赢了100F，赶快去商城兑换礼品或现金吧！！！", true);
            }
            timeFlag = false;  //计时停止
            timeFlag2 = false;  //计时停止
        }).on("loser", function (data) {//失败
            g_Info.total_coins = data.total_coins;
            g_Info.status = STAT_NORMAL;
            g_Info.allowDraw = false;
            showUserInformation(data.roomIdx, data.posIdx, data.data);
            updateRoom(g_Info.posIdx, g_Info);
            if (data.timeOut == 1) {
                showMessage("游戏时间到，你输了100F！！！", true);
            } else {
                showMessage("对不起，你输了100F。再来一局证明自己！！！", true);
            }
            timeFlag = false; //计时停止
            timeFlag2 = false; //计时停止
        }).on("upDataLCoins", function (data) {
            var p1 = (data.winner.posIdx == 0 ? 1 : 2);
            $("#room-p" + p1 + "-jifen").html(data.winner.total_coins);
            var p2 = (data.loser.posIdx == 0 ? 1 : 2);
            $("#room-p" + p2 + "-jifen").html(data.loser.total_coins);
        }).on("upDataWCoins", function (data) {
            var p1 = (data.winner.posIdx == 0 ? 1 : 2);
            $("#room-p" + p1 + "-jifen").html(data.winner.total_coins);
            var p2 = (data.loser.posIdx == 0 ? 1 : 2);
            $("#room-p" + p2 + "-jifen").html(data.loser.total_coins);
        }).on("upDataCoins", function(data){
            var p  = (data.posIdx == 0 ? 1 : 2);
            $("#room-p" + p + "-jifen").html(data.total_coins);
        });
    //加入房间
    $(document).on("click", "#room-box .player", function () {
        var roomIdx = $(this).closest('.room_item').attr('value');
        var posIdx = $(this).attr('value');
        if ($("#room-" + roomIdx + "-icon-" + posIdx).hasClass("yes")) {
            return;
        }
        if (g_Info.status == STAT_START) {
            showMessage("你正在游戏中，不能加入其它房间");
            return;
        }

        app.joinRoom(roomIdx, posIdx);


    });
    //回车键响应时间发送消息
    $("#msg-input").keypress(function (event) {
        if (event.keyCode == 13) {
            $("#msg-button").click();
        }
    });

    //发送消息
    $("#msg-button").click(function () {
        var msg = $("#msg-input").val();
        if (msg == "") {
            return;
        }
        app.sendAllMsg(msg);
        $("#msg-input").val('');
    });

    //回车键响应时间发送消息
    $("#room-msg-input").keypress(function (event) {
        if (event.keyCode == 13) {
            $("#room-msg-button").click();
        }
    })

    //发送消息到房间内
    $("#room-msg-button").click(function () {
        var msg = $("#room-msg-input").val();
        if (!msg) {
            return;
        }
        app.sendRoomMsg(msg);
        $("#room-msg-input").val("");
    });

    //切换窗口
    $("#tag a").click(function () {
        //this这里代表#tag a,attr('href')代表获取第一个href值，substr(1)代表从1开始截取。
        var id = $(this).attr('href').substr(1);
        //hasclass用于判断是否有css属性“on”;
        if ($(this).hasClass('on')) {
            return false;
        }

        if (g_Info.roomIdx == -1) {
            showMessage("你还没有加入房间");
            return false;
        }

        changeTag(id);
        return false;
    });

    //落子
    $("div.room_chess").click(function (ev) {
        var pageX = ev.pageX;
        var pageY = ev.pageY;
        var x = parseInt((pageX - $(this).offset().left - 5) / 35);
        var y = parseInt((pageY - $(this).offset().top - 5) / 35);

        if (g_Info.roomIdx == -1 || g_Info.status != STAT_START ||
            $("#chess-" + x + '-' + y).length > 0 || g_Info.allowDraw == false) {
            return;
        }

        app.drawChess(g_Info.color, x, y);
    });

    //准备
    $("#game_ready").click(function () {
        if (g_Info.status == STAT_START) {
            return;
        }
        app.ready();
    });

    //退出房间
    $("#game_leave").click(function () {
        if (g_Info.status == STAT_START) {
            showMessage("正在游戏中，你不能退出");
            return;
        }
        app.leaveRoom(g_Info.roomIdx);
    });

    //切换
    function changeTag(tag) {
        if (tag == "room_list") {
            //显示给出的id
            $("#room_list").show();
            //addClass添加样式；
            $("#tag_room_list").addClass("on");
            //隐藏给出的id标签
            $("#room").hide();
            //移除class类
            $("#tag_room").removeClass("on");
        } else {
            $("#room").show();
            $("#tag_room").addClass("on");
            $("#room_list").hide();
            $("#tag_room_list").removeClass("on");
        }
    }

    //生成用户html
    function makeHtmlUserList(data) {
        var stat = (data.status == STAT_READY ? "已准备" : (data.status == STAT_START ? "游戏中" : "无状态"));
        var html = ('<li id="user-' + data.id + '"><span>' + stat + "</span>" + data.nickname + "</li>");
        return html;
    }

    //初始化用户列表
    function initUserList(data) {
        var html = '';
        for (var i = 0; i < data.length; i++) {
            html += makeHtmlUserList(data[i]);
        }
        $("#list-box").html(html);
    }

    //初始化房间列表,就是大厅初始化；
    function initRoomList(data) {
        var html = '';
        for (var idx in data) {
            html += '<div id="room-' + idx + '" value="' + idx + '" class="room_item">';
            html += '<div id="room-' + idx + '-name-1" class="player2">' + (data[idx][1] ? data[idx][1].nickname : "") + '</div>';
            html += '<div class="players">';
            html += '<div value="0" id="room-' + idx + '-icon-0" class="player icon1 ' + (data[idx][0] ? "yes" : "no") + '"></div>';
            html += '<div value="1" id="room-' + idx + '-icon-1" class="player icon2 ' + (data[idx][1] ? "yes" : "no") + '"></div>';
            html += '</div>';
            html += '<div id="room-' + idx + '-name-0" class="player1">' + (data[idx][0] ? data[idx][0].nickname : "") + '</div>';
            html += '<div class="roomnum">- ' + (parseInt(idx) + 1) + ' -</div>';
            html += '</div>';
        }
        $("#room-box").html(html);
        for (var id in data) {
            if (data[id][0]) {
                var imgUrl = 'http://static.friendsbt.com' + data[id][0].icon;
                var imgId = $('#room-' + id + '-icon-0');
                imgId.css("background-image", 'url(' + imgUrl + ')');
                imgId.addClass('imgSize');
            }
            if (data[id][1]) {
                var imgUrl = 'http://static.friendsbt.com' + data[id][1].icon;
                var imgId = $('#room-' + id + '-icon-1');
                imgId.css("background-image", 'url(' + imgUrl + ')');
                imgId.addClass('imgSize');
            }
            showUserInformation(id, '0', data[id][0]);
            showUserInformation(id, '1', data[id][1]);
        }
    }

    //初始化房间
    function initRoom(player1, player2) {
        //清除消息和棋子
        $("div.room_chess div").remove();
        $("#room-msg-content span").remove();

        //tag样式切换
        changeTag("room");

        //玩家1
        if (player1) {
            updateRoom(0, player1);
        } else {
            removeRoom(0);
        }

        //玩家2
        if (player2) {
            updateRoom(1, player2);
        } else {
            removeRoom(1);
        }
    }

    //更新房间人员
    function updateRoom(posIdx, player) {
        var p = (posIdx == 0 ? 1 : 2);
        var s = (player.status == STAT_NORMAL ? "未准备" : (player.status == STAT_READY ? "已准备" : "游戏中"));
        $("#room-p" + p + "-nickname").html(player.nickname);
        $("#room-p" + p + "-status").html(s);
        $("#room-p" + p + "-jifen").html(player.total_coins);
        $("#room-p" + p + "-university").html(player.university);
//      $("#room-p" + p + "-name").html(userData);
        $("#room-p" + p + "-img").html('<img style="border-radius: 50%" width="120px" height="120px" src="http://static.friendsbt.com' + player.icon + '">');
        //根据g_Info 是否等于 player.id可以确定一个唯一的玩家
        if (g_Info.id == player.id) {
            var b = (player.status == STAT_NORMAL ? "准备" : (player.status == STAT_READY ? "取消" : "游戏中..."));
            $("#game_ready").val(b);
        }
    }

    //从本房间移除另一个成员
    function removeRoom(posIdx) {
        var p = (posIdx == 0 ? 1 : 2);
        $("#room-p" + p + "-nickname").html('&nbsp;');
        $("#room-p" + p + "-status").html("&nbsp;");
        $("#room-p" + p + "-jifen").html('&nbsp;');
        $("#room-p" + p + "-university").html('&nbsp;');
        $("#room-p" + p + "-img").html('<img style="border-radius: 50%" width="120px" height="120px" src="/images/no_player.gif">');
    }

    //窗口显示提示信息
    function showMessage(data, clear) {
        $("#msgIcon").addClass("ui-icon ui-icon-alert");
        $("#msgShow").html(data);
        $("#dialog-message").dialog({
            position: {my: "bottom", at: "center", of: window},
            modal: true,
            buttons: {
                "确定": function () {
                    $(this).dialog("close");
                    if (clear == true) {
                        $("div.room_chess div").remove();
                        $("#showTime1").html(" ");
                        $("#showTime2").html(" ");
                    }
                    if (clear == "url") {
                        window.location.href = "http://study.friendsbt.com/";
                    }
                    if( clear == "gameNotOver"){
                        $("div.room_chess div").remove();
                        $("#showTime1").html(" ");
                        $("#showTime2").html(" ");
                    }
                }
            }
        });
    }

    //在大厅将鼠标放在用户头像上时显示用户信息
    function showUserInformation(roomIdx, posIdx, data) {
        var html = null;
        $("#room-" + roomIdx + "-icon-" + posIdx).hover(function () {
                var html = '<div style="box-shadow: 6px 6px 6px black;padding: 2px;color: #000088;position: relative;background: #EEEEEE;width: 120px;height: 120px;margin-left: 10px;margin-top: 45px;z-index: 100"  ><ul>' +
                    '<li>' + '昵称：' + data.nickname + '</li>' +
                    '<li>' + '学校：' + data.university + '</li>' +
                    '<li>' + '院系：' + data.college + '</li>' +
                    '<li>' + '总积分：' + data.total_coins + '</li>' +
                    '<li>' + '学习积分：' + data.study_coins + '</li>' +
                    '</ul></div>';
                if ((parseInt(roomIdx) + 1) % 5 == 0 && (parseInt(posIdx) + 1) % 2 == 0) {
                    html = '<div style="margin-left:-100px">' + html + '</div>';
                }
                ;
                if ((parseInt(roomIdx) > 94)) {
                    html = '<div style="margin-top:-155px ">' + html + '</div>';
                }
                if ($("#room-" + roomIdx + "-icon-" + posIdx).is('.yes')) {
                    $(this).html(html);
                    return;
                }
                ;
            }, function () {
                $(this).html('&nbsp;');
            }
        );
    }

    //显示计时函数
    function showTime(color, firstTime) {
        var userColor = {"color": color, "firstTime": firstTime};
        timeFlag = true;
        var timeDiv = $("#showTime1");
        var times = 2 * 60;
        if (timeFlag) {
            var timeObj = window.setInterval(countTimer, 1000);
        }
        if (!timeFlag) {
            window.clearInterval(timeObj);//消除调用；
            return;
        }
        function countTimer() {
            var t = "你的剩余时间:" + Math.floor(times / 60) + "分" + times % 60 + "秒";
            timeDiv.html(t);
            if (times > 0 && timeFlag) {
                times--;
            } else {
                //倒计时结束，提交表单
                if (times == 0) {
                    app.sendWinnerColor(userColor);
                }
                window.clearInterval(timeObj);//消除调用；
                return;
            }
        }
    }

    //现实时间函数2
    function showTime2() {
        timeFlag2 = true;
        var timeDiv = $("#showTime2");
        var times = 2 * 60;
        if (timeFlag2) {
            var timeObj = window.setInterval(countTimer, 1000);
        }
        if (!timeFlag2) {
            window.clearInterval(timeObj);//消除调用；
            return;
        }
        function countTimer() {
            var t = "对方的剩余时间:" + Math.floor(times / 60) + "分" + times % 60 + "秒";
            timeDiv.html(t);
            if (times > 0 && timeFlag2) {
                times--;
            } else {
                //倒计时结束，提交表单
                if (times == 0) {
                }
                window.clearInterval(timeObj);//消除调用；
                return;
            }
        }
    }
    //浏览器刷新或者关闭事件
    window.onbeforeunload = onbeforeunload_handler;
    function onbeforeunload_handler(){
        var warning;
        if(g_Info.status == STAT_START){
            warning = "您正在游戏中，强行退出将自动扣除100F";
        }else{
            warning = "";
        }
        return warning;
    };


    if (app.connect() == false) {
        showMessage("error: " + app.getError());
        return false;
    }
    app.sendAllMsg("加入大厅");
});

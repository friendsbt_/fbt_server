var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');


var multer = require('multer');
var mongoose = require('mongoose');
//var session = require('express-session');

var routes = require('./routes/index');
var users = require('./routes/users');


var module5chess = require("./five");
//require文件，然后新建对象，fiveChessServer变量就相当于FiveChess函数，
//然后调用FiveChess里头的函数，fiveChessServer.SetConfig就是FiveChess里头的
//SetConfig函数，
var fiveChessServer = new module5chess.FiveChess();
fiveChessServer.SetConfig({
    "ListenPort": 3005,
    "RoomTotal": 100,
    "MaxClientNum": 300
});
fiveChessServer.Startup();


//global.dbHandel = require('./database/dbHandel');
//global.db = mongoose.connect("mongodb://game.friendsbt.com:27017/nodedb");

var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require("ejs").__express);
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
// app.use(multer()); // TODO maybe reuse
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;

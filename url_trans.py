import os

def recursive_replace(root, pattern, replace):
    for dir, subdirs, names in os.walk(root):
        for name in names:
            path = os.path.join(dir, name)
            if ".js" in path or ".html" in path or ".py" in path or ".css" in path:
		    try:
			text = open(path).read()
			if pattern in text:
			     open(path, 'w').write(text.replace(pattern, replace))
		    except IOError as e:
			print "pass file: %s because error: %s" % (path,e)

if __name__ == "__main__":
    static_url="static.friendsbt.com"
    test_url="test.friendsbt.com"
    local_url="127.0.0.1:9999"
    print "Choose your operation first"
    print "1) change %s to %s" % (static_url, test_url)
    print "2) change %s to %s" % (test_url, static_url)
    print "3) change %s to %s" % (test_url, local_url)
    print "4) change %s to %s" % (local_url, test_url)
    number = raw_input('Enter your choice: ')
    if number == '1':
        recursive_replace("fbt_server_py",static_url, test_url)
    elif number == '2':
        recursive_replace("fbt_server_py",test_url, static_url)
    elif number == '3':
        recursive_replace("fbt_server_py",test_url, local_url)
    elif number == '4':
        recursive_replace("fbt_server_py",local_url, test_url)
    else:
        print "choose error. exited."
        exit(-1)
    print "Process Ok"

#!/usr/bin/python

import os
import re
#list files

def listFiles(dirPath):
    fileList = []
    for root, dirs, files in os.walk(dirPath):
        for fileObj in files:
            fileList.append(os.path.join(root, fileObj))
    return fileList


def main(type):
    fileDir = "wuzi"
    regex = ur'FUNC_SYS_ADD_ACCDETAIL'
    fileList = listFiles(fileDir)
    for fileObj in fileList:
        f = open(fileObj, 'r+')
        all_the_lines = f.readlines()
        f.seek(0)
        f.truncate()
        for line in all_the_lines:
            if(type == "2"):
                f.write(line.replace('game.friendsbt.com', 'localhost'))   #fbt into localhost
            if(type == "1"):
               f.write(line.replace('localhost', 'game.friendsbt.com'))
        f.close()
if __name__ == '__main__':
    print("1.localhost to game.friendsbt.com")
    print("2.game.friendsbt.com to localhost")
    type = raw_input('please input type number:')
    main(type)

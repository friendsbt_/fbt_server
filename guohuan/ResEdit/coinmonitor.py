# -*- coding:utf-8 -*-

import pymongo
import sys
import string
import datetime
	

if __name__=="__main__":
	conn=pymongo.Connection("localhost",27017)
	db=conn.fbt
	diff=50
	filename="./coinlog/coin_change"+str(datetime.date.today())
	reload(sys)
	sys.setdefaultencoding("utf-8")
	fout=open(filename,"w")
	fout.write("coin changed over %d\n" % diff)
	users=db.coins_of_user.find({},{"uid":1,"total_coins":1,"coins_of_ystday":1})
	for user in users:
		change=user["total_coins"]-user["coins_of_ystday"]
		if change>diff:
			nickname=db.users.find_one({"uid":user["uid"]},{"nick_name":1})["nick_name"]
			fout.write("%d %s(%d): %d -> %d\n" % (change,nickname,user["uid"],user["coins_of_ystday"],user["total_coins"]))
	fout.close()

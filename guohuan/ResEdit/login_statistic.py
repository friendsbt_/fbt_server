# -*- coding:utf-8 -*-

import pymongo
import datetime
import json

if __name__=="__main__":
	# conn=pymongo.Connection("192.168.195.122",27017)
	# db=conn.fbt
	import sys;
	sys.path.append("/home/fbt/latest_fbt_server_py");
	import mongoclient;
	db=mongoclient.fbt;
	lst=db.coins_of_user.find({},{"offline_at":1})
	t2=datetime.datetime.now()
	day=0
	week=0
	for user in lst:
		if "offline_at" not in user:
			continue
		t1=datetime.datetime.fromtimestamp(user["offline_at"])
		t=t2-t1
		# print t.days
		if t.days<1:
			day+=1
			week+=1
		elif t.days<7:
			week+=1
	ret={}
	ret["logtime"]=str(t2)
	ret["day"]=day
	ret["week"]=week
	ans=json.dumps(ret)
	with file("/home/fbt/resEdit/login_statistic.txt","a") as f:
		f.write(ans)
		f.write("\n")

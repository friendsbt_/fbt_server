# -*- coding:utf-8 -*-

import os.path
import tornado.locale
import tornado.httpserver
import tornado.ioloop
import tornado.web
from tornado import gen
import uuid
from pprint import pprint
import pdb

# import pymongo
# import motor
import sys
import time
from datetime import datetime,timedelta,date
import json
import tornadoredis
sys.path.append("/home/fbt/latest_fbt_server_py")
from study_resource_manager import StudyResourceManager
from fb_manager import FBCoinManager
from course_db import CourseDB
coursedb=CourseDB()
import redis
import traceback
# from settings import mongo_machines, REPLICASET_NAME
# from pymongo import ReadPreference

# import motorclient
import mongoclient
import tornado.ioloop
import msg_handle

# pub_client = tornadoredis.Client(host="fbt167",password="123-fbt-pub-sub-!@#",port=6380)
pub_client = redis.StrictRedis(password="123-fbt-pub-sub-!@#",host="10.10.95.244",port=6380)
# channel="tornado"
channel="fbt:resource:pass-audit"
channel2="fbt:resource:hidden"
channel3="fbt:resource:revealed"

# db0=motorclient.fbt
db0=mongoclient.fbt

auto_pass_coin=3000

@gen.coroutine
def audit_res(file_id=None):
    db=db0.all_resources
    db2=db0.user_msg
    db3=db0.users
    # msg = {}
    # msg["type"]=0
    # msg["isRead"] = 0
    # msg["id"] = str(uuid.uuid1().int)
    # msg["sender"] = "0"
    # msg["nick"] = "0"
    # msg["time"] = datetime.now().strftime('%Y-%m-%d %H:%M')
    msg = {}
    msg["type"] = 0
    #msg["isRead"] = 0
    msg["id"] = str(uuid.uuid1().int)
    msg["sender"] = "0"
    msg["nick"] = "0"
    msg["time"] = datetime.now().strftime('%Y-%m-%d %H:%M')

    res=db.find_one({"file_id":file_id,"tobeaudit":1},{"owners":1,'file_name':1})
    if not res or not res['owners']:
        return
    file_name=res['file_name']
    user_coin=db0.coins_of_user.find_one({"uid":long(res["owners"][0]["uid"])},{'total_coins':1})['total_coins']

    if user_coin>auto_pass_coin:
        db.update({"file_id":file_id},{"$unset":{"hidden":1,"tobeaudit":1}})
        db.update({"file_id":file_id},{"$set":{"auditor":'system'}})
        msg["content"] = u" 您的资源 "+file_name+u" 已被审核通过！ 请多保持在线，方便他人下载您的资源。"
        try:
            dic1={}
            dic1["file_id"]=file_id
            dic1["file_name"]=file_name
            dic1["uid"]=res["owners"][0]["uid"]
            msg1=json.dumps(dic1,ensure_ascii=False)
            pub_client.publish(channel, msg1)
        except Exception, e:
            pass
            # traceback.print_exc()
        finally:
            # msg1=json.dumps(dic1,ensure_ascii=False)
            # pub_client.publish(channel, msg1)
            pass
        print file_id,' passed.'
            
    else:
        db.update({"file_id":file_id},{"$unset":{"tobeaudit":1}})
        # db.update({"file_id":file_id},{"$set":{"auditor":self.current_user}})
        tomove=db.find_one({"file_id":file_id})
        tomove["reason"]='积分少于%d'%auto_pass_coin
        db0.recycle_resources.save(tomove)
        db.remove({"file_id":file_id})
        msg["content"] = " 亲，由于你的积分不足%d，暂时没法上传资源哦！当然，你可以加群 FBT供水小组 165435963 参与资源建设，积分多多！"%auto_pass_coin
        print file_id,' not passed.'
    if res:
        uid=int(res["owners"][0]["uid"])
        u_mail=db3.find_one({"uid":uid},{"user":1})
        # pprint(int(res["owners"][0]["uid"]))
        # pprint(u_mail)
        u_mail=u_mail["user"]
        # pprint(u_mail)
        # db2.update({"user":u_mail},{"$push":{"msg_list":msg}})
        msg_handle.addMsg(u_mail, msg, "", "", "")
        print "mail:"+u_mail,msg
        exit()


def main():
    ress=db0.all_resources.find({"tobeaudit":1},{"file_id"})
    for res in ress:
        audit_res(res['file_id'])
        # break
    # study_ress=db0.auditing_study_resources.find({"unpass_reason":None},{'file_id'})
    # study_ress_lst=yield study_ress.to_list(length=(yield study_ress.count()))
    # for study_res in study_ress_lst:
    #     yield audit_study(study_res['file_id'])
    #     exit()

if __name__=='__main__':
    # tornado.ioloop.IOLoop.current().run_sync(main)
    main()
    
# -*- coding:utf-8 -*-

import os.path
import tornado.locale
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado import gen

import pymongo

from tornado.options import define,options
define("port",default=6666,help="run on the given port",type=int)

class Application(tornado.web.Application):
	def __init__(self):
		handlers=[
			(r"/start",StartHandler),
			(r"/find/(.*)",FindHandler),
			(r"/edit/(.*)",EditHandler),
			(r"/recommended/",RecommendedHandler),
			]
		settings=dict(
			template_path=os.path.join(os.path.dirname(__file__),"templates"),
			static_path=os.path.join(os.path.dirname(__file__),"static"),
			ui_modules={"oneres":ResModule},
			debug=True,
				)
		conn=pymongo.Connection("localhost",27017)
		self.db=conn.fbt
		tornado.web.Application.__init__(self,handlers,**settings)


class RecommendedHandler(tornado.web.RequestHandler):
	def get(self):
		coll=self.application.db.all_resources
		res=coll.find_one()
		self.render(
			"recommended.html",
			page_title="Recommended Resources",
			res=res
			)

class ResModule(tornado.web.UIModule):
	def render(self,res):
		return self.render_string("modules/oneres.html",res=res)

class StartHandler(tornado.web.RequestHandler):
	def get(self):
		self.render("start.html")

class FindHandler(tornado.web.RequestHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	def get(self,file_name):
		coll=self.application.db.all_resources
		file_doc=coll.find({"file_name":{"$regex":file_name,"$options":"-i"}})
		if file_doc.count():
			self.render("find.html",res=file_doc)
		else:
			self.set_status(404)
			self.write({"error":"file not found"})

class EditHandler(tornado.web.RequestHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	def get(self,file_id):
		coll=self.application.db.all_resources
		file_doc=coll.find_one({"file_id":file_id})
		if file_doc:
			self.render("edit.html",res=file_doc)
		else:
			self.set_status(404)
			self.write({"error":"file not found"})

	def post(self,file_id=None):
		res_fields=['file_id','file_name','main_type','sub_type','download_num','hidden','public']
		coll=self.application.db.all_resources
		res=dict()
		if file_id:
			res=coll.find_one({"file_id":file_id})
		for key in res_fields:
			res[key]=self.get_argument(key)

		if file_id:
			coll.save(res)
		self.write("You have changed it successfully.")
		self.redirect("/find/"+res['file_name'])
		

if __name__=="__main__":
	tornado.options.parse_command_line()
	http_server=tornado.httpserver.HTTPServer(Application())
	http_server.listen(options.port)
	tornado.ioloop.IOLoop.instance().start()


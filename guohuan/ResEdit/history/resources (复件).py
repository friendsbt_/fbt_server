# -*- coding:utf-8 -*-

import os.path
import tornado.locale
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

import pymongo

from tornado.options import define,options
define("port",default=8000,help="run on the given port",type=int)

class Application(tornado.web.Application):
	def __init__(self):
		handlers=[
			(r"/res/(.*)",ResoucesHandler),
			(r"/edit/(.*)",EditHandler),
			(r"/recommended/",RecommendedHandler),
			]
		settings=dict(
			template_path=os.path.join(os.path.dirname(__file__),"templates"),
			static_path=os.path.join(os.path.dirname(__file__),"static"),
			#ui_modules={"Book":BookModule},
			debug=True,
				)
		conn=pymongo.Connection("localhost",27017)
		self.db=conn.fbt
		tornado.web.Application.__init__(self,handlers,**settings)

class ResoucesHandler(tornado.web.RequestHandler):
	def get(self,file_name):
		coll=self.application.db.all_resources
		file_doc=coll.find_one({"file_name":file_name})
		if file_doc:
			del file_doc["_id"]
			self.write(file_doc)
			self.write("\n")
		else:
			self.set_status(404)
			self.write({"error":"file not found"})

class RecommendedHandler(tornado.web.RequestHandler):
	def get(self):
		coll=self.application.db.all_resources
		res=coll.find_one()
		self.render(
			"recommended.html",
			page_title="Recommended Resources",
			res=res
			)

class ResModule(tornado.web.UIModule):
	def render(self,res):
		return self.render_string("modules/res.html",res=res)

class EditHandler(tornado.web.RequestHandler):
	def get(self,file_name):
		coll=self.application.db.all_resources
		file_doc=coll.find_one({"file_name":file_name})
		if file_doc:
			self.render("edit.html",res=file_doc)
		else:
			self.set_status(404)
			self.write({"error":"file not found"})

'''	def post(self,file_id=None):
		res_fields=['file_id','file_name']
		coll=self.application.db.all_resources
		res=dict()
		if file_id:
			res=coll.find_one({"file_id":file_id})
		for key in res_fields:
			res[key]=self.get_argument(key,None)

		if file_id:
			coll.save(res)
'''
if __name__=="__main__":
	tornado.options.parse_command_line()
	http_server=tornado.httpserver.HTTPServer(Application())
	http_server.listen(options.port)
	tornado.ioloop.IOLoop.instance().start()

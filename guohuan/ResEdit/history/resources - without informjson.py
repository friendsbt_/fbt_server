# -*- coding:utf-8 -*-

import os.path
import tornado.locale
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado import gen
import uuid
from pprint import pprint

import pymongo
import sys
import copy
import time
from datetime import datetime,timedelta
import json

from tornado.options import define,options
define("port",default=6789,help="run on the given port",type=int)

recycle_reasons=[
"清晰度过低/错标",
"已有同名更优资源",
"不受欢迎格式资源",
"命名不规范",
"非法违规资源",
"其他原因"
]

class Application(tornado.web.Application):
	def __init__(self):
		handlers=[
			(r"/start",StartHandler),
			(r"/find/(.*)",FindHandler),
			(r"/hotfind/(.*)",HotFindHandler),
			(r"/edit/(.*)",EditHandler),
			(r"/rmuser/(.*)",RemoveuserHandler),
			(r"/login", LoginHandler),
			(r"/logout", LogoutHandler),
			(r"/user",UserHandler),
			(r"/finduser/(.*)",FindUserHandler),
			(r"/editcoin/(.*)",EditCoinHandler),
			(r"/coinlog/(.*)",CoinlogHandler),
			(r"/audit(\d*)",AuditHandler),
			(r"/auditing/(.*)",AuditingHandler),
			(r"/inform",InformHandler)
			]
		settings=dict(
			template_path=os.path.join(os.path.dirname(__file__),"templates"),
			static_path=os.path.join(os.path.dirname(__file__),"static"),
			ui_modules={"oneres":ResModule,"oneu":UserModule,"oneurl":OneurlModule},
			debug=True,
			cookie_secret= "abcdef12345",
			login_url= "/login", 
				)
		conn=pymongo.Connection("localhost",27017)
		self.db=conn.fbt
		tornado.web.Application.__init__(self,handlers,**settings)

class BaseHandler(tornado.web.RequestHandler):
	def get_current_user(self):
		# self.set_secure_cookie("user", "", expires_days=None)
		# self.set_secure_cookie("user", "", expires=time.time()+20)
		return self.get_secure_cookie("user")

class UserHandler(BaseHandler):
	@tornado.web.authenticated 
	def get(self):
		self.render("user.html")

class FindUserHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated  
	def get(self,user_name):
		if self.current_user!='root':
			self.write("permission denied.")
			return
		user_name=user_name.strip()
		coll=self.application.db.users
		user_doc=coll.find({"$or":[{"user":user_name},{"nick_name":user_name}]})
		if user_doc.count()==0 and user_name.isdigit():
			user_doc=coll.find({"uid":long(user_name)})
		if user_doc.count():
			self.render("finduser.html",u=user_doc)
		else:
			self.set_status(404)
			#self.write('{"error":"user not found"}<br><a href="/user">返回首页</a>')
			self.render('404.html')

class ResModule(tornado.web.UIModule):
	def render(self,res):
		return self.render_string("modules/oneres.html",res=res)

class UserModule(tornado.web.UIModule):
	def render(self,u):
		return self.render_string("modules/oneu.html",u=u)

class OneurlModule(tornado.web.UIModule):
	def render(self,res):
		return self.render_string("modules/oneurl.html",res=res)

class StartHandler(BaseHandler):
	@tornado.web.authenticated 
	def get(self):
		if self.current_user=='root':
			self.render("start_root.html")
		else:
			self.render("start.html")

class LoginHandler(BaseHandler):
	def get(self):
		self.render("login.html")

	def post(self):
		# ÕâÀï²¹³äÒ»¸ö£¬»ñÈ¡ÓÃ»§ÊäÈë
		uname=self.get_argument("name")
		ukey=self.get_argument("psw")
		if uname=='admin' and ukey=='welovefbt':
			#print(uname)
			#print(ukey)
			self.set_secure_cookie("user", uname, expires=time.time()+6000)
			self.redirect("/start")
		elif uname=='root' and ukey=='Ilovefriendsbt':
			self.set_secure_cookie("user", uname, expires=time.time()+6000)
			self.redirect("/start")
		else:
			self.redirect("/login")

class LogoutHandler(BaseHandler):
	def get(self):
		self.set_secure_cookie("user", "")
		self.redirect("/login")

class FindHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated  
	def get(self,file_name):
		coll=self.application.db.all_resources
		file_name=file_name.strip()
		file_name=file_name.replace("[","\[")
		file_name=file_name.replace("]","\]")
		file_doc=coll.find({"file_name":{"$regex":file_name,"$options":"-i"}})
		if file_doc.count():
			self.render("find.html",res=file_doc)
		else:
			self.set_status(404)
			#self.write('{"error":"file not found"}<br><a href="/start">返回首页</a>')
			self.render('404.html')

class HotFindHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated  
	def get(self,file_name):
		coll=self.application.db.hot_resources
		file_name=file_name.strip()
		file_name=file_name.replace("[","\[")
		file_name=file_name.replace("]","\]")
		file_doc=coll.find({"file_name":{"$regex":file_name,"$options":"-i"}})
		if file_doc.count():
			self.render("hotfind.html",ress=file_doc)
		else:
			self.set_status(404)
			#self.write('{"error":"file not found"}<br><a href="/start">返回首页</a>')
			self.render('404.html')

	def post(self,file_name):
		coll=self.application.db.hot_resources
		file_id=self.get_argument('file_id')
		# print(file_id)
		coll.remove({"file_id":file_id})
		self.write("You have deleted resource:%s successfully."%file_id)

class EditHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,file_id):
		coll=self.application.db.all_resources
		file_doc=coll.find_one({"file_id":file_id})
		if file_doc:
			self.render("edit.html",res=file_doc)
		else:
			self.set_status(404)
			self.write('{"error":"file not found"}<br><a href="/start">返回首页</a>')

	def post(self,file_id=None):
		#res_fields=['file_id','file_name','main_type','sub_type','download_num','hidden','public']
		res_fields=['main_type','public',"sub_type"]
		coll=self.application.db.all_resources
		res={} #dict()
		if file_id:
			res=coll.find_one({"file_id":file_id})
			res['file_name']=self.get_argument('file_name')
			for key in res_fields:
				res[key]=self.get_argument(key)
			res["public"]=int(res["public"])
			res["main_type"]=int(res["main_type"])
			res["sub_type"]=int(res["sub_type"])
			if self.get_argument("hidden","")=="1":
				res["hidden"]=1
			else:
				res["hidden"]=0
			if self.get_argument("sticky","")=="1":
				sticky_day=self.get_argument("sticky_day","3")
				sticky_day=int(sticky_day)
				res["sticky"]=sticky_day
				res2=copy.deepcopy(res)
				res2["hot"]=[]
				res2["hot"].append({})
				res2["hot"][0]["hot_day"] = 50000
				self.application.db.hot_resources.save(res2)
			else:
				if res.get("sticky",0)>=1:
					res["sticky"]=0
					res2=copy.deepcopy(res)
					res2["hot"]=[]
					res2["hot"].append({})
					res2["hot"][0]["hot_day"] = 20
					self.application.db.hot_resources.save(res2)
			coll.save(res)
			self.write("You have changed it successfully.")
		self.redirect("/find/"+res['file_name'])

class EditCoinHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,uid):
		if self.current_user!='root':
			self.write("permission denied.")
			return
		coll=self.application.db.coins_of_user
		user_doc=coll.find_one({"uid":long(uid)})
		#user_doc=coll.find_one({"$or":[{"uid":uid},{"uid":long(uid)}]})
		#print(uid)
		if user_doc:
			self.render("editcoin.html",res=user_doc)
		else:
			self.set_status(404)
			self.write('sorry, user not found<br><a href="/user">返回</a>')

	def post(self,uid=None):
		#res_fields=['uid','total_coins']
		res_fields=['total_coins']
		coll=self.application.db.coins_of_user
		res=dict()
		if uid:
			uid=long(uid)
			res=coll.find_one({"uid":uid})
			for key in res_fields:
				res[key]=self.get_argument(key)
				coins_add=self.get_argument('coins_add')

			if coins_add:
				res['total_coins']=float(res['total_coins'])+float(coins_add)
				coll.save(res)
				self.application.db.fblog.update({'uid' : uid},{"$push":{"log":{"date":datetime.now(),"coin":coins_add,"info":"coins added by admin coin:"+str(res['total_coins'])}}})
				self.write("You have changed it successfully.")
			self.redirect("/editcoin/"+str(res['uid']))
		else:
			self.write('{"error":"user not found"}<br><a href="/user">return</a>')

class CoinlogHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,uid):
		if self.current_user!='root':
			self.write("permission denied.")
			return
		user_doc=self.application.db.resources_of_user.find_one({"uid":long(uid)},{"file_hashes":1})
		if user_doc:
			self.write("该用户共有资源%d个<br>"%len(user_doc["file_hashes"]))
		else:
			self.set_status(404)
			self.write('sorry, user not found<br><a href="/user">返回</a>')
			return

		user_doc=self.application.db.users.find_one({"uid":long(uid)},{"time":1})
		if user_doc:
			self.write("注册时间%s<br>"%user_doc["time"])

		user_doc=self.application.db.fblog.find_one({"uid":long(uid)})
		if user_doc:
			self.write("在线时间%d小时<br><br>"%user_doc["online_time"])
			self.write(" 日期------ 时间---------- 加分规则---- 积分---- 加分---<br>")
			for i in user_doc["log"]:
				self.write("%s %s %s<br>"%(i["date"],i["info"],i["coin"]))


class RemoveuserHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,uid):
		if self.current_user!='root':
			self.write("permission denied.")
			return
		coll=self.application.db.users
		user_doc=coll.find_one({"$or":[{"uid":uid},{"uid":long(uid)}]})
		if user_doc:
			self.render("removeu.html",u=user_doc)
		else:
			self.set_status(404)
			self.write('sorry, user not found<br><a href="/user">返回</a>')

	def post(self,uid=None):
		coll1=self.application.db.users
		coll2=self.application.db.users_removed
		if uid and self.get_argument("rm",None):
			# uid=self.get_argument("uid",None)
			res=coll1.find_one({"uid":long(uid)})
			if res:
				coll1.remove({"uid":long(uid)})
				coll2.save(res)
				self.write("You have removed he/she successfully.")
			else:
				self.write("Error. None found.")
		else:
			self.write("Error.")

class AuditHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,audittype=None):
		coll=self.application.db.all_resources
		# file_doc=coll.find({"hidden":1}).limit(10) #for test
		print audittype
		if '0'<=audittype<='9':
			file_doc=coll.find({"tobeaudit":1,"main_type":int(audittype)}).limit(10)
		else:
			file_doc=coll.find({"tobeaudit":1}).limit(15)
		if file_doc.count():
			self.render("audit.html",res=file_doc)
		else:
			self.render("./auditfinish.html")

class AuditingHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,file_id):
		coll=self.application.db.all_resources
		file_doc=coll.find_one({"file_id":file_id})
		if file_doc:
			self.render("oneresaudit.html",res=file_doc)
		else:
			self.set_status(404)
			self.write('{"error":"file not found"}<br><a href="/start">返回首页</a>')

	def post(self,file_id=None):
		db=self.application.db.all_resources
		db2=self.application.db.user_msg
		db3=self.application.db.users
		msg = {}
		msg["type"]=0
		msg["isRead"] = 0
		msg["id"] = str(uuid.uuid1().int)
		msg["sender"] = "0"
		msg["nick"] = "0"
		msg["time"] = datetime.now().strftime('%Y-%m-%d %H:%M')
		file_name=self.get_argument("file_name","None")
		if self.get_argument("save",None):
			db.update({"file_id":file_id},{"$set":{"file_name":file_name}})
			mt=self.get_argument("Combobox1",None)
			st=self.get_argument("Combobox2",None)
			if mt and st:
				mt=int(mt)
				st=int(st)
				db.update({"file_id":file_id},{"$set":{"main_type":mt,"sub_type":st}})
				self.redirect('/auditing/%s'%str(file_id))
				return
			else:
				self.write("Error.")

		if self.get_argument("yes",None):
			db.update({"file_id":file_id},{"$unset":{"hidden":1,"tobeaudit":1}})
			msg["content"] = " 您的资源 "+file_name+" 已被管理员审核通过 "
		elif self.get_argument("no",None):
			reason=self.get_argument("Combobox3",0)
			reason=int(reason)
			db.update({"file_id":file_id},{"$unset":{"tobeaudit":1}})
			tomove=db.find_one({"file_id":file_id})
			tomove["reason"]=recycle_reasons[reason]
			if reason==5:
				other=self.get_argument("other","")
				if other.strip()=="":
					other=recycle_reasons[reason]
				tomove["reason"]=other
				msg["content"] = " 您的资源 "+file_name+" 未被管理员审核通过，因为 "+other
			msg["content"] = " 您的资源 "+file_name+" 未被管理员审核通过，因为 "+recycle_reasons[reason]
			self.application.db.recycle_resources.save(tomove)
			db.remove({"file_id":file_id})
		res=db.find_one({"file_id":file_id},{"owners":1})
		if res:
			uid=int(res["owners"][0]["uid"])
			u_mail=db3.find_one({"uid":uid},{"user":1})
			# pprint(int(res["owners"][0]["uid"]))
			# pprint(u_mail)
			u_mail=u_mail["user"]
			# pprint(u_mail)
			db2.update({"user":u_mail},{"$push":{"msg_list":msg}})
			# print("mail:"+u_mail)
			# db2.update({"user":"guohuanright@163.com"},{"$push":{"msg_list":msg}})
		self.write(file_id+"<br>"+file_name
			+'<br> 已审查完成！<br><a href="/edit/'+file_id+ '" target="_blank">重新修改资源</a>')
		# self.write(opt)

class InformHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self):
		if self.current_user!='root':
			self.write("permission denied.")
			return
		f = file("/home/fbt/fbt_server_py/inform.json");
		s = json.load(f)
		self.render("inform.html",res=s)
		f.close()

	def post(self):
		pass

if __name__=="__main__":
	reload(sys)
	sys.setdefaultencoding('utf8')
	tornado.options.parse_command_line()
	http_server=tornado.httpserver.HTTPServer(Application())
	http_server.listen(options.port)
	tornado.ioloop.IOLoop.instance().start()


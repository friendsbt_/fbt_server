# -*- coding:utf-8 -*-

import os.path
import tornado.locale
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado import gen

import pymongo

from tornado.options import define,options
define("port",default=6789,help="run on the given port",type=int)

class Application(tornado.web.Application):
	def __init__(self):
		handlers=[
			(r"/start",StartHandler),
			(r"/find/(.*)",FindHandler),
			(r"/edit/(.*)",EditHandler),
			(r"/login", LoginHandler),
			(r"/logout", LogoutHandler),
			(r"/user",UserHandler),
			(r"/finduser/(.*)",FindUserHandler),
			(r"/editcoin/(.*)",EditCoinHandler)
			]
		settings=dict(
			template_path=os.path.join(os.path.dirname(__file__),"templates"),
			static_path=os.path.join(os.path.dirname(__file__),"static"),
			ui_modules={"oneres":ResModule,"oneu":UserModule},
			debug=True,
			cookie_secret= "abcde12345",
			login_url= "/login", 
				)
		conn=pymongo.Connection("localhost",27017)
		self.db=conn.fbt
		tornado.web.Application.__init__(self,handlers,**settings)

class BaseHandler(tornado.web.RequestHandler):
	def get_current_user(self):
		return self.get_secure_cookie("user")

class UserHandler(BaseHandler):
	@tornado.web.authenticated 
	def get(self):
		self.render("user.html")

class FindUserHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated  
	def get(self,user_name):
		user_name=user_name.strip()
		coll=self.application.db.users
		user_doc=coll.find({"$or":[{"user":user_name},{"nick_name":user_name}]})
		if user_doc.count():
			self.render("finduser.html",u=user_doc)
		else:
			self.set_status(404)
			#self.write('{"error":"user not found"}<br><a href="/user">返回首页</a>')
			self.render('404.html')

class ResModule(tornado.web.UIModule):
	def render(self,res):
		return self.render_string("modules/oneres.html",res=res)

class UserModule(tornado.web.UIModule):
	def render(self,u):
		return self.render_string("modules/oneu.html",u=u)

class StartHandler(BaseHandler):
	@tornado.web.authenticated 
	def get(self):
		if self.current_user=='root':
			self.render("start_root.html")
		else:
			self.render("start.html")

class LoginHandler(BaseHandler):
	def get(self):
		self.render("login.html")

	def post(self):
		# ÕâÀï²¹³äÒ»¸ö£¬»ñÈ¡ÓÃ»§ÊäÈë
		uname=self.get_argument("name")
		ukey=self.get_argument("psw")
		if uname=='admin' and ukey=='ilovefbt':
			#print(uname)
			#print(ukey)
			self.set_secure_cookie("user", uname)
			self.redirect("/start")
		elif uname=='root' and ukey=='Ilovefriendsbt':
			self.set_secure_cookie("user", uname)
			self.redirect("/start")
		else:
			self.redirect("/login")

class LogoutHandler(BaseHandler):
	def get(self):
		self.set_secure_cookie("user", "")
		self.redirect("/login")

class FindHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated  
	def get(self,file_name):
		coll=self.application.db.all_resources
		file_name=file_name.strip()
		file_name=file_name.replace("[","\[")
		file_name=file_name.replace("]","\]")
		file_doc=coll.find({"file_name":{"$regex":file_name,"$options":"-i"}})
		if file_doc.count():
			self.render("find.html",res=file_doc)
		else:
			self.set_status(404)
			#self.write('{"error":"file not found"}<br><a href="/start">返回首页</a>')
			self.render('404.html')

class EditHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,file_id):
		coll=self.application.db.all_resources
		file_doc=coll.find_one({"file_id":file_id})
		if file_doc:
			self.render("edit.html",res=file_doc)
		else:
			self.set_status(404)
			self.write('{"error":"file not found"}<br><a href="/start">返回首页</a>')

	def post(self,file_id=None):
		#res_fields=['file_id','file_name','main_type','sub_type','download_num','hidden','public']
		res_fields=['main_type','public']
		coll=self.application.db.all_resources
		res=dict()
		if file_id:
			res=coll.find_one({"file_id":file_id})
		res['file_name']=self.get_argument('file_name')
		for key in res_fields:
			res[key]=self.get_argument(key)
		if self.get_argument("hidden","")=="1":
			res["hidden"]=1
		elif res.get('hidden','')==1:
			res["hidden"]=0
		res["public"]=int(res["public"])
		if file_id:
			coll.save(res)
		self.write("You have changed it successfully.")
		self.redirect("/find/"+res['file_name'])

class EditCoinHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,uid):
		if self.current_user!='root':
			self.write("permission denied.")
			return
		coll=self.application.db.coins_of_user
		user_doc=coll.find_one({"$or":[{"uid":uid},{"uid":long(uid)}]})
		#print(uid)
		if user_doc:
			self.render("editcoin.html",res=user_doc)
		else:
			self.set_status(404)
			self.write('sorry, user not found<br><a href="/user">返回</a>')

	def post(self,uid=None):
		#res_fields=['uid','total_coins']
		res_fields=['total_coins']
		coll=self.application.db.coins_of_user
		res=dict()
		if uid:
			res=coll.find_one({"$or":[{"uid":uid},{"uid":long(uid)}]})
			for key in res_fields:
				res[key]=self.get_argument(key)
				coins_add=self.get_argument('coins_add')

			if coins_add:
				res['total_coins']=float(res['total_coins'])+float(coins_add)
				coll.save(res)
				self.write("You have changed it successfully.")
			self.redirect("/editcoin/"+str(res['uid']))
		else:
			self.write('{"error":"user not found"}<br><a href="/user">return</a>')
		

if __name__=="__main__":
	tornado.options.parse_command_line()
	http_server=tornado.httpserver.HTTPServer(Application())
	http_server.listen(options.port)
	tornado.ioloop.IOLoop.instance().start()


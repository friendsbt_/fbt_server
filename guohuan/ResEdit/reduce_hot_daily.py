# -*- coding:utf-8 -*-

import pymongo
import sys
import string
from datetime import datetime,timedelta
    

if __name__=="__main__":
    # conn=pymongo.Connection("192.168.195.122",27017)
    # db=conn.fbt
    sys.path.append("/home/fbt/latest_fbt_server_py");
    import mongoclient;
    db=mongoclient.fbt;
    db.all_resources.update({"sticky":{"$gt":1}},{"$inc":{"sticky":-1}},multi=True)
    db.all_resources.update({"sticky":1},{"$unset":{"sticky":1}},multi=True)


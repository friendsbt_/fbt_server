# -*- coding:utf-8 -*-

import pymongo
import sys
import string
import datetime
from pprint import pprint


if __name__=="__main__":
    # conn=pymongo.Connection("192.168.195.122",27017)
    # db=conn.fbt
    sys.path.append("/home/fbt/latest_fbt_server_py");
    import mongoclient;
    db=mongoclient.fbt;
    reload(sys)
    sys.setdefaultencoding("utf-8")
    today=str(datetime.date.today())
    oneday=datetime.timedelta(days=1)
    yesterday=str(datetime.date.today()-oneday)
    dic={}
    newusers=db.users.find({"time":{"$gt":yesterday,"$lt":today}},{"school":1})
    for user in newusers:
        if "school" in user:
            dic[user["school"]]=dic.get(user["school"],0)+1
    dic2=sorted(dic.iteritems(),key=lambda d:d[1],reverse=True)

    fout=open("/home/fbt/resEdit/user_info_history/"+yesterday,"w")
    for x,y in dic2:
        if x:
            # print x,y
            fout.write("%s %d\n"%(x,y))
        else:
            # print "None",y
            fout.write("None %d\n"%y)
    fout.close()


# -*- coding:utf-8 -*-

import pymongo
import sys
import re
import string
	

if __name__=="__main__":
	identify = string.maketrans('', '')
	delEStr = string.punctuation + ' ' + string.letters  #ASCII 标点符号，空格和字母
	delChstr=["中文字幕","720p","720P",	"1080p","1080P","HD","人人影视","电影天堂",	\
	"高清版",	"高清版","高清","双语字幕","国语双字","中英双字","分辨率",\
	"国产动画","制作","英语双字","【","】","字幕组","迅雷","国语中字","清晰",\
	"枪版","中字","不喜勿下","国英双语","标清","国语","（","）","mp4","MP4",\
	"国粤双语","中英文","双字幕","双语","中英字幕","英文","繁体","简体",\
	"srt","ass","飞鸟影视","阳光电影"]
	conn=pymongo.Connection("localhost",27017)
	db=conn.fbt.all_resources
	f=open("names","w")
	reload(sys)
	sys.setdefaultencoding("utf-8")
	docs=db.find({"main_type":0},{"ObjectId":1,"file_name":1})
	for doc in docs:
		s=str(doc["file_name"])
		for delstr in delChstr:
			s=s.replace(delstr,"")
		# if ("file_id" in doc):
		# 	f.write(doc["file_id"]+"=")
		# else:
		# 	print("no file_id!-->"+doc["file_name"])
		f.write(str(doc["_id"]) +":")
		if re.findall(u'[\x80-\xff].', s):    #s为中文   
			s=s.translate(identify, delEStr)
			s=re.sub(u"[0-9]{3,10}","",s)
			f.write(s)
		else: #s为英文   
			s=s.replace("."," ")
			year=re.findall("[0-9]{4}",s)
			if (year):
				year=year[0]
				year_pos=s.find(year)
				s=s[:year_pos+4]
			f.write(s)
			# print(s)

		#f.write('\t\t\t<<<---'+doc["file_name"]+'\n')
		f.write('\n')
	f.close()
	
import pymongo
conn = pymongo.Connection("localhost", 27017)
db = conn.fbt

def ResUpdate(filehash,filesize,filename=None,tags=None,maintype=None,subtype=None,public=None,download_num=None):
    if filesize:
        doc=db.all_resources.find_one({"file_id":str(filehash)+'_'+str(filesize)})
    else:
        docs=db.all_resources.find_one({"file_hash":filehash})
        if docs.count()==1:
            doc=docs[0]
        else:
            exit(0)
    if filename:
        doc['file_name']=filename
    if tags:
        if not doc['tags']:
            doc['tags']=[]
        for tag in tags:
            if tag not in doc['tags']:
                doc['tags'].append(tag)
    if maintype:
        doc['main_type']=maintype
    if public:
        doc['public']=public
    if subtype:
        doc['sub_type']=subtype
    if download_num:
        doc['download_num']=download_num
    db.all_resources.save(doc)


if __name__=="__main__":
    ResUpdate(29784195,1034944512,'ubuntu-14.04',tags=['linux','system'],download_num=3)
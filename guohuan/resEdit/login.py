# -*- coding: utf-8 -*-


import tornado.ioloop
import tornado.web

class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("user")

class MainHandler(BaseHandler):
    @tornado.web.authenticated # 如果没有登陆，就自动跳转到登陆页面
    def get(self):
        name = tornado.escape.xhtml_escape(self.current_user)
        self.write("Hello, " + name)
        self.write('<a href="/logout">logout </a>')

class LoginHandler(BaseHandler):
    def get(self):
        self.write('<html><body><form action="/login" method="post">'
                   'Name: <input type="text" name="name"><br>'
                   'Password: <input type="password" name="psw">'
                   '<input type="submit" value="Sign in">'
                   '</form></body></html>')

    def post(self):
        # 这里补充一个，获取用户输入
        uname=self.get_argument("name")
	ukey=self.get_argument("psw")
	if uname=='admin' and ukey=='111':
	        self.set_secure_cookie("user", self.get_argument("name"))
        	self.redirect("/start")
	else:
		self.redirect("/login")

class LogoutHandler(BaseHandler):
	def get(self):
		self.set_secure_cookie("user", "")
		self.redirect("/login")

settings = {
    "cookie_secret": "__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__", # 安全cookie所需的
    "login_url": "/login", # 默认的登陆页面，必须有
}
application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/login", LoginHandler),
    (r"/logout", LogoutHandler),
], **settings)

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

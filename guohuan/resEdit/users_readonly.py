# -*- coding:utf-8 -*-

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

import pymongo


from tornado.options import define,options
define("port",default=8000,help="run on the given port",type=int)

class Application(tornado.web.Application):
	def __init__(self):
		handlers=[(r"/(\w+)",UserHandler)]
		conn=pymongo.Connection("localhost",27017)
		self.db=conn.fbt
		tornado.web.Application.__init__(self,handlers,debug=True)

class UserHandler(tornado.web.RequestHandler):
	def get(self,nick_name):
		coll=self.application.db.users
		user_doc=coll.find_one({"nick_name":nick_name})
		if user_doc:
			del user_doc["_id"]
			self.write(user_doc)
			self.write("\n")
			coll2=self.application.db.coins_of_user
			coin=coll2.find_one({"uid":user_doc["uid"]})
			self.write("\n")
			del coin["_id"]
			self.write(coin)
		else:
			self.set_status(404)
			self.write({"error":"user not found"})

if __name__=="__main__":
	tornado.options.parse_command_line()
	http_server=tornado.httpserver.HTTPServer(Application())
	http_server.listen(options.port)
	tornado.ioloop.IOLoop.instance().start()

# -*- coding: UTF-8 -*-
import pymongo
from pprint import pprint

conn=pymongo.Connection("localhost",27017)
db=conn.fbt

fids=[
"387246044_452070610",
]

for fid in fids:
    res=db.all_resources.find_one({"file_id":fid})
    owners=res.get("owners",None)
    for owner in owners:
        owner_uid=owner["uid"]
        pprint(owner_uid)

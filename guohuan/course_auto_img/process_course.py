# -*- coding: utf-8 -*-

if __name__=='__main__':
    main_dic={}
    dic={}
    last_id=None
    with file('course1.txt') as fin:
        for line in fin.readlines():
            s=line.strip().split()
            try:
                cid=int(s[0])
                last_id=cid
            except Exception, e:
                cid=last_id
            name=s[-1].strip().replace('　','')
            if len(str(cid))<=3:
                main_dic[name]=cid
            else:
                dic[name]=cid
    with file('course_code.py','w') as fout:
        fout.write('# -*- coding: utf-8 -*-\n\n')
        fout.write('main_dic={\n')
        for k,v in main_dic.iteritems():
            fout.write('"%s":"%d",\n'%(k,v))
        fout.write('}\n')
        fout.write('#===================\n')
        fout.write('detail_dic={\n')
        for k,v in dic.iteritems():
            fout.write('"%s":"%d",\n'%(k,v))
        fout.write('}\n')
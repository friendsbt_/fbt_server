# -*- coding: utf-8 -*-
# from tornado import gen
import os
import jieba
import unittest
import pdb

import sys
sys.path.append("/home/fbt/latest_fbt_server_py")
import mongoclient
db=mongoclient.fbt
print db

from course_code import main_dic,detail_dic

img_path='./static/images/course/'
img_files=[]

disable_words=[
'与',
'及其',
'及',
'和',
'其他',
'学科',
'相关',
'工程技术',
'学科',
'概论',
'专业',
'理论',
'原理',
'基础',
'实验',
'技术实验',
'综合',
'听力',
'口语',
'分析',
'课程设计',
'课设',
'导论',
'嵌入式',
'上',
'下',
'项目训练',
'综合实习',
'实习',
'应用',
'基本原理',
'专题',
'（','）',
'经典',
]

all_exists_words=[]

class CourseImg(object):
    def __init__(self):
        jieba.initialize()
        for root,dirs,files in os.walk(img_path):
            for f in files:
                if f.endswith('.jpg'):
                    img_files.append(f)
        # print '\t'.join(img_files)
        for course in main_dic:
            for word in jieba.cut(course):
                all_exists_words.append(word.encode('utf-8','ignore'))
        for course in detail_dic:
            for word in jieba.cut(course):
                all_exists_words.append(word.encode('utf-8','ignore'))

    def get_course_img(self, course_name): 
        course_name=course_name.encode('utf-8','ignore')
        if course_name in main_dic: #课程直接属于大类
            img_name=main_dic[course_name]+'.jpg'
            if img_name in img_files:
                return img_name
        for x in disable_words: #去除某些停用词，课程直接属于大类
            course_name=course_name.replace(x,'')
        if course_name in main_dic:
            img_name=main_dic[course_name]+'.jpg'
            if img_name in img_files:
                return img_name
        if course_name in detail_dic: #课程直接属于小类
            img_name=detail_dic[course_name][:3]+'.jpg'
            if img_name in img_files:
                return img_name
        seg_list=[]
        for word in jieba.cut(course_name): #课程名分词
            word=word.encode('utf-8','ignore')
            if word not in disable_words and word in all_exists_words:
                seg_list.append(word)
        for item in main_dic: #分词全在大类
            if self.all_in(seg_list, item):
                img_name=main_dic[item]+'.jpg'
                if img_name in img_files:
                    return img_name
        for item in detail_dic: #分词全在小类
            if self.all_in(seg_list, item):
                img_name=detail_dic[item][:3]+'.jpg'
                if img_name in img_files:
                    return img_name
        for course in main_dic: #大类名称，分词是否属于课程名
            isit=False            
            for word in jieba.cut(course):
                word=word.encode('utf-8','ignore')
                if word not in disable_words:
                    if word not in course_name:
                        isit=False
                        break
                    else:
                        isit=True
            if isit:
                img_name=main_dic[course]+'.jpg'
                if img_name in img_files:
                    # print '+++',course_name, course
                    return img_name
        # 以上都没找到，则找一个比较相近的
        for word in seg_list:
            for course in main_dic:
                if word in course:
                    return main_dic[course]+'.jpg'
        for word in seg_list:
            for course in detail_dic:
                if word in course:
                    return detail_dic[course][:3]+'.jpg'
        # print '===',course_name, seg_list
        return '0.jpg'

    def all_in(self, lst, s):
        # whether element in lst all in s
        is_all_in=True
        for item in lst:
            if item not in s:
                is_all_in=False
                break
        return is_all_in

class testCase(unittest.TestCase):
    def setUp(self):
        pass
        self.a=CourseImg()

    def testLeet(self):
        self.assertEqual(self.a.get_course_img(u'微电子学'),'510.jpg')
        self.assertEqual(self.a.get_course_img(u'生物工程'),'416.jpg')
        self.assertEqual(self.a.get_course_img(u'计算机科学与技术'),'520.jpg')
        self.assertEqual(self.a.get_course_img(u'信息分类与编码标准化'),'410.jpg')
        self.assertEqual(self.a.get_course_img(u'国际金融学专业'),'790.jpg')
        self.assertEqual(self.a.get_course_img(u'工程力学A（姚学峰）'),'560.jpg')
        self.assertEqual(self.a.get_course_img(u'工程力学（2）－殷雅俊'),'560.jpg')
        self.assertEqual(self.a.get_course_img(u'Access2000数据库'),'520.jpg')
        self.assertEqual(self.a.get_course_img(u'汇编语言程序设计'),'520.jpg')
        self.assertEqual(self.a.get_course_img(u'现代交换技术'),'0.jpg')
        self.assertEqual(self.a.get_course_img(u'行政管理学'),'630.jpg')
        self.assertEqual(self.a.get_course_img(u'宏微观经济学专业'),'790.jpg')
        self.assertEqual(self.a.get_course_img(u'国际商务电子专业'),'0.jpg')
        self.assertEqual(self.a.get_course_img(u'国际贸易专业'),'790.jpg')
        self.assertEqual(self.a.get_course_img(u'理论力学'),'130.jpg')
        self.assertEqual(self.a.get_course_img(u'法语专业'),'740.jpg')
        self.assertEqual(self.a.get_course_img(u'心理学专业'),'190.jpg')
        self.assertEqual(self.a.get_course_img(u'英语专业'),'740.jpg')
        self.assertEqual(self.a.get_course_img(u'管理学'),'630.jpg')
        self.assertEqual(self.a.get_course_img(u'电子技术'),'510.jpg')
        self.assertEqual(self.a.get_course_img(u'工程设计专业'),'560.jpg')
        self.assertEqual(self.a.get_course_img(u'电磁场理论'),'470.jpg')
        self.assertEqual(self.a.get_course_img(u'大众文化与网络传播'),'860.jpg')
        self.assertEqual(self.a.get_course_img(u'传播学理论'),'860.jpg')
        self.assertEqual(self.a.get_course_img(u'操作系统与编译原理'),'520.jpg')
        self.assertEqual(self.a.get_course_img(u'经济学基础'),'790.jpg')
        # self.assertEqual(self.a.get_course_img(u'测试与测验技术基础'),'0.jpg')
        # self.assertEqual(self.a.get_course_img(u'电路与模拟电子技术'),'510.jpg')
        # self.assertEqual(self.a.get_course_img(u'社会保障概论专业'),'840.jpg')

if __name__ == '__main__':
    # unittest.main()
    CI=CourseImg()
    # img=CI.get_course_img(u'物流工程')
    # print '----',img
    # exit()
    courses=db.courses.find({'img_set':None,'img':None})
    # courses=db.courses.find({'img_set':None,"university":"中国人民大学","college":"经济学院"})
    # courses=db.courses.find({"university":"武汉大学","college":"动力与机械学院","img":None})
    cnt=0
    valid_cnt=0
    for course in courses:
        try:
            img=CI.get_course_img(course['course'])
            # print course['course'],img
            cnt+=1
            if img!='0.jpg':
                valid_cnt+=1
                course['img']='//static.friendsbt.com/static/images/course/'+img
            else:
                course['img0']=1
            course['img_set']=1
            db.courses.save(course)
        except Exception, e:
            pass
    print 'auto set image: %d/%d'%(valid_cnt,cnt)

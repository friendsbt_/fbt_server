# -*- coding: UTF-8 -*-
import uuid
import pymongo
from pprint import pprint
from datetime import datetime

conn=pymongo.Connection("localhost",27017)
db=conn.fbt

msg = {}
msg["type"]=0
msg["isRead"] = 0
# msg["sticky"] = 1
msg["id"] = str(uuid.uuid1().int)
msg["sender"] = "0"
msg["nick"] = "0"
# msg["content"] = "content_test"
msg["content"] = " 您的资源***33已被管理用审核通过。 "
msg["time"] = datetime.now().strftime('%Y-%m-%d %H:%M')

db.user_msg.update({"user":"guohuanright@163.com"},{"$push":{"msg_list":msg}})

x=db.user_msg.find_one({"user":"guohuanright@163.com"})
pprint(x)


"""
> db.user_msg.findOne()
{
	"_id" : ObjectId("543221cc1eec4a09dac07255"),
	"user" : "315486169@qq.com",
	"msg_list" : [
		{
			"sender" : "820364309@qq.com",
			"content" : "铅笔想加你为好友,是否同意",
			"nick" : "铅笔",
			"isRead" : 1,
			"time" : "2014-10-06 12:59",
			"type" : 0,
			"id" : "209711085533476014145526526965914080078"
		},
		{
			"sender" : "704214196@qq.com",
			"content" : "Evelyn 宁儿想加你为好友,是否同意",
			"nick" : "Evelyn 宁儿",
			"isRead" : 1,
			"time" : "2014-10-06 20:58",
			"type" : 0,
			"id" : "139721640455743824838457024037701620558"
		}
	]
}
"""
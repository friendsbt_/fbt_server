# -*- coding:utf-8 -*-

import pymongo
import sys
	

if __name__=="__main__":

	conn=pymongo.Connection("localhost",27017)
	db=conn.fbt
	docs=db.users.find({'haslog':None})
	for doc in docs:
		uid=doc["uid"]
		db.fblog.insert({'uid' : uid,'online_time':0, 'log':[]})
		db.users.update({'uid':uid},{'$set':{'haslog':1}})

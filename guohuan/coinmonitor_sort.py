# -*- coding:utf-8 -*-

import pymongo
import sys
import string
import datetime
from pprint import pprint
	

if __name__=="__main__":
	conn=pymongo.Connection("localhost",27017)
	db=conn.fbt
	diff=50
	filename="./coinlog/coin_change_"+str(datetime.date.today())
	reload(sys)
	sys.setdefaultencoding("utf-8")
	lst=[]
	users=db.coins_of_user.find({},{"uid":1,"total_coins":1,"coins_of_ystday":1})
	for user in users:
		try:
			pass
			change=user["total_coins"]-user["coins_of_ystday"]
			if change>diff:
				lst.append((user["uid"],change,user["coins_of_ystday"],user["total_coins"]))
		except Exception, e:
			user["coins_of_ystday"]=user["total_coins"]
			db.coins_of_user.save(user)
	slst=sorted(lst,key=lambda a:a[1],reverse=True)
	# pprint(slst)
	fout=open(filename,"w")
	fout.write("coin changed over %d\n" % diff)
	fout.write("change nickname(uid): yesterday -> today\n")
	for u in slst:
		nickname=db.users.find_one({"uid":u[0]},{"nick_name":1})["nick_name"]
		fout.write("%d %s(%d): %d -> %d\n" % (u[1],nickname,u[0],u[2],u[3]))
		# print("%d %s(%d): %d -> %d\n" % (u[1],nickname,u[0],u[2],u[3]))
	fout.close()



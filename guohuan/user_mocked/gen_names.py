# -*- coding: utf-8 -*-
import os
import random
import pypinyin

def filein(file_name):
    lst=[]
    with file(file_name) as fin:
        for line in fin.readlines():
            lst.append(line.strip())
    return lst

def get_img_files(path):
    lst=[]
    for root,dirs,files in os.walk(path):
        for f in files:
            if f.endswith('.jpg'):
                fname=os.path.join(root,f)
                lst.append(fname)
                # lst.append(f)
    return lst

if __name__ == '__main__':
    mail_postfix=['163','126','qq','gmail','sina']
    family_names=filein('familyname.txt')
    boy_names=filein('boyname.txt')
    girl_names=filein('girlname.txt')
    boy_imgs=get_img_files('./boy_head_img')
    girl_imgs=get_img_files('./girl_head_img')
    with file('boy_lst.txt','w') as fout:
        for boy in boy_imgs:
            name=family_names[int(random.uniform(0,len(family_names)))]+boy_names[int(random.uniform(0,len(boy_names)))]
            pinyin=pypinyin.slug(name.decode('utf-8'),separator='')
            mail='%s%d@%s.com'%(pinyin,random.uniform(1000000000,9999999999),mail_postfix[int(random.uniform(0,len(mail_postfix)))])
            fout.write('%s %s %s\n'%(name,mail.encode('utf-8'),boy))
    with file('girl_lst.txt','w') as fout:
        for girl in girl_imgs:
            name=family_names[int(random.uniform(0,len(family_names)))]+girl_names[int(random.uniform(0,len(girl_names)))]
            pinyin=pypinyin.slug(name.decode('utf-8'),separator='')
            mail='%s%d@%s.com'%(pinyin,random.uniform(1000000000,9999999999),mail_postfix[int(random.uniform(0,len(mail_postfix)))])
            fout.write('%s %s %s\n'%(name,mail.encode('utf-8'),girl))
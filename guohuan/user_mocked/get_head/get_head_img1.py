# -*- coding: utf-8 -*-
import urllib2
from lxml import etree
import re
import json
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
from contextlib import nested


if __name__=="__main__":
    url_template='http://www.tupo.com/search?order_by=desc&order_field=smart&display_price=online&gender=1&page=%d'
    with file('head_imgs.txt','w') as fout:
        for page in xrange(1,51):
            response=urllib2.urlopen(url_template%page)
            html=response.read()
            tree=etree.HTML(html)
            for url in tree.xpath('//ul/li/div/a/img/@src'):
                fout.write("%s\n"%url)
            # break
# -*- coding:utf-8 -*-

import pymongo
import sys
import re
import string
	

if __name__=="__main__":
	conn=pymongo.Connection("localhost",27017)
	db=conn.fbt.all_resources
	f=open("rmnames","w")
	reload(sys)
	sys.setdefaultencoding("utf-8")
	docs=db.find({},{"file_id":1,"file_name":1})
	for doc in docs:
		s=str(doc.get("file_name",""))
		if re.findall("\.[0-9a-zA-Z]{2,4}$",s):
			pass
		else:
			db.update({"file_id":doc["file_id"]},{"$set":{"hidden":1}})
			f.write(str(doc.get("id",""))+" "+str(doc.get("file_name","")))
			f.write('\n')
	f.close()
	
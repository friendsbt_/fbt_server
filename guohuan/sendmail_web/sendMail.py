# -*- coding: utf-8 -*-
import smtplib
from email.mime.text import MIMEText 
from email.mime.multipart import MIMEMultipart
# from tornado import gen

class sendMail(object):
    @classmethod
    def send_mail(cls, subj,to_list,cont):  
        me="fbt_service@friendsbt.com"
        content = cont
        msg = MIMEMultipart('alternative') 
        msg['Subject'] = subj 
        msg['From'] = me  
        msg['To'] = to_list
        # msg['To'] = ";".join(to_list)
        part2 = MIMEText(content, 'html',_charset='utf-8')
        msg.attach(part2)
        try:  
            server = smtplib.SMTP("localhost")  
            server.sendmail(me, to_list, msg.as_string())  
            server.quit()
            return True    
        except Exception, e:  
            # print e
            return False



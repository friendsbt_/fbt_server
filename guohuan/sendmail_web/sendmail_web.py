# -*- coding:utf-8 -*-

import sys
import os.path
import tornado.locale
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado import gen


from tornado.options import define,options
define("port",default=5678,help="run on the given port",type=int)


class Application(tornado.web.Application):
	def __init__(self):
		handlers=[
			(r"/sendmail",SendmailHandler),
			]
		settings=dict(
			template_path=os.path.join(os.path.dirname(__file__),"templates"),
			static_path=os.path.join(os.path.dirname(__file__),"static"),
			debug=True,
			cookie_secret= "abcdef1234567",
			login_url= "/login", 
				)
		tornado.web.Application.__init__(self,handlers,**settings)


class SendmailHandler(tornado.web.RequestHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	# @tornado.web.authenticated 
	def get(self):
		self.render("sendmail.html")

	def post(self):
		mailadd=self.get_argument("mailto",None)
		mailsubj=self.get_argument("mailsubj",None)
		mailcont=self.get_argument("mailcont",None)

		if mailadd:
			from sendMail import sendMail
			ret=sendMail.send_mail(mailsubj,mailadd,mailcont)
			if ret:
				self.write('发送成功！')
			else:
				self.write('发送失败！')
		else:
			self.write('请填写用户邮箱！')

if __name__=="__main__":
	reload(sys)
	sys.setdefaultencoding('utf8')
	tornado.options.parse_command_line()
	http_server=tornado.httpserver.HTTPServer(Application())
	http_server.listen(options.port)
	tornado.ioloop.IOLoop.instance().start()


#-*- coding: utf-8 -*- 
import motor
from tornado.ioloop import IOLoop
from tornado import gen
from datetime import date 
import cPickle as pickle
from random import randint
import os
import pprint
import logging

class HotUpdate(object):
	'''
	hot of resources in all_resources DB is like
		hot[{hot_day:hot, download_num_day:download of last period}]
	'''
	def __init__(self):
		self._main_type_sum = 10 #the amount of main types
		self._db = motor.MotorClient('127.0.0.1', 27017).fbt
		self._coll = self._db.all_resources
		self._DESCENDING  = -1
		self._hot_day_coefficent = 0.7
		self._max_records = 420
		self._dump_max_records = 20
		self._hotRankLists = [[] for i in range(self._main_type_sum)]
		self._today = date.today().strftime("%Y-%m-%d")
		self._MAX_HOT = 50000

	@gen.coroutine	
	def init_hot_day(self):
		yield self._coll.update({}, {"$set": {"hot":[{"hot_day": 0, "download_num_day": 0}]}}, multi=True)
		cursor = self._coll.find({"public": 1, "hidden": {"$ne": 1}})
		try:
			while (yield cursor.fetch_next):
				res = cursor.next_object()
				if "download_num" in res:
					if res["download_num"] != '':
						res["hot"][0]["hot_day"] = int(res["download_num"])
				self._coll.save(res)
		except Exception, e:
			logging.warning(e)
	
	@gen.coroutine
	def update_hot_day(self):
		cursor = self._coll.find({"public": 1, "hidden": {"$ne": 1}})
		try:
			while (yield cursor.fetch_next):
				res = cursor.next_object()
				if "hot" not in res:
					res["hot"] = [{"hot_day": 0, "download_num_day": 0}]
				else:
					if res["hot"][0]["hot_day"] == '':
						res["hot"][0]["hot_day"] = 0
					res["hot"][0]["hot_day"] = int(self._hot_day_coefficent * res["hot"][0]["hot_day"]) + res["hot"][0]["download_num_day"]
					res["hot"][0]["download_num_day"] = 0
				
				if "sticky" not in res:
					res["sticky"] = 0
				else:
					if res["sticky"] > 0:
						res["sticky"] = res["sticky"] - 1

				self._coll.save(res)

			print "update_hot_day: " + self._today
		except Exception, e:
			print "update_hot_day failed: " + self._today
			logging.warning(e)
	
	@gen.coroutine
	def test(self):
		res_sum = 0
		try:	
			cursor = self._db.all_resources.find({})
			while (yield cursor.fetch_next):
				res = cursor.next_object()
				if "file_id" not in res:
					res_sum = res_sum + 1
					print res
					self._coll.remove({"_id": res["_id"]})
			#size = yield cursor.count()
        		#yield self._coll.update({'_id': one_res['_id']}, {'$inc': {'download_num': 1, 'hot.0.download_num_day': 1}}, True)
			#res = yield self._coll.find_one({"_id": one_res['_id']})
			print res_sum
			#print cursor
		except Exception, e:
			logging.warning(e)


	@gen.coroutine
	def get_hottest_resource(self):
		try:
			hot_db = self._db.hot_resources
			hot_db.remove({})

			for res_type in range(self._main_type_sum):
				cursor = self._coll.find({"hidden": {"$ne": 1}, "public": 1, "main_type": res_type}).sort([('hot.0.hot_day', self._DESCENDING), ('download_num', self._DESCENDING)]).limit(self._max_records)
				while (yield cursor.fetch_next):
					res = cursor.next_object()
					hot_db.save(res)

			cursor = self._coll.find({"hidden": {"$ne": 1}, "public": 1, "sticky": {"$gt": 0}})
			while (yield cursor.fetch_next):
				res = cursor.next_object()
				res["hot"][0]["hot_day"] = self._MAX_HOT
				hot_db.save(res)

			print "get_hottest_resource: " + self._today
		except Exception, e:
			print "get_hottest_resource failed: " + self._today
			logging.warning(e)


	def backup_hot_resource(self):
		#make the dirs for backup files
		assert self._dump_max_records <= self._max_records
		path = "dump_hot_resource"
		if not os.path.exists(path):
			os.makedirs(path)
		for res_type in range(self._main_type_sum):
			new_path = os.path.join(path, self._main_type[res_type])
			if not os.path.exists(new_path):
				os.makedirs(new_path)

		for res_type in range(self._main_type_sum):
			today = date.today().strftime("%Y-%m-%d")
			file_name = os.path.join(path, self._main_type[res_type], today+ ".pkl")
			with open(file_name, "wb") as infile:
				pickle.dump(self._hotRankLists[res_type][0:self._dump_max_records], infile)
			infile.close()

	def get_hot_resource_from_backup(self):
		path = "dump_hot_resource"			
		for res_type in range(self._main_type_sum):
			today = date.today().strftime("%Y-%m-%d")
			file_name = os.path.join(path, self._main_type[res_type], today+ ".pkl")
			with open(file_name, "rb") as outfile:
				pickle.load(outfile)
			outfile.close()
	

	@gen.coroutine
	def run(self, func):
		IOLoop.current().run_sync(func)

	

	


	

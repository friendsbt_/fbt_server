# coding: utf-8
# convert file_hash to file_id in db.resources_of_user 
import pymongo


if __name__ == '__main__':
	conn=pymongo.Connection("localhost",27017)
	db=conn.fbt
	docs=db.tags.find({"file_ids":None})
	# docs=db.resources_of_user.find({"file_ids":None})
	# docs=db.resources_of_user.find({"uid":14111306944683})
	for doc in docs:
		if doc.get("file_hashes",None):
			file_ids=[]
			for file_hash in doc["file_hashes"]:
				file_id=db.all_resources.find({"file_hash":file_hash},{"file_id":1})
				if file_id.count()==1:
					if "file_id" in file_id[0]:
						file_ids.append(file_id[0]["file_id"])
					# print("%s -> %s"%(file_hash,file_id[0]["file_id"]))
			doc["file_ids"]=file_ids
			# db.resources_of_user.save(doc)
			db.tags.save(doc)
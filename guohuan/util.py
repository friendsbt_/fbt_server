#-*- coding:utf-8 -*-
import json
from time import time
import os
import tornadoredis
import tornado.gen
from constant import *
# import gevent.monkey
# gevent.monkey.patch_socket()
# from gevent.pool import Pool
# import requests
# import tornado.httpclient
# from tornado import gen
Cache_Expire = [30*60, 10*60, 5*60]
class transaction:
    '''
    args={"db": db_obj, "transaction": [{"collection": "collection", "action":"insert|update|remove", "argument":[...]},...]}
    '''
    def batch_exec(args):
        db = args["db"]
        db.transaction.insert({"status": "init", "transaction": args["transaction"]})
#redis for pubsub
class RedisSub(object):
    #@channel str
    #@callback function invoke when the sub msg come
    def __init__(self,channel,callback):
        self.callback = callback
        self.sub_client = tornadoredis.Client(password="123-fbt-pub-sub-!@#",port=6380)
        self.channel = channel
        self.subscribe()

    @tornado.gen.engine
    def subscribe(self):
        self.sub_client.connect()
        yield tornado.gen.Task(self.sub_client.subscribe, self.channel)
        self.sub_client.listen(self.on_redis_message)

    def on_redis_message(self, message):
        kind, body = message.kind, message.body
        if kind == 'message' and self.callback:
            self.callback(body)
        if kind == 'disconnect':
            self.close()

    def close(self):
        if self.sub_client.subscribed:
            self.sub_client.unsubscribe(self.channel)
            self.sub_client.disconnect()
class RedisPub(object):
    _redisPub = None
    @staticmethod 
    def init():
        if not RedisPub._redisPub:
            RedisPub._redisPub = tornadoredis.Client(password="123-fbt-pub-sub-!@#",port=6380)
    #@message str the publish message
    #@channel str the publish channel
    @staticmethod
    def publish(channel, message):
        RedisPub.init()
        RedisPub._redisPub.publish(channel,message)

#redis for cache
class RedisHandle:
    _redis = None
    type_token = 0
    type_cache = 1
    @staticmethod
    def init():
        if not RedisHandle._redis:
            import redis
            RedisHandle._redis = []
            RedisHandle._redis.append(redis.StrictRedis(password="123-fbt-session-cache-!@#", port=6381))
            RedisHandle._redis.append(redis.StrictRedis(password="123-fbt-all-cache-!@#", port=6382))

    @staticmethod
    def get(key, tp=type_cache):
        RedisHandle.init()
        return RedisHandle._redis[tp].get(key)

    #@ex the expire time in ex seconds
    @staticmethod
    def set(key,value,ex=None, tp=type_cache):
        RedisHandle.init()
        if ex:
            RedisHandle._redis[tp].setex(key, ex, value)
        else:
            RedisHandle._redis[tp].set(key, value)

    #@ex the expire time in ex seconds
    @staticmethod
    def mset(mapping, ex=None, tp=type_cache):
        RedisHandle.init()
        if ex:
            pipe = RedisHandle._redis[tp].pipeline()
            for (k,v) in mapping:
                pipe.setex(k, ex, v)
            pipe.execute()
        else:
            RedisHandle._redis[tp].mset(mapping)

    @staticmethod
    def delete(tp=type_cache, *names):
        RedisHandle.init()
        return RedisHandle._redis[tp].delete(*names)

    @staticmethod
    def exists(name, tp=type_cache):
        RedisHandle.init()
        return RedisHandle._redis[tp].exists(name)

    #Return the value of key within the hash name
    @staticmethod
    def hget(name, key, tp=type_cache):
        RedisHandle.init()
        return RedisHandle._redis[tp].hget(name, key)

    #Return dict within the hash name
    @staticmethod
    def hgetall(name, tp=type_cache):
        RedisHandle.init()
        return RedisHandle._redis[tp].hgetall(name)

    #Set key to value within hash name
    @staticmethod
    def hset(name, key, value, tp=type_cache):
        RedisHandle.init()
        return RedisHandle._redis[tp].hset(name, key, value)

    #Increment the value of key in hash name by amount
    @staticmethod
    def hincrease(name, key, amount=1, tp=type_cache):
        RedisHandle.init()
        RedisHandle._redis[tp].hincrby(name, key, amount)

    #Add value(s) to set name
    @staticmethod
    def sadd(name, tp=type_cache, *values):
        RedisHandle.init()
        RedisHandle._redis[tp].sadd(name, *values)

    #Remove values from set name
    @staticmethod
    def sremove(name, tp=type_cache, *values):
        RedisHandle.init()
        RedisHandle._redis[tp].srem(name, *values)

    #Return all members of the set name
    @staticmethod
    def smembers(name, tp=type_cache):
        RedisHandle.init()
        return RedisHandle._redis[tp].smembers(name)

    #Set mapping within hash name
    @staticmethod
    def hmset(name, mapping, tp=type_cache):
        RedisHandle.init()
        return RedisHandle._redis[tp].hmset(name, mapping)

    #delete keys from hash name
    @staticmethod
    def hdel(name, tp=type_cache, *keys):
        RedisHandle.init()
        return RedisHandle._redis[tp].hdel(name, *keys)

    #Returns a boolean indicating if key exists
    @staticmethod
    def hexists(name, key, tp=type_cache):
        RedisHandle.init()
        return RedisHandle._redis[tp].hexists(name, key)

    @staticmethod
    def append(name, tp=type_cache, *value):
        RedisHandle.init()
        return RedisHandle._redis[tp].rpush(name, *value)

    @staticmethod
    def save():
        RedisHandle.init()
        for item in RedisHandle._redis:
            item.bgsave()

# class HttpClient():
#     @staticmethod
#     def get(url):
#         def fetch(url):
#             response = requests.request('GET', url, timeout=5.0)
#         pool = Pool(2)
#         pool.spawn(fetch, url)
#         pool.join()

    # @staticmethod
    # @gen.coroutine
    # def get(url):
    #     http_client = tornado.httpclient.AsyncHTTPClient()
    #     yield http_client.fetch(url)

class MemCache():
    _expire_time = {}
    _cache = {}
    _expire_limit = 24*60*60
    @staticmethod
    def get(key, name=None):
        cur_t = long(time())
        if name and name in MemCache._expire_time:
            t = MemCache._expire_time[name]
            if t[1] != -1 and cur_t - t[0] >= t[1]:
                del(MemCache._expire_time[name])
                del(MemCache._cache[name])
                return None
            else:
                if not key:
                    return MemCache._cache[name]
                if key in MemCache._cache[name]:
                    return MemCache._cache[name][key]
                else:
                    return None
        else:
            if key in MemCache._expire_time:
                t = MemCache._expire_time[key]
                if t[1] != -1 and cur_t - t[0] >= t[1]:
                    del(MemCache._expire_time[key])
                    del(MemCache._cache[key])
                    return None
                else:
                    return MemCache._cache.get(key)
            else:
                return None
    @staticmethod
    def set(key, value, name=None, expire=-1):
        if name:
            if name not in MemCache._cache:
                MemCache._cache[name] = {}
            MemCache._cache[name][key] = value
            MemCache._expire_time[name] = [long(time()), expire]
        else:
            MemCache._cache[key] = value
            MemCache._expire_time[key] = [long(time()), expire]
    @staticmethod
    def delete(key, name=None):
        if name:
            if key:
                (MemCache._cache[name]).pop(key, None)
            else:
                MemCache._cache.pop(name, None)
                MemCache._expire_time(name, None)
        else:
            MemCache._expire_time.pop(key, None)
            MemCache._expire_time(key, None)
    @staticmethod
    def clear():
        MemCache._expire_time = {}
        MemCache._cache = {}
    @staticmethod
    def clearFb():
        del(MemCache._expire_time[WEEKLY_TOP])
        del(MemCache._expire_time[MONTHLY_TOP])
        del(MemCache._expire_time[TOTAL_RANK])
        del(MemCache._cache[WEEKLY_TOP])
        del(MemCache._cache[MONTHLY_TOP])
        del(MemCache._cache[TOTAL_RANK])
    @staticmethod
    def clearRes():
        del(MemCache._expire_time[RES_MAIN_180])
        del(MemCache._expire_time[RES_MAIN_179])
        del(MemCache._cache[RES_MAIN_180])
        del(MemCache._cache[RES_MAIN_179])
    @staticmethod
    def save(port):
        out_time = open(port+"cache_time.json","w")
        out_time.write(json.dumps(MemCache._expire_time))
        out_time.close()
        out_c = open(port+"cache_content.json","w")
        out_c.write(json.dumps(MemCache._cache))
        out_c.close()
    @staticmethod
    def load(port):
        if os.path.isfile(port+"cache_time.json") and os.path.isfile(port+"cache_content.json"):
            in_time = open(port+"cache_time.json","r")
            MemCache._expire_time = json.load(in_time)
            in_time.close()
            in_c = open(port+"cache_content.json","r")
            MemCache._cache = json.load(in_c)
            in_c.close()

def log(msg):
    pass

'''
types:0 for error, 1 for fine
'''
def write(obj, types, error, result):
    msg = {}
    msg["type"] = types
    msg["error"] = error
    msg["result"] = result
    msg = json.dumps(msg)
    obj.set_header("Content-Type","application/json")
    obj.write(msg)

'''
types:0 for error, 1 for fine
'''
def write_d(obj, msg):
    obj.set_header("Content-Type","application/json")
    obj.write(msg)

'''
0 for param error, 1 for db error
'''
def errorHandle(obj, num):
    if num == 0:
        write(obj, 0, "400", {})
    elif num == 1:
        write(obj, 0, "sorry", {})

def del_space(s):
    if not s:
        return s
    tmp = ""
    for item in s:
        if item != " ":
            tmp += item
    return tmp

def g_cookie(s,name):
    name = name.strip()
    c = del_space(s.request.headers.get("Cookie"))
    if not c:
        c = s.get_argument("cookie","")
        if not c:
            return None
    cookie = json.loads(c)
    if name in cookie:
        return cookie[name]
    else:
        return None

def encrypt(key, s):
    b = bytearray(str(s).encode("utf-8"))
    n = len(b)
    c = bytearray(n*2)
    j = 0
    for i in range(0, n):
        b1 = b[i]
        b2 = b1 ^ key
        c1 = b2 % 16
        c2 = b2 // 16
        c1 = c1 + 65
        c2 = c2 + 65
        c[j] = c1
        c[j+1] = c2
        j = j+2
    return c.decode("utf-8")

def decrypt(key, s):
    c = bytearray(str(s).encode("utf-8"))
    n = len(c)
    if n % 2 != 0 :
        return ""
    n = n // 2
    b = bytearray(n)
    j = 0
    for i in range(0, n):
        c1 = c[j]
        c2 = c[j+1]
        j = j+2
        c1 = c1 - 65
        c2 = c2 - 65
        b2 = c2*16 + c1
        b1 = b2^ key
        b[i]= b1
    try:
        return b.decode("utf-8")
    except:
        return "failed"

def encode(obj):
    obj = obj.replace("<", "&lt;")
    return obj.replace(">", "&lt;")


def gen_file_id(file_hash,file_size):
    assert file_size>0
    return str(file_hash)+"_"+str(file_size)

# -*- coding:utf-8 -*-

import os.path
import tornado.locale
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado import gen
from tornado.escape import utf8, to_unicode
import uuid
from pprint import pprint

# import pymongo
import motor
import sys
import copy
import time
from datetime import datetime,timedelta,date
import json
import tornadoredis
sys.path.append("/home/fbt/latest_fbt_server_py")
from study_resource_manager import StudyResourceManager
from fb_manager import FBCoinManager
from course_db import CourseDB
coursedb=CourseDB()

from settings import mongo_machines, REPLICASET_NAME
from pymongo import ReadPreference

from tornado.options import define,options
define("port",default=6789,help="run on the given port",type=int)

pub_client = tornadoredis.Client(host="fbt167",password="123-fbt-pub-sub-!@#",port=6380)
# channel="tornado"
channel="fbt:resource:pass-audit"
channel2="fbt:resource:hidden"
channel3="fbt:resource:revealed"

recycle_reasons=[
"同名",
"非法",
"其他原因"
]

userlst={
	"admin":"welovefbt",
	"fever911@126.com":"321456321", #kiddy
	"13590225970@qq.com":"1995630a", #铁洪
	"121618155@qq.com":"775852188", #忠义孝贤
	"451986454@qq.com":"aptx4869", #喵~
	"215260991@qq.com":"sunshine0712", #Silver
	"1554048358@qq.com":"xiaoxiao136159", #小小
	"cryo_heat@yeah.net":"18716t@", #小艾
	"zhuchangbo.2008@163.com":"fbt2015zhu", #朱昌波
	"913114402@qq.com":"525jiamy@5", #宋猛
	"1026250255@qq.com":"dy19960719", #杜洋
	"142594008@qq.com":"201417", #阿V
	"121618155@qq.com":"775852188", #陈忠义
	"1258034386@qq.com":"lz123456", #李众
	"bgy123123@126.com":"bgy123", #巴广益
	"972362039@qq.com":"Justforyou9513", #秦自立
	"352035524@qq.com":"07133992265myj", #小琪
}

rootlist={
"root":"root",
"1026250255@qq.com":"杜洋", #杜洋
"1258034386@qq.com":"李众", #李众
"451986454@qq.com":"喵~", #喵~
"142594008@qq.com":"阿V", #阿V
"fever911@126.com":"kiddy", #kiddy
"bgy123123@126.com":"巴广益",
"972362039@qq.com":"秦自立",
"352035524@qq.com":"小琪",
}

class Application(tornado.web.Application):
	def __init__(self):
		handlers=[
			(r"/test/(.*)",TestHandler),
			(r"/start",StartHandler),
			(r"/find",FindHandler),
			# (r"/hotfind/(.*)",HotFindHandler),
			(r"/edit/(.*)",EditHandler),
			(r"/rmuser/(.*)",RemoveuserHandler),
			(r"/login", LoginHandler),
			(r"/logout", LogoutHandler),
			(r"/user",UserHandler),
			(r"/finduser/(.*)",FindUserHandler),
			(r"/editcoin/(.*)",EditCoinHandler),
			(r"/coinlog/(.*)",CoinlogHandler),
			(r"/coinchange/(.*)",CoinchangeHandler),
			(r"/coinadd/(.*)",CoinaddHandler),
			(r"/audit",AuditHandler),
			(r"/auditing/(.*)",AuditingHandler),
			(r"/auditingstudy/(.*)",AuditingstudyHandler),
			(r"/autopass",AutopassHandler),
			(r"/inform",InformHandler),
			(r"/usercnt",UsercntHandler),
			(r"/userinfo/(.*)",UserinfoHandler),
			(r"/sendregmail",SendregmailHandler),
			(r"/sendresetmail",SendresetmailHandler),
			(r"/auditstudy",AuditstudyHandler),
			]
		settings=dict(
			template_path=os.path.join(os.path.dirname(__file__),"templates"),
			static_path=os.path.join(os.path.dirname(__file__),"static"),
			ui_modules={"oneres":ResModule,"oneu":UserModule,"oneurl":OneurlModule},
			debug=True,
			cookie_secret= "abcdef1234567",
			login_url= "/login", 
				)
		# conn=pymongo.Connection(mongo_machines,replicaSet=REPLICASET_NAME,readPreference=ReadPreference.PRIMARY)
		conn=motor.MotorReplicaSetClient(
    			','.join(mongo_machines),
   				replicaSet=REPLICASET_NAME,
    			readPreference=ReadPreference.PRIMARY)
		self.db=conn.fbt
		tornado.web.Application.__init__(self,handlers,**settings)

# conn1=pymongo.Connection(mongo_machines,replicaSet=REPLICASET_NAME,readPreference=ReadPreference.PRIMARY)
conn1=motor.MotorReplicaSetClient(
    			','.join(mongo_machines),
   				replicaSet=REPLICASET_NAME,
    			readPreference=ReadPreference.PRIMARY)
db0=conn1.fbt

class BaseHandler(tornado.web.RequestHandler):
	def get_current_user(self):
		# self.set_secure_cookie("user", "", expires_days=None)
		# self.set_secure_cookie("user", "", expires=time.time()+20)
		return self.get_secure_cookie("user")

class TestHandler(BaseHandler):
	@tornado.web.authenticated 
	def get(self,msg):
		pub_client.publish(channel, msg)
		self.write(msg)

class UserHandler(BaseHandler):
	@tornado.web.authenticated 
	def get(self):
		self.render("user.html")

class FindUserHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated  
	def get(self,user_name):
		if self.current_user not in rootlist: #!='root':
			self.write("permission denied.")
			return
		user_name=user_name.strip()
		coll=self.application.db.users
		user_doc=coll.find({"$or":[{"user":user_name},{"nick_name":user_name}]})
		# if user_doc_cnt==0 and user_name.isdigit():
		if (yield user_doc.count())==0 and user_name.isdigit():
			user_doc=yield coll.find_one({"uid":long(user_name)})
			self.render("finduser.html",u=[user_doc])
			return
		if user_doc.fetch_next:
			user_doc=yield user_doc.to_list(length=(yield user_doc.count()))
			self.render("finduser.html",u=user_doc)
		else:
			self.set_status(404)
			#self.write('{"error":"user not found"}<br><a href="/user">返回首页</a>')
			self.render('404.html')

class ResModule(tornado.web.UIModule):
	def render(self,res):
		return self.render_string("modules/oneres.html",res=res)

class UserModule(tornado.web.UIModule):
	def render(self,u):
		return self.render_string("modules/oneu.html",u=u)

class OneurlModule(tornado.web.UIModule):
	def render(self,res):
		return self.render_string("modules/oneurl.html",res=res)

class StartHandler(BaseHandler):
	@tornado.web.authenticated 
	def get(self):
		if self.current_user in rootlist:# =='root':
			self.render("start_root.html")
		else:
			self.render("start.html")

class LoginHandler(BaseHandler):
	def get(self):
		self.render("login.html")

	def post(self):
		# ÕâÀï²¹³äÒ»¸ö£¬»ñÈ¡ÓÃ»§ÊäÈë
		uname=self.get_argument("name")
		ukey=self.get_argument("psw")
		if uname=='root' and ukey=='Ilovefriendsbt!':
			self.set_secure_cookie("user", uname, expires=time.time()+6000)
			self.redirect("/start")
		elif uname in userlst and ukey==userlst[uname]:
			#print(uname)
			#print(ukey)
			self.set_secure_cookie("user", uname, expires=time.time()+6000)
			self.redirect("/start")
		else:
			self.redirect("/login")

class LogoutHandler(BaseHandler):
	def get(self):
		self.set_secure_cookie("user", "")
		self.redirect("/login")

class FindHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated  
	def get(self):
		coll=self.application.db.all_resources
		file_name=self.get_argument("name","")
		file_name=file_name.strip()
		file_name=file_name.replace("[","\[")
		file_name=file_name.replace("]","\]")
		file_name=file_name.replace("(","\(")
		file_name=file_name.replace(")","\)")
		if file_name=="":
			self.write("亲，你想找什么？告诉我才能帮你找哦~")
			return
		file_type=self.get_argument("type",None)
		file_size=self.get_argument("size",None)
		file_nohide=self.get_argument("nohide","false")
		if file_type and file_size and file_nohide:
			file_type=int(file_type)
			file_size=int(file_size)*1024*1024
			if file_nohide=="true":
				file_doc=coll.find({"file_name":{"$regex":file_name,"$options":"-i"},"main_type":file_type,"$or":[{"hidden":None},{"hidden":0}],"file_size":{"$lt":file_size}})
				# file_nohide=None
			else:
				file_doc=coll.find({"file_name":{"$regex":file_name,"$options":"-i"},"main_type":file_type,"file_size":{"$lt":file_size}})
				# file_nohide=1
		else:
			file_doc=coll.find({"file_name":{"$regex":file_name,"$options":"-i"}})
		if (yield file_doc.count()):
			file_doc=yield file_doc.to_list(length=(yield file_doc.count()))
			self.render("find.html",res=file_doc)
		else:
			self.set_status(404)
			#self.write('{"error":"file not found"}<br><a href="/start">返回首页</a>')
			self.render('404.html')

	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated  
	def post(self,file_name=None):
		# print file_name
		checklist=self.get_arguments("hidegroup")
		# print checklist
		if checklist:
			coll=self.application.db.all_resources
			for x in checklist:
				# pass
				yield coll.update({"file_id":x},{"$set":{"hidden":1}})
		self.write('已成功隐藏%d个资源<br>'%len(checklist))
		self.write('<a href="/start" >返回首页</a>')

# class HotFindHandler(BaseHandler):
# 	@tornado.web.asynchronous
# 	@gen.coroutine
# 	@tornado.web.authenticated  
# 	def get(self,file_name):
# 		coll=self.application.db.hot_resources
# 		file_name=file_name.strip()
# 		file_name=file_name.replace("[","\[")
# 		file_name=file_name.replace("]","\]")
# 		file_doc=coll.find({"file_name":{"$regex":file_name,"$options":"-i"}})
# 		if file_doc.count():
# 			self.render("hotfind.html",ress=file_doc)
# 		else:
# 			self.set_status(404)
# 			self.render('404.html')

# 	def post(self,file_name):
# 		coll=self.application.db.hot_resources
# 		file_id=self.get_argument('file_id')
# 		# print(file_id)
# 		coll.remove({"file_id":file_id})
# 		self.write("You have deleted resource:%s successfully."%file_id)

class EditHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,file_id):
		coll=self.application.db.all_resources
		file_doc=yield coll.find_one({"file_id":file_id})
		if file_doc:
			self.render("edit.html",res=file_doc)
		else:
			self.set_status(404)
			self.write('{"error":"file not found"}<br><a href="/start">返回首页</a>')

	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def post(self,file_id=None):
		#res_fields=['file_id','file_name','main_type','sub_type','download_num','hidden','public']
		res_fields=['main_type','public',"sub_type"]
		coll=self.application.db.all_resources
		res={} #dict()
		if file_id:
			res=yield coll.find_one({"file_id":file_id})
			res['file_name']=self.get_argument('file_name')
			for key in res_fields:
				res[key]=self.get_argument(key)
			res["public"]=int(res["public"])
			res["main_type"]=int(res["main_type"])
			res["sub_type"]=int(res["sub_type"])
			if self.get_argument("hidden","")=="1":
				res["hidden"]=1
				try:
					dic1={}
					dic1["file_id"]=file_id
					dic1["file_name"]=file_name
					msg1=json.dumps(dic1,ensure_ascii=False)
					pub_client.publish(channel2, msg1)
				except Exception, e:
					pass
			else:
				res["hidden"]=0
				try:
					dic1={}
					dic1["file_id"]=file_id
					dic1["file_name"]=file_name
					msg1=json.dumps(dic1,ensure_ascii=False)
					pub_client.publish(channel3, msg1)
				except Exception, e:
					pass
			if self.get_argument("sticky","")=="1":
				sticky_day=self.get_argument("sticky_day","3")
				sticky_day=int(sticky_day)
				res["sticky"]=sticky_day
				res2=copy.deepcopy(res)
				res2["hot"]=[]
				res2["hot"].append({})
				res2["hot"][0]["hot_day"] = 50000
				yield self.application.db.hot_resources.save(res2)
			else:
				if res.get("sticky",0)>=1:
					res["sticky"]=0
					res2=copy.deepcopy(res)
					res2["hot"]=[]
					res2["hot"].append({})
					res2["hot"][0]["hot_day"] = 20
					yield self.application.db.hot_resources.save(res2)
			yield coll.save(res)
			self.write("You have changed it successfully.")
		self.redirect("/find?name="+res['file_name'])

class EditCoinHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,uid):
		if self.current_user not in rootlist: #!='root':
			self.write("permission denied.")
			return
		coll=self.application.db.coins_of_user
		user_doc=yield coll.find_one({"uid":long(uid)})
		#user_doc=coll.find_one({"$or":[{"uid":uid},{"uid":long(uid)}]})
		#print(uid)
		if user_doc:
			self.render("editcoin.html",res=user_doc)
		else:
			self.set_status(404)
			self.write('sorry, user not found<br><a href="/user">返回</a>')

	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def post(self,uid=None):
		coll=self.application.db.coins_of_user
		if uid:
			uid=long(uid)
			res=yield coll.find_one({"uid":uid})
			coins_add=self.get_argument('coins_add')
			print coins_add
			if coins_add:
				res['total_coins']=float(res['total_coins'])+float(coins_add)
				yield coll.save(res)
				filename="/home/fbt/resEdit/coinlog/coin_add_"+str(date.today())
				utmp=yield self.application.db.users.find_one({"uid":uid},{"nick_name":1,"real_name":1})
				unick=utmp["nick_name"]
				ureal=utmp["real_name"]
				fout=open(filename,"a")
				fout.write("%s %s %s coins added by %s  coin:%s\n"%(unick,ureal,coins_add,rootlist[self.current_user],str(res['total_coins'])))
				fout.close()
				yield self.application.db.fblog.update({'uid' : uid},{"$push":{"log":{"date":datetime.now(),"coin":coins_add,"info":"coins added by "+ rootlist[self.current_user] +" coin:"+str(res['total_coins'])}}})
				self.write("You have changed it successfully.")
			self.redirect("/editcoin/"+str(res['uid']))
		else:
			self.write('{"error":"user not found"}<br><a href="/user">return</a>')

class CoinlogHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,uid):
		if self.current_user not in rootlist: #!='root':
			self.write("permission denied.")
			return
		user_doc=yield self.application.db.resources_of_user.find_one({"uid":long(uid)},{"file_ids":1})
		if user_doc:
			if "file_ids" in user_doc:
				self.write("该用户共有资源%d个<br>"%len(user_doc["file_ids"]))
			else:
				self.write("该用户共有资源0个<br>")
		else:
			self.set_status(404)
			self.write('sorry, user not found<br><a href="/user">返回</a>')
			return

		user_doc=yield self.application.db.users.find_one({"uid":long(uid)},{"time":1})
		if user_doc:
			self.write("注册时间%s<br>"%user_doc["time"])

		user_doc=yield self.application.db.fblog.find_one({"uid":long(uid)})
		if user_doc:
			self.write("在线时间%d小时<br><br>"%user_doc["online_time"])
			self.write(" 日期------ 时间---------- 加分规则---- 积分---- 加分---<br>")
			for i in user_doc["log"]:
				self.write("%s %s %s<br>"%(i["date"],i["info"],i["coin"]))

class CoinchangeHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,date1):
		if self.current_user not in rootlist: #!='root':
			self.write("permission denied.")
			return
		filename="./coinlog/coin_change_"+date1
		try:
			f=open(filename)
			self.write("<html><body>")
			for l in f.readlines():
				self.write(l+"<br>")
			self.write("</body></html>")
		except Exception, e:
			self.set_status(404)
			self.write('sorry, coin_change_log does not exist.')
			return

class CoinaddHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,date1):
		if self.current_user not in rootlist: #!='root':
			self.write("permission denied.")
			return
		filename="./coinlog/coin_add_"+date1
		try:
			f=open(filename)
			self.write("<html><body>")
			for l in f.readlines():
				self.write(l+"<br>")
			self.write("</body></html>")
		except Exception, e:
			self.set_status(404)
			self.write('sorry, coin_add_log does not exist.')
			return

class RemoveuserHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,uid):
		if self.current_user not in rootlist: #!='root':
			self.write("permission denied.")
			return
		coll=self.application.db.users
		user_doc=yield coll.find_one({"$or":[{"uid":uid},{"uid":long(uid)}]})
		if user_doc:
			self.render("removeu.html",u=user_doc)
		else:
			self.set_status(404)
			self.write('sorry, user not found<br><a href="/user">返回</a>')

	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def post(self,uid=None):
		coll1=self.application.db.users
		coll2=self.application.db.users_removed
		if uid and self.get_argument("rm",None):
			# uid=self.get_argument("uid",None)
			res=yield coll1.find_one({"uid":long(uid)})
			if res:
				yield coll1.remove({"uid":long(uid)})
				yield coll2.save(res)
				self.write("You have removed he/she successfully.")
			else:
				self.write("Error. None found.")
		else:
			self.write("Error.")

class AuditHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,audittype=None):
		coll=self.application.db.all_resources
		# file_doc=coll.find({"hidden":1}).limit(10) #for test
		audittype=self.get_argument("type",None)
		auditname=self.get_argument("name",None)
		if audittype and auditname:
			file_doc=coll.find({"tobeaudit":1,"main_type":int(audittype),"file_name":{"$regex":auditname,"$options":"-i"}}).limit(10)
		elif audittype:
			file_doc=coll.find({"tobeaudit":1,"main_type":int(audittype)}).limit(10)
		elif auditname:
			file_doc=coll.find({"tobeaudit":1,"file_name":{"$regex":auditname,"$options":"-i"}}).limit(10)
		else:
			file_doc=coll.find({"tobeaudit":1}).limit(15)
		if (yield file_doc.count()):
			file_doc=yield file_doc.to_list(length=(yield file_doc.count()))
			self.render("audit.html",res=file_doc)
		else:
			self.render("./auditfinish.html")

class AuditstudyHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,audittype=None):
		coll=self.application.db.auditing_study_resources
		# file_doc=coll.find({"hidden":1}).limit(10) #for test
		file_doc=coll.find({"unpass_reason":None}).limit(15)
		if (yield file_doc.count()):
			file_doc=yield file_doc.to_list(length=(yield file_doc.count()))
			self.render("auditstudy.html",res=file_doc)
		else:
			self.render("./auditfinish.html")

def auditautopass(file_id):
		db=db0.all_resources
		db2=db0.user_msg
		db3=db0.users

		res=db.find_one({"file_id":file_id},{"owners":1,"file_name":1})
		# res=db.find_one({"file_id":file_id,"tobeaudit":1},{"owners":1,"file_name":1})
		if not res:
			return
		uid=int(res["owners"][0]["uid"])
		u_mail=db3.find_one({"uid":uid},{"user":1,"credit":1})
		# return u_mail.get("credit",0)
		if u_mail.get("credit",0)<50:
			return 
		u_mail=u_mail["user"]

		msg = {}
		msg["type"]=0
		msg["isRead"] = 0
		msg["id"] = str(uuid.uuid1().int)
		msg["sender"] = "0"
		msg["nick"] = "0"
		msg["time"] = datetime.now().strftime('%Y-%m-%d %H:%M')

		file_name=res["file_name"]
		db.update({"file_id":file_id},{"$unset":{"hidden":1,"tobeaudit":1}})
		db.update({"file_id":file_id},{"$set":{"auditor":"autopass"}})
		msg["content"] = " 您的资源 "+file_name+" 已通过审核 "
		
		try:
			dic1={}
			dic1["file_id"]=file_id
			dic1["file_name"]=file_name
			dic1["uid"]=res["owners"][0]["uid"]
			msg1=json.dumps(dic1,ensure_ascii=False)
			pub_client.publish(channel, msg1)
		except Exception, e:
			pass
		finally:
			pass
				
		db2.update({"user":u_mail},{"$push":{"msg_list":msg}})

class AutopassHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	def get(self,file_id=None):
		file_id=self.get_argument("file_id",None)
		if file_id:
			ret=auditautopass(file_id)
			self.write(str(ret))

class AuditingHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,file_id):
		coll=self.application.db.all_resources
		file_doc=yield coll.find_one({"file_id":file_id})
		if file_doc:
			self.render("oneresaudit.html",res=file_doc)
		else:
			self.set_status(404)
			self.write('{"error":"file not found"}<br><a href="/start">返回首页</a>')

	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated
	def post(self,file_id=None):
		db=self.application.db.all_resources
		db2=self.application.db.user_msg
		db3=self.application.db.users
		msg = {}
		msg["type"]=0
		msg["isRead"] = 0
		msg["id"] = str(uuid.uuid1().int)
		msg["sender"] = "0"
		msg["nick"] = "0"
		msg["time"] = datetime.now().strftime('%Y-%m-%d %H:%M')
		file_name=self.get_argument("file_name","None")
		if self.get_argument("save",None):
			yield db.update({"file_id":file_id},{"$set":{"file_name":file_name}})
			mt=self.get_argument("Combobox1",None)
			st=self.get_argument("Combobox2",None)
			if mt and st:
				mt=int(mt)
				st=int(st)
				yield db.update({"file_id":file_id},{"$set":{"main_type":mt,"sub_type":st}})
				self.redirect('/auditing/%s'%str(file_id))
				return
			else:
				self.write("Error.")
				return

		res=yield db.find_one({"file_id":file_id,"tobeaudit":1},{"owners":1})
		if not res:
			self.write(" 不存在的 待审核资源。 (可能已被其他管理员审核) ")
			return

		if self.get_argument("yes",None):
			yield db.update({"file_id":file_id},{"$unset":{"hidden":1,"tobeaudit":1}})
			yield db.update({"file_id":file_id},{"$set":{"auditor":self.current_user}})
			msg["content"] = " 您的资源 "+file_name+" 已被管理员审核通过！ 请多保持在线，方便他人下载您的资源。"
			yield db3.update({"uid":res["owners"][0]["uid"]},{"$inc":{"credit":1}})
			try:
				dic1={}
				dic1["file_id"]=file_id
				dic1["file_name"]=file_name
				dic1["uid"]=res["owners"][0]["uid"]
				msg1=json.dumps(dic1,ensure_ascii=False)
				pub_client.publish(channel, msg1)
			except Exception, e:
				pass
			finally:
				# msg1=json.dumps(dic1,ensure_ascii=False)
				# pub_client.publish(channel, msg1)
				pass
				
		elif self.get_argument("no",None):
			reason=self.get_argument("Combobox3",0)
			reason=int(reason)
			yield db.update({"file_id":file_id},{"$unset":{"tobeaudit":1}})
			yield db.update({"file_id":file_id},{"$set":{"auditor":self.current_user}})
			tomove=yield db.find_one({"file_id":file_id})
			tomove["reason"]=recycle_reasons[reason]
			if reason==2:
				other=self.get_argument("other","")
				if other.strip()=="":
					other=recycle_reasons[reason]
				tomove["reason"]=other
				msg["content"] = " 您的资源 "+file_name+" 未被管理员审核通过，因为 "+other
			else:
				msg["content"] = " 您的资源 "+file_name+" 未被管理员审核通过，因为 "+recycle_reasons[reason]
			yield self.application.db.recycle_resources.save(tomove)
			yield db.remove({"file_id":file_id})
			yield db3.update({"uid":res["owners"][0]["uid"]},{"$inc":{"credit":-1}})
		if res:
			uid=int(res["owners"][0]["uid"])
			u_mail=yield db3.find_one({"uid":uid},{"user":1})
			# pprint(int(res["owners"][0]["uid"]))
			# pprint(u_mail)
			u_mail=u_mail["user"]
			# pprint(u_mail)
			yield db2.update({"user":u_mail},{"$push":{"msg_list":msg}})
			# print("mail:"+u_mail)
			# db2.update({"user":"guohuanright@163.com"},{"$push":{"msg_list":msg}})
		self.write(file_id+"<br>"+file_name
			+'<br> 已审查完成！<br><a href="/edit/'+file_id+ '" target="_blank">重新修改资源</a>')
		# self.write(opt)

class AuditingstudyHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,file_id):
		coll=self.application.db.auditing_study_resources
		file_doc=yield coll.find_one({"file_id":file_id})
		if file_doc:
			self.render("oneresauditstudy.html",res=file_doc)
		else:
			self.set_status(404)
			self.write('{"error":"file not found"}<br><a href="/start">返回首页</a>')

	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated
	def post(self,file_id=None):
		db=self.application.db.auditing_study_resources
		# db2=self.application.db.user_msg
		db3=self.application.db.users
		# msg = {}
		# msg["type"]=0
		# msg["isRead"] = 0
		# msg["id"] = str(uuid.uuid1().int)
		# msg["sender"] = "0"
		# msg["nick"] = "0"
		# msg["time"] = datetime.now().strftime('%Y-%m-%d %H:%M')
		

		res=yield db.find_one({"file_id":file_id,"unpass_reason":None},{"uploader":1})
		if not res:
			self.write(" 不存在的 待审核资源。 (可能已被其他管理员审核) ")
			return
		print(res)
		if self.get_argument("yes",None):
			yield db.update({"file_id":file_id},{"$set":{"auditor":self.current_user}})
			yield db3.update({"user":res["uploader"]},{"$inc":{"credit":1}})
			par=StudyResourceManager()
			par.pass_audit_resource(file_id)

			# 学习资源通过后加分
			uid=yield db3.find_one({"user":res["uploader"]},{"uid":1})
			if uid and u'uid' in uid:
				uid=uid[u'uid']
			# print uid
			how_many=self.get_argument("Combobox_FB",'0')
			fblist=[20,50,80,100]
			how_many=fblist[int(how_many)]
			# print(how_many)
			FBCoinManager.set_db(self.application.db)
			FBCoinManager.pass_audit_a_study_res(uid, file_id, how_many)

			category=self.get_argument("s2"," ")
			print(category)
			if category!=" ":
				img=coursedb.get_course_img(category)
				print(img)
				yield db.update({"file_id":file_id},{"$set":{"img":img}})
				
		elif self.get_argument("no",None):
			reason=self.get_argument("Combobox3",0)
			reason=int(reason)
			yield db.update({"file_id":file_id},{"$set":{"auditor":self.current_user}})
			if reason==5:
				other=self.get_argument("other","")
				if other.strip()=="":
					other=recycle_reasons[reason]
				# msg["content"] = " 您的资源 "+file_name+" 未被管理员审核通过，因为 "+other
				yield db.update({"file_id":file_id},{"$set":{"unpass_reason":other}})
			# msg["content"] = " 您的资源 "+file_name+" 未被管理员审核通过，因为 "+recycle_reasons[reason]
			yield db.update({"file_id":file_id},{"$set":{"unpass_reason":recycle_reasons[reason]}})
			yield db3.update({"user":res["uploader"]},{"$inc":{"credit":-1}})
		# if res:
		# 	u_mail=res["uploader"]
		# 	yield db2.update({"user":u_mail},{"$push":{"msg_list":msg}})
		self.write(file_id +'<br> 已审查完成！<br><a href="/edit/'+file_id+ '" target="_blank">重新修改资源</a>')
		# self.write(opt)

class InformHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self):
		# if self.current_user not in rootlist: #!='root':
		# 	self.write("permission denied.")
		# 	return
		f = file("/home/fbt/latest_fbt_server_py/inform.json");
		s = json.load(f)
		self.render("inform.html",res=s)
		f.close()

	def post(self):
		pass
		keys1=["type","sticky","msg"]
		keys2=["to","content"]
		dic={}
		for key1 in keys1:
			dic[key1]=int(self.get_argument(key1,0))
		for key2 in keys2:
			dic[key2]=self.get_argument(key2,"")
		s=json.dumps(dic,ensure_ascii=False,indent=2)
		with open("/home/fbt/latest_fbt_server_py/inform.json","w") as fout:
			fout.write(s)
		self.write(s)
		self.write('<br>3秒后自动返回修改页面<br><a href="/inform">立即返回修改页面</a>')
		# self.redirect("/inform")
		self.write("""<html><script language=javascript>setTimeout('window.location="http://211.149.223.98:6789/inform"',3000)</script></html>""")

class UsercntHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self):
		# if self.current_user not in rootlist: #!='root':
		# 	self.write("permission denied.")
		# 	return
		f = file("user_cnt_history");
		s=f.readlines()
		f.close()
		self.write(u"已注册用户数：<br>")
		for line in s:
			date1=line.split(":")[0]
			if date1==str(date.today()):
				self.write("%s<br>"%line)
			else:
				self.write('<a href="/userinfo/%s">%s</a><br>'%(date1,line))
		self.finish()

class UserinfoHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self,date1):
		# if self.current_user not in rootlist: #!='root':
		# 	self.write("permission denied.")
		# 	return
		try:
			f = file("user_info_history/"+date1);
		except Exception, e:
			self.write(u"没有当天的统计数据>_<")
			self.finish()
		s=f.readlines()
		f.close()
		self.write(u"新注册用户数统计：<br>")
		for line in s:
			self.write(line+"<br>")
		self.finish()

class SendregmailHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self):
		self.render("sendregmail.html")

	def post(self):
		mailadd=self.get_argument("mailto",None)
		if mailadd:
			from sendRegistryMail import sendRegistryMail
			sendRegistryMail.set_db(db0)
			ret=sendRegistryMail.sendmail(mailadd)
			if ret==None:
				self.write('发送成功！<br><a href="/start">返回首页</a>')
			else:
				self.write('发送失败！<br><a href="/start">返回首页</a>')
		else:
			self.write('请填写用户邮箱！<br><a href="/start">返回首页</a>')

class SendresetmailHandler(BaseHandler):
	@tornado.web.asynchronous
	@gen.coroutine
	@tornado.web.authenticated 
	def get(self):
		self.render("sendresetmail.html")

	def post(self):
		mailadd=self.get_argument("mailto",None)
		if mailadd:
			from sendMail import sendRegistryMail
			sendRegistryMail.set_db(db0)
			ret=sendRegistryMail.send_resetmail(mailadd)
			self.write('发送完成！<br><a href="/start">返回首页</a>')
		else:
			self.write('请填写用户邮箱！<br><a href="/start">返回首页</a>')


if __name__=="__main__":
	reload(sys)
	sys.setdefaultencoding('utf8')
	tornado.options.parse_command_line()
	http_server=tornado.httpserver.HTTPServer(Application())
	http_server.listen(options.port)
	tornado.ioloop.IOLoop.instance().start()


# encoding: utf8

import pymongo 

schools=["中国科学技术大学","中国科学院研究生院","中科院研究生院","中科院大学"]

if __name__ == '__main__':
    conn=pymongo.Connection("192.168.195.32")
    db=conn.fbt
    # for school in schools:
    for i in range(len(schools)):
        school=schools[i]
        users=db.users.find({"school":school},{"user":1})
        with file(str(i),"w") as f:
            for u in users:
                f.write(u.get("user",""))
                f.write("\n")

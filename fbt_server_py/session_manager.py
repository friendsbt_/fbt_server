# -*- coding: utf-8 -*-

from redis_handler import RedisHandler
import uuid


class SessionManager(object):
    _REDIS_SESSION_KEY_PREFIX = "session:"

    def __init__(self, redis_session=None):
        if redis_session:
            self._redis_session = redis_session
        else:
            self._redis_session = RedisHandler(RedisHandler.type_token)

    @staticmethod
    def generate_session_id():
        return uuid.uuid1().get_hex()

    def save_session_id(self, session_id, user_info):
        session_key = self._REDIS_SESSION_KEY_PREFIX + session_id
        some_time = 24 * 3600
        self._redis_session.setex(session_key, some_time, user_info)

    def get_user_info_by_sid(self, session_id):
        if session_id:
            session_key = self._REDIS_SESSION_KEY_PREFIX + session_id
            return self._redis_session.get(session_key)
        else:
            return None

    def del_session(self, session_id):
        session_key = self._REDIS_SESSION_KEY_PREFIX + session_id
        self._redis_session.delete(session_key)

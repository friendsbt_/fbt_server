# -*- coding: utf-8 -*-

__author__ = 'bone'

from university_db import UniversityDB
from util import is_validate_email
from singleton import singleton
from tornado.escape import utf8

@singleton
class MailMan(object):
    def __init__(self):
        self._985_mail_suffix = {
            "清华大学": "tsinghua.edu.cn",
            "北京大学": "pku.edu.cn",
            "厦门大学": "stu.edu.cn",
            "南京大学": "nju.edu.cn",
            "复旦大学": "fudan.edu.cn",
            "天津大学": "tju.edu.cn",
            "浙江大学": "zju.edu.cn",
            "南开大学": "nankai.edu.cn",
            "西安交通大学": "xjtu.edu.cn",
            "东南大学": "seu.edu.cn",
            "武汉大学": "whu.edu.cn",
            "上海交通大学": "sjtu.edu.cn",
            "山东大学": "sdu.edu.cn",
            "湖南大学": "hnu.edu.cn",
            "中国人民大学": "ruc.edu.cn",
            "吉林大学": "jlu.edu.cn",
            "重庆大学": "cqu.edu.cn",
            "电子科技大学": "uestc.edu.cn",
            "四川大学": "scu.edu.cn",
            "中山大学": "sysu.edu.cn",
            "华南理工大学": "scut.edu.cn",
            "兰州大学": "lzu.edu.cn",
            "东北大学": "neu.edu.cn",
            "西北工业大学": "nwpu.edu.cn",
            "哈尔滨工业大学": "nwpu.edu.cn",
            "华中科技大学": "hust.edu.cn",
            "中国海洋大学": "ouc.edu.cn",
            "北京理工大学": "bit.edu.cn",
            "大连理工大学": "dlut.edu.cn",
            "北京航空航天大学": "buaa.edu.cn",
            "北京师范大学": "bnu.edu.cn",
            "同济大学": "tongji.edu.cn",
            "中南大学": "csu.edu.cn",
            "中国科学技术大学": "ustc.edu.cn",
            "中国农业大学": "cau.edu.cn",
            "国防科学技术大学": "nudt.edu.cn",
            "中央民族大学": "muc.edu.cn",
            "华东师范大学": "ecnu.edu.cn",
            "西北农林科技大学": "nwsuaf.edu.cn",
        }
        self._udb = UniversityDB()
        assert len(self._985_mail_suffix) == len(self._udb.get_985university())-1
        for u, m in self._985_mail_suffix.iteritems():
            assert self._udb.is_985(u)
            assert is_validate_email("test@"+m)
        self._985_mail_suffix["中国科学院"] = "ucas.ac.cn"

    def is_985_mail(self, mail, university):
        university = utf8(university)
        if university == "北京大学" and "bjmu.edu.cn" in mail:
            return True
        return is_validate_email(mail) and university in self._985_mail_suffix \
               and self._985_mail_suffix[university] in mail

if __name__ == "__main__":
    man = MailMan()
    assert man.is_985_mail("test@mails.ucas.ac.cn", "中国科学院")
    assert man.is_985_mail("test@csu.edu.cn", "中南大学")
    assert man.is_985_mail("test@bjmu.edu.cn", "北京大学")
    assert not man.is_985_mail("test@whatever.edu.cn", "中南大学")
    assert not man.is_985_mail("test@#csu.edu.cn","中南大学")

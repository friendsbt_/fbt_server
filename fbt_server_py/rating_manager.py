# -*- coding: utf-8 -*-
__author__ = 'bone-lee'

from tornado import gen
from datetime import datetime
from time import time
import motorclient

class RatingManager(object):
    def __init__(self, db = None):
        if db:
            self._db = db
        else:
            self._db = motorclient.fbt

    @gen.coroutine
    def rating(self, file_id, score, user, uid):
        resource = yield self.get_collection().find_one({"file_id": file_id, "uid": uid})
        if resource:
            vary_score = score - resource["rating"]
            resource["rating"] = score
            resource["index_ctime"] = long(time()),
            resource["ctime"] = datetime.now().strftime('%Y-%m-%d %H:%M'),
            yield self.get_collection().save(resource)
            raise gen.Return([0, vary_score])
        else:
            score_item = {"file_id": file_id,"rating": score, "who": user, "uid": uid, "index_ctime": long(time()), "ctime": datetime.now().strftime('%Y-%m-%d %H:%M')}
            yield self.get_collection().insert(score_item)
            raise gen.Return([1, score])

    def get_collection(self):
        return self._db.rating

class RatingManagerP2P(RatingManager):
    def __init__(self, db = None):
        RatingManager.__init__(self, db)

    def get_collection(self):
        return self._db.rating2

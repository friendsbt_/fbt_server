#!/usr/bin/env python

from constant import TOKEN_SECRET, STAR_CLASS, CHAT_PORT
from token_handler import TokenHandler
from message_manager import MessageManager
from rpctcpclient import FutureRPCClient
from users_manager import UserManager

import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.options
from tornado import autoreload
import json
from tornado import gen
from tornado.escape import utf8


class BaseHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS")
        self.set_header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')

    def get_user_by_token(self, token):
        #return self.application.token_handler.get_user_by_token(self.get_cookie("user_token"))
        return self.application.token_handler.get_user_by_token(token)

    @property
    def msg_man(self):
        return self.application.msg_man

    @property
    def users_manager(self):
        return self.application.users_manager

    @property
    def rpc_client(self):
        return self.application.RPC_client


class OnlineUsersHandler(BaseHandler):
    ERROR = {"NONE": 0, "ARGUMENT_ERR": 1}

    def get(self):
        password = self.get_argument("password", "")
        if password == "liuyanan":
            users = self.msg_man.get_users()
            self.write(json.dumps({"err": self.ERROR["NONE"],
                       "how_many": len(users), "list": users}))
        else:
            self.write(json.dumps({"err": self.ERROR["ARGUMENT_ERR"]}))


class MessageProxyHandler(BaseHandler):
    ERROR = {"NONE": 0, "ARGUMENT_ERR": 1}

    def post(self):
        try:
            data = json.loads(self.request.body)
        except:
            self.write(json.dumps({"err": self.ERROR["ARGUMENT_ERR"]}))
        else:
            # TODO security problem
            if data["password"] == "xiaoyuanxingkong":
                self.msg_man.new_system_message(data["content"], data["user_to"], data["type"])
                self.write(json.dumps({"err": self.ERROR["NONE"]}))
            else:
                self.write(json.dumps({"err": self.ERROR["ARGUMENT_ERR"]}))


class MessageHandler(BaseHandler):
    ERROR = {"NONE": 0, "ARGUMENT_ERR": 1, "CONTENT_LENGTH_ERR": 2,
             "USER_OFFLINE": 3}
    ARG_LENGTH_LIMIT = {"content": 1000}

    @tornado.web.asynchronous
    @gen.coroutine
    def get(self):
        user = self.get_argument("user", "")
        if user and self.get_user_by_token(self.get_argument("user_token", "")) == utf8(user):
            # Save the future returned by wait_for_messages so we can cancel
            # it in wait_for_messages
            self.user = user
            message = yield self.msg_man.wait_for_message(user)
            if self.request.connection.stream.closed():
                self.msg_man.cancel_wait(self.user)
                return
            if message:
                self.write(json.dumps({"err": self.ERROR["NONE"], "message": message}))
        else:
            self.write(json.dumps({"err": self.ERROR["ARGUMENT_ERR"]}))
        self.finish()

    def on_connection_close(self):
        self.msg_man.cancel_wait(self.user)

    @tornado.web.asynchronous
    @gen.coroutine
    def post(self, *args, **kwargs):
        """
        Post a message here
        """
        data = json.loads(self.request.body)
        if self.get_user_by_token(data["user_token"]) == utf8(data["user_from"]):
            if len(data["content"]) <= self.ARG_LENGTH_LIMIT["content"]:
                user_from = data["user_from"]
                user_to = data["user_to"]
                db_user = yield self.users_manager.find_user(user_from)
                db_user_to = yield self.users_manager.find_user(user_to)
                if db_user and db_user_to:
                    ok = self.msg_man.new_private_message(user_from, data["user_to"], data["content"], db_user, db_user_to)
                    if ok:
                        self.write(json.dumps({"err": self.ERROR["NONE"]}))
                    else:
                        self.write(json.dumps({"err": self.ERROR["USER_OFFLINE"]}))
                else:
                    self.write(json.dumps({"err": self.ERROR["ARGUMENT_ERR"]}))
            else:
                self.write(json.dumps({"err": self.ERROR["CONTENT_LENGTH_ERR"]}))
        else:
            self.write(json.dumps({"err": self.ERROR["ARGUMENT_ERR"]}))
        self.finish()


class MainHandler(tornado.web.RequestHandler):
    """
    The main handler
    """

    def get(self, *args, **kwargs):
        self.write("hello world.")


class ChatApplication(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/', MainHandler),
            (r'/online', OnlineUsersHandler),
            (r'/message/proxy', MessageProxyHandler),
            (r'/message', MessageHandler),
        ]

        settings = {
            'template_path': 'templates',
            'static_path': 'static',
            #"cookie_secret": "AISAOWKJKHWHKQWHKKHDKKZJXBKHKSHKEHKEHWIEIOWSCKNSZ",
            #"xsrf_cookies": True,
        }
        tornado.web.Application.__init__(self, handlers, **settings)
        self.msg_man = MessageManager()
        self.users_manager = UserManager()
        self.RPC_client = FutureRPCClient()
        self.token_handler = TokenHandler(TOKEN_SECRET)


if __name__ == '__main__':
    tornado.options.parse_command_line()
    app = ChatApplication()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(CHAT_PORT)
    ioloop = tornado.ioloop.IOLoop.instance()
    autoreload.start(ioloop)
    ioloop.start()

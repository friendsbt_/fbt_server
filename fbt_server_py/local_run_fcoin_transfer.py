import mock
import redis
from motor import MotorClient
from pymongo import MongoClient

if __name__ == "__main__":
    print "************************************************"
    print "Please ensure that redis and mongoDB is running."
    print "Then use http://localhost:8787 to browse."
    print "************************************************"
    mock_redis = redis.StrictRedis()
    sync_db = MongoClient()
    db = MotorClient()
    with mock.patch('redis_handler.RedisHandler.redis_client', return_value=mock_redis) as whate_ever:
        with mock.patch('redis_cluster_proxy.Redis', return_value=mock_redis) as whate_ever1:
            with mock.patch("pymongo.MongoReplicaSetClient", return_value=sync_db) as what_ever2:
                with mock.patch("motor.MotorReplicaSetClient", return_value=db) as what_ever3:
                    from fcoin_transfer import main
                    main()

# -*- coding: utf-8 -*-
__author__ = 'bone'

import os
import base64
import logging

import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.options
import simplejson as json
import tornado.escape
import tornado.gen

from constant import DEBUG_ENV, OUR_WEB_URL, STAR_POSTS_THRESHOLD, STAR_THUMB_NUM_THRESHOLD, STAR_CLASS, CLASS_STAR
from constant import CHAT_PORT
from constant import TOKEN_SECRET
from constant import MAX_PREVIEW_SIZE
from find_password import send_bind_mail
from users_manager import UserManager
from session_manager import SessionManager
from user_msg_center import SystemMessageProxy
from university_db import UniversityDB
from coin_manager import CoinManager
from qiniu_utils import generate_upload_token, generate_image_upload_token
from qiniu_utils import get_download_link, get_preview_link, get_key_from_download_link
from qiniu_utils import office_to_pdf
from study_resource_manager import StudyResourceManager
from university_course_manager import RedisDBManager
from course_db import CourseDB
from comment_manager import ResourceCommentManager, CourseCommentManager
from rating_manager import RatingManager
from request_handler import SendResetEmailHandler, ResetPwdHandler
from user_resource_manager import UserResourceManager
from reward_study_manager import RewardManager
from user_upload_log_manager import UserUploadLogManager
from question_manager import QuestionManager
from QA_manager import QAManager
from token_handler import TokenHandler
from message_manager import MessageManager
from find_password import send_invitation_mail

from mail_manager import MailMan
from util import fetch_token_in_cache, is_validate_email
from redis_handler import RedisHandler
from captcha import create_validate_code
from StringIO import StringIO
from random import sample
from time import time
from datetime import datetime

XSRF_KEY_EPIRE_SECONDS = 60
TEXT_PIN_NUMBER = 5


# TODO async http post
# def send_message(phone, pin):
#     _http_client = AsyncHTTPClient()
#     data = {"mobile": phone,
#             "message": "尊敬的用户，您的验证码是：%s，请在10分钟内输入并注册【校园星空】" % pin}
#     req = HTTPRequest("http://sms-api.luosimao.com/v1/send.json",
#                       method="POST",
#                       body=urllib.urlencode(data),
#                       auth_username="api",
#                       auth_password="key-7fab304c396764b5c0f2159fceb717f7")
#     return _http_client.fetch(req)


# def gen_pin(len):
#     return ''.join([str(randint(0, 9)) for _ in range(len)])


class BaseHandler(tornado.web.RequestHandler):
    @property
    def token_manager(self):
        return self.application.token_manager

    @property
    def study_resource_manager(self):
        return self.application.study_resource_manager

    @property
    def user_resource_manager(self):
        return self.application.user_resource_manager

    @property
    def rating_manager(self):
        return self.application.rating_manager

    @property
    def system_msg_proxy(self):
        return self.application.system_msg_proxy

    @property
    def message_manager(self):
        return self.application.message_manager

    @property
    def redis_db_manager(self):
        return self.application.redis_db_manager

    @property
    def university_db(self):
        return self.application.university_db

    @property
    def user_log_manager(self):
        return self.application.user_log_manager

    @property
    def course_db(self):
        return self.application.course_db

    @property
    def resource_comment_manager(self):
        return self.application.resource_comment_manager

    @property
    def course_comment_manager(self):
        return self.application.course_comment_manager

    @property
    def users_manager(self):
        return self.application.users_manager

    @property
    def session_manager(self):
        return self.application.session_manager

    @property
    def QA_manager(self):
        return self.application.QA_manager

    @property
    def reward_manager(self):
        return self.application.reward_manager

    @property
    def coin_manager(self):
        return self.application.coin_manager

    @property
    def question_manager(self):
        return self.application.question_manager

    @property
    def lru_redis(self):
        return self.application.lru_redis

    def get_current_user(self):
        return self.application.token_manager.get_user_by_token(self.get_cookie("user_token"))

    def set_default_headers(self):
        ALLOW_ORIGIN = "*"  # http://test.friendsbt.com
        self.set_header("Access-Control-Allow-Origin", ALLOW_ORIGIN)

    def get_question_link(self, question_id, question_title, answer_id=""):
        if answer_id:
            return u"<a href='%s/#/qadetail/%s#%s' target='_blank'>%s</a>" % (OUR_WEB_URL, question_id, answer_id, question_title)
        else:
            return u"<a href='%s/#/qadetail/%s' target='_blank'>%s</a>" % (OUR_WEB_URL, question_id, question_title)

    def get_user_link(self, db_user):
        user_info = tornado.escape.to_unicode(db_user["real_name"]) + u"(来自" +  tornado.escape.to_unicode(db_user["university"]) + u")"
        user_page_url = "/user_page?user="+base64.b64encode(db_user["user"])
        return u'<a href="%s" target="_blank">%s</a>' % (user_page_url, user_info)

    @tornado.gen.coroutine
    def extract_user_info_from(self, db_user):
        university = ""
        college = ""
        if "university" in db_user and "college" in db_user and \
                self.university_db.is_valid_university_and_college(
                    tornado.escape.utf8(db_user["university"]), tornado.escape.utf8(db_user["college"])):
            university = db_user["university"]
            college = db_user["college"]
        real_name = ("real_name" in db_user and db_user["real_name"]) or ""
        gender = ("gender" in db_user and db_user["gender"]) or ""
        nick_name = ("nick_name" in db_user and db_user["nick_name"]) or ""
        thumb_num = ("thumb_num" in db_user and db_user["thumb_num"]) or 0
        thanks_coin = ("thanks_coin" in db_user and db_user["thanks_coin"]) or 0
        answers_num = ("answers_num" in db_user and db_user["answers_num"]) or 0
        followers_num = ("followers_num" in db_user and db_user["followers_num"]) or 0
        desc = ("desc" in db_user and db_user["desc"]) or ""
        total_fb, study_fb = yield self.coin_manager.get_coin(db_user["uid"])
        allow_login = bool(university and college and real_name and gender and nick_name)
        if allow_login:
            messages, msg_cnt = self.message_manager.get_recent_msg(db_user["user"])
        else:
            messages, msg_cnt = [], 0
        raise tornado.gen.Return({"nick_name": nick_name, "icon": db_user["icon"],
                "allow_login": allow_login,
                "university": university, "college": college, "total_coins": total_fb, "coins_by_study": study_fb,
                "study_coins": study_fb, "message_cnt": msg_cnt, "recent_messages": messages, "username": db_user["user"], "uid": db_user["uid"],
                "followers_num": followers_num, "answers_num": answers_num, "thanks_coin": thanks_coin,
                "thumb_num": thumb_num, "desc": desc, "real_name": real_name, "password": ("password" in db_user and db_user["password"]) or ""})


class CourseSearchHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        by_all = self.get_argument("by_all", 0)
        page = self.get_argument("page", '1')
        if not page.isdigit():
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
            self.finish()
            return
        page = int(page)
        keyword = self.get_argument("keyword", "").strip()
        if len(keyword) > 30:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
            self.finish()
            return
        keyword = tornado.escape.utf8(keyword)
        if not by_all:
            is_985 = self.get_argument("is_985", 0)
            is_211 = self.get_argument("is_211", 0)
            if keyword:
                try:
                    is_985 = int(is_985)
                    is_211 = int(is_211)
                except ValueError as e:
                    self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
                else:
                    res_list, cur_page, total_page = self.redis_db_manager.search_resource_by_university(keyword, page,
                                                                                                         is_211, is_985)
                    self.write(json.dumps({"err": self.ERR["NONE"], "resource_list": res_list, "total_page": total_page,
                                           "current_page": cur_page}))
            else:
                self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            if "/course/search" in self.request.uri:
                total_page, current_page, course_list = yield self.study_resource_manager.search_course(keyword, page)
            else:
                total_page, current_page, course_list = yield self.study_resource_manager.search_course2(keyword, page)
            ret = {"err": self.ERR["NONE"], "course_list": course_list, "total_page": total_page,
                                   "current_page": current_page}
            if current_page == 1:
                ret["hot_keywords"] = self.study_resource_manager.top_search_keywords()
            self.write(json.dumps(ret))
        self.finish()


class UniversityResourceHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        university = tornado.escape.utf8(self.get_argument("university", "")).strip()
        if self.university_db.is_valid_university(university):
            university_resource_num, college_resource_num, top_college = self.redis_db_manager.get_university_nav_info(
                university)
            total_page, current_page, college_course_list = yield self.study_resource_manager.get_college_course_overview(
                university, top_college, 1)
            self.write(json.dumps({"err": self.ERR["NONE"], "university_resource_num": university_resource_num,
                                   "college_resource_num": college_resource_num,
                                   "current_college": top_college,
                                   "current_college_course_list": college_course_list,
                                   "current_college_course_total_page": total_page}))
        else:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        self.finish()


class CollegeResourceHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        university = tornado.escape.utf8(self.get_argument("university", "")).strip()
        college = tornado.escape.utf8(self.get_argument("college", "")).strip()
        page = self.get_argument("page", 1)
        try:
            page = int(page)
        except ValueError as e:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            if self.university_db.is_valid_university_and_college(university, college):
                total_page, current_page, college_course_list = yield self.study_resource_manager.get_college_course_overview(
                    university, college, page)
                self.write(json.dumps(
                    {"err": self.ERR["NONE"], "course_list": college_course_list, "total_page": total_page,
                     "current_page": current_page}))
            else:
                self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        self.finish()


class CourseListHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    def get(self):
        university = tornado.escape.utf8(self.get_argument("university", "")).strip()
        college = tornado.escape.utf8(self.get_argument("college", "")).strip()
        # keyword = tornado.escape.utf8(self.get_argument("keyword","")).strip()
        if self.university_db.is_valid_university_and_college(university, college):
            college_course_list = self.redis_db_manager.get_college_courses(university, college)
            self.write(json.dumps({"err": self.ERR["NONE"], "course_list": college_course_list}))
        else:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})


class CourseViewHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        page = self.get_argument("page", 1)
        location = self.get_argument("course_id", "")
        try:
            page = int(page)
        except ValueError as e:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            if location:
                total_page, current_page, resource_list = yield self.study_resource_manager.get_course_resources_by_id(
                    location, page)
                self.write(json.dumps(
                    {"err": self.ERR["NONE"], "resource_list": resource_list, "total_page": total_page,
                     "current_page": current_page}))
            else:
                self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        self.finish()


class ResourceListHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    def get(self):
        page = self.get_argument("page", 1)
        is_985 = self.get_argument("is_985", 0)
        is_211 = self.get_argument("is_211", 0)
        try:
            page = int(page)
            is_985 = int(is_985)
            is_211 = int(is_211)
        except ValueError as e:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            res_list, cur_page, total_page = self.redis_db_manager.get_university_course_overview(page, is_211, is_985)
            self.write(json.dumps({"err": self.ERR["NONE"], "resource_list": res_list, "total_page": total_page,
                                   "current_page": cur_page}))


class ResourceDetailHandler(BaseHandler):
    ERR = {"NONE": 0}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        file_id = self.get_argument("fid", "")
        audited = int(self.get_argument("audited", 1))
        if audited:
            resource_info = yield self.study_resource_manager.get_a_resource(file_id)
            total_page, cur_page, comments_list = yield self.resource_comment_manager.get_comments(file_id)
            self.write(json.dumps({"err": self.ERR["NONE"], "info": resource_info, "comments_list": comments_list,
                                   "comments_total_page": total_page, "comments_current_page": cur_page}))
        else:
            resource_info = yield self.study_resource_manager.get_a_resource2(file_id)
            self.write(json.dumps(
                {"err": self.ERR["NONE"], "info": resource_info, "comments_list": [], "comments_total_page": 0,
                 "comments_current_page": 1}))
        self.finish()


class CourseDetailHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        course_id = self.get_argument("course_id", "")
        if course_id:
            course_info = yield self.study_resource_manager.get_course2(course_id)
            total_page, cur_page, comments_list = yield self.course_comment_manager.get_comments(course_id)
            self.write(json.dumps({"err": self.ERR["NONE"], "info": course_info, "comments_list": comments_list,
                                   "comments_total_page": total_page, "comments_current_page": cur_page}))
        else:
            self.write(json.dumps({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}))
        self.finish()


class ResourceRecommendHandler(BaseHandler):
    ERR = {"NONE": 0}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        ret = yield self.study_resource_manager.get_recommended_resources()
        self.write(json.dumps({"err": self.ERR["NONE"], "recommend_resource": ret}))
        self.finish()


class MoreResourceRecommendHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        tag = self.get_argument("tag", "")
        page = self.get_argument("page", 1)
        try:
            page = int(page)
        except ValueError as e:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            if tag and len(tag) <= 50:
                tag = tornado.escape.utf8(tag).strip()
                total_page, current_page, res_list = yield self.study_resource_manager.get_more_recommended_resources(
                    tag, page)
                self.write(json.dumps({"err": self.ERR["NONE"], "total_page": total_page, "current_page": current_page,
                                       "resource_list": res_list}))
            else:
                self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        self.finish()


class LogoutHandler(BaseHandler):
    def get(self):
        # user = self.get_current_user()
        # if user:
        #     try:
        #         self.redis_db_manager.record_logout_user(user)
        #     except Exception as e:
        #         print "err record user:" + e.message
        # sid = self.get_cookie("sid", None)
        # if sid:
        #     self.session_manager.del_session(sid)
        self.clear_all_cookies()
        self.write(json.dumps({"OK": 0}))


# class OnlineUserHandler(BaseHandler):
#     def get(self):
#         if self.get_argument("key", "") == "FriendsBT":
#             users = self.redis_db_manager.get_online_users()
#             self.write(json.dumps({"online_users": users, "how_many": len(users)}))
#         else:
#             self.write(json.dumps({"err": 1}))


class RewardExpiredHandler(BaseHandler):
    '''
    for timer TODO add it in crontab
    '''

    def get(self):
        if self.get_argument("key", "") == "FriendsBT":
            users = self.reward_manager.process_expired_reward()
            self.write(json.dumps({"online_users": users, "how_many": len(users)}))
        else:
            self.write(json.dumps({"err": 1}))


class UniversityDBHandler(BaseHandler):
    def get(self):
        province = tornado.escape.utf8(self.get_argument("province", "")).strip()
        university = tornado.escape.utf8(self.get_argument("university", "")).strip()
        if province:
            if "v2" in self.request.uri:
                self.write(json.dumps({"err": 0, "list": self.university_db.get_university_by_province(province)}))
            else:
                self.write(json.dumps({"err": 0, "list": self.university_db.get_university_by_province(province, False)}))
        elif university:
            if "v2" in self.request.uri:
                self.write(json.dumps({"err": 0, "list": self.university_db.get_college_by_university(university)}))
            else:
                self.write(json.dumps({"err": 0, "list": self.university_db.get_college_by_university(university, False)}))
        else:
            self.write(json.dumps({"err": 1, "info": "参数错误"}))


# class CourseDBHandler(BaseHandler):
#     def get(self):
#         class1 = tornado.escape.utf8(self.get_argument("class1","")).strip()
#         class2 = tornado.escape.utf8(self.get_argument("class2","")).strip()
#         if class1 and class2:
#             self.write(json.dumps({"err": 0, "list": self.course_db.get_course_list(class1, class2)}))
#         elif class1:
#             self.write(json.dumps({"err": 0, "list": self.course_db.get_course_list(class1)}))
#         else:
#             self.write(json.dumps({"err": 0, "list": self.course_db.get_course_overview()}))

class UserInfoHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "FAILED": 2, "UNAUTHENTICATED": 3}

    @tornado.gen.coroutine
    def send_fake_invite_msg(self, user, university):
        _, _, users = yield self.users_manager.get_user_by_thumb_num(university=university)
        if users:
            fake_user = sample(users, 1)[0]
            _, _, latest_questions = yield self.QA_manager.get_experience_overview(current_page=1, tag_class="校园", sort_by=self.QA_manager.SORT_BY["time"])
            Q = sample(latest_questions, 1)[0]
            # sleep 1 minute
            yield tornado.gen.Task(tornado.ioloop.IOLoop.instance().add_timeout, time() + 60)
            yield self.system_msg_proxy.send_system_message(user, MessageManager.MSG_TYPE["get_a_fans"],
                                                            u"发现一枚用户%s邀请您来帮忙回答问题%s！" % (self.get_user_link(fake_user), self.get_question_link(Q["id"], Q["title"])))

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        data = json.loads(self.request.body)
        if not user:
            user = data["user"]
        db_user = yield self.users_manager.find_user(user)
        university = data["university"]
        college = data["college"]
        real_name = data["name"]
        gender = data["gender"]
        nick_name = data["nick"]
        local_img_path = "/static"
        if db_user["icon"][:len(local_img_path)] == local_img_path:
            user_page_url = "/user_page?user="+base64.b64encode(user)
            yield self.system_msg_proxy.send_system_message(user, MessageManager.MSG_TYPE["other_system_msg"], u'系统检测到您还没有个人头像呢，赶紧到<a href="%s" target="_blank">我的主页</a>上传头像吧，'
                                                u'可以获得"发起问答权限"，以及更多"点赞"、更多"粉丝"哦！' % user_page_url)
        if self.university_db.is_valid_university_and_college(university, college) and real_name and gender and nick_name:
            yield self.users_manager.change_user_info(user, university, college, gender, real_name, nick_name)
            ret = {"err": self.ERR["NONE"]}
            self.write(json.dumps(ret))
            self.finish()
            yield self.send_fake_invite_msg(user, university)
        else:
            ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
            self.write(json.dumps(ret))
            self.finish()

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            user_info = yield self.users_manager.get_user_info(user)
            ret = {"err": self.ERR["NONE"], "user_info": user_info}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()


class StarUserAuthHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "FAILED": 2, "UNAUTHENTICATED": 3,
           "985_EMAIL_ERR": 4, "THUMB_NUM_NOT_ENOUGH": 5, "POST_NUM_NOT_ENOUGH": 6}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        university = tornado.escape.utf8(self.get_argument("university", "")).strip()
        college = tornado.escape.utf8(self.get_argument("college", "")).strip()
        entrance_year = tornado.escape.utf8(self.get_argument("entrance_year", "")).strip()
        degree = tornado.escape.utf8(self.get_argument("degree", "")).strip()
        class2 = tornado.escape.utf8(self.get_argument("class2", "")).strip()
        university_mail = tornado.escape.utf8(self.get_argument("university_mail", "")).strip()

        user = self.get_current_user()
        if user:
            if self.university_db.is_valid_university_and_college(university, college) and entrance_year\
                    and degree and class2 and university_mail and self.QA_manager.is_valid_tag_class(class2):
                yield self.users_manager.set_star_user_info(user, university, college, degree,
                                                            entrance_year, university_mail)
                pass_audit = False
                if self.university_db.is_985(university) or tornado.escape.utf8(university) == "中国科学院":
                    if MailMan().is_985_mail(university_mail, university):
                        pass_audit = True
                        ret = {"err": self.ERR["NONE"]}
                    else:
                        pass_audit = False
                        ret = {"err": self.ERR["985_EMAIL_ERR"]}
                else:
                    # check user's posts
                    thumb_num = yield self.users_manager.get_thumb_num(user)
                    post_num = yield self.users_manager.get_post_num_by_tag(user, class2)
                    if DEBUG_ENV:
                        pass_audit = True
                        ret = {"err": self.ERR["NONE"]}
                    else:
                        if thumb_num < STAR_THUMB_NUM_THRESHOLD:
                            ret = {"err": self.ERR["THUMB_NUM_NOT_ENOUGH"]}
                        elif post_num < STAR_POSTS_THRESHOLD:
                            ret = {"err": self.ERR["POST_NUM_NOT_ENOUGH"]}
                        else:
                            pass_audit = True
                            ret = {"err": self.ERR["NONE"]}
                if pass_audit:
                    user_info = yield self.users_manager.find_user(user, preview=True)
                    user_nick = user_info["nick_name"]
                    token = self.QA_manager.generate_auth_token(user, class2)
                    # TODO(bonelee): add this url to constant
                    url = u"{}/user/star/validate?user={}&token={}".format(OUR_WEB_URL, user, token)
                    # send invaidation mail with validate mail
                    send_bind_mail(university_mail, user_nick, url)
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            user_info = yield self.users_manager.get_user_info(user)
            ret = {"err": self.ERR["NONE"], "university": ("university" in user_info and user_info["university"]) or None,
                   "college": "college" in user_info and user_info["college"] or None, "degree": ("degree" in user_info and user_info["degree"]) or None,
                   "entrance_year": ("entrance_year" in user_info and user_info["entrance_year"]) or None,
                   "university_mail": ("university_mail" in user_info and user_info["university_mail"]) or None,
                   }
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()


class StarUserValidateHandler(BaseHandler):
    ERR = {"NONE": 0, "EXPIRED_TOKEN": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        token = self.get_argument("token", "")
        user = self.get_argument("user", "")
        tag_class = self.QA_manager.fetch_star_tag_of_auth_token(token, user)
        if tag_class is not None:
            ok = yield self.users_manager.set_star_user(user, tag_class)
            if ok:
                yield self.system_msg_proxy.send_system_message(user, MessageManager.MSG_TYPE["other_system_msg"], u"恭喜，您通过了校园星空%s之星审核！"
                                                    u"此时此刻，我对您的敬仰如滔滔江水延绵不绝！！！" % tornado.escape.to_unicode(tag_class))
                self.render('auth_star.html', tag_class=tag_class, error=None)
            else:
                self.render('auth_star.html', error = u"亲，参数错误！", tag_class=None)
        else:
            self.render('auth_star.html', error = u"亲，网页过期啦，请重新认证吧！", tag_class=None)


class StarUserSetHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        who = self.get_argument("who", "").strip()
        passwd = self.get_argument("password", "").strip()
        tag_class = self.get_argument("class", u"校园").strip()
        user = self.get_current_user()
        if user:
            db_user = yield self.users_manager.find_user(user)
            if db_user and passwd == "liuyanan" and who and self.QA_manager.is_valid_tag_class(tag_class):
                if "star/set" in self.request.uri:
                    yield self.users_manager.set_star_user(who, tag_class)
                    ret = {"err": self.ERR["NONE"]}
                else:
                    yield self.users_manager.unset_star_user(who, tag_class)
                    ret = {"err": self.ERR["NONE"]}
            else:
                ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()


class StatisticOfTodayHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        passwd = self.get_argument("password", "").strip()
        user = self.get_current_user()
        if user:
            db_user = yield self.users_manager.find_user(user)
            if db_user and passwd == "liuyanan":
                today_users = yield self.users_manager.get_today_users()
                today_download = yield self.user_resource_manager.get_today_download()
                today_questions = yield self.QA_manager.get_today_questions()
                today_answers = yield self.QA_manager.get_today_answers()
                today_auditing_res, today_uploaded_res = yield self.study_resource_manager.get_today_upload()
                self.render('statistic.html', today_download=today_download,
                            today_answers=today_answers, today_users=today_users,
                            today_questions=today_questions, today_auditing_res=today_auditing_res,
                            today_uploaded_res=today_uploaded_res, date_time=datetime.now().strftime('%Y-%m-%d'))
            else:
                ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
                self.write(ret)
                self.finish()
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
            self.write(ret)
            self.finish()


class UserInfoPreviewHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_argument("user", "")
        user_info = yield self.users_manager.find_user(user, preview=True)
        ret = {"err": self.ERR["NONE"], "user_info": user_info}
        self.write(json.dumps(ret))
        self.finish()


class RegisterHandler(BaseHandler):
    ERR = {"NONE": 0, "EMAIL_REGISTERED": 1, "NICK_EXISTS": 2, "ARGUMENT_NULL": 3, "UNIVERSITY_INFO_ERR": 4,
           "WRONG_FORMAT": 5, "REAL_NAME_TOO_LONG": 6, "ARGUMENT_ERR": 7, "CAPTCHA_EXPIRE": 8}

    def get(self):
        self.render('register.html', error=None)

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_argument("user", "").strip()
        passwd = self.get_argument("passwd", "").strip()
        nick = self.get_argument("nick", "").strip()
        real_name = self.get_argument("name", "").strip()
        gender = self.get_argument("gender", "").strip()
        school = self.get_argument("school", "").strip()
        college = self.get_argument("college", "").strip()
        referee = self.get_argument("referee", "").strip()
        input_code = self.get_argument("captcha", "").strip().lower()

        if user and passwd and nick and input_code:
            if not is_validate_email(user):
                ret = {"err": self.ERR["WRONG_FORMAT"], "info": u"输入信息格式错误"}
            elif len(real_name) > 20 or len(user) > 50 or len(passwd) > 100 or len(nick) > 30 or len(
                    real_name) > 10 or len(gender) > 2:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": u"注册参数过长！"}
            else:
                school = tornado.escape.utf8(school)
                college = tornado.escape.utf8(college)
                real_name = tornado.escape.utf8(real_name)
                gender = tornado.escape.utf8(gender)

                register_info = yield self.users_manager.is_user_registered(user, nick)
                if register_info == self.users_manager.register_info["SAME_EMAIL"]:
                    ret = {"err": self.ERR["EMAIL_REGISTERED"], "info": u"该邮箱已被注册，请选用其他邮箱！"}
                elif register_info == self.users_manager.register_info["SAME_NICK"]:
                    ret = {"err": self.ERR["NICK_EXISTS"], "info": u"昵称已经存在,请选用其他昵称"}
                else:
                    xsrf_tuple = self._get_raw_xsrf_token()
                    xsrf_key = 'xsrf.' + '|'.join(map(str, xsrf_tuple))
                    code = self.lru_redis.get(xsrf_key)
                    if not code:
                        ret = {"err": self.ERR["CAPTCHA_EXPIRE"], "info": u"验证码已过期，请重新获取并验证！"}
                    elif input_code != code:
                        ret = {"err": self.ERR["CAPTCHA_EXPIRE"], "info": u"验证码不正确，请重新输入！"}
                    else:
                        user_icon_url = self.static_url(
                            'images/user_icon/' + self.users_manager.generate_user_icon() + '.jpg')
                        uid = yield self.users_manager.register_user(user, passwd, user_icon_url, real_name, school,
                                                                     college, nick, gender, "")
                        yield self.coin_manager.register_ok(uid, user)

                        if referee:
                            if is_validate_email(referee):
                                user_info = yield self.users_manager.find_user(referee)
                            else:
                                user_info = yield self.users_manager.find_user_by_nick(referee)
                            if user_info:
                                referee_uid = user_info["uid"]
                                referee_email = user_info["user"]
                                succeed = yield self.coin_manager.refer_ok(uid, user, referee_uid, referee_email)
                                if succeed:
                                    yield self.system_msg_proxy.send_system_message(referee_email, MessageManager.MSG_TYPE["other_system_msg"], u"您推荐好友 %s 成功注册，系统奖励了您 %d F" % (
                                    user, self.coin_manager.REFEREE_COIN))
                                    yield self.system_msg_proxy.send_system_message(user, MessageManager.MSG_TYPE["other_system_msg"], u"由于您的好友 %s 推荐，系统奖励了您 %d F" % (
                                    referee, self.coin_manager.REFER_COIN))

                        ret = {"err": self.ERR["NONE"]}
        else:
            ret = {"err": self.ERR["ARGUMENT_NULL"], "info": u"缺少必要的注册信息，请填写！"}
        self.write(json.dumps(ret))
        self.finish()


class RegisterNewHandler(BaseHandler):
    ERR = {"NONE": 0, "EMAIL_REGISTERED": 1, "NICK_EXISTS": 2, "ARGUMENT_NULL": 3, "UNIVERSITY_INFO_ERR": 4,
           "WRONG_FORMAT": 5, "REAL_NAME_TOO_LONG": 6, "ARGUMENT_ERR": 7, "CAPTCHA_EXPIRE": 8}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_argument("user", "").strip()
        passwd = self.get_argument("passwd", "").strip()
        input_code = self.get_argument("captcha", "").strip().lower()

        if user and passwd and input_code:
            if not is_validate_email(user):
                ret = {"err": self.ERR["WRONG_FORMAT"], "info": u"输入信息格式错误"}
            elif len(user) > 50 or len(passwd) > 100:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": u"注册参数过长！"}
            else:
                register_info = yield self.users_manager.is_email_registered(user)
                if register_info == self.users_manager.register_info["SAME_EMAIL"]:
                    ret = {"err": self.ERR["EMAIL_REGISTERED"], "info": u"该邮箱已被注册，请选用其他邮箱！"}
                else:
                    xsrf_tuple = self._get_raw_xsrf_token()
                    xsrf_key = 'xsrf.' + '|'.join(map(str, xsrf_tuple))
                    code = self.lru_redis.get(xsrf_key)
                    if not code:
                        ret = {"err": self.ERR["CAPTCHA_EXPIRE"], "info": u"验证码已过期，请重新获取并验证！"}
                    elif input_code != code:
                        ret = {"err": self.ERR["CAPTCHA_EXPIRE"], "info": u"验证码不正确，请重新输入！"}
                    else:
                        user_icon_url = self.static_url(
                            'images/user_icon/' + self.users_manager.generate_user_icon() + '.jpg')
                        uid = yield self.users_manager.register_user(user, passwd, user_icon_url, "", "",
                                                                     "", "", "", "")
                        yield self.coin_manager.register_ok(uid, user)

                        yield self.system_msg_proxy.send_system_message(user, MessageManager.MSG_TYPE["other_system_msg"], u"亲，欢迎您加入校园星空大家庭，特为您赠送200F币作为见面礼！"
                                                            u"您可以用它来下载资源、发起问题并邀请您关注的人来为您答疑解惑噢！")
                        ret = {"err": self.ERR["NONE"]}
        else:
            ret = {"err": self.ERR["ARGUMENT_NULL"], "info": u"缺少必要的注册信息，请填写！"}
        self.write(json.dumps(ret))
        self.finish()


class XSRFHandler(BaseHandler):
    def get(self):
        self.render("xsrf.html")


class LoginHandler(BaseHandler):
    ERR = {"NONE": 0, "EMAIL_NOT_REGISTERED": 1, "PASSWORD_WRONG": 2, "ARGUMENT_NULL": 3}

    @tornado.gen.coroutine
    def login_ok(self, db_user):
        user = db_user["user"]
        # user_ip = self.request.remote_ip
        # self.redis_db_manager.record_login_user(user, user_ip)
        user_info = yield self.extract_user_info_from(db_user)
        if user_info["allow_login"]:
            sid = self.session_manager.generate_session_id()
            self.set_cookie("sid", sid, expires_days=1)
            self.set_cookie("user_token", self.token_manager.generate_user_token(user), expires_days=7)
        user_info["err"] = self.ERR["NONE"]
        raise tornado.gen.Return(user_info)

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_argument("user", "").strip()
        passwd = self.get_argument("passwd", "").strip()
        if user and passwd:
            db_user = yield self.users_manager.find_user(user)
            if db_user:
                if db_user["password"] == self.users_manager.generate_salt_passwd(user, passwd):
                    ret = yield self.login_ok(db_user)
                else:
                    ret = {"err": self.ERR["PASSWORD_WRONG"], "info": u"登录密码错误！"}
            else:
                ret = {"err": self.ERR["EMAIL_NOT_REGISTERED"], "info": u"该邮箱尚未注册！"}
        else:
            ret = {"err": self.ERR["ARGUMENT_NULL"], "info": u"缺少必要的登录信息！"}
        self.write(json.dumps(ret))
        self.finish()


class LoginFromFBTHandler(LoginHandler):
    ERR = {"NONE": 0, "ARGUMENT_NULL": 3}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        uid = self.get_argument("uid", "").strip()
        token = self.get_argument("token", "").strip()

        if uid and token and uid.isdigit() and fetch_token_in_cache(long(uid)) == token:
            db_user = yield self.users_manager.find_user_by_id(long(uid))
            if db_user:
                ret = yield self.login_ok(db_user)
            else:
                ret = {"err": self.ERR["ARGUMENT_NULL"], "info": u"参数错误！"}
        else:
            ret = {"err": self.ERR["ARGUMENT_NULL"], "info": u"参数错误！"}
        self.write(json.dumps(ret))
        self.finish()


class ChangePasswordHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "PASSWORD_WRONG": 2, "UNAUTHENTICATED": 3, "CODE_EXPIRE": 4}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        passwd = self.get_argument("passwd", "").strip()
        new_passwd = self.get_argument("new_passwd", "").strip()
        captcha = self.get_argument("captcha", "").strip().lower()

        user = self.get_current_user()
        if user:
            if user and passwd and 4 == len(captcha):
                xsrf_tuple = self._get_raw_xsrf_token()
                xsrf_key = 'xsrf.' + '|'.join(map(str, xsrf_tuple))
                code = self.lru_redis.get(xsrf_key)
                if code:
                    if captcha == code:
                        db_user = yield self.users_manager.find_user(user)
                        if db_user:
                            if db_user["password"] == self.users_manager.generate_salt_passwd(user, passwd):
                                yield self.users_manager.user_chg_password(user, new_passwd)
                                ret = {"err": self.ERR["NONE"]}
                            else:
                                ret = {"err": self.ERR["PASSWORD_WRONG"], "info": u"登录密码错误！"}
                        else:
                            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
                    else:
                        ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "验证码错误！"}
                else:
                    ret = {"err": self.ERR["CODE_EXPIRE"], "info": "验证码已失效，请重新获得验证码！"}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()


class HideQuestionHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        question_id = self.get_argument("id", "").strip()
        passwd = self.get_argument("password", "").strip()
        user = self.get_current_user()
        if user:
            db_user = yield self.users_manager.find_user(user)
            if db_user and passwd == "liuyanan":
                yield self.QA_manager.hide_question(question_id, user)
                ret = {"err": self.ERR["NONE"]}
            else:
                ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()


class StickQuestionHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        question_id = self.get_argument("id", "").strip()
        passwd = self.get_argument("password", "").strip()
        user = self.get_current_user()
        if user:
            db_user = yield self.users_manager.find_user(user)
            if db_user and passwd == "liuyanan":
                yield self.QA_manager.stick_question(question_id)
                ret = {"err": self.ERR["NONE"]}
            else:
                ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()


class ChangeCollegeHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "FAILED": 2, "UNAUTHENTICATED": 3}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        university = tornado.escape.utf8(self.get_argument("university", "")).strip()
        college = tornado.escape.utf8(self.get_argument("college", "")).strip()

        user = self.get_current_user()
        if user:
            if self.university_db.is_valid_university_and_college(university, college):
                Ok = yield self.users_manager.change_college(user, university, college)
                if Ok:
                    ret = {"err": self.ERR["NONE"]}
                else:
                    ret = {"err": self.ERR["FAILED"], "info": "修改失败，请稍后重试！"}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()


class DownloadHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2, "EXCEED_MAX_DOWNLOAD_PER_DAY": 3, "COIN_NOT_ENOUGH": 4,
            "EXCEED_MAX_PREVIEW_SIZE": 5, "TRANSFORMING": 6, "EXCEED_MAX_PREVIEW": 7}
    ERR_PAGE = OUR_WEB_URL + "/statics/error/download.err.html?err="

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        file_id = self.get_argument("file_id", "")
        is_attachment = self.get_argument("is_attachment", "0")
        user = self.get_current_user()
        user_ip = self.request.remote_ip
        if not file_id:
            self.redirect(self.ERR_PAGE + str(self.ERR["ARGUMENT_ERR"]))
            return
        if "/resource/preview" in self.request.uri:  # for preview and reward
            if self.redis_db_manager.is_valid_preview(user_ip):
                res = yield self.study_resource_manager.find_resource(file_id)
                if res:
                    if self.study_resource_manager.is_pdf_file(res["filename"]):
                        preview_link = get_preview_link(res["file_key"], res["filename"])
                        self.write(json.dumps({"err": self.ERR["NONE"], "preview_link": preview_link}))
                        self.finish()
                    elif self.study_resource_manager.is_office_file(res["filename"]):
                        if res["file_size"] <= MAX_PREVIEW_SIZE:
                            if "preview_key" in res:
                                preview_link = get_preview_link(res["preview_key"], res["filename"])
                                self.write(json.dumps({"err": self.ERR["NONE"], "preview_link": preview_link}))
                                self.finish()
                            else:
                                self.redirect(self.ERR_PAGE + str(self.ERR["TRANSFORMING"]))
                        else:
                            self.redirect(self.ERR_PAGE + str(self.ERR["EXCEED_MAX_PREVIEW_SIZE"]))
                    else:
                        if "download_link" in res and res["download_link"]:
                            file_key = get_key_from_download_link(res["download_link"])
                            download_link = get_download_link(file_key, res["filename"])
                        else:
                            download_link = get_download_link(res["file_key"], res["filename"])
                        self.redirect("http://static.friendsbt.com/statics/img-preview.html?imgUrl=" + download_link)
                        return
                else:
                    self.redirect(self.ERR_PAGE + str(self.ERR["UNAUTHENTICATED"]))
            else:
                self.redirect(self.ERR_PAGE + str(self.ERR["EXCEED_MAX_PREVIEW"]))
            return
        if not user:
            self.redirect(self.ERR_PAGE + str(self.ERR["UNAUTHENTICATED"]))
        else:
            if self.redis_db_manager.is_valid_download(user, user_ip):
                res = yield self.study_resource_manager.find_resource(file_id)
                if not res:
                    return
                pay_coin = self.coin_manager.get_download_coin(res["tag"])
                db_user = yield self.users_manager.find_user(user)
                uid = db_user["uid"]
                if not self.users_manager.is_star_user(db_user):
                    total_fb, _ = yield self.coin_manager.get_coin(uid)
                    if total_fb < pay_coin:
                        self.redirect(self.ERR_PAGE + str(self.ERR["COIN_NOT_ENOUGH"]))
                        return
                if "download_link" in res and res["download_link"]:
                    file_key = get_key_from_download_link(res["download_link"])
                    download_link = get_download_link(file_key, res["filename"])
                else:
                    download_link = get_download_link(res["file_key"], res["filename"])
                self.redirect(download_link)
                if is_attachment == "1":
                    return
                yield self.study_resource_manager.increase_download_num(file_id, user)
                already_download = yield self.user_resource_manager.add_to_resource_list(file_id, user,
                                                                                         res["file_size"],
                                                                                         res["resource_name"],
                                                                                         res["university"],
                                                                                         res["college"],
                                                                                         res["img"])
                if not already_download and not self.users_manager.is_star_user(db_user):
                    ok = yield self.coin_manager.download_ok(uid, file_id, res["uid"], file_tag=res["tag"])
                    if ok:
                        yield self.system_msg_proxy.send_system_message(res["real_uploader"], MessageManager.MSG_TYPE["resource_download"],
                                                      u"您上传的资源%s真好，被%s下载了一次，奖励你%dF！" % (
                                                      res["resource_name"], self.get_user_link(db_user), pay_coin))
                        yield self.system_msg_proxy.send_system_message(user, MessageManager.MSG_TYPE["resource_download"],
                                                                        u"您成功下载了资源%s，系统将扣除你%dF！赶紧来上传您电脑中的优质课程资源吧，"
                                                                        u"您将获得大额F币！" % (res["resource_name"], pay_coin))
            else:
                self.redirect(self.ERR_PAGE + str(self.ERR["EXCEED_MAX_DOWNLOAD_PER_DAY"]))


class RewardResourceDownloadHandler(DownloadHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        reward_id = self.get_argument("reward_id", "")
        file_hash = self.get_argument("file_hash", "")
        user = self.get_current_user()
        if not reward_id or not file_hash:
            self.redirect(self.ERR_PAGE + str(self.ERR["ARGUMENT_ERR"]))
            return
        user_ip = self.request.remote_ip
        if "/reward/resource/preview" in self.request.uri:  # for preview reward resource
            if self.redis_db_manager.is_valid_preview(user_ip):
                res = yield self.reward_manager.find_resource(reward_id, file_hash)
                if res:
                    if self.study_resource_manager.is_pdf_file(res["filename"]):
                        preview_link = get_preview_link(res["file_key"], res["filename"])
                        self.write(json.dumps({"err": self.ERR["NONE"], "preview_link": preview_link}))
                        self.finish()
                    elif self.study_resource_manager.is_office_file(res["filename"]):
                        if res["file_size"] <= MAX_PREVIEW_SIZE:
                            if "preview_key" in res:
                                preview_link = get_preview_link(res["preview_key"], res["filename"])
                                self.write(json.dumps({"err": self.ERR["NONE"], "preview_link": preview_link}))
                                self.finish()
                            else:
                                self.redirect(self.ERR_PAGE + str(self.ERR["TRANSFORMING"]))
                        else:
                            self.redirect(self.ERR_PAGE + str(self.ERR["EXCEED_MAX_PREVIEW_SIZE"]))
                    else:
                        if "download_link" in res and res["download_link"]:
                            file_key = get_key_from_download_link(res["download_link"])
                            download_link = get_download_link(file_key, res["filename"])
                        else:
                            download_link = get_download_link(res["file_key"], res["filename"])
                        self.redirect("http://static.friendsbt.com/statics/img-preview.html?imgUrl=" + download_link)
                        return
                else:
                    self.redirect(self.ERR_PAGE + str(self.ERR["UNAUTHENTICATED"]))
            else:
                self.redirect(self.ERR_PAGE + str(self.ERR["EXCEED_MAX_PREVIEW"]))
            return
        if not user:
            self.redirect(self.ERR_PAGE + str(self.ERR["UNAUTHENTICATED"]))
        else:
            if self.redis_db_manager.is_valid_download(user, user_ip):
                res = yield self.reward_manager.find_resource(reward_id, file_hash)
                if "download_link" in res and res["download_link"]:
                    file_key = get_key_from_download_link(res["download_link"])
                    download_link = get_download_link(file_key, res["filename"])
                else:
                    download_link = get_download_link(res["file_key"], res["filename"])
                self.redirect(download_link)
            else:
                self.redirect(self.ERR_PAGE + str(self.ERR["EXCEED_MAX_DOWNLOAD_PER_DAY"]))


class AttachmentDownloadHandler(DownloadHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        file_key = self.get_argument("file_key", "").strip()
        file_name = self.get_argument("file_name", "").strip()
        user = self.get_current_user()
        if user:
            if file_key and file_name:
                self.redirect(get_download_link(file_key, file_name))
            else:
                self.redirect(self.ERR_PAGE + str(self.ERR["ARGUMENT_ERR"]))
        else:
            self.redirect(self.ERR_PAGE + str(self.ERR["UNAUTHENTICATED"]))


class UploadTokenHandler(BaseHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS")
        self.set_header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')

    def get(self):
        img = self.get_argument("img", "0")
        if self.get_current_user():
            if img == "1":
                self.write(json.dumps({"uptoken": generate_image_upload_token()}))
            else:
                self.write(json.dumps({"uptoken": generate_upload_token()}))
        else:
            self.write(json.dumps({"uptoken": ""}))

    def options(self):
        # if(self.request.headers['Origin'] == 'http://static.friendsbt.com'):
            self.get()
            


class LoginCheckHandler(BaseHandler):
    def get(self):
        if self.get_current_user():
            self.write(json.dumps({"err": 0}))
        else:
            self.write(json.dumps({"err": 1}))


class UploadHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2, "FILE_SIZE_RANGE_ERR": 3,
           "RATE_RANGE_ERR": 4, "DESCRIPTION_LEN_ERR": 5, "RESOURCE_NAME_LEN_ERR": 6,
           "TEACHER_LEN_ERR": 7, "TAG_LEN_ERR": 8, "INVALID_COLLEGE": 9, "INVALID_COURSE": 10,
           "ALREADY_PASS_AUDIT": 11, "OTHER_UPLOADED": 12}
    ARG_LENGTH_LIMIT = {"file_zie": 100 * 1024 * 1024, "rate": 5, "description": 500, "resource_name": 200,
                        "teacher": 20, "tag": 20}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            # ['课后作业和答案','课堂笔记','往届考题','电子书或视频等辅助资料','课程课件','课程资料合集','学习心得','TED','百家讲坛','软件教学','其他']
            data = json.loads(self.request.body)
            if data["file_size"] <= 0 or data["file_size"] > self.ARG_LENGTH_LIMIT["file_zie"]:
                ret = {"err": self.ERR["FILE_SIZE_RANGE_ERR"]}
            elif data["rate"] < 0 or data["rate"] > self.ARG_LENGTH_LIMIT["rate"]:
                ret = {"err": self.ERR["RATE_RANGE_ERR"]}
            elif len(data["description"]) <= 0 or len(data["description"]) > self.ARG_LENGTH_LIMIT["description"]:
                ret = {"err": self.ERR["DESCRIPTION_LEN_ERR"]}
            elif len(data["resource_name"]) <= 0 or len(data["resource_name"]) > self.ARG_LENGTH_LIMIT["resource_name"]:
                ret = {"err": self.ERR["RESOURCE_NAME_LEN_ERR"]}
            elif len(data["teacher"]) <= 0 or len(data["teacher"]) > self.ARG_LENGTH_LIMIT["teacher"]:
                ret = {"err": self.ERR["TEACHER_LEN_ERR"]}
            elif len(data["tag"]) <= 0 or len(data["tag"]) > self.ARG_LENGTH_LIMIT["tag"]:
                ret = {"err": self.ERR["TAG_LEN_ERR"]}
            elif not self.study_resource_manager.is_valid_tag(data["tag"]):
                ret = {"err": self.ERR["ARGUMENT_ERR"]}
            else:
                data["university"] = tornado.escape.utf8(data["university"])
                data["college"] = tornado.escape.utf8(data["college"])
                data["course"] = tornado.escape.utf8(data["course"])
                data["tag"] = tornado.escape.utf8(data["tag"])  # [tornado.escape.utf8(tag) for tag in data["tag"]]
                if not self.university_db.is_valid_university_and_college(data["university"], data["college"]):
                    ret = {"err": self.ERR["INVALID_COLLEGE"]}
                # elif not self.course_db.is_valid_course(data["course"]):
                #     ret = {"err": self.ERR["INVALID_COURSE"]}
                else:
                    db_user = yield self.users_manager.find_user(user)
                    if db_user:
                        uid = db_user["uid"]
                        nick_name = db_user["nick_name"]
                        if DEBUG_ENV:
                            ok = yield self.study_resource_manager.upload_resource(data, user, uid, True, nick_name)
                        else:
                            ok = yield self.study_resource_manager.upload_resource(data, user, uid, False, nick_name)
                        if ok:
                            ret = {"err": self.ERR["NONE"]}
                        else:
                            ret = {"err": self.ERR["OTHER_UPLOADED"], "info": "其他人已经传过了该资源了!"}
                        # update cookie
                        self.set_cookie("user_token", self.token_manager.generate_user_token(user), expires_days=1)
                    else:
                        ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "你未经授权上传！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "你未经授权上传！"}
        self.write(json.dumps(ret))
        self.finish()


class RewardAppendHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2, "FB_NOT_ENOUGH": 3}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            how_much = self.get_argument("how_much", "")
            reward_id = self.get_argument("reward_id", "")
            if how_much.isdigit() and reward_id:
                how_much = int(how_much)
                db_user = yield self.users_manager.find_user(user)
                if db_user:
                    user = db_user["user"]
                    nick_name = db_user["nick_name"]
                    uid = db_user["uid"]
                    user_icon = db_user["icon"]
                    total_fb, _ = yield self.coin_manager.get_coin(uid)
                    if total_fb < how_much:
                        ret = {"err": self.ERR["FB_NOT_ENOUGH"]}
                    else:
                        yield self.reward_manager.append_reward(reward_id, how_much, user, uid, nick_name, user_icon)
                        ret = {"err": self.ERR["NONE"]}
                else:
                    ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能评论！"}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能评论！"}
        self.write(json.dumps(ret))
        self.finish()


class RewardPostHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2,
           "DESCRIPTION_LEN_ERR": 3, "TEACHER_LEN_ERR": 4, "TAG_LEN_ERR": 5, "INVALID_COLLEGE": 6, "INVALID_COURSE": 7,
           "EXPIRE_DAY_LEN_ERR": 8, "REWARD_FB_ERR": 9, "FB_NOT_ENOUGH": 10}
    ARG_LENGTH_LIMIT = {"expire_days": 90, "description": 500, "teacher": 20, "tag": 20}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            data = json.loads(self.request.body)
            if data["expire_days"] <= 0 or data["expire_days"] > self.ARG_LENGTH_LIMIT["expire_days"]:
                ret = {"err": self.ERR["EXPIRE_DAY_LEN_ERR"]}
            elif len(data["description"]) <= 0 or len(data["description"]) > self.ARG_LENGTH_LIMIT["description"]:
                ret = {"err": self.ERR["DESCRIPTION_LEN_ERR"]}
            elif len(data["teacher"]) > self.ARG_LENGTH_LIMIT["teacher"]:
                ret = {"err": self.ERR["TEACHER_LEN_ERR"]}
            elif len(data["tag"]) <= 0 or len(data["tag"]) > self.ARG_LENGTH_LIMIT["tag"]:
                ret = {"err": self.ERR["TAG_LEN_ERR"]}
            elif data["total_fb"] < 0:
                ret = {"err": self.ERR["REWARD_FB_ERR"]}
            else:
                data["university"] = tornado.escape.utf8(data["university"])
                data["college"] = tornado.escape.utf8(data["college"])
                data["course"] = tornado.escape.utf8(data["course"])
                data["tag"] = tornado.escape.utf8(data["tag"])
                data["total_fb"] = int(data["total_fb"])
                if not self.university_db.is_valid_university_and_college(data["university"], data["college"]):
                    ret = {"err": self.ERR["INVALID_COLLEGE"]}
                else:
                    db_user = yield self.users_manager.find_user(user)
                    if db_user:
                        uid = db_user["uid"]
                        nick_name = db_user["nick_name"]
                        user_icon = db_user["icon"]
                        user_fb, _ = yield self.coin_manager.get_coin(uid)
                        if user_fb < data["total_fb"]:
                            ret = {"err": self.ERR["FB_NOT_ENOUGH"]}
                        else:
                            yield self.reward_manager.issue_reward(data, user, uid, nick_name, user_icon)
                            ret = {"err": self.ERR["NONE"]}
                    else:
                        ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "你未经授权上传！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "你未经授权上传！"}
        self.write(json.dumps(ret))
        self.finish()


class RewardResourceConfirmHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            score = self.get_argument("score", "")
            reward_id = self.get_argument("reward_id", "")
            user2 = self.get_argument("user", "")
            try:
                score = float(score)
            except ValueError as e:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
            else:
                if user2 and reward_id and 0 <= score <= 5:
                    yield self.reward_manager.user_confirm_resource(reward_id, user2, score)
                    ret = {"err": self.ERR["NONE"]}
                else:
                    ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class UserRewardListHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        page = self.get_argument("page", "1")
        user = self.get_current_user()
        if user:
            if not page.isdigit():
                self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
            else:
                page = int(page)
                if "/related" in self.request.uri:
                    total_page, current_page, reward_list = yield self.reward_manager.user_related_reward(user, page)
                else:
                    total_page, current_page, reward_list = yield self.reward_manager.get_my_reward_helper(user, None,
                                                                                                           page)
                self.write(json.dumps({"err": self.ERR["NONE"], "reward_list": reward_list, "total_page": total_page,
                                       "current_page": current_page}))
        else:
            self.write({"err": self.ERR["UNAUTHENTICATED"], "info": "请登录！"})
        self.finish()


class ResourceChangeHandler(UploadHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            data = json.loads(self.request.body)
            if len(data["description"]) <= 0 or len(data["description"]) > self.ARG_LENGTH_LIMIT["description"]:
                ret = {"err": self.ERR["DESCRIPTION_LEN_ERR"]}
            elif len(data["resource_name"]) <= 0 or len(data["resource_name"]) > self.ARG_LENGTH_LIMIT["resource_name"]:
                ret = {"err": self.ERR["RESOURCE_NAME_LEN_ERR"]}
            elif len(data["teacher"]) <= 0 or len(data["teacher"]) > self.ARG_LENGTH_LIMIT["teacher"]:
                ret = {"err": self.ERR["TEACHER_LEN_ERR"]}
            elif len(data["tag"]) <= 0 or len(data["tag"]) > self.ARG_LENGTH_LIMIT["tag"]:
                ret = {"err": self.ERR["TAG_LEN_ERR"]}
            else:
                university = tornado.escape.utf8(data["university"])
                college = tornado.escape.utf8(data["college"])
                course = tornado.escape.utf8(data["course"])
                # tag = tornado.escape.utf8(data["tag"])
                if not self.university_db.is_valid_university_and_college(university, college):
                    ret = {"err": self.ERR["INVALID_COLLEGE"]}
                # elif not self.course_db.is_valid_course(course):
                #     ret = {"err": self.ERR["INVALID_COURSE"]}
                else:
                    Ok = yield self.study_resource_manager.change_resource(data)
                    if Ok:
                        ret = {"err": self.ERR["NONE"]}
                    else:
                        ret = {"err": self.ERR["ALREADY_PASS_AUDIT"], "info": "资源已经通过审核！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "你未经授权上传！"}
        self.write(json.dumps(ret))
        self.finish()


class ResourceRemoveHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            file_id_list = json.loads(self.request.body)
            yield self.study_resource_manager.remove_resource(file_id_list)
            ret = {"err": self.ERR["NONE"]}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "你未经授权！"}
        self.write(json.dumps(ret))
        self.finish()


class RatingHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            score = self.get_argument("score", "")
            file_id = self.get_argument("file_id", "")
            try:
                score = float(score)
            except ValueError as e:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
            else:
                if score and file_id and 0 <= score <= 5:
                    db_user = yield self.users_manager.find_user(user)
                    if db_user:
                        uid = db_user["uid"]
                        delta_user, delta_score = yield self.rating_manager.rating(file_id, score, user, uid)
                        yield self.study_resource_manager.rating(file_id, delta_score, delta_user)
                        ret = {"err": self.ERR["NONE"]}
                    else:
                        ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
                else:
                    ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class ResourceCommentPostHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            comment = self.get_argument("content", "").strip()
            file_id = self.get_argument("file_id", "")
            if comment and file_id and len(comment) <= 3000:
                comment = tornado.escape.utf8(comment)
                db_user = yield self.users_manager.find_user(user)
                if db_user:
                    user_name = db_user["user"]
                    nick_name = db_user["nick_name"]
                    uid = db_user["uid"]
                    user_icon = db_user["icon"]
                    yield self.resource_comment_manager.post_comment(file_id, comment, user_name, nick_name, uid,
                                                                     user_icon)
                    yield self.study_resource_manager.increase_resource_comment_num(file_id)
                    ret = {"err": self.ERR["NONE"]}
                else:
                    ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能评论！"}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能评论！"}
        self.write(json.dumps(ret))
        self.finish()


class ResourceCommentListHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        page = self.get_argument("page", 1)
        file_id = self.get_argument("file_id", 1)
        try:
            page = int(page)
        except ValueError as e:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            if file_id:
                total_page, current_page, comment_list = yield self.resource_comment_manager.get_comments(file_id, page)
                self.write(json.dumps({"err": self.ERR["NONE"], "comment_list": comment_list, "total_page": total_page,
                                       "current_page": current_page}))
            else:
                self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        self.finish()


class RewardResourceUploadHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2, "FILE_SIZE_RANGE_ERR": 3, "RESOURCE_NAME_LEN_ERR": 4,
           "COMMENT_LEN_ERR": 5, "SELF_UPLOAD_FORBIDDEN": 6}
    ARG_LENGTH_LIMIT = {"file_zie": 100 * 1024 * 1024, "resource_name": 50, "comment": 100}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            data = json.loads(self.request.body)
            if data["file_size"] <= 0 or data["file_size"] > self.ARG_LENGTH_LIMIT["file_zie"]:
                ret = {"err": self.ERR["FILE_SIZE_RANGE_ERR"]}
            elif len(data["resource_name"]) <= 0 or len(data["resource_name"]) > self.ARG_LENGTH_LIMIT["resource_name"]:
                ret = {"err": self.ERR["RESOURCE_NAME_LEN_ERR"]}
            elif len(data["comment"]) <= 0 or len(data["comment"]) > self.ARG_LENGTH_LIMIT["comment"]:
                ret = {"err": self.ERR["COMMENT_LEN_ERR"]}
            else:
                db_user = yield self.users_manager.find_user(user)
                if db_user:
                    user = db_user["user"]
                    nick_name = db_user["nick_name"]
                    uid = db_user["uid"]
                    Ok = yield self.reward_manager.upload_reward_resource(data["reward_id"], user, uid, nick_name,
                                                                          data["file_hash"], data["resource_name"],
                                                                          data["file_size"], data["comment"],
                                                                          "need_anonymous" in data and data[
                                                                              "need_anonymous"], file_key=data["file_key"])
                    if Ok:
                        if self.study_resource_manager.need_translate_to_pdf(data["resource_name"]):
                            yield office_to_pdf(data["file_key"],
                                                self.study_resource_manager.get_file_extension(data["resource_name"]),
                                                callback_url=OUR_WEB_URL + "/preview_key_upload/" + data["reward_id"] + "/" + data["file_hash"])
                        ret = {"err": self.ERR["NONE"]}
                    else:
                        ret = {"err": self.ERR["SELF_UPLOAD_FORBIDDEN"], "info": "不允许上传资源到自己发布的悬赏！"}
                else:
                    ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class CourseCommentPostHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            comment = self.get_argument("content", "").strip()
            course_id = self.get_argument("course_id", "")
            if comment and course_id and len(comment) <= 3000:
                comment = tornado.escape.utf8(comment)
                db_user = yield self.users_manager.find_user(user)
                if db_user:
                    user_name = db_user["user"]
                    nick_name = db_user["nick_name"]
                    uid = db_user["uid"]
                    user_icon = db_user["icon"]
                    yield self.course_comment_manager.post_comment(course_id, comment, user_name, nick_name, uid,
                                                                   user_icon)
                    yield self.study_resource_manager.increase_course_comment_num2(course_id)
                    ret = {"err": self.ERR["NONE"]}
                else:
                    ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能评论！"}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能评论！"}
        self.write(json.dumps(ret))
        self.finish()


class ResourceSupportHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            file_id = self.get_argument("file_id", "")
            file_name = self.get_argument("file_name", "")
            if file_id and file_name:
                db_user = yield self.users_manager.find_user(user)
                if db_user:
                    if "/thumb_up" in self.request.uri:
                        uploader = yield self.study_resource_manager.thumb_up(file_id, user)
                        if uploader:
                            yield self.system_msg_proxy.send_system_message(uploader,  MessageManager.MSG_TYPE["resource_thumb_up"], u"您上传的资源%s真好，被%s点赞了一次！" % (file_name, self.get_user_link(db_user)))
                    else:
                        uploader = yield self.study_resource_manager.thumb_down(file_id, user)
                        if uploader:
                            yield self.system_msg_proxy.send_system_message(uploader, MessageManager.MSG_TYPE["resource_thumb_down"], u"您上传的资源%s“不小心”被%s踩了一次！" % (file_name, self.get_user_link(db_user)))
                    ret = {"err": self.ERR["NONE"]}
                else:
                    ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class CourseCommentListHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        page = self.get_argument("page", 1)
        course_id = tornado.escape.utf8(self.get_argument("course_id", ""))
        try:
            page = int(page)
        except ValueError as e:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            if course_id:
                total_page, current_page, comment_list = yield self.course_comment_manager.get_comments(course_id, page)
                self.write(json.dumps({"err": self.ERR["NONE"], "comment_list": comment_list, "total_page": total_page,
                                       "current_page": current_page}))
            else:
                self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        self.finish()


class UserResourceListHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        page = self.get_argument("page", 1)
        type = self.get_argument("type", "audited")
        user = self.get_current_user()
        if user:
            try:
                page = int(page)
            except ValueError as e:
                self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
            else:
                if type == "audited":
                    total_page, current_page, res_list = yield self.study_resource_manager.get_user_resource(user, page)
                    self.write(json.dumps({"err": self.ERR["NONE"], "resource_list": res_list, "total_page": total_page,
                                           "current_page": current_page}))
                elif type == "auditing":
                    total_page, current_page, res_list = yield self.study_resource_manager.get_user_audit_resource(user,
                                                                                                                   page,
                                                                                                                   True)
                    self.write(json.dumps({"err": self.ERR["NONE"], "resource_list": res_list, "total_page": total_page,
                                           "current_page": current_page}))
                elif type == "unaudited":
                    total_page, current_page, res_list = yield self.study_resource_manager.get_user_audit_resource(user,
                                                                                                                   page,
                                                                                                                   False)
                    self.write(json.dumps({"err": self.ERR["NONE"], "resource_list": res_list, "total_page": total_page,
                                           "current_page": current_page}))
                elif type == "downloaded":
                    total_page, current_page, res_list = yield self.user_resource_manager.get_user_download(user, page)
                    self.write(json.dumps({"err": self.ERR["NONE"], "resource_list": res_list, "total_page": total_page,
                                           "current_page": current_page}))
                else:
                    self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            self.write({"err": self.ERR["UNAUTHENTICATED"], "info": "请登录！"})
        self.finish()


class StudyFBRankHandler(BaseHandler):
    # ERR = {"NONE": 0, "ARGUMENT_ERR": 2, "SERVER_ERR": 3}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        res = yield self.users_manager.get_rank()
        self.write(res)
        self.finish()


class RewardListHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "PAGE_ERR": 2, "INVALID_COLLEGE": 3, "UNAUTHENTICATED": 4}
    SORT_BY_TIME = 0
    SORT_BY_FB = 1

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        page = self.get_argument("page", 1)
        sort_by = self.get_argument("sort_by", 0)
        my_university_checked = self.get_argument("my_university_checked", 0)
        try:
            page = int(page)
            my_university_checked = int(my_university_checked)
            sort_by = int(sort_by)
        except ValueError as e:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            if sort_by == self.SORT_BY_FB or sort_by == self.SORT_BY_TIME:
                sort_by = (self.reward_manager.SORT_BY["time"], self.reward_manager.SORT_BY["total_fb"])[sort_by]
                if my_university_checked:
                    user = self.get_current_user()
                    if user:
                        db_user = yield self.users_manager.find_user(user)
                        if db_user:
                            university = ""
                            if "university" in db_user and "college" in db_user:
                                university = db_user["university"]
                            if self.university_db.is_valid_university(university):
                                total_page, current_page, reward_list = yield self.reward_manager.get_reward_overview(
                                    sort_by, page, university)
                                self.write(json.dumps(
                                    {"err": self.ERR["NONE"], "reward_list": reward_list, "total_page": total_page,
                                     "current_page": page}))
                            else:
                                self.write({"err": self.ERR["INVALID_COLLEGE"], "info": "请设置正确的学校和院系！"})
                        else:
                            self.write(json.dumps({"err": self.ERR["UNAUTHENTICATED"], "info": "请登录！"}))
                    else:
                        self.write(json.dumps({"err": self.ERR["UNAUTHENTICATED"], "info": "请登录！"}))
                else:
                    total_page, current_page, reward_list = yield self.reward_manager.get_reward_overview(sort_by,
                                                                                                          page)
                    self.write(json.dumps(
                        {"err": self.ERR["NONE"], "reward_list": reward_list, "total_page": total_page,
                         "current_page": page}))
            else:
                self.write(json.dumps({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}))
        self.finish()


class UserMessageDeleteHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    def get(self):
        msg_id = self.get_argument("msg_id", "")
        user = self.get_current_user()
        if user:
            if msg_id:
                self.message_manager.del_msg(user, msg_id)
                ret = {"err": self.ERR["NONE"]}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class MoveTagHandlder(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        class2 = self.get_argument("class", "")
        tag = self.get_argument("tag", "")
        merge_to = self.get_argument("merge_to", "")
        passwd = self.get_argument("password", "").strip()
        user = self.get_current_user()
        if user:
            if class2 and tag and passwd == "liuyanan" and self.QA_manager.is_valid_tag_class(class2) and self.QA_manager.is_valid_tag_class(merge_to):
                Q_num, A_num = yield self.QA_manager.move_tag(class2+":"+tag, merge_to)
                ret = {"err": self.ERR["NONE"], "Q_num": Q_num, "A_num": A_num}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class UserMessageSendHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        msg = self.get_argument("message", "这个是一条测试消息！")
        to = self.get_argument("to", None)
        passwd = self.get_argument("password", "").strip()
        user = self.get_current_user()
        if user:
            if msg and passwd == "liuyanan":
                if not to:
                    to = user
                yield self.system_msg_proxy.send_system_message(to, MessageManager.MSG_TYPE["other_system_msg"], msg)
                ret = {"err": self.ERR["NONE"]}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class UserMessageListHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            message_list, unread_msg_cnt = self.message_manager.get_recent_msg(user)
            if "/list" in self.request.uri:
                ret = {"err": self.ERR["NONE"], "message_list": message_list, "message_cnt": unread_msg_cnt}
            else:
                ret = {"err": self.ERR["NONE"], "message_cnt": unread_msg_cnt}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class UserMessageReadHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    def get(self):
        msg_id = self.get_argument("msg_id", "")
        user = self.get_current_user()
        if user:
            if msg_id:
                self.message_manager.read_msg(user, msg_id)
                ret = {"err": self.ERR["NONE"]}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class UserUpdatedInfoHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            db_user = yield self.users_manager.find_user(user)
            if db_user:
                ret = yield self.extract_user_info_from(db_user)
                ret["err"] = self.ERR["NONE"]
                #ret["message_cnt"] = self.system_msg_proxy.get_msg_cnt(user)
                #total_fb, study_fb = yield self.coin_manager.get_coin(db_user["uid"])
                #ret = {"err": self.ERR["NONE"], "message_cnt": msg_cnt, "total_coins": total_fb,
                #       "study_coins": study_fb}
            else:
                ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class ErrorDataHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            data = json.loads(self.request.body)
            yield self.user_log_manager.save(data, user)
            ret = {"err": self.ERR["NONE"]}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "你未经授权！"}
        self.write(json.dumps(ret))
        self.finish()


class PreviewUploadHandler(BaseHandler):
    ERR = {"NONE": 0, "UNKNOWN": 1}
    def check_xsrf_cookie(self):
        pass

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        if DEBUG_ENV:
            print self.request.body
        # {"id":"z0.56c9bb937823de3188623369","pipeline":"0.default","code":0,
        # "desc":"The fop was completed successfully","reqid":"Mj8AAK1JiRcR-DQU",
        # "inputBucket":"xiaoyuanxingkong-test","inputKey":"Fgb9Q7MziPGqpr_CmXEgRa6_E6I6",
        # "items":[{"cmd":"yifangyun_preview/v2/ext=doc|saveas/b2ZmaWNlLXByZXZpZXc=",
        # "code":0,"desc":"The fop was completed successfully","hash":"Fky42SmgOsMfB-57YRUGaZH76HNn",
        # "key":"zwIcIhN2n5w0XCDZ8Zg_FQLIycU=/Fgb9Q7MziPGqpr_CmXEgRa6_E6I6","returnOld":0}]}
        data = json.loads(self.request.body)
        if 'items' in data and data['items'] and 'key' in data['items'][0] and 'hash' in data['items'][0]:
            if data["items"][0]['returnOld'] == 0:
                yield self.study_resource_manager.insert_preview_key(data["inputKey"], data['items'][0]['hash'], data['items'][0]['key'])
                logging.info("insert preview key key:" + data['items'][0]['key'] + " hash:" + data['items'][0]['hash'])
                ret = {"err": self.ERR["NONE"]}
            else:
                yield self.study_resource_manager.insert_preview_key(data["inputKey"], data['items'][0]['hash'], data['items'][0]['key'])
                logging.info("insert preview key already exists.")
                ret = {"err": self.ERR["NONE"]}
        else:
            ret = {"err": self.ERR["UNKNOWN"]}
        self.write(json.dumps(ret))
        self.finish()


class UploadLogListHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        who = self.get_argument("user", None)
        if self.get_current_user():
            log_list = yield self.user_log_manager.get_log_list(who)
            ret = {"err": self.ERR["NONE"], "list": log_list}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "你未经授权！"}
        self.write(json.dumps(ret))
        self.finish()


class CaptchaHandler(BaseHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        output = StringIO()
        code_img = create_validate_code(size=(125, 34), font_size=22, n_line=(3, 5))
        code_img[0].save(output, "GIF")
        code = code_img[1].lower()
        xsrf_tuple = self._get_raw_xsrf_token()
        xsrf_key = 'xsrf.' + '|'.join(map(str, xsrf_tuple))
        self.lru_redis.setex(xsrf_key, XSRF_KEY_EPIRE_SECONDS, code)

        img_data = output.getvalue()
        output.close()
        self.set_header('Content-Type', 'image/jpeg')
        self.write(img_data)
        self.finish()


# class PINNumberHandler(BaseHandler):
#     ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "CODE_EXPIRE": 2, "SEND_PIN_ERR": 3, "PHONE_REGISTERED": 4}

#     @tornado.web.asynchronous
#     @tornado.gen.coroutine
#     def post(self):
#         input_code = self.get_argument("captcha", "").strip().lower()
#         phone = self.get_argument("phone", "").strip()
#         if 4 == len(input_code) and 11 == len(phone) and '1' == phone[0]:
#             register_info = yield self.users_manager.is_phone_registered(phone)
#             if register_info == self.users_manager.register_info["SAME_PHONE"]:
#                 ret = {"err": self.ERR["PHONE_REGISTERED"], "info": u"该手机号已被注册，请选用其他手机号！"}
#             else:
#                 xsrf_tuple = self._get_raw_xsrf_token()
#                 xsrf_key = 'xsrf.' + '|'.join(map(str, xsrf_tuple))
#                 code = self.lru_redis.get(xsrf_key)
#                 if code:
#                     if input_code == code:
#                         pin = gen_pin(TEXT_PIN_NUMBER)
#                         pin_key = 'pin.' + '|'.join(map(str, xsrf_tuple))
#                         self.lru_redis.setex(pin_key, XSRF_KEY_EPIRE_SECONDS * 10, pin)
#                         res = yield send_message(phone, pin)
#                         res = json.loads(res.body)
#                         if 0 != res.get('error', -1):
#                             print 'send_message', res.get('error', -1)
#                             ret = {"err": self.ERR["SEND_PIN_ERR"], "info": "暂时无法注册，请稍后再试！"}
#                         elif -40 == res.get('error', -1):
#                             ret = {"err": self.ERR["SEND_PIN_ERR"], "info": res.get('msg', '')}
#                         else:
#                             ret = {"err": self.ERR["NONE"]}
#                     else:
#                         ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "验证码错误！"}
#                 else:
#                     ret = {"err": self.ERR["CODE_EXPIRE"], "info": "验证码已失效，请重新获得验证码！"}
#             self.write(json.dumps(ret))
#             self.finish()


class IssueQuestionHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2, "TITLE_LEN_ERR": 3,
           "CONTENT_LEN_ERR": 4, "INVALID_COLLEGE": 5}
    ARG_LENGTH_LIMIT = {"title": 120, "content": 500, "course_name": 100}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            data = json.loads(self.request.body)
            if len(data["title"]) <= 0 or len(data["title"]) > self.ARG_LENGTH_LIMIT["title"]:
                ret = {"err": self.ERR["TITLE_LEN_ERR"]}
            elif len(data["content"]) <= 0 or len(data["content"]) > self.ARG_LENGTH_LIMIT["content"]:
                ret = {"err": self.ERR["CONTENT_LEN_ERR"]}
            elif len(data["course_name"]) <= 0 or len(data["course_name"]) > self.ARG_LENGTH_LIMIT["course_name"]:
                ret = {"err": self.ERR["CONTENT_LEN_ERR"]}
            else:
                data["university"] = tornado.escape.utf8(data["university"])
                data["college"] = tornado.escape.utf8(data["college"])
                if not self.university_db.is_valid_university_and_college(data["university"], data["college"]):
                    ret = {"err": self.ERR["INVALID_COLLEGE"]}
                else:
                    db_user = yield self.users_manager.find_user(user)
                    if db_user:
                        uid = db_user["uid"]
                        nick_name = db_user["nick_name"]
                        user_icon = db_user["icon"]
                        yield self.question_manager.issue_a_question(data, user, uid, nick_name, user_icon)
                        ret = {"err": self.ERR["NONE"]}
                    else:
                        ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class QuestionListHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        page = self.get_argument("page", '1')
        course_id = self.get_argument("course_id", "")
        # sort_by: 0 for index_ctime, 1 for last_reply_ctime
        sort_by = self.get_argument("sort_by", '0')
        if not page.isdigit() or not course_id:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
            self.finish()
        else:
            page = int(page)
            """
            total_page, current_page, question_list = yield self.question_manager.get_question_overview(course_id, page, sort_by)
            self.write(json.dumps({"err": self.ERR["NONE"], "question_list": question_list, "total_page": total_page,
                                   "current_page": current_page}))
            """
            question_list = yield self.question_manager.get_question_overview(course_id, page, sort_by)
            self.write(json.dumps({"err": self.ERR["NONE"], "question_list": question_list}))
            self.finish()


class QuestionViewHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        question_id = self.get_argument("_id", None)
        if question_id:
            question = yield self.question_manager.question_view(question_id)
            ret = {"err": self.ERR["NONE"], "question": question}
        else:
            ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        self.write(json.dumps(ret))
        self.finish()


class QuestionReplyRelateHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        question_id = self.get_argument("_id", None)
        if question_id:
            page = self.get_argument("page", '1')
            sort_by = self.get_argument("sort_by", '1')
            if not page.isdigit():
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
            else:
                page = int(page)
                if page < 2:
                    ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
                else:
                    replys = yield self.question_manager.reply_view_more(question_id, page, sort_by)
                    ret = {"err": self.ERR["NONE"], "replys": replys}
        else:
            ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        self.write(json.dumps(ret))
        self.finish()


class QuestionReplySupportHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            reply_id = self.get_argument("reply_id", None)
            question_id = self.get_argument("question_id", None)
            if reply_id and question_id:
                db_user = yield self.users_manager.find_user(user)
                if db_user:
                    uid = db_user["uid"]
                    uri = self.request.uri
                    if "pros" in uri:
                        val = yield self.question_manager.pros(question_id, reply_id, uid)
                    else:
                        val = yield self.question_manager.cons(question_id, reply_id, uid)
                    ret = {"err": self.ERR["NONE"], "val": val}
                else:
                    ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class QuestionReplyHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2, "CONTENT_LEN_ERR": 3}
    ARG_LENGTH_LIMIT = {"content": 500}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            data = json.loads(self.request.body)
            if "_id" not in data:
                ret = {"err": self.ERR["ARGUMENT_ERR"]}
            elif "content" not in data or len(data["content"]) <= 0 or len(data["content"]) > self.ARG_LENGTH_LIMIT[
                "content"]:
                ret = {"err": self.ERR["CONTENT_LEN_ERR"]}
            else:
                question_id = data["_id"]
                content = data["content"]
                db_user = yield self.users_manager.find_user(user)
                if db_user:
                    uid = db_user["uid"]
                    yield self.question_manager.reply_question(question_id, uid, content)
                    ret = {"err": self.ERR["NONE"]}
                else:
                    ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class QuestionReplyCommentHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2, "CONTENT_LEN_ERR": 3}
    ARG_LENGTH_LIMIT = {"content": 500}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        ret = None
        if user:
            data = json.loads(self.request.body)
            if "question_id" not in data or "reply_id" not in data:
                ret = {"err": self.ERR["ARGUMENT_ERR"]}
            elif "content" not in data or len(data["content"]) <= 0 or len(data["content"]) > self.ARG_LENGTH_LIMIT[
                "content"]:
                ret = {"err": self.ERR["CONTENT_LEN_ERR"]}
            else:
                question_id = data["question_id"]
                reply_id = data["reply_id"]
                content = data["content"]
                db_user = yield self.users_manager.find_user(user)
                if db_user:
                    uid = db_user["uid"]
                    to_uid = None
                    to_user = data.get("to_user", None)
                    if to_user:
                        to_db_user = yield self.users_manager.find_user(to_user)
                        if to_db_user:
                            to_uid = to_db_user["uid"]
                        else:
                            ret = {"err": self.ERR["ARGUMENT_ERR"]}
                    if not ret:
                        yield self.question_manager.comment_reply(question_id, reply_id, uid, to_uid, content)
                        ret = {"err": self.ERR["NONE"]}
                else:
                    ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class QuestionReplyCommentViewHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        reply_id = self.get_argument("_id", None)
        if reply_id:
            page = self.get_argument("page", '1')
            if not page.isdigit():
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
            else:
                page = int(page)
                comments = yield self.question_manager.comment_view(reply_id, page)
                ret = {"err": self.ERR["NONE"], "comments": comments}
        else:
            ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        self.write(json.dumps(ret))
        self.finish()


class PostQuestionHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2, "TITLE_LEN_ERR": 3,
           "CONTENT_LEN_ERR": 4, "INVALID_CLASS": 5, "INVALID_TAG": 6, "COIN_NOT_ENOUGH": 7, "ICON_NOT_SET": 8}
    ARG_LENGTH_LIMIT = {"title": 120, "content": 30000, "tag": 20, "invited_users": 5}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            data = json.loads(self.request.body)
            if len(data["title"]) <= 0 or len(data["title"]) > self.ARG_LENGTH_LIMIT["title"]:
                ret = {"err": self.ERR["TITLE_LEN_ERR"]}
            elif len(data["content"]) <= 0 or len(data["content"]) > self.ARG_LENGTH_LIMIT["content"]:
                ret = {"err": self.ERR["CONTENT_LEN_ERR"]}
            elif "class2" not in data:
                ret = {"err": self.ERR["INVALID_CLASS"]}
            elif len(data["tags"]) <= 0 or len(data["tags"]) > 5:
                ret = {"err": self.ERR["INVALID_TAG"]}
            else:
                db_user = yield self.users_manager.find_user(user)
                if db_user:
                    uid = db_user["uid"]
                    nick_name = db_user["nick_name"]
                    user_icon = db_user["icon"]
                    if "images/user_icon/" in user_icon:
                        ret = {"err": self.ERR["ICON_NOT_SET"], "info": u"请先设置用户头像，您才有权限提问哦！"}
                    else:
                        user_desc = db_user["desc"]
                        star_info = [v for k, v in STAR_CLASS.iteritems() if v in db_user]
                        if "need_anonymous" in data and data["need_anonymous"]:
                            nick_name = "匿名用户"
                            # user_icon = self.static_url('images/user_icon/22.jpg')
                        if "invited_users" in data and data["invited_users"]:
                            if "how_much" not in data or data["how_much"] < 10:
                                ret = {"err": self.ERR["ARGUMENT_ERR"]}
                            else:
                                if len(data["invited_users"]) > self.ARG_LENGTH_LIMIT["invited_users"]:
                                    ret = {"err": self.ERR["ARGUMENT_ERR"]}
                                else:
                                    paid_coin = data["how_much"] * len(data["invited_users"])
                                    total_coins, coins_by_study = yield self.coin_manager.get_coin(db_user["uid"])
                                    if total_coins > paid_coin:
                                        question_id = yield self.QA_manager.post_question(data, user, uid, nick_name,
                                                                                        user_icon, db_user["university"],
                                                                                        db_user["college"], user_desc,
                                                                                        star_info, db_user["real_name"])
                                        for u in data["invited_users"]:
                                            assert "user" in u and "real_name" in u
                                            if "need_anonymous" in data and data["need_anonymous"]:
                                                yield self.system_msg_proxy.send_system_message(u["user"],  MessageManager.MSG_TYPE["question_invite"],
                                                 u"匿名用户发起了问题%s，邀请您来帮忙回答，并乐意为您支付%d积分！" % (self.get_question_link(question_id, data["title"]), data["how_much"]))
                                            else:
                                                yield self.system_msg_proxy.send_system_message(u["user"],  MessageManager.MSG_TYPE["question_invite"],
                                                 u"用户%s发起了问题%s，邀请您来帮忙回答，并乐意为您支付%d积分！" %
                                                                          (self.get_user_link(db_user), self.get_question_link(question_id, data["title"]), data["how_much"]))
                                            from_user = tornado.escape.to_unicode(db_user["real_name"]) + u"(来自" +  tornado.escape.to_unicode(db_user["university"]) + u")"
                                            question_link = u"%s/#/qadetail/%s" % (OUR_WEB_URL, question_id)
                                            send_invitation_mail(u["user"], from_user, u["real_name"], data["title"], question_link)
                                        ret = {"err": self.ERR["NONE"]}
                                    else:
                                        ret = {"err": self.ERR["COIN_NOT_ENOUGH"]}
                        else:
                            yield self.QA_manager.post_question(data, user, uid, nick_name, user_icon, db_user["university"],
                                                                db_user["college"], user_desc, star_info, db_user["real_name"])
                            ret = {"err": self.ERR["NONE"]}
                else:
                    ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class EditQuestionHandler(PostQuestionHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            data = json.loads(self.request.body)
            if "id" not in data or "content" not in data:
                ret = {"err": self.ERR["ARGUMENT_ERR"]}
            elif len(data["content"]) <= 0 or len(data["content"]) > self.ARG_LENGTH_LIMIT["content"]:
                ret = {"err": self.ERR["CONTENT_LEN_ERR"]}
            else:
                db_user = yield self.users_manager.find_user(user)
                if db_user:
                    uid = db_user["uid"]
                    nick_name = db_user["nick_name"]
                    user_icon = db_user["icon"]
                    user_desc = db_user["desc"]
                    star_info = [v for k, v in STAR_CLASS.iteritems() if v in db_user]
                    yield self.QA_manager.edit_question(data, user, uid, nick_name, user_icon, db_user["university"],
                                                        db_user["college"], user_desc, star_info, db_user["real_name"])
                    ret = {"err": self.ERR["NONE"]}
                else:
                    ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class PostAnswerHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2, "CONTENT_LEN_ERR": 3, "ALREADY_POST": 4, "ICON_NOT_SET": 5}
    ARG_LENGTH_LIMIT = {"content": 50000}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            data = json.loads(self.request.body)
            if "question_id" not in data:
                ret = {"err": self.ERR["ARGUMENT_ERR"]}
            elif len(data["content"]) <= 0 or len(data["content"]) > self.ARG_LENGTH_LIMIT["content"]:
                ret = {"err": self.ERR["CONTENT_LEN_ERR"]}
            else:
                db_user = yield self.users_manager.find_user(user)
                if db_user:
                    uid = db_user["uid"]
                    nick_name = db_user["nick_name"]
                    user_icon = db_user["icon"]
                    if "images/user_icon/" in user_icon:
                        ret = {"err": self.ERR["ICON_NOT_SET"], "info": u"请先设置用户头像，您才有权限回答哦！"}
                    else:
                        star_info = [v for k, v in STAR_CLASS.iteritems() if v in db_user]
                        if "need_anonymous" in data and data["need_anonymous"]:
                            nick_name = "匿名用户"
                            # user_icon = self.static_url('images/user_icon/22.jpg')
                        answer_id, question = yield self.QA_manager.post_answer(data, user, uid, nick_name,
                                                                                         user_icon, db_user["university"],
                                                                                         db_user["college"], db_user["desc"], star_info, real_name=db_user["real_name"])
                        if answer_id is None and question:
                            ret = {"err": self.ERR["ALREADY_POST"]}
                        else:
                            if question["be_invited"]:
                                yield self.coin_manager.invite_user_answer_question(uid, question["id"], question["publisher_uid"], question["how_much"])

                                yield self.system_msg_proxy.send_system_message(question["publisher"], MessageManager.MSG_TYPE["invited_user_answered"], u"亲，您发布的问题%s被邀请人%s回答了！" % (self.get_question_link(question["id"], question["title"]), self.get_user_link(db_user)))
                            else:
                                if "need_anonymous" in data and data["need_anonymous"]:
                                    yield self.system_msg_proxy.send_system_message(question["publisher"], MessageManager.MSG_TYPE["invited_user_answered"], u"亲，您发布的问题%s被神秘用户回答了一次！" % self.get_question_link(question["id"], question["title"]))
                                else:
                                    yield self.system_msg_proxy.send_system_message(question["publisher"], MessageManager.MSG_TYPE["invited_user_answered"], u"亲，您发布的问题%s被%s回答了一次！" % (self.get_question_link(question["id"], question["title"]), self.get_user_link(db_user)))
                                del db_user["password"]
                            ret = {"err": self.ERR["NONE"], "answer_id": answer_id, "user_info": db_user}
                else:
                    ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class EditAnswerHandler(PostAnswerHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            data = json.loads(self.request.body)
            if "id" not in data or "content" not in data:
                ret = {"err": self.ERR["ARGUMENT_ERR"]}
            elif len(data["content"]) <= 0 or len(data["content"]) > self.ARG_LENGTH_LIMIT["content"]:
                ret = {"err": self.ERR["CONTENT_LEN_ERR"]}
            else:
                db_user = yield self.users_manager.find_user(user)
                if db_user:
                    uid = db_user["uid"]
                    nick_name = db_user["nick_name"]
                    user_icon = db_user["icon"]
                    star_info = [v for k, v in STAR_CLASS.iteritems() if v in db_user]
                    question_id, question_title, question_poster = yield self.QA_manager.edit_answer(data, user, uid, nick_name, user_icon, db_user["university"],
                                                      db_user["college"], db_user["desc"], star_info, real_name=db_user["real_name"])
                    if question_poster:
                        yield self.system_msg_proxy.send_system_message(question_poster, MessageManager.MSG_TYPE["question_be_answered"], u"%s编辑了对%s的回答！" % (self.get_user_link(db_user), self.get_question_link(question_id, question_title)))
                    ret = {"err": self.ERR["NONE"], "answer_id": data["id"], "user_info": db_user}
                else:
                    ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "您需要登录后才能操作！"}
        self.write(json.dumps(ret))
        self.finish()


class QuestionListInHomePageHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        class2 = self.get_argument("class2", "校园")
        total_page, current_page, experience_list = yield self.QA_manager.get_experience_overview(1, class2)
        self.write(json.dumps({"err": self.ERR["NONE"], "question_list": experience_list}))
        self.finish()


class QuestionListHandler2(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        class2 = self.get_argument("class2", "校园")
        page = self.get_argument("page", "1")
        detailed = self.get_argument("detail", "1")
        if not page.isdigit() or not detailed.isdigit() or not self.QA_manager.is_valid_tag_class(class2):
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            page = int(page)
            detailed = int(detailed)
            total_page, current_page, experience_list = yield self.QA_manager.get_experience_overview(page, class2)
            if detailed:
                total_page2, current_page2, experience_list2 = yield self.QA_manager.get_question_overview(0)
                total_page3, current_page3, experience_list3 = yield self.QA_manager.get_question_overview(1)
                self.write(json.dumps(
                    {"err": self.ERR["NONE"], "user": None,
                     "cared_question": {"question_list": experience_list, "total_page": total_page,
                                                                 "current_page": current_page},
                     "latest_question": {"question_list": experience_list2, "total_page": total_page2,
                                         "current_page": current_page2},
                     "hottest_question": {"question_list": experience_list3, "total_page": total_page3,
                                          "current_page": current_page3}}))
            else:
                self.write(json.dumps({"err": self.ERR["NONE"],
                                       "cared_question": {"question_list": experience_list, "total_page": total_page,
                                                          "current_page": current_page}
                                       }))
        self.finish()


class QuestionListHandler3(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        by_all = self.get_argument("by_all", "1")
        class2 = self.get_argument("class2", "校园")
        page = self.get_argument("page", "1")
        detailed = self.get_argument("detail", "1")
        user = self.get_current_user()
        if not page.isdigit() or not by_all.isdigit() and detailed.isdigit():
            self.write(json.dumps({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}))
        else:
            page = int(page)
            by_all = int(by_all)
            detailed = int(detailed)
            if user:
                db_user = yield self.users_manager.find_user(user, True)
                university = "university" in db_user and db_user["university"] or None
                no_recommended = False
                if by_all:
                    # total_page, cur_page, experience_list = yield self.QA_manager.get_recommended_question2(user, page, True, class2, None)
                    # if total_page == self.QA_manager.NOT_SET_TAG:
                        total_page, cur_page, experience_list = yield self.QA_manager.get_experience_overview(page, class2)
                        # no_recommended = True
                else:
                    total_page, cur_page, experience_list = yield self.QA_manager.get_experience_overview(page, class2, university=university)
                    #total_page, cur_page, experience_list = yield self.QA_manager.get_recommended_question2(user, page, True, class2, university)

                if detailed:
                    total_page2, current_page2, experience_list2 = yield self.QA_manager.get_question_overview(0)
                    total_page3, current_page3, experience_list3 = yield self.QA_manager.get_question_overview(1)
                    self.write(json.dumps({"err": self.ERR["NONE"],
                                       "cared_question": {"question_list": experience_list, "total_page": total_page,
                                                          "current_page": page, "no_recommended": no_recommended},
                                       "latest_question": {"question_list": experience_list2, "total_page": total_page2,
                                                           "current_page": current_page2},
                                       "hottest_question": {"question_list": experience_list3,
                                                            "total_page": total_page3,
                                                            "current_page": current_page3}}))
                else:
                    self.write(json.dumps({"err": self.ERR["NONE"],
                                           "cared_question": {"question_list": experience_list, "total_page": total_page,
                                                              "current_page": page, "no_recommended": no_recommended}
                                           }))
            else:
                self.write(json.dumps({"err": self.ERR["UNAUTHENTICATED"]}))
        self.finish()


class QuestionDetailHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        question_id = self.get_argument("question_id", "")
        detailed = self.get_argument("detail", "0")
        user = self.get_current_user()
        if question_id and detailed.isdigit():
            question_info = yield self.QA_manager.get_question_by_id(question_id)
            total_page, cur_page, answers_list = yield self.QA_manager.get_answers(question_id, 1)
            if user:
                self.QA_manager.insert_user_interested_tags(user, question_info["tags_with_class"])
            if int(detailed):
                # TODO similarity question algorithm
                # TF-IDF, or similarity hash
                total_page2, current_page2, experience_list2 = yield self.QA_manager.get_question_overview(0)
                total_page3, current_page3, experience_list3 = yield self.QA_manager.get_question_overview(1)
                self.write(json.dumps({"err": self.ERR["NONE"], "question": question_info, "answers_list": answers_list,
                                   "total_page": total_page, "current_page": cur_page,
                                    "latest_question": {"question_list": experience_list2, "total_page": total_page2,
                                         "current_page": current_page2},
                                    "hottest_question": {"question_list": experience_list3, "total_page": total_page3,
                                          "current_page": current_page3}
                                   }))
            else:
                self.write(json.dumps({"err": self.ERR["NONE"], "question": question_info, "answers_list": answers_list,
                                       "total_page": total_page, "current_page": cur_page}))
        else:
            self.write(json.dumps({"err": self.ERR["ARGUMENT_ERR"]}))
        self.finish()


class AnswerListHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        question_id = self.get_argument("question_id", "")
        # 0 for time, 1 for thumb up number
        sort_by = self.get_argument("sort_by", "1")
        page = self.get_argument("page", "1")
        if sort_by.isdigit() and page.isdigit():
            sort_by = int(sort_by)
            page = int(page)
            total_page, cur_page, answers_list = yield self.QA_manager.get_answers(question_id, page, sort_by)
            self.write(json.dumps({"err": self.ERR["NONE"], "answers_list": answers_list,
                                   "total_page": total_page, "current_page": cur_page}))
        else:
            self.write(json.dumps({"err": self.ERR["ARGUMENT_ERR"]}))
        self.finish()


class QuestionThumbUpHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2, "ALREADY_DONE": 3}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        qa_id = self.get_argument("id", "")
        user = self.get_current_user()
        is_answer = "answer" in self.request.uri
        if user:
            if qa_id:
                ok, publisher, title, question_id = yield self.QA_manager.inc_thumb_up_num(qa_id, user, is_answer)
                if ok:
                    db_user = yield self.users_manager.find_user(user)
                    if is_answer:
                        yield self.system_msg_proxy.send_system_message(publisher, MessageManager.MSG_TYPE["QA_thumb_up"], u"您对%s的回答真好，被%s点赞了一次！" % (self.get_question_link(question_id, title), self.get_user_link(db_user)))
                    else:
                        yield self.system_msg_proxy.send_system_message(publisher, MessageManager.MSG_TYPE["QA_thumb_up"], u"您分享的问题%s被%s点赞了一次！" % (self.get_question_link(question_id, title), self.get_user_link(db_user)))
                    self.write({"err": self.ERR["NONE"]})
                else:
                    self.write({"err": self.ERR["ALREADY_DONE"]})
            else:
                self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            self.write({"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"})
        self.finish()





class QuestionCollectHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        question_id = self.get_argument("question_id", "")
        user = self.get_current_user()
        if user:
            if question_id:
                yield self.QA_manager.collect_experience(user, question_id)
                self.write(json.dumps({"err": self.ERR["NONE"]}))
            else:
                self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            self.write({"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"})
        self.finish()


class UserQuestionListHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        page = self.get_argument("page", "1")
        if page.isdigit():
            page = int(page)
            user = self.get_argument("user", None)
            if not user: 
                self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
                self.finish()
                return
            need_anonymous = True if self.get_current_user() == user else False
            if "/user/question/posted" in self.request.uri:
                total_page, current_page, question_list = yield self.QA_manager.get_posted_experience(user, page, need_anonymous=need_anonymous)
            elif "/user/question/answered" in self.request.uri:
                total_page, current_page, question_list = yield self.QA_manager.get_answered_experience(user, page, need_anonymous=need_anonymous)
            else:
                total_page, current_page, question_list = yield self.QA_manager.get_collected_experience(user, page)
            self.write(
                json.dumps({"err": self.ERR["NONE"], "question_list": question_list, "total_page": total_page,
                            "current_page": current_page}))
        else:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        self.finish()


class QACommentHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2, "CONTENT_LEN_ERR": 3}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        QA_id = self.get_argument("id", "")
        content = self.get_argument("content", "")
        reply_to_user = self.get_argument("reply_to_user", "")
        user = self.get_current_user()
        if user:
            if QA_id and content and reply_to_user and len(content) <= 100:
                is_answer = "answer" in self.request.uri
                db_user = yield self.users_manager.find_user(user)
                db_user2 = yield self.users_manager.find_user(reply_to_user)
                if db_user2:
                    question_id, title = yield self.QA_manager.post_comment(QA_id, content, db_user["user"], db_user["icon"], db_user["university"], db_user["real_name"], db_user2["user"], db_user2["icon"], db_user2["university"], db_user2["real_name"], is_answer=is_answer)
                    if title:
                        if is_answer:
                            yield self.system_msg_proxy.send_system_message(reply_to_user, MessageManager.MSG_TYPE["QA_comment"], u"用户%s为您的回答%s添加了评论：%s" % (self.get_user_link(db_user), self.get_question_link(question_id, title, QA_id), content))
                        else:
                            yield self.system_msg_proxy.send_system_message(reply_to_user, MessageManager.MSG_TYPE["QA_comment"], u"用户%s为您的问题%s添加了评论：%s" % (self.get_user_link(db_user), self.get_question_link(QA_id, title), content))
                        self.write({"err": self.ERR["NONE"]})
                    else:
                        self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
                else:
                    self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
            else:
                self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            self.write({"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"})
        self.finish()


class QAThanksHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2, "COIN_NOT_ENOUGH": 3}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        QA_id = self.get_argument("id", "")
        how_much = self.get_argument("how_much", "")
        user = self.get_current_user()
        is_answer = "answer" in self.request.uri
        if user:
            if QA_id and how_much.isdigit() and int(how_much) > 0:
                how_much = int(how_much)
                db_user = yield self.users_manager.find_user(user)
                uid = db_user["uid"]
                total_fb, _ = yield self.coin_manager.get_coin(uid)
                if total_fb < how_much:
                    ret = {"err": self.ERR["COIN_NOT_ENOUGH"], "info": "积分不够了！亲，您的积分仅仅为:" + str(total_fb)}
                    self.write(json.dumps(ret))
                    self.finish()
                    return
                ok, publisher, title, question_id, publisher_uid = yield self.QA_manager.thanks(QA_id, user,
                                                                                   db_user["nick_name"], is_answer, how_much)
                if ok:
                    yield self.users_manager.inc_thanks_coin(publisher, how_much)
                    yield self.coin_manager.thanks_user_post_experience(uid, publisher_uid, how_much, QA_id, is_answer)
                    if is_answer:
                        yield self.system_msg_proxy.send_system_message(publisher, MessageManager.MSG_TYPE["QA_reward"], u"%s给予您%d积分，感谢您很好地回答了问题%s！" % (self.get_user_link(db_user), how_much, self.get_question_link(question_id, title)))
                    else:
                        yield self.system_msg_proxy.send_system_message(publisher, MessageManager.MSG_TYPE["QA_reward"], u"%s给予您%d积分，感谢您发起了问题%s！" % (self.get_user_link(db_user), how_much, self.get_question_link(question_id, title)))
                self.write({"err": self.ERR["NONE"]})
            else:
                self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        else:
            self.write({"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"})
        self.finish()


# class QuestionListAfterLoginHandler(BaseHandler):
#     ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2}
#
#     @tornado.web.asynchronous
#     @tornado.gen.coroutine
#     def get(self):
#         page = self.get_argument("page", "1")
#         is_follow = self.get_argument("is_follow", "0")
#         user = self.get_current_user()
#         if user:
#             if not page.isdigit() or not is_follow.isdigit():
#                 self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
#             else:
#                 page = int(page)
#                 is_follow = int(is_follow)
#                 if not is_follow:
#                     total_page, current_page, experience_list = yield self.QA_manager.get_recommended_experience(user,
#                                                                                                                  page)
#                 else:
#                     total_page, current_page, experience_list = yield self.QA_manager.get_followed_experience(user,
#                                                                                                               page)
#                 self.write(
#                     json.dumps({"err": self.ERR["NONE"], "experience_list": experience_list, "total_page": total_page,
#                                 "current_page": current_page}))
#         else:
#             self.write({"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"})
#         self.finish()


class MyTagRemoveHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            data = json.loads(self.request.body)
            if "tags_with_class" in data:
                yield [self.users_manager.remove_my_interested_tags(user, tag_with_class) for tag_with_class in data["tags_with_class"]]
                ret = {"err": self.ERR["NONE"]}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"]}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()


class MyTagAddHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        if user:
            data = json.loads(self.request.body)
            if "tags_with_class" in data:
                # TODO remove
                # yield [self.users_manager.insert_my_interested_tags(user, tag_with_class) for tag_with_class in data["tags_with_class"]]
                self.QA_manager.insert_user_interested_tags(user, data["tags_with_class"])
                ret = {"err": self.ERR["NONE"]}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"]}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()


class MyInterestedTagListHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            tags = yield self.users_manager.get_my_interested_tags(user)
            ret = {"err": self.ERR["NONE"], "tags": list(tags)}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()


class TagListHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        tag_class = self.get_argument("tag_class", "")
        user = self.get_current_user()
        if user:
            if not tag_class:
                tags = self.QA_manager.get_all_experience_tags()
            else:
                tags = self.QA_manager.get_tags_by_class(tag_class)
            ret = {"err": self.ERR["NONE"], "tags": tags}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()


class TagExtractHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2}

    def post(self):
        user = self.get_current_user()
        if user:
            data = json.loads(self.request.body)
            tag_class = data["tag_class"]
            title = data["question_title"]
            content = data["question_content"]
            if self.QA_manager.is_valid_tag_class(tag_class) and title and content:
                tags = self.QA_manager.extract_tag_from_text(title+" "+content, tag_class)
                ret = {"err": self.ERR["NONE"], "tags": tags}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"]}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))


class MyTagListHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            tags = yield self.users_manager.get_my_tags(user)
            ret = {"err": self.ERR["NONE"], "tags": list(tags)}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()


class UserFollowHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1, "ALREADY_FOLLOW": 2, "FOLLOW_SELF_NOT_ALLOWED": 3}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        data = json.loads(self.request.body)
        who = data["who"]
        follower = self.get_current_user()
        if follower and who:
            if follower != who:
                ok = yield self.users_manager.follow(who, follower)
                if ok:
                    db_user = yield self.users_manager.find_user(follower)
                    yield self.system_msg_proxy.send_system_message(who, MessageManager.MSG_TYPE["get_a_fans"], u'恭喜您，获得了粉丝%s！' % self.get_user_link(db_user))
                    ret = {"err": self.ERR["NONE"]}
                else:
                    ret = {"err": self.ERR["ALREADY_FOLLOW"]}
            else:
                ret = {"err": self.ERR["FOLLOW_SELF_NOT_ALLOWED"]}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(ret)
        self.finish()


class UserUnfollowHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        data = json.loads(self.request.body)
        who = data["who"]
        follower = self.get_current_user()
        if follower and who:
            yield self.users_manager.unfollow(who, follower)
            ret = {"err": self.ERR["NONE"]}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(ret)
        self.finish()


class UsersFollowersHandler(BaseHandler):
    ERR = {"NONE": 0, "UNAUTHENTICATED": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            users_list = yield self.users_manager.get_following_users(user)
            ret = {"err": self.ERR["NONE"], "following_users": users_list}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(json.dumps(ret))
        self.finish()


class UserPageHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "FAILED": 2, "UNAUTHENTICATED": 3}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        desc = self.get_argument("desc", None)
        phone = self.get_argument("phone", None)
        qq = self.get_argument("qq", None)
        state = self.get_argument("state", None)
        state_desc = self.get_argument("state_desc", None)
        honor = self.get_argument("honor", None)
        icon = self.get_argument("icon", None)
        user = self.get_current_user()
        if user:
            info_dict = dict()
            if desc is not None:
                info_dict['desc'] = tornado.escape.utf8(desc).strip()
            if phone is not None and (11 == len(phone) or 0 == len(phone)):
                info_dict['phone'] = tornado.escape.utf8(phone).strip()
            if qq is not None:
                info_dict['qq'] = tornado.escape.utf8(qq).strip()
            if state is not None:
                info_dict['state'] = tornado.escape.utf8(state).strip()
            if state_desc is not None:
                info_dict['state_desc'] = tornado.escape.utf8(state_desc).strip()
            if honor is not None:
                honor = json.loads(honor)
                info_dict['honor'] = honor
            if icon is not None:
                info_dict["icon"] = icon
                yield self.QA_manager.update_user_icon(user, icon)
            if info_dict:
                yield self.users_manager.change_user_basic_info(user, info_dict)
                ret = {"err": self.ERR["NONE"]}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"], "info": "未经授权操作！"}
        self.write(ret)
        self.finish()

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        user = self.get_argument("user", "")
        if user:
            current_user = self.get_current_user()
            is_me = user == current_user
            db_user = yield self.users_manager.find_user_info(user, is_me)
            if db_user:
                cert_list = list()
                for c, t in CLASS_STAR.iteritems():
                    if c in db_user:
                        db_user.pop(c)
                        cert_list.append(t)
                if cert_list:
                    db_user["cert"] = cert_list
                uid = db_user.pop('uid')
                total_coins, coins_by_study = yield self.coin_manager.get_coin(uid)
                db_user["is_me"] = is_me
                if not is_me:
                    followers = yield self.users_manager.get_followers(user)
                    db_user["is_followed"] = current_user in followers
                db_user["total_coins"] = total_coins
                db_user["coins_by_study"] = coins_by_study
                ret = {"err": self.ERR["NONE"], "info": db_user}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        self.write(ret)
        self.finish()


class RewardResKeyUploadHandler(BaseHandler):
    ERR = {"NONE": 0, "UNKNOWN": 1}

    def check_xsrf_cookie(self):
        pass

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self, reward_id, file_hash):
        data = json.loads(self.request.body)
        if 'items' in data and data['items'] and 'key' in data['items'][0] and 'hash' in data['items'][0]:
            if data["items"][0]['returnOld'] == 0:
                ok = yield self.reward_manager.insert_reward_res_preview_key(reward_id, file_hash, data['items'][0]['key'], data['items'][0]['hash'])
                if ok:
                    logging.info("insert preview key of reward resource OK:" + data['items'][0]['key'] + " hash:" + data['items'][0]['hash'])
                    ret = {"err": self.ERR["NONE"]}
                else:
                    logging.info("insert preview key of reward resource failed:" + data['items'][0]['key'] + " hash:" + data['items'][0]['hash'])
            else:
                yield self.reward_manager.insert_reward_res_preview_key(reward_id, file_hash, data['items'][0]['key'], data['items'][0]['hash'])
                logging.info("insert preview key of reward reward already exists.")
                ret = {"err": self.ERR["NONE"]}
        else:
            ret = {"err": self.ERR["UNKNOWN"]}
        self.write(json.dumps(ret))
        self.finish()


class UserAnswerRecommendHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_current_user()
        data = json.loads(self.request.body)
        if user:
            top_users = yield self.QA_manager.get_recommended_top_users(data["tag_class"], data["tags"])
            ret = {"err": self.ERR["NONE"], "users": top_users}
        else:
            ret = {"err": self.ERR["UNAUTHENTICATED"]}
        self.write(json.dumps(ret))
        self.finish()


class UserRecommendHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1, "UNAUTHENTICATED": 2}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        page = self.get_argument("page", "1")
        by_all = self.get_argument("by_all", "1")
        need_star = self.get_argument("need_star", "1")
        interested = self.get_argument("interested", "1")
        user = self.get_current_user()
        if user:
            if page.isdigit() and by_all.isdigit() and need_star.isdigit():
                page = int(page)
                by_all = int(by_all)
                interested = int(interested)
                need_star = int(need_star)
                if by_all:
                    if interested:
                        total_page, cur_page, users = yield self.QA_manager.get_recommended_users2(user, None, page, use_cache=True)
                        if total_page == self.QA_manager.NOT_SET_TAG:
                            _, _, users = yield self.users_manager.get_some_recommended_users(page=1)
                    else:
                        total_page, cur_page, users = yield self.users_manager.get_user_by_thumb_num(None, page)
                else:
                    db_user = yield self.users_manager.find_user(user)
                    if interested:
                        if db_user:
                            total_page, cur_page, users = yield self.QA_manager.get_recommended_users2(user, db_user["university"], page, use_cache=True)
                        else:
                            total_page, cur_page, users = 0, 1, []
                    else:
                        if db_user:
                            total_page, cur_page, users = yield self.users_manager.get_user_by_thumb_num(db_user["university"], page)
                        else:
                            total_page, cur_page, users = 0, 1, []
                if need_star:
                    top_stars = yield self.users_manager.get_top_star_users()
                    ret = {"err": self.ERR["NONE"], "users": users, "total_page": total_page, "current_page": cur_page, "stars": top_stars}
                else:
                    ret = {"err": self.ERR["NONE"], "users": users, "total_page": total_page, "current_page": cur_page}
            else:
                ret = {"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"}
        else:
            total_page, cur_page, users = yield self.users_manager.get_some_recommended_users(int(page))
            top_stars = yield self.users_manager.get_top_star_users()
            ret = {"err": self.ERR["NONE"], "users": users, "total_page": total_page, "current_page": cur_page, "stars": top_stars}
        self.write(json.dumps(ret))
        self.finish()


class UserQuestionMainHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    def get(self):
        # TODO add nginx route URL
        # self.redirect("/statics/home.html")
        with open("static/user-experience.html") as f:
            self.write(f.read())
        self.finish()


class QuestionSearchHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        page = self.get_argument("page", '1')
        keyword = self.get_argument("keyword", "").strip()
        if not page.isdigit():
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
            self.finish()
            return
        page = int(page)
        if len(keyword) > 30:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
            self.finish()
            return
        keyword = tornado.escape.utf8(keyword)
        if keyword:
            total_page, cur_page, question_list = yield self.QA_manager.search(keyword, page)
            ret = {"err": self.ERR["NONE"], "resource_list": question_list, "total_page": total_page, "current_page": cur_page}
            if cur_page == 1:
                ret["hot_keywords"] = self.QA_manager.top_search_keywords()
            self.write(json.dumps(ret))
        else:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        self.finish()


class UserSearchHandler(BaseHandler):
    ERR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        page = self.get_argument("page", '1')
        keyword = self.get_argument("keyword", "").strip()
        if not page.isdigit() or len(keyword) > 30:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
            self.finish()
            return
        page = int(page)
        keyword = tornado.escape.utf8(keyword)
        if keyword:
            users, cur_page, total_page = yield self.users_manager.search_user(keyword, page)
            ret = {"err": self.ERR["NONE"], "user_list": users, "total_page": total_page, "current_page": cur_page}
            if cur_page == 1:
                ret["hot_keywords"] = self.users_manager.top_search_keywords()
            self.write(json.dumps(ret))
        else:
            self.write({"err": self.ERR["ARGUMENT_ERR"], "info": "参数错误！"})
        self.finish()


class MainHandler(BaseHandler):
    MAIN_CONTENT = ""
    mobile_header = ('mobile',
                     'iPhone', 'iPad', 'iPod', 'iOS',
                     'Android',
                     'ucweb', 'windowsce', 'operamini','wap',
                     'operamobi', 'opera mobi', 'openwave',
                     "Windows Phone",
                     "MicroMessenger", # 微信内置browser
                     'MQQBrowser', # QQ browser
                     )

    def is_from_mobile(self):
        user_agent = self.request.headers['User-Agent']
        logging.info("User Agent:" + user_agent)
        user_agent = user_agent.lower()
        for h in self.mobile_header:
            if user_agent.find(h.lower()) >= 0:
                return True
        return False

    def get(self):
        if self.is_from_mobile():
            # self.redirect(self.request.protocol + "://" + self.request.host + "/mobile/app/index.html")
            # http://test.friendsbt.com/mobile/app/index.html#/find/%E6%A0%A1%E5%9B%AD
            logging.info("OK, is from mobile.")
            self.redirect(self.request.protocol + "://" + self.request.host + "/mobile/app/")
        else:
            if not self.MAIN_CONTENT:
                with open("static/index.html") as f:
                    self.MAIN_CONTENT = f.read()
            self.write(self.MAIN_CONTENT)
            self.finish()
            # self.redirect(self.request.protocol + "://" + self.request.host + "/statics/index.html")


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            # just for administer
            # (r'/', MainHandler),
            (r"/statistic/today", StatisticOfTodayHandler),
            (r"/star/unset", StarUserSetHandler),
            (r"/star/set", StarUserSetHandler),
            (r"/question/hide", HideQuestionHandler),
            (r"/question/stick", StickQuestionHandler),
            (r"/message/send", UserMessageSendHandler),
            (r"/tag/move", MoveTagHandlder),
            (r"/resource/list", ResourceListHandler),
            (r"/resource/recommend", ResourceRecommendHandler),
            (r"/resource/recommend/more", MoreResourceRecommendHandler),
            (r"/resource/search", CourseSearchHandler),
            (r"/resource/university", UniversityResourceHandler),
            (r"/resource/college", CollegeResourceHandler),
            (r"/resource/course", CourseViewHandler),
            (r"/resource/thumb_up", ResourceSupportHandler),
            (r"/resource/thumb_down", ResourceSupportHandler),

            (r"/course/list", CourseListHandler),
            (r"/course/comment/list", CourseCommentListHandler),  # TODO remove
            (r"/course/comment/post", CourseCommentPostHandler),  # TODO remove

            (r"/course/question/issue", IssueQuestionHandler),
            (r"/course/question/list", QuestionListHandler),
            (r"/course/question/view", QuestionViewHandler),
            (r"/course/question_reply/view_more", QuestionReplyRelateHandler),
            (r"/course/question_reply/sort", QuestionReplyRelateHandler),
            (r"/course/question_reply/pros", QuestionReplySupportHandler),
            (r"/course/question_reply/cons", QuestionReplySupportHandler),
            (r"/course/question/reply", QuestionReplyHandler),
            # (r"/course/question/follow", QuestionFollowHandler),
            (r"/course/question_reply/comment", QuestionReplyCommentHandler),
            (r"/course/question_reply_comment/view", QuestionReplyCommentViewHandler),

            (r"/course/detail", CourseDetailHandler),
            (r"/course/search", CourseSearchHandler),

            (r"/reward/post", RewardPostHandler),
            (r"/reward/append_fb", RewardAppendHandler),
            (r"/reward/list", RewardListHandler),
            (r"/reward/resource/upload", RewardResourceUploadHandler),
            (r"/reward/resource/confirm", RewardResourceConfirmHandler),
            (r"/reward/user/all", UserRewardListHandler),
            (r"/reward/user/related", UserRewardListHandler),
            (r"/reward/loop/expired", RewardExpiredHandler),

            (r"/user/message/read", UserMessageReadHandler),
            (r"/user/message/delete", UserMessageDeleteHandler),
            (r"/user/message/list", UserMessageListHandler),
            (r"/user/message/count", UserMessageListHandler),

            (r"/user/updated_info", UserUpdatedInfoHandler),

            (r"/user/log/upload", ErrorDataHandler),
            (r"/user/log/list", UploadLogListHandler),

            (r"/fbrank", StudyFBRankHandler),

            (r"/resource/comment/list", ResourceCommentListHandler),
            (r"/resource/comment/post", ResourceCommentPostHandler),
            (r"/resource/rating", RatingHandler),
            (r"/resource/user", UserResourceListHandler),
            (r"/resource/detail", ResourceDetailHandler),
            (r"/resource/remove", ResourceRemoveHandler),
            (r"/resource/download", DownloadHandler),
            (r"/reward/resource/download", RewardResourceDownloadHandler),
            (r"/reward/resource/preview", RewardResourceDownloadHandler),
            (r"/resource/preview", DownloadHandler),
            (r"/resource/preview/upload", PreviewUploadHandler),
            (r"/change_upload", ResourceChangeHandler),
            (r"/download", DownloadHandler),  # TODO Remove
            (r"/fetch_token", UploadTokenHandler),

            (r"/user/info/change_college", ChangeCollegeHandler),
            (r"/user/info/change", UserInfoHandler),
            (r"/user/info/fecth", UserInfoHandler),
            (r"/user/info/preview", UserInfoPreviewHandler),

            (r"/user/info", UserPageHandler), # url for user page

            (r"/user/question/collected", UserQuestionListHandler),
            (r"/user/question/posted", UserQuestionListHandler),
            (r"/user/question/answered", UserQuestionListHandler),

            (r'/captcha', CaptchaHandler),
            # (r'/gen_PINNumber', PINNumberHandler),

            (r"/user/star/auth", StarUserAuthHandler),
            (r"/user/star/validate", StarUserValidateHandler),
            (r"/user/unfollow", UserUnfollowHandler),
            (r"/user/follow", UserFollowHandler),
            (r"/user/following", UsersFollowersHandler),
            (r"/user/recommend", UserRecommendHandler),
            (r"/user/search", UserSearchHandler),
            #(r"/question/overview", QuestionListInHomePageHandler),
            (r"/question/search", QuestionSearchHandler),
            (r"/question/post", PostQuestionHandler),
            (r"/question/edit", EditQuestionHandler),
            (r"/question/list", QuestionListHandler2),
            (r"/question/login/list", QuestionListHandler3),
            (r"/question/detail", QuestionDetailHandler),
            (r"/question/thumb_up", QuestionThumbUpHandler),
            (r"/question/thanks", QAThanksHandler),
            (r"/question/collect", QuestionCollectHandler),
            (r"/question/comment", QACommentHandler),
            (r"/question/user/recommend", UserAnswerRecommendHandler),

            (r"/answer/list", AnswerListHandler),
            (r"/answer/post", PostAnswerHandler),
            (r"/answer/edit", EditAnswerHandler),
            (r"/answer/thumb_up", QuestionThumbUpHandler),
            (r"/answer/thanks", QAThanksHandler),
            (r"/answer/comment", QACommentHandler),

            (r"/user_page", UserQuestionMainHandler),

            (r"/tag/add", MyTagAddHandler),
            (r"/tag/remove", MyTagRemoveHandler),
            (r"/tag/list", MyInterestedTagListHandler),
            (r"/tag/all", TagListHandler),
            (r"/tag/extract", TagExtractHandler),
            # (r"/tag/my_list", MyTagListHandler),

            (r"/xsrf", XSRFHandler),
            (r"/register", RegisterHandler),
            (r"/register_new", RegisterNewHandler),
            (r"/upload", UploadHandler),
            (r"/preview_key_upload/(\w+)/(.*)", RewardResKeyUploadHandler),
            (r"/login", LoginHandler),
            (r"/login_from_fbt", LoginFromFBTHandler),
            (r"/is_login", LoginCheckHandler),
            (r"/logout", LogoutHandler),
            (r"/change_password", ChangePasswordHandler),
            (r"/attachment/download", AttachmentDownloadHandler),
            (r"/reset_password", SendResetEmailHandler),
            (r"/do_reset_password", ResetPwdHandler),
            # (r"/user/online", OnlineUserHandler),
            (r"/get_university", UniversityDBHandler),
            (r"/v2/get_university", UniversityDBHandler),
            # (r"/get_course", CourseDBHandler),
            (r"/statics/(.*)", tornado.web.StaticFileHandler,
             {"path": os.path.join(os.path.dirname(__file__), "static")}),

            (r"/mobile/(.*)", tornado.web.StaticFileHandler, {"path": os.path.join(os.path.dirname(__file__), "static", "mobile"), "default_filename": "index.html"}),
            (r"/(.*)", tornado.web.StaticFileHandler, {"path": os.path.join(os.path.dirname(__file__), "static"), "default_filename": "index.html"})

        ]
        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
            cookie_secret="bZJc2sWbQLKos6GkHn/VB9oXwQt8S0R0kRvJ5/xJ89E=",
            login_url="/login",
            debug=DEBUG_ENV,
            gzip=True,
        )
        tornado.web.Application.__init__(self, handlers, **settings)

        # dependency across all handlers
        self.study_resource_manager = StudyResourceManager()
        self.redis_db_manager = RedisDBManager()
        self.university_db = UniversityDB()
        self.course_db = CourseDB()
        self.resource_comment_manager = ResourceCommentManager()
        self.course_comment_manager = CourseCommentManager()
        self.users_manager = UserManager()
        self.session_manager = SessionManager()
        self.rating_manager = RatingManager()
        self.system_msg_proxy = SystemMessageProxy()
        self.user_resource_manager = UserResourceManager()
        self.user_log_manager = UserUploadLogManager()
        self.reward_manager = RewardManager(self.study_resource_manager)
        self.coin_manager = CoinManager()
        self.question_manager = QuestionManager()
        self.QA_manager = QAManager()
        self.message_manager = MessageManager()
        self.lru_redis = RedisHandler(RedisHandler.type_lru)
        self.token_manager = TokenHandler(TOKEN_SECRET)


from tornado.options import define, options, parse_command_line

define('port', default=9999, help='run on the port', type=int)


def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application(), xheaders=True)
    http_server.listen(options.port)

    if DEBUG_ENV:
        from chat import ChatApplication
        app = ChatApplication()
        http_server = tornado.httpserver.HTTPServer(app)
        http_server.listen(CHAT_PORT)
    try:
        tornado.ioloop.IOLoop.instance().start()
    except Exception as e:
        print "OK. Exit...with:" + e.message


if __name__ == "__main__":
    main()

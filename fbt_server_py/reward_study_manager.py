# -*- coding: utf-8 -*-
__author__ = 'bone'

import motorclient
from util import generate_pkey
# from fb_manager import FBCoinManager
from coin_manager import CoinManager
from university_db import UniversityDB
from user_msg_center import SystemMessageProxy

from tornado.gen import coroutine
from tornado.gen import Return
from datetime import datetime
from datetime import timedelta
from time import time
from tornado.concurrent import Future


class RewardManager(object):
    RES_PAGE_NUM_IN_MY_REWARD = 15
    _DESCENDING = -1
    SORT_BY = {"time": "index_ctime", "total_fb": "total_fb"}
    STATUS = {"pending": "pending", "over": "over", "expired": "expired"}

    def __init__(self, study_res_man, db=None, fb_manager=None, university_db=None, user_msg_center=None):
        if db is None:
            self._db = motorclient.fbt
        else:
            self._db = db
        if fb_manager:
            self._fb_manager = fb_manager
        else:
            self._fb_manager = CoinManager()
        if university_db:
            self.university_db = university_db
        else:
            self.university_db = UniversityDB()
        self._study_res_man = study_res_man
        if user_msg_center:
            self._user_msg_center = user_msg_center
        else:
            self._user_msg_center = SystemMessageProxy()

    def set_user_msg_center(self, user_msg_center):
        self._user_msg_center = user_msg_center

    @coroutine
    def clear_db_for_test(self):
        yield self._db.study_reward.remove({})

    # @coroutine
    # def get_my_reward_expired(self, user, current_page=1):
    #     res = yield self.get_my_reward_helper(user, self.STATUS["expired"], current_page)
    #     raise Return(res)
    #
    # @coroutine
    # def get_my_reward_over(self, user, current_page=1):
    #     res = yield self.get_my_reward_helper(user, self.STATUS["over"], current_page)
    #     raise Return(res)
    #
    # @coroutine
    # def get_my_reward_over(self, user, current_page=1):
    #     res = yield self.get_my_reward_helper(user, self.STATUS["over"], current_page)
    #     raise Return(res)

    @coroutine
    def get_my_reward(self, user, current_page=1):
        cursor = self._db.study_reward.find({"publisher": user}, {"_id":0}).sort([('index_ctime', self._DESCENDING),])
        total_num = yield cursor.count()
        cursor = cursor.limit(self.RES_PAGE_NUM_IN_MY_REWARD).skip((current_page-1)*self.RES_PAGE_NUM_IN_MY_REWARD)
        reward_list = yield cursor.to_list(None)
        raise Return([(total_num+self.RES_PAGE_NUM_IN_MY_REWARD-1)/self.RES_PAGE_NUM_IN_MY_REWARD, current_page, reward_list])

    @coroutine
    def get_my_reward_helper(self, user, status, current_page=1):
        if status is None:
            cursor = self._db.study_reward.find({"publisher": user}, {"_id":0}).sort([('index_ctime', self._DESCENDING),])
        else:
            cursor = self._db.study_reward.find({"publisher": user, "status": status}, {"_id":0}).sort([('index_ctime', self._DESCENDING),])
        total_num = yield cursor.count()
        cursor = cursor.limit(self.RES_PAGE_NUM_IN_MY_REWARD).skip((current_page-1)*self.RES_PAGE_NUM_IN_MY_REWARD)
        reward_list = yield cursor.to_list(None)
        raise Return([(total_num+self.RES_PAGE_NUM_IN_MY_REWARD-1)/self.RES_PAGE_NUM_IN_MY_REWARD, current_page, reward_list])

    def gen_university_id(self, university): #, college):
        return self.university_db.get_university_id(university) #+":"+str(self.university_db.get_college_id(university,college))

    @coroutine
    def issue_reward(self, reward, user, uid, nick_name, user_icon=None):
        if "need_anonymous" in reward and reward["need_anonymous"]:
            reward["publisher_nick"] = "平沙落雁"
        else:
            reward["publisher_nick"] = nick_name
        reward["university_id"] = self.gen_university_id(reward["university"]) #, reward["college"])
        reward["publisher"] = user
        reward["publisher_uid"] = uid
        reward["publisher_real_nick"] = nick_name
        reward["publisher_img"] = user_icon
        ctime = datetime.now().strftime('%Y-%m-%d %H:%M')
        reward["more_seeker"] = [{"uid": uid, "user": user, "nick_name": nick_name,
                                           "ctime": ctime, "index_ctime": long(time()), "img": user_icon,
                                           "how_much": reward["total_fb"]}]
        reward["resource_list"] = []
        reward["index_ctime"] = long(time())
        reward["ctime"] = ctime
        reward["index_expired_date"] = long(time())+reward["expire_days"]*3600*24
        reward["expired_date"] = (datetime.now()+timedelta(days=reward["expire_days"])).strftime('%Y-%m-%d %H:%M')
        reward["status"] = self.STATUS["pending"]
        reward["related_users"] = [] # users who uploaded resource and appended fb
        reward["id"] = generate_pkey()
        yield self._db.study_reward.insert(reward)
        yield self._fb_manager.issue_a_reward(uid, reward["id"], -reward["total_fb"])
        raise Return(reward["id"])

    @coroutine
    def get_reward_overview(self, sort_by, current_page=1, university=None):
        assert sort_by==self.SORT_BY["total_fb"] or sort_by==self.SORT_BY["time"]
        if university:
            university_id = self.gen_university_id(university) #, college)
            cursor = self._db.study_reward.find({"status": {"$ne": self.STATUS["over"]}, "university_id": university_id,"index_expired_date": {"$gt": long(time())}}, {"_id":0}).sort([(sort_by, self._DESCENDING),])
        else:
            cursor = self._db.study_reward.find({"status": {"$ne": self.STATUS["over"]}, "index_expired_date": {"$gt": long(time())}}, {"_id":0}).sort([(sort_by, self._DESCENDING),])
        total_num = yield cursor.count()
        cursor = cursor.limit(self.RES_PAGE_NUM_IN_MY_REWARD).skip((current_page-1)*self.RES_PAGE_NUM_IN_MY_REWARD)
        reward_list = yield cursor.to_list(None)
        raise Return([(total_num+self.RES_PAGE_NUM_IN_MY_REWARD-1)/self.RES_PAGE_NUM_IN_MY_REWARD, current_page, reward_list])

    @coroutine
    def append_reward(self, reward_id, how_much, user, uid, nick_name, user_icon=None):
        index_ctime = long(time())
        ctime = datetime.now().strftime('%Y-%m-%d %H:%M')
        yield self._db.study_reward.update({"id": reward_id}, {"$inc": {"total_fb": how_much},"$push": {"more_seeker":
                                          {"uid": uid, "user": user, "nick_name": nick_name,
                                           "ctime": ctime, "index_ctime": index_ctime, "img": user_icon,
                                           "how_much": how_much}}, "$addToSet": {"related_users": user}})
        yield self._fb_manager.issue_a_reward(uid, reward_id, -how_much)

    @coroutine
    def get_reward_fb(self, reward_id):
        reward = yield self._db.study_reward.find_one({"id": reward_id}, {"_id":0, "total_fb":1})
        if reward:
            raise Return(reward["total_fb"])
        else:
            raise Return(0)

    @coroutine
    def find_resource(self, reward_id, file_hash):
        reward = yield self._db.study_reward.find_one({"id": reward_id})
        if not reward:
            raise Return(None)
        for user_resource in reward["resource_list"]:
            for res in user_resource["uploaded_resources"]:
                if res["file_hash"] == file_hash:
                    raise Return(res)
        raise Return(None)

    @coroutine
    def insert_reward_res_preview_key(self, reward_id, file_hash, preview_key, preview_hash):
        reward = yield self._db.study_reward.find_one({"id": reward_id})
        for user_resource in reward["resource_list"]:
            for res in user_resource["uploaded_resources"]:
                if res["file_hash"] == file_hash:
                    res["preview_key"] = preview_key
                    res["preview_hash"] = preview_hash
                    yield self._db.study_reward.save(reward)
                    raise Return(True)
        raise Return(False)
        
    @coroutine
    def upload_reward_resource(self, reward_id, user, uid, nick_name, file_hash, filename, file_size, comment=None, need_anonymous=False, file_key=None):
        index_ctime = long(time())
        ctime = datetime.now().strftime('%Y-%m-%d %H:%M')
        fake_nick_name = "我是雷锋" if need_anonymous else nick_name
        reward = yield self._db.study_reward.find_one({"id": reward_id})
        if reward and reward["publisher"] != user:
            if user not in reward["related_users"]:
                reward["related_users"].append(user)
            found = False
            resource = {"filename": filename, "file_size": file_size, "file_hash": file_hash, "file_key": file_key}
            for user_resource in reward["resource_list"]:
                if user_resource["user"] == user:
                    user_resource["uploaded_resources"].append(resource)
                    found = True
                    break
            if not found:
                reward["resource_list"].append({"uid": uid, "user": user, "nick_name": fake_nick_name,"real_nick_name": nick_name,
                                               "ctime": ctime, "index_ctime": index_ctime, "accepted": False,
                                                "uploaded_resources": [resource],
                                               "rate": 0,  "comment": comment})
            yield self._db.study_reward.save(reward)
            self._user_msg_center.add_msg(reward["publisher"], u'%s 为您的悬赏"%s"上传了一个资源，请到我的悬赏中查看！' % (nick_name, reward["university"]+"-"+reward["college"]+"-"+reward["course"]+"-"+reward["tag"]))
            raise Return(True)
        else:
            raise Return(False)

    # @coroutine
    # def thumb_up(self, reward_id, resource_index, nick_name):
    #     yield self._db.study_reward.update({"id": reward_id}, {"$addToSet":{"resource_list."+str(resource_index)+".supporters": nick_name}})
    #
    # @coroutine
    # def thumb_down(self, reward_id, resource_index, nick_name):
    #     yield self._db.study_reward.update({"id": reward_id}, {"$addToSet":{"resource_list."+str(resource_index)+".protesters": nick_name}})

    @coroutine
    def get_reward_resource_list(self, reward_id):
        reward = yield self._db.study_reward.find_one({"id": reward_id}, {"_id":0, "resource_list":1})
        if reward:
            raise Return(reward["resource_list"])
        else:
            raise Return([])

    @coroutine
    def user_confirm_resource(self, reward_id, user, rate):
        reward = yield self._db.study_reward.find_one({"id": reward_id})
        if reward and reward["status"] == self.STATUS["pending"]:
            if user not in reward["related_users"]:
                return
            reward["status"] = self.STATUS["over"]
            resource_index = -1
            for resource_index,user_resource in enumerate(reward["resource_list"]):
                if user_resource["user"] == user:
                    user_resource["accepted"] = True
                    user_resource["rate"] = rate
                    break
            assert resource_index >= 0
            yield self._db.study_reward.save(reward)
            yield self.accept_reward(reward, resource_index)

    @coroutine
    def accept_reward(self, reward, resource_index, auto_pass_audit=True):
        best_upload = reward["resource_list"][resource_index]
        uid = best_upload["uid"]
        user = best_upload["user"]
        nick_name = best_upload["nick_name"]
        yield self._fb_manager.reward_accepted(uid, reward["id"], reward["total_fb"])
        self._user_msg_center.add_msg(user, u'恭喜您，您上传的资源获得了"%s"悬赏 %d F！' % (reward["university"]+"-"+reward["college"]+"-"+reward["course"]+"-"+reward["tag"], reward["total_fb"]))

        resource = {"is_reward": True, "reward_id": reward["id"]}
        for key in ("course", "teacher", "university", "college", "description", "tag", "year"):
            resource[key] = reward[key]
        futures = []
        for res_dict in best_upload["uploaded_resources"]:
            res = resource.copy()
            for key in ("filename", "file_size", "file_hash", "file_key", "preview_key", "preview_hash", "download_link"):
                if key in res_dict:
                    res[key] = res_dict[key]
            res["resource_name"] = res["filename"]
            res["rate"] = best_upload["rate"]
            future = self._study_res_man.upload_resource(res, user, uid, auto_pass_audit, nick_name)
            if isinstance(future, Future):
                futures.append(future)
        if futures:
            yield futures

    @coroutine
    def get_a_reward(self, reward_id):
        reward = yield self._db.study_reward.find_one({"id": reward_id},{"_id":0})
        raise Return(reward)

    @coroutine
    def process_expired_reward(self):
        cursor = self._db.study_reward.find({"status": self.STATUS["pending"], "index_expired_date": {"$lt": long(time())}})
        while (yield cursor.fetch_next):
            reward = cursor.next_object()
            if reward["resource_list"]:
                reward["status"] = self.STATUS["over"]
                yield self.accept_reward(reward, 0, False)
            else:
                reward["status"] = self.STATUS["expired"]
            yield self._db.study_reward.save(reward)

        # yield self._db.study_reward.update({"status": self.STATUS["pending"], "resource_list": [], "index_expired_date": {"$lt": long(time())}},
        #                                               {"$set": {"status": self.STATUS["expired"]}})
        # reward = yield self._db.study_reward.find_and_modify({"status": self.STATUS["pending"], "resource_list":{"$ne": []},
        #                                                "index_expired_date": {"$lt": long(time())}},
        #                                               {"$set": {"resource_list.0.accepted": True, "status": self.STATUS["over"]}})
        # if reward:
        #     IOLoop.current().add_timeout(time() + 0.001, lambda: IOLoop.current().run_sync(self.process_expired_reward))
        # while True: # not allowed
        # for i in range(10000): # not allowed
        #     reward = yield future
        #     if reward is None: break
        #     yield self.accept_reward(reward, 0, False)

    @coroutine
    def user_related_reward(self, user, current_page=1):
        cursor = self._db.study_reward.find({"related_users": user}, {"_id":0}).sort([('index_ctime', self._DESCENDING),])
        total_num = yield cursor.count()
        cursor = cursor.limit(self.RES_PAGE_NUM_IN_MY_REWARD).skip((current_page-1)*self.RES_PAGE_NUM_IN_MY_REWARD)
        reward_list = yield cursor.to_list(None)
        raise Return([(total_num+self.RES_PAGE_NUM_IN_MY_REWARD-1)/self.RES_PAGE_NUM_IN_MY_REWARD, current_page, reward_list])




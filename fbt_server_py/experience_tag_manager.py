__author__ = 'bone'

from redis_handler import RedisHandler
from stop_words_manager import StopWordsManager
from redis_cluster_proxy import Redis
from tornado.escape import utf8


class QuestionTagManager(object):
    _REDIS_TAG_NUM_KEY = "experience_tag_num"
    _REDIS_EXP_LIST = "experience:list:"
    _REDIS_EXP_TAG_USERS = "experience:tag:users:"
    EXP_LIST_NUM = 200

    def __init__(self, redis_db=None, redis_cache=None):
        self._redis_db = redis_db or RedisHandler(tp=RedisHandler.type_db)
        self._redis_cache = redis_cache or Redis()
        self._stop_words_man = StopWordsManager()

    def add_question_tag(self, tags, tag_class):
        tag_class = utf8(tag_class)
        pipe = self._redis_db.pipeline()
        for tag in tags:
            tag = utf8(tag)
            pipe.hincrby(self._REDIS_TAG_NUM_KEY + ":" + tag_class,  tag, 1)
            pipe.hincrby(self._REDIS_TAG_NUM_KEY, tag_class, 1)
        pipe.execute()
        # what if sort by time or thumb num??? sort in memory???
    
    def remove_question_tag(self, tags, tag_class):
        tag_class = utf8(tag_class)
        pipe = self._redis_db.pipeline()
        for tag in tags:
            tag = utf8(tag)
            pipe.hdel(self._REDIS_TAG_NUM_KEY + ":" + tag_class,  tag)
            pipe.hincrby(self._REDIS_TAG_NUM_KEY, tag_class, -1)
        pipe.execute()

    def get_all_tags_by_class(self, tag_class):
        tag_class = utf8(tag_class)
        tag_num = self._redis_db.hgetall(self._REDIS_TAG_NUM_KEY + ":" + tag_class)
        if tag_num:
            return tag_num
        else:
            return {}

    def extract_tag_from_text(self, text, tag_class):
        tag_class = utf8(tag_class)
        ret = {"found_tags": [], "recommend_tags": []}
        extracted_words = self._stop_words_man.extract_words(text)
        if extracted_words:
            tags_num = self._redis_db.hmget(self._REDIS_TAG_NUM_KEY + ":" + tag_class, extracted_words)
            if tags_num:
                for i, tag_num in enumerate(tags_num):
                    if tag_num and tag_num.isdigit():
                        ret["found_tags"].append(extracted_words[i])
                    else:
                        ret["recommend_tags"].append(extracted_words[i])
        return ret

    # def join_experience_by_tags(self, tags):
    #     assert len(tags) > 0
    #     tags = [self._REDIS_TAG_NUM_KEY + ":" + utf8(tag_with_class) for tag_with_class in tags]
    #     ret = self._redis_db.sunion(tags)
    #     if not ret:
    #         return set()
    #     else:
    #         return ret

    # def insert_user_interested_experience(self, user, exp_id):
    #     key = self._REDIS_EXP_LIST + user
    #     pipe = self._redis_db.pipeline()
    #     pipe.lpush(key, exp_id)
    #     pipe.ltrim(key, 0, self.EXP_LIST_NUM-1)
    #     pipe.execute()

    # def get_user_interested_experience(self, user):
    #     key = self._REDIS_EXP_LIST + user
    #     ret = self._redis_db.lrange(key, 0, -1)
    #     if ret:
    #         return ret
    #     else:
    #         return []

    # def join_user_by_tags(self, tags_with_class):
    #     tags = [self._REDIS_EXP_TAG_USERS + utf8(tag_with_class) for tag_with_class in tags_with_class]
    #     ret = self._redis_db.sunion(tags)
    #     if not ret:
    #         return set()
    #     else:
    #         return ret

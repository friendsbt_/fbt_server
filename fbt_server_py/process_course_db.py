# coding: utf-8
import re
import io
import simplejson as json

def strip2(s):
    return s.decode("utf-8").strip().encode("utf-8")

def main():
    lines = [line.strip() for line in open('course2008.txt')]
    all_fields = [line.split() for line in lines]

    len_class1 = 3
    len_class2 = 5
    len_class3 = 7

    course_class1 = dict()
    course_class2 = dict()

    course_id_dict = dict()
    id_course_dict = dict()

    def is_number(s):
        return re.match(r'^\d+$', s)

    i = 0
    while i < len(all_fields):
        fields = all_fields[i]
        fields[0] = strip2(fields[0])
        if is_number(fields[0]):
            fields[1] = strip2(fields[1])
            if is_number(fields[0]):
                if len(fields[0]) == len_class1:
                    assert fields[1] not in course_id_dict
                    course_id_dict[fields[1]] = fields[0]
                elif len(fields[0]) == len_class2:
                    prefix = fields[0][:3]
                    course_prefix = id_course_dict[prefix]
                    course_id_dict[course_prefix+":"+fields[1]] = fields[0]
                elif len(fields[0]) == len_class3:
                    prefix = fields[0][:3]
                    course_prefix = id_course_dict[prefix]
                    prefix2 = fields[0][:5]
                    course_prefix2 = id_course_dict[prefix2]
                    course_id_dict[course_prefix+":"+course_prefix2+":"+fields[1]] = fields[0]
            assert fields[0] not in id_course_dict
            id_course_dict[fields[0]] = fields[1]
        i += 1

    i = 0
    while i < len(all_fields):
        top_course_id = all_fields[i][0]
        if len(top_course_id) == len_class1:
            top_course_name = all_fields[i][1]
            if top_course_name not in course_class1:
                course_class1[top_course_name] = []
            last_course_id = None
            while i+1 < len(all_fields) and ((all_fields[i+1][0][:3] == top_course_id) or
                   not is_number(all_fields[i+1][0])):
                i += 1
                if all_fields[i][0][:3] == top_course_id:
                    if len(all_fields[i][0]) == len_class2:
                        assert all_fields[i][1] not in course_class1[top_course_name]
                        course_class1[top_course_name].append(all_fields[i][1])
                        if all_fields[i][1] not in course_class2:
                            course_class2[all_fields[i][1]] = []
                    if len(all_fields[i][0]) == len_class3:
                        course_name = id_course_dict[all_fields[i][0][:5]]
                        assert all_fields[i][1] not in course_class2[course_name]
                        course_class2[course_name].append(all_fields[i][1])
                    last_course_id = all_fields[i][0][:5]
                else:
                    if len(all_fields[i]) < 2:
                        print "Warning, this line is invalid, pass it:",all_fields[i][0]
                        continue
                    all_fields[i][1] = strip2(all_fields[i][1])
                    m = re.search(r"见(\d+)",all_fields[i][1])
                    if not m:
                        print "WARNING: syntax error found. Pass line:", all_fields[i][0], all_fields[i][1]
                        continue
                    course_id = m.group(1)
                    assert course_id in id_course_dict
                    course_name = id_course_dict[course_id]
                    if len(course_id) == len_class2:
                        assert course_name not in course_class1[top_course_name]
                        course_class1[top_course_name].append(course_name)
                    elif len(course_id) == len_class3:
                        if last_course_id is None:
                            print "Warning: I dont know how to classfy this line:", all_fields[i][0], all_fields[i][1]
                        else:
                            #"林学:森林培育学:水土保持学"
                            prefix = last_course_id[:3]
                            course_prefix = id_course_dict[prefix]
                            prefix2 = last_course_id[:5]



                            course_prefix2 = id_course_dict[prefix2]
                            course_id_dict[course_prefix+":"+course_prefix2+":"+ course_name] = course_id

                            key = id_course_dict[last_course_id]
                            assert course_name not in course_class2[key]
                            course_class2[key].append(course_name)
                    else:
                        assert False
        i += 1

    assert len(course_class1) == 62
    assert len(id_course_dict) == 3532

    res = ["数学史", "数理逻辑与数学基础", "数论", "代数学", "代数几何学",
           "几何学", "拓扑学", "数学分析", "非标准分析", "函数论", "常微分方程",
           "偏微分方程", "动力系统", "积分方程", "泛函分析", "计算数学", "概率论",
           "数理统计学", "应用统计数学", "运筹学", "组合数学", "离散数学", "模糊数学",
           "计算机数学", "应用数学", "数学其他学科"]
    assert course_class1["数学"] == res

    res2 = [ "初等数论", "解析数论", "代数数论", "超越数论", "丢番图逼近", "数的几何",
             "概率数论", "计算数论", "数论其他学科", ]
    assert res2 == course_class2["数论"]


    assert course_class2["半导体技术"] == [ "半导体测试技术", "半导体材料",
                                       "半导体器件与技术", "传感器技术", "集成电路技术",
                                       "半导体加工技术", "半导体技术其他学科", ]

    assert course_class1["交通运输工程"] == [ "道路工程", "公路运输", "铁路运输",
                                            "水路运输", "船舶、舰船工程", "航空运输",
                                            "交通运输系统工程", "交通运输安全工程", "交通运输经济学", "交通运输工程其他学科"]


    assert course_class2["环境科学技术基础学科"] == [ "环境物理学", "环境化学", "环境生物学", "环境气象学",
        "环境地学", "环境生态学", "环境毒理学", "自然环境保护学",
        "环境管理学", "环境法学", "环境科学技术基础学科其他学科", ]

    assert course_class1["环境科学技术及资源科学技术"] == ["环境科学技术基础学科","环境医学","环境经济学","环境学",
    "环境工程学","资源科学技术","环境科学技术及资源科学技术其他学科",]
 
    assert "林学:森林培育学:水土保持学" in course_id_dict

    print "OK. all data pass test."

    top_classes ={
        110:"数学",
        120:"信息科学与系统科学",
        130:"力学",
        140:"物理学",
        150:"化学",
        160:"天文学",
        170:"地球科学",
        180:"生物学",
        190:"心理学",
        210:"农学",
        220:"林学",
        230:"畜牧、兽医科学",
        240:"水产学",
        310:"基础医学",
        320:"临床医学",
        330:"预防医学与公共卫生学",
        340:"军事医学与特种医学",
        350:"药学",
        360:"中医学与中药学",
        410:"工程与技术科学基础学科",
        413:"信息与系统科学相关工程与技术",
        416:"自然科学相关工程与技术",
        420:"测绘科学技术",
        430:"材料科学",
        440:"矿山工程技术",
        450:"冶金工程技术",
        460:"机械工程",
        470:"动力与电气工程",
        480:"能源科学技术",
        490:"核科学技术",
        510:"电子与通信技术",
        520:"计算机科学技术",
        530:"化学工程",
        535:"产品应用相关工程与技术",
        540:"纺织科学技术",
        550:"食品科学技术",
        560:"土木建筑工程",
        570:"水利工程",
        580:"交通运输工程",
        590:"航空、航天科学技术",
        610:"环境科学技术及资源科学技术",
        620:"安全科学技术",
        630:"管理学",
        710:"马克思主义",
        720:"哲学",
        730:"宗教学",
        740:"语言学",
        750:"文学",
        760:"艺术学",
        770:"历史学",
        780:"考古学",
        790:"经济学",
        810:"政治学",
        820:"法学",
        830:"军事学",
        840:"社会学",
        850:"民族学与文化学",
        860:"新闻学与传播学",
        870:"图书馆、情报与文献学",
        880:"教育学",
        890:"体育科学",
        910:"统计学",
    }
    class0= [("自然科学",110,190),
             ("农业科学",210,240),
             ("医药科学",310,360),
             ("工程与技术科学",410,630),
             ("人文与社会科学",710,910)]

    overview = []
    for c in class0:
        subjects = []
        for i in range(c[1], c[2]+1):
            if i in top_classes:
                assert top_classes[i] in course_class1
                subjects.append(top_classes[i])
        overview.append([c[0], subjects])

    json_file = "static/json/course.json"
    with io.open(json_file, 'w', encoding='utf8') as file:
        content=json.dumps({"overview": overview,
                            "course_class1": course_class1,
                            "course_class2": course_class2,
                            #"id_course_dict": id_course_dict,
                            "course_id_dict": course_id_dict,
                            }, ensure_ascii=False, encoding='utf8', indent=2)
        file.write(unicode(content))

    with open(json_file,'r') as f:
         json.load(f)

    print "process OK"

if __name__ == "__main__":
    main()

# -*- coding: utf-8 -*-
__author__ = 'bone-lee'

import motorclient

from tornado import gen
from datetime import datetime
from time import time
from tornado.escape import to_unicode

class BaseCommentManager(object):
    _DESCENDING = -1
    RES_PAGE_NUM = 25

    def __init__(self, db = None):
        if db:
            self._db = db
        else:
            self._db = motorclient.fbt

    def get_collection(self):
        raise NotImplementedError("Please implement get_collection function.")

    @gen.coroutine
    def post_comment(self, id, comment, user_name, nick_name, uid, user_icon):
        yield self.get_collection().insert({"id": id, "content": comment, "user": user_name, "uid": uid,
                                            "nick_name": nick_name, "user_icon": user_icon,"index_ctime": long(time()),
                                            "ctime": datetime.now().strftime('%Y-%m-%d %H:%M')})

    @gen.coroutine
    def get_comments(self, id, current_page = 1):
        cursor = self.get_collection().find({"id": id}, {"_id":0})
        total_comment_num = yield cursor.count()
        cursor = cursor.sort([('index_ctime', self._DESCENDING),]).limit(self.RES_PAGE_NUM).skip((current_page-1)*self.RES_PAGE_NUM)
        comment_list = yield cursor.to_list(None)
        # comment_list = []
        # while (yield cursor.fetch_next):
        #     res = cursor.next_object()
        #     comment_list.append(res)
        raise gen.Return([(total_comment_num+self.RES_PAGE_NUM-1)/self.RES_PAGE_NUM, current_page, comment_list])

class ResourceCommentManager(BaseCommentManager):
    def __init__(self, db = None):
        BaseCommentManager.__init__(self, db)

    def get_collection(self):
        return self._db.resource_comments

class ResourceCommentManagerP2P(BaseCommentManager):
    def __init__(self, db = None):
        BaseCommentManager.__init__(self, db)

    def get_collection(self):
        return self._db.comments2

class CourseCommentManager(BaseCommentManager):
    def __init__(self, db = None):
        BaseCommentManager.__init__(self, db)

    def get_collection(self):
        return self._db.course_comments

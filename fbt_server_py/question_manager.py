# -*- coding: utf-8 -*-
__author__ = 'bone'

"""
table questions:
    reply_list: [{"pros": 0, "cons": 0, "content": str, "uid": uid,
                        "ctime": seconds from 1970.1.1, "id": uuid, "comment_num": int}]
    reply_num: int
table quest_replys:
    id: uuid
    supporters: [uid]
    protesters: [uid]
    comments: [{"uid": uid, "ctime": seconds from 1970.1.1, "content": str, "reply_to_uid": if None, it means the comment to the replyer}]
"""

import motorclient
from util import generate_pkey
# from fb_manager import FBCoinManager
from coin_manager import CoinManager
from university_db import UniversityDB

from tornado.gen import coroutine
from tornado.gen import Return
from datetime import datetime
from time import time
from bson.objectid import ObjectId

QUESTION_VIEW_REPLY_NUM = 10
REPLY_VIEW_COMMENT_NUM = 10

class QuestionManager(object):
    PAGE_NUM = 15
    _DESCENDING = -1

    def __init__(self, db=None, fb_manager=None, university_db=None):
        if db is None:
            self._db = motorclient.fbt
        else:
            self._db = db
        if fb_manager:
            self._fb_manager = fb_manager
        else:
            self._fb_manager = CoinManager()
        if university_db:
            self.university_db = university_db
        else:
            self.university_db = UniversityDB()

    def gen_college_id(self, university, college):
        return str(self.university_db.get_university_id(university)) +":"+str(self.university_db.get_college_id(university,college))

    @coroutine
    def issue_a_question(self, question, user, uid, nick_name, user_icon=None):
        assert "course_name" in question and "course_id" in question and "title" in question and "content" in question
        question["college_id"] = self.gen_college_id(question["university"], question["college"])
        question["publisher"] = user
        question["publisher_nick"] = nick_name
        question["publisher_uid"] = uid
        question["publisher_img"] = user_icon
        ctime = datetime.now().strftime('%Y-%m-%d %H:%M')
        #question["followers"] = [{"uid": uid, "user": user, "nick_name": nick_name,
        #                           "ctime": ctime, "index_ctime": long(time()), "img": user_icon}]
        """
        question["followers"] = []
        question["reply_list"] = []
        """
        question["reply_num"] = 0
        question["index_ctime"] = question["last_reply_ctime"]  = long(time())
        question["ctime"] = ctime
        #question["id"] = generate_pkey()
        yield self._db.questions.insert(question)
        raise Return(str(question["_id"]))

    @coroutine
    def get_question_overview(self, course_id, current_page=1, sort_by="0"):
        if '0' == sort_by:
            sort_by = "index_ctime"
        else:
            sort_by = "last_reply_ctime"
        #cursor = self._db.questions.find({"course_id": course_id}, {"_id": 0, "reply_list": 0}).sort([(sort_by, self._DESCENDING)])
        #total_num = yield cursor.count()
        cursor = self._db.questions.find({"course_id": course_id}, {"reply_list": 0}).sort([(sort_by, self._DESCENDING)])
        cursor = cursor.limit(self.PAGE_NUM).skip((current_page-1)*self.PAGE_NUM)
        question_list = yield cursor.to_list(None)
        for question in question_list:
            question["_id"] = str(question["_id"])
        #raise Return([(total_num+self.PAGE_NUM-1)/self.PAGE_NUM, current_page, reward_list])
        raise Return(question_list)

    # sort_by: 0 for ctime (the natural order), 1 for pros
    @coroutine
    def reply_view(self, reply_list, page, sort_by="1"):
        total_page = (len(reply_list) + QUESTION_VIEW_REPLY_NUM - 1) / QUESTION_VIEW_REPLY_NUM
        start = (page - 1) * QUESTION_VIEW_REPLY_NUM
        end = start + QUESTION_VIEW_REPLY_NUM
        if "1" == sort_by:
            reply_list.sort(key=lambda x: x["pros"], reverse=True)
        reply_list = reply_list[start:end]
        if reply_list:
            uid_list = list()
            uid_reply_index_dict = dict()
            for idx, reply in enumerate(reply_list):
                uid = reply.pop("uid")
                uid_list.append(uid)
                uid_reply_index_dict[uid] = idx
            user_list = yield self._db.users.find({"uid": {"$in": uid_list}}, {"user": 1, "icon": 1, "nick_name": 1, "uid": 1}).to_list(None)
            for user in user_list:
                user.pop('_id')
                idx = uid_reply_index_dict[user["uid"]]
                user.pop("uid")
                reply_list[idx].update(user)

        raise Return((total_page, page, reply_list))

    @coroutine
    def question_view(self, question_id):
        res = yield self._db.questions.find_one({"_id": ObjectId(question_id)}, {"_id": 0})
        if res:
            if "reply_list" in res:
                total_page, page, reply_list = yield self.reply_view(res["reply_list"], 1)
                res["reply_list"] = reply_list
                res["current_page"] = page
                res["total_page"] = total_page
            raise Return(res)
        else:
            raise Return(None)

    @coroutine
    def reply_view_more(self, question_id, page, sort_by):
        res = yield self._db.questions.find_one({"_id": ObjectId(question_id)})
        if res:
            if "reply_list" in res:
                result = dict()
                total_page, page, reply_list = yield self.reply_view(res["reply_list"], page, sort_by)
                result["total_page"] = total_page
                result["reply_list"] = reply_list
                result["page"] = page
                raise Return(result)
        raise Return(None)

    @coroutine
    def pros(self, question_id, reply_id, uid):
        reply = yield self._db.quest_replys.find_one({"id": reply_id}, {"comments": 0, "protesters": 0})
        val = 0
        if reply:
            if ("supporters" not in reply) or uid not in reply["supporters"]:
                if "supporters" not in reply:
                    reply["supporters"] = [uid]
                else:
                    reply["supporters"].append(uid)
                val = 1
        else:
            val = 1
            reply = {"id": reply_id, "supporters": [uid]}
        if val:
            yield self._db.quest_replys.save(reply)
            yield self._db.questions.update({"_id": ObjectId(question_id), "reply_list.id": reply_id}, {"$inc": {"reply_list.$.pros": 1}})
        raise Return(val)

    @coroutine
    def cons(self, question_id, reply_id, uid):
        reply = yield self._db.quest_replys.find_one({"id": reply_id}, {"comments": 0, "supporters": 0})
        val = 0
        if reply:
            if ("protesters" not in reply) or uid not in reply["protesters"]:
                if "protesters" not in reply:
                    reply["protesters"] = [uid]
                else:
                    reply["protesters"].append(uid)
                val = 1
        else:
            val = 1
            reply = {"id": reply_id, "protesters": [uid]}
        if val:
                yield self._db.quest_replys.save(reply)
                yield self._db.questions.update({"_id": ObjectId(question_id), "reply_list.id": reply_id}, {"$inc": {"reply_list.$.cons": 1}})
        raise Return(val)

    @coroutine
    def reply_question(self, question_id, uid, content):
        reply_ctime = long(time())
        reply_id = generate_pkey()
        reply = {"id": reply_id, "pros": 0, "cons": 0, "content": content, "uid": uid, "ctime": reply_ctime, "comment_num": 0}
        yield self._db.questions.update({"_id": ObjectId(question_id)}, {"$push": {"reply_list": reply}, "$set": {"last_reply_ctime": reply_ctime},
                                                               "$inc": {"reply_num": 1}})
        raise Return(reply_id)

    @coroutine
    def comment_reply(self, question_id, reply_id, uid, to_uid, content):
        comment_time = long(time())
        comment = {"uid": uid, "ctime": comment_time, "content": content}
        if to_uid:
            comment["reply_to_uid"] = to_uid
        yield self._db.quest_replys.update({"id": reply_id}, {"$push": {"comments": comment}}, upsert=True)
        yield self._db.questions.update({"_id": ObjectId(question_id)}, {"$inc": {"comment_num": 1}})

    @coroutine
    def comment_view(self, reply_id, page):
        res = yield self._db.quest_replys.find_one({"id": reply_id})
        if res:
            if "comments" in res:
                result = dict()
                comments = res["comments"]
                result["page"] = page
                result["total_page"] = (len(comments) + REPLY_VIEW_COMMENT_NUM - 1) / REPLY_VIEW_COMMENT_NUM
                start = (page - 1) * REPLY_VIEW_COMMENT_NUM
                end = start + REPLY_VIEW_COMMENT_NUM
                comments = comments[start:end]
                if comments:
                    uid_set = set()
                    for idx, comment in enumerate(comments):
                        uid = comment["uid"]
                        uid_set.add(uid)
                        if "reply_to_uid" in comment:
                            to_uid = comment["reply_to_uid"]
                            uid_set.add(to_uid)

                    user_list = yield self._db.users.find({"uid": {"$in": list(uid_set)}}, {"user": 1, "icon": 1, "nick_name": 1, "uid": 1}).to_list(None)
                    user_dict = dict()
                    for user in user_list:
                        uid = user.pop("uid")
                        user.pop("_id")
                        user_dict[uid] = user
                    for comment in comments:
                        uid = comment.pop("uid")
                        comment.update(user_dict[uid])
                        if "reply_to_uid" in comment:
                            to_uid = comment["reply_to_uid"]
                            comment["reply_to_uid"] = user_dict[to_uid]

                result["comments"] = comments
                raise Return(result)

        raise Return(None)
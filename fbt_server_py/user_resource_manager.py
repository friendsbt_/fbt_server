# -*- coding: utf-8 -*-
__author__ = 'bone-lee'

from util import seconds_of_today_at_wee_hours

from tornado import gen
from datetime import datetime
from time import time
import motorclient


class UserResourceManager(object):
    _DESCENDING = -1
    RES_PAGE_NUM = 16

    def __init__(self, db = None):
        if db:
            self._db = db
        else:
            self._db = motorclient.fbt

    @gen.coroutine
    def clear_db_just_for_test(self):
        yield self._db.users_study_resource.remove({})

    @gen.coroutine
    def add_to_resource_list(self, file_id, user, file_size, resource_name, university, college, icon=None):
        res = yield self._db.users_study_resource.find_and_modify({"file_id": file_id},{"$set": {"downloader": user, "college": college,
                                        "resource_name": resource_name, "img": icon,
                                        "file_size": file_size, "university": university,"index_ctime": long(time()),
                                        "ctime": datetime.now().strftime('%Y-%m-%d %H:%M')}}, True)
        raise gen.Return(res)

    @gen.coroutine
    def get_user_download(self, user, current_page=1):
        cursor = self._db.users_study_resource.find({"downloader": user}, {"_id":0,"resource_name":1, "img":1, "file_id": 1, "university": 1,"file_size":1, "ctime":1})
        total_comment_num = yield cursor.count()
        cursor = cursor.sort([('index_ctime', self._DESCENDING),]).limit(self.RES_PAGE_NUM).skip((current_page-1)*self.RES_PAGE_NUM)
        download_list = yield cursor.to_list(None)
        for f in download_list:
            f["filename"]=f["resource_name"]
        raise gen.Return([(total_comment_num+self.RES_PAGE_NUM-1)/self.RES_PAGE_NUM, current_page, download_list])

    @gen.coroutine
    def get_today_download(self):
        cursor = self._db.users_study_resource.find({"index_ctime": {"$gt": seconds_of_today_at_wee_hours()}}, {"_id": 0})
        res = yield cursor.to_list(None)
        raise gen.Return(res)

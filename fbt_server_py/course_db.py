# -*- coding: utf-8 -*-
from singleton import singleton
from constant import CDN_URL

import simplejson as json
import os
from codecs import open as open
from tornado.escape import to_unicode

@singleton
class CourseDB(object):
    img_relative_path = "static/images/course/"
    default_icon = "0.jpg"
    default_course = "其他学科"

    def __init__(self):
        dir = os.path.dirname(os.path.abspath(__file__))
        self.img_path = os.path.join(dir, self.img_relative_path)
        json_file = os.path.join(dir,  "static/json/course.json")
        with open(json_file,'r') as f:
            data = json.load(f)
            self._course_class_overview = data["overview"]
            self._course_class1 = data["course_class1"]
            self._course_class2 = data["course_class2"]
            self._course_id_dict = data["course_id_dict"]

            self._top_course_img_dict = {}
            for k in self._course_class1.keys():
                course_id = self._course_id_dict[k]
                file_path = self.img_path + course_id +".jpg"
                if os.path.isfile(file_path):
                    self._top_course_img_dict[k] = CDN_URL + self.img_relative_path + course_id +".jpg"
                else:
                    self._top_course_img_dict[k] = self.get_default_img()

    # def _trimmed_course(self, course_name):
    #     course_name = to_unicode(course_name)
    #     course_class = course_name.split(":")
    #     if course_class[-1] == to_unicode(self.default_course):
    #         course_name = ":".join(course_class[:-1])
    #     return course_name

    def get_course_id(self, course_name):
        course_name = to_unicode(course_name)
        return str(abs(hash(course_name)))+"_"+str(len(course_name))
        # 数学:代数学:群论
        # course_name = self._trimmed_course(course_name)
        # if course_name not in self._course_id_dict:
        #     return hash(course_name)
        # else:
        #     return self._course_id_dict[course_name]

    # def is_valid_course(self, course_name):
    #     course_name = self._trimmed_course(course_name)
    #     return course_name in self._course_id_dict

    def get_course_img(self, course_class_name):
        # course_class_name such as "信息科学与系统科学"
        course_class_name = to_unicode(course_class_name)
        if course_class_name in self._top_course_img_dict:
            return self._top_course_img_dict[course_class_name]
        else:
            return self.get_default_img()

    def get_default_img(self):
        return CDN_URL + self.img_relative_path + self.default_icon

    def get_course_overview(self):
        return self._course_class_overview

    # def get_course_list(self, class1, class2=None):
    #     try:
    #         if class2:
    #             class2 = to_unicode(class2)
    #             ret = self._course_class2[class2]
    #         else:
    #             class1 = to_unicode(class1)
    #             ret = self._course_class1[class1]
    #     except KeyError as e:
    #         ret = []
    #     if ret:
    #         return ret
    #     else:
    #         return [self.default_course,]

if __name__ == '__main__':
    # test
    c = CourseDB()
    print c.get_course_img("生物学:免疫学:免疫治疗学")
    print c.get_course_id("生物学:免疫学:免疫治疗学")

    print c.get_course_img(u"生物学")
    print c.get_course_id(u"生物学")
    print "*****************************"
    for k,v in c.get_course_overview():
        print k
    # print "*****************************"
    # for k in c.get_course_list("生物学"):
    #     print k
    # print "*****************************"
    # for k in c.get_course_list("生物学","免疫学"):
    #     print k
    # assert c.is_valid_course("预防医学与公共卫生学:劳动卫生学")
    # assert c.is_valid_course("预防医学与公共卫生学:劳动卫生学:其他学科")

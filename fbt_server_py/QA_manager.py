# -*- coding: utf-8 -*-

__author__ = 'bone'

from users_manager import UserManager
from abstract_searcher import AbstractSearcher
from util import generate_pkey
from coin_manager import CoinManager
from experience_tag_manager import QuestionTagManager
from es_search import ESSearch
from constant import ES_HOST, ES_PORT
from es_mapping import question_analyze_fields, question_index_mapping, question_none_analyze_fields
from redis_cluster_proxy import Redis
from util import seconds_of_today_at_wee_hours
import constant
import mongoclient

import motorclient
from tornado.gen import coroutine
from tornado.gen import Return
from university_db import UniversityDB
from datetime import datetime
from time import time
import simplejson as json
from tornado.escape import utf8, to_unicode
from heapq import heappush, heappop
from datetime import timedelta
from random import randint


class UserAnswerThumbUpRecorder(object):
    TAG_KEY = 'top-user:tag:'

    def __init__(self, redis_db=None):
        self._redis = redis_db or Redis(Redis.FOR_DB)

    def thumb_up_for_user(self, user, tags_with_class, num=1):
        pipe = self._redis.pipeline()
        for tag_with_class in tags_with_class:
            key = self.TAG_KEY + utf8(tag_with_class)
            pipe.zincrby(key, user, num)
        pipe.execute()

    def top_users_by_tag(self, tag_with_class):
        key = self.TAG_KEY + utf8(tag_with_class)
        ret = self._redis.zrevrange(key, 0, 30, withscores=True)
        if ret:
            return ret
        else:
            return []


class UserInterestedTagRecorder(object):
    TAG_KEY = 'user:tag:interested:'

    def __init__(self, redis_db=None):
        self._redis = redis_db or Redis(Redis.FOR_DB)

    def insert_interested_tag(self, user, tags_with_class):
        key = self.TAG_KEY + user
        pipe = self._redis.pipeline()
        for tag_with_class in tags_with_class:
            pipe.zincrby(key, utf8(tag_with_class), 1)
        pipe.execute()

    def top_users_interested_tag(self, user):
        key = self.TAG_KEY + user
        ret = self._redis.zrevrange(key, 0, 20, withscores=False)
        if ret:
            return ret
        else:
            return []


class QAManager(AbstractSearcher):
    TAG_CLASS = constant.TAG_CLASS
    STAR_CLASS = constant.STAR_CLASS
    SORT_BY = {"time": 0, "hot_value": 1}
    SCORE_POINTES = {"view": 1, "comment": 1, "thumb_up": 2, "reward": 3, "answer": 5, "collect": 10}
    PAGE_NUM = 15
    _DESCENDING = -1
    MAX_SORT_CNT = 200
    NOT_SET_TAG = -1

    def __init__(self, db=None, fb_manager=None, user_manager=None, exp_tag_manager=None,
                 redis_cache=None, redis_db=None, es_host=None, es_port=None):
        self._db = db or motorclient.fbt
        self._fb_manager = fb_manager or CoinManager()
        self._user_tag_manager = user_manager or UserManager()
        self._redis_cache = redis_cache or Redis()
        super(QAManager, self).__init__(self._redis_cache)
        self._exp_tag_manager = exp_tag_manager or QuestionTagManager()
        self._university_db = UniversityDB()

        self._question_searcher = ESSearch(host=es_host or ES_HOST, port=es_port or ES_PORT,
                                           index_name="question_index",
                                           type_name="question",
                                           analyze_fields=question_analyze_fields,
                                           none_analyze_fields=question_none_analyze_fields,
                                           index_mapping=question_index_mapping)

        self._thumb_up_recorder = UserAnswerThumbUpRecorder(redis_db or Redis(Redis.FOR_DB))
        self._user_interested_tag_manager = UserInterestedTagRecorder(redis_db or Redis(Redis.FOR_DB))

    def insert_user_interested_tags(self, user, tags_with_class):
        self._user_interested_tag_manager.insert_interested_tag(user, tags_with_class)

    def is_valid_tag_class(self, class2):
        class2 = to_unicode(class2)
        return class2 in self.TAG_CLASS

    @coroutine
    def edit_question(self, question2, user, uid, nick_name, user_icon=None, university=None, college=None, user_desc="", star_info=None, real_name=None):
        assert "id" in question2 and "content" in question2
        question = yield self._db.questions.find_one({"id": question2["id"]})
        if question and question["publisher"] == user:
            question["university"] = university
            question["user_description"] = user_desc
            question["university_id"] = self._university_db.get_university_id(university)
            question["college"] = college
            question["publisher"] = user
            question["publisher_star_info"] = star_info or []
            question["publisher_real_name"] = real_name
            question["publisher_nick"] = nick_name
            question["publisher_uid"] = uid
            question["publisher_img"] = user_icon
            question["content"] = question2["content"]
            yield self._db.questions.save(question)
            yield self._question_searcher.update_field(question["id"], "content", question["content"])

    @coroutine
    def move_tag(self, tag_with_class, class_to_merge):
        tag_with_class = utf8(tag_with_class)
        class_to_merge = to_unicode(class_to_merge)
        assert ":" in tag_with_class
        assert self.is_valid_tag_class(class_to_merge)
        class2, tag = tag_with_class.split(":")
        assert self.is_valid_tag_class(class2)
        cursor = self._db.questions.find({"tags_with_class": tag_with_class})
        questions = yield cursor.to_list(None)
        for question in questions:
            self._exp_tag_manager.remove_question_tag(question["tags"], question["class2"])
            question["class2"] = class_to_merge
            question["index_class"] = self.TAG_CLASS[class_to_merge]
            tags_with_class = [class_to_merge + ":" + tag for tag in question["tags"]]
            question["tags_with_class"] = tags_with_class
            self._exp_tag_manager.add_question_tag(question["tags"], class_to_merge)
            yield self._db.questions.save(question)
        cursor = self._db.answers.find({"class2": class2, "tags": tag})
        answers = yield cursor.to_list(None)
        for answer in answers:
            answer["class2"] = class_to_merge
            answer["index_class"] = self.TAG_CLASS[class_to_merge]
            yield self._db.answers.save(answer)
        raise Return([len(questions), len(answers)])


    @coroutine
    def post_question(self, question, user, uid, nick_name, user_icon=None, university=None, college=None, user_desc="", star_info=None, real_name=None, sync=False):
        assert "tags" in question and "title" in question \
               and "content" in question
        if "class" in question: question["class2"] = question["class"]
        question["index_class"] = self.TAG_CLASS[question["class2"]]
        question["university"] = university
        question["university_id"] = self._university_db.get_university_id(university)
        question["college"] = college
        question["publisher"] = user
        question["answer_users"] = []
        question["publisher_img"] = user_icon
        question["user_description"] = user_desc
        question["publisher_nick"] = nick_name
        question["publisher_real_name"] = real_name
        question["publisher_star_info"] = star_info or []
        question["publisher_uid"] = uid
        question["reply_num"] = 0
        question["comment_num"] = 0
        question["comment_list"] = []
        question["best_answer"] = {}
        question["thumb_up_num"] = 0
        tags_with_class = [question["class2"] + ":" + tag for tag in question["tags"]]
        question["tags_with_class"] = tags_with_class
        question["thumb_up_users"] = []
        question["thanked_users"] = []
        question["thanks_coin"] = 0
        question["index_ctime"] = long(time() * 1000)
        ctime = datetime.now().strftime('%Y-%m-%d %H:%M')
        if sync:
            fake_time = datetime.now()+timedelta(days=randint(0, 0), hours=randint(0, 12),
                                                 minutes=randint(0, 59), seconds=randint(0, 59))
            ctime = fake_time.strftime('%Y-%m-%d %H:%M')
        question["ctime"] = ctime
        question["id"] = generate_pkey()

        question["hot_value"] = 0
        question["points"] = 0
        question["views"] = 0
        question["collected_num"] = 0

        if sync:
            db = mongoclient.fbt
            db.questions.insert(question)
        else:
            yield self._db.questions.insert(question)
        self._exp_tag_manager.add_question_tag(question["tags"], question["class2"])
        yield self.record_user_posted_question(user, question["id"], sync)

        yield self._question_searcher.insert(question["id"], question)

        self.add_to_latest_questions(question)
        raise Return(question["id"])

    def add_to_latest_questions(self, question):
        sort_by = "index_ctime"
        cache_key = "questions:overview:" + sort_by
        ret_str = self._redis_cache.get(cache_key)
        if ret_str:
            total_page, cur_page, question_list = json.loads(ret_str)
            if "_id" in question:
                del question["_id"]
            question_list.insert(0, question)
            total_page = (len(question_list) + self.PAGE_NUM - 1) / self.PAGE_NUM
            ret = [total_page, cur_page, question_list]
            self._redis_cache.setex(cache_key, 10 * 60, json.dumps(ret))

    @coroutine
    def edit_answer(self, answer2, user, uid, nick_name, user_icon=None, university=None, college=None, user_desc="", star_info=None, real_name=None):
        assert "id" in answer2 and "content" in answer2
        answer = yield self._db.answers.find_one({"id": answer2["id"]})
        if answer and answer["publisher"] == user:
            answer["university"] = university
            answer["user_description"] = user_desc
            answer["university_id"] = self._university_db.get_university_id(university)
            answer["college"] = college
            answer["publisher"] = user
            answer["publisher_star_info"] = star_info or []
            answer["publisher_real_name"] = real_name
            answer["publisher_nick"] = nick_name
            answer["publisher_uid"] = uid
            answer["publisher_img"] = user_icon
            answer["content"] = answer2["content"]
            yield self._db.answers.save(answer)
            raise Return([answer["question_id"], answer["title"], "question_poster" in answer and answer["question_poster"] or None])
        raise Return([None, None, None])

    @coroutine
    def post_comment(self, QA_id, content, user, user_icon, university, real_name, reply_to_user=None, reply_to_user_icon=None, reply_to_user_university=None, reply_to_real_name=None, is_answer=False):
        if is_answer:
            collection = self._db.answers
        else:
            collection = self._db.questions
        QA = yield collection.find_one({"id": QA_id})
        if not QA:
            raise Return(None)
        if "comment_num" not in QA:
            QA["comment_num"] = 0
            QA["comment_list"] = []
        QA["comment_num"] += 1
        ctime = datetime.now().strftime('%Y-%m-%d %H:%M')
        comment = {"content": content, "from": {"user": user, "real_name": real_name, "user_icon": user_icon, "university": university},  "ctime": ctime, "to": {"user": reply_to_user or QA["publisher"], "user_icon": reply_to_user_icon or QA["publisher_img"], "university": reply_to_user_university or QA["university"], "real_name": reply_to_real_name or QA["publisher_real_name"]}}
        QA["comment_list"].append(comment)
        yield collection.save(QA)
        Q_id = ("question_id" in QA and QA["question_id"]) or QA_id
        yield self._db.questions.update({"id": Q_id}, {"$inc": {"points": self.SCORE_POINTES["comment"]}}, upsert=True)
        raise Return([Q_id, QA["title"]])

    @coroutine
    def post_answer(self, answer, user, uid, nick_name, user_icon=None, university=None, college=None, user_desc="", star_info=None, real_name=None, sync=False):
        assert "question_id" in answer and "content" in answer
        if not sync:
            question = yield self._db.questions.find_one({"id": answer["question_id"]})
        else:
            db = mongoclient.fbt
            question = db.questions.find_one({"id": answer["question_id"]})
        if not question:
            raise Return([None, None])
        if "answer_users" not in question:
            question["answer_users"] = []
        if user in question["answer_users"]:
            raise Return([None, question])
        question["answer_users"].append(user)
        answer["title"] = question["title"]
        answer["question_poster"] = question["publisher"]
        answer["tags"] = question["tags"]
        answer["class2"] = question["class2"]
        answer["index_class"] = self.TAG_CLASS[question["class2"]]
        answer["university"] = university
        answer["user_description"] = user_desc
        answer["university_id"] = self._university_db.get_university_id(university)
        answer["college"] = college
        answer["publisher"] = user
        answer["publisher_star_info"] = star_info or []
        answer["publisher_real_name"] = real_name
        answer["publisher_nick"] = nick_name
        answer["publisher_uid"] = uid
        answer["publisher_img"] = user_icon
        answer["thumb_up_num"] = 0
        answer["thumb_up_users"] = []
        answer["thanked_users"] = []
        answer["thanks_coin"] = 0
        answer["index_ctime"] = long(time() * 1000)
        answer["comment_num"] = 0
        answer["comment_list"] = []
        ctime = datetime.now().strftime('%Y-%m-%d %H:%M')
        if sync:
            fake_time = datetime.now()+timedelta(days=randint(0, 0), hours=randint(0, 12),
                                                 minutes=randint(0, 59), seconds=randint(0, 59))
            ctime = fake_time.strftime('%Y-%m-%d %H:%M')
        answer["ctime"] = ctime
        answer["id"] = generate_pkey()
        if not sync:
            yield self._db.answers.insert(answer)
        else:
            db.answers.insert(answer)

        tags = [question["class2"] + ":" + tag for tag in question["tags"]]
        if not sync:
            yield self._user_tag_manager.insert_my_tags(user, uid, tags)
        # self._exp_tag_manager.add_user_to_tag(user, tags)

        # update time
        question["index_ctime"] = long(time() * 1000)
        question["reply_num"] += 1
        if "points" not in question:
            question["points"] = 0
        question["points"] += self.SCORE_POINTES["answer"]
        if not question["best_answer"]:
            question["best_answer"] = {"id": answer["id"], "content": answer["content"], "thumb_up_num": 0, "thanks_coin":0,
                                       "user_description": answer["user_description"], "college": answer["college"], "university": answer["university"],
                                       "publisher": answer["publisher"], "publisher_star_info": answer["publisher_star_info"],
                                       "publisher_real_name": answer["publisher_real_name"], "publisher_nick": answer["publisher_nick"],
                                       "publisher_uid": answer["publisher_uid"], "publisher_img": answer["publisher_img"], "ctime": answer["ctime"],
                                       "comment_num": 0, "comment_list": [],
                                       "need_anonymous": ("need_anonymous" in answer and answer["need_anonymous"]) or False}

        be_invited = False
        if "invited_users" in question:
            invited_users_len = len(question["invited_users"])
            question["invited_users"] = [_ for _ in question["invited_users"] if _["user"] != user]
            be_invited = len(question["invited_users"]) != invited_users_len
        if not sync:
            yield self._db.questions.save(question)
        else:
            db.questions.save(question)

        if not sync:
            yield self._user_tag_manager.inc_answers_num(user)

        yield self.record_user_answered_question(user, question["id"], sync)

        question["be_invited"] = be_invited

        raise Return([answer["id"], question])

    @coroutine
    def get_answers(self, question_id, current_page=1, sort_by=SORT_BY["hot_value"], use_cache=True):
        question = yield self.get_question_by_id(question_id)
        if not question:
            raise Return([0, 1, []])

        yield self._db.questions.update({"id": question_id},
                                        {"$inc": {"points": self.SCORE_POINTES["view"], "views": 1}}, upsert=True)

        if self.SORT_BY["hot_value"] == sort_by:
            sort_by = "thumb_up_num"
        else:
            sort_by = "index_ctime"
        cache_key = "question:answers:" + question_id + ":" + sort_by
        answers_str = self._redis_cache.get(cache_key)
        if use_cache and answers_str:
            experience_list = json.loads(answers_str)
        else:
            cursor = self._db.answers.find({"question_id": question_id}, {"_id": 0, "thumb_up_users": 0}).sort(
                [(sort_by, self._DESCENDING)])
            experience_list = yield cursor.to_list(None)
            if experience_list:
                self._redis_cache.setex(cache_key, 2 * 60, json.dumps(experience_list))
        total_num = len(experience_list)
        raise Return([(total_num + self.PAGE_NUM - 1) / self.PAGE_NUM, current_page,
                      experience_list[(current_page-1)*self.PAGE_NUM:current_page*self.PAGE_NUM]])

    @coroutine
    def get_question_overview(self, sort_by=SORT_BY["hot_value"], use_cache=True):
        if self.SORT_BY["hot_value"] == sort_by:
            sort_by = "hot_value"
        else:
            sort_by = "index_ctime"
        cache_key = "questions:overview:" + sort_by
        ret_str = self._redis_cache.get(cache_key)
        if use_cache and ret_str:
            ret = json.loads(ret_str)
        else:
            condition = {}
            cursor = self._db.questions.find(condition, {"_id": 0, "thumb_up_users": 0, "best_answer": 0}).sort(
                [(sort_by, self._DESCENDING)])
            cursor = cursor.limit(self.PAGE_NUM)
            experience_list = yield cursor.to_list(None)
            experience_list = [_ for _ in experience_list if "hidden" not in _ or _["hidden"] == 0]
            total_num = len(experience_list)
            total_page = (total_num + self.PAGE_NUM - 1) / self.PAGE_NUM
            ret = [total_page, 1, experience_list]
            self._redis_cache.setex(cache_key, 10 * 60, json.dumps(ret))
        raise Return(ret)

    @coroutine
    def get_experience_overview(self, current_page=1, tag_class=None, sort_by=SORT_BY["hot_value"], university=None, use_cache=True):
        if self.SORT_BY["hot_value"] == sort_by:
            sort_by = "hot_value"
        else:
            sort_by = "index_ctime"
        cache_key = "questions:overview:" + sort_by
        if university:
            university_id = self._university_db.get_university_id(university)
            cache_key += str(university_id)
        if tag_class:
            cache_key += utf8(tag_class)
        ret_str = self._redis_cache.get(cache_key)
        if use_cache and ret_str:
            experience_list = json.loads(ret_str)
        else:
            condition = {}
            if tag_class:
                class2 = self.TAG_CLASS[to_unicode(tag_class)]
                condition["index_class"] = class2
            if university:
                condition["university_id"] = university_id
            cursor = self._db.questions.find(condition, {"_id": 0, "thumb_up_users": 0}).sort(
                                        [(sort_by, self._DESCENDING)]).limit(self.MAX_SORT_CNT)
            experience_list = yield cursor.to_list(None)
            experience_list = [_ for _ in experience_list if "hidden" not in _ or _["hidden"] == 0]
            if not experience_list:
                self._redis_cache.setex(cache_key, 10 * 60, json.dumps(experience_list))
        total_num = len(experience_list)
        total_page = (total_num + self.PAGE_NUM - 1) / self.PAGE_NUM
        ret = [total_page, current_page, experience_list[((current_page-1)*self.PAGE_NUM):(current_page*self.PAGE_NUM)]]
        raise Return(ret)


    @coroutine
    def get_recommended_question2(self, user, page=1, use_cache=True, tag_class=None, university=None):
        # TODO add unit test for it
        # tag_set = yield self._user_tag_manager.get_my_interested_tags(user)
        tag_set = self._user_interested_tag_manager.top_users_interested_tag(user)
        interested_tags = []
        tag_class = utf8(tag_class)
        for item in tag_set:
            tmp = item.split(":")
            if utf8(tmp[0]) == tag_class:
                interested_tags.append(item)
        if not interested_tags:
            raise Return([self.NOT_SET_TAG, page, []])
        if university:
            university = utf8(university)
            cache_key = "questions:recommended:" + university + ":" + " ".join(interested_tags)
        else:
            cache_key = "questions:recommended:" + " ".join(interested_tags)
        question_list_str = self._redis_cache.get(cache_key)
        if use_cache and question_list_str:
            question_list = json.loads(question_list_str)
        else:
            sorted_question_lists = yield [self.get_questions_by_tag(tag_with_class, university, use_cache) for tag_with_class in interested_tags]
            question_list = self.topK_of_sorted_lists(sorted_question_lists, self._user_tag_manager.MAX_USER_CNT, "hot_value", "id")
            self._redis_cache.setex(cache_key, 30 * 60, json.dumps(question_list))
        raise Return([(len(question_list) + self.PAGE_NUM - 1) / self.PAGE_NUM, page,
                      question_list[(page - 1) * self.PAGE_NUM:page * self.PAGE_NUM]])

    @coroutine
    def get_questions_by_tag(self, tag_with_class, university=None, use_cache=True):
        if university:
            university = utf8(university)
            cache_key = "questions:tagged:" + university + ":" + tag_with_class
        else:
            cache_key = "questions:tagged:" + tag_with_class
        users_list_str = self._redis_cache.get(cache_key)
        if use_cache and users_list_str:
            questions_list = json.loads(users_list_str)
        else:
            if university:
                university_id = self._university_db.get_university_id(university)
                cursor = self._db.questions.find({"tags_with_class": tag_with_class, "university_id": university_id}, {"_id": 0, "thumb_up_users": 0})
            else:
                cursor = self._db.questions.find({"tags_with_class": tag_with_class}, {"_id": 0, "thumb_up_users": 0})
            cursor = cursor.sort([("hot_value", self._DESCENDING)]).limit(self.MAX_SORT_CNT)
            questions_list = yield cursor.to_list(None)
            questions_list = [_ for _ in questions_list if "hidden" not in _ or _["hidden"] == 0]
            self._redis_cache.setex(cache_key, 30 * 60, json.dumps(questions_list))
        raise Return(questions_list)

    # TODO remove
    # @coroutine
    # def get_recommended_experience(self, user, page=1, use_cache=True, tag_class=None, university=None):
    #     interested_tags = yield self._user_tag_manager.get_my_interested_tags(user)
    #     interested_tag_list = []
    #     if tag_class:
    #         tag_class = utf8(tag_class)
    #         for tag in interested_tags:
    #             if tag_class in utf8(tag):
    #                 interested_tag_list.append(tag)
    #     else:
    #         interested_tag_list = interested_tags
    #     cache_key = "experience:recommended:" + " ".join(interested_tag_list)
    #     experience_list_str = self._redis_cache.get(cache_key)
    #     if use_cache and experience_list_str:
    #         experience_list = json.loads(experience_list_str)
    #     else:
    #         if interested_tag_list:
    #             experience_id_list = self._exp_tag_manager.join_experience_by_tags(interested_tag_list)
    #             if experience_id_list:
    #                 experience_list = yield [self.get_question_by_id(exp_id, use_cache) for exp_id in
    #                                          experience_id_list]
    #                 if experience_list:
    #                     # TODO thumb_up_num need hot algorithm
    #                     experience_list.sort(lambda x, y: cmp(y["thumb_up_num"], x["thumb_up_num"]))
    #                     self._redis_cache.setex(cache_key, 10 * 60, json.dumps(experience_list))
    #                 else:
    #                     experience_list = []
    #             else:
    #                 experience_list = []
    #         else:
    #             experience_list = []
    #     if university:
    #         university_id = self._university_db.get_university_id(university)
    #         experience_list = [_ for _ in experience_list if _["university_id"] == university_id]
    #     raise Return([(len(experience_list) + self.PAGE_NUM - 1) / self.PAGE_NUM, page,
    #                   experience_list[(page - 1) * self.PAGE_NUM:page * self.PAGE_NUM]])

    @coroutine
    def get_question_by_id(self, exp_id, use_cache=True):
        cache_key = "experience:id:" + exp_id
        experience = self._redis_cache.get(cache_key)
        if use_cache and experience:
            ret = json.loads(experience)
        else:
            ret = yield self._db.questions.find_one({"id": exp_id}, {"_id": 0, "thumb_up_users": 0})
            if ret:
                self._redis_cache.setex(cache_key, 5 * 60, json.dumps(ret))
        raise Return(ret)

    @coroutine
    def inc_thumb_up_num(self, exp_id, user, is_answer=False):
        if is_answer:
            exp = yield self._db.answers.find_one({"id": exp_id})
        else:
            exp = yield self._db.questions.find_one({"id": exp_id})
        if exp and user != exp["publisher"]:
            if user not in exp["thumb_up_users"]:
                exp["thumb_up_num"] += 1
                exp["thumb_up_users"].append(user)
                if is_answer:
                    # update best answer in question
                    question = yield self._db.questions.find_one({"id": exp["question_id"]})
                    assert question is not None
                    if "thumb_up_num" not in question["best_answer"] or "ctime" not in question["best_answer"] or question["best_answer"]["thumb_up_num"] < exp["thumb_up_num"]:
                        question["best_answer"]["id"] = exp["id"]
                        question["best_answer"]["ctime"] = exp["ctime"],
                        question["best_answer"]["content"] = exp["content"]
                        question["best_answer"]["thumb_up_num"] = exp["thumb_up_num"]
                        question["best_answer"]["thanks_coin"] = ("thanks_coin" in exp and exp["thanks_coin"]) or 0
                        question["best_answer"]["user_description"] = exp["user_description"]
                        question["best_answer"]["college"] = exp["college"]
                        question["best_answer"]["university"] = exp["university"]
                        question["best_answer"]["publisher"] = exp["publisher"]
                        question["best_answer"]["publisher_star_info"] = exp["publisher_star_info"]
                        question["best_answer"]["publisher_real_name"] = exp["publisher_real_name"]
                        question["best_answer"]["publisher_nick"] = exp["publisher_nick"]
                        question["best_answer"]["publisher_uid"] = exp["publisher_uid"]
                        question["best_answer"]["publisher_img"] = exp["publisher_img"]
                        question["best_answer"]["need_anonymous"] = ("need_anonymous" in exp and exp["need_anonymous"]) or False
                        question["best_answer"]["comment_num"] = "comment_num" in exp and exp["comment_num"] or 0
                        question["best_answer"]["comment_list"] = "comment_list" in exp and exp["comment_list"] or []
                    self._thumb_up_recorder.thumb_up_for_user(exp["publisher"], question["tags_with_class"])
                    if "points" not in question:
                        question["points"] = 0
                    question["points"] += self.SCORE_POINTES["thumb_up"]
                    yield self._db.questions.save(question)
                    yield self._db.answers.save(exp)
                    # yield self._question_searcher.update_field(question["id"], "thumb_up_num", question["thumb_up_num"])
                else:
                    yield self._db.questions.save(exp)
                yield self._user_tag_manager.inc_thumb_num(exp["publisher"])
                raise Return([True, exp["publisher"], exp["title"], ("question_id" in exp and exp["question_id"]) or exp["id"]])
            else:
                raise Return([False, exp["publisher"], exp["title"], ("question_id" in exp and exp["question_id"]) or exp["id"]])
        else:
            raise Return([False, None, None, None])

    @coroutine
    def get_top_users_by_tag(self, tag_with_class):
        tag_with_class = utf8(tag_with_class)
        cache_key = "users:top:by-tag:" + tag_with_class
        users_list_str = self._redis_cache.get(cache_key)
        if users_list_str:
            ret = json.loads(users_list_str)
        else:
            users_and_thumb_num = self._thumb_up_recorder.top_users_by_tag(tag_with_class)
            ret = yield [self._user_tag_manager.find_user(user, True) for user, thumb_num in users_and_thumb_num]
            for i, user_info in enumerate(ret):
                if "tags" in user_info:
                    if tag_with_class in user_info["tags"]:
                        post_num = user_info["tags"][tag_with_class]
                    else:
                        tag_with_class = to_unicode(tag_with_class)
                        if tag_with_class in user_info["tags"]:
                            post_num = user_info["tags"][tag_with_class]
                        else:
                            post_num = 1
                else:
                    post_num = 1
                user_info["wanted"] = {"thumb_num_of_tag": int(users_and_thumb_num[i][1]), "tag": tag_with_class, "posted_num": post_num}
            self._redis_cache.setex(cache_key, 60 * 60, json.dumps(ret))
        raise Return(ret)

    @coroutine
    def get_recommended_top_users(self, tag_class, tags):
        tag_class = utf8(tag_class)
        users = yield [self.get_top_users_by_tag(tag_class+":"+utf8(_)) for _ in tags]
        ret = {}
        for i, tag in enumerate(tags):
            ret[tag] = users[i]
        raise Return(ret)

    @coroutine
    def thanks(self, exp_id, user, user_nick, is_answer=False, how_much=0):
        if is_answer:
            exp = yield self._db.answers.find_one({"id": exp_id})
        else:
            exp = yield self._db.questions.find_one({"id": exp_id})
        if exp and user != exp["publisher"]:
            if "thanks_coin" not in exp:
                exp["thanks_coin"] = 0
            exp["thanks_coin"] += how_much
            if "thanked_users" not in exp or user_nick not in exp["thanked_users"]:
                if "thanked_users" not in exp:
                    exp["thanked_users"] = [user_nick]
                else:
                    exp["thanked_users"].append(user_nick)
                if is_answer:
                    yield self._db.answers.save(exp)
                else:
                    yield self._db.questions.save(exp)
            Q_id = ("question_id" in exp and exp["question_id"]) or exp["id"]
            yield self._db.questions.update({"id": Q_id}, {"$inc": {"points": self.SCORE_POINTES["reward"]}}, upsert=True)
            raise Return([True, exp["publisher"], exp["title"], Q_id, exp["publisher_uid"]])
        else:
            raise Return([False, None, None, None, None])

    @coroutine
    def record_user_answered_question(self, user, question_id, sync=False):
        if not sync:
            question = yield self._db.user_questions.find_one({"user": user})
            if question:
                if question_id not in question["answered"]:
                    question["answered"].insert(0, question_id)
                    yield self._db.user_questions.save(question)
            else:
                yield self._db.user_questions.insert({"user": user, "collected": [], "posted": [], "answered": [question_id]})
        else:
            db = mongoclient.fbt
            question = db.user_questions.find_one({"user": user})
            if question:
                if question_id not in question["answered"]:
                    question["answered"].insert(0, question_id)
                    db.user_questions.save(question)
            else:
                db.user_questions.insert({"user": user, "collected": [], "posted": [], "answered": [question_id]})

    @coroutine
    def record_user_posted_question(self, user, question_id, sync=False):
        if not sync:
            question = yield self._db.user_questions.find_one({"user": user})
            if question:
                if question_id not in question["posted"]:
                    question["posted"].insert(0, question_id)
                    yield self._db.user_questions.save(question)
            else:
                yield self._db.user_questions.insert({"user": user, "collected": [], "posted": [question_id], "answered": []})
        else:
            db = mongoclient.fbt
            question = db.user_questions.find_one({"user": user})
            if question:
                if question_id not in question["posted"]:
                    question["posted"].insert(0, question_id)
                    db.user_questions.save(question)
            else:
                db.user_questions.insert({"user": user, "collected": [], "posted": [question_id], "answered": []})

    @coroutine
    def collect_experience(self, user, exp_id):
        yield self._db.questions.update({"id": exp_id}, {"$inc": {"points": self.SCORE_POINTES["collect"], "collected_num": 1}}, upsert=True)
        experience = yield self._db.user_questions.find_one({"user": user})
        if experience:
            if exp_id not in experience["collected"]:
                experience["collected"].insert(0, exp_id)
                yield self._db.user_questions.save(experience)
        else:
            yield self._db.user_questions.insert({"user": user, "collected": [exp_id], "posted": [], "answered": []})

    @coroutine
    def get_posted_experience_helper(self, user, is_question, current_page=1, need_anonymous=False, use_cache=True):
        if is_question:
            cache_key = "question-list:users:" + user 
        else:
            cache_key = "answer-list:users:" + user 
        questions_str = self._redis_cache.get(cache_key)
        if use_cache and questions_str:
            experience_list = json.loads(questions_str)
        else:
            if is_question:
                cursor = self._db.questions.find({'publisher': user}, {"_id": 0, "thumb_up_users": 0, "best_answer": 0}).sort(
                    [("index_ctime", self._DESCENDING)])
            else:
                cursor = self._db.answers.find({'publisher': user}, {"_id": 0, "thumb_up_users": 0}).sort(
                    [("index_ctime", self._DESCENDING)])
            experience_list = yield cursor.to_list(None)
            if experience_list:
                self._redis_cache.setex(cache_key, 2 * 60, json.dumps(experience_list))
        if not need_anonymous:
            experience_list = [_ for _ in experience_list if "need_anonymous" not in _ or not _["need_anonymous"]]
        total_num = len(experience_list)
        raise Return([(total_num + self.PAGE_NUM - 1) / self.PAGE_NUM, current_page,
                      experience_list[(current_page-1)*self.PAGE_NUM:current_page*self.PAGE_NUM]])

    @coroutine
    def get_posted_experience(self, user, current_page=1, use_cache=True, need_anonymous=False):
        total_page, current_page, question_list = yield self.get_posted_experience_helper(user, True, current_page, need_anonymous=need_anonymous, use_cache=use_cache)
        raise Return([total_page, current_page, question_list])

    @coroutine
    def get_answered_experience(self, user, current_page=1, use_cache=True, need_anonymous=False):
        total_page, current_page, question_list = yield self.get_posted_experience_helper(user, False, current_page, need_anonymous=need_anonymous, use_cache=use_cache)
        raise Return([total_page, current_page, question_list])

    @coroutine
    def get_collected_experience(self, user, current_page=1):
        total_page, current_page, question_list = yield self.get_user_questions(user, "collected", current_page)
        raise Return([total_page, current_page, question_list])

    @coroutine
    def get_user_questions(self, user, type, current_page):
        # TODO add cache for db
        exp_id_list = yield self._db.user_questions.find_one({'user': user}, {"_id": 0})
        exp_list = []
        total_num = 0
        if exp_id_list:
            for exp_id in exp_id_list[type][(current_page - 1) * self.PAGE_NUM:current_page * self.PAGE_NUM]:
                exp = yield self.get_question_by_id(exp_id)
                exp_list.append(exp)
            total_num = len(exp_id_list[type])
        raise Return([(total_num + self.PAGE_NUM - 1) / self.PAGE_NUM, current_page, exp_list])

    def topK_of_sorted_lists(self, sorted_lists, K, sort_field, id_field):
        ret = []
        temp_set = set()
        heap_array = []
        for i, array in enumerate(sorted_lists):
            if array:
                if sort_field in sorted_lists[i][0]:
                    value = (-sorted_lists[i][0][sort_field], i, 0, sorted_lists[i][0])
                else:
                    value = (0, i, 0, sorted_lists[i][0])
                heappush(heap_array, value)
        while len(heap_array) < K and heap_array:
            _, i, j, item = heappop(heap_array)
            if item[id_field] not in temp_set:
                temp_set.add(item[id_field])
                ret.append(item)
            if j+1 < len(sorted_lists[i]):
                if sort_field in sorted_lists[i][j+1]:
                    value = (-sorted_lists[i][j+1][sort_field], i, j+1, sorted_lists[i][j+1])
                else:
                    value = (0, i, j+1, sorted_lists[i][j+1])
                heappush(heap_array, value)
        return ret

    @coroutine
    def get_recommended_users2(self, user, university=None, page=1, use_cache=False): # TODO
        # interested_tags = yield self._user_tag_manager.get_my_interested_tags(user)
        interested_tags = self._user_interested_tag_manager.top_users_interested_tag(user)
        if not interested_tags:
            raise Return([self.NOT_SET_TAG, page, []])
        if university:
            university = utf8(university)
            cache_key = "users:recommended2:" + university + ":" + " ".join(interested_tags)
        else:
            cache_key = "users:recommended2:" + " ".join(interested_tags)
        users_list_str = self._redis_cache.get(cache_key)
        if use_cache and users_list_str:
            top_users_list = json.loads(users_list_str)
        else:
            sorted_user_lists = yield [self._user_tag_manager.get_user_by_tag(tag_with_class, university, use_cache) for tag_with_class in interested_tags]
            top_users_list = self.topK_of_sorted_lists(sorted_user_lists, self._user_tag_manager.MAX_USER_CNT, "thumb_num", "user")
            self._redis_cache.setex(cache_key, 30 * 60, json.dumps(top_users_list))
        USER_OF_PAGE = 10
        raise Return([(len(top_users_list) + USER_OF_PAGE - 1) / USER_OF_PAGE, page,
                      top_users_list[(page - 1) * USER_OF_PAGE:page * USER_OF_PAGE]])

    @coroutine
    def get_recommend_users(self, tag_class, tags, page=1, use_cache=True):
        tag_class = utf8(tag_class)
        interested_tags = [tag_class+":"+utf8(_) for _ in tags]
        cache_key = "users:recommended:" + " ".join(interested_tags)
        users_list_str = self._redis_cache.get(cache_key)
        if use_cache and users_list_str:
            top_users_list = json.loads(users_list_str)
        else:
            sorted_user_lists = yield [self._user_tag_manager.get_user_by_tag(tag_with_class) for tag_with_class in interested_tags]
            top_users_list = self.topK_of_sorted_lists(sorted_user_lists, self._user_tag_manager.MAX_USER_CNT, "thumb_num", "user")
            self._redis_cache.setex(cache_key, 30 * 60, json.dumps(top_users_list))
        raise Return([(len(top_users_list) + self.PAGE_NUM - 1) / self.PAGE_NUM, page,
                      top_users_list[(page - 1) * self.PAGE_NUM:page * self.PAGE_NUM]])

    def comp(self, user1, user2):
        if "thumb_num" not in user1:
            user1["thumb_num"] = 0
        if "thumb_num" not in user2:
            user2["thumb_num"] = 0
        return cmp(user2["thumb_num"], user1["thumb_num"])

    def get_all_experience_tags(self):
        ret = {}
        for tag_class in self.TAG_CLASS:
            tag_class = utf8(tag_class)
            tags_num = self._exp_tag_manager.get_all_tags_by_class(tag_class)
            for tag, num in tags_num.iteritems():
                ret[tag_class + ":" + tag] = num
        return ret

    def get_tags_by_class(self, tag_class):
        ret = {}
        if self.is_valid_tag_class(tag_class):
            tag_class = utf8(tag_class)
            tags_num = self._exp_tag_manager.get_all_tags_by_class(tag_class)
            for tag, num in tags_num.iteritems():
                ret[tag] = num
        return ret

    def extract_tag_from_text(self, text, tag_class):
        return self._exp_tag_manager.extract_tag_from_text(text, tag_class)

    @coroutine
    def _clean_search_db(self):
        yield self._question_searcher.clean_all()

    @coroutine
    def search(self, keyword, current_page, use_cache=True):
        cache_key = "question:search:qa:"+keyword
        self.record_keyword(keyword)
        cached_data = self._redis_cache.get(cache_key)
        if use_cache and cached_data:
            question_ids = json.loads(cached_data)
        else:
            questions = yield self._question_searcher.search(keyword)
            question_ids = [_["id"] for _ in questions]
            self._redis_cache.setex(cache_key, 10*60, json.dumps(question_ids))
        total_res_num = len(question_ids)
        question_list = []
        for question_id in question_ids[(current_page-1)*self.PAGE_NUM:current_page*self.PAGE_NUM]:
            res = yield self.get_question_by_id(question_id)
            question_list.append(res)
        raise Return([(total_res_num+self.PAGE_NUM-1)/self.PAGE_NUM, current_page, question_list])

    def generate_auth_token(self, user, tag_class):
        tag_class = to_unicode(tag_class)
        assert constant.TAG_CLASS[tag_class] < 10
        token = generate_pkey() + str(constant.TAG_CLASS[tag_class])
        self._redis_cache.setex("auth_token:" + user, 10 * 60, token)
        return token

    def fetch_star_tag_of_auth_token(self, token, user):
        token2 = self._redis_cache.get("auth_token:" + user)
        if token2 and token2 == token:
            tag_class = int(token2[-1])
            for k,v in constant.TAG_CLASS.iteritems():
                if v == tag_class:
                    return k
            return None
        else:
            return None

    @coroutine
    def update_user_icon(self, user, icon):
        yield self._db.questions.update({"publisher": user}, {"$set": {"publisher_img": icon}}, upsert=False, multi=True)
        yield self._db.questions.update({"best_answer.publisher": user}, {"$set": {"best_answer.publisher_img": icon}}, upsert=False, multi=True)
        yield self._db.answers.update({"publisher": user}, {"$set": {"publisher_img": icon}}, upsert=False, multi=True)

    @coroutine
    def hide_question(self, question_id, user=None):
        yield self._db.questions.update({"id": question_id}, {"$set": {"hidden": 1, "hidden_user": user}}, upsert=False, multi=False)

    @coroutine
    def stick_question(self, question_id):
        question = yield self._db.questions.find_one({"id": question_id})
        if question:
            cursor = self._db.questions.find({"index_class": question["index_class"]}, {"hot_value": 1}).sort(
                [("hot_value", self._DESCENDING)]).limit(1)
            top_questions = yield cursor.to_list(None)
            if len(top_questions) == 1:
                if top_questions[0]["hot_value"]:
                    question["hot_value"] = top_questions[0]["hot_value"] + 5
                else:
                    question["hot_value"] = 1
                yield self._db.questions.save(question)
                raise Return(True)
        raise Return(False)

    @coroutine
    def get_today_questions(self):
        cursor = self._db.questions.find({"index_ctime": {"$gt": seconds_of_today_at_wee_hours() * 1000}}, {"_id": 0, "thumb_up_users": 0})
        questions = yield cursor.to_list(None)
        raise Return(questions)
        
    @coroutine
    def get_today_answers(self):
        cursor = self._db.answers.find({"index_ctime": {"$gt": seconds_of_today_at_wee_hours() * 1000}}, {"_id": 0, "thumb_up_users": 0})
        questions = yield cursor.to_list(None)
        raise Return(questions)

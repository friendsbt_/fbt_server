#coding: utf-8
import simplejson as json
import io
from process_university_db import data

def generate_university_id():
    id_of_university = {}
    for university in data["universities"]:
        provinceID = university[0]
        universityID = university[1]
        university_name = university[2]
        id_of_university[university_name] = universityID
    return id_of_university
   
def generate_college_id():
    id_of_college={}
    for university in data["colleges"]:
        collegeID=university[0]
        universityID=university[1]
        collegeName=university[2]
        if universityID not in id_of_college:
            id_of_college[universityID]={}
        id_of_college[universityID][collegeName]=collegeID
    return id_of_college


if __name__ == "__main__":
    generated_university_id = generate_university_id()
    
    json_file = "static/json/university_id.json"
    with io.open(json_file, 'w', encoding='utf8') as file:
        content=json.dumps(generated_university_id, ensure_ascii=False, encoding='utf8', indent=2)
        file.write(unicode(content))


    generated_college_id = generate_college_id()

    json_file = "static/json/college_id.json"
    with io.open(json_file, 'w', encoding='utf8') as file:
        content=json.dumps(generated_college_id, ensure_ascii=False, encoding='utf8', indent=2)
        file.write(unicode(content))
    
    print "process OK"


__author__ = 'bone'

from redis_cluster_proxy import Redis
from datetime import datetime
from time import time
from util import generate_pkey
from tornado.gen import coroutine
from tornado import httpclient
from constant import CHAT_HOST
import simplejson as json


class SystemMessageProxy(object):
    MESSAGES_MAX_LEN = 100
    _MSG_TIME_KEY = "user:messages:"
    _MSG_CONTENT_KEY = "user:message-content:"

    def __init__(self, redis_cache=None):
        if redis_cache:
            self._redis_cache = redis_cache
        else:
            self._redis_cache = Redis()
        self._pipe = self._redis_cache.pipeline()
        self.http_client = httpclient.AsyncHTTPClient()

    def get_msg(self, user):
        msg_ids = self._redis_cache.zrange(self._MSG_TIME_KEY + user, 0, -1, desc=True, withscores=True)
        ret = []
        if msg_ids:
            msg_contents = self._redis_cache.hmget(self._MSG_CONTENT_KEY + user, [msg_id for msg_id, ctime in msg_ids])
            for i, content in enumerate(msg_contents):
                ret.append({"content": content, "id": msg_ids[i][0], "ctime2": float(msg_ids[i][1]), "ctime": self.time2date(msg_ids[i][1])})
        return ret

    def get_msg_cnt(self, user):
        return self._redis_cache.hlen(self._MSG_CONTENT_KEY + user)

    def del_msg(self, user, msg_id):
        self._pipe.zrem(self._MSG_TIME_KEY + user, msg_id)
        self._pipe.hdel(self._MSG_CONTENT_KEY + user, msg_id)
        self._pipe.execute()

    def add_msg(self, user, msg):
        msg_id = generate_pkey() # time()*1000 is bad
        self._pipe.zadd(self._MSG_TIME_KEY + user, time(), msg_id)
        self._pipe.hset(self._MSG_CONTENT_KEY + user, msg_id, msg)
        self._pipe.execute()
        return msg_id

    def time2date(self, ctime):
        return datetime.fromtimestamp(long(ctime)).strftime('%Y-%m-%d %H:%M:%S')

    @coroutine
    def send_system_message(self, user_to, msg_type, msg):
        try:
            request = httpclient.HTTPRequest(CHAT_HOST,
                                            body=json.dumps({"user_to": user_to,
                                                            "content": msg,
                                                            "type": msg_type,
                                                            "password": "xiaoyuanxingkong"}),
                                            method="POST")

            yield self.http_client.fetch(request)
        except httpclient.HTTPError as e:
            print "HTTPError:", e
            print "*****Warning******\nIf you want to use system message. Please run python chat.py."

# coding: utf-8
import base64, time, hmac, hashlib, urllib, urllib2, re
from tornado.httputil import url_concat
from constant import OUR_WEB_URL, DEBUG_ENV
from tornado import httpclient, gen
from tornado.escape import utf8

# class Settings(object):
AccessKey = "3rer6DB4jKt2CqSVzBjNmAC3NQe4s_LkK5PuOB4s"
SecretKey = "vmwWsF8_EEoiB9mJPYuKDKvoVvkGn0AwdfFBWXlM"
EXPIRE_TIME = 8 * 3600


def generate_upload_token():
    putPolicy = r'{"scope":"xiaoyuanxingkong-test","deadline":deadtime,"insertOnly":1,"returnBody":"{\"key\":$(key),\"hash\":$(etag),\"name\":$(fname),\"mimeType\":$(mimeType)}"}'
    putPolicy = putPolicy.replace("deadtime", str(time.time() + EXPIRE_TIME).split(".")[0])
    encodePolicy = base64.urlsafe_b64encode(putPolicy)
    sign = hmac.new(SecretKey, encodePolicy, hashlib.sha1).digest()
    encodeSign = base64.urlsafe_b64encode(sign)
    uploadToken = AccessKey + ":" + encodeSign + ":" + encodePolicy
    return uploadToken


def generate_image_upload_token():
    putPolicy = r'{"scope":"xiaoyuanxingkong","deadline":deadtime,"insertOnly":1,"returnBody":"{\"key\":$(key),\"hash\":$(etag),\"name\":$(fname)}"}'
    putPolicy = putPolicy.replace("deadtime", str(time.time() + EXPIRE_TIME).split(".")[0])
    encodePolicy = base64.urlsafe_b64encode(putPolicy)
    sign = hmac.new(SecretKey, encodePolicy, hashlib.sha1).digest()
    encodeSign = base64.urlsafe_b64encode(sign)
    uploadToken = AccessKey + ":" + encodeSign + ":" + encodePolicy
    return uploadToken


# deprecated, new logic use key,filename generate download link.
def add_token_to_download_link(url):
    if url.find("?download/") > 0:
        words = url.split('/')
        words[-2] = urllib.quote(words[-2].split('?')[0]) + '?' + words[-2].split('?')[1]
        words[-1] = urllib.quote(words[-1])
        #         print words
        url = "/".join(words)
        downloadUrl = "%s&e=%s" % (url, str(time.time() + EXPIRE_TIME).split(".")[0])
    else:
        words = url.split('/')
        words[-1] = urllib.quote(words[-1])
        url = "/".join(words)
        downloadUrl = "%s?e=%s" % (url, str(time.time() + EXPIRE_TIME).split(".")[0])
    sign = hmac.new(SecretKey, downloadUrl, hashlib.sha1).digest()
    encodeSign = base64.urlsafe_b64encode(sign)
    token = "%s:%s" % (AccessKey, encodeSign)
    realDownloadUrl = "%s&token=%s" % (downloadUrl, token)
    # print realDownloadUrl
    return realDownloadUrl


# 生成管理凭证
def generate_access_token(path, body=None):
    if (body is None):
        signing_str = path + '\n'
    else:
        signing_str = '\n'.join([path, body])

    # print 'signing_str = ' + signing_str
    sign = hmac.new(SecretKey, signing_str, hashlib.sha1).digest()
    encoded_sign = base64.urlsafe_b64encode(sign)
    access_token = AccessKey + ':' + encoded_sign

    # print 'access_token = ' + access_token
    return access_token


def get_key_from_download_link(download_link):
    pattern = re.compile('\.com/.+\?download')  # .com/key?download
    match = pattern.search(download_link)
    key = download_link[match.start(): match.end()][5:-9]

    return key


def get_file_qetag_from_download_link(download_link):
    key = get_key_from_download_link(download_link)
    # print 'key = ' + key

    bucket = 'xiaoyuanxingkong-test'
    entry = bucket + ':' + key
    # print 'entry = ' + entry

    encoded_entry_uri = base64.urlsafe_b64encode(entry)
    # print 'encoded_entry_uri' + encoded_entry_uri

    path = '/stat/' + encoded_entry_uri
    # print 'path = ' + path
    access_token = generate_access_token(path)

    req = urllib2.Request('http://rs.qiniu.com' + path)

    req.add_header('Authorization', 'QBox ' + access_token)
    response = urllib2.urlopen(req)
    return response.read()


@gen.coroutine
def office_to_pdf(key_src, file_extension, sync=False, callback_url= OUR_WEB_URL + "/resource/preview/upload"):
    pipeline = 'office2pdf'
    bucket_src = 'xiaoyuanxingkong-test'

    saved_bucket = 'office-preview'
    # saved_key = 'preview' + key_src
    encoded_entry_uri = base64.urlsafe_b64encode(saved_bucket)

    data = {
        'bucket': utf8(bucket_src),
        'key': utf8(key_src),
        'force': 0,
        'pipeline': pipeline,
        'fops': 'yifangyun_preview/v2/ext=%s|saveas/%s' % (file_extension, encoded_entry_uri),
        'notifyURL': callback_url
    }

    # print urllib.urlencode(data)
    ac_token = generate_access_token('/pfop/', urllib.urlencode(data))
    op_host = 'http://api.qiniu.com/pfop/'

    if sync:
        req = urllib2.Request(op_host, urllib.urlencode(data))
        req.add_header('Authorization', 'QBox ' + ac_token)
        req.add_header('Content-Type', 'application/x-www-form-urlencoded')
        info = urllib2.urlopen(req).read()
    else:
        req = httpclient.HTTPRequest(op_host,body=urllib.urlencode(data),method='POST',
                                     headers={'Authorization': 'QBox ' + ac_token,
                                              'Content-Type': 'application/x-www-form-urlencoded'})
        http_client = httpclient.AsyncHTTPClient()
        info = yield http_client.fetch(req)
    if DEBUG_ENV:
        print info


def get_download_link(key, file_name, bucket=None):

    # urllib.quote only accept str, not unicode
    key = unicode(key).encode('utf-8')
    file_name = unicode(file_name).encode('utf-8')

    if bucket is not None:
        if bucket == 'xiaoyuanxingkong-test':  # 这个里边放的是资料
            dn_host = 'http://7xjkjd.dl1.z0.glb.clouddn.com/'
        elif bucket == 'xiaoyuanxingkong':  # 这个里边放的是用户的头像和问答里边的图片 ,公有空间
            dn_host = 'http://7xjkhy.dl1.z0.glb.clouddn.com/'
        elif bucket == 'office-preview':  # 预览
            dn_host = 'http://7xqvry.com1.z0.glb.clouddn.com/'
        else:
            return ''
    else:  # 默认是下载资料的
        dn_host = 'http://7xjkjd.dl1.z0.glb.clouddn.com/'

    dn_link = '%s%s?attname=%s&e=%s' % (
    dn_host, urllib.quote(key), urllib.quote(file_name), str(time.time() + EXPIRE_TIME).split(".")[0])

    sign = hmac.new(SecretKey, dn_link, hashlib.sha1).digest()
    encode_sign = base64.urlsafe_b64encode(sign)
    token = "%s:%s" % (AccessKey, encode_sign)
    auth_dn_link = "%s&token=%s" % (dn_link, token)

    return auth_dn_link


def get_preview_link(key, file_name):

    if file_name.split('.')[-1].lower() == u'pdf':
        return get_download_link(key, file_name, 'xiaoyuanxingkong-test')
    return get_download_link(key, file_name, 'office-preview')


if __name__ == "__main__":
    pass
    # generate_upload_token()
    # add_token_to_download_link("http://7xirwu.com1.z0.glb.clouddn.com/注册机.exe_1433512266988?download/注册机.exe")
    # print add_token_to_download_link("http://7xirwu.com1.z0.glb.clouddn.com/test.pyc")
    # print add_token_to_download_link("http://7xirwu.com1.z0.glb.clouddn.com/GIS开发整理.docx")
    # print get_file_qetag_from_download_link("http://7xirwu.com1.z0.glb.clouddn.com/导学关系毕业生问卷-请回复至dxwenjuan@ucas.ac.cn，感谢参与.doc_1433552695427?download/导学关系毕业生问卷-请回复至dxwenjuan@ucas.ac.cn，感谢参与.doc")
    # print get_file_qetag_from_download_link('http://7xjkjd.dl1.z0.glb.clouddn.com/FjKXKYYICvHnVkERDv-0lZCDaHfx?download/vim_user_manual_603.0.pdf')
    # print office_to_pdf('第6章 再保险_1440154993294.ppt', 'ppt')
    # print office_to_pdf('第6章 再保险_1440154993294.ppt', 'ppt',True)
    # print get_download_link('第6章 再保险_1440154993294.ppt','第6章 再保险.ppt')
    print get_preview_link(u'zwIcIhN2n5w0XCDZ8Zg_k中文FQLIycU=/Flb6_so0RUWTwENPtPiOb3iTNW0z',u'第6章 再保险.doc')

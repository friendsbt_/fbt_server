__author__ = 'bone-lee'

import mongoclient

import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.gen
import tornado.options

from math import pow
from datetime import datetime
from time import mktime, time
from QA_manager import QAManager

'''
Hottness algorithm:
From: https://moz.com/blog/reddit-stumbleupon-delicious-and-hacker-news-algorithms-exposed
https://medium.com/hacking-and-gonzo/how-hacker-news-ranking-algorithm-works-1d9b0cf2c08d#.ochi4bh2a

Y Combinator's Hacker News:
Formula:

    (p - 1) / (t + 2)^1.8

Description:

    Votes divided by age factor

    p = votes (points) from users.
    t = time since submission in hours.

    p is subtracted by 1 to negate submitters vote.
    age factor is (time since submission in hours plus two) to the power of 1.5.

Source: Paul Graham, creator of Hacker News
'''


def calculate_hotness(points, ctime_in_seconds):
    return round(points / pow((long(time()) - ctime_in_seconds)/3600 + 2, 1.8), 6)


def date2time(date_obj):
    return long(mktime(date_obj.timetuple()))


class UpdateHotnessTask(object):
    REFRESH_INTERVAL = 4*3600

    def __init__(self, ioloop=None, db=None):
        self._io_loop = ioloop or tornado.ioloop.IOLoop.instance()
        self.db = db or mongoclient.fbt

    def run(self):
        assert (self._io_loop is not None)
        # print("running refresh db the first time....")
        # self._refresh_hot_value(update_all=True)
        # print("refresh db over....")
        self._io_loop.add_timeout(time() + self.REFRESH_INTERVAL, lambda: self._refresh_hot_value())

    def _refresh_hot_value(self, update_all=False):
        print("refresh hotness in DB ...")
        cnt = 0
        a_week_ago = time() - 7 * 24 * 3600
        begin_time = 0 if update_all else long(a_week_ago*1000)
        for Q in self.db.questions.find({"index_ctime": {"$gt": begin_time}}).batch_size(30):
            if "views" not in Q:
                Q["views"] = 0
            if "comment_num" not in Q:
                Q["comment_num"] = 0
            if "collected_num" not in Q:
                Q["collected_num"] = 0
            if "thanked_users" not in Q:
                Q["thanked_users"] = []
            if "points" not in Q:
                Q["points"] = Q["reply_num"]*QAManager.SCORE_POINTES["answer"] + Q["thumb_up_num"]*QAManager.SCORE_POINTES["thumb_up"] +\
                              Q["views"]*QAManager.SCORE_POINTES["view"] + Q["comment_num"]*QAManager.SCORE_POINTES["comment"] +\
                              Q["collected_num"]*QAManager.SCORE_POINTES["collect"] + len(Q["thanked_users"])*QAManager.SCORE_POINTES["reward"]
            ctime_seconds = date2time(datetime.strptime(Q["ctime"], '%Y-%m-%d %H:%M'))
            Q["hot_value"] = calculate_hotness(Q["points"], ctime_seconds)
            self.db.questions.save(Q)
            cnt += 1
        print("refresh hotness in DB count:" + str(cnt))
        self._io_loop.add_timeout(time() + self.REFRESH_INTERVAL, lambda: self._refresh_hot_value())


class MainHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        self.write("OK....")


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/', MainHandler),
        ]
        settings = dict()
        tornado.web.Application.__init__(self, handlers, **settings)


if __name__ == '__main__':
    http_server = tornado.httpserver.HTTPServer(Application(), xheaders=True)
    http_server.listen(8788)
    ioloop = tornado.ioloop.IOLoop.instance()
    task = UpdateHotnessTask(ioloop)
    task.run()
    try:
        ioloop.start()
    except:
        print "OK. Exit..."

# coding: utf8
__author__ = 'bone'

from university_db import UniversityDB

from tornado import gen
from tornado import ioloop
import os

@gen.coroutine
def check_university_college(check_university=True, check_college=False, add_to_db=False):
    udb = UniversityDB()
    man = None
    for dir, subdirs, names in os.walk("csv/more3"):
        for name in names:
            path = os.path.join(dir, name)
            university, _ = os.path.splitext(name)
            # print "processing ", university
            if check_university:
                if not udb.is_valid_university(university):
                    print university, "is not a valid university. fixed it first."
                    exit(-1)
            elif check_college:
                try:
                    lines = open(path).readlines()
                    for i,line in enumerate(lines):
                        if i==0: continue
                        # if university == "北京大学" and i==1336:
                        #     print "check"
                        # if university == "清华大学" and i==466:
                        #     print "check"
                        dec_line = line.decode("GBK")
                        data = dec_line.split(',')
                        if len(data) < 3:
                            # print "pass line:", i+1, dec_line
                            continue
                        course = data[0].strip()
                        if not course:
                            # print "pass line:", i+1, dec_line
                            continue
                        if course[0] == '"':
                            i = -1
                            course = ""
                            while i+1 < len(data) and data[i+1][-1] != '"':
                                course += data[i+1]
                                i += 1
                                course += ","
                            course += data[i+1]
                            course = course.replace('"', "")
                            i += 1
                        else:
                            i = 0
                        i += 1
                        teacher = data[i].strip()
                        if teacher and teacher[0] == '"':
                            teacher = ""
                            i -= 1
                            while i+1<len(data) and data[i+1][-1] != '"':
                                teacher += data[i+1]
                                i += 1
                                teacher += ","
                            i += 1
                            teacher += data[i]
                            assert data[i][-1] == '"'
                            teacher = teacher.replace('"',"")
                            college = data[i+1].strip()
                        else:
                            college = data[i+1].strip()

                        course = course.replace("?","")
                        teacher = teacher.replace("?","")
                        college = college.replace("?","")

                        if not course:
                            continue
                        if not college:
                            continue

                        if not udb.is_valid_university_and_college(university, college):
                            # print "line:", i+1, " college invalid error:", dec_line
                            # print course, teacher, college, university
                            print university, college
                            continue
                            # exit(-1)
                        else:
                            if add_to_db:
                                from study_resource_manager import StudyResourceManager
                                if man is None:
                                    man = StudyResourceManager()
                                #yield man.customize_add_course(university, college, course, teacher)
                                man.customize_add_course2(university, college, course, teacher)
                except IOError as e:
                    print "pass file: %s because error: %s" % (path,e)


@gen.coroutine
def main():
    check_university_college(check_university=True, check_college=False)
    yield check_university_college(check_university=False, check_college=True, add_to_db=False)

if __name__ == '__main__':
    ioloop.IOLoop.instance().run_sync(main)

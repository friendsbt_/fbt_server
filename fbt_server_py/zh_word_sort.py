# coding: utf8
__author__ = 'bone'

from pypinyin import pinyin, lazy_pinyin, TONE2


def key2(x):
    return pinyin(x, style=TONE2)

for i in sorted([u"我", u"是", u"好", u"人", u"一个"], key=key2):
    print i

for i in sorted([u"赵", u"钱", u"孙", u"李", u"佘"], key=key2):
    print i


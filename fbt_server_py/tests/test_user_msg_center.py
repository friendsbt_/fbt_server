__author__ = 'bone'

import sys
import os.path

fbt_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

sys.path.append(fbt_path)

from user_msg_center import SystemMessageProxy

import tornado.testing
import tornado.gen
import redis

class RewardManagerTestCase(tornado.testing.AsyncTestCase):
    @tornado.testing.gen_test(timeout=3)
    def test_add_msg(self):
        r = redis.StrictRedis()
        msg_center = SystemMessageProxy(r)
        r.flushdb()
        user = "test_user"
        messages = msg_center.get_msg(user)
        self.assertEqual(len(messages),0)
        message_cnt = msg_center.get_msg_cnt(user)
        self.assertEqual(message_cnt,0)
        msg = "some content"
        msg_center.add_msg(user, msg)
        messages = msg_center.get_msg(user)
        message_cnt = msg_center.get_msg_cnt(user)
        self.assertEqual(message_cnt,1)
        self.assertEqual(len(messages),1)

        msg = "some content2"
        msg_center.add_msg(user, msg)
        messages = msg_center.get_msg(user)
        self.assertEqual(len(messages),2)
        message_cnt = msg_center.get_msg_cnt(user)
        self.assertEqual(message_cnt,2)
        # print messages[1]["ctime2"]
        # print messages[0]["ctime2"]
        # print messages
        self.assertTrue(messages[0]["ctime2"]>messages[1]["ctime2"])

        max_len = msg_center.MESSAGES_MAX_LEN
        for i in range(max_len):
            msg_center.add_msg(user, msg)
        messages = msg_center.get_msg(user)
        self.assertEqual(len(messages),max_len+2)
        message_cnt = msg_center.get_msg_cnt(user)
        self.assertEqual(message_cnt, max_len+2)

    def test_msg_del(self):
        r = redis.StrictRedis()
        msg_center = SystemMessageProxy(r)
        r.flushdb()
        user = "test_user2"
        messages = msg_center.get_msg(user)
        self.assertEqual(len(messages),0)
        message_cnt = msg_center.get_msg_cnt(user)
        self.assertEqual(message_cnt, 0)
        msg = "some content"
        msg_id_list = []
        for i in range(10):
            msg_id = msg_center.add_msg(user, msg)
            msg_id_list.append(msg_id)
        messages = msg_center.get_msg(user)
        self.assertEqual(len(messages),10)
        message_cnt = msg_center.get_msg_cnt(user)
        self.assertEqual(message_cnt, 10)

        msg_center.del_msg(user, msg_id_list[0])
        messages = msg_center.get_msg(user)
        self.assertEqual(len(messages),9)
        message_cnt = msg_center.get_msg_cnt(user)
        self.assertEqual(message_cnt, 9)

        msg_center.del_msg(user, msg_id_list[4])
        messages = msg_center.get_msg(user)
        self.assertEqual(len(messages),8)
        message_cnt = msg_center.get_msg_cnt(user)
        self.assertEqual(message_cnt, 8)

        msg_center.del_msg(user, msg_id_list[4])
        messages = msg_center.get_msg(user)
        self.assertEqual(len(messages),8)

        for msg_id in msg_id_list:
            msg_center.del_msg(user, msg_id)
        messages = msg_center.get_msg(user)
        self.assertEqual(len(messages),0)
        message_cnt = msg_center.get_msg_cnt(user)
        self.assertEqual(message_cnt, 0)

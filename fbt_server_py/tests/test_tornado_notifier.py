# coding: utf-8

import sys
import os
import json
import unittest
import threading
import shutil
from codecs import open as open
from tornado.ioloop import IOLoop

sys.path.append('..')
import course_db
import settings


class TestCourseDB(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        if os.path.exists(settings.COURSE_FILE_BAK):
            os.remove(settings.COURSE_FILE_BAK)

        cls.original_course_file = settings.COURSE_FILE + '_old'
        shutil.copy(settings.COURSE_FILE, cls.original_course_file)
        with open(settings.COURSE_FILE, encoding='utf-8') as f:
            cls.original_json_data = json.load(f)

        cls.coursedb = course_db.CourseDB()

    @classmethod
    def tearDownClass(cls):
        shutil.copy(cls.original_course_file, settings.COURSE_FILE)
        os.remove(cls.original_course_file)
        IOLoop.instance().stop()
        cls.coursedb.stop_notifier()

    def test_bak_generation(self):
        self.assertTrue(os.path.exists(settings.COURSE_FILE_BAK))

    def test_handle_delete(self):
        """ Can't test async test without tornado.testing
        os.remove(settings.COURSE_FILE)

        self.assertTrue(os.path.exists(settings.COURSE_FILE))
        with open(settings.COURSE_FILE, encoding='utf-8') as f:
            restored_json_data = json.load(f)

        print("test_handle_delete in new thread")
        self.assertDictEqual(self.original_json_data, restored_json_data)
        """
        pass

    def test_handle_valid_modify(self):
        pass

    def test_handle_invalid_modify(self):
        pass


if __name__ == '__main__':
    def start_io_loop():
        IOLoop.instance().start()

    t = threading.Thread(target=start_io_loop)
    t.daemon = True
    t.start()
    unittest.main()

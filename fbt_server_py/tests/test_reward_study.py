__author__ = 'bone'

import sys
import os.path

fbt_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

sys.path.append(fbt_path)

from university_db import UniversityDB
from user_msg_center import SystemMessageProxy
from random import randint
import tornado.testing
import tornado.gen
import motor
from pymongo import MongoClient
import mock
import redis


class RewardManagerTestCase(tornado.testing.AsyncTestCase):
    def tearDown(self):
        pass

    @tornado.testing.gen_test(timeout=3)
    def test_upload_reward_and_view(self):
        sync_db = MongoClient()
        with mock.patch("pymongo.MongoReplicaSetClient", return_value=sync_db) as what_ever:
            with mock.patch('redis_lru_scheduler.RedisDelegate', mock.MagicMock()) as w:
                with mock.patch('redis_handler.RedisHandler.redis_client', return_value=redis.StrictRedis()) as whate_ever:
                    from coin_manager import CoinManager
                    from reward_study_manager import RewardManager

                    db = motor.MotorClient().fbt_test
                    reward_man = RewardManager(mock.MagicMock(), db, CoinManager(db, redis.StrictRedis()))
                    yield reward_man.clear_db_for_test()
                    user = "123@123.com"
                    uid = 123
                    nick_name = "bone"
                    reward = {"university": 'xxx',
                              "college": 'xxx',
                              "course": 'xxxx',
                              "teacher": 'xxx',
                              "tag": 'xxx',
                              "year": 'xxx',
                              "description": "xxx",
                              "expire_days": 2,
                              "total_fb": 10
                              }
                    total_page, cur_page, my_reward = yield reward_man.get_my_reward(user, 1)
                    self.assertEqual(len(my_reward), 0)
                    self.assertEqual(total_page, 0)
                    self.assertEqual(cur_page, 1)
                    yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)
                    total_page, cur_page, my_reward = yield reward_man.get_my_reward(user, 1)
                    self.assertEqual(len(my_reward), 1)
                    self.assertEqual(total_page, 1)
                    self.assertEqual(cur_page, 1)

                    yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)
                    total_page, cur_page, my_reward = yield reward_man.get_my_reward(user, 1)
                    self.assertEqual(total_page, 1)
                    self.assertEqual(cur_page, 1)
                    self.assertEqual(len(my_reward), 2)

                    for i in range(reward_man.RES_PAGE_NUM_IN_MY_REWARD):
                        yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)
                    total_page, cur_page, my_reward = yield reward_man.get_my_reward(user, 2)
                    self.assertEqual(total_page, 2)
                    self.assertEqual(cur_page, 2)
                    self.assertEqual(len(my_reward), 2)

    @tornado.testing.gen_test(timeout=3)
    def test_reward_overview(self):
        sync_db = MongoClient()
        with mock.patch("pymongo.MongoReplicaSetClient", return_value=sync_db) as what_ever:
            with mock.patch('redis_lru_scheduler.RedisDelegate', mock.MagicMock()) as w:
                with mock.patch('redis_handler.RedisHandler.redis_client', return_value=redis.StrictRedis()) as whate_ever:
                    from coin_manager import CoinManager
                    from reward_study_manager import RewardManager

                    db = motor.MotorClient().fbt_test
                    university_db = UniversityDB()
                    reward_man = RewardManager(mock.MagicMock(), db, CoinManager(db, redis.StrictRedis()))
                    yield reward_man.clear_db_for_test()

                    total_page, cur_page, reward_list = yield reward_man.get_reward_overview(reward_man.SORT_BY["time"],
                                                                                             1)
                    self.assertEqual(len(reward_list), 0)
                    self.assertEqual(total_page, 0)
                    self.assertEqual(cur_page, 1)

                    user = "123@123.com"
                    uid = 123
                    nick_name = "bone"
                    university, college = university_db.random_choose_college_of_university()
                    reward = {"university": university, "college": college, "course": 'xxxx', "teacher": 'xxx',
                              "tag": 'xxx', "year": 'xxx', "description": "xxx", "expire_days": -1, "total_fb": 10}

                    # invalid upload
                    yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)
                    total_page, cur_page, reward_list = yield reward_man.get_reward_overview(reward_man.SORT_BY["time"],
                                                                                             1)
                    self.assertEqual(len(reward_list), 0)
                    self.assertEqual(total_page, 0)
                    self.assertEqual(cur_page, 1)

                    reward["expire_days"] = 1
                    fb_list = []
                    for i in range(100):
                        fb = randint(1, 30)
                        reward["total_fb"] = fb
                        fb_list.append(fb)
                        yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)
                    total_page, cur_page, reward_list = yield reward_man.get_reward_overview(reward_man.SORT_BY["time"],
                                                                                             1)
                    self.assertEqual(len(reward_list), min(100, reward_man.RES_PAGE_NUM_IN_MY_REWARD))
                    self.assertEqual(total_page, (
                    100 + reward_man.RES_PAGE_NUM_IN_MY_REWARD - 1) / reward_man.RES_PAGE_NUM_IN_MY_REWARD)
                    self.assertEqual(cur_page, 1)

                    total_page, cur_page, reward_list = yield reward_man.get_reward_overview(
                        reward_man.SORT_BY["total_fb"], 1)
                    self.assertEqual(len(reward_list), min(100, reward_man.RES_PAGE_NUM_IN_MY_REWARD))
                    self.assertEqual(total_page, (
                    100 + reward_man.RES_PAGE_NUM_IN_MY_REWARD - 1) / reward_man.RES_PAGE_NUM_IN_MY_REWARD)
                    self.assertEqual(cur_page, 1)
                    fb_list.sort(reverse=True)
                    for i, r in enumerate(reward_list):
                        self.assertEqual(fb_list[i], r["total_fb"])

                    total_page, cur_page, reward_list = yield reward_man.get_reward_overview(reward_man.SORT_BY["total_fb"], 1, university)
                    self.assertEqual(len(reward_list), min(100, reward_man.RES_PAGE_NUM_IN_MY_REWARD))
                    self.assertEqual(total_page, (100 + reward_man.RES_PAGE_NUM_IN_MY_REWARD - 1) / reward_man.RES_PAGE_NUM_IN_MY_REWARD)
                    self.assertEqual(cur_page, 1)

    @tornado.testing.gen_test(timeout=3)
    def test_append_reward(self):
        sync_db = MongoClient()
        with mock.patch("pymongo.MongoReplicaSetClient", return_value=sync_db) as what_ever:
            with mock.patch('redis_lru_scheduler.RedisDelegate', mock.MagicMock()) as w:
                with mock.patch('redis_handler.RedisHandler.redis_client', return_value=redis.StrictRedis()) as whate_ever:
                    from coin_manager import CoinManager
                    from reward_study_manager import RewardManager

                    db = motor.MotorClient().fbt_test
                    university_db = UniversityDB()
                    coin_man = CoinManager(db, redis.StrictRedis())
                    reward_man = RewardManager(mock.MagicMock(), db, coin_man)
                    yield reward_man.clear_db_for_test()

                    user = "123@123.com"
                    uid = 123
                    nick_name = "bone"
                    reward_fb = 321
                    reward = {"university": 'xxx',
                              "college": 'xxx',
                              "course": 'xxxx',
                              "teacher": 'xxx',
                              "tag": 'xxx',
                              "year": 'xxx',
                              "description": "xxx",
                              "expire_days": 2,
                              "total_fb": reward_fb
                              }
                    reward_id = yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)
                    user2 = "123@tre.com"
                    uid2 = 12334
                    nick_name2 = "kaka"
                    how_much = 10
                    yield coin_man.register_ok(uid2, user2)
                    yield reward_man.append_reward(reward_id, how_much, user2, uid2, nick_name2)
                    total_fb = yield reward_man.get_reward_fb(reward_id)
                    self.assertEqual(total_fb, reward_fb + how_much)

                    coin, _ = yield coin_man.get_coin(uid2)
                    self.assertEqual(coin_man.REGISTER_COIN-how_much, coin)

                    yield reward_man.append_reward(reward_id, how_much, user2, uid2, nick_name2)
                    total_fb2 = yield reward_man.get_reward_fb(reward_id)
                    self.assertEqual(total_fb2, total_fb+how_much)

    @tornado.testing.gen_test(timeout=3)
    def test_set_upload_preview_key(self):
        sync_db = MongoClient()
        with mock.patch("pymongo.MongoReplicaSetClient", return_value=sync_db) as what_ever:
            with mock.patch('redis_lru_scheduler.RedisDelegate', mock.MagicMock()) as w:
                with mock.patch('redis_handler.RedisHandler.redis_client', return_value=redis.StrictRedis()) as whate_ever:
                    from coin_manager import CoinManager
                    from reward_study_manager import RewardManager

                    db = motor.MotorClient().fbt_test
                    # reward_man = RewardManager(mock.MagicMock(), db, FBCoinManager)
                    coin_man = CoinManager(db, redis.StrictRedis())
                    reward_man = RewardManager(mock.MagicMock(), db, coin_man)
                    yield reward_man.clear_db_for_test()

                    user = "123@123.com"
                    uid = 123
                    nick_name = "bone"
                    reward_fb = 321
                    reward = {"university": 'xxx',
                              "college": 'xxx',
                              "course": 'xxxx',
                              "teacher": 'xxx',
                              "tag": 'xxx',
                              "year": 'xxx',
                              "description": "xxx",
                              "expire_days": 2,
                              "total_fb": reward_fb
                              }
                    reward_id = yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)

                    r = redis.StrictRedis()
                    user_msg_center = SystemMessageProxy(r)
                    r.flushdb()
                    reward_man.set_user_msg_center(user_msg_center)
                    user2 = "123@tre.com"
                    uid2 = 12334
                    nick_name2 = "kaka"
                    resource_link = "file_hash1"
                    yield reward_man.upload_reward_resource(reward_id, user2, uid2, nick_name2, resource_link, "filename", 1234)
                    file_hash = "file_hash2"
                    yield reward_man.upload_reward_resource(reward_id, user2, uid2, nick_name2, file_hash, "filename", 1234)
                    resource_list = yield reward_man.get_reward_resource_list(reward_id)
                    self.assertEqual(len(resource_list), 1)
                    self.assertEqual(len(resource_list[0]["uploaded_resources"]), 2)
                    self.assertEqual(resource_list[0]["uploaded_resources"][0]["file_hash"], "file_hash1")
                    self.assertEqual(resource_list[0]["uploaded_resources"][1]["file_hash"], "file_hash2")
                    yield reward_man.insert_reward_res_preview_key(reward_id, file_hash, "preview_key", "preview_hash")
                    res = yield reward_man.find_resource(reward_id, file_hash)
                    self.assertEqual(res["file_hash"], "file_hash2")
                    self.assertEqual(res["preview_key"], "preview_key")
                    self.assertEqual(res["preview_hash"], "preview_hash")


    @tornado.testing.gen_test(timeout=3)
    def test_upload_reward_resource(self):
        sync_db = MongoClient()
        with mock.patch("pymongo.MongoReplicaSetClient", return_value=sync_db) as what_ever:
            with mock.patch('redis_lru_scheduler.RedisDelegate', mock.MagicMock()) as w:
                with mock.patch('redis_handler.RedisHandler.redis_client', return_value=redis.StrictRedis()) as whate_ever:
                    from coin_manager import CoinManager
                    from reward_study_manager import RewardManager

                    db = motor.MotorClient().fbt_test
                    # reward_man = RewardManager(mock.MagicMock(), db, FBCoinManager)
                    coin_man = CoinManager(db, redis.StrictRedis())
                    reward_man = RewardManager(mock.MagicMock(), db, coin_man)
                    yield reward_man.clear_db_for_test()

                    user = "123@123.com"
                    uid = 123
                    nick_name = "bone"
                    reward_fb = 321
                    reward = {"university": 'xxx',
                              "college": 'xxx',
                              "course": 'xxxx',
                              "teacher": 'xxx',
                              "tag": 'xxx',
                              "year": 'xxx',
                              "description": "xxx",
                              "expire_days": 2,
                              "total_fb": reward_fb
                              }
                    reward_id = yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)

                    r = redis.StrictRedis()
                    user_msg_center = SystemMessageProxy(r)
                    r.flushdb()
                    reward_man.set_user_msg_center(user_msg_center)
                    user2 = "123@tre.com"
                    uid2 = 12334
                    nick_name2 = "kaka"
                    resource_link = "http://xxx.xxx.xxx/filexxx"
                    yield reward_man.upload_reward_resource(reward_id, user2, uid2, nick_name2, resource_link, "filename", 1234)
                    resource_list = yield reward_man.get_reward_resource_list(reward_id)
                    self.assertEqual(len(resource_list), 1)
                    self.assertEqual(resource_list[0]["uploaded_resources"][0]["filename"], "filename")

                    # self upload is invalid
                    yield reward_man.upload_reward_resource(reward_id, user, uid, nick_name2, resource_link, "filename", 1234)
                    resource_list = yield reward_man.get_reward_resource_list(reward_id)
                    self.assertEqual(len(resource_list), 1)
                    self.assertEqual(resource_list[0]["uploaded_resources"][0]["filename"], "filename")

                    reward_id = "reward_id_not_exist"
                    yield reward_man.upload_reward_resource(reward_id, user2, uid2, nick_name2, resource_link,"filename", 1234)
                    resource_list = yield reward_man.get_reward_resource_list(reward_id)
                    self.assertEqual(len(resource_list), 0)

    @tornado.testing.gen_test(timeout=3)
    def test_confirm_reward_resource(self):
        sync_db = MongoClient()
        with mock.patch("pymongo.MongoReplicaSetClient", return_value=sync_db) as what_ever:
            with mock.patch('redis_lru_scheduler.RedisDelegate', mock.MagicMock()) as w:
                with mock.patch('redis_handler.RedisHandler.redis_client', return_value=redis.StrictRedis()) as whate_ever:
                    from coin_manager import CoinManager
                    from reward_study_manager import RewardManager

                    db = motor.MotorClient().fbt_test
                    # reward_man = RewardManager(mock.MagicMock(), db, FBCoinManager)
                    coin_man = CoinManager(db, redis.StrictRedis())
                    reward_man = RewardManager(mock.MagicMock(), db, coin_man)

                    user = "123@123.com"
                    uid = 123
                    nick_name = "bone"
                    reward_fb = 321
                    reward = {"university": 'xxx',
                              "college": 'xxx',
                              "course": 'xxxx',
                              "teacher": 'xxx',
                              "tag": 'xxx',
                              "year": 'xxx',
                              "description": "xxx",
                              "expire_days": 2,
                              "total_fb": reward_fb
                              }
                    reward_id = yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)
                    user2 = "123@tre.com"
                    uid2 = 12334
                    nick_name2 = "kaka"
                    resource_link = "http://xxx.xxx.xxx/filexxx"

                    r = redis.StrictRedis()
                    user_msg_center = SystemMessageProxy(r)
                    r.flushdb()
                    reward_man.set_user_msg_center(user_msg_center)
                    yield reward_man.upload_reward_resource(reward_id, user2, uid2, nick_name2, resource_link,"filename", 1234)
                    self.assertEqual(len(user_msg_center.get_msg(user)), 1)

                    user3 = "4123@tre.com"
                    uid3 = 123345
                    nick_name3 = "xxkaka"
                    resource_link2 = "http://xxx.xxx.xxx/filexxx2"
                    yield coin_man.register_ok(uid3, user3)
                    yield reward_man.upload_reward_resource(reward_id, user3, uid3, nick_name3, resource_link2,"filename", 1234)
                    yield reward_man.upload_reward_resource(reward_id, user3, uid3, nick_name3, resource_link2,"filename22222", 43)
                    yield reward_man.upload_reward_resource(reward_id, user3, uid3, nick_name3, resource_link2,"filename333", 212)
                    resource_list = yield reward_man.get_reward_resource_list(reward_id)
                    self.assertEqual(len(resource_list), 2)
                    # print user_msg_center.get_msg(user)
                    self.assertEqual(len(user_msg_center.get_msg(user)), 4)

                    score = 4.5
                    yield reward_man.user_confirm_resource(reward_id, user3, score)
                    reward = yield reward_man.get_a_reward(reward_id)
                    self.assertEqual(reward["status"], "over")
                    self.assertEqual(reward["resource_list"][1]["accepted"], True)
                    total_fb, _ = yield coin_man.get_coin(uid3)
                    self.assertEqual(total_fb, coin_man.REGISTER_COIN + reward_fb)
                    # print user_msg_center.get_msg(user3)
                    self.assertEqual(len(user_msg_center.get_msg(user3)), 1)

    @tornado.testing.gen_test(timeout=5)
    def test_reward_expired(self):
        sync_db = MongoClient()
        with mock.patch("pymongo.MongoReplicaSetClient", return_value=sync_db) as what_ever:
            with mock.patch('redis_lru_scheduler.RedisDelegate', mock.MagicMock()) as w:
                with mock.patch('redis_handler.RedisHandler.redis_client', return_value=redis.StrictRedis()) as whate_ever:
                    from coin_manager import CoinManager
                    from reward_study_manager import RewardManager

                    db = motor.MotorClient().fbt_test
                    # reward_man = RewardManager(mock.MagicMock(), db, FBCoinManager)
                    coin_man = CoinManager(db, redis.StrictRedis())
                    reward_man = RewardManager(mock.MagicMock(), db, coin_man)
                    yield reward_man.clear_db_for_test()

                    user = "123@123.com"
                    uid = 123
                    nick_name = "bone"
                    reward_fb = 321
                    expire_days = -2
                    reward = {"university": 'xxx',
                              "college": 'xxx',
                              "course": 'xxxx',
                              "teacher": 'xxx',
                              "tag": 'xxx',
                              "year": 'xxx',
                              "description": "xxx",
                              "expire_days": expire_days,
                              "total_fb": reward_fb
                              }
                    reward_id = yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)
                    total_page, cur_page, reward_list = yield reward_man.get_my_reward_helper(user, reward_man.STATUS["over"], 1)
                    self.assertEqual(total_page,0)
                    self.assertEqual(cur_page,1)
                    self.assertEqual(len(reward_list),0)

                    total_page, cur_page, reward_list =yield reward_man.get_my_reward_helper(user, reward_man.STATUS["pending"], 1)
                    self.assertEqual(total_page,1)
                    self.assertEqual(cur_page,1)
                    self.assertEqual(len(reward_list),1)

                    total_page, cur_page, reward_list =yield reward_man.get_my_reward_helper(user, reward_man.STATUS["expired"], 1)
                    self.assertEqual(total_page,0)
                    self.assertEqual(cur_page,1)
                    self.assertEqual(len(reward_list),0)

                    yield reward_man.process_expired_reward()

                    total_page, cur_page, reward_list = yield reward_man.get_my_reward_helper(user, reward_man.STATUS["over"], 1)
                    self.assertEqual(total_page,0)
                    self.assertEqual(cur_page,1)
                    self.assertEqual(len(reward_list),0)

                    total_page, cur_page, reward_list =yield reward_man.get_my_reward_helper(user, reward_man.STATUS["pending"], 1)
                    self.assertEqual(total_page,0)
                    self.assertEqual(cur_page,1)
                    self.assertEqual(len(reward_list),0)

                    total_page, cur_page, reward_list =yield reward_man.get_my_reward_helper(user, reward_man.STATUS["expired"], 1)
                    self.assertEqual(total_page,1)
                    self.assertEqual(cur_page,1)
                    self.assertEqual(len(reward_list),1)

                    r = redis.StrictRedis()
                    user_msg_center = SystemMessageProxy(r)
                    r.flushdb()
                    reward_man.set_user_msg_center(user_msg_center)
                    reward_id = yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)
                    user2 = "123@tre.com"
                    uid2 = 12334
                    nick_name2 = "kaka"
                    resource_link = "http://xxx.xxx.xxx/filexxx"
                    yield reward_man.upload_reward_resource(reward_id, user2, uid2, nick_name2, resource_link, "filename", 1234)

                    total_page, cur_page, reward_list =yield reward_man.get_my_reward_helper(user, None, 1)
                    self.assertEqual(total_page,1)
                    self.assertEqual(cur_page,1)
                    self.assertEqual(len(reward_list),2)

                    yield reward_man.process_expired_reward()
                    total_page, cur_page, reward_list = yield reward_man.get_my_reward_helper(user, reward_man.STATUS["over"], 1)
                    self.assertEqual(total_page,1)
                    self.assertEqual(cur_page,1)
                    self.assertEqual(len(reward_list),1)

                    total_page, cur_page, reward_list =yield reward_man.get_my_reward_helper(user, reward_man.STATUS["pending"], 1)
                    self.assertEqual(total_page,0)
                    self.assertEqual(cur_page,1)
                    self.assertEqual(len(reward_list),0)

                    total_page, cur_page, reward_list =yield reward_man.get_my_reward_helper(user, reward_man.STATUS["expired"], 1)
                    self.assertEqual(total_page,1)
                    self.assertEqual(cur_page,1)
                    # self.assertEqual(len(reward_list),1)

                    total_page, cur_page, reward_list =yield reward_man.get_my_reward_helper(user, None, 1)
                    self.assertEqual(total_page,1)
                    self.assertEqual(cur_page,1)
                    self.assertEqual(len(reward_list),2)

    '''
    @tornado.testing.gen_test(timeout=3)
    def test_thumb_up_and_down(self):
        sync_db = MongoClient()
        with mock.patch("pymongo.MongoReplicaSetClient", return_value=sync_db) as what_ever:
            with mock.patch('redis_lru_scheduler.RedisDelegate', mock.MagicMock()) as w:
                with mock.patch('redis.StrictRedis.publish', mock.MagicMock()) as what_ever2:
                    from fb_manager import FBCoinManager
                    from reward_study_manager import RewardManager

                    db = motor.MotorClient().fbt_test
                    FBCoinManager.set_db(db)
                    reward_man = RewardManager(mock.MagicMock(), db, FBCoinManager)
                    yield reward_man.clear_db_for_test()
                    yield FBCoinManager.clear_db()

                    user = "123@123.com"
                    uid = 123
                    nick_name = "bone"
                    reward_fb = 321
                    reward = {"university": 'xxx',
                              "college": 'xxx',
                              "course": 'xxxx',
                              "teacher": 'xxx',
                              "tag": 'xxx',
                              "year": 'xxx',
                              "description": "xxx",
                              "expire_days": 2,
                              "total_fb": reward_fb
                              }
                    reward_id = yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)
                    user2 = "123@tre.com"
                    uid2 = 12334
                    nick_name2 = "kaka"
                    resource_link = "http://xxx.xxx.xxx/filexxx"

                    r = redis.StrictRedis()
                    user_msg_center = UserMsgCenter(r)
                    r.flushdb()
                    reward_man.set_user_msg_center(user_msg_center)
                    yield reward_man.upload_reward_resource(reward_id, user2, uid2, nick_name2, resource_link,"filename", 1234)

                    reward2 = yield reward_man.get_a_reward(reward_id)
                    self.assertEqual(len(reward2["resource_list"][0]["supporters"]), 0)
                    user_nick3 = "what ever"
                    yield reward_man.thumb_up(reward_id, 0 , user_nick3)
                    reward2 = yield reward_man.get_a_reward(reward_id)
                    self.assertEqual(len(reward2["resource_list"][0]["supporters"]), 1)
                    yield reward_man.thumb_up(reward_id, 0 , user_nick3)
                    reward2 = yield reward_man.get_a_reward(reward_id)
                    self.assertEqual(len(reward2["resource_list"][0]["supporters"]), 1)

                    user_nick4 = "xxxxx some user"
                    yield reward_man.thumb_up(reward_id, 0 , user_nick4)
                    reward2 = yield reward_man.get_a_reward(reward_id)
                    self.assertEqual(len(reward2["resource_list"][0]["supporters"]), 2)

                    reward2 = yield reward_man.get_a_reward(reward_id)
                    self.assertEqual(len(reward2["resource_list"][0]["protesters"]), 0)
                    user_nick4 = "xxxxx some user"
                    yield reward_man.thumb_down(reward_id, 0 , user_nick4)
                    reward2 = yield reward_man.get_a_reward(reward_id)
                    self.assertEqual(len(reward2["resource_list"][0]["protesters"]), 1)
    '''

    @tornado.testing.gen_test(timeout=3)
    def test_user_related_reward(self):
        sync_db = MongoClient()
        with mock.patch("pymongo.MongoReplicaSetClient", return_value=sync_db) as what_ever:
            with mock.patch('redis_lru_scheduler.RedisDelegate', mock.MagicMock()) as w:
                with mock.patch('redis_handler.RedisHandler.redis_client', return_value=redis.StrictRedis()) as whate_ever:
                    from coin_manager import CoinManager
                    from reward_study_manager import RewardManager

                    db = motor.MotorClient().fbt_test
                    # reward_man = RewardManager(mock.MagicMock(), db, FBCoinManager)
                    coin_man = CoinManager(db, redis.StrictRedis())
                    reward_man = RewardManager(mock.MagicMock(), db, coin_man)
                    r = redis.StrictRedis()
                    user_msg_center = SystemMessageProxy(r)
                    r.flushdb()
                    reward_man.set_user_msg_center(user_msg_center)
                    yield reward_man.clear_db_for_test()

                    user = "123@123.com"
                    uid = 123
                    nick_name = "bone"
                    reward_fb = 321
                    reward = {"university": 'xxx',
                              "college": 'xxx',
                              "course": 'xxxx',
                              "teacher": 'xxx',
                              "tag": 'xxx',
                              "year": 'xxx',
                              "description": "xxx",
                              "expire_days": 2,
                              "total_fb": reward_fb
                              }
                    reward_id = yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)
                    user2 = "123@tre.com"
                    uid2 = 12334
                    nick_name2 = "kaka"
                    how_much = 10
                    total_page, cur_page, reward_list = yield reward_man.user_related_reward(user, 1)
                    self.assertEqual(total_page, 0)
                    self.assertEqual(cur_page, 1)
                    self.assertEqual(len(reward_list), 0)

                    yield reward_man.append_reward(reward_id, how_much, user2, uid2, nick_name2)
                    total_page, cur_page, reward_list = yield reward_man.user_related_reward(user2, 1)
                    self.assertEqual(total_page, 1)
                    self.assertEqual(cur_page, 1)
                    self.assertEqual(len(reward_list), 1)

                    user3 = "31123@test.com"
                    uid3 = 4321
                    nick_name3 = "kaka222"
                    resource_link = "http://xxx.xxx.xxx/filexxx"
                    yield reward_man.upload_reward_resource(reward_id, user3, uid3, nick_name3, resource_link, "filename", 1234)
                    total_page, cur_page, reward_list = yield reward_man.user_related_reward(user3, 1)
                    self.assertEqual(total_page, 1)
                    self.assertEqual(cur_page, 1)
                    self.assertEqual(len(reward_list), 1)

                    reward_id2 = yield reward_man.issue_reward(reward.copy(), user, uid, nick_name)
                    yield reward_man.append_reward(reward_id2, how_much, user3, uid3, nick_name3)
                    total_page, cur_page, reward_list = yield reward_man.user_related_reward(user3, 1)
                    self.assertEqual(total_page, 1)
                    self.assertEqual(cur_page, 1)
                    self.assertEqual(len(reward_list), 2)

if __name__ == '__main__':
    import unittest
    unittest.main()

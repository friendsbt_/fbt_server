# -*- coding: utf-8 -*-
__author__ = 'bone'

import sys
import os.path

fbt_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

sys.path.append(fbt_path)

from message_manager import MessageManager
from system_msg_rpc_server import SystemMessageRPCServer, RPC_PORT

from random import randint
from tornado.escape import utf8
import tornado.testing
import tornado.gen
import motor
import redis


class ChatMessageManagerTestCase(tornado.testing.AsyncTestCase):
    def setUp(self):
        super(ChatMessageManagerTestCase, self).setUp()
        db = motor.MotorClient().fbt_test
        redis_db = redis.StrictRedis()
        redis_db.flushdb()
        self.msg_buffer = MessageManager(db, redis_db)

    @tornado.testing.gen_test(timeout=5)
    def test_new_private_message(self):
        user_from = "bone@test.com"
        user_to = "xixi@test.com"
        msg_content = "hello world"
        msg_cnt = self.msg_buffer.get_msg_cnt(user_to)
        self.assertEqual(msg_cnt, 0)
        message_list = self.msg_buffer.get_msg(user_to)
        self.assertEqual(len(message_list), 0)

        result = self.msg_buffer.wait_for_message(user_to)
        ok = self.msg_buffer.new_private_message(user_from, user_to, msg_content, {}, {})
        self.assertTrue(ok)
        message = yield result
        self.assertEqual(message["user_from"], user_from)
        self.assertEqual(message["user_to"], user_to)
        self.assertEqual(message["content"], msg_content)
        self.assertEqual(message["type"], 1)
        msg_cnt = self.msg_buffer.get_msg_cnt(user_to)
        self.assertEqual(msg_cnt, 1)
        message_list = self.msg_buffer.get_msg(user_to)
        self.assertEqual(len(message_list), 1)
        self.assertEqual(message_list[0]["user_from"], user_from)
        self.assertEqual(message_list[0]["user_to"], user_to)
        self.assertEqual(message_list[0]["content"], msg_content)
        self.assertEqual(message_list[0]["type"], 1)
        msg_cnt = self.msg_buffer.get_msg_cnt(user_from)
        self.assertEqual(msg_cnt, 1)

    @tornado.testing.gen_test(timeout=5)
    def test_new_private_offline_message(self):
        user_from = "bone@test.com"
        user_to = "xixi@test.com"
        msg_content = "hello world"
        msg_cnt = self.msg_buffer.get_msg_cnt(user_to)
        self.assertEqual(msg_cnt, 0)
        message_list = self.msg_buffer.get_msg(user_to)
        self.assertEqual(len(message_list), 0)
        ok = self.msg_buffer.new_private_message(user_from, user_to, msg_content, {}, {})
        self.assertFalse(ok)
        msg_cnt = self.msg_buffer.get_msg_cnt(user_to)
        self.assertEqual(msg_cnt, 1)
        message_list = self.msg_buffer.get_msg(user_to)
        self.assertEqual(len(message_list), 1)
        self.assertEqual(message_list[0]["user_from"], user_from)
        self.assertEqual(message_list[0]["user_to"], user_to)
        self.assertEqual(message_list[0]["content"], msg_content)
        self.assertEqual(message_list[0]["type"], 1)

    @tornado.testing.gen_test(timeout=5)
    def test_cancel_wait(self):
        user_from = "bone@test.com"
        user_to = "xixi@test.com"
        msg_content = "hello world"
        msg_cnt = self.msg_buffer.get_msg_cnt(user_to)
        self.assertEqual(msg_cnt, 0)
        message_list = self.msg_buffer.get_msg(user_to)
        self.assertEqual(len(message_list), 0)

        result = self.msg_buffer.wait_for_message(user_to)
        self.msg_buffer.cancel_wait(user_to)
        ok = self.msg_buffer.new_private_message(user_from, user_to, msg_content, {}, {})
        self.assertFalse(ok)
        message = yield result
        self.assertEqual(message, None)
        message_list = self.msg_buffer.get_msg(user_to)
        self.assertEqual(len(message_list), 1)
        self.assertEqual(message_list[0]["user_from"], user_from)
        self.assertEqual(message_list[0]["user_to"], user_to)
        self.assertEqual(message_list[0]["content"], msg_content)
        self.assertEqual(message_list[0]["type"], 1)

    @tornado.testing.gen_test(timeout=5)
    def test_new_system_msg(self):
        user_to = "xixi@test.com"
        msg_content = "hello world"
        msg_cnt = self.msg_buffer.get_msg_cnt(user_to)
        self.assertEqual(msg_cnt, 0)
        message_list = self.msg_buffer.get_msg(user_to)
        self.assertEqual(len(message_list), 0)
        result = self.msg_buffer.wait_for_message(user_to)
        self.msg_buffer.new_system_message(msg_content, user_to)
        message = yield result
        self.assertEqual(message["user_from"], "system")
        self.assertEqual(message["user_to"], user_to)
        self.assertEqual(message["content"], msg_content)
        self.assertEqual(message["type"], 0)
        msg_cnt = self.msg_buffer.get_msg_cnt(user_to)
        self.assertEqual(msg_cnt, 1)
        message_list = self.msg_buffer.get_msg(user_to)
        self.assertEqual(len(message_list), 1)
        self.assertEqual(message_list[0]["user_from"], "system")
        self.assertEqual(message_list[0]["user_to"], user_to)
        self.assertEqual(message_list[0]["content"], msg_content)
        self.assertEqual(message_list[0]["type"], 0)

    # @tornado.testing.gen_test(timeout=5)
    # def test_rpc_send_system_message(self):
    #     rpc_msg_server = SystemMessageRPCServer(self.msg_buffer)
    #     rpc_msg_server.listen(RPC_PORT)
    #
    #     user_to = "xixi@test.com"
    #     msg_content = "hello world"
    #     msg_cnt = self.msg_buffer.get_msg_cnt(user_to)
    #     self.assertEqual(msg_cnt, 0)
    #     message_list = self.msg_buffer.get_msg(user_to)
    #     self.assertEqual(len(message_list), 0)
    #     result = self.msg_buffer.wait_for_message(user_to)
    #     rpc_msg_server.send_system_msg(msg_content, user_to)
    #     message = yield result
    #     self.assertEqual(message["user_from"], "system")
    #     self.assertEqual(message["user_to"], user_to)
    #     self.assertEqual(message["content"], msg_content)
    #     self.assertEqual(message["type"], 0)
    #     msg_cnt = self.msg_buffer.get_msg_cnt(user_to)
    #     self.assertEqual(msg_cnt, 1)

    @tornado.testing.gen_test(timeout=5)
    def test_recent_message(self):
        user_from = "bone@test.com"
        user_to = "xixi@test.com"
        msg_content = "hello world"
        message_list, unread_msg_cnt = self.msg_buffer.get_recent_msg(user_to)
        self.assertEqual(unread_msg_cnt, 0)
        self.assertEqual(len(message_list), 0)

        result = self.msg_buffer.wait_for_message(user_to)
        ok = self.msg_buffer.new_private_message(user_from, user_to, msg_content, {}, {})
        self.assertTrue(ok)
        message = yield result

        self.msg_buffer.new_system_message(msg_content, user_to)

        message_list, unread_msg_cnt = self.msg_buffer.get_recent_msg(user_to)
        self.assertEqual(unread_msg_cnt, 2)
        self.assertEqual(len(message_list), 2)
        self.assertEqual(message_list[1]["id"], message["id"])

        self.msg_buffer.read_msg(user_to, message["id"])
        message_list, unread_msg_cnt = self.msg_buffer.get_recent_msg(user_to)
        self.assertEqual(unread_msg_cnt, 1)
        message_list = self.msg_buffer.get_msg(user_to)
        self.assertEqual(len(message_list), 2)
        self.assertEqual(message_list[1]["id"], message["id"])
        self.assertEqual(message_list[1]["is_read"], 1)
        self.assertEqual(message_list[0]["is_read"], 0)

if __name__ == '__main__':
    import unittest
    unittest.main()

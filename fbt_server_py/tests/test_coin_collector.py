__author__ = 'bone-lee'

import os, sys
fbt_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

sys.path.append(fbt_path)

from motor import MotorClient
from coin_manager import CoinStatisticCollector

import tornado.gen
import tornado.testing
import tornado.gen
import tornado.testing
import tornado.ioloop
import redis
from time import time
from datetime import datetime
from datetime import timedelta
import mock


class CoinCollectorTestCase(tornado.testing.AsyncTestCase):
    def setUp(self):
        super(CoinCollectorTestCase, self).setUp()

    @tornado.testing.gen_test(timeout=3)
    def test_next_day(self):
        c = CoinStatisticCollector()
        dt = datetime(2016, 2, 28)
        dt2 = c.next_day(dt)
        self.assertEqual(dt2.month, 2)
        self.assertEqual(dt2.day, 29)

        dt = datetime(2016, 2, 29)
        dt2 = c.next_day(dt)
        self.assertEqual(dt2.month, 3)
        self.assertEqual(dt2.day, 1)

    @tornado.testing.gen_test(timeout=3)
    def test_next_monday(self):
        c = CoinStatisticCollector()
        dt = datetime(2016, 2, 28)
        dt2 = c.next_monday(dt)
        self.assertEqual(dt2.month, 2)
        self.assertEqual(dt2.day, 29)
       
        dt = datetime(2016, 2, 1)
        dt2 = c.next_monday(dt)
        self.assertEqual(dt2.month, 2)
        self.assertEqual(dt2.day, 8)
       
        dt = datetime(2016, 2, 7)
        dt2 = c.next_monday(dt)
        self.assertEqual(dt2.month, 2)
        self.assertEqual(dt2.day, 8)

        dt = datetime(2016, 2, 15)
        dt2 = c.next_monday(dt)
        self.assertEqual(dt2.month, 2)
        self.assertEqual(dt2.day, 22)

        dt = datetime(2016, 3, 28)
        dt2 = c.next_monday(dt)
        self.assertEqual(dt2.month, 4)
        self.assertEqual(dt2.day, 4)
        # print "next_monday pass test"

    @tornado.testing.gen_test(timeout=3)
    def test_first_day_of_next_month(self):
        c = CoinStatisticCollector()
        dt = datetime(2016, 2, 28)
        dt2 = c.first_day_of_next_month(dt)
        self.assertEqual(dt2.year, 2016)
        self.assertEqual(dt2.month, 3)
        self.assertEqual(dt2.day, 1)

        dt = datetime(2016, 12, 28)
        dt2 = c.first_day_of_next_month(dt)
        self.assertEqual(dt2.year, 2017)
        self.assertEqual(dt2.month, 1)
        self.assertEqual(dt2.day, 1)
        # print "first_day_of_next_month pass test"

    @tornado.testing.gen_test(timeout=3)
    def test_date2time(self):
        c = CoinStatisticCollector()
        t = c.date2time(datetime.now())
        self.assertEqual(t, long(time()))

    @tornado.testing.gen_test(timeout=3)
    def test_transfer_coin_not_expire(self):
        uid = 1234
        redis_db = redis.StrictRedis()
        redis_db.flushdb()
        t = datetime.now() + timedelta(hours=1)
        with mock.patch('coin_manager.CoinStatisticCollector.first_day_of_next_month',
                return_value=t) as whate_ever:
            with mock.patch('coin_manager.CoinStatisticCollector.next_day',
                    return_value=t) as whate_ever2:
                c = CoinStatisticCollector(redis_db)
                c.transfer_coin(uid, 100)
                daily_coin, monthly_coin, total_coin = c.get_total_income(uid)
                self.assertEqual(100, daily_coin)
                self.assertEqual(100, monthly_coin)
                self.assertEqual(100, total_coin)

                c.transfer_coin(uid, 100)
                daily_coin, monthly_coin, total_coin = c.get_total_income(uid)
                self.assertEqual(200, daily_coin)
                self.assertEqual(200, monthly_coin)
                self.assertEqual(200, total_coin)

                c.transfer_coin(uid, 0)
                daily_coin, monthly_coin, total_coin = c.get_total_income(uid)
                self.assertEqual(200, daily_coin)
                self.assertEqual(200, monthly_coin)
                self.assertEqual(200, total_coin)

    @tornado.testing.gen_test(timeout=3)
    def test_transfer_coin_expired(self):
        uid = 1234
        redis_db = redis.StrictRedis()
        redis_db.flushdb()
        t = datetime.now() - timedelta(days=1)
        with mock.patch('coin_manager.CoinStatisticCollector.first_day_of_next_month',
                return_value=t) as whate_ever3:
            with mock.patch('coin_manager.CoinStatisticCollector.next_day',
                    return_value=t) as whate_ever4:
                c = CoinStatisticCollector(redis_db)
                c.transfer_coin(uid, 100)
                daily_coin, monthly_coin, total_coin = c.get_total_income(uid)
                self.assertEqual(100, daily_coin)
                self.assertEqual(100, monthly_coin)
                self.assertEqual(100, total_coin)

                c.transfer_coin(uid, 200)
                daily_coin, monthly_coin, total_coin = c.get_total_income(uid)
                self.assertEqual(200, daily_coin)
                self.assertEqual(200, monthly_coin)
                self.assertEqual(300, total_coin)
        
if __name__ == '__main__':
    import unittest
    unittest.main()

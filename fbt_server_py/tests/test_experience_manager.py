# coding: utf-8
__author__ = 'bone'

import sys
import os.path

fbt_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

sys.path.append(fbt_path)

import mock

#################################################################
######  mock redis locally ##########
# It's bad idea
# import constant
# constant.REDIS_MASTER_HOST = "127.0.0.1"
# constant.REDIS_PORT = (6379, 6379, 6379, 6379, 6379, 6379)
# constant.REDIS_PWD = (None, None, None, None, None, None)
################################################################

from experience_tag_manager import QuestionTagManager
from QA_manager import UserInterestedTagRecorder
# from QA_manager import QAManager
import tornado.testing
import tornado.gen
from motor import MotorClient
import redis
# from users_manager import UserManager
from university_db import UniversityDB
from university_course_manager import RedisDBManager
from coin_manager import CoinManager


class MockHotBorad(object):
    def __init__(self, db, ioloop):
        pass

    def __getattr__(self, attr_name):
        return self.mock_any_method

    def mock_any_method(self, *args, **kwargs):
        # such as folows
        return 1

        # def generate_hot_in_db(self):
        #     pass
        #
        # def user_tag_change(self, user, university):
        #     pass
        #
        # def init_hot_in_cache(self, Qid, QageInSeconds, Qupdated, Qviews=1, Qanswers=0, Qscore=0, Ascores=0):
        #     pass
        #
        # def inc_views(self, Qid, Qtag):
        #     pass
        #
        # def inc_answers(self, Qid, Qtag):
        #     pass
        #
        # def inc_question_score(self, Qid, Qtag):
        #     pass
        #
        # def dec_question_score(self, Qid, Qtag):
        #     pass
        #
        # def inc_answer_score(self, Qid, Qtag):
        #     pass
        #
        # def dec_answer_score(self, Qid, Qtag):
        #     pass


class ExperienceManagerTestCase(tornado.testing.AsyncTestCase):
    def setUp(self):
            from QA_manager import QAManager
            from users_manager import UserManager

            super(ExperienceManagerTestCase, self).setUp()
            self.db = MotorClient().fbt_test
            redis_cache = redis.StrictRedis()
            self.redis_cache = redis_cache
            self.coin_man = CoinManager(db=self.db, redis_cache=redis_cache)
            self.experience_tag_man = QuestionTagManager(redis_cache, redis_cache)
            self.user_manager = UserManager(db=self.db, redis_cache=redis_cache,
                                            redis_db_man=RedisDBManager(redis_db=redis_cache, redis_cache=redis_cache),
                                            es_port=9200, es_host="localhost")
            self.experience_man = QAManager(db=self.db, fb_manager=self.coin_man, user_manager=self.user_manager,
                                            exp_tag_manager=self.experience_tag_man, redis_cache=redis_cache,
                                            redis_db=redis_cache, es_host="localhost", es_port=9200)
            self.tag_man = UserInterestedTagRecorder(redis_cache)
            self.io_loop.run_sync(self.fixture_setup)

    @tornado.gen.coroutine
    def fixture_setup(self):
            user_manager = self.user_manager
            self.redis_cache.flushdb()
            self.user = "test@test.com"
            passwd, user_icon_url, real_name, school, college, nick, gender = "123", "NOT SET", "bone", "NOT SET", "NOT SET", "bonelee", "MALE"
            yield user_manager.register_user(self.user, passwd, user_icon_url, real_name, school, college, nick, gender)
            yield self.db.questions.remove()
            yield self.db.answers.remove()

    def tearDown(self):
        pass

    @tornado.testing.gen_test(timeout=3)
    def test_insert_user_interested_tag(self):
        user = "test@test.com"
        tags_with_class = ["考研:计算机", "学校:爱情"]
        self.tag_man.insert_interested_tag(user, tags_with_class)
        tags = self.tag_man.top_users_interested_tag(user)
        self.assertEqual(len(tags), 2)

    @tornado.testing.gen_test(timeout=3)
    def test_get_question_overview(self):
        total_page, cur_page, experience_list = yield self.experience_man.get_experience_overview(1, use_cache=False)
        self.assertEqual(len(experience_list), 0)
        self.assertEqual(total_page, 0)
        self.assertEqual(cur_page, 1)

        experience = {"class": u"留学", "tags": ["test-tag", "another_tag"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, self.user, "uid", "nick name", "test.jpg")
        self.assertTrue(experience_id is not None)
        total_page, cur_page, experience_list = yield self.experience_man.get_question_overview(1, use_cache=False)
        self.assertEqual(len(experience_list), 1)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)
        total_page, cur_page, experience_list = yield self.experience_man.get_question_overview(0, use_cache=False)
        self.assertEqual(len(experience_list), 1)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

    @tornado.testing.gen_test(timeout=3)
    def test_post_experience(self):
        total_page, cur_page, experience_list = yield self.experience_man.get_experience_overview(1, use_cache=False)
        self.assertEqual(len(experience_list), 0)
        self.assertEqual(total_page, 0)
        self.assertEqual(cur_page, 1)

        experience = {"class": u"留学", "tags": ["test-tag", "another_tag"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, self.user, "uid", "nick name", "test.jpg")
        self.assertTrue(experience_id is not None)
        total_page, cur_page, experience_list = yield self.experience_man.get_experience_overview(1, use_cache=False)
        self.assertEqual(len(experience_list), 1)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

    @tornado.testing.gen_test(timeout=3)
    def test_hide_question(self):
        total_page, cur_page, experience_list = yield self.experience_man.get_experience_overview(1, use_cache=False)
        self.assertEqual(len(experience_list), 0)
        self.assertEqual(total_page, 0)
        self.assertEqual(cur_page, 1)

        experience = {"class": u"留学", "tags": ["test-tag", "another_tag"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, self.user, "uid", "nick name", "test.jpg")
        self.assertTrue(experience_id is not None)
        total_page, cur_page, experience_list = yield self.experience_man.get_experience_overview(1, use_cache=False)
        self.assertEqual(len(experience_list), 1)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

        total_page, cur_page, experience_list = yield self.experience_man.get_question_overview(1, use_cache=False)
        self.assertEqual(len(experience_list), 1)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

        yield self.experience_man.hide_question(experience_id)
        total_page, cur_page, experience_list = yield self.experience_man.get_experience_overview(1, use_cache=False)
        self.assertEqual(len(experience_list), 0)
        self.assertEqual(total_page, 0)
        self.assertEqual(cur_page, 1)

        total_page, cur_page, experience_list = yield self.experience_man.get_question_overview(1, use_cache=False)
        self.assertEqual(len(experience_list), 0)
        self.assertEqual(total_page, 0)
        self.assertEqual(cur_page, 1)

    @tornado.testing.gen_test(timeout=3)
    def test_get_experience_by_id(self):
        total_page, cur_page, experience_list = yield self.experience_man.get_experience_overview(1, use_cache=False)
        self.assertEqual(len(experience_list), 0)
        self.assertEqual(total_page, 0)
        self.assertEqual(cur_page, 1)

        experience = {"class": u"留学", "tags": ["test-tag", "another_tag"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, self.user, "uid", "nick name", "test.jpg")
        self.assertTrue(experience_id is not None)
        experience = yield self.experience_man.get_question_by_id(experience_id)
        experience2 = yield self.experience_man.get_question_by_id(experience_id, False)
        self.assertDictEqual(experience, experience2)

    @tornado.testing.gen_test(timeout=3)
    def test_inc_experience_thumb_up_num(self):
        user = "test11@test.com"
        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                                "test.jpg")
        user2 = "test2@test.com"
        ok, _, _, qid = yield self.experience_man.inc_thumb_up_num(experience_id, user2)
        self.assertEqual(qid, experience_id)
        self.assertTrue(ok)
        experience = yield self.experience_man.get_question_by_id(experience_id, False)
        self.assertEqual(experience["thumb_up_num"], 1)
        user2 = "test2@test.com"
        yield self.experience_man.inc_thumb_up_num(experience_id, user2)
        experience = yield self.experience_man.get_question_by_id(experience_id, False)
        self.assertEqual(experience["thumb_up_num"], 1)
        user3 = "test3@test.com"
        yield self.experience_man.inc_thumb_up_num(experience_id, user3)
        experience = yield self.experience_man.get_question_by_id(experience_id, False)
        self.assertEqual(experience["thumb_up_num"], 2)

    @tornado.testing.gen_test(timeout=3)
    def test_get_all_tags(self):
        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, "test@test.com", "uid", "nick name",
                                                                "test.jpg")
        tags = self.experience_man.get_all_experience_tags()
        self.assertDictEqual(tags, {"考研:文学": '1', "考研:计算机": '1'})
        experience = {"class": u"考研", "tags": [u"文学", u"计算所"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, "test'2'@test.com", "uid", "nick name",
                                                                "test.jpg")
        tags = self.experience_man.get_all_experience_tags()
        self.assertDictEqual(tags, {"考研:文学": '2', "考研:计算机": '1', "考研:计算所": '1'})
        experience = {"class": u"实习", "tags": [u"微软", u"谷歌"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, "test'2'@test.com", "uid", "nick name",
                                                                "test.jpg")
        tags = self.experience_man.get_all_experience_tags()
        self.assertDictEqual(tags, {"考研:文学": '2', "考研:计算机": '1', "考研:计算所": '1', "实习:微软": '1', "实习:谷歌": '1'})
        print "test get all tags OK"

    @tornado.testing.gen_test(timeout=3)
    def test_thanks_post_experience(self):
        user = u"test11@test.com"
        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                                "test.jpg")
        user2 = u"test2@test.com"
        user2_nick = "what ever"
        ok, publisher, title,_, publisher_uid = yield self.experience_man.thanks(experience_id, user2, user2_nick)
        self.assertTrue(ok)
        self.assertEqual(_, experience_id)
        self.assertEqual(publisher, user)
        ok, publisher, title,_, publisher_uid = yield self.experience_man.thanks(experience_id, user2, user2_nick)
        self.assertTrue(ok)
        self.assertEqual(_, experience_id)
        self.assertEqual(publisher, user)
        user3 = u"test33@test.com"
        user3_nick = "wwww"
        ok, publisher, title,_, publisher_uid = yield self.experience_man.thanks(experience_id, user3, user3_nick)
        self.assertTrue(ok)
        self.assertEqual(_, experience_id)
        self.assertEqual(publisher, user)
        answer = {"question_id": experience_id, "content": "Good!!!"}
        user333 = "test333@test.com"
        udb = UniversityDB()
        university, college = udb.random_choose_college_of_university()
        answer_id, _ = yield self.experience_man.post_answer(answer, user333, "uid", "nick_name", "user_icon", university,
                                                             college)
        ok, publisher, title,_, publisher_uid = yield self.experience_man.thanks(answer_id, user3, user3_nick)
        self.assertTrue(ok)
        self.assertEqual(_, experience_id)
        self.assertEqual(publisher, user)

    @tornado.testing.gen_test(timeout=3)
    def test_collect_experience(self):
        yield self.db.user_questions.remove({})
        user = u"test11@test.com"
        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                                "test.jpg")
        user2 = u"test2@test.com"
        total_page, cur_page, experience_list = yield self.experience_man.get_collected_experience(user2, 1)
        self.assertEqual(len(experience_list), 0)
        self.assertEqual(total_page, 0)
        self.assertEqual(cur_page, 1)

        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id2 = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                                 "test.jpg")
        yield self.experience_man.collect_experience(user2, experience_id)
        total_page, cur_page, experience_list = yield self.experience_man.get_collected_experience(user2, 1)
        self.assertEqual(len(experience_list), 1)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

        yield self.experience_man.collect_experience(user2, experience_id2)
        total_page, cur_page, experience_list = yield self.experience_man.get_collected_experience(user2, 1)
        self.assertEqual(len(experience_list), 2)
        self.assertEqual(experience_list[0]["id"], experience_id2)
        self.assertEqual(experience_list[1]["id"], experience_id)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

    @tornado.testing.gen_test(timeout=300)
    def test_post_answer(self):
        yield self.db.answers.remove({})
        user = u"test11@test.com"
        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        question_id = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                              "test.jpg")
        exp = yield self.experience_man.get_question_by_id(question_id)
        self.assertEqual(exp["reply_num"], 0)

        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        question_id2 = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                              "test.jpg")

        total_page, cur_page, experience_list = yield self.experience_man.get_question_overview(0, use_cache=False)
        self.assertEqual(len(experience_list), 2)
        self.assertEqual(experience_list[0]["id"], question_id2)
        self.assertEqual(experience_list[1]["id"], question_id)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

        total_page, cur_page, answer_list = yield self.experience_man.get_answers(question_id, 1)
        self.assertEqual(len(answer_list), 0)
        answer = {"question_id": question_id, "content": "Good!!!"}
        user2 = "test@test.com"
        udb = UniversityDB()
        university, college = udb.random_choose_college_of_university()
        answer_id, _ = yield self.experience_man.post_answer(answer, user2, "uid", "nick_name", "user_icon", university,
                                                             college)
        self.assertTrue(answer_id is not None)
        total_page, cur_page, answer_list = yield self.experience_man.get_answers(question_id, 1)
        self.assertEqual(len(answer_list), 1)
        self.assertEqual(answer_list[0]["id"], answer_id)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

        exp = yield self.experience_man.get_question_by_id(question_id, use_cache=False)
        self.assertEqual(exp["reply_num"], 1)

        answer = {"question_id": question_id, "content": "bad!!!"}
        user3 = "test3@test.com"
        university, college = udb.random_choose_college_of_university()
        answer_id2, _ = yield self.experience_man.post_answer(answer, user3, "uid", "nick_name", "user_icon",
                                                              university, college)

        total_page, cur_page, answer_list = yield self.experience_man.get_answers(question_id, 1, use_cache=False)
        self.assertEqual(len(answer_list), 2)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

        exp = yield self.experience_man.get_question_by_id(question_id, False)
        self.assertEqual(exp["reply_num"], 2)

        total_page, cur_page, experience_list = yield self.experience_man.get_question_overview(0, use_cache=False)
        self.assertEqual(len(experience_list), 2)
        self.assertEqual(experience_list[0]["id"], question_id)
        self.assertEqual(experience_list[1]["id"], question_id2)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

    @tornado.testing.gen_test(timeout=3)
    def test_inc_thumb_up_answer(self):
        yield self.db.answers.remove({})
        user = u"test11@test.com"
        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        question_id = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                              "test.jpg")
        exp = yield self.experience_man.get_question_by_id(question_id)
        self.assertDictEqual(exp["best_answer"], {})

        answer = {"question_id": question_id, "content": "Good!!!"}
        user2 = "test@test.com"
        udb = UniversityDB()
        university, college = udb.random_choose_college_of_university()
        answer_id, _ = yield self.experience_man.post_answer(answer, user2, "uid", "nick_name", "user_icon", university,
                                                             college)

        exp = yield self.experience_man.get_question_by_id(question_id, False)
        self.assertEqual(exp["best_answer"]["id"], answer_id)
        self.assertEqual(exp["best_answer"]["content"], "Good!!!")

        answer = {"question_id": question_id, "content": "bad!!!"}
        user3 = "test3@test.com"
        university, college = udb.random_choose_college_of_university()
        answer_id2, _ = yield self.experience_man.post_answer(answer, user3, "uid", "nick_name", "user_icon",
                                                              university, college)

        exp = yield self.experience_man.get_question_by_id(question_id, False)
        self.assertEqual(exp["best_answer"]["id"], answer_id)
        self.assertEqual(exp["best_answer"]["content"], "Good!!!")

        ok, _, _, qid = yield self.experience_man.inc_thumb_up_num(answer_id2, "user what ever", True)
        self.assertEqual(qid, question_id)
        self.assertTrue(ok)
        ok,_, _, qid = yield self.experience_man.inc_thumb_up_num(answer_id2, "user what ever", True)
        self.assertEqual(qid, question_id)
        self.assertFalse(ok)

        exp = yield self.experience_man.get_question_by_id(question_id, False)
        self.assertEqual(exp["best_answer"]["id"], answer_id2)
        self.assertEqual(exp["best_answer"]["content"], "bad!!!")

        yield self.experience_man.inc_thumb_up_num(answer_id, "user what ever2", True)
        yield self.experience_man.inc_thumb_up_num(answer_id, "user what ever3", True)
        yield self.experience_man.inc_thumb_up_num(answer_id, "user what ever4", True)

        exp = yield self.experience_man.get_question_by_id(question_id, False)
        self.assertEqual(exp["best_answer"]["id"], answer_id)
        self.assertEqual(exp["best_answer"]["content"], "Good!!!")
        self.assertEqual(exp["best_answer"]["publisher"], user2)
        print "test thumb up answer OK"

    @tornado.testing.gen_test(timeout=3)
    def test_user_question(self):
        yield self.db.answers.remove({})
        yield self.db.user_questions.remove({})
        user = u"test11@test.com"
        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}

        total_page, cur_page, answer_list = yield self.experience_man.get_posted_experience(user, 1)
        self.assertEqual(len(answer_list), 0)
        self.assertEqual(total_page, 0)
        self.assertEqual(cur_page, 1)

        question_id = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                              "test.jpg")

        total_page, cur_page, answer_list = yield self.experience_man.get_posted_experience(user, 1)
        self.assertEqual(len(answer_list), 1)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

        answer = {"question_id": question_id, "content": "bad!!!"}
        udb = UniversityDB()
        university, college = udb.random_choose_college_of_university()

        answer_id, _ = yield self.experience_man.post_answer(answer, user, "uid", "nick_name", "user_icon", university,
                                                             college)
        self.assertTrue(answer_id is not None)
        total_page, cur_page, answer_list = yield self.experience_man.get_answered_experience(user, 1)
        self.assertEqual(len(answer_list), 1)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

        answer = {"question_id": question_id, "content": "bad!!!"}
        user2 = "test33@test.com"
        answer_id, _ = yield self.experience_man.post_answer(answer, user2, "uid", "nick_name", "user_icon", university,
                                                             college)

        total_page, cur_page, answer_list = yield self.experience_man.get_answered_experience(user2, 1)
        self.assertEqual(len(answer_list), 1)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        question_id2 = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                               "test.jpg")
        answer = {"question_id": question_id2, "content": "bad!!!"}
        answer_id2, _ = yield self.experience_man.post_answer(answer, user2, "uid", "nick_name", "user_icon", university,
                                                             college)
        total_page, cur_page, answer_list = yield self.experience_man.get_answered_experience(user2, 1, False)
        self.assertEqual(len(answer_list), 2)
        self.assertEqual(answer_list[0]["id"], answer_id2)
        self.assertEqual(answer_list[1]["id"], answer_id)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

    @tornado.testing.gen_test(timeout=3)
    def test_get_recommended_users2(self):
        yield self.db.users.remove({})
        user = "test@test.com"

        total_page, cur_page, experience_list = yield self.experience_man.get_recommended_users2(user, None, 1, False)
        self.assertEqual(len(experience_list), 0)
        self.assertEqual(total_page, -1)
        self.assertEqual(cur_page, 1)

        star_user1 = "test1@test.com"
        star_user2 = "test2@test.com"

        yield self.user_manager.register_user(user, "password", "icon", "real name",
                                              "school", "college", "nick", "M")
        yield self.user_manager.register_user(star_user1, "password", "icon", "real name",
                                              "school", "college", "nick", "M")
        yield self.user_manager.register_user(star_user2, "password", "icon", "real name",
                                              "school", "college", "nick", "M")

        # self.user_manager.insert_my_interested_tags(user, "考研:计算机")
        # self.user_manager.insert_my_interested_tags(user, "考研:计算所")
        self.experience_man.insert_user_interested_tags(user, ["考研:计算机", "考研:计算所"])

        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id1 = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                                 "test.jpg")

        experience = {"class": u"考研", "tags": [u"计算所", u"计算"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id2 = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                                 "test.jpg")
        total_page, cur_page, experience_list = yield self.experience_man.get_recommended_users2(user, None, 1, False)
        self.assertEqual(len(experience_list), 0)
        self.assertEqual(total_page, 0)
        self.assertEqual(cur_page, 1)

        university = "university"
        college = "college"
        answer = {"question_id": experience_id1, "content": "Good!!!"}
        answer_id, _, = yield self.experience_man.post_answer(answer, star_user1, "uid", "nick_name", "user_icon",
                                                              university, college)

        answer = {"question_id": experience_id2, "content": "bad!!!"}
        answer_id, _, = yield self.experience_man.post_answer(answer, star_user2, "uid", "nick_name", "user_icon",
                                                              university, college)

        total_page, cur_page, experience_list = yield self.experience_man.get_recommended_users2(user, None, 1, False)
        self.assertEqual(len(experience_list), 2)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

        total_page, cur_page, experience_list = yield self.experience_man.get_recommended_users2(user, university, 1,
                                                                                                 False)
        self.assertEqual(len(experience_list), 2)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

    @tornado.testing.gen_test(timeout=15)
    def test_search(self):
        yield self.experience_man._clean_search_db()
        total_page, cur_page, experience_list = yield self.experience_man.search(u"计算所考研我是小鸟", 1)
        self.assertEqual(len(experience_list), 0)
        self.assertEqual(total_page, 0)
        self.assertEqual(cur_page, 1)

        question = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": u"experience content 我是一只小小小小鸟",
                    "content": u"experience content 我是一只小小小小鸟", "attachment": None}

        experience_id = yield self.experience_man.post_question(question, "test@test.com", "uid", "nick name",
                                                                "test.jpg", real_name="李智华")
        question = {"class": u"考研", "tags": [u"文学", u"计算所"], "title": u"随便输入一个问题，然后等待其他人的回答",
                    "content": u"随便输入一个问题，然后等待其他人的回答", "attachment": None}

        experience_id2 = yield self.experience_man.post_question(question, "test'2'@test.com", "uid", "nick name",
                                                                 "test.jpg", real_name="李智慧")

        total_page, cur_page, experience_list = yield self.experience_man.search("计算所 考研", 1, False)

        self.assertEqual(len(experience_list), 2)
        # self.assertEqual(experience_list[0]["id"], experience_id2)
        # self.assertEqual(experience_list[1]["id"], experience_id)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)
        total_page, cur_page, experience_list = yield self.experience_man.search("考研文学", 1)
        self.assertEqual(len(experience_list), 2)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)
        total_page, cur_page, experience_list = yield self.experience_man.search("考研文学", 1)
        self.assertEqual(len(experience_list), 2)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

        total_page, cur_page, experience_list = yield self.experience_man.search("考研文学问题回答", 1, False)
        self.assertEqual(len(experience_list), 2)
        self.assertEqual(experience_list[0]["id"], experience_id2)
        self.assertEqual(experience_list[1]["id"], experience_id)
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)

        keywords = self.experience_man.top_search_keywords()
        self.assertEqual(len(keywords), 4)
        self.assertEqual(keywords[0][1], 2)
        self.assertEqual(keywords[0][0], "考研文学")

        total_page, cur_page, experience_list = yield self.experience_man.search("小小鸟", 1, False)
        self.assertEqual(len(experience_list), 1)
        self.assertEqual(experience_list[0]["id"], experience_id)

        total_page, cur_page, experience_list = yield self.experience_man.search("李智华", 1, False)
        self.assertEqual(len(experience_list), 1)
        self.assertEqual(experience_list[0]["id"], experience_id)

        total_page, cur_page, experience_list = yield self.experience_man.search("李智慧", 1, False)
        self.assertEqual(len(experience_list), 1)
        self.assertEqual(experience_list[0]["id"], experience_id2)

        print "experience_id:", experience_id
        yield self.experience_man.edit_question({"id": experience_id, "content": "编辑的新内容，鲨鱼来了，快跑！"}, "test@test.com", "uid", "nick")
        import time
        time.sleep(11)
        total_page, cur_page, experience_list = yield self.experience_man.search("小小鸟", 1, False)
        self.assertEqual(len(experience_list), 1)
        print experience_list[0]["content"]

        total_page, cur_page, experience_list = yield self.experience_man.search("新内容鲨鱼", 1, False)
        self.assertEqual(len(experience_list), 1)
        self.assertEqual(experience_list[0]["id"], experience_id)


    @tornado.testing.gen_test(timeout=3)
    def test_thanks_post_experience(self):
        user = u"test11@test.com"
        tag_class = u"考研"
        token = self.experience_man.generate_auth_token(user, tag_class)
        self.assertEqual(self.experience_man.fetch_star_tag_of_auth_token(token, user), tag_class)
        user = "test@test.com"
        tag_class = u"留学"
        token = self.experience_man.generate_auth_token(user, tag_class)
        self.assertEqual(self.experience_man.fetch_star_tag_of_auth_token(token, user), tag_class)


    @tornado.testing.gen_test(timeout=3)
    def test_invite_user(self):
        yield self.db.users.remove({})
        user = "test@test.com"

        star_user1 = "test1@test.com"
        star_user2 = "test2@test.com"
        yield self.user_manager.register_user(user, "password", "icon", "real name",
                                              "school", "college", "nick", "M")
        yield self.user_manager.register_user(star_user1, "password", "icon", "real name",
                                              "school", "college", "nick", "M")
        yield self.user_manager.register_user(star_user2, "password", "icon", "real name",
                                              "school", "college", "nick", "M")
        experience = {"invited_users": [{"user": star_user1, "real_name": "what??"},{"user": star_user2, "real_name": "xx"}], "how_much": 10, "class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id1 = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                                 "test.jpg")

        university = "university"
        college = "college"
        answer = {"question_id": experience_id1, "content": "Good!!!"}
        answer_id, Q = yield self.experience_man.post_answer(answer, star_user1, "uid", "nick_name", "user_icon",
                                                              university, college)
        self.assertTrue(Q["be_invited"])

        answer = {"question_id": experience_id1, "content": "bad!!!"}
        answer_id, Q, = yield self.experience_man.post_answer(answer, star_user2, "uid", "nick_name", "user_icon",
                                                              university, college)
        self.assertTrue(Q["be_invited"])

        print "post answer invited twice..."
        answer = {"question_id": experience_id1, "content": "bad!!!"}
        answer_id, Q = yield self.experience_man.post_answer(answer, star_user2, "uid", "nick_name", "user_icon",
                                                              university, college)
        self.assertTrue(answer_id is None)
        self.assertTrue(Q is not None)

    @tornado.testing.gen_test(timeout=3)
    def test_post_answer_twice(self):
        yield self.db.users.remove({})
        user = "test@test.com"

        answer_user = "test1@test.com"
        yield self.user_manager.register_user(user, "password", "icon", "real name",
                                              "school", "college", "nick", "M")
        yield self.user_manager.register_user(answer_user, "password", "icon", "real name",
                                              "school", "college", "nick", "M")
        experience = {"how_much": 10, "class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id1 = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                                 "test.jpg")

        university = "university"
        college = "college"
        answer = {"question_id": experience_id1, "content": "Good!!!"}
        answer_id, Q = yield self.experience_man.post_answer(answer, answer_user, "uid", "nick_name", "user_icon",
                                                              university, college)
        self.assertTrue(answer_id is not None)
        self.assertTrue(Q is not None)

        answer = {"question_id": experience_id1, "content": "bad!!!"}
        answer_id, Q, = yield self.experience_man.post_answer(answer, answer_user, "uid", "nick_name", "user_icon",
                                                              university, college)
        self.assertTrue(answer_id is None)
        self.assertTrue(Q is not None)

    @tornado.testing.gen_test(timeout=3)
    def test_update_user_icon(self):
        experience = {"class": u"留学", "tags": ["test-tag", "another_tag"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience2 = dict(experience)
        experience_id = yield self.experience_man.post_question(experience, self.user, "uid", "nick name", "test.jpg")
        experience_id2 = yield self.experience_man.post_question(experience2, self.user, "uid", "nick name", "test.jpg")

        answer_user = "test222@test.com"
        answer = {"question_id": experience_id, "content": "bad!!!"}
        answer_id, Q = yield self.experience_man.post_answer(answer, answer_user, "uid", "nick_name", "user_icon",
                                                              "university", "college")
        self.assertTrue(experience_id is not None)
        self.assertTrue(experience_id == Q["id"])

        total_page, cur_page, experience_list = yield self.experience_man.get_question_overview(1, use_cache=False)
        self.assertEqual(experience_list[0]["publisher_img"], "test.jpg")
        self.assertEqual(experience_list[1]["publisher_img"], "test.jpg")

        yield self.experience_man.update_user_icon(self.user, "test2.jpg")

        total_page, cur_page, experience_list = yield self.experience_man.get_question_overview(1, use_cache=False)
        self.assertEqual(experience_list[0]["publisher_img"], "test2.jpg")
        self.assertEqual(experience_list[1]["publisher_img"], "test2.jpg")

        experience2 = yield self.experience_man.get_question_by_id(experience_id, False)
        self.assertEqual(experience2["best_answer"]["publisher_img"], "user_icon")
        yield self.experience_man.update_user_icon(answer_user, "test2.jpg")
        experience2 = yield self.experience_man.get_question_by_id(experience_id, False)
        self.assertEqual(experience2["best_answer"]["publisher_img"], "test2.jpg")

    @tornado.testing.gen_test(timeout=3)
    def test_latest_question(self):
        experience = {"class": u"留学", "tags": ["test-tag", "another_tag"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience2 = dict(experience)

        total_page, cur_page, experience_list = yield self.experience_man.get_question_overview(0)
        self.assertEqual(total_page, 0)
        self.assertEqual(len(experience_list), 0)

        experience_id = yield self.experience_man.post_question(experience, self.user, "uid", "nick name", "test.jpg")
        experience_id2 = yield self.experience_man.post_question(experience2, self.user, "uid", "nick name", "test.jpg")

        total_page, cur_page, experience_list = yield self.experience_man.get_question_overview(0)
        self.assertEqual(experience_list[0]["id"], experience_id2)
        self.assertEqual(experience_list[1]["id"], experience_id)

    @tornado.testing.gen_test(timeout=3)
    def test_eidt_question_and_answer(self):
        previous_content = "xxxx"
        experience = {"class": u"留学", "tags": ["test-tag", "another_tag"], "title": "experience title",
                      "content": previous_content, "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, self.user, "uid", "nick name", "test.jpg")

        answer_user = "test222@test.com"
        previous_answer = "xxxxxxx"
        answer = {"question_id": experience_id, "content": previous_answer}
        answer_id, Q = yield self.experience_man.post_answer(answer, answer_user, "uid", "nick_name", "user_icon",
                                                             "university", "college")
        question = yield self.experience_man.get_question_by_id(experience_id, False)
        self.assertEqual(question["content"], previous_content)

        total_page, cur_page, answer_list = yield self.experience_man.get_answers(experience_id, 1)
        self.assertEqual(answer_list[0]["content"], previous_answer)

        question = yield self.experience_man.get_question_by_id(experience_id, False)
        self.assertEqual(question["content"], previous_content)

        new_content = "aaaaaaaaaaaaaaaaaa"
        yield self.experience_man.edit_question({"id": experience_id, "content": new_content}, self.user, "uid", "nick name", "test.jpg")
        question = yield self.experience_man.get_question_by_id(experience_id, False)
        self.assertEqual(question["content"], new_content)

        yield self.experience_man.edit_answer({"id": answer_id, "content": new_content}, answer_user, "uid", "nick name", "test.jpg")
        total_page, cur_page, answer_list = yield self.experience_man.get_answers(experience_id, 1, use_cache=False)
        self.assertEqual(answer_list[0]["content"], new_content)

    @tornado.testing.gen_test(timeout=300)
    def test_move_tag(self):
        previous_content = "xxxx"
        tag1 = "tag1"
        tag2 = "tag2"
        class2 = u"留学"
        experience = {"class": class2, "tags": [tag1, tag2], "title": "experience title",
                      "content": previous_content, "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, self.user, "uid", "nick name", "test.jpg")

        answer_user = "test222@test.com"
        previous_answer = "xxxxxxx"
        answer = {"question_id": experience_id, "content": previous_answer}
        answer_id, Q = yield self.experience_man.post_answer(answer, answer_user, "uid", "nick_name", "user_icon",
                                                             "university", "college")
        Q = yield self.experience_man.get_question_by_id(experience_id)
        self.assertEqual(Q["class2"], class2)
        self.assertTrue(class2+":"+tag1, Q["tags_with_class"])
        self.assertTrue(class2+":"+tag2, Q["tags_with_class"])
        self.assertEqual(Q["index_class"], self.experience_man.TAG_CLASS[class2])

        answers = yield self.experience_man.get_answers(experience_id)
        self.assertEqual(answers[2][0]["class2"], class2)
        self.assertEqual(answers[2][0]["index_class"], self.experience_man.TAG_CLASS[class2])

        tags = self.experience_tag_man.get_all_tags_by_class(class2)
        self.assertTrue(tag1 in tags)
        self.assertTrue(tag1 in tags)
        merge_to_class = u"校园"
        tags = self.experience_tag_man.get_all_tags_by_class(merge_to_class)
        self.assertFalse(tag1 in tags)
        self.assertFalse(tag1 in tags)

        [moved_q_num, move_A_num] = yield self.experience_man.move_tag(class2+":"+tag1, merge_to_class)
        self.assertEqual(moved_q_num, 1)
        self.assertEqual(move_A_num, 1)
        Q = yield self.experience_man.get_question_by_id(experience_id, use_cache=False)
        self.assertEqual(Q["class2"], merge_to_class)
        self.assertTrue(merge_to_class+":"+tag1, Q["tags_with_class"])
        self.assertTrue(merge_to_class+":"+tag2, Q["tags_with_class"])
        self.assertEqual(Q["index_class"], self.experience_man.TAG_CLASS[merge_to_class])

        answers = yield self.experience_man.get_answers(experience_id, use_cache=False)
        self.assertEqual(answers[2][0]["class2"], merge_to_class)
        self.assertEqual(answers[2][0]["index_class"], self.experience_man.TAG_CLASS[merge_to_class])

        tags = self.experience_tag_man.get_all_tags_by_class(class2)
        self.assertFalse(tag1 in tags)
        self.assertFalse(tag1 in tags)
        tags = self.experience_tag_man.get_all_tags_by_class(merge_to_class)
        self.assertTrue(tag1 in tags)
        self.assertTrue(tag1 in tags)
        print "test move tag OK"

    @tornado.testing.gen_test(timeout=3)
    def test_stick_question(self):
        previous_content = "xxxx"
        tag1 = "tag1"
        tag2 = "tag2"
        class2 = u"留学"
        experience = {"class": class2, "tags": [tag1, tag2], "title": "experience title",
                      "content": previous_content, "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, self.user, "uid", "nick name", "test.jpg")

        experience = {"class": class2, "tags": [tag1, tag2], "title": "experience title",
                      "content": previous_content, "attachment": None}
        experience_id2 = yield self.experience_man.post_question(experience, self.user, "uid", "nick name", "test.jpg")

        yield self.experience_man.stick_question(experience_id2)

        ret = yield self.experience_man.get_experience_overview(current_page=1, tag_class=class2, use_cache=False)
        self.assertEqual(len(ret[2]), 2)
        self.assertEqual(ret[2][0]["id"], experience_id2)

    @tornado.testing.gen_test(timeout=3)
    def test_post_comment(self):
        experience = {"class": u"留学", "tags": ["test-tag", "another_tag"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, self.user, "uid", "nick name", "test.jpg")
        experience = yield self.experience_man.get_question_by_id(experience_id)
        self.assertEqual(experience["comment_num"], 0)
        self.assertEqual(experience["comment_list"], [])
        comment_user = "user222"
        yield self.experience_man.post_comment(experience_id,"comment here", comment_user, "user_icon", "university","real_name")
        experience = yield self.experience_man.get_question_by_id(experience_id, False)
        self.assertEqual(experience["comment_num"], 1)
        self.assertEqual(experience["comment_list"][0]["from"]["user"], comment_user)
        self.assertEqual(experience["comment_list"][0]["to"]["user"], self.user)
        comment_user2 = "test1111"
        yield self.experience_man.post_comment(experience_id, "comment here 2", comment_user2, "user_icon2", "university","real_name2", comment_user, "user_icon", "university","real_name")
        experience = yield self.experience_man.get_question_by_id(experience_id, False)
        self.assertEqual(experience["comment_num"], 2)
        self.assertEqual(experience["comment_list"][1]["from"]["user"], comment_user2)
        self.assertEqual(experience["comment_list"][1]["to"]["user"], comment_user)
        comment_user2 = "test1111"
        answer = {"question_id": experience_id, "content": "Good!!!"}
        user2 = "test333@test.com"
        udb = UniversityDB()
        university, college = udb.random_choose_college_of_university()
        answer_id, _ = yield self.experience_man.post_answer(answer, user2, "uid", "nick_name", "user_icon", university,
                                                             college)
        comment_user3 = "test999@test.com"
        yield self.experience_man.post_comment(answer_id, "comment here 3", comment_user3, "user_icon2", "university","real_name2", is_answer=True)
        total_page, cur_page, answer_list = yield self.experience_man.get_answers(experience_id, 1)
        self.assertEqual(len(answer_list), 1)
        self.assertEqual(answer_list[0]["id"], answer_id)
        self.assertEqual(answer_list[0]["comment_num"], 1)
        self.assertEqual(answer_list[0]["comment_list"][0]["from"]["user"], comment_user3)
        self.assertEqual(answer_list[0]["comment_list"][0]["to"]["user"], user2)

    @tornado.testing.gen_test(timeout=3)
    def test_topK(self):
        sorted_list1 = [{"name":"bone", "age":29}, {"name": "xixi", "age": 26}, {"name": "kaka", "age": 20}]
        sorted_list2 = [{"name": "jack", "age": 50},{"name":"bone", "age":29}, {"name": "lily", "age": 27}, {"name": "tom", "age": 2}]
        ret = self.experience_man.topK_of_sorted_lists([sorted_list1, sorted_list2], 10, "age", "name")
        self.assertEqual(len(ret), 6)
        self.assertEqual(ret[0]["name"], "jack")
        self.assertEqual(ret[1]["name"], "bone")
        self.assertEqual(ret[2]["name"], "lily")
        self.assertEqual(ret[5]["name"], "tom")

    @tornado.testing.gen_test(timeout=3)
    def test_get_recommend_users(self):
        yield self.db.users.remove({})
        self.redis_cache.flushdb()
        user1 = "test@test.com"
        passwd, user_icon_url, real_name, school, college, nick, gender = "123", "NOT SET", "bone", "NOT SET", "NOT SET", "bonelee", "MALE"
        yield self.user_manager.register_user(user1, passwd, user_icon_url, real_name, school, college, nick, gender)
        user2 = "test2@test.com"
        yield self.user_manager.register_user(user2, passwd, user_icon_url, real_name, school, college, nick, gender)
        user3 = "test3@test.com"
        yield self.user_manager.register_user(user3, passwd, user_icon_url, real_name, school, college, nick, gender)

        user = u"test11@test.com"
        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        question_id = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                              "test.jpg")
        experience = {"class": u"考研", "tags": [u"测试", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        question_id2 = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                               "test.jpg")
        answer = {"question_id": question_id, "content": "Good!!!"}
        udb = UniversityDB()
        university, college = udb.random_choose_college_of_university()
        answer_id1, _ = yield self.experience_man.post_answer(answer, user1, "uid", "nick_name", "user_icon", university,
                                                             college)
        answer = {"question_id": question_id2, "content": "Good!!!"}
        answer_id2, _ = yield self.experience_man.post_answer(answer, user2, "uid", "nick_name", "user_icon", university,
                                                             college)
        yield self.experience_man.inc_thumb_up_num(answer_id2, "user_A", True)
        yield self.experience_man.inc_thumb_up_num(answer_id2, "user_B", True)

        answer = {"question_id": question_id, "content": "Good!!!"}
        answer_id3, _ = yield self.experience_man.post_answer(answer, user3, "uid", "nick_name", "user_icon", university,
                                                             college)
        yield self.experience_man.inc_thumb_up_num(answer_id3, "user_C", True)

        total_page, cur_page, user_list = yield self.experience_man.get_recommend_users("考研", ["计算机", "测试"])
        self.assertEqual(total_page, 1)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(user_list), 3)
        self.assertEqual(user_list[0]["user"], user2)
        self.assertEqual(user_list[1]["user"], user3)
        self.assertEqual(user_list[2]["user"], user1)

    @tornado.testing.gen_test(timeout=3)
    def test_get_recommended_top_users(self):
        yield self.db.users.remove({})
        self.redis_cache.flushdb()
        user1 = "test@test.com"
        passwd, user_icon_url, real_name, school, college, nick, gender = "123", "NOT SET", "bone", "NOT SET", "NOT SET", "bonelee", "MALE"
        yield self.user_manager.register_user(user1, passwd, user_icon_url, real_name, school, college, nick, gender)
        user2 = "test2@test.com"
        yield self.user_manager.register_user(user2, passwd, user_icon_url, real_name, school, college, nick, gender)
        user3 = "test3@test.com"
        yield self.user_manager.register_user(user3, passwd, user_icon_url, real_name, school, college, nick, gender)

        user = u"test11@test.com"
        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        question_id = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                              "test.jpg")
        experience = {"class": u"考研", "tags": [u"测试", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        question_id2 = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                               "test.jpg")
        answer = {"question_id": question_id, "content": "Good!!!"}
        udb = UniversityDB()
        university, college = udb.random_choose_college_of_university()
        answer_id1, _ = yield self.experience_man.post_answer(answer, user1, "uid", "nick_name", "user_icon", university,
                                                             college)
        answer = {"question_id": question_id2, "content": "Good!!!"}
        answer_id2, _ = yield self.experience_man.post_answer(answer, user2, "uid", "nick_name", "user_icon", university,
                                                             college)
        yield self.experience_man.inc_thumb_up_num(answer_id2, "user_A", True)
        yield self.experience_man.inc_thumb_up_num(answer_id2, "user_B", True)

        answer = {"question_id": question_id, "content": "Good!!!"}
        answer_id3, _ = yield self.experience_man.post_answer(answer, user3, "uid", "nick_name", "user_icon", university,
                                                             college)
        yield self.experience_man.inc_thumb_up_num(answer_id3, "user_C", True)

        ret = yield self.experience_man.get_recommended_top_users("考研", ["计算机", "测试"])
        self.assertEqual(len(ret["计算机"]), 2)
        self.assertEqual(ret["计算机"][0]["user"], user2)
        self.assertEqual(ret["计算机"][0]["wanted"]["thumb_num_of_tag"], 2)
        self.assertEqual(ret["计算机"][0]["wanted"]["posted_num"], 1)
        self.assertEqual(ret["计算机"][1]["user"], user3)
        self.assertEqual(ret["计算机"][1]["wanted"]["thumb_num_of_tag"], 1)
        self.assertEqual(ret["计算机"][1]["wanted"]["posted_num"], 1)
        self.assertEqual(len(ret["测试"]), 1)
        self.assertEqual(ret["测试"][0]["user"], user2)
        self.assertEqual(ret["测试"][0]["wanted"]["thumb_num_of_tag"], 2)
        self.assertEqual(ret["测试"][0]["wanted"]["posted_num"], 1)

    @tornado.testing.gen_test(timeout=3)
    def test_get_today_QA(self):
        question_list = yield self.experience_man.get_today_questions()
        self.assertEqual(len(question_list), 0)
        answer_list = yield self.experience_man.get_today_answers()
        self.assertEqual(len(answer_list), 0)
        user = u"test11@test.com"
        experience = {"class": u"考研", "tags": [u"文学", u"计算机"], "title": "experience title",
                      "content": "experience content", "attachment": None}
        experience_id = yield self.experience_man.post_question(experience, user, "uid", "nick name",
                                                                "test.jpg")
        answer = {"question_id": experience_id, "content": "Good!!!"}
        user333 = "test333@test.com"
        udb = UniversityDB()
        university, college = udb.random_choose_college_of_university()
        answer_id, _ = yield self.experience_man.post_answer(answer, user333, "uid", "nick_name", "user_icon", university,
                                                             college)
        question_list = yield self.experience_man.get_today_questions()
        self.assertEqual(len(question_list), 1)
        answer_list = yield self.experience_man.get_today_answers()
        self.assertEqual(len(answer_list), 1)
        print "get today question and answer OK"

if __name__ == '__main__':
    import unittest
    unittest.main()

__author__ = 'bone'

import sys
import os.path

fbt_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
sys.path.append(fbt_path)

from rating_manager import RatingManager, RatingManagerP2P
from university_course_manager import RedisDBManager
from study_resource_manager import StudyResourceManager, StudyResourceManagerP2P
from university_db import UniversityDB

from tornado import testing, gen
from motor import MotorClient
import redis
from time import time
from timeout_probe import TimeoutProbe

def mock_uid(user):
    return abs(hash(user))


class RatingManagerTestCase(testing.AsyncTestCase):
    __metaclass__ = TimeoutProbe

    @testing.gen_test
    def test_rating(self):
        yield self.test_rating_helper(RatingManager, StudyResourceManager)

    # @testing.gen_test
    # def test_rating_p2p(self):
    #     yield self.test_rating_helper(RatingManagerP2P, StudyResourceManagerP2P)

    @gen.coroutine
    def test_rating_helper(self, class_name, class_name2):
        db = MotorClient().fbt_test
        rating_manager = class_name(db)
        yield db.rating.remove({})

        redis_db = redis.StrictRedis()
        redis_cache = redis.StrictRedis()

        redis_db.flushdb()
        redis_db_man = RedisDBManager(redis_db)

        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = class_name2(db, redis_cache, redis_db_man, es_host="localhost", es_port=9200)
        yield study_res_manager.clear_db_just_for_test()

        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["rate"] = 3
        resource["college"]=college
        resource["file_hash"] = str(long(time()*1000))

        file_id = yield study_res_manager.upload_resource(resource, "test_user@test.com", mock_uid("test_user@test.com"))

        delta_user, delta_score = yield rating_manager.rating(file_id, 4, "test_user2@test.com", 1234)
        average_rating = yield study_res_manager.rating(file_id, delta_score, delta_user)
        self.assertEqual(average_rating, 3.5)

        # same rating
        delta_user, delta_score = yield rating_manager.rating(file_id, 5, "test_user2@test.com", 1234)
        average_rating = yield study_res_manager.rating(file_id, delta_score, delta_user)
        self.assertEqual(average_rating, 4)

        # another rating
        delta_user, delta_score = yield rating_manager.rating(file_id, 2.5, "test_user3@test.com", 4321)
        average_rating = yield study_res_manager.rating(file_id, delta_score, delta_user)
        self.assertEqual(average_rating, 3.5)
        print "test over"

if __name__ == '__main__':
    import unittest
    unittest.main()

# coding: utf-8
__author__ = 'bone-lee'

import os, sys
fbt_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

sys.path.append(fbt_path)

from motor import MotorClient
from coin_manager import CoinManager
from coin_manager import CoinStatisticCollector

import tornado.gen
import tornado.testing
import tornado.gen
import tornado.testing
import tornado.ioloop
import redis
from time import time
from datetime import datetime
from datetime import timedelta
import mock


class CoinManagerTestCase(tornado.testing.AsyncTestCase):
    def setUp(self):
        super(CoinManagerTestCase, self).setUp()
        self.db = MotorClient().fbt_test
        self.redis_cache = redis.StrictRedis()
        self.coin_man = CoinManager(self.db, self.redis_cache)
        self.redis_cache.flushdb()
        self.io_loop.run_sync(self.fixture_setup)
        # self.io_loop.add_future(self.fixture_setup(), lambda x: True)

    @tornado.gen.coroutine
    def fixture_setup(self):
        yield self.db.users.remove()
        yield self.db.coins_of_user.remove()
        yield self.db.coin_log.remove()

    @tornado.testing.gen_test(timeout=3)
    def test_register(self):
        coin_man = self.coin_man
        uid = 12345
        total_coin, study_coin = yield coin_man.get_coin(uid)
        self.assertEqual(total_coin, 0)
        self.assertEqual(study_coin, 0)
        user = "test@test.com"
        yield coin_man.register_ok(uid, user)
        total_coin, study_coin = yield coin_man.get_coin(uid)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN)
        self.assertEqual(study_coin, coin_man.REGISTER_COIN)

    @tornado.testing.gen_test(timeout=3)
    def test_referee(self):
        coin_man = self.coin_man
        # register with coin
        uid = 12345
        user = "test@test.com"
        yield coin_man.register_ok(uid, user)

        uid2 = 54321
        user2 = "test2@test.com"
        yield coin_man.register_ok(uid2, user2)

        yield coin_man.refer_ok(uid2, user2, uid, user)
        total_coin, study_coin = yield coin_man.get_coin(uid2)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN+coin_man.REFER_COIN)
        self.assertEqual(study_coin, coin_man.REGISTER_COIN+coin_man.REFER_COIN)

        total_coin, study_coin = yield coin_man.get_coin(uid)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN+coin_man.REFEREE_COIN)
        self.assertEqual(study_coin, coin_man.REGISTER_COIN+coin_man.REFEREE_COIN)

    @tornado.testing.gen_test(timeout=3)
    def test_invalid_referee(self):
        coin_man = self.coin_man
        # register with coin
        uid = 12345
        user = "test@test.com"

        uid2 = 54321
        user2 = "test2@test.com"
        yield coin_man.register_ok(uid2, user2)

        try:
            yield coin_man.refer_ok(uid2, user2, uid, user)
        except AssertionError as e:
            pass
        total_coin, study_coin = yield coin_man.get_coin(uid2)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN+coin_man.REFER_COIN)
        self.assertEqual(study_coin, coin_man.REGISTER_COIN+coin_man.REFER_COIN)

        total_coin, study_coin = yield coin_man.get_coin(uid)
        self.assertEqual(total_coin, coin_man.REFEREE_COIN)
        self.assertEqual(study_coin, coin_man.REFEREE_COIN)

    @tornado.testing.gen_test(timeout=3)
    def test_invalid_add_coin(self):
        coin_man = self.coin_man
        uid = 1234
        total_coin, status = yield coin_man.add_study_coin(uid, 0)
        self.assertEqual(status, coin_man.STATUS["err"])

        total_coin, status = yield coin_man.add_study_coin(uid, 10)
        self.assertEqual(status, coin_man.STATUS["err"])

    @tornado.testing.gen_test(timeout=3)
    def test_valid_add_coin(self):
        coin_man = self.coin_man
        uid = 1234
        yield coin_man.register_ok(uid, "test@test.com")
        total_coin, status = yield coin_man.add_study_coin(uid, 10)
        self.assertEqual(status, coin_man.STATUS["ok"])

    @tornado.testing.gen_test(timeout=3)
    def test_log_register(self):
        coin_man = self.coin_man
        uid = 1234
        log_list = yield coin_man.get_user_coin_log(uid)
        self.assertListEqual(log_list, [])
        yield coin_man.register_ok(uid, "test@test.com")

        log_list = yield coin_man.get_user_coin_log(uid)
        self.assertEqual(len(log_list), 1)
        self.assertEqual(log_list[0]["uid"], uid)
        self.assertEqual(log_list[0]["how_many"], coin_man.REGISTER_COIN)
        self.assertEqual(log_list[0]["reason_id"], coin_man.COIN_REASON["register_ok"])
        self.assertEqual(log_list[0]["last_total_coin"], 0)

    @tornado.testing.gen_test(timeout=3)
    def test_log_refer(self):
        coin_man = self.coin_man
        uid = 12345
        user = "test@test.com"
        yield coin_man.register_ok(uid, user)

        uid2 = 54321
        user2 = "test2@test.com"
        yield coin_man.register_ok(uid2, user2)

        yield coin_man.refer_ok(uid2, user2, uid, user)

        log_list = yield coin_man.get_user_coin_log(uid)
        self.assertEqual(len(log_list), 2)
        self.assertEqual(log_list[1]["uid"], uid)
        self.assertEqual(log_list[1]["how_many"], coin_man.REGISTER_COIN)
        self.assertEqual(log_list[1]["reason_id"], coin_man.COIN_REASON["register_ok"])
        self.assertEqual(log_list[1]["last_total_coin"], 0)

        self.assertEqual(log_list[0]["uid"], uid)
        self.assertEqual(log_list[0]["how_many"], coin_man.REFEREE_COIN)
        self.assertEqual(log_list[0]["reason_id"], coin_man.COIN_REASON["be_referred_ok"])
        self.assertEqual(log_list[0]["last_total_coin"], coin_man.REGISTER_COIN)

        log_list = yield coin_man.get_user_coin_log(uid2)
        self.assertEqual(len(log_list), 2)
        self.assertTrue(log_list[0]["index_ctime"] > log_list[1]["index_ctime"])
        self.assertEqual(log_list[1]["uid"], uid2)
        self.assertEqual(log_list[1]["how_many"], coin_man.REGISTER_COIN)
        self.assertEqual(log_list[1]["reason_id"], coin_man.COIN_REASON["register_ok"])
        self.assertEqual(log_list[1]["last_total_coin"], 0)

        self.assertEqual(log_list[0]["uid"], uid2)
        self.assertEqual(log_list[0]["how_many"], coin_man.REFER_COIN)
        self.assertEqual(log_list[0]["reason_id"], coin_man.COIN_REASON["refer_ok"])
        self.assertEqual(log_list[0]["last_total_coin"], coin_man.REGISTER_COIN)

    @tornado.testing.gen_test(timeout=3)
    def test_pass_audit_resource_log(self):
        coin_man = self.coin_man
        uid = 12345
        user = "test@test.com"
        uid2 = 54321
        user2 = "test2@test.com"
        yield coin_man.register_ok(uid, user)
        yield coin_man.register_ok(uid2, user2)

        ok = yield coin_man.refer_ok(uid, user,uid2, user2)
        self.assertTrue(ok)

        yield coin_man.pass_audit_a_study_res(uid, "file_id", 100)

        log_list = yield coin_man.get_user_coin_log(uid)
        self.assertEqual(len(log_list), 3)
        self.assertEqual(log_list[2]["uid"], uid)
        self.assertEqual(log_list[2]["how_many"], coin_man.REGISTER_COIN)
        self.assertEqual(log_list[2]["reason_id"], coin_man.COIN_REASON["register_ok"])
        self.assertEqual(log_list[2]["last_total_coin"], 0)

        self.assertEqual(log_list[1]["uid"], uid)
        self.assertEqual(log_list[1]["how_many"], coin_man.REFER_COIN)
        self.assertEqual(log_list[1]["reason_id"], coin_man.COIN_REASON["refer_ok"])
        self.assertEqual(log_list[1]["last_total_coin"], coin_man.REGISTER_COIN)

        self.assertEqual(log_list[0]["uid"], uid)
        self.assertEqual(log_list[0]["how_many"], coin_man.REFEREE_COIN_PERCENTAGE*100)
        self.assertEqual(log_list[0]["reason_id"], coin_man.COIN_REASON["pass_audit_a_study_res"])
        self.assertEqual(log_list[0]["last_total_coin"], coin_man.REGISTER_COIN+coin_man.REFER_COIN)

        log_list = yield coin_man.get_user_coin_log(uid2)
        self.assertEqual(len(log_list), 3)
        self.assertEqual(log_list[2]["uid"], uid2)
        self.assertEqual(log_list[2]["how_many"], coin_man.REGISTER_COIN)
        self.assertEqual(log_list[2]["reason_id"], coin_man.COIN_REASON["register_ok"])
        self.assertEqual(log_list[2]["last_total_coin"], 0)

        self.assertEqual(log_list[1]["uid"], uid2)
        self.assertEqual(log_list[1]["how_many"], coin_man.REFEREE_COIN)
        self.assertEqual(log_list[1]["reason_id"], coin_man.COIN_REASON["be_referred_ok"])
        self.assertEqual(log_list[1]["last_total_coin"], coin_man.REGISTER_COIN)

        self.assertEqual(log_list[0]["uid"], uid2)
        self.assertEqual(log_list[0]["how_many"], (1-coin_man.REFEREE_COIN_PERCENTAGE)*100)
        self.assertEqual(log_list[0]["reason_id"], coin_man.COIN_REASON["pass_audit_a_study_res_referee"])
        self.assertEqual(log_list[0]["last_total_coin"], coin_man.REGISTER_COIN+coin_man.REFEREE_COIN)

    @tornado.testing.gen_test(timeout=3)
    def test_invalid_refer(self):
        coin_man = self.coin_man
        uid = 12345
        user = "test@test.com"
        uid2 = 54321
        user2 = "test2@test.com"
        yield coin_man.register_ok(uid, user)

        ok = yield coin_man.refer_ok(uid, user,uid2, user2)
        self.assertFalse(ok)

    @tornado.testing.gen_test(timeout=3)
    def test_valid_refer(self):
        coin_man = self.coin_man
        uid = 12345
        user = "test@test.com"
        uid2 = 54321
        user2 = "test2@test.com"
        yield coin_man.register_ok(uid, user)
        yield coin_man.register_ok(uid2, user2)

        ok = yield coin_man.refer_ok(uid, user,uid2, user2)
        self.assertTrue(ok)

    @tornado.testing.gen_test(timeout=3)
    def test_pass_audit_resource_with_no_coin(self):
        coin_man = self.coin_man
        uid = 12345
        user = "test@test.com"
        yield coin_man.register_ok(uid, user)
        yield coin_man.pass_audit_a_study_res(uid, "file_id", 0)
        total_coin, study_coin = yield coin_man.get_coin(uid)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN)
        self.assertEqual(study_coin, coin_man.REGISTER_COIN)

    @tornado.testing.gen_test(timeout=3)
    def test_pass_audit_resource_normally(self):
        coin_man = self.coin_man
        uid = 12345
        user = "test@test.com"
        yield coin_man.register_ok(uid, user)
        yield coin_man.pass_audit_a_study_res(uid, "file_id", 100)
        total_coin, study_coin = yield coin_man.get_coin(uid)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN+100)
        self.assertEqual(study_coin, coin_man.REGISTER_COIN+100)

    @tornado.testing.gen_test(timeout=3)
    def test_pass_audit_resource_normally2(self):
        coin_man = self.coin_man
        uid = 12345
        user = "test@test.com"
        uid2 = 54321
        user2 = "test2@test.com"
        yield coin_man.register_ok(uid, user)
        yield coin_man.register_ok(uid2, user2)

        yield coin_man.refer_ok(uid, user,uid2, user2)

        yield coin_man.pass_audit_a_study_res(uid, "file_id", 100)
        total_coin, study_coin = yield coin_man.get_coin(uid)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN+coin_man.REFER_COIN+100*coin_man.REFEREE_COIN_PERCENTAGE)
        self.assertEqual(study_coin, coin_man.REGISTER_COIN+100*coin_man.REFEREE_COIN_PERCENTAGE+coin_man.REFER_COIN)

        total_coin, study_coin = yield coin_man.get_coin(uid2)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN+coin_man.REFEREE_COIN+100*(1-coin_man.REFEREE_COIN_PERCENTAGE))
        self.assertEqual(study_coin, coin_man.REGISTER_COIN+100*(1-coin_man.REFEREE_COIN_PERCENTAGE)+coin_man.REFEREE_COIN)

    @tornado.testing.gen_test(timeout=3)
    def test_pass_audit_resource_but_no_coin(self):
        coin_man = self.coin_man
        uid = 12345
        user = "test@test.com"
        uid2 = 54321
        user2 = "test2@test.com"
        yield coin_man.register_ok(uid, user)
        yield coin_man.register_ok(uid2, user2)

        yield coin_man.refer_ok(uid, user,uid2, user2)

        yield coin_man.pass_audit_a_study_res(uid, "file_id", 0)
        total_coin, study_coin = yield coin_man.get_coin(uid)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN+coin_man.REFER_COIN+0*coin_man.REFEREE_COIN_PERCENTAGE)
        self.assertEqual(study_coin, coin_man.REGISTER_COIN+0*coin_man.REFEREE_COIN_PERCENTAGE+coin_man.REFER_COIN)

        total_coin, study_coin = yield coin_man.get_coin(uid2)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN+coin_man.REFEREE_COIN+0*(1-coin_man.REFEREE_COIN_PERCENTAGE))
        self.assertEqual(study_coin, coin_man.REGISTER_COIN+0*(1-coin_man.REFEREE_COIN_PERCENTAGE)+coin_man.REFEREE_COIN)

    @tornado.testing.gen_test(timeout=3)
    def test_pass_audit_resource_but_no_coin_log(self):
        coin_man = self.coin_man
        uid = 12345
        user = "test@test.com"
        uid2 = 54321
        user2 = "test2@test.com"
        yield coin_man.register_ok(uid, user)
        yield coin_man.register_ok(uid2, user2)
        yield coin_man.refer_ok(uid, user,uid2, user2)
        yield coin_man.pass_audit_a_study_res(uid, "file_id", 0)

        log_list = yield coin_man.get_user_coin_log(uid2)
        self.assertEqual(len(log_list), 2)

        log_list = yield coin_man.get_user_coin_log(uid)
        self.assertEqual(len(log_list), 2)

    @tornado.testing.gen_test(timeout=3)
    def test_download_ok(self):
        coin_man = self.coin_man
        uid = 12345
        user = "test@test.com"
        uid2 = 54321
        user2 = "test2@test.com"
        yield coin_man.register_ok(uid, user)

        ok = yield coin_man.download_ok(uid, "file_id", uid2, "学习心得")
        self.assertFalse(ok)

        yield coin_man.register_ok(uid2, uid2)

        ok = yield coin_man.download_ok(uid, "file_id", uid2, "学习心得")
        self.assertTrue(ok)

        total_coin, study_coin = yield coin_man.get_coin(uid)
        coin = coin_man.get_download_coin("学习心得")
        self.assertEqual(total_coin, coin_man.REGISTER_COIN-2*coin)
        self.assertEqual(study_coin, coin_man.REGISTER_COIN-2*coin)

        total_coin, study_coin = yield coin_man.get_coin(uid2)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN+coin)
        self.assertEqual(study_coin, coin_man.REGISTER_COIN+coin)

    @tornado.testing.gen_test(timeout=3)
    def test_thanks_user_post_experience(self):
        coin_man = self.coin_man
        uid = 12345
        user = "test@test.com"
        uid2 = 54321
        user2 = "test2@test.com"
        yield coin_man.register_ok(uid, user)
        yield coin_man.register_ok(uid2, uid2)

        how_much = 99.2
        ok = yield coin_man.thanks_user_post_experience(uid, uid2, how_much, "exp_id")
        self.assertTrue(ok)

        total_coin, study_coin = yield coin_man.get_coin(uid)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN-int(how_much))
        self.assertEqual(study_coin, coin_man.REGISTER_COIN-int(how_much))

        total_coin, study_coin = yield coin_man.get_coin(uid2)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN+int(how_much))
        self.assertEqual(study_coin, coin_man.REGISTER_COIN+int(how_much))

    @tornado.testing.gen_test(timeout=3)
    def test_invite_user_answer_question(self):
        coin_man = self.coin_man
        uid = 12345
        user = "test@test.com"
        uid2 = 54321
        user2 = "test2@test.com"
        yield coin_man.register_ok(uid, user)
        yield coin_man.register_ok(uid2, uid2)

        how_much = 10
        ok = yield coin_man.invite_user_answer_question(uid2, "question_id", uid, how_much)
        self.assertTrue(ok)

        total_coin, study_coin = yield coin_man.get_coin(uid)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN-int(how_much))
        self.assertEqual(study_coin, coin_man.REGISTER_COIN-int(how_much))

        total_coin, study_coin = yield coin_man.get_coin(uid2)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN+int(how_much))
        self.assertEqual(study_coin, coin_man.REGISTER_COIN+int(how_much))

    @tornado.testing.gen_test(timeout=3)
    def test_transfer_coin(self):
        coin_man = self.coin_man
        uid = 12345
        user = "test@test.com"
        uid2 = 54321
        user2 = "test2@test.com"
        yield coin_man.register_ok(uid, user)
        yield coin_man.register_ok(uid2, user2)

        how_much = 10
        ok = yield coin_man.transfer_coin_ok(how_much, uid, user, "real_name", "university",
                uid2, user2, "real_name2", "university2")
        self.assertTrue(ok)

        total_coin, study_coin = yield coin_man.get_coin(uid)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN-int(how_much))
        self.assertEqual(study_coin, coin_man.REGISTER_COIN-int(how_much))

        total_coin, study_coin = yield coin_man.get_coin(uid2)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN+int(how_much))
        self.assertEqual(study_coin, coin_man.REGISTER_COIN+int(how_much))

        ok = yield coin_man.transfer_coin_ok(how_much, uid, user, "real_name", "university",
                "uid not exist", user2, "real_name2", "university2")
        self.assertFalse(ok)

        ok = yield coin_man.transfer_coin_ok(how_much, "uid not exist2", user, "real_name", "university",
                uid2, user2, "real_name2", "university2")
        self.assertFalse(ok)
        ok = yield coin_man.transfer_coin_ok(999, uid, user, "real_name", "university",
                uid2, user2, "real_name2", "university2")
        self.assertTrue(ok)
        total_coin, study_coin = yield coin_man.get_coin(uid)
        self.assertEqual(total_coin, coin_man.REGISTER_COIN-int(how_much)*2-999)
        self.assertEqual(study_coin, coin_man.REGISTER_COIN-int(how_much)*2-999)


if __name__ == '__main__':
    import unittest
    unittest.main()

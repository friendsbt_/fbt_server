# -*- coding: utf-8 -*-
__author__ = 'bone'

import sys
import os.path

fbt_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

sys.path.append(fbt_path)

from study_resource_manager import StudyResourceManager
from timeout_probe import TimeoutProbe
from university_course_manager import RedisDBManager
from university_db import UniversityDB

from random import randint
from tornado.escape import utf8
import tornado.testing
import tornado.gen
from time import time
import motor
import redis


def mock_uid(user):
    return abs(hash(user))


class StudyResourceManagerTestCase(tornado.testing.AsyncTestCase):
    __metaclass__ = TimeoutProbe

    def setUp(self):
        super(StudyResourceManagerTestCase, self).setUp()
        db = motor.MotorClient().fbt_test #motorclient.fbt_test
        redis_db = redis.StrictRedis()
        redis_cache = redis.StrictRedis()
        redis_db.flushdb()
        self.redis_db_man = RedisDBManager(redis_db)
        self.university_db = UniversityDB()
        self.study_res_manager = StudyResourceManager(db, redis_cache, self.redis_db_man, es_port=9200, es_host="localhost")
        self.io_loop.run_sync(self.fixture_setup)

    @tornado.gen.coroutine
    def fixture_setup(self):
        yield self.study_res_manager.clear_db_just_for_test()

    def tearDown(self):
        pass

    @tornado.testing.gen_test(timeout=20)
    def test_upload_course_and_overview(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["file_hash"] = str(long(time()*1000))
        resource["college"]=college

        for i in range(0,100):
            resource["filename"]="test file"+str(i)
            resource["file_hash"] = str(long(time()*1000))
            yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        course = resource["course"]
        total_page, current_page, resource_list = yield study_res_manager.get_course_resources(university, college, course, current_page=1)
        self.assertEqual(total_page, (100+study_res_manager.RES_PAGE_NUM-1)/study_res_manager.RES_PAGE_NUM)
        self.assertEqual(current_page, 1)
        # print(resource_list)
        self.assertEqual(len(resource_list),study_res_manager.RES_PAGE_NUM)
        print "test over."

    @tornado.testing.gen_test(timeout=20)
    def test_rename_course(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["college"]=college

        for i in range(0,100):
            resource["file_hash"] = str(long(time()*1000))
            resource["filename"]="test file"+str(i)
            yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        course = resource["course"]
        total_page, current_page, resource_list = yield study_res_manager.get_course_resources(university, college, course, current_page=1)
        self.assertEqual(total_page, (100+study_res_manager.RES_PAGE_NUM-1)/study_res_manager.RES_PAGE_NUM)
        self.assertEqual(current_page, 1)
        self.assertEqual(len(resource_list),study_res_manager.RES_PAGE_NUM)

        new_course_name = "???what???"
        yield study_res_manager.rename_course(university, college, course, new_course_name)
        total_page, current_page, resource_list = yield study_res_manager.get_course_resources(university, college, course, current_page=1)
        self.assertEqual(total_page, 0)
        self.assertEqual(current_page, 1)
        self.assertEqual(len(resource_list), 0)

        total_page, current_page, resource_list = yield study_res_manager.get_course_resources(university, college, new_course_name, current_page=1)
        self.assertEqual(total_page, (100+study_res_manager.RES_PAGE_NUM-1)/study_res_manager.RES_PAGE_NUM)
        self.assertEqual(current_page, 1)
        self.assertEqual(len(resource_list),study_res_manager.RES_PAGE_NUM)

    @tornado.testing.gen_test(timeout=20)
    def test_hide_resources(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["college"]=college

        for i in range(0, 5):
            resource["file_hash"] = str(long(time()*1000))
            resource["filename"]="test file"+str(i)
            yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        course = resource["course"]
        total_page, current_page, resource_list = yield study_res_manager.get_course_resources(university, college, course, current_page=1)
        self.assertEqual(total_page, (5+study_res_manager.RES_PAGE_NUM-1)/study_res_manager.RES_PAGE_NUM)
        self.assertEqual(len(resource_list),5)

        # can be search
        # total_page, cur_page, resource_list = yield study_res_manager.search_course2("test", 1)
        # self.assertEqual(total_page, (5+study_res_manager.RES_PAGE_NUM-1)/study_res_manager.RES_PAGE_NUM)
        # self.assertEqual(cur_page, 1)
        # self.assertEqual(len(resource_list), 5)

        file_ids = [utf8(res['file_id']) for res in resource_list]
        ok = yield study_res_manager.hide_resource(file_ids)
        self.assertTrue(ok)
        total_page, current_page, resource_list = yield study_res_manager.get_course_resources(university, college, course, current_page=1)
        self.assertEqual(total_page, 0)
        self.assertEqual(current_page, 1)
        self.assertEqual(len(resource_list), 0)

        # can't be search any more
        total_page, cur_page, resource_list = yield study_res_manager.search_course2("test", 1)
        self.assertEqual(total_page, 0)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 0)
        print "hide resource test passed"

    @tornado.testing.gen_test(timeout=20)
    def test_recommend_resource(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["college"]=college

        tags_num = {}
        for i in range(0,100):
            resource["file_hash"] = str(long(time()*1000))
            resource["filename"]="test file"+str(i)
            resource["tag"] = "tag " + str(randint(0,20))
            if resource["tag"] not in tags_num:
                tags_num[resource["tag"]] = 0
            tags_num[resource["tag"]] += 1
            yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        resource_list = yield study_res_manager.get_recommended_resources()
        for resource in resource_list:
            tag = resource["tag"]
            self.assertEqual(tags_num[tag], resource["resource_num"])
            self.assertTrue(len(resource["resource_list"])>=tags_num[tag])
            # print resource["resource_list"]
        print "test over."
        self.stop()

    @tornado.testing.gen_test(timeout=20)
    def test_recommend_resource(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["college"]=college

        tags_num = 0
        tag = "tag what ever"
        for i in range(0,100):
            resource["filename"]="test file"+str(i)
            resource["tag"] = tag
            resource["file_hash"] = str(long(time()*1000))
            tags_num += 1
            yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        total_page, cur_page, resource_list = yield study_res_manager.get_more_recommended_resources(tag, 3)
        self.assertEqual((tags_num+study_res_manager.RES_PAGE_NUM-1)/study_res_manager.RES_PAGE_NUM, total_page)
        self.assertEqual(cur_page, 3)
        self.assertEqual(len(resource_list), study_res_manager.RES_PAGE_NUM)
        print "test over."
        self.stop()

    # @tornado.testing.gen_test(timeout=20)
    # def test_search_resource2(self):
    #     yield self.test_search_resource2_helper(StudyResourceManagerP2P)
    #     self.stop()

    @tornado.testing.gen_test(timeout=20)
    def test_search_resource2(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["college"]=college

        tags_num = 0
        tag = "tag what ever"
        for i in range(0,10):
            resource["course"]="test file "+str(i)
            resource["tag"] = tag
            resource["file_hash"] = str(long(time()*1000))
            tags_num += 1
            yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        keyword = "9"
        total_page, cur_page, resource_list = yield study_res_manager.search_course2(keyword, 1)
        self.assertEqual(1, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 1)

        keyword = "test"
        total_page, cur_page, resource_list = yield study_res_manager.search_course2(keyword, 1)
        self.assertEqual((10+study_res_manager.RES_PAGE_NUM-1)/study_res_manager.RES_PAGE_NUM, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 10)

        keywords = [university, college, university+college, university+college+"test"]
        for keyword in keywords:
            total_page, cur_page, resource_list = yield study_res_manager.search_course2(keyword, 1)
            self.assertEqual((10+study_res_manager.RES_PAGE_NUM-1)/study_res_manager.RES_PAGE_NUM, total_page)
            self.assertEqual(cur_page, 1)
            self.assertEqual(len(resource_list), 10)
        print "test search OK."

    # @tornado.testing.gen_test(timeout=20)
    # def test_search_resource(self):
    #     yield self.test_search_resource_helper(StudyResourceManager)

    @tornado.testing.gen_test(timeout=20)
    def test_get_a_resouce(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["college"]=college
        resource["file_hash"] = str(long(time()*1000))

        file_id = yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        found_resource = yield study_res_manager.get_a_resource(file_id)
        for key in ("course", "teacher", "filename", "file_size","resource_name","download_link","description","tag","rate"):
            self.assertEqual(found_resource[key], "test")
        self.assertEqual(found_resource["university"], university.decode("utf8"))
        self.assertEqual(found_resource["college"], college.decode("utf8"))

    @tornado.testing.gen_test(timeout=30)
    def test_get_user_resource(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["college"]=college

        user = "test@test.com"
        for i in range(0,100):
            resource["course"]="test file "+str(i)
            resource["tag"] = "tag"
            resource["file_hash"] = str(long(time()*1000))
            yield study_res_manager.upload_resource(resource.copy(), user, mock_uid(user))
        total_page, cur_page, resource_list = yield study_res_manager.get_user_resource(user, 1)
        self.assertEqual((100+study_res_manager.RES_PAGE_NUM_IN_MY_RES-1)/study_res_manager.RES_PAGE_NUM_IN_MY_RES, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), study_res_manager.RES_PAGE_NUM_IN_MY_RES)

        resource["file_hash"] = str(long(time()*1000))
        file_id = yield study_res_manager.upload_resource(resource.copy(), user,mock_uid(user), False)
        total_page, cur_page, resource_list = yield study_res_manager.get_user_audit_resource(user, 1, True)
        self.assertEqual((1+study_res_manager.RES_PAGE_NUM_IN_MY_RES-1)/study_res_manager.RES_PAGE_NUM_IN_MY_RES, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 1)

        yield study_res_manager.audit_resource(file_id, "unpass reason here")
        total_page, cur_page, resource_list = yield study_res_manager.get_user_audit_resource(user, 1, True)
        self.assertEqual(0, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 0)

        total_page, cur_page, resource_list = yield study_res_manager.get_user_audit_resource(user, 1, False)
        self.assertEqual(1, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 1)

    @tornado.testing.gen_test(timeout=20)
    def test_get_search_resource_by_university_short_name(self):
        print "search resource by short name"
        university_db = UniversityDB()
        university, college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]="中国科学技术大学"
        resource["college"]="理学院"
        resource["teacher"] = "李智华"
        resource["file_hash"] = str(long(time()*1000))
        yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))

        keyword = "中科大"
        total_page, cur_page, resource_list = yield study_res_manager.search_course2(keyword, 1)

        self.assertEqual(1, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 1)

        resource["resource_name"] = "中国科学院大学，国科大资源是不是可以搜索"
        yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))

        keyword = "国科大"
        total_page, cur_page, resource_list = yield study_res_manager.search_course2(keyword, 1)

        self.assertEqual(1, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 1)

        keyword = "李智华"
        total_page, cur_page, resource_list = yield study_res_manager.search_course2(keyword, 1)

        self.assertEqual(1, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 1)
        # resource["university"]="中国科学"
        # resource["college"]="理学院"
        # yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))

        print "test search course over."

    @tornado.testing.gen_test(timeout=20)
    def test_get_search_course_by_university_short_name(self):
        print "search resource by short name"
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]="中国科学技术大学"
        resource["college"]="理学院"
        resource["file_hash"] = str(long(time()*1000))
        yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))

        keyword = "中科大"
        total_page, cur_page, resource_list = yield study_res_manager.search_course(keyword, 1)
        # test cache
        total_page, cur_page, resource_list = yield study_res_manager.search_course(keyword, 1)

        self.assertEqual(1, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 1)

        # resource["university"]="中国科学"
        # resource["college"]="理学院"
        # yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        print "test search course over."

    @tornado.testing.gen_test(timeout=20)
    def test_search_course(self):
        print "search resource"
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]="中国科学技术大学"
        resource["college"]="理学院"
        resource["teacher"]="李智华"
        resource["file_hash"] = str(long(time()*1000))
        yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))

        # keyword = "中科大理学院李智华"
        keyword = "李智华 中科大理学院"
        # keyword = "李智华"
        total_page, cur_page, resource_list = yield study_res_manager.search_course(keyword, 1)
        # test cache
        total_page, cur_page, resource_list = yield study_res_manager.search_course(keyword, 1)

        self.assertEqual(1, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 1)

        # resource["university"]="中国科学"
        # resource["college"]="理学院"
        # yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        print "test search course over."

    @tornado.testing.gen_test(timeout=20)
    def test_change_upload(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["file_hash"] = str(long(time()*1000))
        resource["university"]=university
        resource["college"]=college

        user = "test@test.com"
        file_id = yield study_res_manager.upload_resource(resource.copy(), user,mock_uid(user), False)
        yield study_res_manager.audit_resource(file_id, "unpass reason here")

        total_page, cur_page, resource_list = yield study_res_manager.get_user_audit_resource(user, 1, False)
        self.assertEqual(1, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 1)
        total_page, cur_page, resource_list = yield study_res_manager.get_user_audit_resource(user, 1, True)
        self.assertEqual(0, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 0)

        university,college = university_db.random_choose_college_of_university()
        resource["university"] = university
        resource["college"] = college
        resource["file_id"] = file_id
        Ok = yield study_res_manager.change_resource(resource)
        self.assertTrue(Ok)

        total_page, cur_page, resource_list = yield study_res_manager.get_user_audit_resource(user, 1, False)
        self.assertEqual(0, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 0)
        total_page, cur_page, resource_list = yield study_res_manager.get_user_audit_resource(user, 1, True)
        self.assertEqual(1, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 1)
        self.stop()

    @tornado.testing.gen_test(timeout=20)
    def test_remove_upload(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["college"]=college
        resource["file_hash"] = str(long(time()*1000))

        user = "test@test.com"
        resource["file_hash"] = str(long(time()*1000))
        file_id = yield study_res_manager.upload_resource(resource.copy(), user,mock_uid(user), False)
        yield study_res_manager.audit_resource(file_id, "unpass reason here")

        resource["file_hash"] = str(long(time()*1000))
        file_id2 = yield study_res_manager.upload_resource(resource.copy(), user,mock_uid(user), False)
        yield study_res_manager.audit_resource(file_id2, "unpass reason here")

        total_page, cur_page, resource_list = yield study_res_manager.get_user_audit_resource(user, 1, False)
        self.assertEqual(1, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 2)

        yield study_res_manager.remove_resource([file_id, file_id2])
        total_page, cur_page, resource_list = yield study_res_manager.get_user_audit_resource(user, 1, False)
        self.assertEqual(0, total_page)
        self.assertEqual(cur_page, 1)
        self.assertEqual(len(resource_list), 0)
        self.stop()

    @tornado.testing.gen_test(timeout=20)
    def test_college_course_overview(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        university_db = UniversityDB()
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"

        university, college = university_db.random_choose_college_of_university()
        resource["university"]= university
        resource["college"]=college
        resource["course"]="course a"
        resource["file_hash"] = str(long(time()*1000))
        yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        resource["course"]="course b"
        resource["file_hash"] = str(long(time()*1000))
        yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        resource["file_hash"] = str(long(time()*1000))
        yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        total_page2,cur_page,course_list2 = yield study_res_manager.get_college_course_overview(university, college, 1)
        self.assertEqual(1, total_page2)
        self.assertEqual(cur_page, 1)
        self.assertEqual("course a", course_list2[0]["course"])
        self.assertEqual(1, course_list2[0]["resource_num"])
        self.assertEqual("course b", course_list2[1]["course"])
        self.assertEqual(2, course_list2[1]["resource_num"])
        print "get_college_course_overview pass test"

    @tornado.testing.gen_test(timeout=20)
    def test_get_course_detail(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        university_db = UniversityDB()
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        university, college = university_db.random_choose_college_of_university()
        resource["university"]= university
        resource["college"]=college
        course_name = u"测试课程"
        resource["course"]=course_name
        resource["file_hash"] = str(long(time()*1000))
        yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        course = yield study_res_manager.get_course(course_name, university, college)
        self.assertEqual(course_name, course["course"])
        self.assertEqual(university, utf8(course["university"]))
        self.assertEqual(college, utf8(course["college"]))

    @tornado.testing.gen_test(timeout=20)
    def test_find_resource(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["college"]=college
        resource["file_hash"] = str(long(time()*1000))

        resource["filename"]="test file"
        file_id = yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        res = yield study_res_manager.find_resource(file_id)
        self.assertTrue("file_size" in res)
        self.assertTrue("resource_name" in res)
        self.assertTrue("university" in res)
        self.assertTrue("college" in res)
        self.assertTrue("img" in res)
        self.assertTrue("uid" in res)
        self.assertTrue("real_uploader" in res)

    @tornado.testing.gen_test(timeout=20)
    def test_thumb_up_and_down(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["college"]=college
        resource["file_hash"] = str(long(time()*1000))

        resource["filename"]="test file"
        file_id = yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        res = yield study_res_manager.find_a_resource(file_id)
        self.assertTrue("thumb_up_num" in res)
        self.assertEqual(res["thumb_up_num"], 0)
        user = "fbt_user@fbt.com"
        user_nick = "fbt"
        ret = yield study_res_manager.thumb_up(file_id, user)
        self.assertTrue(ret)
        res = yield study_res_manager.find_a_resource(file_id)
        self.assertTrue("thumb_up_num" in res)
        self.assertEqual(res["thumb_up_num"], 1)

        ret = yield study_res_manager.thumb_up(file_id, user)
        self.assertFalse(ret)
        res = yield study_res_manager.find_a_resource(file_id)
        self.assertTrue("thumb_up_num" in res)
        self.assertEqual(res["thumb_up_num"], 1)

        user = "fbt_user2@fbt.com"
        user_nick = "fbt2"
        ret = yield study_res_manager.thumb_up(file_id, user)
        self.assertTrue(ret)
        res = yield study_res_manager.find_a_resource(file_id)
        self.assertTrue("thumb_up_num" in res)
        self.assertEqual(res["thumb_up_num"], 2)

        res = yield study_res_manager.find_a_resource(file_id)
        self.assertTrue("thumb_down_num" in res)
        self.assertEqual(res["thumb_down_num"], 0)
        user = "fbt_user3@fbt.com"
        user_nick = "fbt3"
        ret = yield study_res_manager.thumb_down(file_id, user)
        self.assertTrue(ret)
        res = yield study_res_manager.find_a_resource(file_id)
        self.assertTrue("thumb_down_num" in res)
        self.assertEqual(res["thumb_down_num"], 1)
        user = "fbt_user3@fbt.com"
        user_nick = "fbt3"
        ret = yield study_res_manager.thumb_down(file_id, user)
        self.assertFalse(ret)
        res = yield study_res_manager.find_a_resource(file_id)
        self.assertTrue("thumb_down_num" in res)
        self.assertEqual(res["thumb_down_num"], 1)
        user = "fbt_user4@fbt.com"
        user_nick = "fbt4"
        ret = yield study_res_manager.thumb_down(file_id, user)
        self.assertTrue(ret)
        res = yield study_res_manager.find_a_resource(file_id)
        self.assertTrue("thumb_down_num" in res)
        self.assertEqual(res["thumb_down_num"], 2)

    @tornado.testing.gen_test(timeout=20)
    def test_merge_college(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["college"]=college
        resource["file_hash"] = str(long(time()*1000))

        resource["filename"]="test file"
        file_id = yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        course = yield study_res_manager.get_course("test", university, college)
        self.assertEqual(utf8(course["university"]), university)
        self.assertEqual(utf8(course["college"]), college)
        self.assertEqual(course["location"], study_res_manager.generate_course_location(university, college))
        # self.assertEqual(course["course_id"], study_res_manager.generate_course_id("test", university, college))
        res = yield study_res_manager.find_a_resource(file_id)
        self.assertEqual(utf8(res["university"]), university)
        self.assertEqual(utf8(res["college"]), college)
        self.assertEqual(res["location"], study_res_manager.generate_course_id("test", university, college))

        college2 = college
        university, college = university_db.random_choose_college_of_university(university)
        # generate a new course
        resource["college"] = college
        resource["file_hash"] = str(long(time()*1000))
        yield study_res_manager.upload_resource(resource.copy(), "test_user2@test.com", mock_uid("test_user2@test.com"))
        ok = yield study_res_manager.merge2college(university, college2, college)
        self.assertTrue(ok)
        course = yield study_res_manager.get_course("test", university, college)
        self.assertEqual(utf8(course["university"]), university)
        self.assertEqual(utf8(course["college"]), college)
        self.assertEqual(course["location"], study_res_manager.generate_course_location(university, college))
        # self.assertEqual(course["course_id"], study_res_manager.generate_course_id("test", university, college))
        res = yield study_res_manager.find_a_resource(file_id)
        self.assertEqual(utf8(res["university"]), university)
        self.assertEqual(utf8(res["college"]), college)
        self.assertEqual(res["location"], study_res_manager.generate_course_id("test", university, college))

    @tornado.testing.gen_test(timeout=20)
    def test_customize_upload_course_and_overview(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        course_name = "course_name_what_ever"
        yield study_res_manager.customize_add_course(university, college, course_name, None)
        total_page, current_page, resource_list = yield study_res_manager.get_course_resources(university, college, course_name, current_page=1)
        self.assertEqual(total_page, 0)
        self.assertEqual(current_page, 1)
        course_list = self.redis_db_man.get_college_courses(university, college)
        self.assertEqual(len(course_list), 1)
        print "test customize add course over."

    @tornado.testing.gen_test(timeout=20)
    def test_upload_the_same_resource(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["college"]=college
        user = "test@test.com"
        resource["tag"] = "tag"
        resource["file_hash"] = str(long(time()*1000))
        file_id = yield study_res_manager.upload_resource(resource.copy(), user, mock_uid(user))
        self.assertTrue(file_id is not None)

        file_id = yield study_res_manager.upload_resource(resource.copy(), user,mock_uid(user), False)
        self.assertTrue(file_id is None)

        resource["file_hash"] = str(long(time()*1000))
        file_id = yield study_res_manager.upload_resource(resource.copy(), user, mock_uid(user))
        self.assertTrue(file_id is not None)

    @tornado.testing.gen_test(timeout=20)
    def test_insert_preview_key(self):
        university_db = UniversityDB()
        university,college = university_db.random_choose_college_of_university()
        study_res_manager = self.study_res_manager
        resource ={}
        for key in ("course", "teacher", "university", "college","filename", "file_size","resource_name","download_link","description","tag","rate"):
            resource[key]="test"
        resource["university"]=university
        resource["college"]=college
        user = "test@test.com"
        resource["tag"] = "tag"
        file_hash = str(long(time()*1000))
        resource["file_hash"] = file_hash
        resource["file_key"] = file_hash
        file_id = yield study_res_manager.upload_resource(resource.copy(), user, mock_uid(user))
        res = yield study_res_manager.find_a_resource(file_id)
        self.assertTrue("preview_key" not in res)
        file_key = "xxxxxxfile_keyxxxxxx"
        yield study_res_manager.insert_preview_key(res["file_key"], file_hash, file_key)
        res = yield study_res_manager.find_a_resource(file_id)
        self.assertTrue("preview_key" in res)
        self.assertEqual(res["preview_key"], file_key)
        file_key2 = "xxxxxxfile_key2xxxxxx"
        yield study_res_manager.insert_preview_key(res["file_key"], file_hash, file_key2)
        res = yield study_res_manager.find_a_resource(file_id)
        self.assertTrue("preview_key" in res)
        self.assertEqual(res["preview_key"], file_key2)
        print "preview insert key OK"


if __name__ == '__main__':
    import unittest
    unittest.main()

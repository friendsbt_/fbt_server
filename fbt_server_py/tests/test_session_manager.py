# coding: utf-8
__author__ = 'bone-lee'

import sys
sys.path.append('..')
from session_manager import SessionManager

from tornado import testing
from redis import StrictRedis
import simplejson as json


class SessionManagerTestCase(testing.AsyncTestCase):
    def test_gen_session_id(self):
        redis_session = StrictRedis()
        redis_session.flushdb()
        session_man = SessionManager(redis_session)
        uid_set = set()
        for i in range(100):
            sid = session_man.generate_session_id()
            assert sid not in uid_set
            uid_set.add(sid)

    def test_set_session_id(self):
        redis_session = StrictRedis()
        redis_session.flushdb()
        session_man = SessionManager(redis_session)
        sid = session_man.generate_session_id()
        user_info = json.dumps({"user": "123@test.com", "nick_name": "随便吧", "icon": "icon_test"})
        session_man.save_session_id(sid, user_info)
        user_info2 = session_man.get_user_info_by_sid(sid)
        self.assertEqual(user_info, user_info2)
        session_man.del_session(sid)
        user_info2 = session_man.get_user_info_by_sid(sid)
        self.assertEqual(user_info2, None)


if __name__ == '__main__':
    import unittest
    unittest.main()

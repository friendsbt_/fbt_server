# -*- coding: utf-8 -*-
__author__ = 'bone'

import sys
import os.path

fbt_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

sys.path.append(fbt_path)

from experience_tag_manager import QuestionTagManager

import tornado.testing
import tornado.gen
import redis


class ExperienceTagManagerTestCase(tornado.testing.AsyncTestCase):
    def setUp(self):
        super(ExperienceTagManagerTestCase, self).setUp()
        redis_db = redis.StrictRedis()
        redis_db.flushdb()
        self.experience_tag_man = QuestionTagManager(redis_db, redis_db)

    def test_post_experience(self):
        tags = ["计算机","中科院","计算所"]
        tag_class = "考研"
        experience_id = "exp_id123431"
        tag_num = self.experience_tag_man.get_all_tags_by_class(tag_class)
        self.assertDictEqual(tag_num, {})

        self.experience_tag_man.add_question_tag(tags, tag_class)

        tag_num = self.experience_tag_man.get_all_tags_by_class(tag_class)
        self.assertDictEqual(tag_num, {"计算机": '1', "中科院": '1', "计算所": '1'})

        self.experience_tag_man.remove_question_tag(tags, tag_class)
        tag_num = self.experience_tag_man.get_all_tags_by_class(tag_class)
        self.assertDictEqual(tag_num, {})

    def test_extract_tag(self):
        tags = ["计算机", "中科院", "计算所"]
        tag_class = "考研"
        experience_id = "exp_id123431"
        extracted_tags = self.experience_tag_man.extract_tag_from_text("这是一个关于中科院计算机问题的测试", tag_class)
        self.assertDictEqual(extracted_tags, {"found_tags": [], "recommend_tags": ["中科院", "计算机", "测试"]})
        self.experience_tag_man.add_question_tag(tags, tag_class)
        extracted_tags = self.experience_tag_man.extract_tag_from_text("这是中科院一个关于计算机问题的测试", tag_class)
        self.assertDictEqual(extracted_tags, {"found_tags": ["中科院", "计算机"], "recommend_tags": ["测试"]})
        extracted_tags = self.experience_tag_man.extract_tag_from_text("这是中科院计算所一个关于计算机问题的测试", tag_class)
        self.assertDictEqual(extracted_tags, {"found_tags": ["计算机"], "recommend_tags": ["中科院计算所", "测试"]})


    # def test_join_experience(self):
    #     tags = ["计算机","中科院","计算所"]
    #     tag_class = "考研"
    #     experience_id = "exp_id123431"
    #     s = self.experience_tag_man.join_experience_by_tags(["考研:计算机"])
    #     self.assertSetEqual(s, set())
    #     s = self.experience_tag_man.join_experience_by_tags(["考研:中科院"])
    #     self.assertSetEqual(s, set())
    #     s = self.experience_tag_man.join_experience_by_tags(["考研:计算所"])
    #     self.assertSetEqual(s, set())
    #     s = self.experience_tag_man.join_experience_by_tags(["考研:计算机", "考研:中科院", "考研:计算所"])
    #     self.assertSetEqual(s, set())
    #
    #     self.experience_tag_man.user_post_experience(tags, tag_class, experience_id)
    #     s = self.experience_tag_man.join_experience_by_tags(["考研:计算机", "考研:中科院", "考研:计算所"])
    #     self.assertSetEqual(s, set([experience_id]))
    #
    #     experience_id2 = "exp_id1223112"
    #
    #     self.experience_tag_man.user_post_experience(tags, tag_class, experience_id2)
    #     s = self.experience_tag_man.join_experience_by_tags(["考研:计算机"])
    #     self.assertSetEqual(s, set([experience_id, experience_id2]))
    #     s = self.experience_tag_man.join_experience_by_tags(["考研:中科院"])
    #     self.assertSetEqual(s, set([experience_id, experience_id2]))
    #     s = self.experience_tag_man.join_experience_by_tags(["考研:计算所"])
    #     self.assertSetEqual(s, set([experience_id, experience_id2]))
    #     s = self.experience_tag_man.join_experience_by_tags(["考研:计算机", "考研:中科院", "考研:计算所"])
    #     self.assertSetEqual(s, set([experience_id, experience_id2]))
    #
    #     self.experience_tag_man.user_post_experience(tags, tag_class, experience_id2)
    #     s = self.experience_tag_man.join_experience_by_tags(["考研:计算机"])
    #     self.assertSetEqual(s, set([experience_id, experience_id2]))
    #     s = self.experience_tag_man.join_experience_by_tags(["考研:中科院"])
    #     self.assertSetEqual(s, set([experience_id, experience_id2]))
    #     s = self.experience_tag_man.join_experience_by_tags(["考研:计算所"])
    #     self.assertSetEqual(s, set([experience_id, experience_id2]))
    #     s = self.experience_tag_man.join_experience_by_tags(["考研:计算机", "考研:中科院", "考研:计算所"])
    #     self.assertSetEqual(s, set([experience_id, experience_id2]))

    # def test_insert_user_interested_experience(self):
    #     user = "test@test.com"
    #     experience_id = "exp_id123431"
    #     exp_list = self.experience_tag_man.get_user_interested_experience(user)
    #     self.assertListEqual(exp_list, [])
    #     self.experience_tag_man.insert_user_interested_experience(user, experience_id)
    #     exp_list = self.experience_tag_man.get_user_interested_experience(user)
    #     self.assertListEqual(exp_list, [experience_id])
    #     experience_id2 = "exp_id1234312222"
    #     self.experience_tag_man.insert_user_interested_experience(user, experience_id2)
    #     exp_list = self.experience_tag_man.get_user_interested_experience(user)
    #     self.assertListEqual(exp_list, [experience_id2, experience_id])
    #     experience_id3 = "exp_id_lxxxxx"
    #     for i in range(self.experience_tag_man.EXP_LIST_NUM-1):
    #         self.experience_tag_man.insert_user_interested_experience(user, experience_id3)
    #     exp_list = self.experience_tag_man.get_user_interested_experience(user)
    #     self.assertEqual(len(exp_list), self.experience_tag_man.EXP_LIST_NUM)
    #     self.assertEqual(exp_list[0], experience_id3)
    #     self.assertEqual(exp_list[-1], experience_id2)

if __name__ == '__main__':
    import unittest
    unittest.main()
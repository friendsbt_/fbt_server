# -*- coding: utf-8 -*-
__author__ = 'bone'

import sys
import os.path

fbt_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

sys.path.append(fbt_path)

from university_course_manager import RedisDBManager
from university_course_manager import RedisDBManagerP2P
from university_db import UniversityDB
from tornado.escape import utf8

import tornado.testing
import tornado.gen
import redis
import mock
from time import time
from random import randint

class RedisDBManagerTestCase(tornado.testing.AsyncTestCase):
    def test_upload(self):
        redis_db = redis.StrictRedis()
        redis_db.flushdb()
        redis_db_man = RedisDBManager(redis_db)

        university_db = UniversityDB()

        course = "TODO course"
        university, college = university_db.random_choose_college_of_university()
        self.assertEqual(redis_db_man.get_university_course_num(university), 0)
        self.assertEqual(redis_db_man.get_college_course_num(university, college), 0)

        university, college = university_db.random_choose_college_of_university()
        redis_db_man.user_upload_resource(university, college, course)
        self.assertEqual(redis_db_man.get_university_course_num(university), 1)
        self.assertEqual(redis_db_man.get_college_course_num(university, college), 1)

        redis_db_man.user_upload_resource(university, college, course)
        self.assertEqual(redis_db_man.get_university_course_num(university), 1)
        self.assertEqual(redis_db_man.get_college_course_num(university, college), 1)

    def test_remove_course(self):
        redis_db = redis.StrictRedis()
        redis_db.flushdb()
        redis_db_man = RedisDBManager(redis_db)

        university_db = UniversityDB()

        course = "TODO course"
        university, college = university_db.random_choose_college_of_university()
        self.assertEqual(redis_db_man.get_university_course_num(university), 0)
        self.assertEqual(redis_db_man.get_college_course_num(university, college), 0)

        university, college = university_db.random_choose_college_of_university()
        redis_db_man.user_upload_resource(university, college, course)
        self.assertEqual(redis_db_man.get_university_course_num(university), 1)
        self.assertEqual(redis_db_man.get_college_course_num(university, college), 1)

        redis_db_man.delete_course(university, college, course)
        self.assertEqual(redis_db_man.get_university_course_num(university), 0)
        self.assertEqual(redis_db_man.get_college_course_num(university, college), 0)

        redis_db_man.delete_course(university, college, course)
        self.assertEqual(redis_db_man.get_university_course_num(university), 0)
        self.assertEqual(redis_db_man.get_college_course_num(university, college), 0)


    def test_get_resource_overview(self):
        redis_db = redis.StrictRedis()
        redis_db.flushdb()
        redis_db_man = RedisDBManager(redis_db)

        university_db = UniversityDB()

        course = "TODO course"
        university, college = university_db.random_choose_college_of_university()
        # self.assertEqual(redis_db_man.get_university_resource_num(university), 0)
        # self.assertEqual(redis_db_man.get_college_resource_num(university, college), 0)
        # self.assertEqual(redis_db_man.get_course_resource_num(university, college, course), 0)

        university_course_dict= {}
        university_course_cnt_tmp={}
        for i in range(1000):
            university, college = university_db.random_choose_college_of_university()
            redis_db_man.register_user(university, college)
            course_suffix = randint(0,10)
            course_name = course+str(course_suffix)
            redis_db_man.user_upload_resource(university, college, course_name)
            if university not in university_course_dict:
                university_course_dict[university]=0
            key = university+":"+college
            if key not in university_course_dict:
                university_course_dict[key] = set()
            if course_name not in university_course_dict[key]:
                university_course_dict[key].add(course_name)
                if university not in university_course_cnt_tmp:
                    university_course_cnt_tmp[university]=0
                university_course_cnt_tmp[university]+=1
        sorted_university_res_cnt = sorted(university_course_cnt_tmp.items(),key = lambda x:x[1], reverse=True)
        res_page_num = RedisDBManager.RES_NUM_IN_A_PAGE
        total_page = (len(university_db.get_university())+res_page_num-1)/res_page_num
        for page in range(1, len(sorted_university_res_cnt)/res_page_num):
            res_list, cur_page, total_page2 = redis_db_man.get_university_course_overview(page, 0, 0)
            self.assertEqual(cur_page, page)
            self.assertEqual(total_page, total_page2)
            i = 0
            for university,res_cnt in sorted_university_res_cnt[(page-1)*res_page_num:page*res_page_num]:
                self.assertEqual(res_list[i]["resource_num"],res_cnt)
                # self.assertEqual(res_list[i]["university"],university)
                i+=1

    def test_get_college_resource_num_list(self):
        redis_db = redis.StrictRedis()
        redis_db.flushdb()
        redis_db_man = RedisDBManager(redis_db,redis_db)

        university_db = UniversityDB()
        course = "TODO course"

        university = None
        college_course = {}
        college_course2 = {}
        for k in range(100):
            university, college = university_db.random_choose_college_of_university(university)
            if college not in college_course:
                college_course[college]=0
            course_suffix = randint(0,10)
            course_name = course+str(course_suffix)
            redis_db_man.user_upload_resource(university, college, course_name)
            if college not in college_course2:
                college_course2[college] = set()
            if course_name not in college_course2[college]:
                college_course2[college].add(course_name)
                college_course[college]+=1
        college_resource_num_list =redis_db_man.get_college_course_num_list(university)
        college_resource_num_list2 = sorted(college_course.items(),key = lambda x:x[1], reverse=True)
        i = 0
        for college, course_num in college_resource_num_list2:
            self.assertEqual(course_num, college_resource_num_list[i]["resource_num"])
            i += 1
        print "get_college_resource_num_list passed test"


    def test_search_resource(self):
        redis_db = redis.StrictRedis()
        redis_db.flushdb()
        redis_db_man = RedisDBManager(redis_db,redis_db)

        university_db = UniversityDB()
        course = "TODO course"

        for i in range(1000):
            university, college = university_db.random_choose_college_of_university()
            upload_time = randint(0,10)
            for i in range(upload_time):
                redis_db_man.user_upload_resource(university, college, course)
        university = "电子"
        page = 1
        res_list, cur_page, total_page2 = redis_db_man.search_resource_by_university(university, page, 0, 0)
        self.assertEqual(cur_page, page)
        self.assertTrue(total_page2 > 0)
        for res in res_list:
            assert university in utf8(res["university"])
        redis_db_man.search_resource_by_university(university, page, 0, 0)
        print "test over."

    def test_search_resource2(self):
        redis_db = redis.StrictRedis()
        redis_db.flushdb()
        redis_db_man = RedisDBManager(redis_db,redis_db)

        course = "TODO course"
        redis_db_man.user_upload_resource("中国地质大学（北京）", "其它院系", course)
        redis_db_man.user_upload_resource("中国地质大学（武汉）", "其它院系", course)
        university = "地大"
        page = 1
        res_list, cur_page, total_page2 = redis_db_man.search_resource_by_university(university, page, 0, 0)
        self.assertEqual(cur_page, page)
        self.assertTrue(total_page2 > 0)
        for res in res_list:
            assert "中国地质大学" in utf8(res["university"])

        res_list, cur_page, total_page2 = redis_db_man.search_resource_by_university("中地大", page, 0, 0)
        self.assertEqual(cur_page, page)
        self.assertTrue(total_page2 > 0)
        for res in res_list:
            assert "中国地质大学" in utf8(res["university"])


    def test_get_college_course_list(self):
        redis_db = redis.StrictRedis()
        redis_db.flushdb()
        redis_db_man = RedisDBManager(redis_db,redis_db)

        university_db = UniversityDB()

        college_course = {}
        for i in range(1000):
            course = "TODO course "+ str(randint(0,100))
            university, college = university_db.random_choose_college_of_university()
            redis_db_man.user_upload_resource(university, college, course)
            k = university+":"+college
            if k not in college_course:
                college_course[k] = set()
            college_course[k].add(course)
        for k in college_course.keys():
            university,college = k.split(":")
            course_list = redis_db_man.get_college_courses(university, college)
            self.assertEqual(len(course_list), len(college_course[k]))

    def test_get_online_users(self):
        redis_db = redis.StrictRedis()
        redis_db.flushdb()
        redis_db_man = RedisDBManager(redis_db,redis_db)
        user = "bone"
        online_users = redis_db_man.get_online_users()
        self.assertEqual(len(online_users),0)
        redis_db_man.record_login_user(user, "192.168.1.1")
        online_users = redis_db_man.get_online_users()
        self.assertEqual(len(online_users),1)
        redis_db_man.record_logout_user(user)
        online_users = redis_db_man.get_online_users()
        self.assertEqual(len(online_users),0)

    def test_lua(self):
        lua = """
        local users = redis.call('set', "name", "bone")
        local result = redis.call('get', "name")
        return result
        """
        lua_func = redis.StrictRedis().register_script(lua)
        self.assertEqual(lua_func(keys=[], args=[]), "bone")

    def test_lua2(self):
        lua = """
        redis.call('sadd', "set_list", "bone")
        redis.call('sadd', "set_list", "lee")
        local result = redis.call('smembers', "set_list")
        return result
        """
        lua_func = redis.StrictRedis().register_script(lua)
        self.assertListEqual(lua_func(keys=[], args=[]), ["bone", "lee"])

    def test_lua3(self):
        lua = """
        local users = redis.call('smembers', "online_user")
        local result = {}
        for i, v in ipairs(users) do
            result[v]=redis.call('get', 'online_user:'..v)
        end
        return result
        """
        get_online_users_list = redis.StrictRedis().register_script(lua)
        self.assertListEqual(get_online_users_list(), [])

    def test_check_preview_num(self):
        with mock.patch("university_course_manager.RedisDBManager.MAX_PREVIEW_A_DAY", 2) as what_ever:
            with mock.patch("university_course_manager.RedisDBManager.ONE_DAY", 2) as what_ever2:
                redis_db = redis.StrictRedis()
                redis_db.flushdb()
                redis_db_man = RedisDBManager(redis_db, redis_db)

                is_valid = redis_db_man.is_valid_preview("1.1.1.1")
                self.assertEqual(is_valid, True)
                is_valid = redis_db_man.is_valid_preview("1.1.1.1")
                self.assertEqual(is_valid, True)
                is_valid = redis_db_man.is_valid_preview("1.1.1.1")
                self.assertEqual(is_valid, False)
                def test_func():
                    is_valid = redis_db_man.is_valid_preview("1.1.1.1")
                    self.assertEqual(is_valid, True)
                    print "test check valid preview pass"
                    self.stop()

                self.io_loop.add_timeout(time() + 3, test_func)
                self.wait()

    def test_check_download_num(self):
        with mock.patch("university_course_manager.RedisDBManager.MAX_DOWNLOAD_A_DAY_LOGIN", 4) as what_ever:
          with mock.patch("university_course_manager.RedisDBManager.MAX_DOWNLOAD_A_DAY_NOT_LOGIN", 2) as what_ever:
            with mock.patch("university_course_manager.RedisDBManager.ONE_DAY", 2) as what_ever2:
                redis_db = redis.StrictRedis()
                redis_db.flushdb()
                redis_db_man = RedisDBManager(redis_db, redis_db)

                is_valid = redis_db_man.is_valid_download(None, "1.1.1.1")
                self.assertEqual(is_valid, True)
                is_valid = redis_db_man.is_valid_download(None, "1.1.1.1")
                self.assertEqual(is_valid, True)
                is_valid = redis_db_man.is_valid_download(None, "1.1.1.1")
                self.assertEqual(is_valid, False)

                # login and continue download
                is_valid = redis_db_man.is_valid_download("test@test.com", "1.1.1.1")
                self.assertEqual(is_valid, True)
                is_valid = redis_db_man.is_valid_download("test@test.com", "1.1.1.1")
                self.assertEqual(is_valid, True)
                is_valid = redis_db_man.is_valid_download("test@test.com", "1.1.1.1")
                self.assertEqual(is_valid, False)
                is_valid = redis_db_man.is_valid_download("test2@test.com", "1.1.1.1")
                self.assertEqual(is_valid, False)
                is_valid = redis_db_man.is_valid_download("test@test.com", "2.1.1.1")
                self.assertEqual(is_valid, True)
                is_valid = redis_db_man.is_valid_download("test@test.com", "2.1.1.1")
                self.assertEqual(is_valid, False)

                def test_func():
                    is_valid = redis_db_man.is_valid_download("test@test.com", "1.1.1.1")
                    self.assertEqual(is_valid, True)
                    is_valid = redis_db_man.is_valid_download("test2@test.com", "1.1.1.1")
                    self.assertEqual(is_valid, True)
                    is_valid = redis_db_man.is_valid_download("test@test.com", "2.1.1.1")
                    self.assertEqual(is_valid, True)
                    print "test check valid download pass"
                    self.stop()

                self.io_loop.add_timeout(time() + 3, test_func)
                self.wait()


    def test_university_course_manager_p2p_online(self):
        redis_db = redis.StrictRedis()
        redis_db.flushdb()
        redis_db_man = RedisDBManagerP2P(redis_db,redis_db)
        user = "bone"
        online_users = redis_db_man.get_online_users()
        self.assertEqual(len(online_users),0)
        redis_db_man.record_login_user(user,"192.168.1.1")
        online_users = redis_db_man.get_online_users()
        self.assertEqual(len(online_users),1)
        redis_db_man.record_logout_user(user)
        online_users = redis_db_man.get_online_users()
        self.assertEqual(len(online_users),0)



if __name__ == '__main__':
    import unittest
    unittest.main()

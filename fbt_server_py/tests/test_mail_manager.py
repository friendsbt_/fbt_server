# coding: utf8
__author__ = 'bone-lee'

import sys
sys.path.append('..')

from mail_manager import MailMan

import tornado.gen
import tornado.testing


class MailManTestCase(tornado.testing.AsyncTestCase):
    @tornado.testing.gen_test
    def test_mail_validation(self):
        man = MailMan()
        assert man.is_985_mail("test@mails.ucas.ac.cn", "中国科学院")
        assert man.is_985_mail("test@csu.edu.cn", "中南大学")
        assert not man.is_985_mail("test@whatever.edu.cn", "中南大学")
        assert not man.is_985_mail("test@#csu.edu.cn","中南大学")

if __name__ == '__main__':
    import unittest
    unittest.main()

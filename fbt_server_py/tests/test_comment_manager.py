__author__ = 'bone'

import sys
import os.path

fbt_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

sys.path.append(fbt_path)

from comment_manager import ResourceCommentManager,ResourceCommentManagerP2P, CourseCommentManager

from tornado import testing, gen
from motor import MotorClient

class CommentManagerTestCase(testing.AsyncTestCase):
    # def setUp(self):
    #     pass
    #
    # def tearDown(self):
    #     pass

    @testing.gen_test
    def test_post_and_get_comment(self):
        yield self.test_post_and_get_comment_helper(ResourceCommentManager)

    @testing.gen_test
    def test_post_and_get_comment_p2p(self):
        yield self.test_post_and_get_comment_helper(ResourceCommentManagerP2P)

    @testing.gen_test
    def test_post_and_get_course_comment(self):
        yield self.test_post_and_get_comment_helper(CourseCommentManager)

    @gen.coroutine
    def test_post_and_get_comment_helper(self, class_name):
        db = MotorClient().fbt_test
        comment_manager = class_name(db)
        yield comment_manager.get_collection().remove({})
        file_id, comment, user_name, nick_name, uid, user_icon = "test_file_id", u"TEST content", "test@test.com", "test", 1234, "static/img/test.jpg"
        total_page, current_page, comment_list = yield comment_manager.get_comments(file_id)
        self.assertEqual(total_page,0)
        self.assertEqual(current_page,1)
        self.assertListEqual(comment_list,[])
        yield comment_manager.post_comment(file_id, comment, user_name, nick_name, uid, user_icon)
        total_page, current_page, comment_list = yield comment_manager.get_comments(file_id)
        self.assertEqual(total_page,1)
        self.assertEqual(current_page,1)
        self.assertEqual(len(comment_list),1)

        for i in range(0,comment_manager.RES_PAGE_NUM):
            yield comment_manager.post_comment(file_id, comment, user_name, nick_name, uid, user_icon)
        total_page, current_page, comment_list = yield comment_manager.get_comments(file_id)
        self.assertEqual(total_page,2)
        self.assertEqual(current_page, 1)
        self.assertEqual(len(comment_list),comment_manager.RES_PAGE_NUM)

        total_page, current_page, comment_list = yield comment_manager.get_comments(file_id,2)
        self.assertEqual(total_page,2)
        self.assertEqual(current_page, 2)
        self.assertEqual(len(comment_list), 1)
        print "test over"
        self.stop()


if __name__ == '__main__':
    import unittest
    unittest.main()

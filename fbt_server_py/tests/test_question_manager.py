# coding: utf-8
__author__ = 'bone'

import sys
import os.path

fbt_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

sys.path.append(fbt_path)

from question_manager import QuestionManager
import tornado.testing
import tornado.gen
from motor import MotorClient
import redis
import time
from users_manager import UserManager
from university_course_manager import RedisDBManager
from bson.objectid import ObjectId

course_id = "123:321:xxxx"
questions = [{"course_name": "test1","course_id": course_id, "university": "北京大学", "college": "考研学院",
                     "title": "title1", "content": "如题，请根据学霸们的考研经历回答一下吧！谢谢！"
                     },
                     {"course_name": "test2","course_id": course_id, "university": "北京大学", "college": "考研学院",
                     "title": "title2", "content": "如题，请根据学霸们的考研经历回答一下吧！谢谢！"
                     },
                     {"course_name": "test3","course_id": course_id, "university": "北京大学", "college": "考研学院",
                     "title": "title3", "content": "如题，请根据学霸们的考研经历回答一下吧！谢谢！"
                     }]

replies = ["title" + str(_) for _ in range(20)]
comments = ["comment" + str(_) for _ in range(18)]
user = "bone@test.com"
uid = UserManager.generate_uid(user)

class QuestionanagerTestCase(tornado.testing.AsyncTestCase):
    def setUp(self):
        super(QuestionanagerTestCase, self).setUp()
        self.db = MotorClient().fbt_test
        #MotorClient().drop_database('fbt_test')
        self.question_man = QuestionManager(db=self.db)
        self.io_loop.run_sync(self.fixture_setup)

    @tornado.gen.coroutine
    def fixture_setup(self):
        redis_db = redis.StrictRedis()
        redis_cache = redis.StrictRedis()
        redis_db.flushdb()
        redis_cache.flushdb()
        redis_db_man = RedisDBManager(redis_db, redis_cache)
        user_manager = UserManager(self.db, redis_cache, redis_db_man)
        passwd, user_icon_url, real_name, school, college, nick, gender = "123", "NOT SET", "bone", "NOT SET", "NOT SET", "bonelee", "MALE"
        yield user_manager.register_user(user, passwd, user_icon_url, real_name, school, college, nick, gender)
        yield self.db.questions.remove()

    def tearDown(self):
        pass

    @tornado.testing.gen_test
    def test_issue_question(self):
        question_list = yield self.question_man.get_question_overview(course_id, 1)
        self.assertEqual(len(question_list), 0)
        #self.assertEqual(total_page, 0)
        #self.assertEqual(cur_page, 1)

        # uid = 12344
        # user = "test@fbt.com"
        question = questions[0]
        question_id = yield self.question_man.issue_a_question(question, user, uid, "nick name", "test.jpg")
        self.assertTrue(question_id is not None)
        question_list = yield self.question_man.get_question_overview(course_id, 1)
        self.assertEqual(len(question_list), 1)

        question = questions[1]
        question_id = yield self.question_man.issue_a_question(question, user, uid, "nick name", "test.jpg")
        question = questions[2]
        question_id = yield self.question_man.issue_a_question(question, user, uid, "nick name", "test.jpg")
        question_list = yield self.question_man.get_question_overview(course_id, 1)
        self.assertEqual(len(question_list), 3)
        self.assertEqual([_['title'] for _ in question_list], [_['title'] for _ in questions])

        question_id = str(question_list[2]['_id'])
        question = yield self.question_man.question_view(question_id)
        self.assertEqual(question["reply_num"], 0)

        time.sleep(1)
        for r in replies[:3]:
            yield self.question_man.reply_question(question_id, uid, r)
        question = yield self.question_man.question_view(question_id)
        self.assertEqual(question["reply_num"], 3)

        question_list = yield self.question_man.get_question_overview(course_id, 1, '1')
        self.assertEqual([_['title'] for _ in question_list], ["title3", "title1", "title2"])

        # reply_view_more
        for r in replies[3:]:
            yield self.question_man.reply_question(question_id, uid, r)
        reply_list = yield self.question_man.reply_view_more(question_id, 2, '0')
        reply_list = reply_list['reply_list']
        self.assertEqual([_['content'] for _ in reply_list], replies[10:])

        # cons
        reply_id = reply_list[0]['id']
        res = yield self.question_man.cons(question_id, reply_id, uid)
        self.assertEqual(res, 1)
        reply = yield self.db.quest_replys.find_one({"id": reply_id}, {"protesters": 1})
        self.assertTrue(uid in reply['protesters'])
        reply_list = yield self.question_man.reply_view_more(question_id, 2, '0')
        reply_list = reply_list['reply_list']
        self.assertEqual(reply_list[0]["cons"], 1)
        #cons again
        res = yield self.question_man.cons(question_id, reply_id, uid)
        self.assertEqual(res, 0)
        reply = yield self.db.quest_replys.find_one({"id": reply_id}, {"protesters": 1})
        self.assertTrue(uid in reply['protesters'])
        reply_list = yield self.question_man.reply_view_more(question_id, 2, '0')
        reply_list = reply_list['reply_list']
        self.assertEqual(reply_list[0]["cons"], 1)

        # pros
        res = yield self.question_man.pros(question_id, reply_id, uid)
        reply_list = yield self.question_man.reply_view_more(question_id, 1, '1')
        reply_list = reply_list['reply_list']
        self.assertEqual(reply_list[0]["content"], "title10")

    @tornado.testing.gen_test
    def test_comment_reply(self):
        # uid = 12344
        # user = "test@fbt.com"
        question = questions[0]
        question_id = yield self.question_man.issue_a_question(question, user, uid, "nick name", "test.jpg")
        self.assertTrue(question_id is not None)
        reply_id = yield self.question_man.reply_question(question_id, uid, replies[0])
        for c in comments:            
            yield self.question_man.comment_reply(question_id, reply_id, uid, None, c)
        res = yield self.db.questions.find_one({"_id": ObjectId(question_id)})
        self.assertEqual(res["comment_num"], len(comments))
        res = yield self.question_man.comment_view(reply_id, 1)
        self.assertEqual(res["total_page"], 2)
        self.assertEqual(len(res["comments"]), 10)
        res = yield self.question_man.comment_view(reply_id, 2)
        self.assertEqual(len(res["comments"]), 8)

if __name__ == '__main__':
    import unittest
    unittest.main()

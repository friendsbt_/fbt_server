# -*- coding: utf-8 -*-
__author__ = 'bone'

import sys
import os.path

fbt_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

sys.path.append(fbt_path)

from user_resource_manager import UserResourceManager
from university_db import UniversityDB

from random import randint
import tornado.testing
import tornado.gen
import motor

def mock_uid(user):
    return abs(hash(user))

class UserResourceManagerTestCase(tornado.testing.AsyncTestCase):
    @tornado.testing.gen_test(timeout=5)
    def test_download_and_overview(self):
        # WORK
        db = motor.MotorClient().fbt_test #motorclient.fbt_test
        user_res_man = UserResourceManager(db)
        university_db = UniversityDB()

        yield user_res_man.clear_db_just_for_test()

        file_set = set()
        user = "test@test.com"
        for i in range(80):
            file_id = "file "+ str(randint(0,30))
            file_size = 123
            file_name = "random file name"
            university, college = university_db.random_choose_college_of_university()
            res = yield user_res_man.add_to_resource_list(file_id, user, file_size, file_name, university, college)
            if file_id not in file_set:
                self.assertEqual(res, None)
                # print "Ok...."
                file_set.add(file_id)

        total_page, current_page, resource_list = yield user_res_man.get_user_download(user,current_page=1)
        self.assertEqual(total_page, (len(file_set)+user_res_man.RES_PAGE_NUM-1)/user_res_man.RES_PAGE_NUM)
        self.assertEqual(current_page, 1)
        self.assertEqual(len(resource_list),user_res_man.RES_PAGE_NUM)
        print "test over."
        self.stop()

if __name__ == '__main__':
    import unittest
    unittest.main()

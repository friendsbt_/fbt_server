__author__ = 'bone'

from tornado import gen
from datetime import datetime
from time import time
import motorclient


class UserUploadLogManager(object):
    def __init__(self, db = None):
        if db:
            self._db = db
        else:
            self._db = motorclient.fbt

    @gen.coroutine
    def clear_db_just_for_test(self):
        yield self._db.user_upload_log.remove({})

    @gen.coroutine
    def save(self, data, user):
        data["index_ctime"] = long(time())
        data["ctime"] = datetime.now().strftime('%Y-%m-%d %H:%M')
        data["user"] = user
        yield self._db.user_upload_log.insert(data)

    @gen.coroutine
    def get_log_list(self, user=None):
        if user:
            cursor = self._db.user_upload_log.find({"user": user},{"_id":0})
        else:
            cursor = self._db.user_upload_log.find({},{"_id":0})
        log_list = yield cursor.to_list(None)
        raise gen.Return(log_list)
# coding: utf8
__author__ = 'bone'

from QA_manager import QAManager
from user_generator import UserGenerator
from university_db import UniversityDB

import mongoclient
from tornado import gen
from tornado import ioloop
from tornado.escape import to_unicode, utf8
import os

from random import sample, shuffle, randint
import codecs

DEBUG = False
user_generator = UserGenerator()
university_db = UniversityDB()


def random_choose(array):
    return sample(array, 1)[0]


def line_with(line, prefix):
    return prefix in line


def line_start_with(line, prefix):
    return line[:len(prefix)] == prefix


def collect_saying():
    ret = []
    for dir, subdirs, names in os.walk("QA/saying"):
        for name in names:
            path = os.path.join(dir, name)
            file_name, suffix = os.path.splitext(name)
            print "processing file:", path
            if suffix != ".txt":
                continue
            try:
                f = codecs.open(path, 'r', 'gbk')
                lines = f.readlines()
                f.close()
                # print lines
                ret.extend(lines)
                print "process OK"
            except UnicodeDecodeError:
                print "error. weired charset. file:", path
    return ret


def get_users():
    faked_users = ["xy001@xyxk.com", "xy002@xyxk.com", "xy003@xyxk.com",
                   "xy004@xyxk.com", "xy005@xyxk.com", "546762602@qq.com"]
    return [mongoclient.fbt.users.find_one({"user": u}) for u in faked_users]


@gen.coroutine
def post_question(question, debug=False, qa_man=None):
    if university_db.is_valid_university(question["university"]):
        university = question["university"]
    else:
        print "invalid university in question:", question["university"]
        university = None
    question["gender"] = utf8(question["gender"])
    if question["gender"] == "男" or question["gender"] == "女":
        gender = question["gender"]
    else:
        r = randint(1, 10)
        gender = "男" if r <= 4 else "女"
        print "invalid gender in question:", question["gender"], "generate gender:", gender
    user_info = user_generator.generate_user(university=university, gender=gender)
    user_desc = ""
    print user_info["user"], user_info["uid"], user_info["real_name"], user_info["university"], user_info["icon"], gender
    if debug:
        raise gen.Return(None)
    qid = yield qa_man.post_question(question, user_info["user"], user_info["uid"],
                                     user_info["real_name"], user_info["icon"],
                                     user_info["university"], "", user_desc, real_name=user_info["real_name"],
                                     sync=True)
    raise gen.Return(qid)


@gen.coroutine
def post_answer(answer, debug=False, qa_man=None, user_info=None):
    if university_db.is_valid_university(answer["university"]):
        university = answer["university"]
    else:
        print "invalid university in answer:", answer["university"]
        university = None
    answer["gender"] = utf8(answer["gender"])
    if answer["gender"] == "男" or answer["gender"] == "女":
        gender = answer["gender"]
    else:
        r = randint(1, 10)
        gender = "男" if r <= 4 else "女"
        print "invalid gender in answer:", answer["gender"], "generate gender:", gender

    if not user_info:
        user_info = user_generator.generate_user(university=university, gender=gender)

    user_desc = "校园星空大使"
    print user_info["user"], user_info["uid"], user_info["real_name"], user_info["university"], user_info["icon"], gender
    if debug:
        raise gen.Return(None)
    yield qa_man.post_answer(answer, user_info["user"], user_info["uid"],
                             user_info["real_name"], user_info["icon"],
                             user_info["university"], "", user_desc, real_name=user_info["real_name"], sync=True)


def line_with_keywords(line, keywords):
    for keyword in keywords:
        if keyword in line:
            return True
    return False


def find_line_start_with(lines, start, field):
    k = start
    if not line_start_with(lines[k], field):
        while k + 1 <= len(lines) and not line_start_with(lines[k + 1], field):
            k += 1
        k += 1
    assert line_start_with(lines[k], field)
    return k


def extract_field(lines, start, end, field, keywords):
    assert end < len(lines)
    k = start
    if not line_with(lines[k], field):
        while k + 1 <= end and not line_with(lines[k + 1], field):
            k += 1
        k += 1
    if not line_with(lines[k], field):
        return ""
    assert line_with(lines[k], field)
    index = lines[k].find("=")
    if index < 0:
        print "not found = in line:", lines[k], " field", field
        return ""
    field_value = lines[k][index+1:]

    while k + 1 <= end and not line_with_keywords(lines[k + 1], keywords):
        k += 1
        field_value += lines[k]
    return field_value.strip()


def extract_filed_list(lines, start, field, keywords):
    i = start
    ret = []
    while True:
        while i + 1 < len(lines) and not line_with(lines[i + 1], field):
            i += 1
        i += 1
        if i == len(lines):
            return ret
        assert line_with(lines[i], field)
        index = lines[i].find("=")
        if index < 0:
            print "error. answer(=) not found:", field, "line:", lines[i]
            break
        field_value = lines[i][index + 1:]

        while i + 1 < len(lines) and not line_with_keywords(lines[i + 1], keywords):
            i += 1
            field_value += lines[i]
        field_value = field_value.strip()
        # answer = answer.replace(" ", "&nbsp;&nbsp;&nbsp;&nbsp;")
        field_value = field_value.replace("\r", "<br>")
        # print answer_content
        ret.append(field_value)
        if not field_value:
            # print "error. answer not found:", path, lines[i], len(lines), i
            continue
        if i + 1 == len(lines):
            break
    return ret


@gen.coroutine
def generate_QA():
    question_list = []

    for class2 in ["考研", "考证", "留学", "校园", "就业", "实习"]:
        for dir, subdirs, names in os.walk("QA/collected/new2/" + class2):
            for name in names:
                path = os.path.join(dir, name)
                '''
                if DEBUG:
                    # path = "QA/collected/考研/问答 - 副本 (26).txt"
                    # path = "QA/collected/考证/学法网1 - 副本 (4).txt"
                    path = "QA/collected/new/考研/求问武大新闻学学硕考研经验？？.txt"
                    # path = "QA/collected/考研/往届生，考研，工作，迷茫，HR？.txt"
                '''
                file_name, suffix = os.path.splitext(name)
                if suffix != ".txt":
                    # print "pass file:", path
                    continue
                # print path, " is processing..."
                try:
                    try:
                        f = codecs.open(path, 'r', 'utf-8')
                        lines = f.readlines()
                        f.close()
                    except UnicodeDecodeError:
                        try:
                            f = codecs.open(path, 'r', 'gbk')
                            lines = f.readlines()
                            f.close()
                        except UnicodeDecodeError:
                            # print "pass not decoded file:", path
                            print "error. weired charset. file:", path
                            continue

                    question_keywords = ["title", "content", "tag", "user_school", "user_gender"]

                    try:
                        qa_depart_line = find_line_start_with(lines, 0, "A")
                    except IndexError:
                        print "error. A: not found. file:", path
                        continue

                    title = extract_field(lines, 0, qa_depart_line - 1, "title", question_keywords)

                    if not title:
                        title = file_name
                    # print title

                    content = extract_field(lines, 0, qa_depart_line - 1, "content", question_keywords)

                    if not content:
                        content = u"如题,先感谢大家回答了！"
                    # print content

                    tags_str = extract_field(lines, 0, qa_depart_line - 1, "tag", question_keywords)
                    # print tags_str
                    tags = tags_str.split()
                    if len(tags) == 1:
                        tags = tags_str.split(u"，")
                        if len(tags) > 1:
                            print "error. tag error. file:", path
                            continue
                    # print tags
                    if not tags:
                        print "error. tags not found:", path
                        print tags_str
                        continue
                    if len(tags) > 8:
                        print "error. tags exceed 8:", path
                        continue
                    for tag in tags:
                        if len(tag) > 10:
                            print "error. tag length exceed 10:", path
                        continue

                    link = extract_field(lines, 0, qa_depart_line - 1, "link", question_keywords)
                    # print link

                    university = extract_field(lines, 0, qa_depart_line - 1, "user_school", question_keywords)
                    # print university

                    gender = extract_field(lines, 0, qa_depart_line - 1, "user_gender", question_keywords)
                    # print gender

                    question = {"tags": tags, "class2": to_unicode(class2), "title": title, "content": content,
                                "link": link, "gender": gender, "university": university}
                    Q = {"question": question, "answers": []}

                    answer_keywords = ["content", "school", "gender", "from", "link"]
                    content_list = extract_filed_list(lines, qa_depart_line, "content", answer_keywords)
                    # print " ".join(content_list)
                    university_list = extract_filed_list(lines, qa_depart_line, "school", answer_keywords)
                    # print " University ".join(university_list)
                    gender_list = extract_filed_list(lines, qa_depart_line, "gender", answer_keywords)
                    # print " Gender ".join(gender_list)
                    link_list = extract_filed_list(lines, qa_depart_line, "link", answer_keywords)
                    # print " Link ".join(link_list)
                    if not content_list:
                        print "error. answer content not found:", path
                        continue

                    for i, content in enumerate(content_list):
                        if i < len(university_list):
                            university = university_list[i]
                        else:
                            university = ""
                        if i < len(gender_list):
                            gender = gender_list[i]
                        else:
                            gender = ""
                        if i < len(link_list):
                            link = link_list[i]
                        else:
                            link = ""
                        Q["answers"].append({"content": content, "link": link, "university": university, "gender": gender})
                    question_list.append(Q)
                except IOError as e:
                    print "pass file: %s because error: %s" % (path, e)
                except IndexError as e:
                    print "error. file, index error:", path
                    raise
                except ValueError as e:
                    print "error. file, value error:", path
                    raise

    print "Question list len:", len(question_list)
    if DEBUG:
        return
    shuffle(question_list)

    qa_man = QAManager()
    # desc_list = collect_saying()
    fake_users = get_users()
    assert fake_users
    tail = "<br><br>————————权益声明————————<br>此条内容转自网络，由星空大使为您收集，本着互联网的共享精神，我们免费为用户收集整理问题答案，" \
           "如果您发现本条回复侵犯了您的合法权益，欢迎您联系我们，我们将及时删除。<br>" \
           "邮箱:lifeng1989@pku.edu.cn。"

    for Q in question_list:
        question = Q["question"]
        qid = yield post_question(question, DEBUG, qa_man=qa_man)
        i = 0
        shuffle(fake_users)
        for answer in Q["answers"][:len(fake_users)]:
            answer["question_id"] = qid
            answer["content"] = utf8(answer["content"]) + utf8(tail)
            yield post_answer(answer, DEBUG, qa_man=qa_man, user_info=fake_users[i])
            i += 1


if __name__ == '__main__':
    ioloop.IOLoop.instance().run_sync(generate_QA)

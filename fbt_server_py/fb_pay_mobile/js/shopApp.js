var app = angular.module('shopApp', ['ngRoute']);

app.config(function($routeProvider){
    $routeProvider
    .when('/',{
        controller: 'shopController',
        templateUrl: 'partials/shop.html'
    })
    .when('/login',{
        controller: 'loginController',
        templateUrl: 'partials/login.html'
    })
    .otherwise({
        redirectTo: '/login'
    });
});

app.service('shopService', function($http,$location){
	var self = this;
    self.errorSleepTime = 500;
    //保存用户cookie，如果有的话返回true，否则返回false；
    self.saveCookie = function(){
        var pattern = /\buser_token="([^;]*)"/g;
        if(pattern.exec(document.cookie))return true;
        else return false;
    };
    self.item_list = ['real_name','university'];
    if(!self.userToken)$location.path('/login');
    self.saveInfo = function(userInfo){
        for(var i = 0;i<self.item_list.length;i++){
            var key = self.item_list[i];
            localStorage[key] = userInfo[key];
        }
        localStorage.user = userInfo.user;
    };

    self.clearInfo = function(){
        for(var i = 0; i<self.item_list.length;i++){
            var key = self.item_list[i];
            delete localStorage[key];
        }
        //删除商家交易记录
        delete localStorage.transferList;
    };

    self.userLogin = function(userData,callback){
        var req = {
            url:'/login',
            method:'POST',
            data: $.param(userData),
            headers: {
                "Content-Type":'application/x-www-form-urlencoded'
            }
        };

        $http(req).success(function(response){
            callback(response);
        }).error(function(error){
            console.log(error);
        })
    };

    self.userLogout = function(callback){
        var req = {
            url: '/logout',
            method: 'GET'
        };

        $http(req).success(function(response){
            callback(response);
        }).error(function(error){
            console.log(error);
        })
    };

    self.getTransferList = function(callback){
        var req = {
            url: '/transfer_record/list',
            method: 'GET'
        };
        $http(req).success(function(response){
            //处理交易记录，将其值放到localStorage中，方便以后添加
            localStorage.transferList = JSON.stringify(response.transfer_list);
            callback(response);
            self.getMessage();
        }).error(function(error){
            console.log(error);
        });
    };

    self.getMessage = function(){
        var req = {
            url:'/message',
            method: 'GET'
        };
        $http(req).success(function(response){
            if(response.err == 0){
                try{
                    self.synNewMsg(response.message);
                    self.content = response.message.content;
                }
                catch(e){
                    console.log("get shop transaction new message is " + e );
                    self.errorSleepTime *=2;
                    console.log("get shop transaction error: sleeping for " + self.errorSleepTime + 'ms');
                    window.setTimeout(self.getMessage(),self.errorSleepTime);
                }
                self.errorSleepTime = 500;
                window.setTimeout(self.getMessage(),self.errorSleepTime);
            }
        }).error(function(error){
            console.log(error);
            self.errorSleepTime *=2;
            window.setTimeout(self.getMessage(),self.errorSleepTime);
        });
    };

    self.synNewMsg = function(newMessage){
        var transferList = JSON.parse(localStorage.transferList);
        var message = {};
        var from = {};
        var extra_info = {};
        var stringMsg = newMessage.content;
        var real_name = stringMsg.split('(')[0];
        var otherString = stringMsg.split(')')[1];
        var pattern = /-?(\d)+/i;
        message.ctime = newMessage.ctime;
        message.how_many = pattern.exec(otherString)[0];
        from.real_name = real_name;
        extra_info.from = from;
        message.extra_info = extra_info;
        transferList.unshift(message);
        localStorage.transferList = JSON.stringify(transferList);
    }
});

app.controller('shopController', function($scope, $location, shopService){

    function init(){
        $scope.rate = 200;
        $scope.transfer_list = {};
        $scope.eachPage = 15;
        $scope.totalPage = 0;
        $scope.currentPage = 0;
        if(shopService.saveCookie())$scope.getTransferList();
        else $location.path('/login');
        $scope.real_name = localStorage.real_name;
        $scope.university = localStorage.university;
    };

    $scope.hideNewMessage = function(){
        $("#newMessage").hide();
    };


    //交易记录分页函数
    $scope.divRecord = function(page){
        var length = parseInt($scope.transfer_list.length/$scope.eachPage);
        $scope.totalPage = ($scope.transfer_list.length%$scope.eachPage == 0)? (length-1):length;
        if(page == 0){
            $scope.currentPage = 0;
        }else if(page){
            $scope.currentPage += page;
        }else {
            $scope.currentPage = 0;
        }
        var lh = $scope.transfer_list.length;
        var start = $scope.currentPage * $scope.eachPage;
        var end = (start + $scope.eachPage > lh)? lh:(start + $scope.eachPage);
        $scope.list = $scope.transfer_list.slice(start,end);

    };

    //使用watch监听新的交易信息，如果有新的，直接加到上面
    $scope.$watch(function(){
        try {
            return localStorage.transferList;
        }catch(e){
            console.log('error is ' + e);
        }
    },function(newVal, oldVal){
        try{
            if(JSON.parse(localStorage.transferList).length){
                if(shopService.content){
                    $scope.content = shopService.content;
                    $("#newMessage").show();
                }
                $scope.transfer_list = JSON.parse(localStorage.transferList);
                $scope.divRecord();
            }
        }catch(e){
        }
    },true);

    // 从服务器获取商家交易信息；
    $scope.getTransferList = function(){
        shopService.getTransferList(function(response){
            switch(response.err){
                case 0:$scope.daily_income = response.daily_income;
                        $scope.monthly_income = response.monthly_income;
                        $scope.total_income = response.total_income;
                        $scope.transfer_list = response.transfer_list;
                        $scope.divRecord();
                       break;
                case 1:console.log(response);
                       break;
            }
        })
    };

    $scope.logout = function(){
         shopService.userLogout(function(response){
            if(response){
                shopService.clearInfo();
                $location.path('/login');
            }
         })
    };

    init();
});

app.controller('loginController', function($scope, $location,shopService){
    
    function init(){
        $scope.password = '';
        $scope.errorInfo = '';
        if(shopService.saveCookie())$location.path('/');
        if(localStorage.user)$scope.username = localStorage.user;
    }

    $scope.clearErrorInfo = function(){
        $scope.errorInfo = '';
    };

    $scope.$watch(function(){
        var password = document.getElementsByName('password').value;
        return password;
    },function(newVal, oldVal){
        if(newVal && (!$scope.password))$scope.password = newVal;
    }) ;

    $scope.login = function(){
        if($scope.username){
            if($scope.password){
                var userData = {
                    'user': $scope.username,
                    'passwd': $.md5($scope.password)
                };
                shopService.userLogin(userData,function(response){
                    switch(response.err){
                        case 0:shopService.saveInfo(response.user_info);
                               $location.path('/');break;
                        case 1:$scope.errorInfo = '*亲，用户名不存在或者尚未注册';break;
                        case 2:$scope.errorInfo = '*亲，密码输入有误，请重新输入';break;
                        case 3:$scope.errorInfo = '*亲，用户名不存在或者尚未注册，请重新输入';break;
                    }
                })
            }else{
                $scope.errorInfo = '亲，密码为空';
            }
        }else{
            $scope.errorInfo = '亲，用户名为空';
        }
        if($scope.errorInfo)$scope.errorInfo = '*' + $scope.errorInfo;
    };

    init();
});
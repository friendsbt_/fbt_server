#!/usr/bin/env python

import motorclient
from redis_cluster_proxy import Redis
from util import generate_pkey
from constant import STAR_CLASS

from tornado.concurrent import Future
import simplejson as json
from datetime import datetime
from time import time


class MessageManager(object):
    MSG_TYPE = {# for private chat
                "private": 1,
                # for other system message
                "other_system_msg": 0,
                # for QA(-1~-10)
                "QA_thumb_up": -1,
                "QA_comment": -2,
                "question_be_answered": -3,
                "question_invite": -4,
                "invited_user_answered": -5,
                "QA_reward": -6,
                "QA_comment": -7,
                # for resource and course (-21~-30)
                "resource_thumb_up": -21,
                "resource_thumb_down": -21,
                "resource_download": -22,
                # for sns(-31~-40)
                "get_a_fans": -31,
                "fake_invite_user_answer": -32,
                "transfer_coin": -33,
                }
    MESSAGES_MAX_LEN = 100
    _MSG_TIME_KEY = "user:message:"
    _MSG_CONTENT_KEY = "user:message-details:"

    def __init__(self, db=None, redis_db=None):
        if redis_db:
            self._redis_db = redis_db or Redis(Redis.FOR_DB)
        else:
            self._redis_db = Redis(Redis.FOR_DB)
        self._pipe = self._redis_db.pipeline()
        self._db = db or motorclient.fbt
        self.waiters = {}

    def get_msg(self, user):
        msg_ids = self._redis_db.zrange(self._MSG_TIME_KEY + user, 0, -1,
                                        desc=True, withscores=True)
        ret = []
        if msg_ids:
            msg_contents = self._redis_db.hmget(self._MSG_CONTENT_KEY + user,
                                                [msg_id for msg_id, ctime in msg_ids])
            ret = [json.loads(content) for content in msg_contents]
        return ret

    def get_recent_msg(self, user):
        max_recent_cnt = 200
        msg_ids = self._redis_db.zrange(self._MSG_TIME_KEY + user, 0, max_recent_cnt-1,
                                        desc=True, withscores=True)
        ret = []
        unread_cnt = 0
        if msg_ids:
            msg_contents = self._redis_db.hmget(self._MSG_CONTENT_KEY + user,
                                                [msg_id for msg_id, ctime in msg_ids])
            for content in msg_contents:
                content2 = json.loads(content)
                if content2["is_read"] == 0:
                    unread_cnt += 1
                ret.append(content2)
        return ret, unread_cnt

    def get_msg_cnt(self, user):
        return self._redis_db.hlen(self._MSG_CONTENT_KEY + user)

    def del_msg(self, user, msg_id):
        self._pipe.zrem(self._MSG_TIME_KEY + user, msg_id)
        self._pipe.hdel(self._MSG_CONTENT_KEY + user, msg_id)
        self._pipe.execute()

    def read_msg(self, user, msg_id):
        msg = self._redis_db.hget(self._MSG_CONTENT_KEY + user, msg_id)
        if msg:
            msg = json.loads(msg)
            msg["is_read"] = 1
            self._redis_db.hset(self._MSG_CONTENT_KEY + user, msg["id"], json.dumps(msg))

    def add_msg(self, user, msg):
        msg_id = msg["id"]
        self._pipe.zadd(self._MSG_TIME_KEY + user, time(), msg_id)
        self._pipe.hset(self._MSG_CONTENT_KEY + user, msg_id, json.dumps(msg))
        self._pipe.execute()
        return msg_id

    def wait_for_message(self, user):
        # Construct a Future to return to our caller.  This allows
        # wait_for_messages to be yielded from a coroutine even though
        # it is not a coroutine itself.  We will set the result of the
        # Future when results are available.
        result_future = Future()
        self.waiters[user] = result_future
        return result_future

    def cancel_wait(self, user):
        if user in self.waiters:
            # Set an empty result to unblock any coroutines waiting.
            self.waiters[user].set_result(None)
            del self.waiters[user]

    def new_system_message(self, message, user_to, msg_type=MSG_TYPE["other_system_msg"]):
        assert -40 <msg_type <= 1
        packed_msg = {"id": generate_pkey(), "type": msg_type, "content": message,
                "user_from": "system", "user_to": user_to, "is_read": 0, "more_info_of_user_from": {},
                "index_ctime": long(time() * 1000), "ctime": datetime.now().strftime('%Y-%m-%d %H:%M')}
        self.send_to_one(packed_msg, "system", user_to)
        self.add_msg(user_to, packed_msg)

    def new_private_message(self, user_from, user_to, message, db_user, db_user_to):
        packed_msg = {"id": generate_pkey(), "type": self.MSG_TYPE["private"], "content": message, "is_read": 0,
                      "user_from": user_from, "user_to": user_to, "more_info_of_user_from": {}, "more_info_of_user_to": {},
                      "index_ctime": long(time() * 1000), "ctime": datetime.now().strftime('%Y-%m-%d %H:%M')}
        for k in ("uid", "nick_name", "icon", "desc", "university", "college", "real_name"):
            packed_msg["more_info_of_user_from"][k] = db_user[k] if k in db_user else None
            packed_msg["more_info_of_user_to"][k] = db_user_to[k] if k in db_user_to else None
        packed_msg["more_info_of_user_from"]["star_info"] = [v for k, v in STAR_CLASS.iteritems() if v in db_user]
        packed_msg["more_info_of_user_to"]["star_info"] = [v for k, v in STAR_CLASS.iteritems() if v in db_user_to]
        ok = self.send_to_one(packed_msg, user_from, user_to)
        self.add_msg(user_to, packed_msg)
        # user send message add to outbox
        packed_msg2 = dict(packed_msg)
        packed_msg2["is_read"] = 1
        packed_msg2["id"] = generate_pkey()
        self.add_msg(user_from, packed_msg2)
        return ok

    def send_to_all(self, message):
        for future in self.waiters.values():
            future.set_result(message)
        self.waiters = {}

    def send_to_one(self, message, user_from, user_to):
        if user_to in self.waiters:
            self.waiters[user_to].set_result(message)
            del self.waiters[user_to]
            return True
        else:
            # user offline
            return False

    def get_users(self):
        return self.waiters.keys()

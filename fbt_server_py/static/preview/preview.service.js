/**
 * Created by dq on 2/12/16.
 */
(function () {

    angular.module('fbtApp').factory('PreviewService', PreviewService);

    PreviewService.$inject = ['$http','toaster'];


    function PreviewService($http,toaster) {


        var FetchModeEnum = {
            'NORMAL':'NORMAL',
            'REWARD':'REWARD'
        }

        var service = {

            fetchPreviewLink: undefined,
            fetchRewardPreviewLink:fetchRewardPreviewLink,
            fetchNormalPreviewLink:fetchNormalPreviewLink,
            getPreviewLink: getPreviewLink,
            showPreviewModal: showPreviewModal,
            getFileId: getFileId,
            getFileName: getFileName,

            fetchMode:FetchModeEnum.NORMAL,//是正常资源的预览还是悬赏资源的预览

        };


        return service;


        function fetchNormalPreviewLink() {

            var req = {
                url: "resource/preview",
                method: 'GET',
                params: {
                    file_id: service.fileId
                }
            };
            return $http(req).success(__SuccessCallbackFor$http__).error(__FailedCallbackFor$http__);
        }




        function fetchRewardPreviewLink(rewardId, fileHash){

            var req = {
                url: "reward/resource/preview",
                method: 'GET',
                params: {
                    reward_id:service.rewardId,
                    file_hash:service.fileHash
                }
            };

            return $http(req).success(__SuccessCallbackFor$http__).error(__FailedCallbackFor$http__);

        }

        function getPreviewLink() {
            return service.previewLink;
        }

        //function showPreviewModal(fileId, fileName) {


        function showPreviewModal() {

            if(arguments.length === 2){
                service.fileId = arguments[0];
                service.fileName = arguments[1];
                service.fetchMode = FetchModeEnum.NORMAL;
                service.fetchPreviewLink = service.fetchNormalPreviewLink;
            }
            else if(arguments.length === 3){
                service.rewardId = arguments[0];
                service.fileHash = arguments[1];
                service.fileName = arguments[2];
                service.fetchMode = FetchModeEnum.REWARD;
                service.fetchPreviewLink = service.fetchRewardPreviewLink;
            }
            $('#previewModal').modal('show');
        }

        function getFileId() {
            return service.fileId;
        }

        function getFileName() {
            return service.fileName;
        }

        function __SuccessCallbackFor$http__(data, status, headers, config){
            switch (data.err) {
                case 0:
                    service.previewLink = data.preview_link;
                    break;
                case 5:
                    service.previewLink = undefined;
                    toaster.pop('error', '暂不支持预览大于10M的文件,请直接下载查看哦!', '', false);
                    $('#previewModal').modal('hide');
                    break;
                case 6:
                    service.previewLink = undefined;
                    toaster.pop('error', '文档正在进行转换中,请稍后再试!', '', false);
                    $('#previewModal').modal('hide');
                    break;
                case 7:
                    service.previewLink = undefined;
                    toaster.pop('error', '一天最多预览20次,请直接下载查看吧...', '', false);
                    $('#previewModal').modal('hide');
                    break;
                default:
                    toaster.pop('error', '获取预览资源出错!', '', false);
                    service.previewLink = '';
                    $('#previewModal').modal('hide');
                    console.log('Get preview link get undefined state!');
            }

        }

        function __FailedCallbackFor$http__(data, status, headers, config){
            toaster.pop('error', '获取预览资源出错!', '', false);
            $('#previewModal').modal('hide');
            console.log('Get preview link error' + status);
        }

    }

})();
/**
 * Created by dq on 2/14/16.
 */

describe('PreviewService', function () {


    var $httpBackend, PreviewService;

    beforeEach(function () {
        module('fbtApp');
        inject(function (PreviewService, $httpBackend) {
            this.PreviewService = PreviewService;
            this.$httpBackend = $httpBackend;
        })
    });


    it('should return right data', function () {

        var result;
        var expected = {
            "err": 0,
            "link": "http://",
        };

        httpBackend.expectGET('/preview').respond(expected);

        var promise = PreviewService.fetchPreviewLink();

        promise.then(function (data) {
            result = data;
        });

        $httpBackend.flush();

        expect(result).toEqual(expected);

    })


})
/**
 * Created by dq on 2/12/16.
 */
(function () {

    angular.module('fbtApp').controller('PreviewController', PreviewController);

    PreviewController.$inject = ['$scope', 'PreviewService', '$routeParams'];

    function PreviewController($scope, PreviewService, $routeParams) {

        //var vm = $scope;

        var vm = this;

        $scope.isLoading = true;
        $scope.onLoad = onLoad;

        vm.activate=activate;


        vm.activate();

        function activate() {

            $('#previewModal').on('shown.bs.modal', function () {

                vm.fileId = PreviewService.getFileId();
                vm.fileName = PreviewService.getFileName();

                PreviewService.fetchPreviewLink().then(function () {
                    $scope.pdfUrl = PreviewService.getPreviewLink();

                    //test
                    //$scope.pdfUrl = 'http://7xqvry.com1.z0.glb.clouddn.com/zwIcIhN2n5w0XCDZ8Zg_FQLIycU%3D/Flb6_so0RUWTwENPtPiOb3iTNW0z?attname=%E7%AC%AC6%E7%AB%A0%20%E5%86%8D%E4%BF%9D%E9%99%A9.doc&e=1456342635&token=3rer6DB4jKt2CqSVzBjNmAC3NQe4s_LkK5PuOB4s:IP3i_fY9gL3cDulSdOg_DAZdr0Y=';
                    //$scope.pdfUrl  = "http://7xqvry.com1.z0.glb.clouddn.com/fwPWagkAHQ2vmAEknblUgAqlEJk%3D/FgYpTf9gUpQCu7BgmfxSB6me6zlQ?attname=2011.01%E6%9C%9F%E6%9C%AB%E8%AF%95%E9%A2%98%E7%AD%94%E6%A1%88.ppt&e=1456344684&token=3rer6DB4jKt2CqSVzBjNmAC3NQe4s_LkK5PuOB4s:ussp2oGnq0V6KFelpMprr41P-kQ=";
                    //$scope.$digest();
                });
            });

            $('#previewModal').on('hide.bs.modal', function () {
                $scope.pdfUrl  = '';
                $scope.isLoading= true;
            });

        }


        function onLoad () {
            // do something when pdf is fully loaded
            $scope.isLoading = false;
        }



    }

})()
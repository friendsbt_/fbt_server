/**
 * Created by dq on 3/13/16.
 */
(function () {
    angular.module('mApp').factory('findService',findService);

    findService.$inject = ['$state','$http'];

    function findService($state,$http){
        var service = {
            getQuestions:getQuestions,
            fetchQuestions:fetchQuestions,



        };

        return service;

        function fetchQuestions(qClass,pageNum){

            var req = {
                url:'/question/list',
                method:'GET',
                params:{

                    by_all:1,//全部高校
                    detail:0, //不获取最新最热
                    class2: qClass,
                    page: pageNum,

                },


            };

            return $http(req).success(success).error(error);

            function success(data,status,headers,config){
                switch (data.err) {
                    case 0:
                        service.questions = data.cared_question.question_list;
                        break;j
                    case 1:
                        service.questions = [];

                }

            };
            function  error(data,status,headers,config){
                service.questions = [];

            }


        }

        function  getQuestions(){
            return service.questions;
        }

    }

})();

/**
 * Created by dq on 3/12/16.
 */
(function () {
    angular.module('mApp').controller('FindController', FindController);

    FindController.$inject = ['findService', '$state', '$stateParams','$document'];

    function FindController(findService, $state, $stateParams,$document) {

        var vm = this;
        vm.active = active;
        vm.fetchQuestions = fetchQuestions;
        vm.fetchMoreQuestions = fetchMoreQuestions;

        vm.categories = [{
            name: '校园',
            state: 'find.school'
        }, {name: '考研'}, {name: '就业'}, {name: '留学'}, {name: '考证'}, {name: '实习'}];


        function active() {

            var qClass = $stateParams['qClass'];
            if (!qClass) {
                $state.go('find', {qClass: '校园'});
            }
            vm.qClass = qClass;
            vm.pageNum = 1;//初始化为1,如果pageNum < 0 ,则不显示 加载更多 按钮
            vm.isLoading = true;
            vm.fetchQuestions();

            $document[0].title= qClass + ' - 校园星空';


        }

        vm.active();

        function fetchQuestions() {
            findService.fetchQuestions(vm.qClass, vm.pageNum).then(function () {

                vm.questions = findService.getQuestions();
                vm.pageNum += 1;
                vm.isLoading = false;

                if (!vm.questions.length) {
                    vm.pageNum = -1;
                }


            });

        };


        function fetchMoreQuestions() {

            vm.isLoading = true;
            findService.fetchQuestions(vm.qClass, vm.pageNum).then(function () {

                vm.isLoading = false;
                var newQeustions = findService.getQuestions();
                if (newQeustions.length) {

                    vm.questions = vm.questions.concat(newQeustions);
                    vm.pageNum += 1;

                } else {
                    vm.pageNum = -1;
                }

            });
        };

    };

})();

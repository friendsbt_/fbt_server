/**
 * Created by dq on 3/13/16.
 */
(function () {
    angular.module('mApp').factory('questionService', questionService);

    questionService.$inject = ['$http'];


    function questionService($http) {

        var service = {
            fetchQuestionDetail: fetchQuestionDetail,
            getQuestionInfo: getQuestionInfo,
            getAnswers: getAnswers,
        };

        return service;


        function fetchQuestionDetail(questionId) {

            var req = {
                method: 'GET',
                url: '/question/detail',
                params: {
                    detail: 0,//不获取最新最热
                    question_id: questionId
                }
            };

            return $http(req).success(success).error(error);

            function success(data, status, headers, config) {
                switch (data.err) {
                    case 0:
                        service.question = data.question;
                        service.answers = data.answers_list;
                        break;

                }

            }

            function error(data, status, headers, config) {

            }

        };

        function getQuestionInfo() {
            return service.question;
        }

        function getAnswers() {
            return service.answers;
        }

    }

})();

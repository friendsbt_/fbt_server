/**
 * Created by dq on 3/13/16.
 */
(function () {
    angular.module('mApp').controller('QuestionController', QuestionController);

    QuestionController.$inject = ['questionService', '$stateParams','$document'];

    function QuestionController(questionService, $stateParams,$document) {

        var vm = this;

        vm.litmitNum = 3;//折叠一些不被显示出来

        vm.active = active;
        vm. fetchQuestionDetail= fetchQuestionDetail;
        vm.showMore = showMore;  // 显示折叠的答案


        vm.active();

        function active() {
            var questionId = $stateParams['questionId'];
            vm.fetchQuestionDetail(questionId);
        }


        function fetchQuestionDetail(questionId) {
            questionService.fetchQuestionDetail(questionId).then(function () {


                vm.question = questionService.getQuestionInfo();
                vm.answers = questionService.getAnswers();

                $document[0].title = vm.question.title + ' - 校园星空';
            });
        };


        function showMore(){
            var num = vm.litmitNum   + 3;
            vm.litmitNum = num < vm.answers.length ? num : vm.answers.length;

        }


    };


})();
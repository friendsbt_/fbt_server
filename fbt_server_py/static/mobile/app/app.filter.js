/**
 * Created by dq on 3/13/16.
 */
(function () {

    angular.module('mApp').filter('toTrustHtml',toTrustHtml);




    toTrustHtml.$inject = ['$sce'];
    function toTrustHtml($sce) {
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    };


})();

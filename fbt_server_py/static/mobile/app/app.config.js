(function () {

    angular.module('mApp').config(routeConfig);

    routeConfig.$inject = ['$stateProvider','$urlRouterProvider'];


    function routeConfig($stateProvider,$urlRouterProvider) {

        $urlRouterProvider.otherwise("/qahome/校园");

        $stateProvider
            .state('qahome', {
                url: '/qahome/:qClass',
                templateUrl:'find/find.tpl.html',
                //resolve:{
                //    fetchQuestions:function(findService,$stateParmas){
                //        return findService.fetchQuestions();
                //    }
                //}
            })
            .state('qadetail', {
                url: '/qadetail/:questionId',
                templateUrl: 'question/question.tpl.html'

            });

    }


})();
var gulp = require('gulp');
var uglify = require('gulp-uglify');//压缩js
var rev = require('gulp-rev'); //对文件名加MD5后缀
var revReplace = require('gulp-rev-replace');//替换加了md5的引用
var useref = require('gulp-useref');  //对标记内的引用进行处理
var gulpif = require('gulp-if');

var cssnano = require('gulp-cssnano');//压缩css
var htmlMin = require('gulp-htmlmin');
var inject = require('gulp-inject-string');//插入

var templateChange = require('gulp-angular-templatecache'); //把所有的templateUrl都缓存起来.

//var version = gulp.env.version || (new Date()).valueOf();

//var publicDir = 'public/' + version + '/';
var staticDomain = '//static.friendsbt.com/static/mobile/'

//publicDir = 'app/';

var htmlMinOpts = {
    removeComments: true,
    collapseWhitespace: true,
    minifyCSS: true,
};

gulp.task('useref', ['replaceTemplate'], function () {
    var assets = useref.assets();
    return gulp.src('app/index.html')
        .pipe(assets)
        .pipe(gulpif('*.js', uglify({
            mangle: false,
            preserveComments: false
        })))
        .pipe(gulpif('*.css', cssnano(
            {
                zindex: false,

            }
        )))
        .pipe(rev())
        .pipe(gulp.dest('./'))
        .pipe(assets.restore())
        .pipe(useref())
        .pipe(revReplace(
            {
                prefix: staticDomain
            }
        ))
        .pipe(gulpif('*.html', htmlMin(htmlMinOpts)))
        .pipe(gulp.dest('app'));
});

gulp.task('htmlMin', function () {
    return gulp.src('public/**/*.html')
        .pipe(htmlMin(htmlMinOpts))
        .pipe(gulp.dest('app/'));
});


gulp.task('template', function () {
    var opts = {
        //root: '/statics/',
        module: 'mApp'
    };
    return gulp.src('app/**/*.html')
        .pipe(gulpif('*.html', htmlMin(htmlMinOpts)))
        .pipe(templateChange(opts))
        .pipe(gulp.dest('js'));
});

//插入template.js
gulp.task('replaceTemplate', ['template'], function () {
    return gulp.src('app/index.html')
        .pipe(inject.replace('<!-- templateCache -->', '<script type="text/javascript" src="../' + 'js/templates.js' + '"></script>'))
        .pipe(gulp.dest('app'))
});


gulp.task('deploy', ['template', 'replaceTemplate', 'useref']);

gulp.task('default', ['deploy']);

/*********************************************************************************
 *     File Name           :     imageUploadToQiniu.plugin.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-10-09 18:54]
 *     Last Modified       :     [2015-10-09 19:22]
 *     Description         :
 **********************************************************************************/


tinymce.PluginManager.add('imageuploadtoQiniu', function(editor) {

  function showDialog() {

    editor.windowManager.open({

      title: 'Insert/edit image',
      body: [{
        type: 'textbox',
        name: 'url',
        label: 'Image URL'
      }, {
        type: 'button',
        name: 'image-pick',
        id: 'image-pick'
      }]
    });

  }

  editor.addButton('image', {
    icon: 'image',
    tooltip: 'Insert/edit image',
    onclick: showDialot(),
  });

  editor.addMenuItem('image', {
    icon: 'image',
    text: 'Insert/edit image',
    onclick: showDialot(),
    context: 'insert',
    prependToContext: true
  });
});

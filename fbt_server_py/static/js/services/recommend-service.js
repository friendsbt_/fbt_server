app.service('recommendSrv', ['$rootScope', '$http', 'downloadSrv', 'loading', function($rootScope, $http, downloadSrv, loading) {
  var recommend_resource = [];
  var more_recommend_resource = [];
  var page = {
    current_page: 0,
    total_page: 0,
    tag: ''
  };

  this.get_recommend_resource = function() {
    return recommend_resource;
  };

  this.get_more_recommend_resource = function() {
    return more_recommend_resource;
  };

  this.fetch_recommend_resource = function() {
    if (recommend_resource.length !== 0)
      return;

    loading.show();
    $http({
        url: '/resource/recommend',
        method: 'GET'
      })
      .success(function(response) {
        loading.hide();
        if (0 == response.err) {
          recommend_resource.length = 0;
          angular.forEach(response.recommend_resource, function(v, i) {
            recommend_resource.push(v);
          });
        }
      })
      .error(function(data, status, headers, config) {
        // 失败处理
        loading.hide();
        alert('页面加载失败，请重试！');
      });
  };

  this.fetch_more_recommend_resource = function(tag, page_num) {
    if (page.tage == tag && page_num == page.current_page)
      return;
    loading.show();
    $http({
        url: '/resource/recommend/more',
        method: 'GET',
        params: {
          tag: tag,
          page: page_num
        }
      })
      .success(function(response) {
        loading.hide();
        if (0 == response.err) {
          page.tag = tag;
          page.current_page = response.current_page;
          page.total_page = response.total_page;

          more_recommend_resource.length = 0;
          angular.forEach(response.resource_list, function(v, i) {
            more_recommend_resource.push(v);
          });
        }
      })
      .error(function(data, status, headers, config) {
        // 失败处理
        loading.hide();
        alert('页面加载失败，请重试！');
      });
  };

  

  this.download = function(file_id,url) {
    downloadSrv.download(file_id,url);
  }


  this.preview = function(res){
     downloadSrv.preview(res);
  }

}]); //推荐服务
app.service('universityCollegeSrv', function($http) {
  var universities = [];
  var colleges = [];
  var prov_list = [
    '北京',
    '上海',
    '黑龙江',
    '吉林',
    '辽宁',
    '天津',
    '安徽',
    '江苏',
    '浙江',
    '陕西',
    '湖北',
    '广东',
    '湖南',
    '甘肃',
    '四川',
    '山东',
    '福建',
    '河南',
    '重庆',
    '云南',
    '河北',
    '江西',
    '山西',
    '贵州',
    '广西',
    '内蒙古',
    '宁夏',
    '青海',
    '新疆',
    '海南',
    '西藏',
    '香港',
    '澳门',
    '台湾',
  ];

  this.getUniversities = function() {
    return universities;
  };

  this.getColleges = function() {
    return colleges;
  };

  this.getProvList = function() {
    return prov_list;
  };

  this.fetchUniversities = function(province) {
    // fetch from server
    $http({
        url: 'v2/get_university',
        method: 'GET',
        params: {
          province: province
        }
      })
      .success(function(response) {
        if (0 === response.err) {
          universities.length = 0;
          angular.forEach(response.list, function(v, i) {
            universities.push(v);
          });
        }
      })
      .error(function(data, status, headers, config) {
        // 失败处理
        alert('页面加载失败，请重试！');
      });
  };

  this.fetchColleges = function(university) {
    // fetch from server
    $http({
        url: 'v2/get_university',
        method: 'GET',
        params: {
          university: university
        }
      })
      .success(function(response) {
        if (0 === response.err) {
          colleges.length = 0;
          angular.forEach(response.list, function(v, i) {
            colleges.push(v);
          });
        }
      })
      .error(function(data, status, headers, config) {
        // 失败处理
        alert('页面加载失败，请重试！');
      });
  };
});

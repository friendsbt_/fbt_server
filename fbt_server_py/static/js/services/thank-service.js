/*********************************************************************************
 *     File Name           :     thank-service.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-11-08 11:16]
 *     Last Modified       :     [2015-12-09 22:22]
 *     Description         :     奖励问答的service
 **********************************************************************************/

app.service('ThankSrv', ['$http', 'toaster', 'User', '$rootScope',
  function($http, toaster, User, $rootScope) {
    var self = this;
    self.total_coins = $rootScope.total_coins;
    self.thankQuestion = {}, //由于封装了，这两个需要在调出modal的时候赋值
      self.thankAnswer = {},

      self.answerThanks = function(fb) { //奖励某个答案

        if (parseInt(fb) > parseInt(self.total_coins)) {
          toaster.pop('error', '', '亲，您的FB不够支付哦！', false);
          return;
        }

        var req = {
          url: '/answer/thanks',
          method: 'GET',
          params: {
            id: self.thankAnswer.id,
            how_much: fb
          }
        };
        $http(req).success(function(data, status, headers, config) {
            var r = angular.fromJson(data);
            if (r.err === 0) {
              toaster.pop('success', '奖励成功！', '', true);
              self.thankAnswer.thanks_coin += parseInt(config.params.how_much);
              //User.user_info.total_coins = parseInt(User.user_info.total_coins) - parseInt(config.params.how_much);
              //window.localStorage.total_coins = User.user_info.total_coins;
            } else if (r.err === 1) {
              toaster.pop('error', '奖励失败!', '参数错误！', false);
            } else if (r.err === 2) {
              toaster.pop('error', '奖励失败!', '没有授权,不能对此问题进行奖励，请检查是否登录！', false);
            } else if (r.err === 3) {
              toaster.pop('error', '奖励失败!', '亲，您的FB不够了，赶紧去回答问题或者上传资源赚取更多的FB吧！', false);
              //toaster.pop('error', '奖励失败!', 'FB不足，支付不了啊！', false);
            }
          })
          .error(function(data, status, headers, config) {
            toaster.pop('error', '奖励失败！', '请求过程错误！', false);
          });

      };

    self.questionThanks = function(fb) { //奖励某个问题
      if (parseInt(fb) > parseInt(self.total_coins)) {
        toaster.pop('error', '', '亲，您的FB不够支付哦！', false);
        return;
      }
      var req = {
        url: '/question/thanks',
        method: 'GET',
        params: {
          id: self.thankQuestion.id,
          how_much: fb
        }
      };
      $http(req).success(function(data, status, headers, config) {
          var r = angular.fromJson(data);
          if (r.err === 0) {
            toaster.pop('success', '奖励成功！', '', true);
            self.thankQuestion.thanks_coin += parseInt(config.params.how_much);
            //User.user_info.total_coins = parseInt(User.user_info.total_coins) - parseInt(config.params.how_much);
            //window.localStorage.total_coins = User.user_info.total_coins;
          } else if (r.err === 1) {
            toaster.pop('error', '奖励失败!', '参数错误！', false);
          } else if (r.err === 2) {
            toaster.pop('error', '奖励失败!', '没有授权,不能对此问题进行奖励，请检查是否登录！', false);
          } else if (r.err === 3) {
            toaster.pop('error', '奖励失败!', '亲，您的FB不够了，赶紧去回答问题或者上传资源赚取更多的FB吧！', false);
          }
        })
        .error(function(data, status, headers, config) {
          toaster.pop('error', '奖励失败！', '请求过程错误！', false);
        });
    };

    self.showQuestionThanksModal = function(question) {
      self.total_coins = $rootScope.total_coins;
      self.thankQuestion = question;
      angular.element('#thankQuestionModal').modal('show');
    };

    self.showAnswerThanksModal = function(answer) {
      self.total_coins = $rootScope.total_coins;
      self.thankAnswer = answer;
      angular.element('#thankAnswerModal').modal('show');
    };

  }
]);

/*********************************************************************************
 *     File Name           :     hot-latest-question-service.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-11-20 20:03]
 *     Last Modified       :     [2015-11-20 20:24]
 *     Description         :     最新和最热的service
 **********************************************************************************/


app.service('HotLatestQuestionSrv', ['$http',
  function($http) {

      this.GotHotLatestQuestions = 1; //第一次请求会把最新和最热的拿过来，第二次置为0后不再请求最新最热
      //this.alreadyInit = false;

    this.hotQuestions = [];
    this.latestQuestions = [];
    this.showedHotQuestions = [];
    this.showedLatestQuestions = [];

    this.changeHot = function(changeHotClickNum) {

      var length = this.hotQuestions.question_list.length;
      var start = (changeHotClickNum * 5) % length;
      var end = (start + 5) > length ? ((start + 5) % length) : (start + 5);

      if (start >= end) {

        this.showedHotQuestions = this.hotQuestions.question_list.slice(start, length);
        this.showedHotQuestions.concat(this.hotQuestions.question_list.slice(0, end + 1));
      } else {
        this.showedHotQuestions = this.hotQuestions.question_list.slice(start, end + 1);
      }
      //this.changeHotClickNum += 1;

    };

    this.changeLatest = function(changeLatestClickNum) {

      var length = this.latestQuestions.question_list.length;
      var start = (changeLatestClickNum * 5) % length;
      var end = (start + 5) > length ? ((start + 5) % length) : (start + 5);

      if (start >= end) { //又得从头开始
        this.showedLatestQuestions = this.latestQuestions.question_list.slice(start, length);
        this.showedLatestQuestions.concat(this.latestQuestions.question_list.slice(0, end + 1));
      } else {
        this.showedLatestQuestions = this.latestQuestions.question_list.slice(start, end + 1);
      }
      //this.changeLatestClickNum += 1;
    };



  }
]);

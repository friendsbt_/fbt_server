app.service('downloadSrv', ['$rootScope', '$http', '$window', 'toaster', 'User',
  function($rootScope, $http, $window, toaster, User) {
    var self = this;
    this.webID = 1989;
    this.office365URL = "http://officeweb365.com/o/?i=" + this.webID + "&furl=";

    this.MSofficeWebViewURL = "https://view.officeapps.live.com/op/view.aspx?src=";
    this.default_download_qiniu_callBackFunc = function(data, status, headers, config) {
      switch (status) {
        case 401:
          { //认证失败
            toaster.pop('error', '系统提示', '下载凭证验证失败,请重试!', false);
            break;
          }
        case 404:
          {
            //请求资源不存在
            toaster.pop('error', '系统提示', '请求的资源不存在!', false);
            break;
          }
        case 612:
          {
            //指定资源不存在或已经被删除
            toaster.pop('error', '系统提示', '指定资源不存在或已经被删除!', false);
            break;
          }
        default:
          {
            toaster.pop('error', '系统提示', '数据云服务器异常!', false);
          }

      }

    };

    this.default_getdownloadToken_callBackFunc = function(data, status, headers, config) {
      switch (status) {
        case 200:
          {
            switch (data.err) {
              case 1:
                {
                  //未授权
                  // toaster.pop('error', '系统提示', '请登录后再下载!', false);
                  angular.element('#login-modal').modal('show');
                  break;
                }
              case 2:
                {
                  //参数错误
                  toaster.pop('error', '系统提示', '请求参数不正确!', false);
                  break;
                }
              case 3:
                {
                  toaster.pop('error', '系统提示', '您今天的下载已经达到5个限额，请明天再来下载吧，谢谢您的支持~', false);
                  break;
                }
              case 4:
                {
                  toaster.pop('error', '系统提示', '您当前F币不足，请分享资源赚取F币！', false);
                  break;
                }
              default:
                {
                  toaster.pop('error', '系统提示', '未知错误！', false);
                }

            }
            break;
          }
        case 408:
          {
            //请求超时
            toaster.pop('error', '系统提示', '请求超时!', false);
            break;
          }
        case 500:
          {
            //服务器内部错误
            toaster.pop('error', '系统提示', '服务器内部出错,请稍候再试!', false);
            break;
          }
        default:
          {
            toaster.pop('warning', '系统提示', '未知处理处理状态!', false);
          }
      }
    };

    //callback1(data, status, headers, config) 是指请求业务服务器出错
    //callback2(data, status, headers, config) 是指从七牛服务器下载出错
    //两个函数都有一个默认,可以为空,可以自行指定,4个参数是$http处理请求后的参数
    this.__download = function(file_id, url, callback1, callback2) {
      if (callback1 === undefined) {
        callback1 = this.default_getdownloadToken_callBackFunc;
      }
      if (callback2 === undefined) {
        callback2 = this.default_download_qiniu_callBackFunc;
      }
      var req = {
        method: 'POST',
        url: "/download",
        timeout: 10000,

        headers: {
          "X-CSRFToken": getCookie("_xsrf"),
        },
        data: {
          download_link: url,
          file_id: file_id,
        }
      };
      $http(req).success(function(data, status, headers, config) {
          //获取加token的链接请求成功
          var result = angular.fromJson(data);
          if (data.err === 0) {
            //正确获取到加token的链接
            // $http.get(data.download_link).success(function() {
            // 		//从七牛下载成功,不做处理了
            // 	})
            // 	.error(function(data, status, headers, config) {
            // 		//从七牛下载失败
            // 		self.default_download_qiniu_callBackFunc(data, status, headers, config);
            // 	});
            toaster.pop('success', '下载提示', '下载成功，稍后将扣除您1F币！', true);
            $window.open(data.download_link, "_blank"); //很奇怪哈,这里取到的this竟然是window对象 
          } else {
            //获取加token的链接请求成功,但参数不正确或未授权
            self.default_getdownloadToken_callBackFunc(data, status, headers, config);
          }
        })
        .error(function(data, status, headers, config) {
          //获取加token的请求失败
          self.default_getdownloadToken_callBackFunc(data, status, headers, config);
        });

    };
    this.__download_minus_fb = function(file_id, url, callback1, callback2) {
      if (callback1 === undefined) {
        callback1 = this.default_getdownloadToken_callBackFunc;
      }
      if (callback2 === undefined) {
        callback2 = this.default_download_qiniu_callBackFunc;
      }
      var req = {
        method: 'POST',
        url: "resource/download",
        timeout: 10000,

        headers: {
          "X-CSRFToken": getCookie("_xsrf"),
        },
        data: {
          download_link: url,
          file_id: file_id,
        }
      };
      $http(req).success(function(data, status, headers, config) {
          //获取加token的链接请求成功
          var result = angular.fromJson(data);
          if (data.err === 0) {
            toaster.pop('success', '下载提示', '下载成功，稍后将扣除您1F币！', true);
            $window.open(data.download_link, "_blank"); //很奇怪哈,这里取到的this竟然是window对象 
          } else {
            //获取加token的链接请求成功,但参数不正确或未授权
            self.default_getdownloadToken_callBackFunc(data, status, headers, config);
          }
        })
        .error(function(data, status, headers, config) {
          //获取加token的请求失败
          self.default_getdownloadToken_callBackFunc(data, status, headers, config);
        });

    };


    this.__preview__OFFICE365 = function(file_id, url, previewCallBack1, previewCallBack2) {
      if (previewCallBack1 === undefined) {
        previewCallBack1 = this.default_getdownloadToken_callBackFunc;
      }
      //TODO 有待完善
      if (previewCallBack2 === undefined) {
        previewCallBack2 = this.default_download_qiniu_callBackFunc;
      }
      var req = {
        method: 'POST',
        url: "/download",
        timeout: 10000,

        headers: {
          "X-CSRFToken": getCookie("_xsrf"),
        },
        data: {
          download_link: url,
          file_id: file_id,
        }
      };
      $http(req).success(function(data, status, headers, config) {
          //获取加token的链接请求成功
          var result = angular.fromJson(data);
          if (data.err === 0) {

            //对链接进行处理 ，利用office 365实现在线预览
            var previewURL = self.office365URL + data.download_link;
            $window.open(previewURL, "_blank");


          } else {
            //获取加token的链接请求成功,但参数不正确或未授权
            self.default_getdownloadToken_callBackFunc(data, status, headers, config);
          }
        })
        .error(function(data, status, headers, config) {
          //获取加token的请求失败
          self.default_getdownloadToken_callBackFunc(data, status, headers, config);
        });
    };


    this.__preview__MS = function(file_id, url, previewCallBack1, previewCallBack2) {
      if (previewCallBack1 === undefined) {
        previewCallBack1 = this.default_getdownloadToken_callBackFunc;
      }
      //TODO 有待完善
      if (previewCallBack2 === undefined) {
        previewCallBack2 = this.default_download_qiniu_callBackFunc;
      }
      var req = {
        method: 'POST',
        url: "resource/preview",
        timeout: 10000,

        headers: {
          "X-CSRFToken": getCookie("_xsrf"),
        },
        data: {
          download_link: url,
          file_id: file_id,
        }
      };
      $http(req).success(function(data, status, headers, config) {
          //获取加token的链接请求成功
          var result = angular.fromJson(data);
          if (data.err === 0) {
            //对链接进行处理 ，利用office 365实现在线预览
            var previewURL = self.MSofficeWebViewURL + encodeURIComponent(data.download_link);
            $window.open(previewURL, "_blank");

          } else {
            //获取加token的链接请求成功,但参数不正确或未授权
            self.default_getdownloadToken_callBackFunc(data, status, headers, config);
          }
        })
        .error(function(data, status, headers, config) {
          //获取加token的请求失败
          self.default_getdownloadToken_callBackFunc(data, status, headers, config);
        });

    };

    this.__preview__PDF = function(file_id, url, previewCallBack1, previewCallBack2) {
      if (previewCallBack1 === undefined) {
        previewCallBack1 = this.default_getdownloadToken_callBackFunc;
      }
      //TODO 有待完善
      if (previewCallBack2 === undefined) {
        previewCallBack2 = this.default_download_qiniu_callBackFunc;
      }
      var link = url.substring(0, url.indexOf('?download/'));
      var req = {
        method: 'POST',
        url: "resource/preview",
        timeout: 10000,

        headers: {
          "X-CSRFToken": getCookie("_xsrf"),
        },
        data: {
          download_link: link,
          file_id: file_id,
        }
      };
      $http(req).success(function(data, status, headers, config) {
          //获取加token的链接请求成功
          var result = angular.fromJson(data);
          if (data.err === 0) {
            $window.open("//test.friendsbt.com/statics/pdf-preview.html?pdfUrl=" + data.download_link);

          } else {
            //获取加token的链接请求成功,但参数不正确或未授权
            self.default_getdownloadToken_callBackFunc(data, status, headers, config);
          }
        })
        .error(function(data, status, headers, config) {
          //获取加token的请求失败
          self.default_getdownloadToken_callBackFunc(data, status, headers, config);
        });
    };

    this.__preview__IMG = function(file_id, url, previewCallBack1, previewCallBack2) {
      if (previewCallBack1 === undefined) {
        previewCallBack1 = this.default_getdownloadToken_callBackFunc;
      }
      //TODO 有待完善
      if (previewCallBack2 === undefined) {
        previewCallBack2 = this.default_download_qiniu_callBackFunc;
      }
      var link = url.substring(0, url.indexOf('?download/'));
      var req = {
        method: 'POST',
        url: "resource/preview",
        timeout: 10000,

        headers: {
          "X-CSRFToken": getCookie("_xsrf"),
        },
        data: {
          download_link: link,
          file_id: file_id,
        }
      };
      $http(req).success(function(data, status, headers, config) {
          //获取加token的链接请求成功
          var result = angular.fromJson(data);
          if (data.err === 0) {
            //$window.open(data.download_link, "_blank");
            $window.open("//test.friendsbt.com/statics/img-preview.html?imgUrl=" + data.download_link);
          } else {
            //获取加token的链接请求成功,但参数不正确或未授权
            self.default_getdownloadToken_callBackFunc(data, status, headers, config);
          }
        })
        .error(function(data, status, headers, config) {
          //获取加token的请求失败
          self.default_getdownloadToken_callBackFunc(data, status, headers, config);
        });
    };

    this.download = function(file_id, url, callback1, callback2) {
      User.login(function() {
        //self.__download(file_id, url, callback1, callback2);
        self.__download_minus_fb(file_id, url, callback1, callback2);
      });
    };

    this.preview = function(res) {
      User.login(function() {
        if (getFileExtension(res.filename).toLocaleLowerCase() == ".pdf") {
          //pdf自定义预览
          self.__preview__PDF(res.file_id, res.download_link);
        } else if (getFileExtension(res.filename).toLocaleLowerCase() == ".jpg" || getFileExtension(res.filename).toLocaleLowerCase() == ".gif" || getFileExtension(res.filename).toLocaleLowerCase() == ".png") {
          self.__preview__IMG(res.file_id, res.download_link);
        } else {
          self.__preview__MS(res.file_id, res.download_link);
        }
      });
    };

  }
]);

/*********************************************************************************
 *     File Name           :     invite-service.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-11-01 16:39]
 *     Last Modified       :     [2016-01-13 20:44]
 *     Description         :     邀请回答的servcie
 **********************************************************************************/

app.service('InviteReplySrv', ['$http', 'toaster', 'QuestionAnswerSrv',
  function($http, toaster, QuestionAnswerSrv) {
    var self = this;
    //this.viewVar = AskQuestionSrv.viewVar;//控制页面显示的参数

    this.alreadyInit = false;
    this.invitedUsers = [];

    this.myFollowing = []; //我关注的人，我靠，这个怎么翻译
    this.recommendedUsers = {}; //推荐的用户


    //this.myFollowing = [{
    //  thumb_num: 3,
    //  followers_num: 1,
    //  tags: [{
    //    tagClass: "校园:计算机",
    //    num: 4
    //  }],
    //  nick_name: "ddddd",
    //  gender: "男",
    //  university: "北京大学",
    //  real_name: "ddddd",
    //  answers_num: 4,
    //  college: "医学网络教育学院",
    //  user: "234@234.com",
    //  uid: 14491415038122,
    //  icon: "http://7xjkhy.dl1.z0.glb.clouddn.com/FvkQlDjyVP_jEs9-PM2QCHbTicoL",
    //  desc: "",
    //  star_info: [
    //    "is_internship_star"
    //  ]
    //}];



    //主要是把勋章信息标准化
    this.normalizeUsersStarInfo = function(users) {
      angular.forEach(users, function(value, key) {
        value.honors = QuestionAnswerSrv.normalizePublisherStarInfo(value.star_info);
      });
    };


    this.fetchMyFollowers = function() {
      var req = {
        url: '/user/following',
        method: 'GET',
        headers: {
          "X-CSRFToken": getCookie("_xsrf")
        }

      };

      return $http(req).success(function(data, status, headers, config) {

          var r = angular.fromJson(data);
          if (r.err === 0) {
            self.myFollowing.length = 0;
            angular.forEach(r.following_users, function(value, key) {
              self.sortUserTags(value);
              self.myFollowing.push(value);
            });
            self.normalizeUsersStarInfo(self.myFollowing);
          } else if (r.err === 1) {
            toaster.pop('error', '获取邀请人列表', '没有授权操作，请检查是否登录', false);
          }
        })
        .error(function(data, status, headers, config) {
          toaster.pop('error', '获取邀请人列表', '请求过程错误', false);
        });
    };

    //对我关注的人的tags排序,换成[{tagClass: "校园:计算机",num: 4},{}]
    this.sortUserTags = function(user) {
      var tags = [];
      //users.tags 的原始结构是{'校园:aaaaa': 4,'校园:bbbb': 3}
      angular.forEach(user.tags, function(value, key) {
        tags.push({
          tagClass: key,
          num: value
        });

      });
      tags.sort(function(a, b) {
        return b.num - a.num;
      });

      user.tags = [];
      angular.forEach(tags, function(value, key) {
        user.tags.push(value);
      });
      //angular.copy(tags, users.tags);
      //users.tags = tags;

    };

    this.autoRecommendUsers = function(tagClass, tags) {

      var req = {
        url: '/question/user/recommend',
        method: 'POST',
        data: {
          tag_class: tagClass,
          tags: tags,
        },
        headers: {
          "X-CSRFToken": getCookie("_xsrf")
        }

      };

      $http(req).success(function(data, status, headers, config) {

          var r = angular.fromJson(data);
          switch (r.err) {
            case 0:
              //r.users = {
              //  "abc": [{
              //    "followers_num": 3,
              //    "icon": "http://7xjkhy.dl1.z0.glb.clouddn.com/FtH5V0_zsohv_15P8CVqaDNaXZOL",
              //    "thanks_coin": 27,
              //    "college": "钢琴系",
              //    "real_name": "BoneLee",
              //    "uid": 14470710482766,
              //    "state": "就业",
              //    "thumb_num": 14,
              //    "tags": {
              //      "校园:心理学": 2,
              //      "校园:大学生就业": 1,
              //      "考证:意义": 2,
              //      "校园:工作": 1,
              //      "就业:就业": 1,
              //      "考研:企业管理": 1,
              //      "校园:自杀": 1,
              //      "就业:留学": 1,
              //      "就业:海归": 1,
              //      "校园:大学生": 1,
              //      "考研:经验": 1,
              //      "就业:优势": 1,
              //      "考证:大学生村官": 2,
              //      "考证:国考": 1,
              //      "就业:飞行器动力工程": 1,
              //      "校园:经济学": 1,
              //      "考证:公务员考试": 1,
              //      "考研:中大": 1,
              //      "考研:考研": 1,
              //      "校园:职业规划": 1
              //    },
              //    "answers_num": 9,
              //    "user": "bone@test.com",
              //    "wanted": {
              //      "thumb_num_of_tag": 1,
              //      "tag": "校园:学习",
              //      "posted_num": 0
              //    },
              //    "desc": "",
              //    "nick_name": "Bone",
              //    "gender": "男",
              //    "university": "中央音乐学院",
              //    "state_desc": "阿里巴巴",
              //    star_info: [
              //      "is_internship_star",
              //      "is_work_star"
              //    ],
              //    "coins_by_study": 15,
              //    "honor": [
              //      ["2013", "全国", "ACM一等奖"]
              //    ]
              //  }],
              //  "baccd": [{
              //    "followers_num": 3,
              //    "icon": "http://7xjkhy.dl1.z0.glb.clouddn.com/FtH5V0_zsohv_15P8CVqaDNaXZOL",
              //    "thanks_coin": 27,
              //    "college": "钢琴系",
              //    "real_name": "BoneLee",
              //    "uid": 14470710482766,
              //    "state": "就业",
              //    "thumb_num": 14,
              //    "tags": {
              //      "校园:心理学": 2,
              //      "校园:大学生就业": 1,
              //      "考证:意义": 2,
              //      "校园:工作": 1,
              //      "就业:就业": 1,
              //      "考研:企业管理": 1,
              //      "校园:自杀": 1,
              //      "就业:留学": 1,
              //      "就业:海归": 1,
              //      "校园:大学生": 1,
              //      "考研:经验": 1,
              //      "就业:优势": 1,
              //      "考证:大学生村官": 2,
              //      "考证:国考": 1,
              //      "就业:飞行器动力工程": 1,
              //      "校园:经济学": 1,
              //      "考证:公务员考试": 1,
              //      "考研:中大": 1,
              //      "考研:考研": 1,
              //      "校园:职业规划": 1
              //    },
              //    star_info: [
              //      "is_internship_star",
              //      "is_work_star"
              //    ],
              //    "answers_num": 9,
              //    "user": "bone@test.com",
              //    "wanted": {
              //      "thumb_num_of_tag": 1,
              //      "tag": "校园:计算机",
              //      "posted_num": 0
              //    },
              //    "desc": "",
              //    "nick_name": "Bone",
              //    "gender": "男",
              //    "university": "中央音乐学院",
              //    "state_desc": "阿里巴巴",
              //    "coins_by_study": 15,
              //    "honor": [
              //      ["2013", "全国", "ACM一等奖"]
              //    ]
              //  }]
              //};

              angular.forEach(r.users, function(value, key) {
                self.normalizeUsersStarInfo(value);
                self.recommendedUsers[key] = value;
              });
              break;
            case 1:
              toaster.pop('error', '自动提取标签失败！', '请检查是否已经登录！', false);
              break;
            case 2:
              toaster.pop('error', '自动提取标签失败！', '参数错误！', false);
              break;
          }

        })
        .error(function(data, status, headers, config) {

        });

    };

    this.removeInvitedUser = function(index) {
      angular.forEach(self.recommendedUsers, function(value, key) {
        angular.forEach(value, function(value2, key2) {
          if (value2.uid === self.invitedUsers[index].uid) {
            value2.checked = false;
          }
        });
      });
      self.invitedUsers[index].checked = false;
      self.invitedUsers.splice(index, 1);
    };



  }
]);

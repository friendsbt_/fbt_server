/*********************************************************************************
 *     File Name           :     ask-question-service.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-11-04 17:40]
 *     Last Modified       :     [2016-01-18 19:20]
 *     Description         :     发布问题的service
 **********************************************************************************/


app.service('AskQuestionSrv', ['$http', 'toaster', '$timeout', '$window', 'User', '$location', 'InviteReplySrv',
  function($http, toaster, $timeout, $window, User, $location, InviteReplySrv) {
    var self = this;

    this.invitedUsers = InviteReplySrv.invitedUsers;

    this.autoExtractedTags = {};
    this.allTagsInClass = [];


    this.viewVar = {
      isAutoTagLoadingShow: false,
      isRecommendLoadingShow: false
    };

    this.formdata = {
      title: '',
      class2: '',
      tags: [],
      content: '',
      invited_users: [],
      how_much: 0,
      need_anonymous: 0,
    };


    //根据class来获取下边的所有标签
    this.fetchTagsByClass = function(tagClass) {
      var req = {
        url: '/tag/all',
        method: 'GET',
        params: {
          tag_class: tagClass
        },
        headers: {
          "X-CSRFToken": getCookie("_xsrf")
        }

      };

      $http(req).success(function(data, status, headers, config) {
          var r = angular.fromJson(data);
          if (r.err === 0) {
            self.allTagsInClass.length = 0;
            angular.forEach(r.tags, function(value, key) {

              self.allTagsInClass.push({
                'tag': key,
                'num': value
              });
            });
            self.allTagsInClass.sort(function(a, b) {
              return b.num - a.num;
            });


          }

        })
        .error(function(data, status, headers, config) {
          toaster.pop('error', '获取该分类下的标签出错，不能自动补全', '', true);
        });

    };

    this.removeTag = function(index) {
      this.formdata.tags.splice(index, 1);

    };

    this.autoExtractTags = function(tagClass, questionTitle, questionContent) {

      var req = {
        url: '/tag/extract',
        method: 'POST',
        data: {
          tag_class: tagClass,
          question_title: questionTitle,
          question_content: questionContent
        },
        headers: {
          "X-CSRFToken": getCookie("_xsrf")
        }

      };

      $http(req).success(function(data, status, headers, config) {
          self.viewVar.isAutoTagLoadingShow = false;
          var r = angular.fromJson(data);
          switch (r.err) {
            case 0:

              var tags = {
                foundTags: r.tags.found_tags,
                recommendedTags: r.tags.recommend_tags
              };
              angular.copy(tags, self.autoExtractedTags);
              var usedTags = [];

              if (self.autoExtractedTags.foundTags.length > 0 && self.autoExtractedTags.foundTags.length <= 5) {
                usedTags = self.autoExtractedTags.foundTags;
              } else if (self.autoExtractedTags.foundTags.length === 0) {
                if (self.autoExtractedTags.recommendedTags.length > 0 && self.autoExtractedTags.recommendedTags.length <= 5) {
                  usedTags = self.autoExtractedTags.recommendedTags;
                } else if (self.autoExtractedTags.recommendedTags.length === 0) {
                  //真没有标签了
                  toaster.pop('warnning', '没有自动提取出标签呢，输入一个新的吧！', '', true);
                } else if (self.autoExtractedTags.recommendedTags.length > 5) {
                  for (var i in [1, 2, 3, 4, 5]) {
                    var random = Math.floor(Math.random() * self.autoExtractedTags.recommendedTags.length);
                    if (!(self.autoExtractedTags.recommendedTags[random] in usedTags)) {
                      usedTags.push(self.autoExtractedTags.recommendedTags[random]);
                    }
                  }
                }
              } else if (self.autoExtractedTags.foundTags.length > 5) {
                for (var j in [1, 2, 3, 4, 5]) {
                    var random = Math.floor(Math.random() * self.autoExtractedTags.foundTags.length);
                    if (!(self.autoExtractedTags.foundTags[random] in usedTags)) {
                      usedTags.push(self.autoExtractedTags.foundTags[random]);
                    }
                  //usedTags.push(self.autoExtractedTags.foundTags[Math.floor(Math.random() * self.autoExtractedTags.foundTags.length)]);
                }
              }
              angular.copy(usedTags, self.formdata.tags);
              break;
            case 1:
              toaster.pop('error', '自动提取标签失败！', '请检查是否已经登录！', false);
              break;
            case 2:
              toaster.pop('error', '自动提取标签失败！', '参数错误！', false);
              break;

          }

        })
        .error(function(data, status, headers, config) {

          self.viewVar.isAutoTagLoadingShow = false;
        });

    };

    this.deploy = function() {

      var content = this.formdata.content;

      if (!this.checkQuestionContent(content)) {
        return;
      }
      var iUsers = this.invitedUsers;
      this.formdata.invited_users = [];
      for (var i = 0; i < iUsers.length; i++) {
        this.formdata.invited_users.push({
          //通知页面需要真名
          user: iUsers[i].user,
          real_name: iUsers[i].real_name || iUser[i].nick_name
        });
      }

      var req = {
        url: '/question/post',
        method: 'POST',
        data: this.formdata,
        headers: {
          "X-CSRFToken": getCookie("_xsrf")
        }

      };

      if ($window.localStorage[User.getUserInfo().uid + '-ask-' + req.data.title]) {
        toaster.pop('error', '已经发布过了哦！', '', true);
        return;
      }

      $window.localStorage[User.getUserInfo().uid + '-ask-' + req.data.title] = true;
      $http(req).success(function(data, status, headers, config) {
          var r = angular.fromJson(data);
          switch (r.err) {
            case 0:
              //$window.onbeforeunload = function() {
              //  return '发布成功！';

              //};
              //$window.close();
              $window.localStorage[User.getUserInfo().uid + '-ask-' + config.data.title] = true;
              toaster.pop('success', '发布成功！', '页面将在3秒后跳转到首页', true);
              $timeout(function() {
                $location.path('/qahome/' + config.data.class2 + '/1');
              }, 3000);
              //alert('发布成功');
              break;
            case 1:
              toaster.pop('error', '发布失败！', '请检查是否已经登录！', false);
              $window.localStorage.removeItem(User.getUserInfo().uid + '-ask-' + config.data.title);
              //alert('没有登录');
              break;
            case 2:
              toaster.pop('error', '发布失败！', '参数错误！', false);
              $window.localStorage.removeItem(User.getUserInfo().uid + '-ask-' + config.data.title);
              break;
            case 3:
              toaster.pop('error', '发布失败！', '标题太长啦！', false);
              $window.localStorage.removeItem(User.getUserInfo().uid + '-ask-' + config.data.title);
              //alert('标题太长');
              break;
            case 4:
              toaster.pop('error', '发布失败！', '内容太长啦,最长10000字！', false);
              $window.localStorage.removeItem(User.getUserInfo().uid + '-ask-' + config.data.title);
              //alert('内容太长');
              break;
            case 5:
              toaster.pop('error', '发布失败！', '分类错误！', false);
              $window.localStorage.removeItem(User.getUserInfo().uid + '-ask-' + config.data.title);
              //alert('分类错误');
              break;
            case 6:
              toaster.pop('error', '发布失败！', 'Tag错误！', false);
              $window.localStorage.removeItem(User.getUserInfo().uid + '-ask-' + config.data.title);
              //alert('tag错误');
              break;
            case 7:
              toaster.pop('error', '发布失败！', 'F币不足以支付！', false);
              $window.localStorage.removeItem(User.getUserInfo().uid + '-ask-' + config.data.title);
              break;
            case 8:
              toaster.pop('error', '发布失败！', '您还没有设置头像，暂无发起问答权限,请到个人主页设置头像哦！', false);
              $window.localStorage.removeItem(User.getUserInfo().uid + '-ask-' + config.data.title);
              break;
          }

        })
        .error(function(data, status, headers, config) {
          $window.localStorage.removeItem(User.getUserInfo().uid + '-ask-' + config.data.title);

          if (status == 500) {
            //500服务器内部错误
            toaster.pop('error', '服务器内部错误！', '', false);
            //alert('服务器内部错误');

          } else if (status == 408) {
            //请求超时
            toaster.pop('error', '请求超时！', '', false);
            //alert('请求超时');
          }
        });

    };

    this.checkQuestionContent = function(content) {
      var length = removeHtmlTag(content).length;
      if (length < 1) {
        //toaster.pop('error', '请不要发送空内容哦～～～', '', true);
        //return false;
        return true; //修改为可以不添加问题描述
      } else if (length > 30000) {
        toaster.pop('error', '发送内容太长啦，目前最多3W字哦～～～', '', true);
        return false;
      }
      return true;

    };

    this.updateQuestion = function(id, content, class2) {
      if (!this.checkQuestionContent(content)) {
        return;
      }

      var req = {
        url: '/question/edit',
        method: 'POST',
        data: {
          id: id,
          content: content
        },
        headers: {
          "X-CSRFToken": getCookie("_xsrf")
        }

      };

      $http(req).success(function(data, status, headers, config) {
          var r = angular.fromJson(data);
          switch (r.err) {
            case 0:

              toaster.pop('success', '修改成功！', '', true);
              break;
            case 1:
              toaster.pop('error', '发布失败！', '请检查是否已经登录！', false);
              $window.localStorage.removeItem(User.getUserInfo().uid + '-ask-' + config.data.title);
              //alert('没有登录');
              break;
            case 2:
              toaster.pop('error', '发布失败！', '参数错误！', false);
              $window.localStorage.removeItem(User.getUserInfo().uid + '-ask-' + config.data.title);
              break;
            case 4:
              toaster.pop('error', '发布失败！', '内容太长啦,最长10000字！', false);
              $window.localStorage.removeItem(User.getUserInfo().uid + '-ask-' + config.data.title);
              //alert('内容太长');
              break;
          }

        })
        .error(function(data, status, headers, config) {

          //$window.localStorage.removeItem(User.getUserInfo().uid + '-ask-' + config.data.title);

          if (status == 500) {
            //500服务器内部错误
            toaster.pop('error', '服务器内部错误！', '', false);
            //alert('服务器内部错误');

          } else if (status == 408) {
            //请求超时
            toaster.pop('error', '请求超时！', '', false);
            //alert('请求超时');
          }
        });

    };

    this.setInviteFB = function(fb) {
      this.formdata.how_much = parseInt(fb);
    };
  }
]);

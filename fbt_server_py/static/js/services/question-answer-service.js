/*********************************************************************************
 *     File Name           :     question-answer-service.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-10-28 21:04]
 *     Last Modified       :     [2016-01-21 21:39]
 *     Description         :     问答的service
 *     TODO  : need to remove fetchQuestions and HotLatestQuestionSrv from QuestionAnswerSrv
 *     to keep QuestionAnswerSrv simple (comment by spark, 11:28)
 **********************************************************************************/

app.service('QuestionAnswerSrv', ['$http', 'toaster', 'User', 'HotLatestQuestionSrv', '$window',
  function($http, toaster, User, HotLatestQuestionSrv, $window) {
    var self = this;

    self.QuestionClassEnum = [
      '校园', '考研', '留学', '考证', '就业', '实习'
    ];

    self.init = function() {
      //self.GotHotLatestQuestions = 1; //第一次请求会把最新和最热的拿过来，第二次置为0后不再请求最新最热

      self.caredQuestions = [];
      //self.hotQuestions = [];
      //self.latestQuestions = [];
      self.user = {};

      self.cardUser = {}; //用来显示名片的
      self.currentPage = 0;
      self.totalPage = -1;
      self.noRecommened = false;

    };

    self.fetchQuestions = function(questionScope, currentQuestionClass, page) {
      var req;
      if (!User.logined()) {

        req = {
          url: '/question/list',
          method: 'GET',
          params: {
            by_all: questionScope,
            class2: currentQuestionClass,
            detail: HotLatestQuestionSrv.GotHotLatestQuestions,
            page: page
          }
        };
      } else {
        req = {
          url: '/question/login/list',
          method: 'GET',
          params: {
            by_all: questionScope,
            class2: currentQuestionClass,
            detail: HotLatestQuestionSrv.GotHotLatestQuestions,
            page: page
          }

        };

      }

      return $http(req).success(function(data, status, headers, config) {

          var r = angular.fromJson(data);
          if (r.err === 0) {
            //self.caredQuestions = r.cared_question;

            self.noRecommended = r.cared_question.no_recommended;
            self.normalizeQuestionList(r.cared_question.question_list);
            if (config.params.page === 1) {

              self.caredQuestions = r.cared_question.question_list;
            } else {
              self.caredQuestions = self.caredQuestions.concat(r.cared_question.question_list);
            }
            if (HotLatestQuestionSrv.GotHotLatestQuestions) {
              self.normalizeQuestionList(r.latest_question.question_list);
              self.normalizeQuestionList(r.hottest_question.question_list);
              HotLatestQuestionSrv.latestQuestions = r.latest_question;
              HotLatestQuestionSrv.hotQuestions = r.hottest_question;
              HotLatestQuestionSrv.GotHotLatestQuestions = 0;
            }

            self.user = self.normalizeUserInfo(r.user);
            self.currentPage = r.cared_question.current_page;
            if (r.cared_question.question_list.length) {
              self.totalPage = self.currentPage + 1;
            } else {
              self.totalPage = self.currentPage;
            }

          } else if (r.err === 1) {
            toaster.pop('error', '系统提示', '获取问答列表参数错误', false);

          }
        })
        .error(function(data, status, headers, config) {

          toaster.pop('error', '系统提示', '获取问答列表请求过程错误', false);

        });

    };

    self.answerCollect = function(id) {

      var req = {
        url: '/question/collect',
        method: 'GET',
        params: {
          question_id: id,
        }
      };
      $http(req).success(function(data, status, headers, config) {

          var r = angular.fromJson(data);
          if (r.err === 0) {
            toaster.pop('success', '收藏成功！', '', true);
          } else if (r.err === 1) {
            toaster.pop('error', '收藏失败！', '参数错误!', false);
          } else if (r.err === 2) {
            toaster.pop('error', '收藏失败！', '没有授权，请检查是否登录!', false);
          }

        })
        .error(function(data, status, headers, config) {
          toaster.pop('error', '收藏失败！', '请求过程错误', false);

        });

    };

    self.questionCollect = function(id) {
      var req = {
        url: '/question/collect',
        method: 'GET',
        params: {
          question_id: id,
        }
      };
      $http(req).success(function(data, status, headers, config) {

          var r = angular.fromJson(data);
          if (r.err === 0) {
            // q.has_collect = true;
            // q.followers_num += 1;
            toaster.pop('success', '收藏成功！', '', true);
          } else if (r.err === 1) {
            toaster.pop('error', '收藏失败！', '参数错误!', false);
          } else if (r.err === 2) {
            toaster.pop('error', '收藏失败！', '没有授权，请检查是否登录!', false);
          }

        })
        .error(function(data, status, headers, config) {
          toaster.pop('error', '收藏失败！', '请求过程错误', false);
        });

    };

    self.getAnswers = function(id, sort_by, page) {

      var req = {
        url: '/answer/list',
        method: 'GET',
        params: {
          question_id: id,
          sort_by: sort_by,
          page: page,
        }
      };

      $http(req).success(function(data, status, headers, config) {

          var r = angular.fromJson(data);
          if (r.err === 0) {} else if (r.err === 1) {

            toaster.pop('error', '系统提示', '获取答案参数错误', false);

          }
        })
        .error(function(data, status, headers, config) {

          toaster.pop('error', '系统提示', '获取答案请求过程错误', false);

        });

    };


    self.answerThumbUp = function(answer) {
      var req = {
        url: '/answer/thumb_up',
        method: 'GET',
        params: {
          //id: id,
          id: answer.id
        }

      };

      $http(req).success(function(data, status, headers, config) {
          var r = angular.fromJson(data);
          if (r.err === 0) {
            toaster.pop('success', '点赞成功!', '', true);
            answer.thumb_up_num += 1;
            answer.has_thumb_up = true;
            $window.localStorage[User.getUserInfo().uid + '-answer-thumbup-' + config.params.id] = true;
          } else if (r.err === 1) {
            toaster.pop('error', '点赞失败！', '参数错误!', false);
          } else if (r.err === 2) {
            toaster.pop('error', '点赞失败!', '没有授权，请检查是否登录！', false);
          } else if (r.err === 3) {
            toaster.pop('error', '点赞失败!', '您已经赞过了哟！', false);
          }
        })
        .error(function(data, status, headers, config) {
          toaster.pop('error', '点赞失败!', '请求过程错误!', false);
        });
    };

    self.questionThumbUp = function(question) {
      var req = {
        url: '/question/thumb_up',
        method: 'GET',
        params: {
          id: question.id,
        }
      };

      $http(req).success(function(data, status, headers, config) {
          var r = angular.fromJson(data);
          if (r.err === 0) {
            toaster.pop('success', '点赞成功!', '', true);
            //question.thumb_up_users.push(my_user);
            question.has_thumb_up = true;
            question.thumb_up_num += 1;
            $window.localStorage[User.getUserInfo().uid + '-question-thumbup-' + config.params.id] = true;
          } else if (r.err === 1) {
            toaster.pop('error', '点赞失败！', '参数错误!', true);
          } else if (r.err === 2) {
            toaster.pop('error', '点赞失败!', '没有授权，请检查是否登录！', true);
          } else if (r.err === 3) {
            toaster.pop('error', '点赞失败!', '您已经赞过了哟！', true);
          }
        })
        .error(function(data, status, headers, config) {
          toaster.pop('error', '点赞失败!', '请求过程错误!', false);
        });
    };


    //添加关注（人）
    self.followUser = function(user_info) {
      var req = {
        url: '/user/follow',
        method: 'POST',
        data: {
          who: user_info.user
        },
        xsrfCookieName: '_xsrf',
        xsrfHeaderName: 'X-CSRFToken'
      };
      $http(req).success(function(data, status, headers, config) {
          var response = angular.fromJson(data);
          if (response.err === 0) {
            toaster.pop('success', "关注成功！", '', true);
            user_info.is_followed = true;
            self.addNewFollowUser(user_info);
          } else if (response.err === 1) {
            toaster.pop('error', "关注失败！", '对不起，您还没有登录，请先登录!', true);
          } else if (response.err === 2) {
            toaster.pop('error', "关注失败！", '已经关注过了呢！', true);
          } else if (response.err === 3) {
            toaster.pop('error', "关注失败！", '不能关注自己哟！', true);
          }

        })
        .error(function(data, status, headers, config) {
          toaster.pop('success', "系统提示", "关注请求出错！", true);
          //console.log("careUser function's err is " + err);
        });

    };


    self.fetchUserCard = function(userEmail) {
      var req = {
        url: '/user/info/preview',
        method: 'GET',
        params: {
          user: userEmail
        }
      };
      return $http(req).success(function(data, status, headers, config) {
        var response = angular.fromJson(data);
        if (response.err === 0) {
          self.cardUser = self.normalizeUserInfo(response.user_info);
        } else {
          console.log('获取用户名片出错!');
        }

      }).error(function(data, status, headers, config) {
        console.log('获取用户名片请求出错!');
      });


    };

    //用来标准化用户信息，一些没有的字段需要设置成0
    self.normalizeUserInfo = function(user) {

      if (!user) return user;

      user.thumb_num = user.thumb_num ? user.thumb_num : 0;
      user.followers_num = user.followers_num ? user.followers_num : 0;
      user.answers_num = user.answers_num ? user.answers_num : 0;
      user.thanks_coin = user.thanks_coin ? user.thanks_coin : 0;
      user.desc = user.desc ? user.desc : '暂无描述';
      user.publisher_real_name = user.publisher_real_name === undefined ? user.publisher_real_name : user.nick_name;
      user.real_name = user.real_name === undefined ? user.nick_name : user.real_name;
      user.coins_by_study = user.coins_by_study ? user.coins_by_study : 0;

      return user;

    };



    //对问和答进行标准化，主要解决某些字段不存在的情况，如thanks_coin等
    self.normalizeQuestionList = function(list, not_replace_comment) {
      if (list === undefined || list.length === 0) {
        return list;
      }
      var length = list.length;
      for (var i = 0; i < length; i++) {
        var element = list[i];
        if (element.reply_num && element.best_answer && !not_replace_comment) {
          element['comment_num'] = element.best_answer.comment_num;
          element['comment_list'] = element.best_answer.comment_list;
        }

        element.thanks_coin = element.thanks_coin || 0;
        element.publisher_real_name = element.publisher_real_name ? element.publisher_real_name : element.publisher_nick;
        element.best_answer = element.best_answer ? element.best_answer : {};
        self.normalizeAnswerList([element.best_answer]); //答案也要标准化一下，真是头疼
        element.publisher_star_info = self.normalizePublisherStarInfo(element.publisher_star_info);
        ($window.localStorage[User.getUserInfo().uid + '-question-thumbup-' + element.id]) && (element.has_thumb_up = true);

      }
      return list;

    };

    self.normalizeAnswerList = function(list) {
      if (list === undefined || list.length === 0) {
        return list;
      }
      var length = list.length;
      for (var i = 0; i < length; i++) {
        var element = list[i];
        element.thanks_coin = element.thanks_coin ? element.thanks_coin : 0;
        element.thumb_up_num = element.thumb_up_num ? element.thumb_up_num : 0;
        element.publisher_real_name = element.publisher_real_name ? element.publisher_real_name : element.publisher_nick;
        element.publisher_star_info = self.normalizePublisherStarInfo(element.publisher_star_info);
        ($window.localStorage[User.getUserInfo().uid + '-answer-thumbup-' + element.id]) && (element.has_thumb_up = true);
      }
      return list;

    };

    self.normalizePublisherStarInfo = function(info) {
      if (info === undefined || info === []) {
        return info;
      }
      var result = [];
      var stars = {
        'is_postgraduate_star': '研',
        'is_campus_star': '校',
        'is_certificate_star': '证',
        'is_USA_star': '留',
        'is_work_star': '业',
        'is_internship_star': '习'
      };
      for (var i = 0; i < info.length; i++) {
        if (stars.hasOwnProperty(info[i])) {
          result.push(stars[info[i]]);
        }
      }
      return result;

    };

    /*
    如果关注用户成功，将新加关注用户加到localStorage.careUsers中，更新私信关注人列表
     */
    self.addNewFollowUser = function(user) {
      var followUsers;
      try {
        followUsers = JSON.parse(localStorage.careUsers);
      } catch (e) {}
      var array = {
        "user": user.user,
        "college": user.college,
        "real_name": user.real_name,
        "icon": user.icon,
        "university": user.university,
        "star_info": user.star_info,
        "uid": user.uid
      };
      followUsers.unshift(array);
      localStorage.careUsers = JSON.stringify(followUsers);
    }

  }
]);


app.service('QuestionAnswerDetailSrv', ['$http', 'toaster', 'QuestionAnswerSrv', 'HotLatestQuestionSrv', '$window', 'User', 'rte', '$filter',
  function($http, toaster, QuestionAnswerSrv, HotLatestQuestionSrv, $window, User, rte, $filter) {
    var self = this;

    self.clearContent = rte.clearContent;

    self.fetchMoreAnswer = function(id, page) {

      var req = {
        url: '/answer/list',
        method: 'GET',
        params: {
          question_id: id,
          page: page
        }
      };

      return $http(req).success(function(data, status, headers, config) {
          var response = angular.fromJson(data);
          if (response.err === 0) {
            QuestionAnswerSrv.normalizeAnswerList(response.answers_list);
            self.answers = response.answers_list;
            self.currentPage = response.current_page;
            self.totalPage = response.total_page;

            var my_user = localStorage.username;
            for (var i = 0; i < self.answers.length; i++) {
              var answer = self.answers[i];
              answer['is_me'] = my_user === answer.publisher;
              self.qaList.push({
                tags: self.question.tags,
                class2: self.question.class2,
                reply_num: 1,
                best_answer: self.answers[i],
                comment_num: self.answers[i].comment_num,
                comment_list: self.answers[i].comment_list,
                id: self.question.id,
                title: self.question.title,
              });
            }
          }
        })
        .error(function(data, status, headers, config) {
          console.log('获取问题详情请求出错!');
        });

    };

    self.fetchQuestionAnswerDetail = function(id) {
      var req = {
        url: '/question/detail',
        method: 'GET',
        params: {
          question_id: id,
          detail: HotLatestQuestionSrv.GotHotLatestQuestions,
        }
      };

      return $http(req).success(function(data, status, headers, config) {
        self.question = {};
        self.answers = [];
        self.qaList = [];

        var response = angular.fromJson(data);
        if (response.err === 0) {
          QuestionAnswerSrv.normalizeQuestionList([response.question], true);
          QuestionAnswerSrv.normalizeAnswerList(response.answers_list);
          self.question = response.question;
          self.answers = response.answers_list;
          self.currentPage = response.current_page;
          self.totalPage = response.total_page;
          self.question.reply_num = 0;
          var my_user = localStorage.username
          self.question['is_me'] = my_user === self.question.publisher;
          self.qaList.push(self.question);

          for (var i = 0; i < self.answers.length; i++) {
            var answer = self.answers[i];
            answer['is_me'] = my_user === answer.publisher;
            self.qaList.push({
              tags: self.question.tags,
              class2: self.question.class2,
              reply_num: 1,
              best_answer: self.answers[i],
              comment_num: self.answers[i].comment_num,
              comment_list: self.answers[i].comment_list,
              id: self.question.id,
              title: self.question.title,
            });
          }


          if (HotLatestQuestionSrv.GotHotLatestQuestions) {
            QuestionAnswerSrv.normalizeQuestionList(response.latest_question.question_list);
            QuestionAnswerSrv.normalizeQuestionList(response.hottest_question.question_list);
            HotLatestQuestionSrv.latestQuestions = response.latest_question;
            HotLatestQuestionSrv.hotQuestions = response.hottest_question;
            HotLatestQuestionSrv.GotHotLatestQuestions = 0;
          }

        } else {
          //console.log('获取问题详情参数出错!');
          toaster.pop('error', '系统提示', '获取问题详情出错！', false);
        }

      }).error(function(data, status, headers, config) {
        console.log('获取问题详情请求出错!');
      });

    };



    self.checkAnswerContent = function(content) {

      if (content === '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<p>给别人以知识，给自己以鼓舞！</p>\n</body>\n</html>') {
        toaster.pop('error', '请不要发送空内容哦～～～', '', true);
        return false;
      }
      var length = removeHtmlTag(content).length;

      if (length < 1) {
        toaster.pop('error', '请不要发送空内容哦～～～', '', true);
        return false;
      } else if (length > 10000) {
        toaster.pop('error', '发送内容太长啦，目前最多10000字哦～～～', '', true);
        return false;
      }
      return true;

    };

    self.updateAnswer = function(id, content, isAnonymous) {
      var req = {
        url: '/answer/edit',
        method: 'POST',
        data: {
          id: id,
          content: content,
          //need_anonymous: isAnonymous,
        },
        headers: {
          "X-CSRFToken": getCookie("_xsrf")
        }

      };
      return $http(req).success(function(data, status, headers, config) {
          var r = angular.fromJson(data);
          if (r.err === 0) {
            //for (var i = 0; i < self.qaList.length; i++) {
            //  if (self.qaList[i].best_answer.id === config.data.id) {
            //    self.qaList[i].best_answer.content = config.data.content;
            //    self.qaList[i].best_answer.need_anonymous = config.data.need_anonymous;
            //    break;
            //  }
            //}

            toaster.pop('success', '编辑成功！', '', false);
          } else if (r.err === 1) {

            $window.localStorage.removeItem(User.getUserInfo().uid + '-reply-' + self.question.id);
            toaster.pop('error', '系统提示', '没有授权回答，请检查是否登录！', false);

          } else if (r.err === 2) {
            $window.localStorage.removeItem(User.getUserInfo().uid + '-reply-' + self.question.id);
            toaster.pop('error', '系统提示', '更新参数错误！', false);

          } else if (r.err === 3) {

            $window.localStorage.removeItem(User.getUserInfo().uid + '-reply-' + self.question.id);
            toaster.pop('error', '系统提示', '答案长度超过1W！', false);
          }

        })
        .error(function(data, status, headers, config) {

        });

    };

    self.deployAnswer = function(id, content, isAnonymous) {
      var req = {
        url: '/answer/post',
        method: 'POST',
        data: {
          question_id: id,
          content: content,
          need_anonymous: isAnonymous,
        },
        headers: {
          "X-CSRFToken": getCookie("_xsrf")
        }

      };
      if ($window.localStorage[User.getUserInfo().uid + '-reply-' + req.data.question_id]) {
        toaster.pop('error', '已经回答过了呢', '您可以编辑已有的回答哦!', true);
        return;
      }
      $window.localStorage[User.getUserInfo().uid + '-reply-' + req.data.question_id] = true;

      $http(req).success(function(data, status, headers, config) {

          var r = angular.fromJson(data);
          if (r.err === 0) {
            var answerId = r.answer_id;
            var userInfo = r.user_info;
            var date = new Date();
            var answer = {
              id: answerId,
              is_me: true,
              class2: self.question.class2,
              publisher_img: userInfo.icon,
              publisher_nick: userInfo.nick_name,
              publisher_real_name: userInfo.real_name,
              ctime: $filter('date')(date, 'yyyy-MM-dd HH:mm'),
              content: config.data.content,
              tags: self.question.tags,
              thumb_up_num: 0,
              thanked_users: [],
              title: self.question.title,
              need_anonymous: config.data.need_anonymous,
              user: userInfo.user,
              publisher: userInfo.user,
              university: userInfo.university,
              college: userInfo.college,
              thanks_coin: 0,
              user_description: userInfo.user_description,
            };
            //self.answers.push(answer);
            QuestionAnswerSrv.normalizeAnswerList([answer]);
            self.qaList.push({
              reply_num: 1,
              id: self.question.id,
              best_answer: answer,
              comment_num: answer.comment_num,
              comment_list: answer.comment_list,
              tags: self.question.tags,
              class2: self.question.class2,
              title: self.question.title,
            });
            toaster.pop('success', '系统提示', '回答成功！', true);
            self.clearContent();
            $window.localStorage[User.getUserInfo().uid + '-reply-' + self.question.id] = true;
          } else if (r.err === 1) {

            $window.localStorage.removeItem(User.getUserInfo().uid + '-reply-' + self.question.id);
            toaster.pop('error', '系统提示', '没有授权回答，请检查是否登录！', false);

          } else if (r.err === 2) {
            $window.localStorage.removeItem(User.getUserInfo().uid + '-reply-' + self.question.id);
            toaster.pop('error', '系统提示', '回答参数错误！', false);

          } else if (r.err === 3) {

            $window.localStorage.removeItem(User.getUserInfo().uid + '-reply-' + self.question.id);
            toaster.pop('error', '系统提示', '答案长度超过1W！', false);
          } else if (r.err === 4) {
            $window.localStorage[User.getUserInfo().uid + '-reply-' + self.question.id] = true;
            toaster.pop('error', '系统提示', '已经回答过了哦！', false);
          } else if (r.err === 5) {
            $window.localStorage.removeItem(User.getUserInfo().uid + '-reply-' + self.question.id);
            toaster.pop('error', '系统提示', '您还没有设置头像，暂无发起问答权限,请到个人主页设置头像哦！', false);

          }


        })
        .error(function(data, status, headers, config) {
          $window.localStorage.removeItem(User.getUserInfo().uid + '-reply-' + self.question.id);
          toaster.pop('error', '系统提示', '回答提交请求过程错误', false);
        });

    };

  }
]);

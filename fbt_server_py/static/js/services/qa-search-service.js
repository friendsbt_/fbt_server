/*********************************************************************************
 *     File Name           :     qa-search-service.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-11-03 20:18]
 *     Last Modified       :     [2015-11-10 21:08]
 *     Description         :     问答搜索的service
 **********************************************************************************/

app.service('QASearchSrv', ['$http','toaster','QuestionAnswerSrv',
  function($http,toaster,QuestionAnswerSrv) {

    this.response = {};


    var self = this;

    this.search = function(keyword, needHotKeyword, page) {

      var req = {
        url: '/question/search',
        method: 'GET',
        params: {
          keyword: keyword,
          need_hot_keyword: needHotKeyword,
          page: page,
        }
      };
      return $http(req).success(function(data, status, headers, config) {
          var response = angular.fromJson(data);
          switch (response.err) {
            case 0:
              //self.qaSearchResult = response.resource_list;
              QuestionAnswerSrv.normalizeQuestionList(response.resource_list);
              self.response = response;
              break;
            case 1:
              toaster.pop('error', '系统提示', '问答搜索参数错误!', false);
              break;
            default:
              toaster.pop('error', '系统提示', '问答搜索错误!', false);

          }

        })
        .error(function(data, status, headers, config) {
          toaster.pop('error', '系统提示', '问答搜索请求错误!', false);
        });



    };


  }
]);

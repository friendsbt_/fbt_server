/*********************************************************************************
 *     File Name           :     tag-service.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-10-28 21:06]
 *     Last Modified       :     [2015-11-24 20:35]
 *     Description         :     标签service，所有的标签都从这里调，包括正确编辑的等等
 **********************************************************************************/

app.service('tagSrv', ['$http', '$modal', 'User', 'QuestionAnswerSrv', '$route', 'toaster',
  function($http, $modal, User, QuestionAnswerSrv, $route, toaster) {

    var self = this;


    this.alreadyInit = false;
    this.mode = 'select';

    //mode,表示是选择感兴趣的标签select
    //或者是添加问题的标签edit

    this.tags = []; //所有标签
    this.subscribedTags = []; //用户关注的标签
    this.draftTags = []; //正在编辑的文章的标签

    this.class2 = ''; //是否限制到class2分类中，class2为空字符串就是不限制,添加这个主要是发布问题页面需要


    this.tempTags2Subscribe = []; //添加这两个数组是为了暂存标签，因为用户可以点取消和确定,需要暂存
    this.tempTags2Unsubscribe = [];


    this.fetchMyTags = function() {
      this.subscribedTags.length = 0;

      var req = {
        method: 'GET',
        url: '/tag/list',
        headers: {
          "X-CSRFToken": getCookie("_xsrf")
        }
      };

      return $http(req).success(function(data, status, headers, config) {
          var d = angular.fromJson(data);
          if (d.err === 0) {
            for (var i = 0; i < d.tags.length; i++) {
              self.subscribedTags.push(d.tags[i]);
            }
            if (self.tags.length === 0) {
              self.alreadyInit = false;

            }

            //self.subscribedTags = d.tags;
          }
          else{
              self.alreadyInit = false;
          }
        })
        .error(function(data, status, headers, config) {

              self.alreadyInit = false;
        });

    };

    this.fetchAllTags = function() {
      this.tags.length = 0;

      var req = {
        method: 'GET',
        url: '/tag/all',
        headers: {
          "X-CSRFToken": getCookie("_xsrf")
        }
      };
      return $http(req).success(function(data, status, headers, config) {
          var d = angular.fromJson(data);
          if (d.err === 0) {
            var tags = d.tags; //这里返回的tags竟然是一个map
            for (var key in tags) {
              self.tags.push({
                tag: key,
                num: tags[key]
              });

            }
            self.tags.sort(function(a, b) {
              return b.num - a.num;
            });

            if (self.tags.length === 0) {
              self.alreadyInit = false;
            }

          }
          else{

              self.alreadyInit = false;

          }

        })
        .error(function(data, status, headers, config) {

        });

    };

    this.subscribeTag = function() {
      if (this.tempTags2Subscribe.length === 0) {
        return;
      }
      var req = {
        method: 'POST',
        url: '/tag/add',
        data: {
          tags_with_class: this.tempTags2Subscribe
        },
        headers: {
          "X-CSRFToken": getCookie("_xsrf")
        }
      };

      $http(req).success(function(data, status, headers, config) {})
        .error(function(data, status, headers, config) {

          var response = angular.fromJson(data);
          switch (response.err) {
            case 0:
              for (var i = 0; i < self.tempTags2Subscribe.length; i++) {
                self.subscribedTags.push(self.tempTags2Subscribe(i)); //把新订阅的标签添加到已订阅中
              }
              self.tempTags2Subscribe.length = 0;

              toaster.pop('success', '标签订阅成功！', '', true);
              break;
            case 1:
              toaster.pop('error', '标签订阅', '没有授权，请检查是否登录！', false);
              break;
            case 2:
              toaster.pop('error', '标签订阅', '参数错误！', false);
              break;

          }
        });

    };




    this.unSubscribeTag = function() {
      if (this.tempTags2Unsubscribe.length === 0) {
        return;
      }
      var req = {
        method: 'POST',
        url: '/tag/remove',
        data: {
          tags_with_class: this.tempTags2Unsubscribe
        },
        headers: {
          "X-CSRFToken": getCookie("_xsrf")
        }
      };
      $http(req).success(function(data, status, headers, config) {
          var response = angular.fromJson(data);
          switch (response.err) {
            case 0:
              self.tempTags2Unsubscribe.length = 0;
              toaster.pop('success', '取消标签订阅成功！', '', true);
              break;
            case 1:
              toaster.pop('error', '取消标签订阅', '没有授权，请检查是否登录！', false);
              break;
            case 2:
              toaster.pop('error', '取消标签订阅', '参数错误！', false);
              break;

          }



        })
        .error(function(data, status, headers, config) {


        });
    };


    this.addDraftTag = function(tag) {
      if (this.draftTags.indexOf(tag) >= 0) {
        toaster.pop('warnning', '已经添加了该标签哦！', '', true);
        return;
      }
      if (this.draftTags.lenght === 3) {
        toaster.pop('error', '最多添加3个标签哦！', '', true);
        return;
      }
      this.draftTags.push(tag);
    };

    this.removeDraftTag = function(index) {
      this.draftTags.splice(index, 1);
    };

    this.showTagModal = function(mode) {
      this.mode = mode;
      if (!this.alreadyInit) {
        this.init();
      }
      $('#nav-list').modal("show");
    };

    this.cancel = function() {
      //由于直接把标签添加到订阅标签中了，所以点取消要把对应的删除
      for (var i in this.tempTags2Subscribe) {
        var index = this.subscribedTags.indexOf(this.tempTags2Subscribe[i]);
        if (index >= 0) {
          this.subscribedTags.splice(index, 1);
        }

      }

      //由于直接在订阅标签中删除了，所以点取消要复位
      for (var k in this.tempTags2Unsubscribe) {
        this.subscribedTags.push(this.tempTags2Unsubscribe[k]);
      }
      this.tempTags2Unsubscribe.length = 0;
      this.tempTags2Subscribe.length = 0;

    };


    this.ok = function() {
      if (this.mode === 'select') {
        //在选择感兴趣的标签模式下，点确定后直接运行下面两个函数,service里边会在请求成功后把两个temp数组清空
        this.unSubscribeTag();
        this.subscribeTag();
        QuestionAnswerSrv.caredQuestions.length = 0; //清空重新获取呐
        QuestionAnswerSrv.GotHotLatestQuestions = 1;
        $route.reload();
      }

    };


    this.init = function() {
      this.fetchAllTags();
      this.fetchMyTags();
      this.alreadyInit = true;
    };
    this.init();

  }
]);


app.filter('slice', function() {
  return function(arr, start, end) {
    return (arr || []).slice(start, end);
  };

});

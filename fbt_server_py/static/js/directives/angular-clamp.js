/*********************************************************************************
 *     File Name           :     angular-clamp.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-11-14 22:58]
 *     Last Modified       :     [2015-11-15 21:11]
 *     Description         :     多行文本显示指定行数加...的direcitve
 **********************************************************************************/
//TODO:需要进一步地封装，现在还不太好用
angular.module('clamp', [])
  .service('clamp', ['$window',
    function($window) {
      this.$clamp = $window.$clamp;
    }
  ])
  .directive('clamp', ['clamp','$window',
    function(clamp, $window) {
      return {
        restrict: 'A',
        scope: {
          ngBindHtml: '@'
        },
        link: function(scope, element, attrs) {
          scope.$watch('ngBindHtml', function(value) {
           $window.$clamp(element[0], {
             clamp: parseInt(attrs.clamp),
             useNativeClamp: false
           });
          });
        }

      }; //end of return

    }
  ]);

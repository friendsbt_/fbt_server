/* vim: set sw=2 ts=2 expandtab : */
'use strict';
angular.module('share', [])
.service('share', function($rootScope, $timeout) {
  return {
    show: function(obj) {
      $timeout(function() {
        $rootScope.$broadcast('share-show', obj)
      });
    },
    hide: function() {
      $timeout(function() {
        $rootScope.$broadcast('share-hide')
      });
    }
  };
})
.directive('shareContainer', ['share','$window',  function(share,$window ) {
  return {
    restrict: 'E',
    replace: true,
    scope: {},
    template:
      '<div id="share-container" ng-show="display" ng-click="display=false">' +
        '<div>' +
          '<div>' +
            '<div class="close" ng-click="display=false;">×</div>' +
            '<div class="left">' +
              '<div class="qrcode">' +
                '<qrcode version="8" data="{{ obj.weixin }}" size="200"></qrcode>' +
              '</div>' +
              //'<div class="note"><i class="icon-wechat"></i>校园星空棒棒滴，扫我分享！</div>' +
              '<div class="note">校园星空棒棒滴，扫我分享！</div>' +
            '</div>' +
            '<div class="right">' +
              '<div class="qrcode">' +
                '<qrcode version="8" data="{{ obj.url }}" size="200"></qrcode>' +
              '</div>' +
              //'<div class="note"><i class="icon-pengyouquan"></i>这个问答真心赞，扫我分享！</div>' +
              '<div class="note">这个问答真心赞，扫我分享！</div>' +
            '</div>' +
            '<div class="channel">' +
              //'<div class="note"><i class="fa fa-info-circle"></i> 分享到下方渠道也送500F哦</div>' +
              '<i ng-repeat="c in channels" ng-class="c.icon" ng-click="shareClickHandler(c)"></i>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>',
    link: function(scope, elem, attrs) {
      scope.display = false;
      var summery = "校园星空官网——中国最好的大学学习资源和知识分享社区";
      scope.channels = [
        {id: 'qzone', baseurl: 'http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=URL&title=TITLE&desc=DESC&summary=SUMMARY', icon: 'icon-qzone'},
        {id: 'weibo', baseurl: 'http://service.weibo.com/share/share.php?url=URL&title=TITLE', icon: 'icon-weibo'},
        {id: 'renren', baseurl: 'http://widget.renren.com/dialog/share?resourceUrl=URL&title=TITLE&description=SUMMARY', icon: 'icon-renren'},
        {id: 'douban', baseurl: 'http://www.douban.com/share/service?href=URL&name=TITLE&text=SUMMARY&image=IMAGE', icon: 'icon-douban'},
        {id: 'qq', baseurl: 'http://connect.qq.com/widget/shareqq/index.html?url=URL&title=TITLE&desc=DESC&pics=IMAGE', icon: 'icon-qq'}
        //{id: 'wechat', url: '', icon: 'icon-wechat'},
      ];

      scope.$on('share-show', function(event, obj) {
        scope.display = true;
        scope.obj = obj;
      });
      scope.$on('share-hide', function() {
        scope.display = false;
      });
      scope.shareClickHandler = function(channel) {
        var shareUrl = channel.baseurl
          .replace('URL', encodeURIComponent(scope.obj.url))
          .replace('TITLE', encodeURIComponent(scope.obj.title))
          .replace('DESC', encodeURIComponent(scope.obj.desc))
            .replace('SUMMARY', encodeURIComponent(scope.obj.summary))
          .replace('IMAGE', encodeURIComponent(scope.obj.image));
            $window.open(shareUrl, "_blank");
        scope.display = false;
      };
    }
  };
}]);

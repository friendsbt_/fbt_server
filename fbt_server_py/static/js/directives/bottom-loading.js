/* from module-loading.js : */
angular.module('bottomLoading', [])
.service('bottomLoading', function($rootScope, $timeout) {
  return {
    show: function(title) {
      $timeout(function() {
        $rootScope.$broadcast('bottom-loading-show', title)
      });
    },
    hide: function() {
      $timeout(function() {
        $rootScope.$broadcast('bottom-loading-hide')
      });
    }
  };
})
.directive('bottomLoadingContainer', ['bottomLoading', '$window', '$document', function(bottomLoading, $window, $document) {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      noMore: "=",
      onBottom: "&"
    },
    template:
      '<div id="bottom-loading-container" style="text-align: center; display: block; margin: 20px;">' +
            '<i ng-show="loading.display" class="fa fa-spinner fa-pulse fa-2x"></i>' +
            '<span ng-show="noMore">没有更多了</span>' +
      '</div>',
    controller: function($scope) {
      if(!$scope.loading) $scope.loading = {};

      $scope.loading.display = false;
      $scope.$on('bottom-loading-show', function(event, title) {
        $scope.loading.display = true;
      });
      $scope.$on('bottom-loading-hide', function() {
        $scope.loading.display = false;
      });
    },
    link: function(scope, elem, attrs) {
        var w = angular.element($window);
        var d = angular.element($document);
        var check = function() {
          scope.bottom = (w.scrollTop() + w.height() == d.height());
          // console.log(scope.bottom + '>' + w.scrollTop() + '|' + w.height() + '|' + d.height());
          //console.log(scope.bottom + '|' + scope.noMore + '|' + scope.loading.display);
          if (scope.bottom && !scope.noMore && !scope.loading.display) {
            scope.onBottom();
          }
        };
        var handler = function() {
          //scope.$apply(check);
          check();
        };
        //w.bind('scroll', handler);
        scope.$watch('noMore', function(newValue, oldValue) {
          if (oldValue && !newValue) {
            w.bind('scroll', handler);
          } else if (!oldValue && newValue) {
            w.unbind('scroll', handler);
          }
        });
        check();
        } // end of link 
    }
}])
.directive('moreLoadingContainer', ['bottomLoading', function(bottomLoading) {
    return {
    restrict: 'E',
    replace: true,
    scope: {
      noMore: "=",
      load: "&"
    },
    template:
        '<div> \
          <div class="load-more" ng-click="load()" ng-hide="noMore"> \
            <i ng-show="loading.display" class="fa fa-spinner fa-pulse fa-2x"></i> \
            <span ng-hide="loading.display">加载更多</span> \
          </div> \
          <div class="no-more" ng-show="noMore">没有更多的了</div> \
      </div>',
    controller: function($scope) {
      if(!$scope.loading) $scope.loading = {};
      $scope.loading.display = false;
      $scope.$on('bottom-loading-show', function(event, title) {
        $scope.loading.display = true;
      });
      $scope.$on('bottom-loading-hide', function() {
        $scope.loading.display = false;
      });
    }
  }
}])
.directive('onlyBottom', ['$window', '$document', '$timeout', function($window, $document, $timeout) {
  return {
    link: function(scope, elem, attrs) {
        var w = angular.element($window);
        var d = angular.element($document);
        var e = $(elem);
        var other_side = e.parent();
        var origin_top = e.offset().top;
        // var timer = null;
        var check = function() {
          //console.log(w.scrollTop() + '|' + w.height() + '|' + e.outerHeight()+ '|' + origin_top + '|' + d.outerHeight());
          var h = e.outerHeight();
          if (h > w.height() - origin_top) {
            var tmp = w.scrollTop() + w.height();
            if (tmp > origin_top + h) {
              var new_top = tmp - origin_top - h;
              var bottom_reserved = 30;
              var other_side_cover = other_side.offset().top + other_side.outerHeight() - bottom_reserved;
              if (origin_top + new_top + h > other_side_cover)
                e.css({'top': other_side_cover - origin_top - h - bottom_reserved});
              else
                e.css({'top': new_top});
            } else
             e.css({'top': 0});
          } else {
            var new_top = w.scrollTop();
            //if (new_top < )
            var other_side_cover = other_side.offset().top + other_side.outerHeight()
            if (origin_top + new_top + h > other_side_cover)
                new_top = other_side_cover - origin_top - h;
            e.css({'top': new_top});
          }   
        };
        var handler = function() {
          // if (timer) $timeout.cancel(timer);
          // timer = $timeout(check, 10);
          check();
        };
        w.bind('scroll', handler);
        } // end of link 
    }
}])
.directive('toTop', ['$location', '$anchorScroll', '$window', function($location, $anchorScroll, $window) {
    return {
      restrict: 'E',
      scope: {},
      template:
        '<footer ng-show="scrolled"> \
          <div> \
            <span class="glyphicon glyphicon-arrow-up move-top" ng-click="toTop()"></span> \
          </div> \
        </footer>',
      link: function(scope, elem, attrs) {
          scope.toTop = function(){
            $anchorScroll();
          };
          var w = angular.element($window);
          w.bind('scroll', function() {
            scope.$apply(function() {
              scope.scrolled = w.scrollTop() ? true : false;
            });
          });
      }
  }
}]);
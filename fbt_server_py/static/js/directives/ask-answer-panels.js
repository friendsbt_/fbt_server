app.factory('interactiveService', interactiveService);
interactiveService.$inject = ['$http', '$window', 'User', 'QuestionAnswerSrv', 'share', 'ThankSrv', 'toaster', '$location', '$anchorScroll','rte'];

app.filter("encode_username", function() {
  var filter = function(input) {
    return encode_username(input);
  };
  return filter;
});

app.filter('normalize_size', function() {
  var filter = function(input) {
    return normalizeFileSize(input);
  };
  return filter;
});

app.filter('removeHtmlTag', function() {
  var filter = function(input) {
    if(input=== undefined || input === '')
        return '';
    var s =input;
    s =  s.replace(/(\n)/g,'');
    s =  s.replace(/(\t)/g,'');
    s =  s.replace(/(\r)/g,'');
    s =  s.replace(/<\/?[^>]*>/g,'');
    s =  s.replace(/\s*/g,'');
    s =  s.replace(/&nbsp;|&#160;/gi, '');
    //s = '<html>' + s + '</html>';
    return s;

  };
  return filter;
});

//angular 输出富文本
app.filter('trustHtml', function($sce) {
  return function(text) {
    return $sce.trustAsHtml(text);
  };
});

app.directive('userCard', userCard);
userCard.$inject = ['interactiveService', 'QuestionAnswerSrv','User'];

app.directive('askAnswerPanels', askAnswerPanels);
askAnswerPanels.$inject = ['interactiveService', '$window', 'rte',  'AskQuestionSrv','QuestionAnswerDetailSrv', '$timeout'];

app.directive('inactiveChange', inactiveChange);

app.directive('certIcon', certIcon);

function certIcon() {
    var directive = {
        scope: {
          certIcon: '='
        },
        link: link,
        restrict: 'A'
    };

    function link(scope, elem, attrs) {
      //校园 考研 留学 考证 就业 实习
      var stars = {
        '研': [1, '考研之星'],
        '校': [0, '校园之星'],
        '证': [3, '考证之星'],
        '留': [2, '留学之星'],
        '业': [4, '就业之星'],
        '习': [5, '实习之星']
      };
        var stars2 = {
        'is_postgraduate_star':[1,'考研之星'],
        'is_certificate_star':[3,'考证之星'],
        'is_campus_star':[0,'校园之星'],
        'is_work_star':[4,'就业之星'],
        'is_USA_star':[2,'留学之星'],
        'is_internship_star':[5,'实习之星']
        };
      var e = $(elem);
      var tmp = stars[scope.certIcon];
        if(!tmp){
            tmp = stars2[scope.certIcon];
        }
      e.css({
        width: 18,
        height: 20,
        display: 'inline-block',
        'background-size': '108px 20px',
        'background-repeat': 'no-repeat',
        'vertical-align': 'bottom',
        'background-image': "url('/static/images/cert.png')",
        'background-position': -18 * tmp[0] + "px 0px"
      });
      e.attr({title: tmp[1]});
    }
    return directive;
};

function inactiveChange() {
    var directive = {
        scope: {
          inactiveChange: '=',
          click: '&'
        },
        link: link,
        restrict: 'A'
    };

    function link(scope, elem, attrs) {
      var e = $(elem);
      if (scope.inactiveChange) {
        e.addClass('inactive');
      } else {
        e.addClass('active');
        elem.bind('click', scope.click);
      }
    }
    return directive;
};

function userCard(interactiveService, QuestionAnswerSrv,User) {
    var directive = {
        link: link,
        scope: {
          userQuestion: '='
        },
        templateUrl: '/statics/partials/user-card.html',
        restrict: 'E'
    };
    return directive;

    function link(scope, elem, attrs) {
        scope.followUser = interactiveService.followUser;
        scope.$watch('userQuestion', function(newValue, oldValue) {
          if (newValue)
            showUserCard(newValue);
          else
            hideUserCard();
        });

        function showUserCard(question) {
          if (question.need_anonymous) {
            return;
          }
          var qId = question.id;
          var userEmail = question.publisher;
          interactiveService.fetchUserCard(userEmail).then(function() {
            scope.cardUser = QuestionAnswerSrv.cardUser;
              /*
              这里只有用户登录后才会显示是否已关注
               */
              if(User.logined()){
                  scope.isFollowed = interactiveService.isFollowed;
              }
          });
          // var offset = angular.element('#' + qId).offset();
          // console.log(offset.top + '|' + offset.left);
          var offset = angular.element('#' + qId).parent().position();
          angular.element('#userinfor')
          .css({
            top: offset.top,
            left: offset.left
          })
          // .css({position: 'absolute'})
          .show();
        };

        function hideUserCard() {
          angular.element('#userinfor').hide();
        };
    }
}

function askAnswerPanels(interactiveService, $window, rte, AskQuestionSrv,QuestionAnswerDetailSrv, $timeout) {
    var directive = {
        controller: controller,
        scope: {
            questions: '=',
            isDetail: '=',
            isMyAnswer: '=?',
            isMyQuestion: '=?',
            isMe: '='
        },
        templateUrl: '/statics/partials/ask-answer-panels.html',
        restrict: 'E'
    };
    return directive;

    function controller($scope) {
        $scope.cardUser = null;
        $scope.share = interactiveService.share;
        $scope.followUser = interactiveService.followUser;
        $scope.goAnswer = function(answer_id) {
          if ($scope.isDetail)
            interactiveService.goToReply();
          else
            interactiveService.goAnswer(answer_id);
        };

        //deprecated
        //$scope.editQuestion = function(question){
        //  $window.localStorage['edit-question-title'] = question.title;
        //  $window.localStorage['edit-question-class2'] = question.class2;
        //  $window.localStorage['edit-question-content'] = question.content;
        //  $window.localStorage['edit-question-tags'] = angular.toJson(question.tags);
        //  $window.localStorage['edit-question-invited_users'] = angular.toJson(question.invited_users);
        //  $window.open('/#/askquestion?edit=1&editQuestionId='+question.id, '_blank');
        //};
        //deprecated
        //$scope.editAnswer = function(question_id, answer_id) {
        //  $window.open('/#/qadetail/' + question_id + '?edit=1&answerId=' + answer_id + '#iCanReply', '_blank');
        //};

        $scope.comment = function(question) {
          interactiveService.comment(question, question.comment_content, question.comment_to_user);
        };

        $scope.comment_focus = function(q) {
          var w = angular.element($window);
          var id = q.reply_num? q.best_answer.id: q.id;
          var elem = $("#"+id + '-comment');
          
          $timeout(function() {
            var top = elem.offset().top;
            w.scrollTop(top - w.height() / 2);
            elem.focus();
          }, 1);
        };

        $scope.is_author = function(question, real_name) {
          var info = question.reply_num? question.best_answer: question;
          return real_name === info.publisher_real_name;
        };

        $scope.editQuestion = function(question){
          $window.open('/#/qadetail/'+ question.id, '_blank');
        };

        $scope.editAnswer = function(question_id, answer_id) {
            $window.open('/#/qadetail/' + question_id + '#' + answer_id,'_blank');
          //$window.open('/#/qadetail/' + question_id + '?edit=1&answerId=' + answer_id + '#iCanReply', '_blank');
        };

        $scope.editAnswerHere = function(answer){
            answer.editMode = true;//编辑模式
            $scope.editAnswerMode = true;
            $scope.editedAnswer = answer;
            $scope.editAnswerContent = angular.element('#' + answer.id + '-content').html();
            //$scope.editAnswerContent = answer.content;
            //tinymce.init({
            //    selector:'#' + answer.id + '-content',

            //});
            rte.setEditContent($scope.editAnswerContent);
            rte.init('#' + answer.id + '-content','default');
        };
        $scope.editQuestionHere = function(question){
            question.editMode = true;//编辑模式
            $scope.editQuestionMode = true;
            $scope.editedQuestion = question;
            $scope.editQuestionContent = angular.element('#' + question.id + '-content').html();
            //$scope.editQuestionContent = question.content;
            //tinymce.init({
            //    selector:'#' + question.id + '-content',
            //});
            rte.setEditContent($scope.editQuestionContent);
            rte.init('#' + question.id + '-content','simple');
        };
        $scope.editCancel =function(){
            var editor;
            if($scope.editAnswerMode){
            $scope.editedAnswer.editMode = false;
                editor = tinymce.get($scope.editedAnswer.id + '-content');
                //angular.element('#' + $scope.editAnswer.id + '-content').html($scope.editAnswerContent); 
            editor.setContent($scope.editAnswerContent);
            editor.remove();
            }
            if($scope.editQuestionMode){
            $scope.editedQuestion.editMode = false;
                editor = tinymce.get($scope.editedQuestion.id + '-content');
                //angular.element('#' + $scope.editQuestion.id + '-content').html($scope.editQuestionContent); 
            editor.setContent($scope.editQuestionContent);
            editor.remove();
            }
            $scope.editAnswerMode =false;
            $scope.editQuestionMode = false;
        };
        $scope.editSave = function(){
            var editor;
            if($scope.editAnswerMode){
            $scope.editedAnswer.editMode = false;
                editor = tinymce.get($scope.editedAnswer.id + '-content');
                var content = editor.getContent();
                editor.remove();
                if(QuestionAnswerDetailSrv.checkAnswerContent(content)){
                    QuestionAnswerDetailSrv.updateAnswer($scope.editedAnswer.id, content, $scope.editedAnswer.need_anonymous);
                }

            }
            if($scope.editQuestionMode){
            $scope.editedQuestion.editMode = false;
                editor = tinymce.get($scope.editedQuestion.id + '-content');
                var content = editor.getContent();
                editor.remove();
                if(AskQuestionSrv.checkQuestionContent(content)){
                    AskQuestionSrv.updateQuestion($scope.editedQuestion.id,content,$scope.editedQuestion.need_anonymous);
                }


            }
            $scope.editAnswerMode = false;
            $scope.editQuestionMode = false;
        }

        $scope.questionThumbUp = interactiveService.questionThumbUp;
        $scope.answerThumbUp = interactiveService.answerThumbUp;
        $scope.showQuestionThanksModal = interactiveService.showQuestionThanksModal;
        $scope.showAnswerThanksModal = interactiveService.showAnswerThanksModal;
        $scope.questionCollect = interactiveService.questionCollect;
        
        $scope.setQuestion = function(q) {
          $scope.user_question = q;
        }
    }
}

function interactiveService($http, $window, User, QuestionAnswerSrv, share, ThankSrv, toaster, $location, $anchorScroll,rte) {
    return {
        share: shareQuestion,
        goAnswer: goAnswer,
        goToReply: goToReply,
        comment: comment,
        followUser: QuestionAnswerSrv.followUser,
        unfollowUser: unfollowUser,
        questionThumbUp: QuestionAnswerSrv.questionThumbUp,
        answerThumbUp: QuestionAnswerSrv.answerThumbUp,
        questionCollect: QuestionAnswerSrv.questionCollect,
        fetchUserCard: QuestionAnswerSrv.fetchUserCard,
        normalizeUserInfo: QuestionAnswerSrv.normalizeUserInfo,
        normalizeQuestionList: QuestionAnswerSrv.normalizeQuestionList,
        normalizeAnswerList: QuestionAnswerSrv.normalizeAnswerList,
        normalizePublisherStarInfo: QuestionAnswerSrv.normalizePublisherStarInfo,
        showQuestionThanksModal: ThankSrv.showQuestionThanksModal,
        showAnswerThanksModal: ThankSrv.showAnswerThanksModal,
        isFollowed:isFollowed
    };

    function goToReply() {
      $location.hash('iCanReply');
      $anchorScroll();
    };

    function shareQuestion(question) {
      var url = 'http://' + $location.host() + '/#/qadetail/' + question.id;
      url += question.reply_num ? ('#' + question.best_answer.id) : '';
      share.show({
        title: question.title + ' -' + (question.reply_num ? (question.best_answer.nned_anonymous ? '匿名用户': question.best_answer.publisher_real_name + '的回答') :(question.need_anonymous?'匿名用户' : question.publisher_real_name + '的提问')) + '\n' + url  ,
        desc: '刚看到了一个很好的内容,分享给大家!',
        summary: question.reply_num ? removeHtmlTag(question.best_answer.content):removeHtmlTag(question.best_answer.content),
        url: url,
        weixin: 'http://study.friendsbt.com',
      });
    };

    function goAnswer(id) {
      var url = User.logined() ? '/#/qadetail/' + id + '#iCanReply' : '/#/home?return_url=qadetail/' + id + '#iCanReply';
      $window.open(url, '_blank');
    };

    function get_my_user() {
      var localStorage = $window.localStorage;
      return {
        university: localStorage.university,
        user: localStorage.username,
        user_icon: localStorage.icon,
        real_name: localStorage.real_name
      };
    };

    function get_user(question) {
      return {
        university: question.university,
        user: question.publisher,
        user_icon: question.publisher_img,
        real_name: question.publisher_real_name
      };
    };

    function comment(question, content, to_user) {
      to_user = to_user || get_user(question.reply_num? question.best_answer: question);
      var id = question.reply_num? question.best_answer.id: question.id
      var req = {
        url: question.reply_num ? "/answer/comment": "/question/comment",
        method: 'GET',
        params: {
          id: id,
          content: content,
          reply_to_user: to_user.user
        }
      };
      $http(req).success(function(response){
        if(response.err == 0){
          toaster.pop('success', "系统提示","评论成功",true);
          question.comment_list = question.comment_list || [];
          question.comment_list.push({
            from: get_my_user(),
            to: to_user,
            content: content,
            ctime: '刚刚'
          });
          question.comment_content = "";
          question.comment_to_user = null;
        }else{
          toaster.pop('error', "系统提示", response.info, true);
        }
      }).error(function(err){
        console.log("comment function's err is " + err);
      })
    };

    function unfollowUser(user_info){
      var req = {
        url: '/user/unfollow',
        method: 'POST',
        data: {who: user_info.user},
        xsrfCookieName: '_xsrf',
        xsrfHeaderName: 'X-CSRFToken'
      };
      $http(req).success(function(response){
        if(response.err == 0){
          toaster.pop('success', "系统提示","您已成功取消关注该用户",true);
          user_info.is_followed = false;
            delFollowUser(user_info);
        }else{
          toaster.pop('error', "系统提示", '对不起，您还没有登录，请先登录',true);
        }
      }).error(function(err){
        console.log("careUser function's err is " + err);
      })
    };
    /*
    判断当前用户是否已关注，和localStorage.careUsers中比较，
     */
    function isFollowed(user_info){
        var followUsers = JSON.parse(localStorage.careUsers);
        var length = followUsers.length;
        var flag = false;
        for(var i = 0; i < length; i++){
            if(followUsers[i].user == user_info.user)flag = true;
        };
        return flag;
    }
    /*
    如果用户取消关注某人，更新localStorage关注列表
     */
    function delFollowUser(user_info){
        var followUsers;
        try{
            followUsers = JSON.parse(localStorage.careUsers);
        }catch(e){

        }
        var length = followUsers.length;
        var index = 0;
        for(var i = 0; i < length; i++){
            if(followUsers[i].user == user_info.user)index = i;

        }
        followUsers.splice(index,1);
        localStorage.careUsers = JSON.stringify(followUsers);
    }
}

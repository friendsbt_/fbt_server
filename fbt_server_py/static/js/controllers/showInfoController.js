/**
 * Created by yulei on 2015/11/19.
 */
app.service('userSrv', ['$http', '$q', 'toaster', function ($http, $q, toaster) {
    var self = this;
    var user_info = {
        is_me: true,
        desc_editing: false
    };

    self.setUser = function (user) {
        $http({
            url: '/user/info',
            method: 'GET',
            params: {
                user: user
            }
        })
            .success(function(response) {
                if (response.err)
                    toaster.pop('error', '系统提示', response.info, true);
                else {
                    var tags = response.info.tags;
                    var is_me = response.info.is_me;
                    if (is_me)
                        user_info.who = "我";
                    else {
                        if (user_info.gender==='男')
                            user_info.who = "他";
                        else
                            user_info.who = "她";
                    }
                    response.info.tags = [];
                    for (var key in tags) {
                        if (typeof(tags[key]) !== "function")
                            response.info.tags.push({
                                tag: key,
                                num: tags[key]
                            });
                    }
                    if ('点击编辑' === response.info.desc) response.info.desc = "";
                    if (!response.info.honor) response.info.honor = [];
                    if (!is_me && !response.info.phone)  response.info.phone = "***";
                    if (!is_me && !response.info.qq)  response.info.qq = "***";
                    if (response.info.honor) {
                        response.info.honor.sort(
                            function (a, b) {
                                return a[0] - b[0];
                            }
                        );
                    }
                    angular.extend(user_info, response.info);
                }
            })
            .error(function(data,status,headers,config) {
                toaster.pop('error', '系统提示', '请求过程错误', false);
            });
    };

    self.changeValue = function (name, old_val, new_val) {
        var deferred = $q.defer();
        data = {};
        data[name] = new_val;
        if (old_val === new_val){
            deferred.resolve();
        } else {
            var req = {
                url: '/user/info',
                method: 'POST',
                data: $.param(data),
                xsrfCookieName: '_xsrf',
                xsrfHeaderName: 'X-CSRFToken',
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            };

            $http(req).success(function(response) {
                if (response.err) {
                    toaster.pop('error', '系统提示', response.info, true);
                    deferred.reject();
                }else{
                    user_info[name] = new_val;
                    toaster.pop('success', '系统提示', '修改成功!', true);
                    deferred.resolve();
                }
            })
                .error(function(data, status, headers, config) {
                    toaster.pop('error', '系统提示', '请求过程错误', true);
                    deferred.reject();
                });
        }
        return deferred.promise;
    };

    self.changeMultiValues = function (values) {
        var deferred = $q.defer();
        var req = {
            url: '/user/info',
            method: 'POST',
            data: $.param(values),
            xsrfCookieName: '_xsrf',
            xsrfHeaderName: 'X-CSRFToken',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        };

        $http(req).success(function(response) {
            if (response.err) {
                toaster.pop('error', '系统提示', response.info, true);
                deferred.reject();
            }else{
                for (var key in values) {
                    if (typeof(values[key]) !== "function") user_info[key] = values[key];
                }
                toaster.pop('success', '系统提示', '修改成功!', true);
                deferred.resolve();
            }
        })
            .error(function(data, status, headers, config) {
                toaster.pop('error', '系统提示', '请求过程错误', false);
                deferred.reject();
            });
        return deferred.promise;
    };

    self.updateHonor = function () {
        var deferred = $q.defer();
        var req = {
            url: '/user/info',
            method: 'POST',
            data: $.param({"honor": JSON.stringify(user_info.honor)}),
            xsrfCookieName: '_xsrf',
            xsrfHeaderName: 'X-CSRFToken',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        };

        $http(req).success(function(response) {
            if (response.err) {
                toaster.pop('error', '系统提示', response.info, true);
                deferred.reject();
            }else{
                toaster.pop('success', '系统提示', '修改成功!', true);
                deferred.resolve();
            }
        })
            .error(function(data, status, headers, config) {
                toaster.pop('error', '系统提示', '请求过程错误', false);
                deferred.reject();
            });
        return deferred.promise;
    };

    self.followUser = function(){
        var req = {
            url: '/user/follow',
            method: 'POST',
            data: {who: user_info.user},
            xsrfCookieName: '_xsrf',
            xsrfHeaderName: 'X-CSRFToken'
        };
        $http(req).success(function(response){
            if(response.err == 0){
                toaster.pop('success', "系统提示","您已成功关注该用户",true);
                user_info.is_followed = true;
            }else{
                toaster.pop('error', "系统提示", '对不起，您还没有登录，请先登录',true);
            }
        }).error(function(err){
            console.log("followUser function's err is " + err);
        });
    };

    self.unfollowUser = function(){
        var req = {
            url: '/user/unfollow',
            method: 'POST',
            data: {who: user_info.user},
            xsrfCookieName: '_xsrf',
            xsrfHeaderName: 'X-CSRFToken'
        };
        $http(req).success(function(response){
            if(response.err == 0){
                toaster.pop('success', "系统提示","您已成功取消关注该用户",true);
                user_info.is_followed = false;
            }else{
                toaster.pop('error', "系统提示", '对不起，您还没有登录，请先登录',true);
            }
        }).error(function(err){
            console.log("careUser function's err is " + err);
        })
    };

    self.getAuth = function() {
        var req = {
            url: '/user/star/auth',
            method: 'GET'
        };
        return $http(req);
    };

    self.auth = function(university, college, entrance_year, degree, university_mail, class2) {
        var data = {
            university: university,
            college: college,
            entrance_year: entrance_year,
            degree: degree,
            university_mail: university_mail,
            class2: class2
        };
        var req = {
            url: '/user/star/auth',
            method: 'POST',
            data: $.param(data),
            xsrfCookieName: '_xsrf',
            xsrfHeaderName: 'X-CSRFToken',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        };

        $http(req).success(function(response) {
            if (response.err) {
                var info = "";
                switch(response.err) {
                    case 4:
                        info = "用户邮箱出错";
                        break;
                    case 5:
                        info = "点赞不足66个";
                        break;
                    case 6:
                        info = "该类帖子数不足10个";
                        break;
                    default:
                        info = response.info;
                }
                toaster.pop('error', '系统提示', info, true);
            }else{
                toaster.pop('success', '系统提示', '恭喜您通过校园星空初步认证，请到认证邮箱激活认证链接！', true);
                $('#certModal').modal('hide');
            }
        })
            .error(function(data, status, headers, config) {
                toaster.pop('error', '系统提示', '请求过程错误', false);
            });
    };

    self.getUserInfo = function () {
        return user_info;
    };
}]);
app.controller('ShowInfoController', function ($scope,userSrv, universityCollegeSrv,$rootScope,$location) {

    $scope.init = function () {
        //userSrv.setUser(user);
        $scope.is_login = $rootScope.is_logined;
        $scope.classes = ['留学', '考证', '就业', '实习', '校园'];
        $scope.degrees = ['本科', '硕士', '博士'];
        var current_year = new Date().getFullYear();
        $scope.entrance_year_list = [];
        for (var i = current_year; i >= current_year - 10; i--)
            $scope.entrance_year_list.push(i);
        //定义学校学院初始化信息
        $scope.show_order = 0; // 0 for no selector, 1 for province, 2 for university, 3 for college
        $scope.prov_list = universityCollegeSrv.getProvList();
        $scope.universities = universityCollegeSrv.getUniversities();
        $scope.colleges = universityCollegeSrv.getColleges();
    };

    //选择学校，选择学院函数；
    $scope.isCurrentOrder = function (order) {
        return order == $scope.show_order;
    };

    $scope.universityClickHandler = function () {
        $scope.show_order = 1;
    };

    $scope.chooseProvince = function (province) {
        universityCollegeSrv.fetchUniversities(province);
        $scope.show_order = 2;
    };

    $scope.chooseUniversity = function (university) {
        $scope.university = university;
        $scope.college = "";
        universityCollegeSrv.fetchColleges(university);
        $scope.show_order = 3;
    };

    $scope.chooseCollege = function (college) {
        $scope.college = college;
        $scope.show_order = 0;
    };

    $scope.collegeClickHandler = function () {
        var currentUniversity = $scope.university;
        if (currentUniversity) {
            universityCollegeSrv.fetchColleges(currentUniversity);
            $scope.show_order = 3;
        }
    };
    //提交认证
    $scope.auth = function() {
        var class2 = $scope.class2.slice(0, 2);
        userSrv.auth($scope.university, $scope.college, $scope.entrance_year,
            $scope.degree, $scope.university_mail, $scope.class2);
    };
    //认证窗口
    $scope.showCert = function() {
        userSrv.getAuth().success(function(data, status, headers, config) {
            if(!data.err) {
                $scope.university = data.university;
                $scope.college = data.college;
                $scope.degree = data.degree;
                $scope.entrance_year = data.entrance_year;
                $scope.university_mail = data.university_mail;
            }
            if (!$scope.university) $scope.university = localStorage.university;
            if (!$scope.college) $scope.college = localStorage.college;
            if($scope.is_login){
                $('#certModal').modal('show');
            }else {
                window.location.href="#/home?return_url=/info";
            }

        });
    };

});
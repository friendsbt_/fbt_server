/*********************************************************************************
 *     File Name           :     question-detail-controller.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-10-31 09:07]
 *     Last Modified       :     [2016-01-21 19:57]
 *     Description         :     问题答案详情的Controller
 **********************************************************************************/

app.controller('QuestionAnswerDetailController', ['$scope', 'QuestionAnswerSrv', '$routeParams', 'QuestionAnswerDetailSrv', 'ThankSrv', 'rte', '$location', '$rootScope', 'toaster', 'share', 'User', 'HotLatestQuestionSrv', '$anchorScroll', '$timeout',
  function($scope, QuestionAnswerSrv, $routeParams, QuestionAnswerDetailSrv, ThankSrv, rte, $location, $rootScope, toaster, share, User, HotLatestQuestionSrv, $anchorScroll, $timeout) {

    $scope.init = function() {
      $scope.edit = $location.search().edit;
      if ($scope.edit) {
        $scope.editAnswerId = $location.search().answerId;
      }

      $scope.isLogined = User.logined();
      $scope.url = $location.url();

      $scope.questionId = $routeParams.questionId;
      $scope.fetchQuestionAnswerDetail();

      //$scope.currentPage = 0;
      //$scope.totalPage = -1;

      $scope.changeHotClickNum = 0; //用来记录点了几次换一换
      $scope.changeLatestClickNum = 0;
      $scope.replyFormData = {
        question_id: $scope.questionId,
        content: '',
        need_anonymous: 0,
      };
      //rte.defaultInit();

    };

    $scope.getContent = rte.getContent;
    $scope.getRawContent = rte.getRawContent;



    $scope.fetchMoreAnswer = function() {

      QuestionAnswerDetailSrv.fetchMoreAnswer($scope.questionId,$scope.currentPage + 1).then(function() {

        $scope.qaList = QuestionAnswerDetailSrv.qaList;
        $scope.currentPage = QuestionAnswerDetailSrv.currentPage;
        $scope.totalPage = QuestionAnswerDetailSrv.totalPage;

      });
    };

    $scope.fetchQuestionAnswerDetail = function() {
      QuestionAnswerDetailSrv.fetchQuestionAnswerDetail($scope.questionId).then(function() {
        $scope.qaList = QuestionAnswerDetailSrv.qaList;
        $scope.question = QuestionAnswerDetailSrv.question;
        $scope.currentPage = QuestionAnswerDetailSrv.currentPage;
        $scope.totalPage = QuestionAnswerDetailSrv.totalPage;
        $scope.showedLatestQuestions = HotLatestQuestionSrv.showedLatestQuestions; //显示的最新问题
        $scope.showedHotQuestions = HotLatestQuestionSrv.showedHotQuestions; //显示的最热问题

        if (!$scope.changeHotClickNum) { //只有在第一次自动切换
          $scope.changeHot();
          $scope.changeLatest();
        }

        $timeout(function() {
          $anchorScroll($location.hash());
        }, 500);
        //}

      });
    };

    $scope.changeHot = function() {
      HotLatestQuestionSrv.changeHot($scope.changeHotClickNum);
      $scope.changeHotClickNum += 1;
      $scope.showedHotQuestions = HotLatestQuestionSrv.showedHotQuestions; //显示的最热问题
    };

    $scope.changeLatest = function() {
      HotLatestQuestionSrv.changeLatest($scope.changeLatestClickNum);
      $scope.changeLatestClickNum += 1;
      $scope.showedLatestQuestions = HotLatestQuestionSrv.showedLatestQuestions; //显示的最新问题
    };

    $scope.postAnswer = function() {
      if ($scope.edit) {
        $scope.updateAnswer();
      } else {
        $scope.deployAnswer();
      }
    };

    $scope.deployAnswer = function() {
      var content = $scope.getContent();
      if (QuestionAnswerDetailSrv.checkAnswerContent(content)) {
        QuestionAnswerDetailSrv.deployAnswer($scope.questionId, content, $scope.replyFormData.need_anonymous);
      }
    };



    $scope.updateAnswer = function() {
      var content = $scope.getContent();
      if (QuestionAnswerDetailSrv.checkAnswerContent(content)) {
        QuestionAnswerDetailSrv.updateAnswer($scope.editAnswerId, content, $scope.replyFormData.need_anonymous).then(function() {
          for (var i = 0; i < $scope.qaList.length; i++) {
            if (i && $scope.qaList[i].best_answer.id === $scope.editAnswerId) {
              $scope.qaList[i].best_answer.content = content;
              break;
            }
          }
          $location.hash($scope.editAnswerId);
          $anchorScroll();
        });
      }
    };


  }
]);

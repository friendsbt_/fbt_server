/**
 * Created by yulei on 2015/9/22.
 */
app.controller('QuestionDetailController',['$scope','$routeParams','detailsSrv', function($scope){
    
    $scope.tabValue = -1;
    $scope.tab2Value = function(tabId) {
        $scope.tabValue = tabId;
    }
}]);

app.controller('QuestionClassController', ['$scope','$http', 'toaster',function($scope, $http,toaster){


    $scope.university = "";
    $scope.college = "";
    $scope.course_name = "";
    $scope.title = "";
    $scope.content = "";
    $scope.course_id = "";

    $scope.question_list = {};

    $scope.view_question = {};

    $scope.tabVal = 2;

    $scope.page = 1;
    $scope.sort_by = 0;
    $scope.courseid = "1";

    $scope.question_Id = "";



    $scope.titleVal = 0;
    $scope.changeTitle = function(titleId, questionId){
        $scope.titleVal = titleId;
        if(titleId == 2){
            $scope.question_Id = questionId;
        }
    };

    //初始化问题列表，所有问题
    if($scope.tabVal == 2 ){
        var data = {
            page:$scope.page,
            course_id:$scope.courseid,
            sort_by: $scope.sort_by
        };
        var req = {
            url: '/course/question/list',
            method: 'GET',
            params: data,
        };
        $http(req).success(function(response){
            if(response.err == 0){
                $scope.question_list = response.question_list;
            }
            else {
                console.log(data);
            }
        }).error(function(err){
            console.log("问题列表出现问题如下：" + err);
        });
    };
    $scope.tabTo = function (tabId){
        $scope.tabVal = tabId;
    };

    //查看一个具体的问题
    $scope.viewQuestion = function(tabId, questionId){
        $scope.question_Id = questionId;
        $scope.tabVal = tabId;
        var req = {
            url: '/course/question/view',
            method: 'GET',
            params: {_id: questionId}
        };
        $http(req).success(function(data){
            if(data.err == 0){
                $scope.view_question = data.question;
                $scope.view_question._id = questionId;
            }else{
                console.log(data);
            }
        });
    };

    //发布一个问题
    $scope.publishQuestion = function (){
        var question = {
            university : $scope.university,
            college : $scope.college,
            course_name : $scope.course_name,
            title : $scope.title,
            content : $scope.content,
            course_id :"1"
        };
        $http({
            url: '/course/question/issue',
            method: 'POST',
            xsrfCookieName: '_xsrf',
            xsrfHeaderName: 'X-CSRFToken',
            data : question,
        })
            .success(function(response){
                $('#showBox').modal('hide');
                if(response.err == 0){
                    toaster.pop('success',"系统提示", "您已成功提交该问题", true);
                }
                else if(response.err == 1){
                    toaster.pop('error', "系统提示", "对不起，您需要登录后才能发布问题", true);
                }
            });
    }

//    回答某一个问题
    $scope.answer = function (){
        var answer = {
            content: $scope.content2,
            _id: $scope.question_Id
        };
        $http({
            url: '/course/question/reply',
            method: 'POST',
            xsrfCookieName: '_xsrf',
            xsrfHeaderName: 'X-CSRFToken',
            data: answer
        }).success(function(response){
            $("#showBox").modal("hide");
            if(response.err == 0){
                toaster.pop('success', "系统提示", "您已成功回答该问题", true);

            };
        });
    };

//    评论某一个回答
    $scope.judge = function(){

    };

//    赞同某一个回答
    $scope.prosClick = function (replyId){
        var req = {
            url: '/course/question_reply/pros',
            method: 'GET',
            params: {reply_id: replyId, question_id: $scope.question_Id}
        };

        $http(req).success(function(response){
           if(response.err == 0 && response.val == 1){
               toaster.pop('success', "系统提示", "您的操作成功，赞同数+1",true);
           }else {
               toaster.pop('error', "系统提示", "操作失败",true);
           }
        });
    }
//    反对某一个回答

    $scope.consClick = function (replyId){
        var req = {
            url: '/course/question_reply/cons',
            method: 'GET',
            params: {reply_id: replyId, question_id: $scope.question_Id}
        };
        $http(req).success(function(response){
            if(response.err == 0 && response.val == 1){
                toaster.pop('success', "系统提示", "您的操作成功，反对数+1",true);
            }else {
                toaster.pop('error', "系统提示", "操作失败",true);
            }
        });
    }
}]);

app.controller('MyQuestionController', ['$scope', function($scope){
    $scope.tabVal = 1;
    $scope.tabTo = function(tabId){
        $scope.tabVal = tabId;
    }

    $scope.tab2Val = -1;
    $scope.tab2To = function(tabId){
        $scope.tab2Val = tabId;
    }
}])
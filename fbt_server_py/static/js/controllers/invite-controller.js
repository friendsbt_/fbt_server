/*********************************************************************************
 *     File Name           :     invite-controller.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-11-01 16:39]
 *     Last Modified       :     [2016-01-13 19:25]
 *     Description         :     邀请回答的controller
 **********************************************************************************/

app.controller('InviteReplyController', ['$scope', 'InviteReplySrv', 'toaster', 'AskQuestionSrv',
  function($scope, InviteReplySrv, toaster, AskQuestionSrv) {

    $scope.myFollowing = InviteReplySrv.myFollowing;
    $scope.recommendedUsers = InviteReplySrv.recommendedUsers;
    $scope.invitedUsers = InviteReplySrv.invitedUsers; //用这个是与发布问题的AskQuesitonSrv的表单进行绑定，这样数据就直接传过去了
    $scope.inviteFB = '';

    $scope.init = function() {

      //模态框显示出来时候执行
      angular.element('#inviteReplyModal').on('show.bs.modal', function() {
        //如果标签没有变化，那么就不重新获取推荐人了
        if (!angular.equals($scope.lastTags, AskQuestionSrv.formdata.tags)) {
          angular.copy(AskQuestionSrv.formdata.tags, $scope.lastTags);
          InviteReplySrv.autoRecommendUsers(AskQuestionSrv.formdata.class2, AskQuestionSrv.formdata.tags);
        }
        //如果已经取过关注的人之后，则不再获取
        if (!$scope.alreadyGetMyFollowing) {
          InviteReplySrv.fetchMyFollowers();
          $scope.alreadyGetMyFollowing = true;
        }

      });


    };



    $scope.addInvitedUser = function(user) {

      var flag = false;

      angular.forEach($scope.recommendedUsers, function(value, key) {
        angular.forEach(value, function(value2, key2) {
          if (value2.uid === user.uid) {
            value2.checked = true;
          }
        });
      });
      angular.forEach($scope.invitedUsers, function(value, key) {
        if (value.uid === user.uid) {
          flag = true;
        }
      });
      if ($scope.invitedUsers.length === 5) {
        toaster.pop('error', '一次最多邀请5个人哦！', '', true);
        flag = true;
      }
      if (!flag) {
        $scope.invitedUsers.push(user);
      }

    };


    $scope.removeInvitedUser = function(index) {
      InviteReplySrv.removeInvitedUser(index);
    };


    $scope.confirmInvite = function() {
        AskQuestionSrv.setInviteFB($scope.inviteFB );
    };

    $scope.cancleInvite = function() {
      //$scope.invitedUsers.length = 0;
      for (var i = 0; i < $scope.invitedUsers.length; i++) {
        $scope.removeInvitedUser(i);
      }

    };

    $scope.inviteUserSwitch = function(user) {
      if (user.checked) {
        $scope.addInvitedUser(user);
      } else if (!user.checked) {
        for (var i = 0; i < $scope.invitedUsers.length; i++) {
          if ($scope.invitedUsers[i].uid === user.uid) {
            $scope.invitedUsers.splice(i, 1);

          }
        }


      }

    };

    $scope.showTabPage = function(event) {
      event.preventDefault();
      $(this).tab('show');
    };

    /*// deprecated
        $scope.serach = function(text) {

          if (text === '') {
            $scope.filteredFollowing.length = 0;
            angular.extend($scope.filteredFollowing, $scope.myFollowing);
            return;
          }
          $scope.filteredFollowing.length = 0;
          var reg = new RegExp(text, 'gi');
          for (var i = 0; i < $scope.myFollowing.length; i++) {
            var s = $scope.myFollowing[i].nick_name;
            if (reg.test(s)) {
              $scope.filteredFollowing.push($scope.myFollowing[i]);
            }
          }

        };
        */
  }
]);

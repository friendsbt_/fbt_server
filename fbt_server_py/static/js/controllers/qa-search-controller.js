/*********************************************************************************
 *     File Name           :     qa-search-controller.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-11-03 20:15]
 *     Last Modified       :     [2015-11-13 01:08]
 *     Description         :     问答搜索的controller
 **********************************************************************************/


app.controller('QASearchController', ['$scope', 'QuestionAnswerSrv', 'QASearchSrv','share',
  function($scope, QuestionAnswerSrv, QASearchSrv,share) {
    $scope.search = function(keyword, needHotKeyword, page) {
      QASearchSrv.search(keyword, needHotKeyword, page).then(function() {
        $scope.qaList = QASearchSrv.response.resource_list;
        $scope.currentPage = QASearchSrv.response.current_page;
        $scope.totalPage = QASearchSrv.response.total_page;
      });
    };


    $scope.showQuestonThanksModal = function(id) {
      ThankSrv.showQuestonThanksModal(id);
    };



    $scope.questionThumbUp = function(id) {
      QuestionAnswerSrv.questionThumbUp(id);
    };


    $scope.questionCollect = function(id) {
      QuestionAnswerSrv.questionCollect(id);
    };
    //添加关注（人）
    $scope.follow = function(id) {
      QuestionAnswerSrv.follow(id);
    };

    $scope.removeHtmlTag = function(html){
        return  removeHtmlTag(html);
    };


    $scope.share = function(content, mode) {
      var url;
      if (mode === 'answer') {

        url = 'http://test.friendsbt.com/#/qadetail/' + $scope.question.id;
        url += '#' + content.id;
      } else if (mode === 'question') {

        url = 'http://test.friendsbt.com/#/qadetail/' + content.id;
      }

      share.show({
        title: $scope.question.title,
        desc: removeHtmlTag(content.content),
        url: url,
        weixin: 'http://weixin.qq.com/r/MXULEy7E-XOCrWpw9yAj',

      });
    };

  }
]);

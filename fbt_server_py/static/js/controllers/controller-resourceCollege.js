/* vim: set sw=2 ts=2 : */
'use strict';
//college是学校内的学院，不要理解错了
/*
    schoolStruct{
      img://学校图片 
      name://学校名称
      resourceNum://资源总数
      hot://人气
    }
*/
Function.prototype.method = function(name, func) {
  this.prototype[name] = func;
  return this;
};

Function.method('curry', function() {
  var slice = Array.prototype.slice,
    args = slice.apply(arguments),
    that = this;

  return function() {
    return that.apply(null, args.concat(slice.apply(arguments)));
  };
});

app.service('schoolsSrv', function($rootScope, $http, loading) {
  var schools = [];
  var page = {
    total_page: 0,
    current_page: 0,
    school_type: 1,
    noResult: false,
    noPagination: false,
    max_size: 5,
    total_items: 0,
    items_per_page: 0,
    pageChanged: null
  };

  this.setSchoolType = function(tab) {
    page.school_type = tab;
  };

  function handle_fetch_results(response) {
    if (0 == response.err) {
      schools.length = 0;
      angular.forEach(response.resource_list, function(v, i) {
        schools.push(v);
      });
      page.total_page = response.total_page;
      //page.current_page = response.current_page;
      if (page.total_page > 1) {
        page.noPagination = true;
        page.items_per_page = schools.length;
        page.total_items = page.total_page * page.items_per_page;
      } else {
        page.noPagination = false;
      }
      if (schools.length)
        page.noResult = false;
      else
        page.noResult = true;
    }
  };

  this._fetchSchoolsByTag = function() {
    var tab = page.school_type;
    var is_985 = 0;
    var is_211 = 0;

    if (1 == tab)
      is_985 = 1;
    else if (2 == tab)
      is_211 = 1;

    loading.show();
    $http({
        url: '/resource/list',
        method: 'GET',
        params: {
          is_211: is_211,
          is_985: is_985,
          page: page.current_page
        }
      })
      .success(function(response) {
        loading.hide();
        handle_fetch_results(response);
      })
      .error(function(data, status, headers, config) {
        // 失败处理
        loading.hide();
        alert('页面加载失败，请重试！')
      });
  };

  this.fetchSchoolsByTag = function() {
    page.current_page = 1;
    this._fetchSchoolsByTag();
    page.pageChanged = this._fetchSchoolsByTag;
  };

  this._fetchSchoolsBySearch = function(keyword) {
    //var tab = page.school_type;
    var is_211 = 0;
    var is_985 = 0;
    /*switch (tab) {
      case 1:
        is_211 = 0;
        is_985 = 1;
        break;
      case 2:
        is_211 = 1;
        is_985 = 0;
        break;
    };*/
    loading.show();
    $http({
        url: '/resource/search',
        method: 'GET',
        params: {
          is_211: is_211,
          is_985: is_985,
          keyword: keyword,
          page: page.current_page
        }
      })
      .success(function(response) {
        loading.hide();
        handle_fetch_results(response);
      })
      .error(function(data, status, headers, config) {
        // 失败处理
        loading.hide();
        alert('搜索失败，请重试！')
      });
  };

  this.fetchSchoolsBySearch = function(keyword) {
    page.current_page = 1;
    this._fetchSchoolsBySearch(keyword);
    page.pageChanged = this._fetchSchoolsBySearch.curry(keyword);
  };

  this.getSchools = function() {
    if (0 == schools.length)
      this.fetchSchoolsByTag(1);
    return schools;
  };

  this.getPage = function() {
    page.noResult = false;
    return page;
  };
}); //学校列表服务

app.controller('schoolsController', ['$scope', '$rootScope', '$location', 'schoolsSrv', function($scope, $rootScope, $location, schoolsSrv) {
  //  全部高校: 0,  985名校: 1, 211大学: 2

  $scope.isCurrentSchoolType = function(schoolType) {
    return schoolType === $scope.page.school_type;
  };

  $scope.search = function() {
    var query = $scope.query;
    $scope.page.school_type = 4;
    schoolsSrv.fetchSchoolsBySearch(query);
  };

  $scope.enter = function(ev) {
    if (ev.keyCode == 13) {
      $scope.search();
    }
  };

  init();

  function init() {
    $scope.schools = schoolsSrv.getSchools();
    $scope.page = schoolsSrv.getPage();
  }; //初始化函数，加载默认的学校

  $scope.viewCollegeResourceClickHandler = function(university, iconUrl) {
    $rootScope.currentUniversityIconUrl = iconUrl
    $location.path(['/schools', university].join('/'));
  };

  $scope.fetchSchoolsByTag = function(tab) {
    schoolsSrv.setSchoolType(tab);
    schoolsSrv.fetchSchoolsByTag();
  }; // fetch schools by tag
}]); //schoolsController end

/*
  collegeStruct{
    img://学院图片 
    name://学院名称
    resourceNum://资源总数
    hot://人气
  }
*/
app.service('collegesSrv', function($rootScope, $http, $location, loading) {
  var college_resource_num = [];
  var current_college_course_list = [];
  var current_course_id = null;
  var page = {
    university: '',
    current_college: '',
    university_resource_num: 0,
    current_college_course_total_page: 0,
    noPagination: false,
    current_page: 0,
    max_size: 5,
    total_items: 0,
    items_per_page: 0,
    pageChanged: null
  };

  this.get_current_course_id = function() {
    return current_course_id;
  };

  this.set_university_college = function(university, college) {
    if (university !== page.university) {
      page.university = university;
      this.fetch_colleges();
    }
    if (college && college !== page.current_college)
      this.fetch_one_college(college);
  };

  this.get_college_resource_num = function() {
    return college_resource_num;
  };

  this.get_current_college_course_list = function() {
    return current_college_course_list;
  };

  this.get_page = function() {
    return page;
  };

  this.fetch_colleges = function() {
    loading.show();
    $http({
        url:'/resource/university',
        method: 'GET',
        params: {
          university: page.university
        }
      })
      .success(function(response) {
        loading.hide();
        if (0 == response.err) {
            college_resource_num.length = 0;
            angular.forEach(response.college_resource_num, function(v, i) {
              college_resource_num.push(v);
            });
            current_college_course_list.length = 0;
            angular.forEach(response.current_college_course_list, function(v, i) {
              current_college_course_list.push(v);
            });
            page.current_college = response.current_college;
            page.pageChanged = _fetch_one_college.curry(page.current_college);
            
            page.university_resource_num = response.university_resource_num;
            page.current_page = 1;
            page.current_college_course_total_page = response.current_college_course_total_page;
            if (page.current_college_course_total_page > 1) {
              page.noPagination = true;
              page.items_per_page = current_college_course_list.length;
              page.total_items = page.current_college_course_total_page * page.items_per_page;
            } else {
              page.noPagination = false;
            }
        }
      })
      .error(function(data, status, headers, config) {
        // 失败处理
        loading.hide();
        alert('页面加载失败，请重试！');
      });
  };

  // 查看college课程
  var _fetch_one_college = function(college) {
    loading.show();
    $http({
        url: '/resource/college',
        method: 'GET',
        params: {
          university: page.university,
          college: college,
          page: page.current_page
        }
      })
      .success(function(response) {
        loading.hide();
        if (0 == response.err) {
          page.current_page = response.current_page;
          page.current_college_course_total_page = response.total_page;
          current_college_course_list.length = 0;
          angular.forEach(response.course_list, function(v, i) {
            current_college_course_list.push(v);
          });
          page.current_college = college;
          if (response.total_page > 1) {
            page.noPagination = true;
            page.items_per_page = current_college_course_list.length;
            page.total_items = response.total_page * page.items_per_page;
          } else {
            page.noPagination = false;
          }
        }
      })
      .error(function(data, status, headers, config) {
        // 失败处理
        loading.hide();
        alert('页面加载失败，请重试！');
      });
  };

  this.fetch_one_college = function(college) {
    page.current_page = 1;
    _fetch_one_college(college);
    page.pageChanged = _fetch_one_college.curry(college);
  }

  this.selectCollege = function(college) {
    //collegesSrv.fetch_one_college(college);
    $rootScope.scroll_area_top = $('#scroll_area').scrollTop();
    $location.path(['/schools', page.university, college].join('/'));
  };

  this.viewCollegeResourceDetailsClickHandler = function(course, course_id) {
    //collegesSrv.fetch_course_resource(course);
    var course_name = (course.indexOf('/') === -1? '0' + course: '1' + Base64.encode(course));
    current_course_id = course_id;
    $location.path(['/schools', page.university, page.current_college, course_name].join('/'));
  };
}); //学院列表服务

app.controller('collegesController', ['$scope', '$rootScope', '$location', '$routeParams', 'collegesSrv', 'User','uploadSrv',
  function($scope, $rootScope, $location, $routeParams, collegesSrv, User, uploadSrv) {

  $scope.isCurrentCollege = function(college) {
    return college === $scope.page.current_college;
  };

  $scope.upLoad = function(university, college, course){
    User.login( function() {
        if(uploadSrv.uploader.state == plupload.STOPPED){
            uploadSrv.refreshUploader();
        }
        uploadSrv.setCourseInfo(university, college, course);
        $('#uploadModal').modal("show");
    });
    // var is_login = User.logined();
    // if(is_login == true){
    //  get token...
    // }
};

    $scope.collegeFilter= function(college_resource) {
       if(!$scope.query) return true;
        return lcs($scope.query,college_resource.college);
    };
  init();

  function init() {
    $scope.currentUniversityIconUrl = $rootScope.currentUniversityIconUrl;
    collegesSrv.set_university_college($routeParams.university, $routeParams.college);
    $scope.college_resource_num = collegesSrv.get_college_resource_num();
    $scope.current_college_course_list = collegesSrv.get_current_college_course_list();
    $scope.page = collegesSrv.get_page();
    $scope.selectCollege = collegesSrv.selectCollege;
    $scope.viewCollegeResourceDetailsClickHandler = collegesSrv.viewCollegeResourceDetailsClickHandler;
  };
}]);

app.service('coursesSrv', function($rootScope, $http, loading) {
  var current_course_resource_list = [];
  var page = {
    university: '',
    current_college: '',
    current_course: '',
    current_course_resource_total_page: 0,
    noPagination: false,
    current_page: 0,
    max_size: 5,
    total_items: 0,
    items_per_page: 0,
    pageChanged: null
  };

  this.get_current_course_resource_list = function() {
    return current_course_resource_list;
  };

  this.get_page = function() {
    return page;
  };

  // 查看课程中的资源
  this._fetch_course_resource = function(university, college, course, course_id) {
    loading.show();
    $http({
        url: '/resource/course',
        method: 'GET',
        params: {
          course_id: course_id,
          page: page.current_page
        }
      })
      .success(function(response) {
        loading.hide();
        if (0 == response.err) {
          page.current_course = course;
          page.university = university;
          page.current_course_resource_total_page = response.total_page;
          page.current_page = response.current_page;

          current_course_resource_list.length = 0;
          angular.forEach(response.resource_list, function(v, i) {
            current_course_resource_list.push(v);
          });
          if (response.total_page > 1) {
            page.noPagination = true;
            page.items_per_page = current_course_resource_list.length;
            page.total_items = response.total_page * page.items_per_page;
          } else {
            page.noPagination = false;
          }
        }
      })
      .error(function(data, status, headers, config) {
        // 失败处理
        loading.hide();
        alert('页面加载失败，请重试！');
      });
  };

  this.fetch_course_resource = function(university, college, course, course_id) {
    if (page.current_page === 0 || course !== page.current_course)
      page.current_page = 1;
    page.current_college = college;
    if (university !== page.university || college !== page.current_college || course !== page.current_course)
      this._fetch_course_resource(university, college, course, course_id);
    page.pageChanged = this._fetch_course_resource.curry(university, college, course, course_id);
  };

}); //学院列表服务

app.controller('coursesController', ['$scope', '$rootScope', '$location', '$routeParams', 'collegesSrv', 'coursesSrv', 'downloadSrv', 'PreviewService', function($scope, $rootScope, $location, $routeParams, collegesSrv, coursesSrv, downloadSrv, PreviewService) {
  $scope.isCurrentCollege = function(college) {
    return college === $scope.page.current_college;
  };

  init();

  function init() {
    $scope.isCommentBtnVisible = true;
    $scope.courseResourceController = $rootScope.courseResourceController;
    $scope.moreRes = coursesSrv.get_current_course_resource_list();
    $scope.college_resource_num = collegesSrv.get_college_resource_num();
    $scope.selectCollege = collegesSrv.selectCollege;
    $scope.showPreviewModal = PreviewService.showPreviewModal;

    var c = $routeParams.course;
    $scope.course_id = collegesSrv.get_current_course_id();
    if (c[0] === '1')
      c = Base64.decode(c.slice(1));
    else
      c = c.slice(1);
    coursesSrv.fetch_course_resource($routeParams.university, $routeParams.college, c, $scope.course_id);
    $scope.page = coursesSrv.get_page();
    if (collegesSrv.get_page().university !== $routeParams.university)
      collegesSrv.fetch_colleges();    
    $scope.getMoreRes = $scope.page.pageChanged;
  };

  $scope.downloadHandler = function(file_id, download_link) {
    downloadSrv.download(file_id, download_link);
  };

    $scope.previewHandler = function(res) {
    downloadSrv.preview(res);
  };


}]);

app.controller('courseResourceController', ['$scope', '$location', '$routeParams', 'coursesSrv', 'downloadSrv', function($scope, $location, $routeParams, coursesSrv, downloadSrv) {
  init();

  function init() {
    $scope.hideSearch = true;
    $scope.moreRes = coursesSrv.get_current_course_resource_list();
    coursesSrv.fetch_course_resource($routeParams.university, $routeParams.college, $routeParams.course, $routeParams.course_id);
    $scope.page = coursesSrv.get_page();
    $scope.getMoreRes = $scope.page.pageChanged;
  };

  $scope.downloadHandler = function(file_id, download_link) {
    downloadSrv.download(file_id, download_link);
  };

    $scope.previewHandler = function(res) {
    downloadSrv.preview(res);
  };
}]);

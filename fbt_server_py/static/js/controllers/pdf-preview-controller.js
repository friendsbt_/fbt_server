var app = angular.module('App', ['pdf']);
app.controller('DocCtrl', ['$scope', function($scope) {

  // $scope.pdfName = 'Relativity: The Special and General Theory by Albert Einstein';
  // $scope.pdfUrl = $routeParams.pdfUrl;
  var pdfUrl = window.location.search.substring(8,window.location.search.length);
  $scope.pdfUrl = pdfUrl;
  $scope.scroll = 0;
  $scope.loading = 'loading';

  $scope.getNavStyle = function(scroll) {
    if (scroll > 100) return 'pdf-controls fixed';
    else return 'pdf-controls';
  }

  $scope.onError = function(error) {
    console.log(error);
  }

  $scope.onLoad = function() {
    $scope.loading = '';
  }

  $scope.onProgress = function(progress) {
    console.log(progress);
  }

}]);
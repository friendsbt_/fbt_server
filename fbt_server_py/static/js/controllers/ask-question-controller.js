/*********************************************************************************
 *     File Name           :     ask-question-controller.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-10-28 20:48]
 *     Last Modified       :     [2016-01-14 21:29]
 *     Description         :     提问问题的controller
 **********************************************************************************/

app.filter('addInputTag',  function(){
  return function(arr,inputTag){
    if(!!arr && !!inputTag) {
      arr.unshift({tag:inputTag,num:1});

    }
    return arr;

  }
});

app.controller('AskQuestionController', ['$scope', 'QuestionAnswerSrv', 'rte', '$http', 'toaster', 'AskQuestionSrv', '$location', '$window', 'InviteReplySrv',
  function($scope, QuestionAnswerSrv, rte, $http, toaster, AskQuestionSrv, $location, $window, InviteReplySrv) {


    $scope.getContent = rte.getContent;
    $scope.invitedUsers = AskQuestionSrv.invitedUsers;

    $scope.autoExtractedTags = AskQuestionSrv.autoExtractedTags; //自动提取的标签
    $scope.allTagsInClass = AskQuestionSrv.allTagsInClass; //一个分类下的所有标签，用来自动补全
    $scope.viewVar = AskQuestionSrv.viewVar; //用来控制视图上的一些元素是否显示，主要有异步前后的更改，所以写在service里边了
    $scope.formdata = AskQuestionSrv.formdata; //表单数据，不解释

    $scope.init = function() {


      $scope.classes = {
        '校园': '校园',
        '考研': '考研',
        '留学': '留学',
        '考证': '考证',
        '就业': '就业',
        '实习': '实习',
      };
      $scope.formdata.class2 = '请选择分类';


      // deprecated,because edit is reliazed in qa-detail page
      //$scope.edit = $location.search().edit;
      //if ($scope.edit) {
      //  $scope.editQuestionId = $location.search().editQuestionId;
      //  $scope.formdata.title = $window.localStorage['edit-question-title'];
      //  $scope.formdata.class2 = $window.localStorage['edit-question-class2'];
      //  $scope.formdata.content = $window.localStorage['edit-question-content'];
      //  angular.extend($scope.formdata.tags, angular.fromJson($window.localStorage['edit-question-tags']));
      //  var invited_users = angular.fromJson($window.localStorage['edit-question-invited_users']);

      //  angular.forEach(invited_users, function(value, key) {
      //    //$scope.formdata.invited_users.push({
      //    //  nick_name: value
      //    //});
      //    $scope.invitedUsers.push({
      //      real_name: value.real_name,
      //      user: value.user
      //    });

      //  });

      //  rte.setEditContent($window.localStorage['edit-question-content']);
      //}




    };




    $scope.selectClass = function(argClass) {
      $scope.formdata.class2 = argClass;
    };


    $scope.removeTag = function(index) {
      AskQuestionSrv.removeTag(index);
    };

    $scope.tagSelect = function(item, model, label) {
      if (label === '选择我输入的') {
        $scope.formdata.tags.push($scope.tagInput);
      }
      $scope.formdata.tags.push(label);
      $scope.tagInput = ''; //选择后就清空已经输入的


    };

    $scope.postQuestion = function() {
      if ($scope.edit) {

        $scope.updateQuestion();
      } else {
        $scope.deploy();

      }

    };

    $scope.updateQuestion = function() {

      var content = $scope.getContent();
      if (content === '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<p>(可选)问题背景、条件等信息，让同学更好地理解你的问题！</p>\n</body>\n</html>') {
        //toaster.pop('error', '请不要发送空内容哦～～～', '', true);
        //return;
        content = '如题!';
      }
      AskQuestionSrv.updateQuestion($scope.editQuestionId, content, $scope.formdata.class2);
    };

    $scope.deploy = function() {
      var content = $scope.getContent();
      if (content === '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<p>(可选)问题背景、条件等信息，让同学更好地理解你的问题！</p>\n</body>\n</html>') {
        content = '如题!';
      }
      $scope.formdata.content = content;
      if (!$scope.formdata.how_much) {
        $scope.formdata.how_much = 10;
      }

      AskQuestionSrv.deploy();

    };




    $scope.autoRecommendUsers = function() {
      $scope.viewVar.isRecommendLoadingShow = true;
      AskQuestionSrv.autoRecommendUsers();
    };

    $scope.autoExtractTags = function() {
        
      if($scope.formdata.class2 === '请选择分类'){
          toaster.pop('warning','请先选择问题分类哦！','',true);
          return;
      }
      var questionTitle = AskQuestionSrv.formdata.title;
      if (!questionTitle) {
        toaster.pop('error', '请输入问题标题哦！', '', true);
        return;
      }
      var questionContent = $scope.getContent();
      if (questionContent === '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<p>(可选)问题背景、条件等信息，让同学更好地理解你的问题！</p>\n</body>\n</html>') {
        questionContent = '如题!';
      }

      $scope.viewVar.isAutoTagLoadingShow = true;
      var questionClass = AskQuestionSrv.formdata.class2;
      AskQuestionSrv.autoExtractTags(questionClass, questionTitle, questionContent);

    };


    $scope.afterFocusOnTagInput = function() {
      if ($scope.lastClass === $scope.formdata.class2) {

        return;
      } else {
        $scope.lastClass = $scope.formdata.class2;
        AskQuestionSrv.fetchTagsByClass($scope.lastClass);

      }

    };



    $scope.showInviteModal = function() {
      if ($scope.formdata.tags.length === 0) {
        toaster.pop('warning', '请先完善标签信息哦！', '', true);
        return;
      }
      angular.element('#inviteReplyModal').modal('show');
    };

    $scope.tagInputKeyPress = function(event) {
      if (event.keyCode === 13) {
        //enter.keycode = 13
        $scope.tagSelect('', $scope.tagInput, $scope.tagInput);

      }

    };

    $scope.removeInvitedUser = function(index) {
      InviteReplySrv.removeInvitedUser(index);

    };







  }
]);

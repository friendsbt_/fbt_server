/* vim: set sw=2 ts=2 : */
'use strict';
app.controller('ctrlHead', function ($scope, $location) {
    $scope.showHeader = function () {
        if ($location.url().indexOf('/home') != -1)
            return false;
        $('.header-invisible').removeClass('header-invisible');
        return true;
    };
});
app.controller('HeaderController', function ($scope, $location, $route, $modal, User, $window, $http, $rootScope,toaster, chatService, universityCollegeSrv) {

    $scope.init = function () {
        $scope.tabList = [
            'schools',
            'star',
            'find'
            //'recommendation',
            //'studysearch',
           // 'fmall',
           // 'rank'
        ];
        $scope.tabIcons = [
            'glyphicon glyphicon-home',
            'glyphicon glyphicon-tree-conifer',
            'glyphicon glyphicon-star',
            'glyphicon glyphicon-yen'
        ];

        $scope.goUserPage = function() {
            $window.open("/user_page?user=" + encode_username($rootScope.username), "_blank");
        };

        $scope.tabNames = {
            'home': '',
            //'recommendation': '推荐',
            'schools': '课程',
            'studysearch': '学霸搜',
            'fmall': '商城',
            'reward': '悬赏',
            'rank': '榜单',
            'ask':'问答',
            'pk':'PK',
            'star': '星空',
            'qahome': '主页',
        };

        $rootScope.message_count = 0;

        if (User.logined()) {
            $http.get('/user/updated_info').success(function (data) {
                if ('err' in data && data['err'] == 0) {
                    $rootScope.message_count = data['message_cnt'];
                    $rootScope.total_coins = data['total_coins'];
                    $rootScope.study_coins = data['study_coins'];
                    $rootScope.userIcon = data['icon'];
                    /*
                    * 以下是为了正则匹配出user_token的值，然后传给消息中心使用
                    * */
                    var pattern = /\buser_token="([^;]*)"/g;
                    var r = pattern.exec(document.cookie);
                    localStorage.r = r[1];
                    //对消息进行处理
                    chatService.messageProcess(data['recent_messages']);
                }
            });
        }
    };


    /*
    $scope.gotoAskbot = function () {
        var askbotURL = "http://ask.friendsbt.com/";
        if (User.logined()) {
            $window.open(askbotURL, '_blank');
        } else {
            User.login(function () {
                setTimeout(function () {
                    $window.open(askbotURL, '_blank');
                }, 2000);
            });
        }
        $scope.toggleNavbar();
    };
    */
    
    $scope.goToGame = function() {
        var userCoins = $rootScope.total_coins;
        var URL;
        var gameURL;
        var temURL = $window.location.href;
        //链接跳转
        if((temURL.indexOf("local") > 0)||(temURL.indexOf("127") > 0)){
            URL = "http://localhost:7777/?sid=";
        };
        if (temURL.indexOf("test") > 0){
            URL = "http://game.friendsbt.com:3000/?sid=";
        };
        if(temURL.indexOf("study") > 0){
            URL = "http://game.friendsbt.com:7777/?sid=";
        }
        gameURL = URL +  getCookie("sid");
        if (User.logined()) {
            //获取sid的值
            var temSid = gameURL.split("?")[1].split("=")[1];
            if(userCoins < 100){
                toaster.pop('error', "对不起", "您的F币小于100，暂时不能进入游戏", true);
            }else if(temSid == "undefined"){
                toaster.pop('error', "对不起", "请退出账号重新登录", true);
            }
            else{
                $window.open(gameURL, '_blank');
            }
        }
        else {
            User.login(function() {
                setTimeout( function() {
                    var tempSid = gameURL.split("?")[1].split("=")[1];
                    if(tempSid == "undefined")return;
                    $window.open(gameURL, '_blank');
                }, 1000);
            });
        }
        $scope.toggleNavbar();
    };


    $scope.login = function () {
        User.login(null);
    };

    $scope.gotoLogin = function(){
        $location.url("/home?return_url="+$location.url());
    };

    $scope.logout = function () {
        $('#logout-modal').modal({backdrop: true, keyboard: true});
    };

    $scope.changePassword = function () {
        $modal.open({
            backdrop: true,
            keyboard: true,
            animation: true,
            templateUrl: 'changePassword.html',
            controller: 'changePasswordModalCtrl'
        });
    };

    $scope.changeUniversityCollege = function () {
        $modal.open({
            backdrop: true,
            keyboard: true,
            animation: true,
            templateUrl: 'static/partials/changeUniversityCollege.html',
            controller: 'changeUniversityCollegeModalCtrl'
        });
    };

    $scope.isCurrentTab = function (tab) {
        var currentTab = $location.url().split('?')[0].replace('/', '').replace(/-.+/, '');
        currentTab = currentTab.split('/')[0];
        return currentTab == tab;
    };

    //$scope.enter = function (ev) {
    //    if (ev.keyCode == 13) {
    //        $scope.searchTypeChange();
    //    }
    //};
    //
    //$scope.searchTypeChange = function () {
    //    if ($scope.keyword)
    //        if ($scope.search_type == 'resource')
    //            $location.path('/search').search({keyword: $scope.keyword});
    //        else
    //            $location.path('/search_course').search({keyword: $scope.keyword});
    //};
    $scope.toggleNavbar = function(){
        angular.element('.navbar-collapse.collapse.in').removeClass('in');
    };
    // $scope.selectHeaderTab = function(tab) {
    //   if(tab==="university"){
    //     $location.path('university')
    //   }
    // };


    /** register info modal controllers **/
    $scope.initRegisterInfo = function() {
        //定义学校学院初始化信息
        $scope.show_order = 0; // 0 for no selector, 1 for province, 2 for university, 3 for college
        $scope.prov_list = universityCollegeSrv.getProvList();
        $scope.universities = universityCollegeSrv.getUniversities();
        $scope.colleges = universityCollegeSrv.getColleges();

        var loginCallback = function(username) {
          if (username.length > 0) {
            $scope.updateInfoUsername = username;
            toaster.pop('success','系统提示','您刚从FBT客户端赶过来，请稍事休息完善学校学院等信息，以便对您提供更好的服务。', true);
            $scope.fetchProvince(function() {
                if ($scope.province)
                    $scope.chooseProvince($scope.province);
                $('#registerModal').modal('show');
            });
          }
        };
        var params = $location.search();
        if ('uid' in params && 'token' in params) {
            User.login_from_fbt(params['uid'], params['token'], loginCallback);
        } else {
            var searchParams = $window.location.search.substr(1).split('&');

            var uid = '', token = '';
            for (var i in searchParams) {
                var pair = searchParams[i].split('=');
                if (pair[0] == 'uid') {
                    uid = pair[1];
                } else if (pair[0] == 'token') {
                    token = pair[1];
                }
            }
            if (uid && uid.length > 0 && token && token.length > 0) {
                User.login_from_fbt(uid, token, loginCallback);
            }
        }
    };

    //选择学校，选择学院函数；
    $scope.isCurrentOrder = function (order) {
        return order == $scope.show_order;
    };

    $scope.universityClickHandler = function () {
        $scope.show_order = 1;
    };

    $scope.chooseProvince = function (province) {
        universityCollegeSrv.fetchUniversities(province);
        $scope.show_order = 2;
    };

    $scope.chooseUniversity = function (university) {
        $scope.university = university;
        $scope.college = "";
        universityCollegeSrv.fetchColleges(university);
        $scope.show_order = 3;
    };

    $scope.chooseCollege = function (college) {
        $scope.college = college;
        $scope.show_order = 0;
    };

    $scope.collegeClickHandler = function () {
        var currentUniversity = $scope.university;
        if (currentUniversity) {
            universityCollegeSrv.fetchColleges(currentUniversity);
            $scope.show_order = 3;
        }
    };
    $scope.fetchProvince = function (callback) {
        if ($scope.province) {
            if (callback) callback();
            return;
        }
        var req = {
            url: 'http://api.map.baidu.com/location/ip?callback=JSON_CALLBACK',
            method: 'JSONP',
            params: {ak: 'GuAFsALAn9t53VzZYvkgz5Pc'}
        };
        $http(req).success(function (response) {
            if (response.status == 0) {
                $scope.province = response.address.split('|', 3)[1];
            }
            if (callback) callback();
        }).error(function (err) {
            if (callback) callback();
        });
    };
    //用户完善信息函数
    $scope.infoHandler = function () {
        var userData = {
            university: $scope.university,
            college: $scope.college,
            name: htmlencode($scope.real_name),
            gender: $scope.gender,
            nick: htmlencode($scope.real_name),//htmlencode($scope.nick_name),
            user: $scope.updateInfoUsername
        };
        var req = {
            url: '/user/info/change',
            method: 'POST',
            data: userData,
            xsrfCookieName: '_xsrf',
            xsrfHeaderName: 'X-CSRFToken',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        };
        $http(req).success(function (response) {
            if (response.err == 0) {
                //去掉黑色边框
                $('.modal-backdrop').hide();
                $('.modal-open').css('overflow', 'auto').css('padding-right', '0px');
                $('#registerModal').modal('hide');
                toaster.pop('success','系统提示','已完善信息，欢迎来到校园星空！', true);
                $window.location.reload();
            } else toaster.pop('error', '系统提示', response.info, false);
        }).error(function (err) {
            //console.log('err is ' + err);
            toaster.pop('error', '系统提示', "网络出了点故障，请重试", false);
        });
    };
    /* end of register info modal */
});

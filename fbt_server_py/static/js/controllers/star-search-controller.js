/**
 * Created by yulei on 2015/10/13.
 */

app.controller('StarSearchController', ['$scope', '$rootScope', '$http', 'toaster','QASearchSrv','$location','QuestionAnswerSrv','$routeParams',
    function ($scope, $rootScope, $http, toaster, QASearchSrv, $location, QuestionAnswerSrv, $routeParams) {
        $scope.init = function () {
            $scope.searchClass = "课程";
            $scope.searchClasses = ['问答','学霸','资源','课程'];
            $scope.hotQuestionType = 0;//搜索问题时是否排行，0表示不排行，1表示排行
            $scope.serarchUsers = []; //搜索用户初始为空
            $scope.qaList = [];//搜索问题为空
            $scope.currentPage = 1;
            $scope.totalPage = 1;
            $scope.keyword = $routeParams.keyword;//接受地址栏的关键字参数
            $scope.searchClass = $routeParams.type; //地址栏的搜索类型
            $scope.starSearch();
        };

        //判断当前选项是否激活
        $scope.isCurrentTab = function(id){
            return id == $scope.searchClass;
        };

        $scope.searchVal = function (id) {
            $scope.searchClass = id;
            if($scope.keyword){
                $location.search({keyword:$scope.keyword,type:$scope.searchClass});
            }
        };

        //星空搜索函数
        $scope.starSearch  = function(){
            if($scope.keyword){
                switch($scope.searchClass){
                    case '学霸': $scope.searchUserHandler();break;
                    case '问答': $scope.searchQuestionHandler($scope.keyword, $scope.hotQuestionType, $scope.currentPage) ;break;
                    case '资源': $location.path('/studysearch_resource').search({keyword: $scope.keyword,type:$scope.searchClass});break;
                    default :    $location.path('/studysearch_course').search({keyword: $scope.keyword,type:$scope.searchClass});break;
                }
            }
        };

        //输入框响应函数
        $scope.enterSearch = function (ev) {
            if (ev.keyCode == 13) {
                $scope.starSearch();
            }
        };

        //星空搜索问题函数
        $scope.searchQuestionHandler = function(keyword, hotQuestionType, page){
            if(keyword){
                $scope.searchUsers = [];
                QASearchSrv.search(keyword, hotQuestionType, page).then(function() {
                    $scope.qaList = QASearchSrv.response.resource_list;
                    $scope.currentPage = QASearchSrv.response.current_page;
                    $scope.totalPage = QASearchSrv.response.total_page;
                    if($scope.qaList.length < 1)$scope.noResult = true;
                    else $scope.noResult = false;
                });
            };
        };
        //星空搜索用户函数
        $scope.searchUserHandler = function () {
            if ($scope.keyword) {
                $scope.qaList = [];
                var req = {
                    url: '/user/search',
                    method: 'GET',
                    params: {
                        page: $scope.currentPage,
                        keyword: $scope.keyword
                    }
                };
                $http(req).success(function (response) {
                    if(response.err == 0){
                        $scope.searchUsers = response.total_page;
                        $scope.currentPage = response.current_page;
                        $scope.totalPage = response.user_list;
                        //console.log(response);
                        //判断列表是否为空
                        if($scope.searchUsers.length<1)$scope.noResult = true;
                        else $scope.noResult = false;
                    }else{
                        toaster.pop('error','系统提示','输入搜索内容有误，请重新输入',true);
                    }
                }).error(function (err) {
                    toaster.pop('error','系统提示','请求数据出现问题，请稍后重试',true);
                })
            };
        };

        //搜索学霸后关注用户
        $scope.follow = function(id){
            QuestionAnswerSrv.follow(id);
        }

    }]);




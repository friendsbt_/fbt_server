//回调的文件状态，要给file.status
//var uploadStatus = {
//  ARGUMENT_ERROR: -10000,
//  TIME_OUT: -10001,
//  SERVER_INTERNAL_ERROR: -10002,
//  UNAUTHENTICATED: -10003,
//  OTHER_ERROR: -100010,
//};
var uploadStatus = {
  ARGUMENT_ERROR: 'ARGUMENT_ERROR',
  TIME_OUT: 'TIME_OUT',
  SERVER_INTERNAL_ERROR: 'SERVER_INTERNAL_ERROR',
  UNAUTHENTICATED: 'UNAUTHENTICATED',
  OTHER_ERROR: 'OTHER_ERROR',
  ALREADY_EXISTS_RESOURCE :'ALREADY_EXISTS_RESOURCE',
};
var resourceTags = [
  '作业和答案',
  '课堂笔记',
  '往届考题',
  '电子书或文献',
  '课程课件',
  '学习心得',
  'TED',
  '软件教学',
  '其他'
];

function formatSpeed(speed) {
  //格式化上传速度
  if (speed < 1024) {
    return speed + 'B/S';
  } else if (speed >= 1024 && speed < 1048576) {
    return (speed / 1024).toFixed(2) + 'KB/S';
  } else {
    return (speed / 1048576).toFixed(2) + 'MB/S';
  }
}

function getFileName(full_file_name) {
  var pointIndex = full_file_name.lastIndexOf(".");
  return full_file_name.substring(0, pointIndex);
}

function getFileExtension(full_file_name) {
  var pointIndex = full_file_name.lastIndexOf(".");
  return full_file_name.substring(pointIndex, full_file_name.length);
}

//添加一个文件名长度的限制
plupload.FILENAME_LENGTH_ERROR = -10004;
plupload.addFileFilter('max_filename_length', function(length, file, cb) {
  if (file.name !== undefined && length && file.name.length > length) {
    this.trigger('Error', {
      code: plupload.FILENAME_LENGTH_ERROR,
      message: plupload.translate('FileName length error!'),
      file: file
    });
    cb(false);
  } else {
    cb(true);
  }
});
//年整数限制
var INTEGER_REGEXP = /^\-?\d+$/;
app.directive('year_integer', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$validators.year_integer = function(modelValue, viewValue) {
        if (ctrl.$isEmpty(modelValue)) {
          return true;
        }
        if (viewValue.length <= '4' && INTEGER_REGEXP.test(viewValue)) {
          return true;
        }
        return false;
      };
    }
  };
});

app.directive('course', function() {
  return {
    //restrict: 'A',
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      function validate(value) {
          var valid = true;
          var unpassList = [
            '课件',
            'PPT',
            'ppt',
            '考题',
            '试题',
            '真题',
            '大学课程',
            '习题',
            '答案',
            '考研',
            '心得',
            '马原',
            '毛概',
            '大物',
            '高数',
            '线代',
            '笔记',
            '电子版',
            '第'
          ];
          if (/^[0-9\[\]\(\)\{\}]/.test(value)) {
            valid = false;
          }
          if (/[,，;；。!@#$%^&*<>?？]/.test(value)) {
            valid = false;
          }
          angular.forEach(unpassList, function(value2, key) {
            var regex = new RegExp(value2);
            if (regex.test(value)) {
              valid = false;
            }
          });
          ctrl.$setValidity('course', valid);
          return value;
        }
        //}
      ctrl.$parsers.unshift(validate);
      ctrl.$formatters.unshift(validate);
    }
  };

});

app.service('uploadSrv', ['$rootScope', '$http', '$cookies', '$window', 'toaster', 'User',
  function($rootScope, $http, $cookies, $window, toaster, User) {
    var self = this; //在异步处理函数中,this不能正确取到当前对象,所以用self来代替

    this.lastFetchTokenTime = undefined; //最后一次获取token的时间
    this.isUptokenOk = false; //token是否可用

    this.files = [];
    this.tempFiles = [];
    this.formlist = []; //表单列表，用来存储多次上传的表单,每次上传都对应一个下标
    this.tags = resourceTags;

    this.defaultform = {
      tag: '', //分类信息
      description: '', //描述信息
      //download_link: '', //下载链接
      college: '', //学院
      university: '', //学校
      teacher: '', //老师
      course: '', //课程名称 
      resource_name: '', //资源名称
      filename: '', //文件名
      file_size: '', //文件大小
      need_anonymous: false, //匿名发布
      rate: 4.5, //评分
      year: '',
    };

    this.formdata = {
      tag: '', //分类信息
      description: '', //描述信息
      //download_link: '', //下载链接
      college: '', //学院
      university: '', //学校
      teacher: '', //老师
      course: '', //课程名称 
      resource_name: '', //资源名称
      filename: '', //文件名
      file_size: '', //文件大小
      need_anonymous: false, //匿名发布
      rate: 4.5, //评分
      year: '',
    };


    this.rewardFormData = {
      //download_link: '',
      resource_name: '',
      comment: '',
      need_anonymous: false,
      file_size: '',
      reward_id: '',
    };

    this.defaultRewardFormData = {
      //download_link: '',
      resource_name: '',
      comment: '',
      need_anonymous: false,
      file_size: '',
      reward_id: '',
    };
    this.postRewardFormData = {

      tag: '', //分类信息
      description: '', //描述信息
      college: '', //学院
      university: '', //学校
      teacher: '', //老师
      course: '', //课程名称 
      need_anonymous: false, //匿名发布
      year: '',
      total_fb: '',
      expire_days: ''
    };
    this.defaultPostRewardFormData = {

      tag: '', //分类信息
      description: '', //描述信息
      college: '', //学院
      university: '', //学校
      teacher: '', //老师
      course: '', //课程名称 
      need_anonymous: false, //匿名发布
      year: '',
      total_fb: '',
      expire_days: ''
    };
    this.isProgressBarVisible = false; //进度条默认不显示
    this.isStatusDivVisible = false; //右下角的框也默认不显示
    this.isFileSelected = false; //默认没有选择文件，验证失败
    this.isFormValidate = false;
    this.btnPickFilesText = "选择文件";
    this.isRetryTipVisible = false; //重试的提醒，同时用来判断是否是在重试下弹出的上传框

    this.isRewardMode = false; //是否是悬赏模式下的上传，两者利用一个上传框
    this.isPostRewardMode = false; //是否是发布悬赏模式
    //this.lastRewardFormValidationStatus = true; //用来记录上一次悬赏表单的验证情况

    this.clearAll = function() {

      for (var i = 0; i < this.tempFiles.length; i++) {
        this.uploader.removeFile(this.tempFiles[i]);
      }
      this.clearTempFileList();
      if (this.isRewardMode) {
        $.extend(this.rewardFormData, this.defaultRewardFormData);
      } else if (this.isPostRewardFormData) {
        $.extend(this.postRewardFormData, this.defaultPostRewardFormData);
      } else {
        $.extend(this.formdata, this.defaultform);
      }
    };

    this.addTempFile = function(file) {
      file.isRewardMode = this.isRewardMode;
      file.rewardId = this.rewardId;
      this.tempFiles.push(file);
      this.isFileSelected = true;
      if (this.tempFiles.length > 7) { //根据数量判断，之前的不能实时获取高度
        angular.element(".progressbar-div").css("overflow-y", "scroll").css("height", "300px"); //违反了不操作DOM的约定
      }

      $rootScope.$digest();
    };

    this.clearTempFileList = function() {
      this.tempFiles = [];
    };


    this.updateFile = function(file) {
      for (var i = 0, L = this.files.length; i < L; i++) {
        if (this.files[i].name == file.id) {
          this.files[i] = file;
          break;
        }
      }
    }; //更新文件状态

    this.updateFileList = function() {
      var form = {};
      if (!this.isRewardMode) {
        //普通模式
        $.extend(form, this.formdata);
      } else {
        //悬赏模式，存储的表单是悬赏的表单
        $.extend(form, this.rewardFormData);
      }
      this.formlist.push(form);
      for (var i = 0, L = this.tempFiles.length; i < L; i++) {
        this.tempFiles[i].formdata = form;
        this.files.push(this.tempFiles[i]);
      }
      this.clearTempFileList();
    }; //更新临时列表到永久文件列表中

    //移除文件
    this.removeFile = function(file) {
      for (var i = 0, L = this.files.length; i < L; i++) {
        if (this.files[i].id == file.id) {
          this.files.splice(i, 1);
          break;
        }
      } //这个只能从视图上删除
      this.uploader.removeFile(file); //从上传队列中删除
    };

    this.getProgressbarText = function(file) {
      switch (file.status) {
        case plupload.QUEUED:
          return '等待上传';
        case plupload.UPLOADING:
          return file.percent + '%' + '  ' + formatSpeed(this.uploader.total.bytesPerSec);
        case plupload.DONE:
          return '上传完成';
        case plupload.FAILED:
          return '上传失败';
        case plupload.FILE_SIZE_ERROR:
          return '超过' + this.uploader.getOption("filters").max_file_size.toLocaleUpperCase();
        case plupload.FILE_EXTENSION_ERROR:
          return '无效文件类型';
        case plupload.FILE_DUPLICATE_ERROR:
          return '重复文件';
        case plupload.HTTP_ERROR:
          // return '存储服务器拒绝';
          return file.response;
        case plupload.FILENAME_LENGTH_ERROR:
          return '文件名过长';
        case uploadStatus.ARGUMENT_ERROR:
          return '回参错误';
        case uploadStatus.TIME_OUT:
          return '回调超时';
        case uploadStatus.SERVER_INTERNAL_ERROR:
          return 'SERVER_ERR';
        case uploadStatus.OTHER_ERROR:
          return '入库错误';
        case uploadStatus.ALREADY_EXISTS_RESOURCE:
          return '重复上传的文件!';
        default:
          return '未知错误';
      }
    };

    this.getProgressbarStyle = function(file) {
      var style = 'progress-bar ';
      switch (file.status) {
        case plupload.UPLOADING:
          style = style + 'progress-bar-success';
          break;
        case plupload.DONE:
          style = style + 'progress-bar-success ';
          break;
        case plupload.QUEUED:
          {
            file.percent = 100;
            style = style + 'progress-bar-info';
            break;
          }

        default:
          {
            style = style + 'progress-bar-danger ';
            break;
          }
      }
      return style;
    };

    this.getSummaryStatus = function() {
      if (this.uploader.state == plupload.STOPPED) {
        return '上传完毕';
      } else if (this.uploader.state == plupload.STARTED) {
        //更新的稍微慢一些，会出现当前上传的比总数还大的情况
        var uploaded = this.uploader.total.uploaded + 1;
        var length = this.uploader.files.length;
        uploaded = uploaded > length ? length : uploaded;
        return '正在上传:' + uploaded + '/' + length;
      }
    };

    this.switchProgress = function() {
      //对进度条的显示进行切换
      this.isProgressBarVisible = !this.isProgressBarVisible;
      if (this.isProgressBarVisible) {
        angular.element(".bottom-right-div.upload-status-container.minus").removeClass("minus"); //状态栏最大化啦，之后更改图标
        angular.element(".glyphicon.glyphicon-resize-full").removeClass("glyphicon-resize-full").addClass("glyphicon glyphicon-resize-small");
      } else {
        angular.element(".bottom-right-div.upload-status-container").addClass("minus");
        angular.element(".glyphicon.glyphicon-resize-small").removeClass("glyphicon-resize-small").addClass("glyphicon glyphicon-resize-full");
      }
    };

    this.formatSize = function(file) {
      //格式化文件大小 
      return plupload.formatSize(file.size).toLocaleUpperCase();
    };

    this.refreshUploader = function() {
      this.uploader.destroy();
      this.initUploader();
    };

    this.isRemoveIconVisible = function(file) {
      var list = [
        plupload.FILE_DUPLICATE_ERROR,
        plupload.FILE_SIZE_ERROR,
        plupload.QUEUED,
        plupload.FAILED,
        plupload.FILENAME_LENGTH_ERROR
      ];
      for (var i = 0; i < list.length; i++) {
        if (list[i] == file.status) {
          return true;
        }
      }
      return false;
    };
    this.isRetryIconVisible = function(file) {
      var result = false;
      angular.forEach(uploadStatus, function(value, key) {
        if (file.status == value) {
          result = true;
        }
      });
      return result;
    };
    this.retryCallBack = function(file) {
      //重试回调
      this.isFileSelected = true; //重试的，让表单验证通过
      this.isRetryTipVisible = true;
      this.currentRetryFile = file;
      angular.element("#uploadModal").modal("show");
      //this.callback(file, file.downloadlink);
      //this.isRetryTipVisible = false;
    };
    this.startUpload = function() {
      $cookies.last_upload_university = this.formdata.university;
      $cookies.last_upload_college = this.formdata.college;

      if (this.isRetryTipVisible === false) {

        if (this.isRewardMode) { //把正常模式表单中的数据拿过来一部分
          this.rewardFormData.need_anonymous = this.formdata.need_anonymous;
          this.rewardFormData.rate = this.formdata.rate;
        } else if (this.isPostRewardMode) {
          this.postRewardFormData.university = this.formdata.university;
          this.postRewardFormData.college = this.formdata.college;
          this.postRewardFormData.course = this.formdata.course;
          this.postRewardFormData.tag = this.formdata.tag;
          this.postRewardFormData.teacher = this.formdata.teacher;
          this.postRewardFormData.year = this.formdata.year;
          this.postRewardFormData.need_anonymous = this.formdata.need_anonymous;
          this.postReward(this.postRewardFormData);
        }
        if (!this.isPostRewardMode) {
          //开始文件上传
          this.updateFileList(); //把临时的文件列表更新到永久的文件列表中
          // this.uploader.files = this.files; //把文件列表给uploader的文件列表
          this.uploader.start();
          $window.onbeforeunload = function() {
            return '上传将被中断并不能恢复!';
          };
          this.btnPickFilesText = "选择文件";
          this.isStatusDivVisible = true; //底部状态栏可见啦 ！
        }
      } else if (this.isRetryTipVisible === true) {
        this.callback(this.currentRetryFile);
        this.isRetryTipVisible = false;
      }

      angular.element("#uploadModal").modal("hide");
      this.isFileSelected = false; //点了开始，就又是一个新的表单了，当然不能通过验证
      this.isRewardMode = false; //开始上传后就把悬赏模式关掉
      this.isPostRewardMode = false; //开始上传后就把发布悬赏模式关掉

    };

    this.postReward = function(formData) {
      var req = {
        url: '/reward/post',
        method: 'POST',
        data: formData,
        headers: {
          "X-CSRFToken": getCookie("_xsrf")
        }

      };
      $http(req).success(function(data, status, headers, config) {
          var responseData = angular.fromJson(data);
          switch (responseData.err) {

            case 0:
              toaster.pop('success', '悬赏发布成功!', '请耐心等待一会儿，您的悬赏才会出现在悬赏列表中，您可以到个人中心-我的悬赏中查看悬赏状态！', true);
              break;
            case 1:
              toaster.pop('error', '未经授权！', '请检查是否登录!', false);
              break;
            case 2:
              toaster.pop('error', '参数错误！', '', false);
              break;
            case 3:
              toaster.pop('error', '描述信息过多！', '描述信息长度限制500字以内，请修改后再次发布！', false);
              break;
            case 4:
              toaster.pop('error', '教师长度错误！', '教师长度上限20字符，请修改后再次发布！', false);
              break;
            case 5:
              toaster.pop('error', '资源类别长度错误！', '资源类别长度上限20字符，请修改后再次发布！', false);
              break;
            case 6:
              toaster.pop('error', '院系错误！', '您的院系出错了，请到个人中心设置正确的院系！', false);
              break;
            case 7:
              toaster.pop('error', '课程错误！', '请修改课程信息后再次发布！', false);
              break;
            case 8:
              toaster.pop('error', '过期时间错误！', '过期时间上限30天，下限1天，请修改后再次发布', false);
              break;
            case 9:
              toaster.pop('error', '悬赏金额错误！', '悬赏金额必须大于零！', false);
              break;
            case 10:
              toaster.pop('error', '悬赏金额不足！', '请检查您的F币!', false);
              break;
            default:

              toaster.pop('error', '发布失败！', '未知错误！', false);


          }
        })
        .error(function(data, status, headers, config) {


        });

    };

    this.callback = function(file) {
      //this.formdata = file.formdata;
      //if (this.isFormValidate) {
      //表单合法才进行上传
      //var isTeacherEmpty = false;
      if (!file.isRewardMode) { //普通模式下的上传
        if (file.formdata.teacher === undefined || file.formdata.teacher === '') {
          //isTeacherEmpty = true;
          file.formdata.teacher = "无";
        }
        //file.formdata.download_link = download_link;
        file.formdata.filename = file.name;
        file.formdata.file_size = file.size;
        file.formdata.resource_name = file.name; //资源名称默认为文件名添加
        file.formdata.file_hash = file.hash;
        file.formdata.file_key  = file.key;
        file.formdata.mimeType = file.mimeType;

        var req = {
          method: 'POST',
          url: "/upload",
          timeout: 10000,
          headers: {
            "X-CSRFToken": getCookie("_xsrf"),
          },
          data: file.formdata,
        };

        $http(req).success(function(data, status, headers, config) {
          //回调成功
          var r = angular.fromJson(data);
          if (r.err === 0) {

            file.status = plupload.DONE;
            toaster.pop('success', '文档 ' + file.name + ' 上传完成!', '感谢您的分享，我们审核人员将会确保在24小时内审核完成。如果您有大批优质学习资料希望尽快获得审核通过，欢迎加入校园星空资源建设QQ群:491589986', true);

          } else if (r.err == 1) {

            file.status = uploadStatus.UNAUTHENTICATED;
            toaster.pop('error', '系统提示', '未经授权上传！', false);
            file.errorInfo = 'UNAUTHENTICATED';
            self.handleUploadError(file);

          } else if (r.err == 2) {

            file.status = uploadStatus.ARGUMENT_ERROR;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', '上传表单参数不正确,请在上传表单中更正后点击重试按钮！', false);
            file.errorInfo = 'ARGUMENT_ERROR';
            self.handleUploadError(file);
          } else if (r.err == 3) {
            file.status = uploadStatus.ARGUMENT_ERROR;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', '文件体积过大！', false);
            file.errorInfo = 'FILE_SIZE_ERROR';
            self.handleUploadError(file);
          } else if (r.err == 4) {
            file.status = uploadStatus.ARGUMENT_ERROR;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', '评分参数错误,请在上传表单中更正后点击重试按钮！', false);
            file.errorInfo = 'RATE_RANGE_ERROR';
            self.handleUploadError(file);
          } else if (r.err == 5) {
            file.status = uploadStatus.ARGUMENT_ERROR;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', '资源描述错误,请在上传表单中更正后点击重试按钮！', false);
            file.errorInfo = 'DESCRIPTION_LEN_ERROR';
            self.handleUploadError(file);
          } else if (r.err == 6) {
            file.status = uploadStatus.ARGUMENT_ERROR;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', '资源名称长度错误,请在上传表单中更正后点击重试按钮！', false);
            file.errorInfo = 'RESOURCE_NAME_LEN_ERROR';
            self.handleUploadError(file);
          } else if (r.err == 7) {
            file.status = uploadStatus.ARGUMENT_ERROR;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', '教师参数错误,请在上传表单中更正后点击重试按钮！', false);
            file.errorInfo = 'TEACHER_LEN_ERROR';
            self.handleUploadError(file);
          } else if (r.err == 8) {
            file.status = uploadStatus.ARGUMENT_ERROR;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', '资源分类错误,请在上传表单中更正后点击重试按钮！', false);
            file.errorInfo = 'TAG_LEN_ERROR';
            self.handleUploadError(file);
          } else if (r.err == 9) {
            file.status = uploadStatus.ARGUMENT_ERROR;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', '学院错误,请在上传表单中更正后点击重试按钮！', false);
            file.errorInfo = 'INVALID_COLLEGE';
            self.handleUploadError(file);
          } else if (r.err == 10) {
            file.status = uploadStatus.ARGUMENT_ERROR;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', '课程错误,请在上传表单中更正后点击重试按钮！', false);
            file.errorInfo = 'INVALID_COURSE';
            self.handleUploadError(file);
          } else if (r.err == 11) {
            file.status = uploadStatus.ARGUMENT_ERROR;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', '资源已经通过审核', false);
            file.errorInfo = 'ALREADY_PASS_AUDIT';
            self.handleUploadError(file);
          } else if (r.err == 12){
            file.status = uploadStatus.ALREADY_EXISTS_RESOURCE;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', '该资源其他小伙伴已经上传过了呢!', false);
            file.errorInfo = 'FILE_ALREADY_EXISTS';
            self.handleUploadError(file);
          }

        }).error(function(data, status, headers, config) {
          //回调失败
          if (status == 500) {
            //500服务器内部错误
            file.status = uploadStatus.SERVER_INTERNAL_ERROR;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', file.name + '上传成功,但业务服务器处理失败,请点击重试按钮！', false);
            file.errorInfo = uploadStatus.SERVER_INTERNAL_ERROR;
            self.handleUploadError(file);

          } else if (status == 408) {
            //请求超时
            file.status = uploadStatus.TIME_OUT;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', file.name + '上传成功,但向业务服务器请求超时,请点击重试按钮！', false);
            file.errorInfo = uploadStatus.TIME_OUT;
            self.handleUploadError(file);

          } else {
            file.status = uploadStatus.OTHER_ERROR;
            //file.downloadlink = file.formdata.download_link;
            toaster.pop('error', '系统提示', file.name + '上传成功,但向业务服务器请求超时,请点击重试按钮！', false);
            file.errorInfo = uploadStatus.OTHER_ERROR;
            self.handleUploadError(file);
          }
        });
      } else { //悬赏模式

        //file.formdata.download_link = download_link;
        file.formdata.filename = file.name;
        file.formdata.file_size = file.size;
        file.formdata.resource_name = file.name; //资源名称默认为文件名添加
        file.formdata.reward_id = file.rewardId;
        file.formdata.file_key = file.key;
        file.formdata.file_hash = file.hash;
        file.formdata.mimeType = file.mimeType;
        var req2 = {
          url: 'reward/resource/upload',
          method: 'POST',
          data: file.formdata,
          headers: {
            "X-CSRFToken": getCookie("_xsrf"),
          }
        };
        $http(req2).success(function(data, status, headers, config) {
            var responseData = angular.fromJson(data);
            switch (responseData.err) {
              case 0:
                toaster.pop('success', '资源上传成功！', '资源上传成功，对方采用后即可获得相应F币', true);
                break;
              case 1:
                file.status = uploadStatus.UNAUTHENTICATED;
                toaster.pop('error', '未经授权！', '请检查是否登录', false);
                file.errorInfo = 'UNAUTHENTICATED';
                self.handleUploadError(file);
                break;
              case 2:
                file.status = uploadStatus.ARGUMENT_ERROR;
                toaster.pop('error', '参数错误！', '', false);
                file.errorInfo = 'ARGUMENT_ERROR';
                self.handleUploadError(file);
                break;
              case 3:
                file.status = uploadStatus.ARGUMENT_ERROR;
                toaster.pop('error', '文件大小错误！', '文件最大100M,请检查一下文件大小！', false);
                file.errorInfo = 'FILE_SIZE_ERROR';
                self.handleUploadError(file);
                break;
              case 4:
                file.status = uploadStatus.ARGUMENT_ERROR;
                toaster.pop('error', '资源名称长度错误！', '教师长度上限50字符，请修改后再次发布！', false);
                file.errorInfo = 'RESOURCE_NAME_LEN_ERROR';
                self.handleUploadError(file);
                break;
              case 5:
                file.status = uploadStatus.ARGUMENT_ERROR;
                toaster.pop('error', '描述信息错误！', '描述信息长度限制100，请修改后再次发布！', false);
                file.errorInfo = 'DESCRIPTION_LEN_ERROR';
                self.handleUploadError(file);
                break;
              case 6:
                file.status = uploadStatus.ARGUMENT_ERROR;
                toaster.pop('error', '不能自赏自领！', '自己发布的悬赏不能自己领哦！', false);
                file.errorInfo = 'SELF_UPLOAD_FORBIDDEN';
                self.handleUploadError(file);
                break;

              default:
                file.status = 'UNKNOWN_ERROR';
                toaster.pop('error', '领赏失败！', '未知错误！', false);
                file.errorInfo = 'UNKNOWN_ERROR';
                self.handleUploadError(file);
            }

          })
          .error(function(data, status, headers, config) {

            //回调失败
            if (status == 500) {
              //500服务器内部错误
              file.status = uploadStatus.SERVER_INTERNAL_ERROR;
              //file.downloadlink = file.formdata.download_link;
              toaster.pop('error', '系统提示', file.name + '上传成功,但业务服务器处理失败,请点击重试按钮！', false);
              file.errorInfo = uploadStatus.SERVER_INTERNAL_ERROR;
              self.handleUploadError(file);

            } else if (status == 408) {
              //请求超时
              file.status = uploadStatus.TIME_OUT;
              //file.downloadlink = file.formdata.download_link;
              toaster.pop('error', '系统提示', file.name + '上传成功,但向业务服务器请求超时,请点击重试按钮！', false);
              file.errorInfo = uploadStatus.TIME_OUT;
              self.handleUploadError(file);

            } else {
              file.status = uploadStatus.OTHER_ERROR;
              //file.downloadlink = file.formdata.download_link;
              toaster.pop('error', '系统提示', file.name + '上传成功,但向业务服务器请求超时,请点击重试按钮！', false);
              file.errorInfo = uploadStatus.OTHER_ERROR;
              self.handleUploadError(file);
            }

          });

      }

    };
    this.handleUploadError = function(file) {
      var errData = {
        mode: file.isRewardMode ? 'RewardMode' : 'UploadMode', //file是在正常上传模式下还是领赏模式下
        reward_id: file.formdata.reward_id, //如果是领赏模式，reward_id不为空
        file_size: file.formdata.file_size,
        //download_link: file.formdata.download_link,
        resource_name: file.formdata.resource_name,
        university: file.formdata.university,
        college: file.formdata.college,
        filename: file.name,
        err_stat: file.status, //file的出错状态
        err_info: file.errorInfo, //file的出错信息
        file_key: file.key, //file在七牛空间中的key，可用来操作资源
        file_hash:file.hash,
        file_mimeType:file.mimeType,
        year: file.formdata.year,
        tag: file.formdata.tag,
        description:file.formdata.description,
        last_fetch_token_time: this.lastFetchTokenTime,
        error_occur_time: (new Date()).toLocaleString(),
        //upload_token: this.uploader.uptoken,
      };
      var req = {
        url: '/user/log/upload',
        method: 'POST',
        data: errData,
        headers: {
          "X-CSRFToken": getCookie("_xsrf"),
        }
      };
      $http(req).success(function(data, status, headers, config) {
          //toaster.pop('error', '系统提示', '出错信息已经保存到数据库！', false);
        })
        .error(function(data, status, headers, config) {});

    };

    this.setCourseInfo = function(university, college, course) {
      this.formdata.university = university;
      this.formdata.college = college;
      this.formdata.course = course;

    };

    this.cancel = function() {
      this.isFileSelected = false;
      this.btnPickFilesText = '选择文件';
      this.isRewardMode = false;
      this.isPostRewardMode = false;
      this.clearAll();

    };
    this.initUploader = function() {
      if ($rootScope.university === "" || $rootScope.university === undefined) {
        $rootScope.university = $cookies.last_upload_university;
      }
      if ($rootScope.college === "" || $rootScope.college === undefined) {
        $rootScope.college = $cookies.last_upload_college;
      }
      //this.formdata.university = $rootScope.university;
      //this.formdata.college = $rootScope.college;
      this.formdata.university = $cookies.last_upload_university;
      this.formdata.college = $cookies.last_upload_college;
      if (this.formdata.university === '' || this.formdata.university === undefined) {
        this.formdata.university = $rootScope.university;
      }
      if (this.formdata.college === '' || this.formdata.college === undefined) {
        this.formdata.college = $rootScope.college;
      }

      this.uploader = Qiniu.uploader({
        runtimes: 'html5,flash,html4', //上传模式,依次退化
        browse_button: 'pickfiles', //上传选择的点选按钮，**必需**
        uptoken_url: '/fetch_token',
        domain: 'http://7xjkjd.dl1.z0.glb.clouddn.com/', //bucket 域名，下载资源时用到，**必需**
        chunk_size: '4mb', //分块上传时，每片的体积
        auto_start: false, //选择文件后自动上传，若关闭需要自己绑定事件触发上传,
        filters: { //这里配置过滤器，限制文件大小、文件类型等
          max_file_size: "100mb",
          prevent_duplicates: true,
          max_filename_length: 50
        },
        init: {
          'Init': function(up) {
            //var req = {
            //  url: '/fetch_token',
            //  method: 'GET',
            //  headers: {
            //    "X-CSRFToken": getCookie("_xsrf"),
            //  }
            //};
            //$http(req).success(function(data, status, headers, config) {
            //  var r = angular.fromJson(data);
            //  //up.uptoken = r.uptoken;
            //  up.setOption('uptoken',r.uptoken);
            //  var isLogined = User.logined();
            //  if (isLogined && up.uptoken === '') {
            //    toaster.pop(error, '系统提示', '获取上传权限错误，如果要进行上传，请刷新页面后重试！', false);
            //  }
            //  self.lastFetchTokenTime = (new Date()).toLocaleString();
            //}).error(function(data, status, headers, config) {
            //    toaster.pop(error, '系统提示', '获取上传权限错误，如果要进行上传，请刷新页面后重试！', false);
            //});
          },
          'PostInit': function(up) {
            //if (up.getOption('uptoken') !== '' || up.getOption('uptoken') !== undefined) {
            //  self.isUptokenOk = true;
            //} else {
            //  self.isUptokenOk = false;
            //}
            
              self.lastFetchTokenTime = (new Date()).toLocaleString();
          },

          'FilesAdded': function(up, files) {
            self.btnPickFilesText = "重新选择";
            plupload.each(files, function(file) {
              self.addTempFile(file);
            });

          },

          'BeforeUpload': function(up, file) {
            // 每个文件上传前,处理相关的事情

          },


          'FileFiltered': function(up, file) { //我擦，这个函数竟然是反应符合条件的
            // alert(file.name + '    '+ file.size/1024/1024 + 'mb');
          },

          'UploadProgress': function(up, file) {
            // 每个文件上传时,处理相关的事情
            // $scope.$apply(function() {
            self.updateFile(file);
            $rootScope.$digest();
            // });

          },

          'FileUploaded': function(up, file, info) {

            // $scope.$apply(function() {
            self.updateFile(file);
            //add xsrf
            var domain = up.getOption('domain'); //获得上传的domain
            var res = angular.fromJson(info);
            //var download_link = domain + res.key;
            file.hash = res.hash;
            file.key = res.key;
            file.mimeType = res.mimeType;
            //由于在上传文件名后边添加了时间,所以要设置下载的默认文件名
            //var custome_download_filename_link = download_link + '?download/' + file.name;
            //var custome_download_filename_link = download_link +'&attname=' +  file.name;
            self.callback(file);
            up.files = self.files;
            // });

          },

          'Error': function(up, err, errTip) {
            //上传出错时,处理相关的事情
            if (up.state == plupload.QUEUED) {
              //正在往里边添加文件的时候出错，说明不符合filter
              // $scope.$apply(function() {
              err.file.status = err.code; //把文件的状态改为err.code，否则永远是4
              err.file.percent = 100;
              self.addTempFile(err.file);
              // });
            } else if (up.state == plupload.STARTED) {
              //正在上传的时候出错,说明是七牛出错,正上传时添加重复文件也会运行到这儿
              if (err.code == plupload.FILE_DUPLICATE_ERROR || err.code == plupload.FILE_SIZE_ERROR) {
                //上传时新添加文件不符合filter的情况
                // $scope.$apply(function() {
                err.file.status = err.code; //把文件的状态改为err.code，否则永远是4
                err.file.percent = 100;
                self.addTempFile(err.file);
                // });
              } else {
                //这剩下的是正在上传的时候七牛服务器出错了
                // $scope.$apply(function() {
                err.file.status = err.code;
                err.file.response = err.response;
                err.file.percent = 100;
                err.file.errorInfo = err.response;
                self.handleUploadError(file);
                self.updateFile(err.file);
                // });
              }
            }
          },

          'UploadComplete': function() {
            //队列文件处理完毕后,处理相关的事情
            // toaster.pop('success', '系统提示', '所有文件处理完毕!', true);
            // self.updateFileList();
            // self.refreshUploader();
            $window.onbeforeunload = undefined;

          },
          'Key': function(up, file) {
            //自定义key,在文件名后加一个当前的秒数
            // 该配置必须要在 unique_names: false , save_key: false 时才生效
            //var d = new Date();
            //var fn = getFileName(file.name);
            //var ex = getFileExtension(file.name);
            //var key = fn + '_' + d.getTime() + ex;
            // do something with key here
            //return key;
          }
        }

      });
    };

  }
]);



app.service('classSrv', function($http) {
  var class0List = [];
  var class1List = [];
  var class2List = [];
  var current_class1 = null;

  this.getClass0List = function() {
    return class0List;
  };

  this.getClass1List = function() {
    return class1List;
  };

  this.getClass2List = function() {
    return class2List;
  };

  this.fetchClass0List = function() {
    // fetch from server
    if (class0List.length)
      return;

    $http({
        url: '/get_course',
        method: 'GET'
      })
      .success(function(response) {
        if (0 === response.err) {
          class0List.length = 0;
          angular.forEach(response.list, function(v, i) {
            class0List.push(v);
          });
        }
      })
      .error(function(data, status, headers, config) {
        // 失败处理
        alert('页面加载失败，请重试！');
      });
  };

  this.fetchClass1List = function(class1) {
    // fetch from server
    $http({
        url: '/get_course',
        method: 'GET',
        params: {
          class1: class1
        }
      })
      .success(function(response) {
        if (0 === response.err) {
          class1List.length = 0;
          angular.forEach(response.list, function(v, i) {
            class1List.push(v);
          });
          current_class1 = class1;
        }
      })
      .error(function(data, status, headers, config) {
        // 失败处理
        alert('页面加载失败，请重试！');
      });
  };

  this.fetchClass2List = function(class2) {
    // fetch from server
    $http({
        url: '/get_course',
        method: 'GET',
        params: {
          class1: current_class1,
          class2: class2
        }
      })
      .success(function(response) {
        if (0 === response.err) {
          class2List.length = 0;
          angular.forEach(response.list, function(v, i) {
            class2List.push(v);
          });
        }
      })
      .error(function(data, status, headers, config) {
        // 失败处理
        alert('页面加载失败，请重试！');
      });
  };
});

app.controller('uploadController', ['$rootScope', '$scope', 'uploadSrv', 'User', 'toaster', '$http', 'universityCollegeSrv', 'classSrv',
  function($rootScope, $scope, uploadSrv, User, toaster, $http, universityCollegeSrv, classSrv) {

    $scope.isCurrentOrder = function(order) {
      return order == $scope.show_order;
    };

    $scope.universityClickHandler = function() {
      $scope.show_order = 1;
    };

    $scope.chooseProvince = function(province) {
      universityCollegeSrv.fetchUniversities(province);
      $scope.show_order = 2;
    };

    $scope.chooseUniversity = function(university) {
      $scope.formdata.university = university;
      $scope.formdata.college = "";
      universityCollegeSrv.fetchColleges(university);
      $scope.show_order = 3;
    };

    $scope.chooseCollege = function(college) {
      $scope.formdata.college = college;
      $scope.show_order = 0;
    };

    $scope.collegeClickHandler = function() {
      var currentUniversity = $scope.formdata.university;
      if (currentUniversity) {
        universityCollegeSrv.fetchColleges(currentUniversity);
        $scope.show_order = 3;
      }
    };

    $scope.getCourses = function() {
      if (!($scope.formdata.university && $scope.formdata.college)) return;
      $http({
          url: '/course/list',
          method: 'GET',
          params: {
            university: $scope.formdata.university,
            college: $scope.formdata.college
          }
        })
        .success(function(response) {
          $scope.courses = response.err ? [] : response.course_list;
        })
        .error(function(data, status, headers, config) {
          // 失败处理
          alert('加载失败，请重试！');
        });
    };

    $scope.courseFilter  = function(viewValue){

      return function(value){
        return lcs(viewValue,value.toLowerCase());
      }
    }

    $scope.init = function() {
      //  $scope.service = FileListSrv;
      $scope.input_width = '100%';
      $scope.files = uploadSrv.files;
      $scope.tempFiles = uploadSrv.tempFiles;
      $scope.isProgressBarVisible = uploadSrv.isProgressBarVisible; //进度条默认不显示
      $scope.isStatusDivVisible = uploadSrv.isStatusDivVisible; //右下角的框也默认不显示
      $scope.isFileSelected = uploadSrv.isFileSelected; //默认没有选择文件，验证失败
      $scope.tags = uploadSrv.tags;
      $scope.formdata = uploadSrv.formdata;
      $scope.rewardFormData = uploadSrv.rewardFormData;
      $scope.postRewardFormData = uploadSrv.postRewardFormData;
      $scope.btnPickFilesText = uploadSrv.btnPickFilesText;
      uploadSrv.initUploader(); //初始化之后赋值
      $scope.uploader = uploadSrv.uploader;
      //------以下非引用对象需要监听
      $scope.$watch(function() {
        return uploadSrv.tempFiles;
      }, function(newValue, oldValue) {
        $scope.tempFiles = newValue;
      });

      $scope.$watch(function() {
        return uploadSrv.isRetryTipVisible;
      }, function(newValue, oldValue) {
        $scope.isRetryTipVisible = newValue;
      });
      $scope.$watch(function() {
        return uploadSrv.isStatusDivVisible;
      }, function(newValue, oldValue) {
        $scope.isStatusDivVisible = newValue;
      });
      $scope.$watch(function() {
        return uploadSrv.btnPickFilesText;
      }, function(newValue, oldValue) {
        $scope.btnPickFilesText = newValue;
      });
      $scope.$watch(function() {
        return uploadSrv.isFileSelected;
      }, function(newValue, oldValue) {
        $scope.isFileSelected = newValue;
      });
      $scope.$watch(function() {
        return uploadSrv.isProgressBarVisible;
      }, function(newValue, oldValue) {
        $scope.isProgressBarVisible = newValue;
      });
      $scope.$watch(function() {
        return $scope.upLoadForm.$valid;
      }, function(newValue, oldValue) {
        uploadSrv.isFormValidate = newValue;
      });
      $scope.$watch(function() {
        return uploadSrv.isRewardMode;
      }, function(newValue, oldValue) {
        $scope.isRewardMode = newValue;
      });
      $scope.$watch(function() {
        return uploadSrv.isPostRewardMode;
      }, function(newValue, oldValue) {
        $scope.isPostRewardMode = newValue;
      });
      //-------监听完毕--------

      $scope.show_order = 0; // 0 for no selector, 1 for province, 2 for university, 3 for college
      $scope.prov_list = universityCollegeSrv.getProvList();
      $scope.universities = universityCollegeSrv.getUniversities();
      $scope.colleges = universityCollegeSrv.getColleges();

      // $scope.formdata.university = $rootScope.university;
      // $scope.formdata.college = $rootScope.college;
      $scope.courses = [];

      angular.element("#input-rating").rating({
        "stars": "5",
        "min": "0",
        "max": "5",
        "step": 0.5,
        "size": "xs",
        "showClear": false,
        "glyphicon": false,
        "starCaptions": {
          0.5: "0.5",
            1: "1",
            1.5: "1.5",
            2: "2",
            2.5: "2.5",
            3: "3",
            3.5: "3.5",
            4: "4",
            4.5: "4.5",
            5: "5"
        }
      }).rating('update', 4.5).on('rating.change', function(event, value, caption) {
        $scope.formdata.rate = parseFloat(value);
      }); //暂时用jquery实现评分的初始化

    };
    $scope.cancel = function() {
      uploadSrv.cancel();
    };
    $scope.clearAll = function() {
      $scope.clearTempFileList();
      uploadSrv.clearAll();
      $scope.current_class1 = '';
      $scope.isShowClass2Input = false;
      $scope.isShowCourseInput = false;
      $scope.input_width = '100%';
    };

    $scope.addTempFile = function(file) {
      uploadSrv.addTempFile(file);
    };

    $scope.clearTempFileList = function() {
      uploadSrv.clearTempFileList();
    };

    $scope.updateFileList = function() {
      uploadSrv.updateFileList();
    }; //更新文件服务的文件列表

    $scope.updateFile = function(file) {
      // body...
      uploadSrv.updateFile(file);
    }; //更新文件状态

    $scope.getProgressbarText = function(file) {
      return uploadSrv.getProgressbarText(file);
    };

    $scope.getProgressbarStyle = function(file) {
      return uploadSrv.getProgressbarStyle(file);
    };

    $scope.getSummaryStatus = function() {
      return uploadSrv.getSummaryStatus();
    };

    $scope.switchProgress = function() {
      //对进度条的显示进行切换
      uploadSrv.switchProgress();
    };

    $scope.formatSize = function(file) {
      //格式化文件大小 
      return plupload.formatSize(file.size).toLocaleUpperCase();
    };


    //是否显示删除图标
    $scope.isRemoveIconVisible = function(file) {
      return uploadSrv.isRemoveIconVisible(file);
    };

    $scope.isRetryIconVisible = function(file) {
      return uploadSrv.isRetryIconVisible(file);
    };


    //删除文件
    $scope.removeFile = function(file) {
      uploadSrv.removeFile(file);

    };
    $scope.retryCallBack = function(file) {
      //重试回调 
      uploadSrv.callback(file);
    };

    $scope.startUpload = function() {
      //开始文件上传
      uploadSrv.startUpload();
    };

    $scope.pickFile = function() {
      if (uploadSrv.btnPickFilesText == "重新选择") {
        for (var i = 0; i < uploadSrv.tempFiles.length; i++) {
          uploadSrv.uploader.removeFile(uploadSrv.tempFiles[i]);
        }
        uploadSrv.clearTempFileList();
      }
    };

  }
]); //上传控制器

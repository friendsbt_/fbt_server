/*********************************************************************************
 *     File Name           :     reply-controller.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-10-28 20:21]
 *     Last Modified       :     [2015-11-08 17:54]
 *     Description         :     回答问题的Controller
 **********************************************************************************/
app.controller('ReplyQuestionController', ['$scope', 'rte', 'QuestionAnswerSrv', '$routeParams',
  function($scope, rte, QuestionAnswerSrv, $routeParams) {

    $scope.init = function() {

      $scope.questionClass = $routeParams.questionClass;
      $scope.questionId = $routeParams.questionId;
      $scope.questionTitle = $routeParams.questionTitle;

    };

    $scope.getContent = rte.getContent; //获取编辑器中的内容

    $scope.deployAnswer = function() {
      var content = $scope.getContent();

      var length = removeHtmlTag(content).length;

      if (length < 1) {
        toaster.pop('error', '请不要发送空内容哦～～～', '', true);
        return;
      } else if (length > 3000) {
        toaster.pop('error', '发送内容太长啦，目前最多3000字哦～～～', '', true);
        return;
      }

      QuestionAnswerSrv.deployAnswer($scope.questionId, content, $scope.isAnonymous);
    };

  }
]);

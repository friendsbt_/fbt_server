app.controller('validateTokenController', ['User', 'uploadSrv', '$scope', '$location',
  function(User, uploadSrv, $scope, $location) {
    $scope.upLoad = function() {

      if (User.logined()) {
        if (uploadSrv.uploader.state == plupload.STOPPED) {
          uploadSrv.refreshUploader();
        }
        $('#uploadModal').modal("show");
      } else {
        var url = $location.url();
        $location.path('/home').search('return_url', url);
      }

    };

  }
]);

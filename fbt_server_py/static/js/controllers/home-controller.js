/**
 * Created by yuLei on 2015/11/1.
 * home.html----js---controller
 */

function htmlencode(s) {
    var div = document.createElement('div');
    div.appendChild(document.createTextNode(s));
    return div.innerHTML;
}

/*
完善信息过滤器，如果用户匹配，则返回，
比如输入zky,返回中科院相关
 */
app.filter('ucf',function(){
    return function(list,input,college){
        var listShow = [];
        if(input){
            for(var i = 0;i < list.length; i++){
                //判断用户输入的是汉字还是字母
                if(/^[\u4e00-\u9fa5]*$/.test(input)){
                    if(college){
                        if(lcs(input,list[i].college))listShow.push(list[i]);
                    }else{
                        if(lcs(input,list[i].university))listShow.push(list[i]);
                    }
                }else {
                    //字母的话先变成小写
                    input = input.toLowerCase();
                    if(lcs(input,list[i].pinyin))listShow.push(list[i]);
                }
            }
        };
        if(listShow.length > 0)return listShow;
        else return list;
    };
});

/*
lcs判断是否包含
 */
function lcs(input, listVal){
    var m = input.length,
        j = 0,
        i;
    for(i = 0;i < m; i++){
        try{
            var index = listVal.indexOf(input[i],j);
        }catch(e){

        }
        if(index < 0 )return false;
        j = index + 1;
    }
    return true;
}

app.controller('homeController', ["$scope", "$http", "User", "$location", "toaster", "universityCollegeSrv", "$rootScope", "UtilFactory", "$window",
    function ($scope, $http, User, $location, toaster, universityCollegeSrv, $rootScope, UtilFactory, $window) {
        //页面初始化初始化
        $scope.init = function () {
            $scope.schoolDisplay = ["1001.jpg", "1002.jpg", "1022.jpg", "1003.jpg", "9001.jpg", "1004.jpg", "2001.jpg", 
            "2002.jpg", "3001.jpg", "1011.jpg", "1006.jpg", "11002.jpg"]
            $scope.login = true;
            $scope.aliShow = false;
            $scope.hotClass = ['校园', '考证', '就业', '考研', '实习', '留学'];
            $scope.showClass = '校园';
            //$scope.getHotQuestion($scope.showClass);
            $scope.question_list = [];
            ///初始化用户为空,完善资料提交的信息
            $scope.nick_name = '';
            $scope.university = '';
            $scope.college = '';
            $scope.real_name = '';
            $scope.gender = '';
            $scope.captchaSrc = '';
            $scope.province = '';
            $scope.password = '';
            //定义学校学院初始化信息
            $scope.show_order = 0; // 0 for no selector, 1 for province, 2 for university, 3 for college
            $scope.prov_list = universityCollegeSrv.getProvList();
            $scope.universities = universityCollegeSrv.getUniversities();
            $scope.colleges = universityCollegeSrv.getColleges();
        };

        $scope.fetchProvince = function (callback) {
            if ($scope.province) {
                if (callback) callback();
                return;
            }
            var req = {
                url: 'http://api.map.baidu.com/location/ip?callback=JSON_CALLBACK',
                method: 'JSONP',
                params: {ak: 'GuAFsALAn9t53VzZYvkgz5Pc'}
            };
            $http(req).success(function (response) {
                if (response.status == 0) {
                    $scope.province  = response.address.split('|', 3)[1];
                }
                if (callback) callback();
            }).error(function (err) {
                if (callback) callback();
            });
        };

        //用户登录函数
        $scope.userLogin = function () {
            var i = 0;
            User.loginRequest($scope.username, $.md5($scope.password), function (data) {
                //这里会一直请求， 设置变量i可以使请求只是一次；
                if(i==0){
                    i++;
                    localStorage.email = "";
                    localStorage.pwd = "";
                    if ('err' in data && data['err'] == 0) {
                        if(data.allow_login){
                            var searchObject = $location.search();
                            if("return_url" in searchObject){
                                $location.url(searchObject["return_url"]);
                            }
                            else
                                $location.path('/qahome');
                            User.loginsuccess();
                        }else {
                            $scope.fetchProvince(function() {
                                if ($scope.province)
                                    $scope.chooseProvince($scope.province);
                                $('#registerModal').modal('show');
                            });
                        }
                    } else {
                        if ('info' in data) {
                            toaster.pop('error', '系统提示', data.info, true);
                        }
                    }
                }
            });
        };
        /*
        监测input输入框，自动输入密码时更新$scope.username和$scope.password,
        因为自动输入时angular获取不到密码值
         */
        $scope.$watch(function(){
            var password = document.getElementById('password').value;
            return password;
        },function(newVal,oldVal){
            if(newVal && (!$scope.password))$scope.password = newVal;
        });

        $scope.$watch(function(){
            var username = document.getElementById('username').value;
            return username;
        },function(newVal,oldVal){
            if(newVal && (!$scope.username))$scope.username = newVal;
        });

        $scope.loginPress = function(event){
            var keynum = event.keyCode;
            if (keynum == 13) {
              $scope.userLogin();
              return false;
            }
        };

        $scope.openQuestionDetail = function(q_id, ba_id) {
            var url = "/#/qadetail/" + q_id + "#" + ba_id;
            $window.open(url, "_blank");
        };

        $scope.captchaChange = function () {
            $scope.captchaSrc = '/captcha?a=' + Math.random();
        };

        $scope.register = function (argument) {
            if (!$scope.username || !$scope.passwd || !$scope.captcha) {
                $scope.register_info = "信息不完整";
                return;
            }
            var data = {
                user: $scope.username,
                captcha: $scope.captcha,
                passwd: $.md5($scope.passwd),
                real_name: htmlencode($scope.real_name)
            };

            $http({
                //url: 'http://211.149.208.82:9999/register',
                url: '/register_new',
                method: 'POST',
                data: $.param(data),
                xsrfCookieName: '_xsrf',
                xsrfHeaderName: 'X-CSRFToken',
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            })
                .success(function (response) {
                    if (0 == response.err) {
                        //toaster.pop('success', '系统提示', "请完善用户信息");
                        //console.log(response);
                        $scope.fetchProvince(function() {
                            if ($scope.province)
                                $scope.chooseProvince($scope.province);
                            $('#registerModal').modal('show');
                        });
                        $scope.register_info = "";
                    } else {
                        $scope.register_info = response.info;
                    }
                });
        };

        //选择学校，选择学院函数；
        $scope.isCurrentOrder = function (order) {
            return order == $scope.show_order;
        };

        $scope.universityClickHandler = function () {
            $scope.show_order = 1;
        };

        $scope.chooseProvince = function (province) {
            universityCollegeSrv.fetchUniversities(province);
            $scope.show_order = 2;
        };

        $scope.chooseUniversity = function (university) {
            $scope.university = university;
            $scope.college = "";
            universityCollegeSrv.fetchColleges(university);
            $scope.show_order = 3;
        };

        $scope.chooseCollege = function (college) {
            $scope.college = college;
            $scope.show_order = 0;
        };

        $scope.collegeClickHandler = function () {
            var currentUniversity = $scope.university;
            if (currentUniversity) {
                universityCollegeSrv.fetchColleges(currentUniversity);
                $scope.show_order = 3;
            }
        };

        //用户完善信息函数
        $scope.infoHandler = function () {
            var userData = {
                university: $scope.university,
                college: $scope.college,
                name: htmlencode($scope.real_name),
                gender: $scope.gender,
                nick: htmlencode($scope.real_name),//htmlencode($scope.nick_name),
                user: $scope.username
            };
            var req = {
                url: '/user/info/change',
                method: 'POST',
                data: userData,
                xsrfCookieName: '_xsrf',
                xsrfHeaderName: 'X-CSRFToken',
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            };
            $http(req).success(function (response) {
                if (response.err == 0) {
                    //去掉黑色边框
                    $('.modal-backdrop').hide();
                    $('.modal-open').css('overflow', 'auto').css('padding-right', '0px');
                    $('#registerModal').modal('hide');
                    toaster.pop('success','系统提示','注册成功，请登录', true);
                    $scope.login = true;
                    //$location.path('/qahome');
                } else toaster.pop('error', '系统提示', response.info, false);
            }).error(function (err) {
                //console.log('err is ' + err);
                toaster.pop('error', '系统提示', "网络出了点故障，请重试", false);
            });
        };


        //改变热门问题类别函数
        $scope.changeClass = function (_class) {
            $scope.showClass = _class;
            $scope.getHotQuestion(_class);
        };

        //获取home页面下每个大类的问题函数
        $scope.getHotQuestion = function (_class) {
            var req = {
                url: '/question/overview',
                method: 'GET',
                params: {class2: _class}
            };
            /*
            $http(req).success(function (response) {
                if (response.err == 0) {
                    $scope.question_list = response.question_list;
                    if ($scope.question_list.length > 5) {
                        $scope.question_list.length = 5;
                    };
                }
            }).error(function (err) {
                console.log(err);
            });
            */
        };

        $scope.resetPress = function(event){
            var keynum = event.keyCode;
            if (keynum == 13) {
              $scope.resetPasswordHandler();
              return false;
            }
        };

        //重置密码函数
        $scope.resetPasswordHandler = function () {
            var data = {"user": $scope.resetEmail};
            var req = {
                url: '/reset_password',
                method: 'GET',
                params: data
            };

            $http(req).success(function (data) {
                if ('type' in data && data['type'] == 0) {
                    toaster.pop('warning', "系统提示", "重置密码错误，请稍后重试", true);
                } else {
                    toaster.pop('success', "系统提示", "重置密码的链接已经发到您的邮箱。", false);
                    $('#resetPasswordModal').modal('hide');
                }
            }).error(function () {
                toaster.pop('warning', "系统提示", "服务器抛锚了，请稍后重试", true);
            });
        }

        $scope.trans_vote = function(vote){
            return UtilFactory.transVote(vote);
        }

    }]);


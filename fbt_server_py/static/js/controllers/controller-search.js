app.service('searchSev',['$http','loading','ThankSrv','$location',function($http, loading,ThankSrv,$location){
  //共享输入框
  this.keyword = $location.search().keyword;
  this.getMoreRes = function(url, page, keyword, callback){
    loading.show();
    var data = {};
    if(url=='/question/search'){
      data = {
        keyword: keyword,
        need_hot_keyword: 0,
        page: page
      };
    }else if(url=='/user/search'){
      data = {
        page: page,
        keyword: keyword
      };
    }else{
      data = {
        by_all: 1,
        keyword: keyword,
        page: page
      }
    };
    $http({
      url: url,
      method: 'GET',
      params: data
    })
    .success(function(response) {
      loading.hide();
      callback(0, response);
    })
    .error(function(data,status,headers,config) {
      // 失败处理
      loading.hide();
      callback(1);
    });
  };
}]);
app.controller('searchController', function($scope, $routeParams, $route, searchSev, toaster, downloadSrv, $location,QuestionAnswerSrv,$window,share,ThankSrv,StarService, interactiveService, User, collegesSrv,PreviewService){
  $scope.handle_fetch_results = function(response) {
    if (0 == response.err) {
      $scope.moreRes = [];
      //判定是user question ,course,如果是则改变字段
      if($scope.url=='/user/search'){
        angular.forEach(response.total_page, function(v, i){
          v.publisher_star_info = interactiveService.normalizePublisherStarInfo(v.publisher_star_info);
          $scope.moreRes.push(v);
        });
        $scope.page.total_page = response.user_list;
      }else if($scope.url=='/question/search'){
        angular.forEach(response.resource_list, function(v, i){
          v.publisher_star_info = interactiveService.normalizePublisherStarInfo(v.publisher_star_info);
          $scope.moreRes.push(v);
        });
        $scope.page.total_page = response.total_page;
      }else{
        angular.forEach(response.course_list, function(v, i){
          v.publisher_star_info = interactiveService.normalizePublisherStarInfo(v.publisher_star_info);
          $scope.moreRes.push(v);
        });
        $scope.page.total_page = response.total_page;
      }
      if ($scope.page.total_page > 1) {
        $scope.page.noPagination = true;
        $scope.page.items_per_page = $scope.moreRes.length;
        $scope.page.total_items = $scope.page.total_page * $scope.page.items_per_page;
      } else {
        $scope.page.noPagination = false;
      }
      if ($scope.moreRes.length){
        $scope.allRes[$scope.page.current_page] = $scope.moreRes; 
        $scope.page.noResult = false;
      }
      else
        $scope.page.noResult = true;   
    }
  };
  $scope.getMoreRes = function(){
    if($scope.page.current_page in $scope.allRes){
      $scope.moreRes = $scope.allRes[$scope.page.current_page];
    }
    else{
      searchSev.getMoreRes($scope.url, $scope.page.current_page, $scope.keyword, function(err, data){
        if(err){
          toaster.pop('error', '错误提示', "获取资源失败，请重试", true);
        }
        else{
          $scope.handle_fetch_results(data);
          //如果当前页是1并且没有赋值，那么赋值给$scope.hotKeyWords，同时减1，下次遇到page为1就不用赋值了
          if($scope.page.need_hotWords == 1&& $scope.page.current_page == 1){
            $scope.hotKeyWords = data.hot_keywords;
            $scope.page.need_hotWords--;
          }
        }
      });
    }
  };
  //进入主页函数
  $scope.goToUser = function(username){
    $window.open("/user_page?user=" + encode_username(username), "_blank");
  };
  //问答部分响应事件





  $scope.download = function(file_id,url){
    downloadSrv.download(file_id,url);
  };

  $scope.showPreviewModal  = PreviewService.showPreviewModal;

  $scope.viewDetailHandler = function(file_id){
    $location.path('/details').search({file_id:file_id});
  };

  $scope.courseResource = function(university, college, course, course_id) {
    $location.path('/course_resource').search({university: university, college: college, course: course, course_id: course_id});
  };

  $scope.downloadHandler = function(file_id, download_link) {
    downloadSrv.download(file_id, download_link);
  };

    $scope.previewHandler = function(res) {
    downloadSrv.preview(res);
  };
  //热门搜索响应事件
  $scope.hotWordHandler = function(keyword){
    $location.search({keyword:keyword});
    searchSev.keyword = keyword;
  };
  //关注用户函数
  $scope.follow = function (user) {
    StarService.followUser(user);
  };

  //用户是否是已关注
  $scope.isCareUser = function(user){
    var flag = StarService.isCareUser(user);
    return flag;
  };

  //显示之星的数组
  $scope.allStars = [
    {star:'is_postgraduate_star',value:'研'},
    {star:'is_certificate_star',value:'证'},
    {star:'is_campus_star',value:'校'},
    {star:'is_work_star',value:'业'},
    {star:'is_USA_star',value:'留'},
    {star:'is_internship_star',value:'习'}
  ];

  $scope.isCurrentTab = function(type) {
    var id;
    switch(type){
      case '问答': id = 3;break;
      case '学霸': id = 2;break;
      case '资源': id = 0;break;
      default :id = 1;break;
    }
    return id === $scope.currentType;
  };

  $scope.searchVal = function (id) {
    var value, url;
    switch (id){
      case '问答': value = 3;url='/qsearch';break;
      case '学霸': value = 2;url='/usersearch';break;
      case '资源': value = 0;url='/studysearch_resource';break;
      default :value = 1;url='/studysearch_course'; break;
    }
    $scope.currentType = value;
    if($scope.keyword){
      $location.path(url).search({keyword:$scope.keyword});
    }
  };

  $scope.fetchUserCard = function(userEmail) {
    QuestionAnswerSrv.fetchUserCard(userEmail).then(function() {
      $scope.cardUser = QuestionAnswerSrv.cardUser;
    });
  };
  //鼠标移开头像上时
  $scope.hideUserCard = function() {
    angular.element('#userinfor').hide();
  };
  //鼠标移到头像上时
  $scope.showUserCard = function(qId, userEmail) {
    $scope.fetchUserCard(userEmail);
    var top = angular.element('#' + qId).position().top + 'px';
    var left = angular.element('#' + qId).position().left + 'px';
    angular.element('#userinfor').css({
      'display': 'absolute',
      'top': top,
      'left': left
    }).show();
  };

  //监听热门搜素，如果有新值则变化热门搜索
  //$scope.$watch(function(){
  //  return searchSev.hotSearchList;
  //},function(newValue,oldValue){
  //  $scope.hotKeyWords = searchSev.hotSearchList;
  //});
  //如果用户刷新了页面，则重新加载热门搜索函数
  $scope.hotSearch = function(keyword){
    searchSev.hotSearch(keyword,function(err,data){
      if(!err){
        if(data.hot_keywords.length>6)data.hot_keywords.length=6;
        searchSev.hotSearchList = data.hot_keywords;
      }
    });
  };

  /*
  $scope.searchType
  函数功能：判断当前搜索的类型(有问答，学霸，资源，课程四种)，根据地址栏的path,
  给定不同的值；对应情况如下：
  path包含的值    对应的值
  qsearch          问答
  usersearch       学霸
  studysearch_resource    资源
   studysearch_course     课程
   */
  $scope.searchType = function(){
    var pathVal = $location.path();
    var value;
    switch(pathVal){
      case  '/qsearch': value = '问答';break;
      case  '/usersearch': value = '学霸';break;
      case  '/studysearch_resource':value = '资源';break;
      default:value ='课程' ;break;
    }
    return value;
  };

   $scope.init = function () {
     $scope.moreRes = [];
     $scope.allRes = {};
     $scope.page = {total_page: 0,
       current_page: 1,
       noResult: false,
       noPagination: false,
       max_size: 5,
       total_items: 0,
       items_per_page: 0,
       need_hotWords: 1
     };
    $scope.searchClass = $scope.searchType();
    $scope.searchClasses = ['问答','学霸','资源','课程'];
    $scope.cardUser = {}; //名片上的用户
    $scope.url = $route.current.$$route.url;
    $scope.showUnivColl = true;
    $scope.currentType = $route.current.$$route.currentType;
    $scope.keyword = $routeParams.keyword;
    $scope.getMoreRes();
     //if(!searchSev.hotSearchList)$scope.hotSearch($scope.keyword);
    $scope.qaList = [];
     $scope.isLogined = User.logined();
     $scope.hideSearch = false;

  };
  $scope.$watch(function(){
    return $scope.keyword;
  },function(newVal,oldVal){
    searchSev.keyword = $scope.keyword;
  });
  $scope.init();
  //这里是调用service，显示课程资源详情；
  $scope.viewCollegeResourceDetailsClickHandler = function(course,course_id){
    collegesSrv.viewCollegeResourceDetailsClickHandler(course,course_id);
  }
});

app.controller('studySearchController', function($scope, $location,$routeParams,searchSev) {
  /*
   $scope.searchType
   函数功能：获取地址栏的path，然后根据path返回不同的值，实现不同跳转搜索，对应如下：
   主页---qahome----跳转到问答
   课程---schools---跳转到课程
   星空---star---跳转到学霸
   悬赏---reward--跳转到资源
   */
  $scope.searchType = function(){
    var currentPath = $location.path();
    var dirUrl;
    //对path进行处理,
    currentPath = currentPath.split('/')[1];
    switch(currentPath){
      case 'star': dirUrl = '/usersearch';break;
      case 'schools': dirUrl = '/studysearch_course';break;
      case 'reward': dirUrl = '/studysearch_resource';break;
      default : dirUrl = 'qsearch'
    }
    return dirUrl;
  };
  $scope.$watch(function(){
    return searchSev.keyword;
  },function(newValue,oldValue){
    $scope.keyword = searchSev.keyword;
  });
  $scope.search = function() {
    if(!$scope.keyword)return;
    if(($location.path().indexOf('search'))>-1){
      $location.search({keyword:$scope.keyword});
    }else {
      var dirUrl = $scope.searchType();
      $location.path(dirUrl).search({keyword:$scope.keyword});
    }
  };

  $scope.enter = function(ev) {
    if (ev.keyCode == 13&&($scope.keyword)) {
      $scope.search();
    }
  };
});

/* vim: set sw=2 ts=2 : */
'use strict';

function htmlencode(s){  
    var div = document.createElement('div');  
    div.appendChild(document.createTextNode(s));  
    return div.innerHTML;  
};

/*
function getCookie(name) {
    var c = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return c ? c[1] : undefined;
}
*/

// Your app's root module...
var app = angular.module('fbtApp', ['selector'], function($httpProvider, $interpolateProvider) {
  // Use x-www-form-urlencoded Content-Type
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');

  /**
   * The workhorse; converts an object to x-www-form-urlencoded serialization.
   * @param {Object} obj
   * @return {String}
   */ 
  var param = function(obj) {
    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;
      
    for(name in obj) {
      value = obj[name];
        
      if(value instanceof Array) {
        for(i=0; i<value.length; ++i) {
          subValue = value[i];
          fullSubName = name + '[' + i + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value instanceof Object) {
        for(subName in value) {
          subValue = value[subName];
          fullSubName = name + '[' + subName + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value !== undefined && value !== null)
        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
    }
      
    return query.length ? query.substr(0, query.length - 1) : query;
  };

  // Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data) {
    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
  }];
});

/** copy from Askbot in fbtApp
 *  2015.07.07 21:52
 */

/*
app.service("Askbot",  ['$rootScope', '$http',
  function ($rootScope, $http) {

  var askbotCSRFToken = null;
  this.askbotCSRF = function () {
    if (askbotCSRFToken == null) {
      $http.jsonp('http://ask.friendsbt.com/get_token?callback=JSON_CALLBACK')
      .success(function (data) {
        askbotCSRFToken = data;
      });
    }
    return askbotCSRFToken;
  };
  this.askbotCSRF();

  this.askbotLogin = function (username, password) {
    $.ajax({
      url: "http://ask.friendsbt.com/account/signin/",
      type: "POST",
      data: {
        login_provider_name: "local",
        next:'/',
        persona_assertion:'',
        openid_login_token:'',
        password_action:'login',
        login_with_password:"Sign in",
        "username": username,
        "password": password
      },
      headers: {
        "X-CSRFToken": askbotCSRFToken
      },
      xhrFields: {
        withCredentials: true
      },
      success: function(data, textStatus, xhr) {
        console.log('askbot login success');
      },
      statusCode: {
        302: function() {
          console.log('askbot login success, 302');
        }
      }
    });
  };

  this.askbotSignup = function (postData) {
      $.ajax({
        url: "http://ask.friendsbt.com/account/signup/",
        type: "POST",
        data: postData,
        headers: {
          "X-CSRFToken": askbotCSRFToken
        },
        xhrFields: {
          withCredentials: true
        },
        success: function(data, textStatus, xhr) {
          console.log(textStatus);
        }
      });
    };
}]);
*/

app.controller('RegisterController', function($scope, $http, selector) {
  $scope.register_info = "";
/*
  $scope.colleges_list = [];
  $scope.$watch('school', function(new_school){
    console.log(new_school);
    if (new_school && new_school.originalObject) {
      $scope.colleges_list = $scope.colleges[new_school.originalObject];
      console.log($scope.colleges_list);
    } else {
      $scope.colleges_list = [];
    }
  });
*/

  $scope.submit = function  (argument) {
    var school = $scope.school;
    var college = $scope.college;
    /*var askbotSignupData = {
      username: htmlencode($scope.nick),
      email: $scope.user,
      password1: $.md5($scope.passwd),
      password2: $.md5($scope.passwd)
    };
    */

    $http({
      //url: 'http://211.149.208.82:9999/register',
      url: '/register',
      method: 'POST',
      xsrfCookieName: '_xsrf',
      xsrfHeaderName: 'X-CSRFToken', 
      data: {
        user: $scope.user,
        nick: htmlencode($scope.nick),
        gender: $scope.gender,
        passwd: $.md5($scope.passwd),
        school: htmlencode(school),
        college: htmlencode(college),
        name: htmlencode($scope.real_name)
      }
    //headers: { 'X-CSRFToken' : getCookie("_xsrf")}
    })
    .success(function(response) {
      if (0 == response.err) {
        //Askbot.askbotSignup(askbotSignupData);
        $scope.register_info = "注册成功，请登录！资源下载时某些浏览器可能会阻止下载弹窗，请在地址栏右侧进行设置。";
      } else {
        $scope.register_info = response.info;
      }
    });
  };

  $scope.selectCollegeClickHandler = function() {
    var title = '选择学校与学院';
    var groups = [
      '北京',
      '上海',
      '黑龙江',
      '吉林',
      '辽宁',
      '天津',
      '安徽',
      '江苏',
      '浙江',
      '陕西',
      '湖北',
      '广东',
      '湖南',
      '甘肃',
      '四川',
      '山东',
      '福建',
      '河南',
      '重庆',
      '云南',
      '河北',
      '江西',
      '山西',
      '贵州',
      '广西',
      '内蒙古',
      '宁夏',
      '青海',
      '新疆',
      '海南',
      '西藏',
      '香港',
      '澳门',
      '台湾',
    ];
    var groupWidth = '50px';
    var entityWidth = '120px';
    var entityCallback = function(group, callback) {
      // fetch from server
      $http({
        url: '/get_university',
        method: 'GET',
        params: {
          province: group
        }
      })
      .success(function(response) {
        if (0 === response.err) {
          var entities = response.list;
          callback(entities);
        }
      })
      .error(function(data,status,headers,config) {
        // 失败处理
        alert('页面加载失败，请重试！')
      });
    };

    var universityEntityCallback = function(university, callback) {
      // fetch from server
      $http({
        url: '/get_university',
        method: 'GET',
        params: {
          university: university
        }
      })
      .success(function(response) {
        if (0 === response.err) {
          var entities = response.list;
          callback(entities);
        }
      })
      .error(function(data,status,headers,config) {
        // 失败处理
        alert('页面加载失败，请重试！')
      });
    };

    var submitCallback = function(entity, college_entity) {
      $scope.school = entity;
      $scope.college = college_entity;
      //alert('submitting entity: ' + entity);
    };
    selector.show(title, groups, groupWidth, entityWidth, entityCallback, universityEntityCallback, submitCallback);
  };
});

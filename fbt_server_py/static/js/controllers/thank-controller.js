/*********************************************************************************
 *     File Name           :     thank-controller.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-11-08 11:14]
 *     Last Modified       :     [2015-12-09 22:26]
 *     Description         :     奖励问答的controller
 **********************************************************************************/

app.controller('ThankController', ['$scope', 'ThankSrv','$rootScope',
  function($scope, ThankSrv,$rootScope) {
      $scope.total_coins = ThankSrv.total_coins;
      $scope.$watch(function(){return $rootScope.total_coins;},function(newVal,oldVal){$scope.total_coins = $rootScope.total_coins;});

      $scope.user = ThankSrv.user;


    $scope.answerThanks = function() {
      ThankSrv.answerThanks($scope.thankAnswerFB);
    };


    $scope.questionThanks = function() {
      ThankSrv.questionThanks($scope.thankQuestionFB);
    };



  }
]);

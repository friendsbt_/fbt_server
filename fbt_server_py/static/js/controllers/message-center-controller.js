/*
 这是私信聊天的service部分，为了方便以后都可以调用
 */
app.service('chatService',function($http,$location,$rootScope,toaster){
    var self = this;
    self.userId = '';
    self.errorSleepTime = 500;
    self.cursor = null;
    self.host = $location.host();
    self.url = "http://" + self.host + ":9876/message";
    self.getChatMsg = {};
    //对每一条消息进行分类，
    self.msgType = '';
    //获取关注的用户，这即时聊天消息集合
    self.getFollowUsers = function(callback){
        $http.get('/user/following').success(function(response){
            if(response.err == 0){
                callback(response.following_users);
            }
        }).error(function(error){
            console.log(error);
        })
    };

    //处理消息，进行分类并且赋值给localStorage
    self.messageProcess = function(array){
        //获取cookie中user_token
        self.cookie = localStorage.r;
        self.Cookie = "user_token" + "=" + self.cookie;
        self.username = $rootScope.username;
        var thumbUPArrays = [],
            questionBeAnswered = [],
            rewardArrays = [],
            judgeArrays = [],
            careUsers = [],
            privateLetterUsers = [];
        //获取关注的人
        self.getFollowUsers(function(data){
            careUsers = data;
            for(var i = 0; i < array.length; i ++){
                if(array[i].user_from == 'system'){
                    //判断消息判断是否系统消息，是的话再根据type值细分
                    switch(array[i].type){
                        //社交消息加一
                        case -31:thumbUPArrays.push(array[i]);break;
                        //课程消息消息加一
                        case -21:
                        case -22:
                        case -23:judgeArrays.push(array[i]);break;
                        //问答消息息加一
                        case -1:
                        case -2:
                        case -3:
                        case -4:
                        case -5:
                        case -6:
                        case -7:questionBeAnswered.push(array[i]);break;
                        //其他消息加一
                        case 0:rewardArrays.push(array[i]);break;
                    }
                }else{

                    //暂时这样处理，如果没有user_info_of_user_to,直接执行处理下一条
                    if(!array[i].hasOwnProperty('more_info_of_user_to'))continue;
                    //处理200条消息中的私信，进行分类
                    if(array[i].user_from != array[i].user_to){
                        var flag = 0;
                        var length = privateLetterUsers.length;
                        var temp = (array[i].user_from == self.username)? array[i].user_to:array[i].user_from;
                        for(var j = 0;j<length;j++){
                            if(privateLetterUsers[j].user == temp){
                                if(array[i].is_read == 0)privateLetterUsers[j].newMsgLength++;
                                privateLetterUsers[j].msgLength++;
                                privateLetterUsers[j].chatMessage.push(array[i]);
                                flag = 1;
                            }
                        }
                        //没有找到user_from
                        if(flag == 0)
                        {
                            var arr = {};
                            var user = '';
                            var careFlag = 0;
                            //如果发送方和接受方一样，说明是错误数据，终止
                            if(array[i].user_from == self.username){
                                //自己是发送方，得需要去关注列表找关注的人，获取昵称，id，名字什么的
                                user = array[i].user_to;
                                arr = {
                                    "user_from":user,
                                    "uid":array[i].more_info_of_user_to.uid,
                                    "real_name":array[i].more_info_of_user_to.real_name,
                                    "newMsgLength":0,
                                    "msgLength":1,
                                    "user":user
                                };
                                careFlag = 1;
                            }else{
                                //自己是接收方，直接获取服务器的信息即可
                                user = array[i].user_from;
                                arr = {
                                    "user_from":user,
                                    "uid":array[i].more_info_of_user_from.uid,
                                    "real_name":array[i].more_info_of_user_from.real_name,
                                    "newMsgLength":0,
                                    "msgLength":1,
                                    "user":user
                                };
                                //接受者新建列表直接从发送消息那获取就行，默认careFlag值为1
                                careFlag = 1;
                            }
                            if(careFlag == 1){
                                arr.chatMessage = [];
                                if(array[i].is_read == 0)arr.newMsgLength++;
                                arr.chatMessage.push(array[i]);
                                privateLetterUsers.push(arr);
                            }
                        }
                    }
                }
                //TDD把私信数组汇总到一个列表里面，存到localStorage里面
            }
            //点赞的消息列表和未读消息数
            localStorage.thumbUpArraysL = self.countUnMarkMsg(thumbUPArrays);
            localStorage.thumbUpArrays = JSON.stringify(self.changeMsgReadPosition(thumbUPArrays));
            //发布问题被回答和未读消息数
            localStorage.questionBeAnsweredL = self.countUnMarkMsg(questionBeAnswered);
            localStorage.questionBeAnswered = JSON.stringify(self.changeMsgReadPosition(questionBeAnswered));
            //别人奖励的积分消息和未读消息数
            localStorage.rewardArraysL = self.countUnMarkMsg(rewardArrays);
            localStorage.rewardArrays = JSON.stringify(self.changeMsgReadPosition(rewardArrays));
            //评论消息
            localStorage.judgeArraysL = self.countUnMarkMsg(judgeArrays);
            localStorage.judgeArrays = JSON.stringify(self.changeMsgReadPosition(judgeArrays));
            //关注用户列表
            localStorage.careUsers = JSON.stringify(careUsers);
            //这里应该把200条消息中的用户未读消息，给locaStorage.privateLetterUsers，包括聊天记录
            localStorage.privateLetterUsers = JSON.stringify(privateLetterUsers);
            //对私信消息列表进行排序
            if(privateLetterUsers.length > 2){
                self.sortPrivateLetterUsers();
            }
            //处理完200条消息，才正式开始监听消息
            self.getMessage($rootScope.username,localStorage.r);
        });
    };

    //改变未读消息的位置；方法：首先将未读消息汇总，然后加入到已读消息前面
    self.changeMsgReadPosition = function(arrays){
        var unReadArrays = [];
        if(arrays.length<1)return arrays;
        var j;
        for(var i = 0;i< arrays.length; i++){
            if(arrays[i].is_read == 0){
                //先加再删除，要不发生变化了；
                j = i;
                unReadArrays.push(arrays[i]);
                arrays.splice(i,1);
                i = j;
                i--;
            }
        }
        //未读放在已读前面，连接
        if(unReadArrays.length>0){
            //todo 未读消息排序
            for(var i = unReadArrays.length-1; i>-1;i--)arrays.unshift(unReadArrays[i]);
        }
        return arrays;
    };

    //统计未读消息总数函数
    self.countUnMarkMsg = function(array){
        var count = 0;
        if(array.length == 0)return 0 ;
        for(var i = 0; i<array.length; i++){
            if(array[i].is_read == 0)count++;
        }
        return count;
    };

    //向服务器发送消息
    self.sendMessage = function(userData,callback){
        var req = {
            url:self.url,
            method:'POST',
            Cookie: self.Cookie,
            data: userData,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }

        };
        $http(req).success(function(response){
            switch(response.err){
                //用户在线，用户接收到了消息，消息发送成功后给用户发送一份
                case 0:
                    toaster.pop("success","系统提示","消息发送成功",true);
                    callback(response);
                    break;
                //参数错误，
                case 1:
                    toaster.pop("error","系统提示","对不起，参数错误",true);
                    break;
                //用户不在线，但是消息发送成功，消息发送成功后给用户发送一份
                case 3:
                    callback(response);
                    toaster.pop('waring',"系统提示","亲，该用户不在线呢，不过消息已经投递给Ta了",true);
                    break;
            }
        }).error(function(err){
            console.log("error is " + err);
        });

    };

    //从服务器获取新的消息
    self.getMessage = function(user,user_token,callback){
        var args = {
            "user": user,
            "user_token": user_token
        };
        //if(this.cursor)args.cursor = this.cursor;
        var req = {
            url:self.url,
            method: 'GET',
            Cookie:self.Cookie,
            params:args
        };
        $http(req).success(function(response,callback){
            if(response.err == 0){
                //获取消息成功后TDD
                try{
                    //TDD把获取的消息通过传参给到客户端
                    self.synNewMsg(response.message);
                } catch(e){
                    console.log("response error is " + e);
                    self.errorSleepTime *=2;
                    console.log("getMessage error: sleeping for" + self.errorSleepTime + "ms");
                    window.setTimeout(self.getMessage($rootScope.username,localStorage.r),self.errorSleepTime);
                }
                self.errorSleepTime = 500;
                window.setTimeout(self.getMessage($rootScope.username,localStorage.r),0);
            }
        }).error(function(error){
            console.log("error is " + error);
            self.errorSleepTime*=2;
            window.setTimeout(self.getMessage($rootScope.username,localStorage.r),self.errorSleepTime);

        });
    };

    //将用户收到的新消息同步过来
    self.synNewMsg = function(msg){
        var thumbUPArrays = JSON.parse(localStorage.thumbUpArrays),
            questionBeAnswered = JSON.parse(localStorage.questionBeAnswered),
            rewardArrays = JSON.parse(localStorage.rewardArrays),
            judgeArrays = JSON.parse(localStorage.judgeArrays),
            privateLetterUsers = JSON.parse(localStorage.privateLetterUsers);
        /*
         TDD接收新消息后，信封那加一，
         对新消息进行分类；
         */
        $rootScope.message_count++;
        if(msg.user_from == 'system'){
            //根据content中内容判断是什么消息
            //根据type类型判断消息
            switch(msg.type){
                //社交消息加一
                case -31:thumbUPArrays.unshift(msg);break;
                //课程消息消息加一
                case -21:
                case -22:
                case -23:judgeArrays.unshift(msg);break;
                //问答消息息加一
                case -1:
                case -2:
                case -3:
                case -4:
                case -5:
                case -6:
                case -7:questionBeAnswered.unshift(msg);break;
                //其他消息加一
                case 0:rewardArrays.unshift(msg);break;
            }
        }else{
            //对新消息进行处理
            var flag = 0;
            var length = privateLetterUsers.length;
            //如果发送方和接收方一致，说明错误；
            if(msg.user_from != msg.user_to){
                var temp = (msg.user_from == self.username)? msg.user_to:msg.user_from;
                for(var i = 0;i<length;i++){
                    if(privateLetterUsers[i].user == temp){
                        if(msg.is_read == 0)privateLetterUsers[i].newMsgLength++;
                        privateLetterUsers[i].chatMessage.unshift(msg);
                        privateLetterUsers[i].msgLength++;
                        flag = 1;
                    }
                }
                //如果flag是0，说明私信列表不存在，新建数字并添加到列表；
                var arr = {};
                var user_info = 0;
                if(flag == 0){
                    //新建私信人数组，并加入到私信列表
                    var array;
                    if(msg.user_from == self.username){
                        //自己是发送方
                        array = msg.user_to;
                        arr = {
                            "user_from":msg.user_to,
                            "uid":msg.more_info_of_user_to.uid,
                            "real_name":msg.more_info_of_user_to.real_name,
                            "newMsgLength":0,
                            "msgLength":1,
                            "user":array
                        };
                        user_info = 1;
                    }else{
                        //自己是接受方
                        arr = {
                            "user_from":msg.user_from,
                            "uid":msg.more_info_of_user_from.uid,
                            "real_name":msg.more_info_of_user_from.real_name,
                            "newMsgLength":0,
                            "msgLength":1,
                            "user":msg.user_from
                        };
                        user_info = 1;
                    }
                    if(user_info == 1){
                        if(msg.is_read == 0)arr.newMsgLength++;
                        arr.chatMessage = [];
                        arr.chatMessage.unshift(msg);
                        privateLetterUsers.unshift(arr);
                    }
                }
            }
        }
        //点赞的消息列表和未读消息数
        localStorage.thumbUpArraysL = self.countUnMarkMsg(thumbUPArrays);
        localStorage.thumbUpArrays = JSON.stringify(thumbUPArrays);
        //发布问题被回答和未读消息数
        localStorage.questionBeAnsweredL = self.countUnMarkMsg(questionBeAnswered);
        localStorage.questionBeAnswered = JSON.stringify(questionBeAnswered);
        //别人奖励的积分消息和未读消息数
        localStorage.rewardArraysL = self.countUnMarkMsg(rewardArrays);
        localStorage.rewardArrays = JSON.stringify(rewardArrays);
        //评论消息
        localStorage.judgeArraysL = self.countUnMarkMsg(judgeArrays);
        localStorage.judgeArrays = JSON.stringify(judgeArrays);
        //私信聊天记录列表privateLetterUsers
        localStorage.privateLetterUsers = JSON.stringify(privateLetterUsers);
        //对私信消息排序
        if(privateLetterUsers.length > 1){
            self.sortPrivateLetterUsers();
        }
    };

    //对localStorage中的私信列表按照新消息总数进行排序
    self.sortPrivateLetterUsers = function(){
        var array = JSON.parse(localStorage.privateLetterUsers);
        array.sort(function(a,b){
            if(a.newMsgLength > b.newMsgLength)return -1;
            else if(a.newMsgLength < b.newMsgLength)return 1;
            else return 0;
        });
        localStorage.privateLetterUsers = JSON.stringify(array);
    }
});
/**
 * Created by yulei on 2015/12/27.
 * 消息中心控制器js
 */
app.controller('messageCenterController', function($scope, $http, $rootScope, toaster,chatService, User,$window){
    $scope.init = function(){
        $scope.username = $rootScope.username;
        $scope.currentArrays = []; //显示当前的数组消息
        $scope.careUsers = [];//用户关注列表
        $scope.keyword = '';
        $scope.listType = [
            { "type":"社交消息","value":"社交消息："},  //0
            { "type":"课程消息","value":"课程消息："},
            { "type":"问答消息","value":"问答消息："},//2
            { "type":"关注问题","value":"关注问题的消息："},
            { "type":"通知消息","value":"通知消息："},//4
            { "type":"最近私信","value":"最近私信："},
            { "type":"关注的人","value":"关注的人："},//6
            { "type":"搜索结果","value":"搜索结果："}
        ];
        $scope.currentNav = $scope.listType[2].type;
        $scope.message = {
            title: $scope.listType[2].value,
            type: 1
        };
        //从localStorage读取数据
        $scope.privateListHide = true;//刚开始私信列表为隐藏的

        $scope.thumbUpArrays = $scope.changeString(localStorage.thumbUpArrays);
        $scope.thumbUpArraysL = localStorage.thumbUpArraysL;

        $scope.rewardArrays = $scope.changeString(localStorage.rewardArrays);
        $scope.rewardArraysL = localStorage.rewardArraysL;

        $scope.questionBeAnswered = $scope.changeString(localStorage.questionBeAnswered);
        $scope.questionBeAnsweredL = localStorage.questionBeAnsweredL;

        $scope.judgeArrays = $scope.changeString(localStorage.judgeArrays);
        $scope.judgeArraysL = localStorage.judgeArraysL;

        $scope.careUsers = $scope.changeString(localStorage.careUsers);
        //关注列表分页10个一个
        $scope.time = 10;

        //系统消息每页显示10个，然后是加载更多
        $scope.privateLetterUsers = JSON.parse(localStorage.privateLetterUsers);
        $scope.sendMsgTo = '';//消息发送给谁
        $scope.currentUser = '';//当前显示的私信用户
        $scope.userId = '';//记录打开当前私信用户的id，方便删除用户信时，根据id实现更新；
        $scope.changeNav($scope.listType[2].type);
    };

    //关注用户列表分页--系统消息分页，公用一个函数
    $scope.divCareUsers = function(){
        var length,arrays;
        switch($scope.divType){
            case 0:
                arrays = $scope.thumbUpArrays;//0
                length = arrays.length;
                break;
            case 1:
                arrays = $scope.judgeArrays;  //1
                length = arrays.length;
                break;
            case 2:
                arrays = $scope.questionBeAnswered; //2
                length = arrays.length;
                break;
            case 4:
                arrays = $scope.rewardArrays; //4
                length = arrays.length;
                break;
            default:
                arrays = $scope.careUsers;
                length = arrays.length;
        }
        var end = ($scope.loadMoreTime*$scope.time > length)? length:$scope.loadMoreTime*$scope.time;
        if(end == length)$scope.loadCareUsers = false;
        else $scope.loadCareUsers = true;
        $scope.currentArrays = arrays.slice(0,end);
        $scope.loadMoreTime++;
    };

    //加载最近私信用户，如果用户列表不为空
    $scope.loadPrivateLetter = function(){
        $scope.loadTime = 1;
        $scope.loadMore();
    };

    //用watch实时监听四个函数
    $scope.$watch(function(){
        try{
            return localStorage.thumbUpArrays;
        }catch(e){

        }
    },function(newVal,oldVal){
        if(localStorage.thumbUpArrays){
            //对消息进行已读未读更新
            $scope.thumbUpArrays = chatService.changeMsgReadPosition(JSON.parse(localStorage.thumbUpArrays));
            $scope.thumbUpArraysL = localStorage.thumbUpArraysL;
            if($scope.message.title.split('：')[0]==$scope.listType[0].type){
                $scope.changeNav($scope.message.title.split('：')[0]);
            }
        }
    },true);

    $scope.$watch(function(){
        try{
            return localStorage.judgeArrays;
        }catch(e){

        }
    },function(newVal,oldVal){
        if(localStorage.judgeArrays){
            $scope.judgeArrays = chatService.changeMsgReadPosition(JSON.parse(localStorage.judgeArrays));
            $scope.judgeArraysL = localStorage.judgeArraysL;
            if($scope.message.title.split('：')[0]==$scope.listType[1].type){
                $scope.changeNav($scope.message.title.split('：')[0]);
            }
        }
    },true);

    $scope.$watch(function(){
        try{
            return localStorage.questionBeAnswered;
        }catch(e){

        }
    },function(newVal,oldVal){
        if(localStorage.questionBeAnswered){
            $scope.questionBeAnswered = chatService.changeMsgReadPosition(JSON.parse(localStorage.questionBeAnswered));
            $scope.questionBeAnsweredL = localStorage.questionBeAnsweredL;
            if($scope.message.title.split('：')[0]==$scope.listType[2].type){
                $scope.changeNav($scope.message.title.split('：')[0]);
            }
        }
    },true);

    $scope.$watch(function(){
        try{
            return localStorage.rewardArrays;
        }catch(e){

        }
    },function(newVal,oldVal){
        if(localStorage.rewardArrays){
            $scope.rewardArrays = chatService.changeMsgReadPosition(JSON.parse(localStorage.rewardArrays));
            $scope.rewardArraysL = localStorage.rewardArraysL;
            if($scope.message.title.split('：')[0]==$scope.listType[4].type){
                $scope.changeNav($scope.message.title.split('：')[0]);
            }
        }
    },true);

    //用watch监听localStorage.careUsers变化，更新关注的人，最近私信；
    $scope.$watch(function(){
        try{
            return localStorage.careUsers;
        }catch(e){
        }
    }, function(newVal, oldVal){
        //如果watch监测careUsers值发生变化了
        $scope.careUsers = JSON.parse(localStorage.careUsers);
        if($scope.message.title.split('：')[0] == $scope.listType[6].type){
            $scope.changeNav($scope.message.title.split('：')[0]);
        }
    },true);

    //用watch监听localStorage.privateLetterUsers
    $scope.$watch(function(){
        try{
            return JSON.parse(localStorage.privateLetterUsers);
        }
        catch(e){
        }
    },function(newVal,oldVal){
        if(localStorage.privateLetterUsers){
            $scope.privateLetterUsers = JSON.parse(localStorage.privateLetterUsers);
            /*
            当前是否有新消息，如果有的话，添加点
             */
            $scope.hasNewMsg = false;
            var length = $scope.privateLetterUsers.length;
            for(var i = 0; i<length;i++){
                if($scope.privateLetterUsers[i].newMsgLength > 0)$scope.hasNewMsg = true;
            }
            /*
             这里来新消息时，需要判断是否当前聊天用户，如果是当前聊天用户直接进行已读处理
             */
            var length = $scope.privateLetterUsers.length;
            for(var i = 0; i < length; i++){
                if($scope.privateLetterUsers[i].uid == $scope.sendMsgTo.uid && $scope.privateLetterUsers[i].newMsgLength > 0){
                    $scope.chatMessage($scope.privateLetterUsers[i],'1');
                }
            }
            $scope.loadMore();
        }
    },true);

    //最近私信的人,判断每个联系人中newMsgLength长度，如果为0，则不显示
    $scope.privateLetter = function(array){
        var list = [];
        for(var i = 0;i < array.length;i++){
            if(array[i].newMsgLength > 0){
                var privateList = {
                    "uid":array[i].uid,
                    "real_name":array[i].real_name,
                    "newMsgLength":array[i].newMsgLength,
                    "MsgLength":array[i].MsgLength
                };
                list.push(privateList);
            }
        }
        return  list;
    };

    //格式转化，将localStorage中字符串消息转换为object
    $scope.changeString = function(array){
        return JSON.parse(array);
    };
    //object转为string；
    $scope.changeJson = function(array){
        return JSON.stringify(array);
    };

    //列表转换样式函数
    $scope.changeNav = function(id){
        $scope.loadMoreTime = 1;
        switch(id){
            //这里不同的divType值是为了对不同消息进行不同分组区分；
            case $scope.listType[0].type:
                $scope.rollTop();
                $scope.message.title = $scope.listType[0].value;
                $scope.message.type = 1;
                $scope.divType = 0;
                $scope.sendMsgTo = '';
                break;
            case $scope.listType[1].type:
                $scope.rollTop();
                $scope.message.title = $scope.listType[1].value;
                $scope.message.type = 1;
                $scope.sendMsgTo = '';
                $scope.divType = 1;
                break;//评论消息
            case $scope.listType[2].type:
                $scope.rollTop();$scope.message.title = $scope.listType[2].value;
                $scope.message.type = 1;
                $scope.sendMsgTo = '';
                $scope.divType = 2;
                break;
            case $scope.listType[4].type:
                $scope.rollTop();
                $scope.message.title = $scope.listType[4].value;
                $scope.message.type = 1;
                $scope.divType = 4;
                $scope.sendMsgTo = '';
                break;
            case $scope.listType[5].type:
                if($scope.privateLetterUsers.length == 0){
                    $scope.message.type = 0;
                    $scope.message.title = $scope.listType[5].value;
                }
                $scope.rollTop();
                $scope.loadPrivateLetter();
                $scope.privateListHide = !$scope.privateListHide;
                $scope.type = 5;
                break;//私信
            default :
                $scope.rollTop();
                $scope.divType = 5;
                $scope.message.title = $scope.listType[6].value;
                $scope.message.type = 2;
                $scope.sendMsgTo = '';
                break;//关注的人
        }
        $scope.currentNav = id;
        //调用分页函数
        $scope.divCareUsers();
    };

    /*
     进入用户主页，传进的参数是用户邮箱
     */
    $scope.goToUser = function(email){
        $window.open("/user_page?user=" + encode_username(email),"_blank");
    };

    //加载更多的私信用户
    $scope.loadMore = function(flag){
        var num = 10;
        var length = $scope.privateLetterUsers.length;
        var end = ($scope.loadTime*num < length)? $scope.loadTime*num:length;
        if(end==length )$scope.moreUserShow = false;
        else $scope.moreUserShow = true;
        if(!flag)$scope.loadTime++;
        $scope.privateLetterUsersL = $scope.privateLetterUsers.slice(0,end);
    };

    //消息全部标为已读
    $scope.markAsReadAll = function(){
        if($scope.currentArrays.length > 0)
            for(var i=0;i<$scope.currentArrays.length;i++)
            {
                if($scope.currentArrays[i].is_read == 0){
                    $scope.markAsRead($scope.currentArrays[i]);
                }
            }
    };

    //用户消息标为已读
    $scope.markAsRead = function(msg,type){
        var req = {
            url:'/user/message/read',
            method:'GET',
            params:{msg_id:msg.id}
        };
        $http(req).success(function(response){
            if(response.err == 0){
                if(type=='synPrivateLetter')return;
                $scope.synMsg(msg.id);
            }
        }).error(function(error){
            console.log(error);
        });
    };

    //删除私信消息
    $scope.deleteMsg = function(msg){
        var req = {
            url:'/user/message/delete',
            method:'GET',
            params:{msg_id: msg.id}
        };
        $http(req).success(function(response){
            if(response.err == 0 ){
                //同步删除消息
                $scope.synMsg(msg.id,msg);
            }
        }).error(function(error){
            console.log('delete msg error is' + error);
        });
    };

    //显示私信用户聊天信息
    $scope.chatMessage = function(user,type){
        $scope.rollTop();
        $scope.type = 0;
        //标记是否在最近私信列表那里找到了，如果找到了，flag为1，否则还得新建私信用户;
        var flag = 0;
        //如果私信那里用户未读消息不为0，进行已读处理,type表示是否是从最近私信那里发出的消息，1表示是，如果不是表示是从关注的人那么发送的；
        if(type){
            if(user.newMsgLength!= 0){
                for(var j =0;j<$scope.privateLetterUsers.length;j++){
                    if($scope.privateLetterUsers[j].uid == user.uid){
                        var temp = $scope.privateLetterUsers[j].newMsgLength;
                        $scope.privateLetterUsers[j].newMsgLength = 0;
                        if($rootScope.message_count-temp>0)$rootScope.message_count = $rootScope.message_count-temp;
                        else $rootScope.message_count = 0;
                        //同步已读消息
                        var chatMessage = $scope.privateLetterUsers[j].chatMessage;
                        for(var i = 0;i<chatMessage.length;i++){
                            if(chatMessage[i].is_read == 0)$scope.markAsRead(chatMessage[i],'synPrivateLetter');
                        }
                        $scope.privateLetterUsers[j].chatMessage = chatMessage;
                    }
                }
                //未读消息清空，消息数响应减少
                localStorage.privateLetterUsers = JSON.stringify($scope.privateLetterUsers);
            }
        }
        //根据用户的uid唯一显示私信列表用该用户的聊天记录
        for(var k = 0;k<$scope.privateLetterUsers.length;k++){
            if($scope.privateLetterUsers[k].uid == user.uid){
                $scope.sendMsgTo = $scope.privateLetterUsers[k];
                $scope.userId = $scope.privateLetterUsers[k].uid;
                $scope.userChatMsg = $scope.privateLetterUsers[k].chatMessage;
                //将用户聊天记录加载更多
                flag = 1;
            }
        }
        //flag值为0是代表，这是第一次从关注人那里发信息;
        if(flag == 0){
            $scope.sendMsgTo = user;
            $scope.userId = user.uid;
            $scope.userChatMsg = [];
        };
        $scope.synChat();
        $scope.currentNav = '';//去掉问答消息点击样式
        $scope.message.type = 3;
    };

    //同步问答消息函数
    $scope.synMsg = function(id,msg){
        var currentArrays;
        //消息已读同步到当前页面
        switch($scope.message.title){
            //0点赞，同步当前页面的已读消息
            case $scope.listType[0].value:
                //判断msg参数是否为空，如果不为空则是删除消息
                if(msg){
                    currentArrays = $scope.synDeleteMsg($scope.thumbUpArrays,msg);
                    if(msg.is_read == 0)$scope.thumbUpArraysL--;
                }else{
                    currentArrays = $scope.synMarkMsg($scope.thumbUpArrays,id);
                    if($scope.thumbUpArraysL > 0)$scope.thumbUpArraysL--;
                }
                localStorage.thumbUpArrays = $scope.changeJson(currentArrays);
                localStorage.thumbUpArraysL = $scope.thumbUpArraysL;
                break;
            //1评论，同步当前已读消息
            case $scope.listType[1].value:
                //TDD
                if(msg){
                    currentArrays = $scope.synDeleteMsg($scope.judgeArrays,msg);
                    if(msg.is_read == 0)$scope.judgeArraysL--;
                }else{
                    currentArrays = $scope.synMarkMsg($scope.judgeArrays,id);
                    if($scope.judgeArraysL > 0)$scope.judgeArraysL--;
                }
                localStorage.judgeArrays = $scope.changeJson(currentArrays);
                localStorage.judgeArraysL = $scope.judgeArraysL;
                break;
            //2发布问题消息，同步当前已读消息
            case $scope.listType[2].value:
                if(msg){
                    currentArrays = $scope.synDeleteMsg($scope.questionBeAnswered, msg);
                    if(msg.is_read == 0)$scope.questionBeAnsweredL--;
                }else{
                    currentArrays = $scope.synMarkMsg($scope.questionBeAnswered, id);
                    if($scope.questionBeAnsweredL > 0)$scope.questionBeAnsweredL--;
                }
                localStorage.questionBeAnswered = $scope.changeJson(currentArrays);
                localStorage.questionBeAnsweredL = $scope.questionBeAnsweredL;
                break;
            //3关注问题消息，同步当前已读消息
            case $scope.listType[3].value:
                //TDD
                break;
            //4奖励消息，同步当前已读消息
            case $scope.listType[4].value:
                if(msg){
                    currentArrays = $scope.synDeleteMsg($scope.rewardArrays,msg);
                    if(msg.is_read == 0)$scope.rewardArraysL--;
                }else{
                    currentArrays = $scope.synMarkMsg($scope.rewardArrays, id);
                    if($scope.rewardArraysL > 0)$scope.rewardArraysL--;
                }
                localStorage.rewardArrays = $scope.changeJson(currentArrays);
                localStorage.rewardArraysL = $scope.rewardArraysL;
                break;
        }
    };

    //同步删除用户消息
    $scope.synDeleteMsg = function(currentArrays,msg,userId){
        if(userId){
            for(var i = 0; i< currentArrays.length; i++){
                /*
                 首先根据用户uid找到关注用户中uid，然后将该用户中的
                 chatMessage对应的msg删除；
                 */
                if(currentArrays[i].uid == userId){
                    var index = currentArrays[i].chatMessage.indexOf(msg);
                    if(index > -1)currentArrays[i].chatMessage.splice(index,1);
                }
            }
        }else{
            var index = currentArrays.indexOf(msg);
            if(index > -1)currentArrays.splice(index,1);
        }
        //判断msg是否是已标度消息，不是的话还得需要消息数减一；
        if(msg.is_read == 0 && $rootScope.message_count > 0)$rootScope.message_count--;
        return currentArrays;
    };

    //标为已读消息的同步处理,这里传入的参数是消息的id号
    //currentArrays代表是当前问答消息显示数组，arrays是代表原来类别,msg_id代表已读消息号
    $scope.synMarkMsg = function(currentArrays,msg_id){
        for(var i = 0; i<currentArrays.length; i ++){
            if(currentArrays[i].id == msg_id)currentArrays[i].is_read = 1;
        }
        //最后已读消息在总数rootScope中也减一
        if($rootScope.message_count > 0)$rootScope.message_count--;
        return currentArrays;
    };

    //发送消息函数
    $scope.sendMessage = function(userInfo){
        if(!$scope.userMessage || $scope.userMessage.length == 0)return;
        else if($scope.userMessage.length > 1000)toaster.pop('warning','系统提示：','对不起，你发送的消息过长',true);
        else{
            //发送消息给用户
            var userData = {
                "user_to":$scope.sendMsgTo.user,  //发送给user_from，谁发给我，我就发给谁
                "user_from":$scope.username,   //我自己的邮箱
                "content":$scope.userMessage,
                "user_token":chatService.cookie
            };
            chatService.sendMessage(userData,function(response){
                //TDD消息发送成功后，更新聊天记录；
                if(response.err==0  || response.err ==3 ){
                    $scope.synChatMessage(userInfo);
                    $scope.userMessage = '';
                }
            });
        }
    };

    $scope.synChatMessage = function(userInfo){
        //构造自己发送消息的格式，然后在消息发送的同时更新本地的localStorage
        //使用flag判断发送消息的人是否在私信列表里面，如果在的话直接加到chatMessage里面，没有的话新建
        var flag = 0;
        var userData = {
            "content":$scope.userMessage,
            "user_from":$scope.username,
            "ctime":getTime()
        };
        userData.more_info_of_user_from = {
            "icon":$rootScope.userIcon
        };
        //同步更新新发的消息；$scopeChatMsg就是聊天记录
        for(var i = 0;i<$scope.privateLetterUsers.length;i++){
            if($scope.privateLetterUsers[i].uid == $scope.userId){
                $scope.privateLetterUsers[i].msgLength++;
                //加到当前聊天记录里面
                $scope.userChatMsg.unshift(userData);
                //同步聊天记录，因为加了加载更多
                $scope.privateLetterUsers[i].chatMessage = $scope.userChatMsg;
                localStorage.privateLetterUsers = JSON.stringify($scope.privateLetterUsers);
                flag = 1;
                $scope.synChat();
            }
        }
        //如果flag值还是0，则说明没有在列表中找到该人，需要新建用户信息，聊天记录
        if(flag == 0){
            //除了user_from都是对方的信息；
            var array = {
                "user_from":$scope.username,
                "uid":userInfo.uid,
                "real_name":userInfo.real_name,
                "newMsgLength":0,
                "msgLength":1,
                "user":userInfo.user
            };

            array.chatMessage = [];
            array.chatMessage.push(userData);
            $scope.userChatMsg = array.chatMessage;
            $scope.privateLetterUsers.unshift(array);
            localStorage.privateLetterUsers = JSON.stringify($scope.privateLetterUsers);
            //同步聊天记录，因为加了加载更多
            $scope.synChat();
        }
    };

    //同步聊天记录，就是更新加载更多函数
    $scope.synChat = function(){
        $scope.loadChatTime = 1;
        $scope.loadMoreMsg = false;
        $scope.moreMsg();
    };

    //获取当前时间，发送消息时间
    function getTime(){
        var data = new Date();
        var month = parseInt(data.getMonth()) + 1;
        var cTime = data.getFullYear() + '-' + month + '-' + data.getDate()+ ' ' + data.getHours() + ':' + data.getMinutes();
        return cTime;
    }

    $scope.moreMsg = function(){
        var num = 20;
        var length = $scope.userChatMsg.length;
        var end = ($scope.loadChatTime*num>length)? length:$scope.loadChatTime*num;
        if(end==length)$scope.loadMoreMsg = false;
        else $scope.loadMoreMsg = true;
        $scope.userChatMsgL = $scope.userChatMsg.slice(0,end);
        $scope.loadChatTime++;
    };

    $scope.rollTop = function(){
        $("html body").animate({scrollTop: 0}, "fast");
    };

    //组合键发送消息
    $scope.enter = function(ev){
        if(ev.ctrlKey && (ev.keyCode == 13)){
            $scope.sendMessage($scope.sendMsgTo);
        }
    }
});

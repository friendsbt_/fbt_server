app.service('resourceMoreSev',function($http, loading,downloadSrv){
  this.getMoreRes = function(page, tag, callback){
    loading.show();
    $http({
      url: '/resource/recommend/more',
      method: 'GET',
      params: {
        tag: tag,
        page: page
      }
    })
    .success(function(response) {
      loading.hide();
      callback(0, response);
    })
    .error(function(data,status,headers,config) {
      // 失败处理
      loading.hide();
      callback(1);
    });
  };

  this.download = function(file_id,url){
    downloadSrv.download(file_id,url);
  }
  this.preview = function(res){
    downloadSrv.preview(res);
  }

});
app.controller('resourceMoreCtrl', function($scope, $routeParams, resourceMoreSev, toaster, $location){
  $scope.moreRes = [];
  $scope.allRes = {};
  $scope.tag = $routeParams.tag;
  $scope.res = {total_page: 0, 
         current_page: 1,
         noResult: false,
         noPagination: false,
         max_size: 5,
         total_items: 0,
         items_per_page: 0
       };
  $scope.handle_fetch_results = function(response) {
    if (0 == response.err) {
      $scope.moreRes = [];
      angular.forEach(response.resource_list, function(v, i){
        $scope.moreRes.push(v);
      });
      $scope.res.total_page = response.total_page;
      if ($scope.res.total_page > 1) {
        $scope.res.noPagination = true;
        $scope.res.items_per_page = $scope.moreRes.length;
        $scope.res.total_items = $scope.res.total_page * $scope.res.items_per_page;
      } else {
        $scope.res.noPagination = false;
      }
      if ($scope.moreRes.length){
        $scope.allRes[$scope.res.current_page] = $scope.moreRes; 
        $scope.res.noResult = false;
      }
      else
        $scope.res.noResult = true;   
    }
  };
  $scope.getMoreRes = function(){
    if($scope.res.current_page in $scope.allRes){
      $scope.moreRes = $scope.allRes[$scope.res.current_page];
    }
    else{
      resourceMoreSev.getMoreRes($scope.res.current_page, $scope.tag, function(err, data){
        if(err){
          toaster.pop('error', '错误提示', "获取资源失败，请重试", true);
        }
        else{
          $scope.handle_fetch_results(data);
        }
      });
    }
  };

  $scope.download = function(file_id,url){
    resourceMoreSev.download(file_id,url);
  };
  $scope.preview = function(res){
    resourceMoreSev.preview(res);
  };
  $scope.viewDetailHandler = function(file_id){
    $location.path('/details').search({file_id:file_id});
  };

  $scope.getMoreRes();
});
var shareContent={
	_bdDesc:'校园星空，大学生的学习资源分享平台！高校云集，海量学习资源，免费下载，轻松学，轻松考！麻麻再也不用担心我的学习！',
	_btText:'Hi,小伙伴们，我刚刚在校园星空站下载了不错的考试资料，分享给大家，赶快去下载吧!',
	_bdUrl:"http://study.friendsbt.com" ,
	_bdPic:'http://test.friendsbt.com/static/images/share.jpg'
};

function shareInit(_btText, _bdDesc, _bdUrl, _bdPic) {
	window._bd_share_main = null;
	window._bd_share_config = {
		common: {
			bdText: _btText == undefined ? shareContent._btText : _btText,
			bdDesc: _bdDesc == undefined ? shareContent._bdDesc : _bdDesc,
			bdUrl: _bdUrl == undefined ? shareContent._bdUrl : _bdUrl,
			bdPic: _bdPic == undefined ? shareContent._bdPic : _bdPic,
		},
		share: [{
			"bdSize": 16,
			"bdMini": 2,
			"bdMiniList": ['tsina', "weixin", 'qzone', "renren", "baidu", "tqq", "sqq", "mshare", "tieba", "douban", "hi", "kaixin001", "t163", "tsohu", "fbook", "twi", "linkedin", "ibaidu", "bdhome", "xg", "ty", "meilishuo", "mogujie", "huaban", "duitang", "mail", "isohu", "bdxc", "thx"],
			"bdPopupOffsetLeft": -138
		}]
	};
	with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion='+~(-new Date()/36e5)];
}


app.service('detailsSrv', ['$rootScope', '$http', 'toaster', function($rootScope, $http, toaster) {

	var self = this;
	this.init = function() {
		this.details_info = null;
		this.comments = null;
		this.comments_total_page = null;
		this.comments_current_page = null;
		this.isCommentEmpty = true;
		this.isPaginationVisible = false;
                            this.thumb_up_num = 0;
                            this.thumb_down_num = 0;
                            this.file_id = null;
                            this.file_name = null;
		this.page = {
			// max_size:,
			total_items: 0,
			max_size: 5,
			items_per_page: 25,
			current_page: 1,
			pageChanged: null,
		}
	}

	this.setComments = function(comments) {
		if (comments.length == 0) {
			this.isCommentEmpty = true;
		} else {
			this.comments = comments;
			this.isCommentEmpty = false;
		}
	}

	this.setPagination = function() {
		if (!this.isCommentEmpty) {
			self.page.total_items = self.details_info.comment_num;
			self.page.current_page = self.comments_current_page;
			if (self.page.total_items > 25) {
				this.isPaginationVisible = true;
			}
		}
	}
	this.fetch_detail_info = function(file_id) {

		self.init(); //由于service是单例的,为了防止用户点后退之后又点了一个新的资源详情页面(数据没有丢失),所以要重新初始化一下

		var req = {
			method: 'GET',
			url: '/resource/detail',
			timeout: 10000,
			params: {
				fid: file_id,
			}
		};
		var promise = $http(req).success(function(data, status, headers, config) {
				var data = angular.fromJson(data);
				if (data.err === 0) {
					self.details_info = data.info;
					self.details_info.thumb_up_users = self.details_info.thumb_up_users || [];
					self.details_info.thumb_down_users = self.details_info.thumb_down_users || [];
					// self.comments = data.comments_list;
					self.setComments(data.comments_list);
					self.comments_total_page = data.comments_total_page;
					self.comments_current_page = data.comments_current_page;
                                                                      self.thumb_up_num = data.info.thumb_up_num || 0;
                                                                      self.thumb_down_num = data.info.thumb_down_num || 0;
                                                                      self.file_id = data.info.file_id;
                                                                      self.file_name = data.info.filename;
					self.setPagination();
				} else {

				}
			})
			.error(function(data, status, headers, config) {

			});
		return promise;
	};

	this.fetch_comments = function(file_id, page) {
		var req = {
			method: 'GET',
			url: '/resource/comment/list',
			timeout: 10000,
			params: {
				file_id: file_id,
				page: page,
			}
		};
		var promise = $http(req).success(function(data, status, headers, config) {
				var data = angular.fromJson(data);
				if (data.err == 0) {
					self.comments = data.comment_list;
					self.comments_total_page = data.total_page;
					self.comments_current_page = data.current_page;
					// self.setPagination();
				} else {

				}
			})
			.error(function(data, status, headers, config) {

			})
		return promise;
	};

	this.post_comment = function(file_id, content) {
		var req = {
			method: 'GET',
			url: '/resource/comment/post',
			timeout: 10000,
			headers: {
				"X-CSRFToken": getCookie("_xsrf"),
			},
			params: {
				file_id: file_id,
				content: content,
			}
		};
		var promise = $http(req).success(function(data, status, headers, config) {
				var data = angular.fromJson(data);
				if (data.err == 0) {
					// self.details_info = data.info;
					toaster.pop('success', '系统提示', '评论成功', true);
					comment_new = {
						user_icon: angular.element(".dropdown a img").attr("src"), //从登录处获得头像
						nick_name: '我自己',
						ctime: '刚刚',
						content: content,

					};
					if (self.isCommentEmpty) {
						self.comments = [comment_new];
					} else {
						self.comments.splice(0, 0, comment_new); //直接插入评论里边
					}
					self.isCommentEmpty = false;
					// self.fetch_comments(file_id).then(function(){}); //评论完了再获取一次
				} else if (data.err == 1) {

					toaster.pop('error', '系统提示', '请登录后再评论哟!:)', false);
				} else if (data.err == 2) {
					toaster.pop('error', '系统提示', '参数出错了哎!:(', false);
				} else {
					toaster.pop('error', '系统提示', '小兔也不知道为啥出错了...:(', false);
				}
			})
			.error(function(data, status, headers, config) {
toaster.pop('error', '系统提示', '评论失败', true);
			});
		return promise;
	};

	this.rating = function(file_id, score) {
		var req = {
			method: 'GET',
			url: '/resource/rating',
			timeout: 10000,
			headers: {
				"X-CSRFToken": getCookie("_xsrf"),
			},
			params: {
				file_id: file_id,
				score: score,
			}
		};
		$http(req).success(function(data, status, headers, config) {
				var data = angular.fromJson(data);
				if (data.err == 0) {
					// alert('评分成功');
					toaster.pop('success', '系统提示', '评分成功', true);
				} else if (data.err == 1) {
					toaster.pop('error', '系统提示', '请登录后再评分哟!:)', false);
				} else if (data.err == 2) {
					toaster.pop('error', '系统提示', '参数出错了哎!:(', false);
				} else {
					toaster.pop('error', '系统提示', '小兔也不知道为啥出错了...:(', false);
				}
			})
			.error(function(data, status, headers, config) {
				toaster.pop('error', '系统提示', '评分失败', true);
			})
	};

        this.thumbUp = function() {
            var req = {
                method: 'GET',
                url: '/resource/thumb_up',
                timeout: 10000,
                params: {
                    file_id: self.file_id,
                    file_name: self.file_name
                }
            };
            var promise = $http(req).success(function(data, status, headers, config) {
                    var data = angular.fromJson(data);
                    if (data.err === 0) {
                        self.thumb_up_num += 1;
                        self.details_info.thumb_up_users.push($rootScope.username);
                    }
                })
                .error(function(data, status, headers, config) {});
            return promise;
        };

        this.thumbDown = function() {
            var req = {
                method: 'GET',
                url: '/resource/thumb_down',
                timeout: 10000,
                params: {
                    file_id: self.file_id,
                    file_name: self.file_name
                }
            };
            var promise = $http(req).success(function(data, status, headers, config) {
                    var data = angular.fromJson(data);
                    if (data.err === 0) {
                        self.thumb_down_num += 1;
                        self.details_info.thumb_down_users.push($rootScope.username);
                    }
                })
                .error(function(data, status, headers, config) {});
            return promise;
        };

        this.thumbUped = function() {
            return $rootScope.username && self.details_info && (self.details_info.thumb_up_users.indexOf($rootScope.username) != -1);
        };

        this.thumbDowned = function () {
            return $rootScope.username && self.details_info && (self.details_info.thumb_down_users.indexOf($rootScope.username) != -1);
        };
}]);


app.controller('detailsController', ['$scope', '$routeParams', 'detailsSrv', 'downloadSrv',  'User', 'toaster', 'PreviewService',
    function($scope, $routeParams, detailsSrv, downloadSrv, User, toaster,PreviewService) {

	$scope.$on('$viewContentLoaded',function(){
		$scope.init();
	});

		$scope.showPreviewModal = PreviewService.showPreviewModal;


	$scope.init = function() {

		$scope.file_id = $routeParams.file_id;
		// $scope.comment_new = '';//新评论s
		shareInit(); //初始化分享

		detailsSrv.fetch_detail_info($scope.file_id).then(function() {
			$scope.details_info = detailsSrv.details_info;
			// $scope.page = detailsSrv.page;
			$scope.comments = detailsSrv.comments;
			$scope.page = detailsSrv.page;
			$scope.isCommentEmpty = detailsSrv.isCommentEmpty;
			$scope.isPaginationVisible = detailsSrv.isPaginationVisible;
                                          $scope.thumb_up_num = detailsSrv.thumb_up_num;
                                          $scope.thumb_down_num = detailsSrv.thumb_down_num;
			// detailsSrv.setPagination();

		});

		$("#input-rating").rating({
			"stars": "5",
			"min": "0",
			"max": "5",
			"step": 0.5,
			"size": "xs",
			"showClear": false,
			"glyphicon": false,
			"starCaptions": {
				0.5: "0.5",
				1: "1",
				1.5: "1.5",
				2: "2",
				2.5: "2.5",
				3: "3",
				3.5: "3.5",
				4: "4",
				4.5: "4.5",
				5: "5"
			}
		}).rating('update', 4.5).on('rating.change', function(event, value, caption) {
			// $scope.formdata.rate = parseFloat(value);
			$scope.rating($scope.file_id, parseFloat(value));
		}); //暂时用jquery实现评分的初始化


		//监听一下分页的变化
		$scope.$watch(function() {
			return detailsSrv.page.current_page;
		}, function(newValue, oldValue) {
			if (newValue != oldValue) {
				$scope.fetch_comments($scope.file_id, newValue);
			}
		});
	}


	$scope.rating = function(file_id, score) {
		detailsSrv.rating(file_id, score);
	}
	$scope.fetch_comments = function(file_id, page_num) {
		detailsSrv.fetch_comments(file_id, page_num).then(function() {
			$scope.comments = detailsSrv.comments;
			$scope.page = detailsSrv.page;
		});
	}
	$scope.post_comment = function(comment_new) {
		detailsSrv.post_comment($scope.file_id, comment_new).then(function() {
			$scope.comments = detailsSrv.comments;
			$scope.isCommentEmpty = detailsSrv.isCommentEmpty; //bool不是引用 ,得监视
		});
	}

	$scope.downloadHandler = function() {
		downloadSrv.download($scope.file_id, $scope.details_info.download_link);
	}

	$scope.normalizeFileSize = function(filesize) {
		return normalizeFileSize(filesize); //定义在utils.js中
	}
            $scope.previewHandler = function(){
                downloadSrv.preview($scope.details_info );
            }
            $scope.thumbUp = function() {
                User.login(function() {
                    if ($scope.thumbUped()) return;
                    detailsSrv.thumbUp().then(function(){
                        $scope.thumb_up_num = detailsSrv.thumb_up_num;
                        toaster.pop('success', '系统提示', '你赞了一次', true);
                    });
                });
            };
            $scope.thumbDown = function() {
                User.login(function() {
                    if ($scope.thumbDowned()) return;
                    detailsSrv.thumbDown().then(function(){
                        $scope.thumb_down_num = detailsSrv.thumb_down_num;
                        toaster.pop('success', '系统提示', '你踩了一次', true);
                    });
                });
            };
            $scope.thumbUped = function () {
            	return detailsSrv.thumbUped();
            };
            $scope.thumbDowned = function () {
            	return detailsSrv.thumbDowned();
            };
}]);

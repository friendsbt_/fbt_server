/*********************************************************************************
 *     File Name           :     tag-modal-controller.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-11-01 19:43]
 *     Last Modified       :     [2015-11-24 20:19]
 *     Description         :     标签modal的controller
 **********************************************************************************/

app.controller('TagController', ['$scope', 'tagSrv', 'QuestionAnswerSrv', 'AskQuestionSrv', 'toaster','$timeout',
  function($scope, tagSrv, QuestionAnswerSrv, AskQuestionSrv, toaster,$timeout) {

    $scope.init = function(mode) {
      $scope.showTagIllegal = false;
      $scope.draftTagsLengthErr = false;

      //mode代表模式 ，edit代表用户编辑的时候需要tag，
      //select代表用户需要关注标签
      $scope.classes = QuestionAnswerSrv.QuestionClassEnum; //总共分了多少类
      $scope.mode = tagSrv.mode;
      $scope.draftTags = tagSrv.draftTags;
      $scope.tags = tagSrv.tags;
      $scope.subscribedTags = tagSrv.subscribedTags;

      if (!tagSrv.alreadyInit) { //登录后没有重新获取，所以判断一下长度是不是0,是的话就再获取一次
        tagSrv.fetchAllTags().then(function() {
          $scope.tags = tagSrv.tags;
          if ($scope.tags.length) {
            tagSrv.alreadyInit = true;
          }
        });
        tagSrv.fetchMyTags().then(function() {
          $scope.subscribedTags = tagSrv.subscribedTags;
        });
      }

      $scope.tempTags2Unsubscribe = tagSrv.tempTags2Unsubscribe;
      $scope.tempTags2Subscribe = tagSrv.tempTags2Subscribe;

      //下面4个是函数
      $scope.removeDraftTag = tagSrv.removeDraftTag;




      if (tagSrv.class2 !== '') {
        $scope.findNav = tagSrv.class2;
      } else {
        $scope.findNav = '';
      }
      $scope.find2Nav($scope.findNav ? $scope.findNav : '校园');

      $scope.noNav = false; //提示暂无该标签内容，默认为false
    };

    //监听tagSrv的mode，其他地方从showTagModal(mode)中把mode参数传给tagSrv
    $scope.$watch(function() {
      return tagSrv.mode;
    }, function(newVal, oldVal) {
      $scope.mode = tagSrv.mode;
    });


    //搜索标签函数
    $scope.search = function(id) {
      if (id) {
        $scope.findNav = id;
        $scope.filterNav(id);
      }
    };
    //搜索输入框响应事件函数
    $scope.enter = function(ev) {
      if (ev.keyCode == 13) {
        $scope.search($scope.searchNav);
      }
    };

    //过滤函数，如果标签列表没有显示信息
    $scope.filterNav = function(c) {
      //判断该类别是否包含元素，没有的话显示暂无该类别
      var tag;
      for (tag in $scope.tags) {
        var temp = $scope.tags[tag].tag;
        if (temp.indexOf(c) > -1) {
          $scope.noNav = false;
          //找到一个就终止，说明列表不为空
          break;
        } else $scope.noNav = true;
      }
    };

    $scope.find2Nav = function(c) {
      AskQuestionSrv.formdata.class2 = c;
      $scope.findNav = c;
      $scope.filterNav(c);
    };

    ////用户是否可以设置标签函数
    //$scope.setNav = function() {
    //  if ($scope.is_logined) {
    //    $scope.modalName = '#
    //          findModal ';
    //  } else {
    //    User.login();
    //  }
    //};

    $scope.clickTag = function(tag) {
      if ($scope.mode === 'select') {
        //先判断数组中是否有一样的标签，如果没有再添加：
        if ($scope.subscribedTags.indexOf(tag) > -1) {
          toaster.pop('warning', '系统提示', '您已添加该标签', true);
          return;
        }
        $scope.addTag2Subsribe(tag);

      } else if ($scope.mode === 'edit') {
        AskQuestionSrv.formdata.class2 = tag.split(':')[0];
        $scope.addDraftTag(tag.split(':')[1]);
      }
    };


    //index是索引，移除标签
    $scope.unSubscribeTag = function(index) {
      $scope.addTag2Unsubscribe(index);
    };

    $scope.addTag2Subsribe = function(tag) {

      $scope.tempTags2Subscribe.push(tag);
      $scope.subscribedTags.push(tag);

    };

    //index是索引
    $scope.addTag2Unsubscribe = function(index) {
      $scope.tempTags2Unsubscribe.push($scope.subscribedTags[index]); //添加到要删除的列表中
      $scope.subscribedTags.splice(index, 1);

    };

    $scope.$watch(function() {
      return $scope.newtag;
    }, function(newVal, oldVal) {
      if (newVal === undefined) {
        $scope.showTagIllegal = false;
        return;
      }
      if (newVal.indexOf(':') >= 0) {
        $scope.showTagIllegal = true;

      } else {
        $scope.showTagIllegal = false;
      }

    });

    $scope.cancel = function() {

      tagSrv.cancel();
    };
    $scope.ok = function() {

      tagSrv.ok();
    };

    $scope.addDraftTag = function(tag) {

      if ($scope.showTagIllegal) {
        return;
      }
      if ($scope.draftTags.length === 3) {
        $scope.draftTagsLengthErr = true;
        $timeout(function() {
          $scope.draftTagsLengthErr = false;
        }, 300);
        return;
      } else {
        tagSrv.addDraftTag(tag);
      }
    };

    $scope.tagClassFilter = function(val,index,array){
        if(val['tag'].indexOf($scope.findNav) === 0){
            return true;
        }
        else{ return false;}

    }

  }
]);

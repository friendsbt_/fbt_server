var resourceType = {
  audited: "audited",
  auditing: "auditing",
  unaudited: "unaudited",
  downloaded: "downloaded",
};


app.service("MyResService", ['$rootScope', '$http', '$location', 'toaster','downloadSrv', function($rootScope, $http, $location, toaster,downloadSrv) {

  var self = this;
  this.resource_list = new Array();
  this.current_page;
  this.total_page;
  this.state = resourceType.audited; //audited auditing uploaded unaudited


  this.callback = function(data, status) {
    data = angular.fromJson(data);
    if (data.err == 0) {
      self.resource_list = data.resource_list;
      self.current_page = data.current_page;
      self.total_page = data.total_page;
    } else {
      toaster.pop('error', "错误提示", "资源请求失败，请重试", true);
    }
  }

  this.getMyRes = function(type, page) {
    var kwargs = {
      type: type,
      page: page
    };
    var req = {
      url: '/resource/user',
      method: 'GET',
      params: kwargs
    };
    return $http(req).success(self.callback).error(function(data,status){
      toaster.pop('error', "错误提示", "资源请求失败，请重试", true);
    });
  }

  this.preview = function(res){
    downloadSrv.preview(res);
  }

}]);

app.controller("MyResController", ['$scope', '$routeParams', '$location', 'MyResService', function($scope, $routeParams, $location, MyResService) {

  $scope.isAuditedEmpty = false;
  $scope.isAuditingEmpty = false;
  $scope.isDownloadedEmpty = false;
  $scope.isUnAuditedEmpty = false;


  $scope.pageChange = function() {
    // $scope.getMyRes();
    $location.path('/myresource/' + $scope.state).search({
      'r_type': $scope.state,
      'page_num': $scope.page.current_page
    })
  };
  $scope.page = {
    // max_size:,
    total_items: 0,
    max_size: 5,
    items_per_page: 16,
    //    current_page: 1,
    pageChanged: $scope.pageChange,
  }

  $scope.btnClick = function(state) {
    $scope.state = state;
    $location.path('/myresource/' + state).search('r_type', state);

  }


  $scope.init = function() {
    $scope.state = $routeParams.r_type;
    $scope.current_page = $routeParams.page_num == undefined ? 1 : $routeParams.page_num;
    if ($scope.state == undefined || !($scope.state in resourceType)) {
      $scope.state = resourceType.audited;
    }
    $scope.getMyRes();

  }

  $scope.getJsonParam = function(object) {
    return angular.toJson(object);
  }

  $scope.getMyRes = function() {

    MyResService.getMyRes($scope.state, $scope.current_page).then(function() {
      $scope.resource_list = MyResService.resource_list;
      $scope.page.current_page = MyResService.current_page;
      $scope.page.total_page = MyResService.total_page;

      if ($scope.page.total_page > 1) {
        $scope.page.total_items = MyResService.total_page * 16;
        $scope.isPaginationVisible = true;
      } else {
        $scope.isPaginationVisible = false;
      }
      $scope.showEmptyTip();
    })
  }

  $scope.viewAuditedDetail = function(res) {
    var url = $location.path();
    $location.path('/myresource/unaudited/unpassdetail').search({
      str: angular.toJson(res),
      url: url,
      r_type: $scope.state,
      page_num: $scope.page.current_page,
    });
  }


  $scope.showEmptyTip = function() {
    $scope.isAuditedEmpty = ($scope.state == 'audited') && ($scope.resource_list.length == 0);
    $scope.isAuditingEmpty = ($scope.state == 'auditing') && ($scope.resource_list.length == 0);
    $scope.isDownloadedEmpty = ($scope.state == 'downloaded') && ($scope.resource_list.length == 0);
    $scope.isUnAuditedEmpty = ($scope.state == 'unaudited') && ($scope.resource_list.length == 0);
  }

   $scope.preview = function(res) {
    MyResService.preview(res);
  };

}]);

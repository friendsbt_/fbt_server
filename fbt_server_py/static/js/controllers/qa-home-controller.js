/*********************************************************************************
 *     File Name           :     qa-home-controller.js
 *     Created By          :     DQ - D.Q.Zhang.chn@gmail.com
 *     Creation Date       :     [2015-10-27 17:34]
 *     Last Modified       :     [2015-12-05 17:48]
 *     Description         :      问题主页控制器
 **********************************************************************************/
//QuestionAnswerSrv is in file ../service/queston-answer-service.js

app.controller('QuestionController', ['$scope', '$http', 'QuestionAnswerSrv', '$rootScope', 'tagSrv', 'share', 'User', 'ThankSrv', 'bottomLoading', '$location', 'toaster', '$window', 'HotLatestQuestionSrv','$routeParams','$route',
  function($scope, $http, QuestionAnswerSrv, $rootScope, tagSrv, share, User, ThankSrv, bottomLoading, $location, toaster, $window, HotLatestQuestionSrv,$routeParams,$route) {

    $scope.QuestionScopeEnum = {
      ALL: 1,
      MY_SCHOOL_ONLY: 0
    };
    $scope.QuestionClassEnum = QuestionAnswerSrv.QuestionClassEnum;


    $scope.setQuestionScope = function(scope) { //设置是查看全部高校还是只看自己的高校ndow
      $scope.questionScope = scope;
      //$scope.caredQuestions.length = 0; //清空后自动会触底加载的
      //$scope.currentPage = 0;
      //$scope.totalPage = -1;
      //$scope.fetchQuestions();
      $route.updateParams({'questionScope':scope});

    };

    $scope.$watch(function() {
      return tagSrv.subscribedTags.length;
    }, function(newVal, oldVal) {
      $scope.noSubscribedTags = !newVal;

    });

    $scope.$watch(function() {
      return QuestionAnswerSrv.noRecommended;
    }, function(newVal, oldVal) {
      $scope.noRecommended = newVal;
    });
    $scope.init = function() {
      //加上轮播；
      $('#ad').carousel({
        interval: 5000
      });

      QuestionAnswerSrv.init();
      $scope.isLogined = User.logined();

      $scope.currentQuestionClass = $routeParams.questionClass?$routeParams.questionClass:'校园';
      $scope.questionScope =  ($routeParams.questionScope)?parseInt($routeParams.questionScope):$scope.QuestionScopeEnum.ALL; //

      $scope.caredQuestions = QuestionAnswerSrv.caredQuestions;
      //$scope.hotQuestions = HotLatestQuestionSrv.hotQuestions;
      //$scope.latestQuestions = HotLatestQuestionSrv.latestQuestions;
      $scope.noRecommended = false;
      //如果用户登录加载用户信息
      if (User.logined()) {
        $http.get('/user/updated_info').success(function (data) {
          if ('err' in data && data['err'] === 0) {
            $scope.user = data;
          }
        });
      }else{
        $scope.user = User.getUserInfo();
      }
      //从headontroller那更新那获取用户的简介信息
      $scope.cardUser = {}; //名片上的用户
      $scope.currentPage = 0;
      $scope.totalPage = -1;

      $scope.showedLatestQuestions = HotLatestQuestionSrv.showedLatestQuestions; //显示的最新问题
      $scope.showedHotQuestions = HotLatestQuestionSrv.showedHotQuestions; //显示的最热问题

      $scope.changeHotClickNum = 0; //用来记录点了几次换一换
      $scope.changeLatestClickNum = 0;

      if (!tagSrv.alreadyInit) {
        tagSrv.fetchAllTags();
        tagSrv.fetchMyTags();
      }
      $scope.fetchQuestions();
    };


    $scope.fetchQuestions = function() {
      if ($scope.currentPage === $scope.totalPage || $scope.currentPage > 20)
        return;
      bottomLoading.show();
      var val = $scope.currentPage + 1;
      var GotHotLatestQuestions = HotLatestQuestionSrv.GotHotLatestQuestions;

      QuestionAnswerSrv.fetchQuestions($scope.questionScope, $scope.currentQuestionClass, val)
        .then(function() {
          $scope.caredQuestions = QuestionAnswerSrv.caredQuestions;
          $scope.hotQuestions = HotLatestQuestionSrv.hotQuestions;
          $scope.latestQuestions = HotLatestQuestionSrv.latestQuestions;
          $scope.currentPage += 1;
          $scope.totalPage = QuestionAnswerSrv.totalPage;
          $scope.noRecommended = QuestionAnswerSrv.noRecommended;

          if (GotHotLatestQuestions) {
            $scope.changeHot();
            $scope.changeLatest();

          }
          bottomLoading.hide();
        });
      //console.log('request end');
    };


    $scope.changeHot = function() {

      HotLatestQuestionSrv.changeHot($scope.changeHotClickNum);
      $scope.changeHotClickNum += 1;
      $scope.showedHotQuestions = HotLatestQuestionSrv.showedHotQuestions; //显示的最热问题

    };

    $scope.changeLatest = function() {


      HotLatestQuestionSrv.changeLatest($scope.changeLatestClickNum);
      $scope.changeLatestClickNum += 1;
      $scope.showedLatestQuestions = HotLatestQuestionSrv.showedLatestQuestions; //显示的最新问题
    };




    //用户点击了分类后的操作，对应校园、考证、就业
    $scope.selectClass = function(questionClass) {
      //$scope.currentPage = 0;
      //$scope.totalPage = -1;
      //$scope.currentQuestionClass = questionClass;
      //$scope.caredQuestions.length = 0;
      //$scope.fetchQuestions();
        $route.updateParams({'questionClass':questionClass});
    };

    $scope.fetchUserCard = function(userEmail) {
      QuestionAnswerSrv.fetchUserCard(userEmail).then(function() {
        $scope.cardUser = QuestionAnswerSrv.cardUser;
      });
    };

    //添加关注（人）TODO:delete
    $scope.follow = function(id) {
      QuestionAnswerSrv.follow(id);
    };


    $scope.showTagModal = function(mode) {
      if (!User.logined()) {
        $location.path('home').search({
          return_url: 'qahome'
        });
      } else {
        tagSrv.showTagModal(mode);
      }
    };


    //单击箭头回到顶部函数TODO:DELTET
    $scope.toTop = function() {
      $('html, body').animate({
        scrollTop: 0
      }, "fast");
    };
  }
]);

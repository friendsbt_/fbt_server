app.service('courseDetailsSrv', ['$rootScope', '$http', 'toaster',
    function($rootScope, $http, toaster) {

        var self = this;
        this.init = function() {
            this.details_info = null;
            this.comments = null;
            this.comments_total_page = null;
            this.comments_current_page = null;
            this.isCommentEmpty = true;
            this.isPaginationVisible = false;

            this.page = {
                // max_size:,
                total_items: 0,
                max_size: 5,
                items_per_page: 25,
                current_page: 1,
                pageChanged: null,
            };
        };

        this.setComments = function(comments) {
            if (comments.length === 0) {
                this.isCommentEmpty = true;
            } else {
                this.comments = comments;
                this.isCommentEmpty = false;
            }
        };

        this.setPagination = function() {
            if (!this.isCommentEmpty) {
                self.page.total_items = self.details_info.comment_num;
                self.page.current_page = self.comments_current_page;
                if (self.page.total_items > 25) {
                    this.isPaginationVisible = true;
                }
            }
        };



        this.fetch_detail_info = function(course_id) {

            self.init(); //由于service是单例的,为了防止用户点后退之后又点了一个新的资源详情页面(数据没有丢失),所以要重新初始化一下

            var req = {
                method: 'GET',
                url: '/course/detail',
                timeout: 10000,
                params: {
                    // course_name_id: course_name_id,
                    course_id:course_id
                }
            };
            var promise = $http(req).success(function(data, status, headers, config) {
                    var data = angular.fromJson(data);
                    if (data.err === 0) {
                        self.details_info = data.info;
                        // self.comments = data.comments_list;
                        self.setComments(data.comments_list);
                        self.comments_total_page = data.comments_total_page;
                        self.comments_current_page = data.comments_current_page;
                        self.setPagination();
                    } else {

                    }
                })
                .error(function(data, status, headers, config) {

                });
            return promise;
        };
        //this.fetch_comments = function(university, college, course_name, page) {
        this.fetch_comments = function(course_id,page) {
            var req = {
                method: 'GET',
                url: '/course/comment/list',
                timeout: 10000,
                params: {
                    //university: university,
                    //college: college,
                    //course_name: course_name,
                    course_id:course_id,
                    page: page
                }
            };
            var promise = $http(req).success(function(data, status, headers, config) {
                    var data = angular.fromJson(data);
                    if (data.err === 0) {
                        self.comments = data.comment_list;
                        self.comments_total_page = data.total_page;
                        self.comments_current_page = data.current_page;
                        // self.setPagination();
                    } else {

                    }
                })
                .error(function(data, status, headers, config) {

                });
            return promise;
        };

        //this.post_comment = function(university, college, course_name, content) {
        this.post_comment = function(course_id, content) {
            var req = {
                method: 'GET',
                url: '/course/comment/post',
                timeout: 10000,
                headers: {
                    "X-CSRFToken": getCookie("_xsrf"),
                },
                params: {
                    //course_name: course_name,
                    //university: university,
                    //college: college,
                    content: content,
                    course_id:course_id
                }
            };
            var promise = $http(req).success(function(data, status, headers, config) {
                    var data = angular.fromJson(data);
                    if (data.err === 0) {
                        // self.details_info = data.info;
                        toaster.pop('success', '系统提示', '评论成功', true);
                        comment_new = {
                            user_icon: angular.element(".dropdown a img").attr("src"), //从登录处获得头像
                            nick_name: '我自己',
                            ctime: '刚刚',
                            content: content,

                        };
                        if (self.isCommentEmpty) {
                            self.comments = [comment_new];
                        } else {
                            self.comments.splice(0, 0, comment_new); //直接插入评论里边
                        }
                        self.isCommentEmpty = false;
                        // self.fetch_comments(course_name_id).then(function(){}); //评论完了再获取一次
                    } else if (data.err == 1) {

                        toaster.pop('error', '系统提示', '请登录后再评论哟!:)', false);
                    } else if (data.err == 2) {
                        toaster.pop('error', '系统提示', '参数出错了哎!:(', false);
                    } else {
                        toaster.pop('error', '系统提示', '小兔也不知道为啥出错了...:(', false);
                    }
                })
                .error(function(data, status, headers, config) {
                    toaster.pop('error', '系统提示', '评论失败', true);
                });
            return promise;
        };



    }
]);


app.controller('courseDetailsController', ['$scope', '$routeParams', 'courseDetailsSrv', 'downloadSrv','PreviewService',
    function($scope, $routeParams, courseDetailsSrv, downloadSrv,PreviewService) {

        $scope.$on('$viewContentLoaded', function() {
            $scope.init();
        });

        $scope.showPreviewModal = PreviewService.showPreviewModal;

        $scope.init = function() {

            $scope.university = $routeParams.university;
            $scope.college = $routeParams.college;
            $scope.course_name = $routeParams.course_name;
            $scope.course_id = $routeParams.course_id;
            //courseDetailsSrv.fetch_detail_info($scope.university,$scope.college,$scope.course_name).then(function() {
            courseDetailsSrv.fetch_detail_info($scope.course_id).then(function() {
                $scope.details_info = courseDetailsSrv.details_info;
                $scope.comments = courseDetailsSrv.comments;
                $scope.page = courseDetailsSrv.page;
                $scope.isCommentEmpty = courseDetailsSrv.isCommentEmpty;
                $scope.isPaginationVisible = courseDetailsSrv.isPaginationVisible;
            });



            //监听一下分页的变化
            $scope.$watch(function() {
                return courseDetailsSrv.page.current_page;
            }, function(newValue, oldValue) {
                if (newValue != oldValue) {

                    $scope.fetch_comments(newValue);
                }
            });
        };


        $scope.fetch_comments = function(page_num) {
            //courseDetailsSrv.fetch_comments($scope.university, $scope.college, $scope.course_name, page_num).then(function() {
            courseDetailsSrv.fetch_comments($scope.course_id, page_num).then(function() {
                $scope.comments = courseDetailsSrv.comments;
                $scope.page = courseDetailsSrv.page;
            });
        };
        $scope.post_comment = function(comment_new) {
            //courseDetailsSrv.post_comment($scope.university, $scope.college, $scope.course_name, comment_new).then(function() {
            courseDetailsSrv.post_comment($scope.course_id, comment_new).then(function() {
                $scope.comments = courseDetailsSrv.comments;
                $scope.isCommentEmpty = courseDetailsSrv.isCommentEmpty; //bool不是引用 ,得监视
            });
        };
    }
]);

/**
 * Created by yulei on 2015/7/13.
 */

app.controller('rankController',  ['$scope', '$http', 'loading','$location','$window', function ($scope, $http, loading, $location,$window) {
        //scrollTo(0, 0);
        $scope.fetchFList = function() {
            $scope.showUpdate = false;
            if ($scope.FListDate) return;
            loading.show();
            $http.get("/fbrank?type=6").success(function (FListDate) {
                if (FListDate.data.length > 1) {
                    $scope.FListDate = FListDate.data;
                } else {
                    $scope.showUpdate = true;
                }
                loading.hide();
            });
        };

        $scope.showTab = function (tab) {
            return tab === $scope._showTab;
        };

        $scope.inActive = function(act) {
            return act === $scope._inActive;
        };

        init();
        function init() {
            $scope._showTab = 2;
            $scope._inActive = 2;
            $scope.weekListDate = null;
            $scope.monthListDate = null;
            $scope.FListDate = null;
            $scope.showUpdate = false;
            $scope.fetchFList();
        }
        $scope.goToUser = function(username){
            $window.open("/user_page?user=" + encode_username(username), "_blank");
        };

    }]);

app.filter('rankFilter', function () {
    var money;
    return function (input) {
        if (input > 2000) {
            money = (input - 2000) / 200;
            money = money.toFixed(2);
        }
        else money = 0;
        return money
    };
});

app.directive('scrollOnClick', function() {
  return {
    restrict: 'A',
    link: function(scope, $elm, attrs) {
      var idToScroll = attrs.href;
      $elm.on('click', function() {
        var $target;
        if (idToScroll) {
          $target = $(idToScroll).offset().top;
        } else {
          $target = 0;
        }
        $("html, body").animate({scrollTop: $target}, "fast");
      });
      scope.$on('scrollTop', function () {
         $("html, body").animate({scrollTop: 0}, "fast");
      });
    }
  }
});

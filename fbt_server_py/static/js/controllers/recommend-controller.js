//modified by zhangdequn

//推荐页面控制器
app.controller('recommendController', ['$scope','$location',  '$http', 'recommendSrv','PreviewService', function($scope,$location, $http,recommendSrv,PreviewService) {
  init();
  function init() {
    recommendSrv.fetch_recommend_resource();
    $scope.recommend_resource = recommendSrv.get_recommend_resource();
  };

  $scope.download = function(file_id,url) {
    recommendSrv.download(file_id,url);
  };


  $scope.showPreviewModal = PreviewService.showPreviewModal;

  $scope.viewDetailHandler = function(resource_info) {
   
    $location.path('/details').search({file_id:resource_info.file_id});
    
  };

}]);

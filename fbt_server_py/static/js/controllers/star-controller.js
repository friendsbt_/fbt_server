/**
 * Created by yulei on 2015/10/13.
 */
//星空页面关注用户service
app.service('StarService',function($http,toaster){
   this.followUser = function(user,callback){
       var req = {
           url:'/user/follow',
           method:'POST',
           data:{
               who:user.user
           },
           xsrfCookieName: '_xsrf',
           xsrfHeaderName: 'X-CSRFToken'
       };
       $http(req).success(function(data){
           var response = angular.fromJson(data);
           if (response.err === 0) {
               toaster.pop('success', "关注成功！", '', true);
               callback(response,user);
           } else if (response.err === 1) {
               toaster.pop('error', "关注失败！", '对不起，您还没有登录，请先登录!', true);
           } else if (response.err === 2) {
               toaster.pop('error', "关注失败！", '已经关注过了呢！', true);
           } else if (response.err === 3) {
               toaster.pop('error', "关注失败！", '不能关注自己哟！', true);
           }

       }).error(function(err){
           toaster.pop('success', "系统提示", "关注请求出错！", true);
       });
   };
    /*
    判断是否是已关注用户，返回flag标志位
     */
    this.isCareUser = function(user){
        var careUsers = [];
        var flag = false;
        careUsers = this.careUsers;
        if(careUsers){
            var length = careUsers.length;
            for(var i = 0;i<length;i++){
                if(user.user == careUsers[i].user)flag = true;
            };
        }
        return flag;
    };
});

app.controller('StarController', function($scope, $http, toaster,QuestionAnswerSrv,$window,loading,$rootScope,StarService,User){
    $scope.init = function(){
        $scope.allStars = [
            {star:'is_postgraduate_star',value:'研'},
            {star:'is_certificate_star',value:'证'},
            {star:'is_campus_star',value:'校'},
            {star:'is_work_star',value:'业'},
            {star:'is_USA_star',value:'留'},
            {star:'is_internship_star',value:'习'}
        ];

        $scope.is_logined = $rootScope.is_logined;
        $scope.schoolList = [
            '全国高校',
            '只看自己的学校'
        ];
        $scope.schoolShow = '全国高校';
        $scope.careShow = '意气相投';
        $scope.page_id = 1; // 1表示第一页
        $scope.total_page = -1;//表示总页数
        $scope.by_all_id = 1; // 1表示全部学校，0表示自己关心学校
        $scope.intersted_id = 1; // 1表示意气相投，0表示美不胜收
        $scope.need_star = 1; //表示是否需要之星，用户请求下一页时为0
        $scope.changeSchoolStarNum = 0; //更换校园之星
        $scope.changeResearchStarNum = 0; //更换考证之星
        $scope.changeWorkerStarNum = 0; //更换就业之星
        $scope.changeStudyStarNum = 0; //更换考研之星
        $scope.changePracticeStarNum = 0;//更换实习之星
        $scope.changeStudyBStarNum = 0;//更换留学之星
        $scope.recommendUsers = [];//用户信息数组
        $scope.getRecommend();
        $scope.getStarRecommend();
        $scope.noMore = false;
    };

    //监测关注用户变化，如果列表为空，就不执行是否已关注了
    $scope.$watch(function(){
        var length;
        try {
            $scope.careUsers = JSON.parse(localStorage.careUsers);
            length = $scope.careUsers.length;
        }catch(e){
            length = 0;
        }
        return length;
    },function(newVal, oldVal){
        if(newVal != 0){
            $scope.careUsers = JSON.parse(localStorage.careUsers);
            StarService.careUsers = $scope.careUsers;
        }
    },true);

    $scope.pageInit = function(){
        //$scope.loadMoreNum = 1; //点击加载更多的次数；
        $scope.noMore = false;
        $scope.recommendUsers = [];
        $scope.page_id = 1;
        $scope.getRecommend();
    };

    $scope.goUserPage = function(username) {
        $window.open("/user_page?user=" + encode_username(username), "_blank");
    };

    $scope.changeSchool = function(_class){
        $scope.schoolShow = _class;
        if(_class == '全国高校'){
            $scope.by_all_id = 1;
        }else $scope.by_all_id = 0;
        $scope.pageInit();
    };

    $scope.changeCare = function(_class){
        $scope.careShow = _class;
        if(_class == '意气相投'){
            $scope.intersted_id = 1;
        }else{
            $scope.intersted_id = 0;
        }
        $scope.pageInit();
    };

    //读取服务器推荐的用户信息
    $scope.getRecommend = function(){
        loading.show();
        var req = {
            url: '/user/recommend',
            method: 'GET',
            params: {
                page: $scope.page_id,
                by_all: $scope.by_all_id,
                interested: $scope.intersted_id
            }
        };
        $http(req).success(function(response){
            if(response.err == 0) {
                loading.hide();
                var i = 0,
                    length = response.users.length;
                for(;i<length;i++){
                    $scope.recommendUsers.push(response.users[i]);
                }
                //$scope.recommendUsers = response.users;
                $scope.page_id = response.current_page;
                $scope.total_page = response.total_page;
                if($scope.total_page==-1||$scope.page_id==$scope.total_page) $scope.noMore = true;
            }
        }).error(function(err){
            loading.hide();
            console.log(err);
        });
    };

    //加载更多推荐用户函数
    $scope.loadMore = function(){
        if($scope.page_id<$scope.total_page){
            $scope.page_id++;
            $scope.getRecommend();
        }
    };

    //读取之星服务器推荐的用户信息
    $scope.getStarRecommend = function(){
        loading.show();
        var req = {
            url: '/user/recommend',
            method: 'GET',
            params: {
                page: $scope.page_id,
                by_all: $scope.by_all_id,
                interested: $scope.intersted_id,
                need_star: $scope.need_star
            }
        };
        $http(req).success(function(response){
            if(response.err == 0){
                loading.hide();
                $scope.stars = response.stars;
                $scope.changeSchoolStarFun();
                $scope.changeResearchStarFun();
                $scope.changeWorkerStarFun();
                $scope.changeStudyStarFun();
                $scope.changePracticeStarFun();
                $scope.changeStudyBStarFun();
            }
        }).error(function(err){
            loading.hide();
            console.log(err);
        });
    };

    //更换校园之星函数
    $scope.changeSchoolStarFun = function(){
        var length = $scope.stars['校园'].length;
        var start = ($scope.changeSchoolStarNum*2) % length;
        var end = (start + 2) > length ? ((start + 2) % length) : (start + 2);

        if(start < end){
            $scope.showSchoolStars = $scope.stars['校园'].slice(start,end + 1);
        }else{
            $scope.showSchoolStars = $scope.stars['校园'].slice(start,length);
            $scope.showSchoolStars.concat($scope.stars['校园'].slice(0,end + 1));
        }
        $scope.changeSchoolStarNum += 1;

    };

    //更换考证之星
    $scope.changeResearchStarFun = function(){
        var length = $scope.stars['考证'].length;
        var start = ($scope.changeResearchStarNum*2) % length;
        var end = (start + 2) > length ? ((start + 2) % length) : (start + 2);

        if(start < end){
            $scope.showResearchStars = $scope.stars['考证'].slice(start,end + 1);
        }else{
            $scope.showResearchStars  = $scope.stars['考证'].slice(start,length);
            $scope.showResearchStars.concat($scope.stars['考证'].slice(0,end + 1));
        }
        $scope.changeResearchStarNum += 1;
    };

    //更换就业之星
    $scope.changeWorkerStarFun = function(){

        var length = $scope.stars['就业'].length;
        var start = ($scope.changeWorkerStarNum*2) % length;
        var end = (start + 2) > length ? ((start + 2) % length) : (start + 2);

        if(start < end){
            $scope.showWorkerStars = $scope.stars['就业'].slice(start,end + 1);
        }else{
            $scope.showWorkerStars  = $scope.stars['就业'].slice(start,length);
            $scope.showWorkerStars.concat($scope.stars['就业'].slice(0,end + 1));
        }
        $scope.changeWorkerStarNum += 1;
    };
    //更换考研之星
    $scope.changeStudyStarFun = function(){
        var length = $scope.stars['考研'].length;
        var start = ($scope.changeStudyStarNum*2) % length;
        var end = (start + 2) > length ? ((start + 2) % length) : (start + 2);

        if(start < end){
            $scope.showStudyStars = $scope.stars['考研'].slice(start,end + 1);
        }else{
            $scope.showStudyStars  = $scope.stars['考研'].slice(start,length);
            $scope.showStudyStars.concat($scope.stars['考研'].slice(0,end + 1));
        }
        $scope.changeStudyStarNum += 1;
    };

    //实习之星
    $scope.changePracticeStarFun = function(){
        var length = $scope.stars['实习'].length;
        var start = ($scope.changePracticeStarNum*2) % length;
        var end = (start + 2) > length ? ((start + 2) % length) : (start + 2);

        if(start < end){
            $scope.showPracticeStars = $scope.stars['实习'].slice(start,end + 1);
        }else{
            $scope.showPracticeStars  = $scope.stars['实习'].slice(start,length);
            $scope.showPracticeStars.concat($scope.stars['实习'].slice(0,end + 1));
        }
        $scope.changePracticeStarNum += 1;
    };

    //留学之星
    $scope.changeStudyBStarFun = function(){
        var length = $scope.stars['留学'].length;
        var start = ($scope.changeStudyBStarNum*2) % length;
        var end = (start + 2) > length ? ((start + 2) % length) : (start + 2);

        if(start < end){
            $scope.showStudyBStars = $scope.stars['留学'].slice(start,end + 1);
        }else{
            $scope.showStudyBStars  = $scope.stars['留学'].slice(start,length);
            $scope.showStudyBStars.concat($scope.stars['留学'].slice(0,end + 1));
        }
        $scope.changeStudyBStarNum += 1;
    };


    $scope.changeHonor = function(honor){
        var userHonor = [];
        if(honor){
            var length = honor.length;
            for(j=0,i=length-1;i>-1;i--){
              userHonor[j++] = honor[i];
            };
            return userHonor;
        };
        return false;
    };

    //关注某一个用户
    $scope.follow = function(id){
        StarService.followUser(id,function(response,user){
            //说明关注成功，更新localStorage中的关注用户
            if(response.err == 0){
                var array = {
                    "user":user.user,
                    "college":user.college,
                    "real_name":user.real_name,
                    "icon":user.icon,
                    "university":user.university,
                    "star_info":user.star_info,
                    "uid":user.uid
                };
                $scope.careUsers.unshift(array);
                localStorage.careUsers = JSON.stringify($scope.careUsers);
            }
        });
    };
    $scope.isCareUser = function(user){
        var flag;
        //用户登录后才开始判断是否已关注
        if(User.logined()){
            flag = StarService.isCareUser(user);
        }else{
            flag = false;
        }
        return flag;
    }
});

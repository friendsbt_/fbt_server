#!/usr/bin/env python
# coding=utf-8

from constant import TOKEN_SECRET
from token_handler import TokenHandler
from message_manager import MessageManager
from users_manager import UserManager
from util import lower_bound
from coin_manager import CoinManager, CoinStatisticCollector
from datetime import datetime

import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.options
import json
import logging
import qrcode
import qrcode.image.svg
from tornado import gen
from tornado import autoreload
from StringIO import StringIO
from urllib import unquote, quote
import os


RUN_PORT = 8787


def get_qrcode(size, content, error_degree):
    square_size = size[0] if size[0] <= size[1] else size[1]
    # L,M,Q,H 纠错级别下 1~40 版本的最大容量(Binary)
    l_max = [17, 32, 53, 78, 106, 134, 154, 192, 230, 271, 321,
                367, 425, 458, 520, 586, 644, 718, 792, 858, 929,
                1003, 1091, 1171, 1273, 1367, 1465, 1528, 1628,
                1732, 1840, 1952, 2068, 2188, 2303, 2431, 2563,
                2699, 2809, 2953]
    m_max = [14, 26, 42, 62, 84, 106, 122, 152, 180, 213, 251,
                287, 331, 362, 412, 450, 504, 560, 624, 666, 711,
                779, 857, 911, 997, 1059, 1125, 1190, 1264, 1370,
                1452, 1538, 1628, 1722, 1809, 1911, 1989, 2099,
                2213, 2331]
    q_max = [11, 20, 32, 46, 60, 74, 86, 108, 130, 151, 177,
                203, 241, 258, 292, 322, 364, 394, 442, 482, 509,
                565, 611, 661, 715, 751, 805, 868, 908, 982, 1030,
                1112, 1168, 1228, 1283, 1351, 1423, 1499, 1579, 1663]
    h_max = [7, 14, 24, 34, 44, 58, 64, 84, 98, 119, 137, 155,
                177, 194, 220, 250, 280, 310, 338, 382, 403, 439,
                461, 511, 535, 593, 625, 658, 698, 742, 790, 842,
                898, 958, 983, 1051, 1093, 1139, 1219, 1273]
    versions = {"L": l_max, "M": m_max, "Q": q_max, "H": h_max}
    version = lower_bound(versions[error_degree], len(content)) + 1
    assert version >= 1
    error_correction = {"L": qrcode.constants.ERROR_CORRECT_L,
                        "M": qrcode.constants.ERROR_CORRECT_M,
                        "Q": qrcode.constants.ERROR_CORRECT_Q,
                        "H": qrcode.constants.ERROR_CORRECT_H
                        }[error_degree]
    # 根据 python-qrcode 源码、square_size 及 version 参数求 box_size
    border = 4
    box_size = square_size / ((version * 4 + 17) + border * 2)
    qr = qrcode.QRCode(version=version, error_correction=error_correction,
                       box_size=box_size, border=border)
    qr.add_data(content)
    qr.make(fit=True)
    img = qr.make_image()
    output = StringIO()
    img.save(output)
    qr_data = output.getvalue()
    output.close()
    return qr_data


class BaseHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS")
        self.set_header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')

    def is_from_mobile(self):
        return self.request.headers['User-Agent'].lower().find("mobile") != -1

    def get_user_by_token(self, token):
        return self.application.token_handler.get_user_by_token(token)

    def get_current_user(self):
        return self.application.token_handler.get_user_by_token(self.get_cookie("user_token"))

    @property
    def token_handler(self):
        return self.application.token_handler

    @property
    def msg_man(self):
        return self.application.msg_man

    @property
    def users_manager(self):
        return self.application.users_manager

    @property
    def coin_manager(self):
        return self.application.coin_manager

    @property
    def coin_collector(self):
        return self.application.coin_collector


class LogoutHandler(BaseHandler):
    def get(self):
        transfer_to = self.get_argument("transfer_to", "").strip()
        self.clear_all_cookies()
        next_url = ""
        if transfer_to:
            next_url = unquote("/transfer?to="+transfer_to)
        # self.redirect(self.request.protocol + "://" + self.request.host + "/login/customer?next="+next_url)
        self.render("fb_pay/base.html", user="", action=next_url, error="")


class LoginHandler2(BaseHandler):
    ERROR = {"NONE": 0, "EMAIL_NOT_REGISTERED": 1, "PASSWORD_WRONG": 2, "ARGUMENT_ERR": 3}

    def get(self):
        action = self.get_argument("next", "").strip()
        next_url = ""
        if action:
            next_url = unquote(action)
        self.render("fb_pay/base.html", user="", action=next_url, error="")

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_argument("user", "").strip()
        passwd = self.get_argument("passwd", "").strip()
        action = self.get_argument("action", "").strip()
        if user and passwd:
            db_user = yield self.users_manager.find_user(user)
            if db_user:
                if db_user["password"] == self.users_manager.generate_salt_passwd(user, passwd):
                    self.set_cookie("user_token", self.token_handler.generate_user_token(user), expires_days=30)
                    if action:
                        logging.info("login next url=" + action)
                        self.redirect(self.request.protocol + "://" + self.request.host + action)
                    else:
                        self.write("你看，有飞碟~~~~~~~~^_^~~~~~~~~~")
                        self.finish()
                else:
                    self.render("fb_pay/base.html", action=action, user=user, error="亲，登录密码错误，请重新输入！")
            else:
                self.render("fb_pay/base.html", action=action, user="", error="亲，该邮箱尚未注册！")
        else:
            self.render("fb_pay/base.html", action=action, user="", error="亲，请填写完整的登录信息！")


class LoginHandler(BaseHandler):
    ERROR = {"NONE": 0, "EMAIL_NOT_REGISTERED": 1, "PASSWORD_WRONG": 2, "ARGUMENT_ERR": 3}

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        user = self.get_argument("user", "").strip()
        passwd = self.get_argument("passwd", "").strip()
        if user and passwd:
            db_user = yield self.users_manager.find_user(user)
            if db_user:
                if db_user["password"] == self.users_manager.generate_salt_passwd(user, passwd):
                    self.set_cookie("user_token", self.token_handler.generate_user_token(user), expires_days=30)
                    total_coins, coins_by_study = yield self.coin_manager.get_coin(db_user["uid"])
                    db_user["total_coins"] = total_coins
                    db_user["coins_by_study"] = coins_by_study
                    self.write(json.dumps({"err": self.ERROR["NONE"], "user_info": db_user}))
                else:
                    self.write(json.dumps({"err": self.ERROR["PASSWORD_WRONG"]}))
            else:
                self.write(json.dumps({"err": self.ERROR["ARGUMENT_ERR"]}))
        else:
            self.write(json.dumps({"err": self.ERROR["ARGUMENT_ERR"]}))
        self.finish()


class FCoinTransferHandler(BaseHandler):
    ERROR = {"NONE": 0, "UNAUTHENTICATED": 1, "ARGUMENT_ERR": 2, "NOT_ENOUGH_COIN": 3, "FAILED_TRY_AGAIN": 4}

    @tornado.web.asynchronous
    @gen.coroutine
    def get(self):
        # TODO beautify it
        user_to = self.get_argument("to", "")
        if user_to:
            user_from = self.get_current_user()
            if not user_from:
                this_url = self.request.uri
                logging.info("login next url=" + this_url)
                self.redirect(self.request.protocol + "://" + self.request.host + "/login/customer?next=" + quote(this_url))
                return
            db_user_from = yield self.users_manager.find_user(user_from)
            total_coins, coins_by_study = yield self.coin_manager.get_coin(db_user_from["uid"])
            db_user = yield self.users_manager.find_user(user_to)
            if db_user:
                logging.info("transfer coin to " + user_to + " and from " + user_from)
                self.render("fb_pay/fb_pay.html", real_name=db_user["real_name"], user_to=user_to,
                            my_real_name=db_user_from["real_name"],
                            left_coin=total_coins, left_yuan=total_coins/200.0, error="")
            else:
                logging.error("found transfer user_to not existed:" + user_to)
                self.render("fb_pay/fb_pay.html", real_name="", user_to="",
                            my_real_name=db_user_from["real_name"],
                            left_coin=total_coins, left_yuan=total_coins/200.0, error="苍天啊，你要转账的用户不存在！！！请再度扫码一次试试呢！")
        else:
            logging.error("found transfer user_to not existed:" + user_to)
            self.render("fb_pay/fb_pay.html", real_name="", user_to="",
                        my_real_name="",
                        left_coin="", left_yuan="", error="苍天啊，发现参数出错了。亲，非常抱歉，请再度扫码一次试试！")

    def isfloat(self, value):
        try:
            float(value)
            return True
        except ValueError:
            return False

    @tornado.web.asynchronous
    @gen.coroutine
    def post(self):
        user_to = self.get_argument("user_to", "")
        how_much_yuan = self.get_argument("how_much_yuan", "")
        user_from = self.get_current_user()
        if user_to:
            db_user_from = yield self.users_manager.find_user(user_from)
            if db_user_from:
                total_coins, coins_by_study = yield self.coin_manager.get_coin(db_user_from["uid"])
                db_user_to = yield self.users_manager.find_user(user_to)
                if db_user_to:
                    if how_much_yuan and self.isfloat(how_much_yuan):
                        how_much = int(float(how_much_yuan) * 200)
                        if how_much > total_coins:
                            logging.info("transfer coin NOT enough to " + user_to + " and from " + user_from + str(how_much))
                            self.render("fb_pay/fb_pay.html", real_name=db_user_to["real_name"], user_to=user_to,
                                        my_real_name=db_user_from["real_name"],
                                        left_coin=total_coins, left_yuan=total_coins/200.0, error="余额不足")
                        else:
                            logging.info("transfer coin OK to " + user_to + " and from " + user_from + " how much: " +  str(how_much))
                            # begin transfer
                            ok = yield self.coin_manager.transfer_coin_ok(how_much, db_user_from["uid"], user_from, db_user_from["real_name"], db_user_from["university"],
                                db_user_to["uid"], user_to, db_user_to["real_name"], db_user_to["university"])
                            if ok:
                                self.msg_man.new_system_message(u"%s(来自%s)向你转账了%dF！" % (
                                                                db_user_from["real_name"], db_user_from["university"], how_much),
                                                                user_to, self.msg_man.MSG_TYPE["transfer_coin"])
                                # record monthly and weekly transfer
                                self.coin_collector.transfer_coin(db_user_to["uid"], how_much)
                                self.render("fb_pay/fb_over.html", real_name=db_user_to["real_name"], how_much=how_much,
                                            my_real_name=db_user_from["real_name"],
                                            how_much_yuan=how_much/200.0, time=datetime.now().strftime('%Y-%m-%d %H:%M'))
                            else:
                                logging.error("transfer user_to %s failed" % user_to)
                                self.render("fb_pay/fb_pay.html", real_name=db_user_to["real_name"], user_to=user_to,
                                            my_real_name=db_user_from["real_name"],
                                            left_coin=total_coins, left_yuan=total_coins/200.0, error="转账失败了，请稍后再试！")
                    else:
                        self.render("fb_pay/fb_pay.html", real_name=db_user_to["real_name"], user_to=user_to,
                                    my_real_name=db_user_from["real_name"],
                                    left_coin=total_coins, left_yuan=total_coins/200.0, error="请输入有效转账金额！")
                else:
                    logging.error("found transfer user_to not existed:" + user_to)
                    self.render("fb_pay/fb_pay.html", real_name="", user_to=user_to,
                                my_real_name="",
                                left_coin=total_coins, left_yuan=total_coins/200.0, error="苍天啊，你要转账的用户不存在！！！请再度扫码一次试试！")
            else:
                this_url = self.request.uri
                logging.info("login next url=" + this_url)
                self.redirect(self.request.protocol + "://" + self.request.host + "/login/customer?next=" + quote(this_url))
        else:
            logging.error("found transfer user_to not existed:" + user_to)
            self.render("fb_pay/fb_pay.html", real_name="", user_to="",
                        my_real_name="",
                        left_coin="", left_yuan="", error="苍天啊，你要转账的用户不存在！！！请再度扫码一次试试！")


class CoinMessageHandler(BaseHandler):
    ERROR = {"NONE": 0, "ARGUMENT_ERR": 1}

    @tornado.web.asynchronous
    @gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            # Save the future returned by wait_for_messages so we can cancel
            # it in wait_for_messages
            self.user = user
            message = yield self.msg_man.wait_for_message(user)
            if self.request.connection.stream.closed():
                self.msg_man.cancel_wait(self.user)
                return
            if message:
                self.write(json.dumps({"err": self.ERROR["NONE"], "message": message}))
        else:
            self.write(json.dumps({"err": self.ERROR["ARGUMENT_ERR"]}))
        self.finish()

    def on_connection_close(self):
        self.msg_man.cancel_wait(self.user)


class GenQRCodeHandler(BaseHandler):
    def get(self):
        self.render("gen_qrcode.html")

    @tornado.web.asynchronous
    @gen.coroutine
    def post(self):
        user = self.get_argument("user_mail", "")
        bar_size = self.get_argument("bar_size", "")
        error_degree = self.get_argument("error_degree", "")
        if user and bar_size and error_degree:
            logging.info("user: %s size: %s error_degree: %s" % (user, bar_size, error_degree))
            db_user = yield self.users_manager.find_user(user)
            if db_user:
                self.render("shops_qrcode.html", user_mail=user, bar_size=bar_size, error_degree=error_degree)
                return
            else:
                self.write("无效用户！")
        else:
            self.write("参数错误！")
        self.finish()


class GenQRCodeImgHandler(BaseHandler):
    def get(self):
        user = self.get_argument("user_mail", "")
        bar_size = self.get_argument("bar_size", "")
        error_degree = self.get_argument("error_degree", "")
        size = [int(i) for i in bar_size.split('x')]
        s = get_qrcode(size, "http://" + self.request.host + "/transfer?to=" + user, error_degree)
        self.set_header('Content-type', 'image/jpg')
        self.set_header('Content-length', len(s))
        self.write(s)


class TransferRecordListHandler(BaseHandler):
    ERROR = {"NONE": 0, "UNAUTHENTICATED": 1}

    @tornado.web.asynchronous
    @gen.coroutine
    def get(self):
        user = self.get_current_user()
        if user:
            db_user = yield self.users_manager.find_user(user)
            if db_user:
                uid = db_user["uid"]
                daily_income, monthly_income, total_income = self.coin_collector.get_total_income(uid)
                transfer_list = yield self.coin_manager.get_transfer_list(uid)
                self.write(json.dumps({"err": self.ERROR["NONE"],
                                       "daily_income": daily_income, "transfer_list": transfer_list,
                                       "monthly_income": monthly_income, "total_income": total_income}))
            else:
                self.write(json.dumps({"err": self.ERROR["UNAUTHENTICATED"]}))
        else:
            self.write(json.dumps({"err": self.ERROR["UNAUTHENTICATED"]}))
        self.finish()


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/login', LoginHandler),
            (r'/login/customer', LoginHandler2),
            (r'/gen_qrcode', GenQRCodeHandler),
            # for store
            (r'/transfer_record/list', TransferRecordListHandler),
            (r'/message', CoinMessageHandler),
            (r'/shops_qrcode', GenQRCodeImgHandler),
            (r'/logout', LogoutHandler),
            # for customer
            (r'/transfer', FCoinTransferHandler),
            (r"/(.*)", tornado.web.StaticFileHandler, {"path": os.path.join(os.path.dirname(__file__), "fb_pay_mobile"), "default_filename": "index.html"})
        ]

        settings = {
            'template_path': 'templates',
            'static_path': 'static',
        }
        tornado.web.Application.__init__(self, handlers, **settings)
        self.msg_man = MessageManager()
        self.users_manager = UserManager()
        self.coin_manager = CoinManager()
        self.coin_collector = CoinStatisticCollector()
        self.token_handler = TokenHandler(TOKEN_SECRET)


def main():
    tornado.options.parse_command_line()
    app = Application()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(RUN_PORT)
    ioloop = tornado.ioloop.IOLoop.instance()
    autoreload.start(ioloop)
    ioloop.start()

if __name__ == '__main__':
    main()

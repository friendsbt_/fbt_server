__author__ = 'bone'

from redis_handler import RedisHandler
from university_db import UniversityDB
from course_db import CourseDB
from redis_cluster_proxy import Redis

from tornado.escape import utf8
import simplejson as json
# from redis import StrictRedis

class RedisDBManager(object):
    # _REDIS_RESOURCE_NUM_KEY = "resources_num"
    _REDIS_COURSE_NUM_KEY = "courses"
    _REDIS_TAG_NUM_KEY = "tag_num"
    _REDIS_STUDENTS_NUM_KEY = "students_num"
    _REDIS_ONLINE_USER_KEY = "online_user"
    _REDIS_DOWNLOAD_KEY = "download-num:"
    _REDIS_PREVIEW_KEY = "preivew-num:"
    RES_NUM_IN_A_PAGE = 20
    MAX_DOWNLOAD_A_DAY_LOGIN = 20
    MAX_DOWNLOAD_A_DAY_NOT_LOGIN = 3
    MAX_PREVIEW_A_DAY = 20
    ONE_DAY = 24*60*60

    def __init__(self, redis_db=None, redis_cache=None):
        self._university_db = UniversityDB()
        self._course_db = CourseDB()
        if redis_db:
            self._redis_db = redis_db
        else:
            self._redis_db = RedisHandler(tp=RedisHandler.type_db)
        if redis_cache:
            self.redis_cache = redis_cache
        else:
            self.redis_cache = Redis()

    def get_redis_db(self):
        return self._redis_db

    def user_upload_resource(self, university, college, course):
        university = utf8(university)
        college = utf8(college)
        course = utf8(course)
        assert self._university_db.is_valid_university_and_college(university, college)
        # self._redis_db.hincrby(self._REDIS_RESOURCE_NUM_KEY+ ":" + university + ":" + college, course, 1)

        # self._redis_db.hincrby(self._REDIS_RESOURCE_NUM_KEY, university, 1)
        # self._redis_db.hincrby(self._REDIS_RESOURCE_NUM_KEY+ ":" + university, college, 1)

        # add courses
        Ok = self._redis_db.sadd(self._REDIS_COURSE_NUM_KEY + ":" + university + ":" + college, course)
        if Ok:
            self._redis_db.hincrby(self._REDIS_COURSE_NUM_KEY + ":" + university, college, 1)
            self._redis_db.hincrby(self._REDIS_COURSE_NUM_KEY, university, 1)

    def delete_course(self, university, college, course):
        university = utf8(university)
        college = utf8(college)
        course = utf8(course)
        assert self._university_db.is_valid_university_and_college(university, college)
        Ok = self._redis_db.srem(self._REDIS_COURSE_NUM_KEY + ":" + university + ":" + college, course)
        if Ok:
            self._redis_db.hincrby(self._REDIS_COURSE_NUM_KEY + ":" + university, college, -1)
            self._redis_db.hincrby(self._REDIS_COURSE_NUM_KEY, university, -1)


    def add_resource_tag(self, tag):
        self._redis_db.hincrby(self._REDIS_TAG_NUM_KEY, tag, 1)

    def get_all_tags(self):
        tag_num = self._redis_db.hgetall(self._REDIS_TAG_NUM_KEY)
        if tag_num:
            return tag_num
        else:
            return {}

    def get_tag_num(self, tag):
        num = (self._redis_db.hget(self._REDIS_TAG_NUM_KEY, tag))
        if num is None:
            return 0
        else:
            return long(num)

    def register_user(self, university, college):
        self._redis_db.hincrby(self._REDIS_STUDENTS_NUM_KEY, university, 1)
        self._redis_db.hincrby(self._REDIS_STUDENTS_NUM_KEY+ ":" + university, college, 1)

    # def get_university_resource_num(self, university):
    #     num = (self._redis_db.hget(self._REDIS_RESOURCE_NUM_KEY, university))
    #     if num is None:
    #         return 0
    #     else:
    #         return long(num)

    def get_university_course_num(self, university):
        num = (self._redis_db.hget(self._REDIS_COURSE_NUM_KEY, university))
        if num is None:
            return 0
        else:
            return long(num)

    # def get_universities_resource_num(self, universities):
    #     num = (self._redis_db.hmget(self._REDIS_RESOURCE_NUM_KEY, universities))
    #     if num is None:
    #         return [0]*len(universities)
    #     else:
    #         return num

    def get_universities_course_num(self, universities):
        num = (self._redis_db.hmget(self._REDIS_COURSE_NUM_KEY, universities))
        if num is None:
            return [0]*len(universities)
        else:
            return num

    # def get_college_resource_num(self, university, college):
    #     num = (self._redis_db.hget(self._REDIS_RESOURCE_NUM_KEY+":"+university, college))
    #     if num is None:
    #         return 0
    #     else:
    #         return long(num)

    def get_college_course_num(self, university, college):
        num = (self._redis_db.hget(self._REDIS_COURSE_NUM_KEY+":"+university, college))
        if num is None:
            return 0
        else:
            return long(num)

    # def get_course_resource_num(self, university, college, course):
    #     num = (self._redis_db.hget(self._REDIS_RESOURCE_NUM_KEY+":"+university+":"+college, course))
    #     if num is None:
    #         return 0
    #     else:
    #         return long(num)

    def get_university_student_num(self, university):
        num = (self._redis_db.hget(self._REDIS_STUDENTS_NUM_KEY, university))
        if num is None:
            return 0
        else:
            return long(num)

    def get_universities_student_num(self, universities):
        num = (self._redis_db.hmget(self._REDIS_STUDENTS_NUM_KEY, universities))
        if num is None:
            return [0]*len(universities)
        else:
            return num

    # def get_college_student_num(self, university, college):
    #     self._redis_db.hget(self._REDIS_RESOURCE_NUM_KEY+":"+university, college)

    def get_university_course_overview(self, current_page, is_211, is_985):
        assert current_page > 0
        if is_211:
            universities = self._university_db.get_211university()
        elif is_985:
            universities = self._university_db.get_985university()
        else:
            universities = self._university_db.get_university()
        course_num = self._redis_db.hgetall(self._REDIS_COURSE_NUM_KEY)
        student_num = self._redis_db.hgetall(self._REDIS_STUDENTS_NUM_KEY)
        res = [{"university": university,
                # "university_id": self._university_db.get_university_id(university),
                "university_icon": self._university_db.get_university_icon(university),
                "resource_num": int(course_num[university]) if university in course_num else 0,
                "student_num": int(student_num[university]) if university in student_num else 0,
                } for university in universities]
        res.sort(lambda x, y: cmp(y["resource_num"], x["resource_num"]))
        return (res[(current_page-1)*self.RES_NUM_IN_A_PAGE:current_page*self.RES_NUM_IN_A_PAGE], current_page, (len(universities)+self.RES_NUM_IN_A_PAGE-1)/self.RES_NUM_IN_A_PAGE)

    def get_university_nav_info(self,university):
        university_resource_num = self.get_university_course_num(university)
        college_resource_num = self.get_college_course_num_list(university)
        top_college = college_resource_num[0]["college"]
        return university_resource_num, college_resource_num, top_college

    def get_college_course_num_list(self, university):
        college_resource_info = self._redis_db.hgetall(self._REDIS_COURSE_NUM_KEY + ":" + university)
        college_resource_num = [{"college": college,
                                 # "college_id": self._university_db.get_college_id(university, college),
                                 "resource_num": int(
                                     college_resource_info[college]) if college in college_resource_info else 0,
                                 } for college in self._university_db.get_college(university)]
        college_resource_num.sort(lambda x, y: cmp(y["resource_num"], x["resource_num"]))
        return college_resource_num

    # def get_college_course_overview(self, university, college, current_page):
    #     assert current_page > 0
    #     course_resource_info = self._redis_db.hgetall(self._REDIS_COURSE_NUM_KEY+ ":" + university+":"+ college)
    #     course_list = []
    #     total_page = 0
    #     if course_resource_info:
    #         temp_list = [{"course": k, "resource_num": int(v), "img": self._course_db.get_course_img(k)} for k,v in course_resource_info.iteritems()]
    #         course_list = sorted(temp_list,key = lambda x: x["resource_num"], reverse=True)
    #         total_page = (len(course_list)+self.RES_NUM_IN_A_PAGE-1)/self.RES_NUM_IN_A_PAGE
    #     return total_page, current_page, course_list[self.RES_NUM_IN_A_PAGE*(current_page-1):self.RES_NUM_IN_A_PAGE*current_page]

    # def get_college_courses(self, university, college):
    #     course_resource_info = self._redis_db.hgetall(self._REDIS_RESOURCE_NUM_KEY+ ":" + university+":"+ college)
    #     if course_resource_info:
    #         return course_resource_info.keys()
    #     else:
    #         return []

    def get_college_courses(self, university, college):
        course_resource_info = self._redis_db.smembers(self._REDIS_COURSE_NUM_KEY+ ":" + university+":"+ college)
        if course_resource_info:
            return list(course_resource_info)
        else:
            return []

    def search_resource_by_university(self, keyword, current_page, is_211, is_985):
        assert current_page > 0
        assert len(keyword) > 0
        universities = self._university_db.get_complete_name(keyword)
        if not universities:
            if is_211:
                universities = self._university_db.get_211university(keyword)
            elif is_985:
                universities = self._university_db.get_985university(keyword)
            else:
                universities = self._university_db.get_university(keyword)
        if not universities:
            res = []
        else:
            cache_key = "resource:search:"+str(is_985)+":"+str(is_211)+":"+keyword
            res_list = self.redis_cache.get(cache_key)
            if res_list:
                res = json.loads(res_list)
                # print "use cache"
            else:
                universities = list(universities)
                resource_num = self.get_universities_course_num(universities)
                student_num = self.get_universities_student_num(universities)
                res = [{"university": university,
                        # "university_id": self._university_db.get_university_id(university),
                        "university_icon": self._university_db.get_university_icon(university),
                        "resource_num": int(resource_num[i]) if resource_num[i] is not None else 0,
                        "student_num": int(student_num[i]) if student_num[i] is not None else 0,
                        } for i,university in enumerate(universities)]
                res.sort(lambda x, y: cmp(y["resource_num"], x["resource_num"]))
                self.redis_cache.set(cache_key, json.dumps(res))
                self.redis_cache.expire(cache_key, 10 * 60)
        return (res[(current_page-1)*self.RES_NUM_IN_A_PAGE:current_page*self.RES_NUM_IN_A_PAGE], current_page, (len(universities)+self.RES_NUM_IN_A_PAGE-1)/self.RES_NUM_IN_A_PAGE)

    def record_login_user(self, user, user_ip):
        self._redis_db.sadd(self._REDIS_ONLINE_USER_KEY, user)
        self._redis_db.setex(self._REDIS_ONLINE_USER_KEY+":"+user, self.ONE_DAY, user_ip)

    def record_logout_user(self, user):
        self._redis_db.srem(self._REDIS_ONLINE_USER_KEY, user)
        self._redis_db.delete(self._REDIS_ONLINE_USER_KEY+":"+user)

    def get_online_users(self):
        users = self._redis_db.smembers(self._REDIS_ONLINE_USER_KEY)
        pipe = self._redis_db.pipeline()
        for u in users:
            pipe.get(self._REDIS_ONLINE_USER_KEY+":"+u)
        result = pipe.execute()
        return [r for r in result if r]

    def is_valid_downloaded_user(self, user):
        download_num = self._redis_db.get(self._REDIS_DOWNLOAD_KEY+user)
        if download_num:
            download_num = int(download_num)
            if download_num >= self.MAX_DOWNLOAD_A_DAY_LOGIN:
                return False
            else:
                self._redis_db.incr(self._REDIS_DOWNLOAD_KEY+user)
        else:
            self._redis_db.setex(self._REDIS_DOWNLOAD_KEY+user, self.ONE_DAY, 1)
        return True

    def is_valid_downloaded_ip(self, user_ip, max_download_cnt):
        download_num = self._redis_db.get(self._REDIS_DOWNLOAD_KEY+user_ip)
        if download_num and download_num.isdigit():
            download_num = int(download_num)
            if download_num >= max_download_cnt:
                return False
            else:
                self._redis_db.incr(self._REDIS_DOWNLOAD_KEY+user_ip)
        else:
            self._redis_db.setex(self._REDIS_DOWNLOAD_KEY+user_ip, self.ONE_DAY, 1)
        return True

    def is_valid_download(self, user, user_ip):
        if user:
            return self.is_valid_downloaded_user(user) and \
                   self.is_valid_downloaded_ip(user_ip, self.MAX_DOWNLOAD_A_DAY_LOGIN)
        else:
            return self.is_valid_downloaded_ip(user_ip, self.MAX_DOWNLOAD_A_DAY_NOT_LOGIN)

    def is_valid_preview_ip(self, user_ip):
        download_num = self._redis_db.get(self._REDIS_PREVIEW_KEY+user_ip)
        if download_num and download_num.isdigit():
            download_num = int(download_num)
            if download_num >= self.MAX_PREVIEW_A_DAY:
                return False
            else:
                self._redis_db.incr(self._REDIS_PREVIEW_KEY+user_ip)
        else:
            self._redis_db.setex(self._REDIS_PREVIEW_KEY+user_ip, self.ONE_DAY, 1)
        return True

    def is_valid_preview(self, user_ip):
        return self.is_valid_preview_ip(user_ip)


class RedisDBManagerP2P(RedisDBManager):
    # override these class attr
    _PREFIX = "p2p:"
    # _REDIS_RESOURCE_NUM_KEY = _PREFIX+RedisDBManager._REDIS_RESOURCE_NUM_KEY
    _REDIS_TAG_NUM_KEY = _PREFIX+RedisDBManager._REDIS_TAG_NUM_KEY
    _REDIS_STUDENTS_NUM_KEY = _PREFIX+RedisDBManager._REDIS_STUDENTS_NUM_KEY
    # _REDIS_ONLINE_USER_KEY = _PREFIX+RedisDBManager._REDIS_ONLINE_USER_KEY

    def __init__(self, redis_db=None, redis_cache=None):
        RedisDBManager.__init__(self, redis_db, redis_cache)

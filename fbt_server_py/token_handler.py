# coding: utf8

from cryptography.fernet import Fernet, InvalidToken
from tornado.escape import utf8


class TokenHandler(object):
    TOKEN_SUFFIX = "@fbt"

    def __init__(self, key):
        self._fernet = Fernet(key or Fernet.generate_key())

    def generate_user_token(self, user):
        user = utf8(user)
        token = self._fernet.encrypt(user + self.TOKEN_SUFFIX)
        return token

    def get_user_by_token(self, token):
        try:
            if not token: return None
            token = utf8(token)
            dec_str = self._fernet.decrypt(token)
            suffix_len = len(self.TOKEN_SUFFIX)
            if dec_str[-suffix_len:] == self.TOKEN_SUFFIX:
                return dec_str[:(len(dec_str)-suffix_len)]
            else:
                return None
        except InvalidToken:
            return None


if __name__ == "__main__":
    t = TokenHandler(None)
    from time import time
    from tornado.escape import to_unicode
    start = long(time())
    for i in range(1000):
        token = t.generate_user_token(u"test我@test.com")
        assert token is not None
        user = t.get_user_by_token(to_unicode(token))
        assert user == "test我@test.com"
        user = t.get_user_by_token("fake token")
        assert user is None
    end = long(time())
    if end - start <= 1:
        print "pass test"
    else:
        print "test failed"

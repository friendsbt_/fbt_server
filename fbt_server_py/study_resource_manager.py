# -*- coding: utf-8 -*-
__author__ = 'bone-lee'

from redis_cluster_proxy import Redis
from abstract_searcher import AbstractSearcher
from university_course_manager import RedisDBManager
from university_db import UniversityDB
from course_db import CourseDB
from searcher import RedisFullTextSearcher
from qiniu_utils import office_to_pdf
from es_search import ESSearch
from es_mapping import course_analyze_fields, course_index_mapping, course_none_analyze_fields
from es_mapping import resource_analyze_fields, resource_index_mapping, resource_none_analyze_fields
from constant import ES_HOST, ES_PORT
from util import seconds_of_today_at_wee_hours
from coin_manager import DOWNLOAD_COIN_OF_FILE_TAGS

from tornado import gen
from tornado.escape import utf8
from time import time
from datetime import datetime
from pypinyin import pinyin, TONE2
from tornado.escape import to_unicode
import motorclient
import simplejson as json
import re
import os
import mongoclient


class StudyResourceManager(AbstractSearcher):
    _DESCENDING = -1
    RES_PAGE_NUM = 15
    RES_PAGE_NUM_IN_MY_RES = 16
    COURSE_PAGE_NUM = 20
    COLLEGE_COURSEPAGE_NUM = 15
    RECOMMEND_NUM = 8
    preview_extensions = {"WORD": {"doc", "docx", "odt", "rtf", "wps"},
                          "PPT": {"ppt", "pptx", "odp", "dps"},
                          "EXCEL": {"xls", "xlsx", "ods", "csv", "et"},
                          "PDF": {"pdf"}}


    def __init__(self, mongodb=None, redis_cache=None, redis_db_manager=None, es_host=None, es_port=None):
        self._db = mongodb or motorclient.fbt
        self._redis_cache = redis_cache or Redis()
        super(StudyResourceManager, self).__init__(self._redis_cache)
        self._redis_db_man = redis_db_manager or RedisDBManager()
        self._course_searcher = ESSearch(host=es_host or ES_HOST, port=es_port or ES_PORT,
                                         index_name="course_index",
                                         type_name="course",
                                         analyze_fields=course_analyze_fields,
                                         none_analyze_fields=course_none_analyze_fields,
                                         index_mapping=course_index_mapping)
        self._resource_searcher = ESSearch(host=es_host or ES_HOST, port=es_port or ES_PORT,
                                           index_name="resource_index",
                                           type_name="resource",
                                           analyze_fields=resource_analyze_fields,
                                           none_analyze_fields=resource_none_analyze_fields,
                                           index_mapping=resource_index_mapping)
        self._university_db = UniversityDB()
        self._course_db = CourseDB()

    def is_valid_tag(self, tag):
        return utf8(tag) in DOWNLOAD_COIN_OF_FILE_TAGS

    @gen.coroutine
    def upload_resource(self, data, user, uid, pass_audit=True, nick_name=u"未知"):
        if "need_anonymous" in data and data["need_anonymous"]:
            data["uploader_nick"] = "我是雷锋"
        else:
            data["uploader_nick"] = nick_name
        data["uploader"] = user
        data["university_short_names"] = self._university_db.get_short_name(data["university"])
        data["real_uploader"] = user
        data["uid"] = uid
        data["index_ctime"] = long(time())
        data["ctime"] = datetime.now().strftime('%Y-%m-%d %H:%M')
        # create index
        data["download_num"] = 0
        data["comment_num"] = 0
        data["img"] = self._course_db.get_course_img(data["course"])
        # create index
        data["file_id"] = self.gen_fileid(data["file_size"])
        data["rating_num"] = 1
        data["thumb_up_num"] = 0
        data["thumb_down_num"] = 0
        data["thumb_up_users"] = []
        data["thumb_down_users"] = []
        # for search by tree
        data["location"] = str(self._university_db.get_university_id(data["university"])) + ":" + str(self._university_db.get_college_id(data["university"],data["college"]))+":"+str(self._course_db.get_course_id(data["course"]))
        # just for index and sort
        # data["university_id"] = self._university_db.get_university_id(data["university"])
        # data["college_id"] = self._university_db.get_college_id(data["university"],data["college"])
        # data["course_id"] = self._course_db.get_course_id(data["course"])
        resource, resource2 = yield [self.get_study_resource_collection().find_one({"file_hash":  data["file_hash"]}, {"_id": 1}),
                self.get_auditing_resource_collection().find_one({"file_hash":  data["file_hash"]}, {"_id": 1})]
        if resource or resource2:
            raise gen.Return(None)
        yield self.get_auditing_resource_collection().insert(data)
        if pass_audit:
            yield self.pass_audit_resource(data["file_id"])
        raise gen.Return(data["file_id"])

    @gen.coroutine
    def change_resource(self, resource):
        resource2 = yield self.get_auditing_resource_collection().find_one({"file_id": resource["file_id"]})
        if resource2:
            for key in resource:
                if key in resource2:
                    resource2[key] = resource[key]
            if "unpass_reason" in resource2:
                del resource2["unpass_reason"]
            resource2["img"] = self._course_db.get_course_img(resource2["course"])
            resource2["location"] = str(self._university_db.get_university_id(resource2["university"])) + ":" + str(self._university_db.get_college_id(resource2["university"],resource2["college"]))+":"+str(self._course_db.get_course_id(resource2["course"]))
            yield self.get_auditing_resource_collection().save(resource2)
            raise gen.Return(True)
        else:
            raise gen.Return(False)

    @gen.coroutine
    def remove_resource(self, file_id_list):
        for file_id in file_id_list:
            yield self.get_auditing_resource_collection().remove({"file_id": file_id})

    @gen.coroutine
    def add_course_to_db(self, resource):
        course = {"university":resource["university"], "college":resource["college"],"course": resource["course"],
                  "index_ctime": long(time()), "ctime": datetime.now().strftime('%Y-%m-%d %H:%M')}
        course["location"] = self.generate_course_location(resource["university"], resource["college"])
        course["university_short_names"] = self._university_db.get_short_name(resource["university"])
        #str(self._university_db.get_university_id(resource["university"]))+":"+str(self._university_db.get_college_id(resource["university"],resource["college"]))
        if utf8(resource["teacher"]) == "无":
            to_add = {"real_uploader": resource["real_uploader"]}
        else:
            to_add = {"teacher": resource["teacher"],"real_uploader":resource["real_uploader"]}
        course_id = self.generate_course_id(course["course"],course["university"], course["college"])
        course_detail = yield self.get_course_collection().find_and_modify({"course_id": course_id},{"$set": course,
                                                                            "$addToSet": to_add,
                                                                            "$inc":{"resource_num": 1}}, True)
        self.extract_course_info(resource, course_id)
        if course_detail:
            if utf8(resource["teacher"]) != "无":
                course_detail["teacher"].append(resource["teacher"])
                yield self._course_searcher.update_field(course_id, "teacher", course_detail["teacher"])
        else:
            if utf8(resource["teacher"]) == "无":
                course["teacher"] = []
            else:
                course["teacher"] = [resource["teacher"]]
            course["course_id"] = course_id
            yield self._course_searcher.insert(course_id, course)

    def temp_add_course_to_db2(self, resource):
        course = {"university":resource["university"], "college":resource["college"],"course": resource["course"],
                  "index_ctime": long(time()), "ctime": datetime.now().strftime('%Y-%m-%d %H:%M')}
        course["location"] = self.generate_course_location(resource["university"], resource["college"])
        if utf8(resource["teacher"]) == "无":
            to_add = {"real_uploader": resource["real_uploader"]}
        else:
            to_add = {"teacher": resource["teacher"],"real_uploader":resource["real_uploader"]}
        course_id = self.generate_course_id(course["course"],course["university"], course["college"])
        mongoclient.fbt.courses.update({"course_id": course_id},{"$set": course,
                                                                            "$addToSet": to_add,
                                                                            "$inc":{"resource_num": 1}}, True)
        self.extract_course_info(resource, course_id)

    @gen.coroutine
    def pass_audit_resource(self, file_id):
        resource = yield self.get_auditing_resource_collection().find_and_modify({"file_id": file_id}, remove=True)
        if "_id" in resource:
            del resource['_id']
        yield self.get_study_resource_collection().insert(resource)
        self.add_resource_abstract_info(resource)
        yield self._resource_searcher.insert(file_id, resource)
        yield self.add_course_to_db(resource)
        if self.need_translate_to_pdf(resource["filename"]):
            yield office_to_pdf(resource["file_key"], self.get_file_extension(resource["filename"]))

    def is_office_file(self, filename):
        file_extension = self.get_file_extension(filename)
        return file_extension in self.preview_extensions["WORD"] or \
            file_extension in self.preview_extensions["PPT"] or \
            file_extension in self.preview_extensions["EXCEL"]

    def get_file_extension(self, filename):
        _, file_extension = os.path.splitext(filename)
        file_extension = file_extension.lower()[1:]
        return file_extension

    def is_pdf_file(self, filename):
        _, file_extension = os.path.splitext(filename)
        return self.get_file_extension(filename) in self.preview_extensions["PDF"]

    def need_translate_to_pdf(self, filename):
        file_extension = self.get_file_extension(filename)
        if file_extension in self.preview_extensions["WORD"] or \
           file_extension in self.preview_extensions["PPT"] or \
           file_extension in self.preview_extensions["EXCEL"]:
            return True
        return False

    def customize_add_course2(self, university, college, course_name, teacher):
        assert self._university_db.is_valid_university_and_college(university, college)
        course = {"university": university, "college": college, "course": course_name, "index_ctime": long(time()),
                  "ctime": datetime.now().strftime('%Y-%m-%d %H:%M'),
                  "location": self.generate_course_location(university, college)}
        course_id = self.generate_course_id(course["course"],course["university"], course["college"])
        if teacher:
            mongoclient.fbt.courses.update({"course_id": course_id},{"$set": course,
                                                                                "$addToSet":{"teacher": teacher}}, True)
        else:
            mongoclient.fbt.courses.update({"course_id": course_id},{"$set": course}, True)
        self._redis_db_man.user_upload_resource(university, college, course_name)

    @gen.coroutine
    def customize_add_course(self, university, college, course_name, teacher):
        assert self._university_db.is_valid_university_and_college(university, college)
        course = {"university": university, "college": college, "course": course_name, "index_ctime": long(time()),
                  "ctime": datetime.now().strftime('%Y-%m-%d %H:%M'),
                  "location": self.generate_course_location(university, college)}
        course_id = self.generate_course_id(course["course"],course["university"], course["college"])
        if teacher:
            yield self.get_course_collection().update({"course_id": course_id},{"$set": course,
                                                                            "$addToSet":{"teacher": teacher}}, True)
        else:
            yield self.get_course_collection().update({"course_id": course_id},{"$set": course}, True)
        self._redis_db_man.user_upload_resource(university, college, course_name)

    def generate_course_location(self, university, college):
        # return str(self._university_db.get_university_id(university))+":"+str(self._university_db.get_college_id(university, college))
        university_id = self._university_db.get_university_id(university)
        college_id = self._university_db.get_college_id(university, college)
        return ":".join([str(university_id), str(college_id)])

    def add_resource_abstract_info(self, resource):
        self._redis_db_man.add_resource_tag(resource["tag"])
        # search_text = resource["tag"] + " " + resource["course"] + " " + resource["college"] + " " + resource[
        #     "university"]
        # search_text += " ".join(self._university_db.get_short_name(resource["university"]))
        # search_text += " "+resource["resource_name"]+" "+ resource["teacher"]
        # self._redis_searcher.index(search_text, resource["file_id"])

    def extract_course_info(self, resource, course_id):
        self._redis_db_man.user_upload_resource(resource["university"], resource["college"], resource["course"])
        # search_text_for_course = resource["course"] + " " + resource["college"] + " " + resource["university"]
        # search_text_for_course += " ".join(self._university_db.get_short_name(resource["university"]))
        # search_text_for_course += " "+resource["teacher"]
        # self._redis_searcher2.index(search_text_for_course, course_id)

    @gen.coroutine
    def get_resource_overview(self):
        DESCENDING = -1
        cursor = self.get_study_resource_collection().find({},{"_id": 0, "real_uploader": 0}).sort([('index_ctime', DESCENDING)]).limit(20)
        all_resources = yield cursor.to_list(None)
        raise gen.Return(all_resources)

    def gen_fileid(self, file_size):
        return str(long(time()*(1000))) + "_" + str(file_size)

    @gen.coroutine
    def increase_download_num(self, file_id, user):
        yield self.get_study_resource_collection().update({"file_id": file_id},{"$inc": {"download_num":1}},upsert=True)

    @gen.coroutine
    def find_resource(self, file_id):
        res = yield self.get_study_resource_collection().find_one({"file_id": file_id},
                                                                  {"file_size": 1, "resource_name": 1, "university": 1, "college": 1, "img":1, "uid":1,
                                                                   "tag": 1, "real_uploader": 1, "file_key": 1, "filename": 1, "preview_key": 1, "download_link":1})
        raise gen.Return(res)

    @gen.coroutine
    def find_a_resource(self, file_id):
        res = yield self.get_study_resource_collection().find_one({"file_id": file_id})
        raise gen.Return(res)

    @gen.coroutine
    def get_course_resources(self, university, college, course, current_page=1):
        location = self.generate_course_id(course, university, college)
        ret = yield self.get_course_resources_by_id(location, current_page)
        raise gen.Return(ret)

    @gen.coroutine
    def hide_resource(self, file_ids):
        ok = False
        for file_id in file_ids:
            ret = yield self.get_study_resource_collection().find_and_modify({"file_id": file_id}, {"$set": {"hidden": 1}}, upsert=False, multi=True)
            if ret:
                ok = True
                print ret["university"],ret["college"],ret["course"],ret["resource_name"], "hidden Ok:", file_id
                # can't search any more
                # self._redis_searcher.remove(file_id)
                yield self._resource_searcher.delete(file_id)
        raise gen.Return(ok)

    @gen.coroutine
    def get_course_resources_by_id(self, location, current_page=1):
        cursor = self.get_study_resource_collection().find({"location": location, "hidden":{"$ne": 1}},{"_id":0,"location":0,"real_uploader": 0}).sort(
                [('download_num', self._DESCENDING),])
        total_cnt = yield cursor.count() #(self._redis_db_man.get_course_resource_num(university, college, course)+self.RES_PAGE_NUM-1)/self.RES_PAGE_NUM
        total_page= (total_cnt+self.RES_PAGE_NUM-1)/self.RES_PAGE_NUM
        cursor = cursor.limit(self.RES_PAGE_NUM).skip((current_page - 1) * self.RES_PAGE_NUM)
        resource_list = []
        while (yield cursor.fetch_next):
            res = cursor.next_object()
            resource_list.append(res)
        raise gen.Return([total_page, current_page, resource_list])

    @gen.coroutine
    def rename_course(self, university, college, old_course_name, new_course_name):
        yield self.get_course_collection().update({"location": self.generate_course_location(university, college), "course": old_course_name}, {"$set": {"course": new_course_name}}, upsert=False, multi=True)
        yield self.get_study_resource_collection().update({"location": self.generate_course_id(old_course_name, university, college)}, {"$set": {
            "location": self.generate_course_id(new_course_name, university, college), "course": new_course_name}}, upsert=False, multi=True)
        self._redis_db_man.delete_course(university, college, old_course_name)
        self._redis_db_man.user_upload_resource(university, college, new_course_name)

    def generate_course_id(self, course_name, university,college):
        university_id = self._university_db.get_university_id(university)
        college_id = self._university_db.get_college_id(university, college)
        course_id = self._course_db.get_course_id(course_name)
        return ":".join([str(university_id), str(college_id), str(course_id)])

    @gen.coroutine
    def get_recommended_resources(self):
        tag_dict = self._redis_db_man.get_all_tags()
        all_tags = tag_dict.keys()

        res_dict = {}
        for tag in all_tags:
            cursor = self.get_study_resource_collection().find({"tag": tag},{"_id":0,"location":0,"real_uploader": 0}).sort(
                    [('download_num', self._DESCENDING),]).limit(self.RECOMMEND_NUM)
            resource_list = []
            while (yield cursor.fetch_next):
                res = cursor.next_object()
                resource_list.append(res)
            res_dict[tag] = resource_list
        ret = [{"tag": tag, "resource_num": int(tag_dict[tag]),"resource_list":res_dict[tag]} for tag in all_tags]
        raise gen.Return(ret)

    @gen.coroutine
    def get_more_recommended_resources(self, tag, current_page):
        total_page = (self._redis_db_man.get_tag_num(tag)+self.RES_PAGE_NUM-1)/self.RES_PAGE_NUM
        cursor = self.get_study_resource_collection().find({"tag": tag},{"_id":0,"location":0,"real_uploader": 0}).sort(
                    [('download_num', self._DESCENDING),]).limit(self.RES_PAGE_NUM).skip((current_page-1)*self.RES_PAGE_NUM)
        resource_list = []
        while (yield cursor.fetch_next):
            res = cursor.next_object()
            resource_list.append(res)
        raise gen.Return([total_page, current_page, resource_list])

    def get_cache_key_for_search(self, keyword):
        return "course:search:"+keyword

    def get_cache_key_for_search2(self, keyword):
        return "resource:search:"+keyword

    @gen.coroutine
    def search_course2(self, keyword, current_page):
        cache_key = self.get_cache_key_for_search2(keyword)
        self.record_keyword(keyword)
        cached_data = self._redis_cache.get(cache_key)
        if cached_data:
            file_ids = json.loads(cached_data)
        else:
            # file_ids = self._redis_searcher.search(keyword)
            resources = yield self._resource_searcher.search(keyword)
            file_ids = [_["file_id"] for _ in resources]
            # pipe = self._redis_cache.pipeline()
            # pipe.set(cache_key, json.dumps(file_ids))
            # pipe.expire(cache_key, 10 * 60)
            # pipe.execute()
            self._redis_cache.setex(cache_key, 10*60, json.dumps(file_ids))
            # print "cache_key:"+cache_key
            # print "use cache OK"
        total_res_num = len(file_ids)
        resource_list = []
        for file_id in file_ids[(current_page-1)*self.RES_PAGE_NUM:current_page*self.RES_PAGE_NUM]:
            res = yield self.get_study_resource_collection().find_one({"file_id": file_id},{"_id":0,"location":0,"real_uploader": 0})
            resource_list.append(res)
        raise gen.Return([(total_res_num+self.RES_PAGE_NUM-1)/self.RES_PAGE_NUM, current_page, resource_list])

    @gen.coroutine
    def search_course(self, keyword, current_page):
        cache_key = self.get_cache_key_for_search(keyword)
        cached_data = self._redis_cache.get(cache_key)
        self.record_keyword(keyword)
        if cached_data:
            course_ids = json.loads(cached_data)
        else:
            courses = yield self._course_searcher.search(keyword)
            course_ids = [_["course_id"] for _ in courses]
            # course_ids = self._redis_searcher2.search(keyword)
            pipe = self._redis_cache.pipeline()
            pipe.set(cache_key, json.dumps(course_ids))
            pipe.expire(cache_key, 3 * 60)
            pipe.execute()
            # print "cache_key:"+cache_key
            # print "use cache OK"
        total_res_num = len(course_ids)
        course_list = []
        for course_id in course_ids[(current_page-1)*self.COURSE_PAGE_NUM:current_page*self.COURSE_PAGE_NUM]:
            course = yield self.get_course_collection().find_one({"course_id": course_id},{"_id":0,"real_uploader": 0})
            if course:
                if "img" not in course:
                    course["img"] = self._course_db.get_default_img()
                course_list.append(course)
        raise gen.Return([(total_res_num+self.COURSE_PAGE_NUM-1)/self.COURSE_PAGE_NUM, current_page, course_list])


    @gen.coroutine
    def clear_db_just_for_test(self):
        yield self.get_study_resource_collection().remove({})
        yield self.get_auditing_resource_collection().remove({})
        yield self.get_course_collection().remove({})
        yield self._course_searcher.clean_all()
        yield self._resource_searcher.clean_all()

    @gen.coroutine
    def increase_resource_comment_num(self, file_id):
        yield self.get_study_resource_collection().update({"file_id": file_id},{"$inc": {"comment_num":1}}, upsert=True)
        # yield self.get_study_resource_collection().find_and_modify({"file_id": file_id},{"$inc": {"comment_num":1}}, upsert=True)

    @gen.coroutine
    def increase_course_comment_num(self, course, university, college):
        course_id = self.generate_course_id(course, university, college)
        yield self.increase_course_comment_num2(course_id)
        # yield self.get_course_collection().update({"course_id": course_id},{"$inc": {"comment_num":1}}, upsert=True)

    @gen.coroutine
    def increase_course_comment_num2(self, course_id):
        yield self.get_course_collection().update({"course_id": course_id},{"$inc": {"comment_num":1}}, upsert=True)

    @gen.coroutine
    def rating(self, file_id, delta_score, delta_user):
        assert delta_user == 0 or delta_user == 1
        resource = yield self.get_study_resource_collection().find_one({"file_id": file_id})
        if resource:
            resource["rate"] = (resource["rate"]*resource["rating_num"]+delta_score)/(resource["rating_num"]+delta_user+0.0)
            resource["rating_num"] += delta_user
            yield self.get_study_resource_collection().save(resource)
            raise gen.Return(resource["rate"])
        else:
            raise gen.Return(0)

    @gen.coroutine
    def get_user_resource(self, user, current_page=1):
        cursor = self.get_study_resource_collection().find({"real_uploader": user}, {"_id":0, "location":0,"real_uploader": 0}).sort([('index_ctime', self._DESCENDING),])
        total_comment_num = yield cursor.count()
        cursor = cursor.limit(self.RES_PAGE_NUM_IN_MY_RES).skip((current_page-1)*self.RES_PAGE_NUM_IN_MY_RES)
        comment_list = yield cursor.to_list(None)
        raise gen.Return([(total_comment_num+self.RES_PAGE_NUM_IN_MY_RES-1)/self.RES_PAGE_NUM_IN_MY_RES, current_page, comment_list])

    @gen.coroutine
    def get_user_audit_resource(self, user, current_page=1, is_auditing=False):
        if is_auditing:
            cursor = self.get_auditing_resource_collection().find({"real_uploader": user, "unpass_reason": {"$exists": False}}, {"_id":0, "location":0,"real_uploader": 0}).sort([('index_ctime', self._DESCENDING),])
        else:
            cursor = self.get_auditing_resource_collection().find({"real_uploader": user, "unpass_reason": {"$exists": True}}, {"_id":0, "location":0,"real_uploader": 0}).sort([('index_ctime', self._DESCENDING),])
        total_comment_num = yield cursor.count()
        cursor = cursor.limit(self.RES_PAGE_NUM_IN_MY_RES).skip((current_page-1)*self.RES_PAGE_NUM_IN_MY_RES)
        comment_list = yield cursor.to_list(None)
        raise gen.Return([(total_comment_num+self.RES_PAGE_NUM_IN_MY_RES-1)/self.RES_PAGE_NUM_IN_MY_RES, current_page, comment_list])

    @gen.coroutine
    def audit_resource(self, file_id, reason):
        yield self.get_auditing_resource_collection().update({"file_id": file_id}, {"$set":{"unpass_reason": reason}})

    def get_study_resource_collection(self):
        return self._db.study_resources

    def get_course_collection(self):
        return self._db.courses

    def get_auditing_resource_collection(self):
        return self._db.auditing_study_resources

    @gen.coroutine
    def get_a_resource(self, file_id):
        resource = yield self.get_study_resource_collection().find_one({"file_id": file_id},{"_id":0,"location":0,"real_uploader": 0})
        raise gen.Return(resource)

    @gen.coroutine
    def get_a_resource2(self, file_id):
        resource = yield self.get_auditing_resource_collection().find_one({"file_id": file_id},{"_id":0,"location":0,"real_uploader": 0})
        raise gen.Return(resource)

    @gen.coroutine
    def get_college_course_overview(self, university, college, current_page):
        assert current_page > 0
        location = str(self._university_db.get_university_id(university))+":"+str(self._university_db.get_college_id(university,college))
        key = "course:location:" + location
        cached_data = self._redis_cache.get(key)
        if cached_data:
            course_list = json.loads(cached_data)
        else:
            cursor = self.get_course_collection().find({"location": location}, {"_id":0}).sort([('resource_num', self._DESCENDING),])
            course_list = yield cursor.to_list(None)
            course_list = [course for course in course_list if "resource_num" in course]
            course_list = sorted(course_list, key=lambda x: pinyin(to_unicode(("course" in x and x["course"]) or ""), style=TONE2))
            for course in course_list:
                if "img" not in course:
                    course["img"] = self._course_db.get_default_img()
                if "comment_num" not in course:
                    course["comment_num"]=0
            self._redis_cache.setex(key, 30*60, json.dumps(course_list))
        total_course_num = len(course_list)
        raise gen.Return([(total_course_num+self.COLLEGE_COURSEPAGE_NUM-1)/self.COLLEGE_COURSEPAGE_NUM, current_page, course_list[(current_page-1)*self.COLLEGE_COURSEPAGE_NUM:current_page*self.COLLEGE_COURSEPAGE_NUM]])

    @gen.coroutine
    def get_course(self, course, university, college):
        course_id = self.generate_course_id(course,university, college)
        course = yield self.get_course2(course_id)
        raise gen.Return(course)

    @gen.coroutine
    def get_course2(self,course_id):
        course = yield self.get_course_collection().find_one({"course_id": course_id}, {"_id":0, "real_uploader": 0})
        if course:
            if "img" not in course:
                course["img"]=self._course_db.get_default_img()
            if "comment_num" not in course:
                course["comment_num"]=0
        raise gen.Return(course)

    @gen.coroutine
    def thumb_up(self, file_id, user):
        resource = yield self.get_study_resource_collection().find_one({"file_id": file_id})
        if resource:
            if "thumb_up_users" not in resource:
                resource["thumb_up_users"] = []
                resource["thumb_up_num"] = 0
            if user not in resource["thumb_up_users"]:
                resource["thumb_up_users"].append(user)
                resource["thumb_up_num"] += 1
                yield self.get_study_resource_collection().save(resource)
                raise gen.Return(resource["uploader"])
        raise gen.Return(None)

    @gen.coroutine
    def thumb_down(self, file_id, user):
        resource = yield self.get_study_resource_collection().find_one({"file_id": file_id})
        if resource:
            if "thumb_down_users" not in resource:
                resource["thumb_down_users"] = []
                resource["thumb_down_num"] = 0
            if user not in resource["thumb_down_users"]:
                resource["thumb_down_users"].append(user)
                resource["thumb_down_num"] += 1
                yield self.get_study_resource_collection().save(resource)
                raise gen.Return(resource["uploader"])
        raise gen.Return(None)

    @gen.coroutine
    def merge2college(self, university, college_from, college_to_merge):
        # courses in @college_from will be moved to @college_to_merge
        course_location1 = self.generate_course_location(university, college_from)
        course_location2 = self.generate_course_location(university, college_to_merge)
        ret = yield self.get_course_collection().update({"location": course_location1}, {"$set": {"location": course_location2, "college": college_to_merge}}, upsert=False, multi=True)
        ok = False
        if "updatedExisting" in ret and ret["updatedExisting"]:
            ok = True
            print university, college_from,"==>", college_to_merge,"merged course num:", ret["nModified"]
        cursor = self.get_study_resource_collection().find({"location": re.compile(r"^"+course_location1)})
        num = 0
        while (yield cursor.fetch_next):
            res = cursor.next_object()
            res["location"] = course_location2 + ":" + res["location"].split(":")[-1]
            res["college"] = college_to_merge
            yield self.get_study_resource_collection().save(res)
            num += 1
        print university, college_from,"==>", college_to_merge, "merged resource num:", str(num)
        raise gen.Return(ok)

    @gen.coroutine
    def insert_preview_key(self, src_file_key, file_hash, file_key):
        yield self.get_study_resource_collection().update({"file_key":  src_file_key}, {"$set": {"preview_key": file_key, "preview_hash": file_hash}}, upsert=False)

    @gen.coroutine
    def get_today_upload(self):
        fields = {"resource_name": 1, "uploader": 1, "university": 1, "college": 1, "course": 1}
        cursor = self.get_auditing_resource_collection().find({"index_ctime": {"$gt": seconds_of_today_at_wee_hours()}}, fields)
        auditing_res_list = yield cursor.to_list(None)
        cursor = self.get_study_resource_collection().find({"index_ctime": {"$gt": seconds_of_today_at_wee_hours()}}, fields)
        audited_res_list = yield cursor.to_list(None)
        raise gen.Return([auditing_res_list, audited_res_list])


class StudyResourceManagerP2P(StudyResourceManager):
    def __init__(self, motor_db=None, redis_cache=None, redis_db_manager=None, pymongo_db=None):
        StudyResourceManager.__init__(self,motor_db, redis_cache, redis_db_manager)
        self._redis_searcher = RedisFullTextSearcher("redis_searcher_p2p",self._redis_db_man.get_redis_db())
        if pymongo_db:
            self._sync_db = pymongo_db
        else:
            pass
            # self._sync_db = mongoclient.fbt

    def get_study_resource_collection(self):
        return self._db.study_resources2

    def get_auditing_resource_collection(self):
        return self._db.auditing_study_resources2

    def get_cache_key_for_search(self, keyword):
        return "search:p2p:"+keyword

    def pass_audit_p2p_resource(self, file_id):
        # res = ResourceStoreManager.find_study_resource(self._sync_db, file_id)
        res = None
        if res:
            # "exp_info" : { "resource_school" : "台湾大学", "resource_teacher" : "未知", "resource_course" : "Py教学", "resource_academy" : "信息学院" } }
            #         resource_header = {'file_hash': file_hash, 'file_name': file_name, 'file_id': file_id,
            #                    'main_type': main_type, 'sub_type': sub_type,
            #                    'file_size': file_size, 'mtime': long(time())}
            # ==>
            # for key in ("course", "teacher", "university", "college","filename", "resource_name","download_link","description","tag"):
            # ,{"_id":0,"file_id":1, "exp_info":1, "main_type":1,"file_name":1,"file_size":1, "tags":1, "comments":1, "owners":1})
            exp_info = res["exp_info"]
            course = exp_info["resource_course"]
            teacher = exp_info["resource_teacher"]
            university = exp_info["resource_school"]
            college = exp_info["resource_academy"]
            uploader_nick = exp_info["uploader_nick"] # nick_name
            uploader_user = exp_info["uploader_user"] # email

            file_name = res["file_name"]
            description = res["comments"][0]
            tag = res["tags"][0]
            # owners = res["owners"]
            file_size = res["file_size"]
            rate = res["scores"][0]["score"]
            resource = {"course": course, "teacher": teacher, "university": university,
                        "college": college, "filename": file_name, "resource_name": file_name,
                        "download_link": "", "description": description, "tag": tag,
                        "file_size":file_size, "rate": rate}
            resource["uploader"] = uploader_user
            resource["uploader_nick"] = uploader_nick
            resource["index_ctime"] = long(time())
            resource["ctime"] =  datetime.now().strftime('%Y-%m-%d %H:%M')
            resource["download_num"] = 0
            resource["comment_num"] = 0
            resource["img"] = self._course_db.get_course_img(course)
            resource["rating_num"] = 1
            resource["location"] = str(self._university_db.get_university_id(university)) + ":" + str(self._university_db.get_college_id(university,college))+":"+str(self._course_db.get_course_id(course))
            self._sync_db.study_resources2.insert(resource)
            self.add_resource_abstract_info(resource)


@gen.coroutine
def fix_university():
    res_man = StudyResourceManager()
    # cursor = res_man.get_study_resource_collection().find({"university": "西安建筑科技大学"}).sort(
    #         [('index_ctime', res_man._DESCENDING),]).limit(3000)
    # db = motorclient.fbt
    # cursor = db.study_resources.find({}).sort(
    #     [('index_ctime', res_man._DESCENDING),]).limit(30)
    import mongoclient
    cursor = mongoclient.fbt.study_resources.find({"university": "西安建筑科技大学"}).sort(
        [('index_ctime', res_man._DESCENDING), ]).limit(3000)

    # total_cnt = yield cursor.count()
    # print "total cnt:", total_cnt
    # while (yield cursor.fetch_next):
    #     res = cursor.next_object()
    for res in cursor:
        print res["course"]
        # res_man.temp_add_course_to_db2(res)

if __name__ == "__main__":
    print "running...."
    import tornado.ioloop
    loop = tornado.ioloop.IOLoop.instance()
    loop.run_sync(fix_university, timeout=100)
    print "over...."
